<?php
require("../printable/include/mysql.inc.php");
require("../printable/include/optimize.printable.inc.php");
require("globals.php");


$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);

$portal = new OptimizePortal($COMPANY_ID, $db);

$currentUser = $portal->GetUser($_SESSION['currentuserid']);

$isAdmin = $portal->CheckPriv($currentUser->UserID, 'admin');
$isCorpMan = $portal->CheckPriv($currentUser->UserID, 'corporatemanager');
$isRegMan = $portal->CheckPriv($currentUser->UserID, 'regionmanager');
$isDivMan = $portal->CheckPriv($currentUser->UserID, 'divisionmanager');
if ($isAdmin || $isCorpMan || $isRegMan || $isDivMan)
    $isMan = true;
else
    $isMan = false;

if (!$currentUser) {
    header("Location: login.php?message=" . urlencode("Not logged in or login error."));
    die();
}

$appt = $portal->GetContactAppointments($_GET['contactID']);
$contact = $portal->GetOptimizeContact($_GET['contactID'], $currentUser->UserID);

if (!$contact) {
    die("Invalid Contact");
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <title>
            <?= $portal->CurrentCompany->CompanyName ?> Marketing on Demand :: Appointments with <?= "$contact->FirstName $contact->LastName" ?>
        </title>
        <meta http-equiv="content-type" content="text/html;charset=utf-8" />		
        <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />
        <script src="js/func.js"></script>	

        <?php include("components/bootstrap.php") ?>

    </head>
    <body onload="<?= $close == true ? "window.close();" : ($error == 1 ? "alert('$errorText')" : "" ) ?>">
        <?php
        ?>
        <div class="sectionHeader"><h3>All Appointments with <?= "$contact->FirstName $contact->LastName" ?></h3></div>
        <div id="PopAppointmentDiv" class="sectionDiv well">
            <b><center>Click on an appointment time to view/edit the appointment</center></b>
            <table cellpadding="5" width="100%" class="table table-striped table-hover table-bordered">
                <tr class="colHead" size="2em">
                    <?php
                    if ($isMan) {
                        ?>
                        <td>User Name</td>
                        <?php
                    }
                    ?>
                    <td>Contact Name</td>
                    <td>Company</td>
                    <td>Appointment Time</td>
                    <td>Delete</td>
                </tr>
                <?php
                $i = 0;

                foreach ($appt as $a) {
                    $i++;
                    $aptime = date('n/j/Y g:i a', strtotime($a->AppointmentTime));
                    ?>
                    <tr class="itemSection<?= $i % 2 == 0 ? 'even' : 'odd' ?>">
                        <?php
                        if ($isMan) {
                            if ($a->UserID != $currentUser->UserID)
                                $u = $portal->GetUser($a->UserID);
                            else
                                $u = $currentUser
                                ?>
                            <td><?= $u->FirstName . " " . $u->LastName ?></td>
                            <?php
                        }
                        ?>
                        <td>
                            <?= $contact->FirstName . " " . $contact->LastName ?>
                        </td>
                        <td>
                            <?= $contact->Company ?>
                        </td>
                        <td>
                            <a href = "#" onclick="wopen('edit_appointment.php?id=<?= $a->ContactAppointmentID ?>', 'reminder', 350, 450);" title="Reminder"><?= $aptime ?></a>
                            <a href = "getvcal.php?caid=<?= $a->ContactAppointmentID ?>"><img src="images/outlook-icon.gif" height="15px" width="15px"></a>
                        </td>
                        <td>
                            <a href = "delete_appointment.php?id=<?= $a->ContactAppointmentID ?>">Delete</a>
                        </td>
                    </tr>
                    <?php
                }
                ?>
            </table>
            <a href="add_appointment.php?id=<?= $_GET['contactID'] ?>">Add New Appointment for <?= $contact->FirstName . " " . $contact->LastName ?></a>
            <br/>
            <a href="view_appointments.php">View appointments for all contacts</a>
        </div>

        <?php include("components/bootstrap-footer.php") ?>

    </body>
</html>