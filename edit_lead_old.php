<?php
	require("../printable/include/mysql.inc.php");
	require("../printable/include/optimize.printable.inc.php");
	require("globals.php");
	
	if($_POST['popup_window'] == 'true')
	{
		$_GET['popup'] = true;
	}
	
	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	
	$portal = new OptimizePortal($COMPANY_ID, $db);
	
	$currentUser = $portal->GetUser($_SESSION['currentuserid']);
		
	if(!$currentUser)
	{
		header("Location: login.php?message=" .urlencode("Not logged in or login error."));
		die();
	}
	
	$isSubAdmin = $portal->CheckPriv($currentUser->UserID, 'subadmin');
	$isAdmin = $portal->CheckPriv($currentUser->UserID, 'admin');
	
	if($isSubAdmin)
	{
		$userString = "all";
	}
	else 
	{
		$userString = $currentUser->UserID;
	}
	
	$lead = $portal->GetOptimizeContact($_GET['id'], $userString);
	
	$lead_step_ids = explode(";", $_SESSION['lead_step_ids']);
		
	if(!$lead)
	{
		header("Location: home.php?message=" . urlencode("Lead not Available"));
		die();
	}
	
	if($_POST['Submit'] == 'Update' || $_POST['Submit'] == 'Update & Close')
	{
		$_POST['DOB'] = strtotime("1/1/" . $_POST['DOB']) ? date("Y-1-1", strtotime("1/1/" . $_POST['DOB'])) : "";
		$_POST['CoDOB'] = strtotime("1/1/" . $_POST['CoDOB']) ? date("Y-1-1", strtotime("1/1/" . $_POST['CoDOB'])) : "";
		
	
		if($_POST['ContactStatusTypeID'] == 1 && !$isSubAdmin)
		{
			$_POST['ContactStatusTypeID'] = $lead->ContactStatusTypeID;
		}
		
		$oldStatus = $lead->ContactStatusTypeID;
		foreach($_POST as $k=>$v)
		{
			$lead->$k = $v;
		}
		
		if($isSubAdmin && $_POST['NewUserID'] != $lead->UserID)
		{
			$lead->UserID = $_POST['NewUserID'];
		}
		else
		{
			$lead->UserID = $currentUser->UserID;
		}
		
				
		$portal->UpdateOptimizeContact($lead);
		
		if($_POST['Submit'] == 'Update & Close')
		{
			header('Location: list_lead.php?message=' . urlencode("Lead updated successfully."));
			die();
		}
		
	}
	elseif($_POST['Submit'] == 'Cancel')
	{
		header('Location: list_lead.php?message=' . urlencode("Action Canceled. Lead not updated."));
		die();
	}
	elseif($_POST['Submit'] == 'Delete' && $isSubAdmin)
	{
		$portal->DeleteContact($lead->SubmissionListingID, ($isSubAdmin ? $lead->UserID : $currentUser->UserID));
		
		header('Location: list_lead.php?message=' . urlencode("Lead deleted successfully."));
		die();
	}
	
	$csts = $portal->GetContactStatusTypes();
	$contactStatuseTypes = array();
	$shortStatuses = array();
	$shortStatusDetails = array();
	foreach($csts as $cst) {
		if(!$cst->Disabled) {
			$shortStatuses[$cst->ShortName] = (strlen($cst->DetailName) > 0 ? 1 : 0);
			$shortStatusDetails[$cst->ShortName][] = $cst->ContactStatus;
			$contactStatuseTypes[$cst->ContactStatus] = $cst;
		}
	}
	$contactEventTypes = $portal->GetContactEventTypes();
	$cetLookup = array();
	foreach($contactEventTypes as $cet)
	{
		$cetLookup[$cet->ContactEventTypeID] = $cet->EventType;
	}
	
  	$allEvents = $portal->GetContactHistory($lead->SubmissionListingID);
  	
  	$users = $portal->GetCompanyUsers();
  	$slt = $portal->GetSubmissionListingType($lead->ListingType);
	$appt = $portal->GetOpenAppointment($lead->SubmissionListingID, $currentUser->UserID);
	
	$oppts = $portal->GetOpportunities($lead->SubmissionListingID);
  	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
	<title>
		<?= $portal->CurrentCompany->CompanyName ?> Leads on Demand :: Edit Lead
	</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />		
	<link type="text/css" media="all" rel="stylesheet" href="style.css" />
	<link type="text/css" media="all" rel="stylesheet" href="css/edit_lead.css" />
	<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />
	<script  src="js/func.js"></script>
<script language="JavaScript" type="text/JavaScript">
	function updateContactEvent(id, actionName)
	{
		document.getElementById("LastActionSpan").innerHTML = actionName;
	}

	var statusDetails = new Array();
	<?php
		foreach ($shortStatusDetails as $shortStat=>$details) {
	?>
		statusDetails["<?= $shortStat ?>"] = new Array();
	<?php 
			foreach($details as $det) {
	?>
		statusDetails["<?= $shortStat ?>"][<?= intval($contactStatuseTypes[$det]->ContactStatusTypeID) ?>] = "<?= $contactStatuseTypes[$det]->DetailName ?>";
	<?php 
				
			}
		} 
	?>
	function updateContactStatus(cstid)
	{
		if(isNaN(cstid)) {
			showDetailedContactStatus(cstid);
			return;
		}
		document.getElementById("ContactStatusTypeID").value = cstid;
	}
	function showDetailedContactStatus(cstid) {
		var statusSelect = document.getElementById("detailStatusID");
		document.getElementById("shortStatus").innerText = cstid;
		var i = 0;
		statusSelect.options.length = i;
		for(var id in statusDetails[cstid])
		{
			statusSelect.options.length++;
			statusSelect[i].value = id;
			statusSelect[i].text = statusDetails[cstid][id];
			i++;
		}
		document.getElementById("detailDiv").style.display = "";
	}
	function submitDetailedContactStatus() {
		updateContactStatus(document.getElementById("detailStatusID").value);
		document.getElementById("detailDiv").style.display = "none";
	}
</script>
</head>
<body bgcolor="#FFFFFF">
<?php 
	if($_GET['popup'] == 'true')
	{
	}
	else
	{
		include("components/header.php");
	} 
?>
<div id="body">
<?php 
	if($_GET['popup'] == 'true')
	{
		include("components/popupbar.php");
	}
	else
	{
		$CURRENT_PAGE = "Leads";
		include("components/navbar.php");
	} 
?>
<form method="post" action="<?= $_SERVER['PHP_SELF'] ?>?id=<?= $lead->SubmissionListingID . (isset($_GET['index']) ? "&index=" . $_GET['index'] : ""); ?>">
	<div class="error">
		<?= $_GET['message']; ?>
	</div>
	<div id="EditLeadDiv">
		<center>
		<div class="itemSection">
			<div class="buttonSection">
<?php
	if(isset($_GET['index']) && isset($lead_step_ids[intval($_GET['index']) - 1]))
	{
?>
				<a href="edit_lead.php?id=<?= $lead_step_ids[intval($_GET['index']) - 1] ?>&index=<?= intval($_GET['index']) - 1 ?>">&lt;- Prev</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<?php 
	} 
?>
				<input type="submit" value="Update" name="Submit"/>&nbsp;&nbsp;
				<input type="submit" value="Update & Close" name="Submit"/>&nbsp;&nbsp;
<?php 
	if($_GET['popup'] == 'true')
	{
	}
	else
	{ 
		if($isSubAdmin)
		{
?>
				<input type="submit" value="Delete" name="Submit" onclick="return confirm('Are you sure you want to delete this lead?')"/>&nbsp;&nbsp;
<?php
		}
?>
				<input type="submit" value="Cancel" name="Submit"/>
<?php
	}
	if(isset($_GET['index']) && isset($lead_step_ids[intval($_GET['index']) + 1]))
	{
?>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="edit_lead.php?id=<?= $lead_step_ids[intval($_GET['index']) + 1] ?>&index=<?= intval($_GET['index']) + 1 ?>">Next -&gt;</a>
<?php 
	}
?>
			</div>
		</div>
		</center>
		<center>
		<div class="itemSection" id="StatusSectionDiv">
			<div id="ContactStatusTypeIDDiv">
				<label for="ContactStatusTypeID">Status:</label><br/>
				<select onchange="updateContactStatus(this.value);">
<?php
			$shown = false;
			foreach($shortStatuses as $stat=>$isShort)
			{
				if($isShort) {
					$shown = (array_search($lead->ContactStatus, $shortStatusDetails[$stat]) === false) ? $shown : true; 
?>
					<option 
							value="<?= $stat ?>"
							<?= array_search($lead->ContactStatus, $shortStatusDetails[$stat]) === false ? '' : 'SELECTED'?>>
						<?= $stat ?>
					</option>			
<?php
					
				}
				else {
					$shown = ($contactStatuseTypes[$stat]->ContactStatusTypeID == $lead->ContactStatusTypeID) ? true : $shown; 
?>
					<option 
							value="<?= $contactStatuseTypes[$stat]->ContactStatusTypeID ?>"
							<?= $contactStatuseTypes[$stat]->ContactStatusTypeID == $lead->ContactStatusTypeID ? 'SELECTED' : ''?>>
						<?= $stat ?>
					</option>			
<?php
				}
			}
			if(!$shown) {
?>
					<option value="<?= $lead->ContactStatusTypeID ?>" selected="selected">
						<?= $lead->ContactStatus ?>
					</option>			
<?php				
			}
?>	
				</select>
				<input type="hidden" name="ContactStatusTypeID" id="ContactStatusTypeID" value="<?= $lead->ContactStatusTypeID ?>"/>
				<span class="print_only"><?= $lead->ContactStatus ?></span>
			</div>
			<div id="LastContactEventTypeIDDiv">
				<label for="LastContactEventTypeID">Last Action:</label><br/>
				<span id="LastActionSpan"><?= $cetLookup[$lead->LastContactEvent->ContactEventTypeID] ? $cetLookup[$lead->LastContactEvent->ContactEventTypeID] : "No action taken." ?></span>&nbsp;
					<a href="#" onclick="wopen('add_action.php?id=<?= $lead->SubmissionListingID; ?>', 'add_action', 350, 350); return false;" class="no_print">New Action</a>
			</div>
		</div>
		</center>
		<div class="sectionHeader">
			Customer Info
			<a href="edit_lead_detail.php?id=<?= $lead->SubmissionListingID; ?>" class="no_print">(view details)</a> 
		</div>
		<div id="FileDiv" class="sectionDiv">
			<div id="CustomerInfoDiv">
				<div id="LeftSection">
				<div class="itemSection">
					<div id="ListingTypeDiv">
						<div style='display: inline; float: left;'>
							<label for="ListingType">Lead Source:</label><br/>
<?php
	if($slt->UserManageable == "1")
	{
		$slts = $portal->GetSubmissionListingTypes(0, 'SortOrder, SubmissionListingTypeID', 'all', 'UserManageable = 1 && submissionlistingtype.SubmissionListingSourceID = 9999');
?>
							<select name="ListingType" id="ListingType">
<?php
	foreach($slts as $slt)
	{
?>
								<option value="<?= $slt->SubmissionListingTypeID?>"
										<?= $slt->SubmissionListingTypeID == $lead->ListingType ? "SELECTED" : "" ?>>
									<?= $slt->ListingType ?>
								</option>
<?php
	}
?>
							</select>
							<span class="print_only"><?= $lead->ListingTypeName  ?></span>
						</div>
						<div id="OtherListingTypeDiv">
							<label for="OtherListingType">Campaign Code:</label><br/>
							<input type="text" id="OtherListingType" name="OtherListingType" style="width: 100px" value="<?= $lead->OtherListingType; ?>"/>
							<span class="print_only"><?= $lead->OtherListingType ?></span>
	<?php
		}
		else
		{
			echo $lead->ListingTypeName;		
		}
	?>
						</div>
					</div>
					<div id="NewUserIDDiv">
						<label for="NewUserID">Assigned To:</label><br/>
<?php
	if($isSubAdmin)
	{
?>
						<select name="NewUserID" id="NewUserID">
<?php
	foreach($users as $u)
	{
?>
							<option value="<?= $u->UserID ?>" <?= $u->UserID == $lead->UserID ? "SELECTED" : "" ?>>
								<?= $u->FirstName . " " . $u->LastName ?>
							</option>
<?php
	}
?>
						</select>
						<span class="print_only"><?= $lead->UserFullname ?></span>
<?php
	}
	elseif($lead->ContactStatusTypeID != 1)
	{
		echo $lead->UserFullname;		
	}
?>
					</div>
				</div>
				<div class="itemSection">
					<div id="FirstNameDiv">
						<label for="FirstName">Borrower First Name:</label><br/>
						<input type="text" id="FirstName" name="FirstName" style="width: 130px" value="<?= $lead->FirstName; ?>"/>
						<span class="print_only"><?= $lead->FirstName ?></span>
					</div>
					<div id="LastNameDiv">
						<label for="LastName">Last Name:</label><br/>
						<input type="text" id="LastName" name="LastName" style="width: 130px" value="<?= $lead->LastName; ?>"/>
						<span class="print_only"><?= $lead->LastName ?></span>
					</div>
					<div id="DOBDiv">
						<label for="DOB">Year Of Birth:</label>
						<input type="text" id="DOB" name="DOB" style="width: 125px" value="<?= (strtotime($lead->DOB) ? date("Y", strtotime($lead->DOB)) : ""); ?>" />
						<span class="print_only"><?= (strtotime($lead->DOB) ? date("Y", strtotime($lead->DOB)) : ""); ?></span>
					</div>
				</div>
				<div class="itemSection">
					<div id="FirstName2Div">
						<label for="FirstName">Co-Borrow First Name:</label><br/>
						<input type="text" id="FirstName2" name="FirstName2" style="width: 130px" value="<?= $lead->FirstName2; ?>"/>
						<span class="print_only"><?= $lead->FirstName2 ?></span>
					</div>
					<div id="LastName2Div">
						<label for="FirstName">Last Name:</label><br/>
						<input type="text" id="LastName2" name="LastName2" style="width: 130px" value="<?= $lead->LastName2; ?>"/>
						<span class="print_only"><?= $lead->LastName2 ?></span>
					</div>
					<div id="CoDOBDiv">
						<label for="CoDOB">Year Of Birth:</label>
						<input type="text" id="CoDOB" name="CoDOB" style="width: 125px" value="<?= (strtotime($lead->CoDOB) ? date("Y", strtotime($lead->CoDOB)) : ""); ?>" />
						<span class="print_only"><?= (strtotime($lead->CoDOB) ? date("Y", strtotime($lead->CoDOB)) : ""); ?></span>
					</div>
				</div>
				<div class="itemSection">
					<div id="Address1Div">
						<label for="Address1">Street Address:</label><br/>
						<input type="text" id="Address1" name="Address1" style="width: 415px" value="<?= $lead->Address1; ?>"/>
						<span class="print_only"><?= $lead->Address1 ?></span>
					</div>
				</div>
				<div class="itemSection">
					<div id="CityDiv">
						<label for="City">City:</label><br/>
						<input type="text" id="City" name="City" style="width: 250px" value="<?= $lead->City; ?>"/>
						<span class="print_only"><?= $lead->City ?></span>
					</div>
					<div id="StateDiv">
						<label for="State">ST:</label><br/>
						<input type="text" id="State" name="State" style="width: 35px" value="<?= $lead->State; ?>"/>
						<span class="print_only"><?= $lead->State ?></span>
					</div>
					<div id="ZipDiv">
						<label for="Zip">Zip:</label><br/>
						<input type="text" id="Zip" name="Zip" style="width: 100px" value="<?= $lead->Zip; ?>"/>
						<span class="print_only"><?= $lead->Zip ?></span>
					</div>
				</div>
				<div class="itemSection">
					<div id="ContactTimeDiv">
						<label for="ContactTime">Best Contact Time:</label><br/>
						<input type="text" id="ContactTime" name="ContactTime" style="width: 415px" value="<?= $lead->ContactTime; ?>"/>
						<span class="print_only"><?= $lead->ContactTime ?></span>
					</div>
				</div>
				<div class="itemSection">
					<div id="PhoneDiv">
						<label for="Phone">
							Home Phone: 
							(<a href="#" onclick="wopen('trigger_call.php?id=<?= $lead->SubmissionListingID; ?>&field=Phone', 'add_action', 350, 285); return false;">call</a>)
						</label><br/>
						<input type="text" id="Phone" name="Phone" style="width: 120px" value="<?= $lead->Phone; ?>"/>
						<span class="print_only"><?= $lead->Phone ?></span>
					</div>
					<div id="Phone2Div">
						<label for="Phone2">
							Cell Phone: 
							(<a href="#" onclick="wopen('trigger_call.php?id=<?= $lead->SubmissionListingID; ?>&field=Phone2', 'add_action', 350, 285); return false;">call</a>)
						</label><br/>
						<input type="text" id="Phone2" name="Phone2" style="width: 120px" value="<?= $lead->Phone2; ?>"/>
						<span class="print_only"><?= $lead->Phone2 ?></span>
					</div>
					<div id="FaxDiv">
						<label for="Fax">Fax:</label><br/>
						<input type="text" id="Fax" name="Fax" style="width: 120px" value="<?= $lead->Fax; ?>"/>
						<span class="print_only"><?= $lead->Fax ?></span>
					</div>
				</div>
				<div class="itemSection">
					<div id="EmailDiv">
						<label for="Email">Email Address:</label><br/>
						<input type="text" id="Email" name="Email" style="width: 350px" value="<?= $lead->Email; ?>"/>
						<span class="print_only"><?= $lead->Email ?></span>
					</div>
				</div>
				</div>
				<div id="RightTopSection">
					<div class="itemSection">
						<div id="LoanTypeDiv">
							<label for="LoanType">Loan Type Request:</label><br/>
							<input type="text" id="LoanType" name="LoanType" style="width: 180px" value="<?= $lead->LoanType ? $lead->LoanType : ""; ?>"/>
							<span class="print_only"><?= $lead->LoanType ? $lead->LoanType : ""; ?></span>
						</div>
					</div>
					<div class="itemSection">	
						<div id="LoanAmountDiv">
							<label for="LoanAmount">Loan Amount:</label><br/>
							<input type="text" id="LoanAmount" name="LoanAmount" style="width: 120px" value="<?= $lead->LoanAmount ? $lead->LoanAmount : ""; ?>"/>
							<span class="print_only"><?= $lead->LoanAmount ? $lead->LoanAmount : ""; ?></span>
						</div>
						<div id="Mortgage1RateDiv">
							<label for="Mortgage1Rate">Current Rate:</label><br/>
							<input type="text" id="Mortgage1Rate" name="Mortgage1Rate" style="width: 120px" value="<?= $lead->Mortgage1Rate ? $lead->Mortgage1Rate : ""; ?>"/>
							<span class="print_only"><?= $lead->Mortgage1Rate ? $lead->Mortgage1Rate : ""; ?></span>
						</div>
					</div>
					<div class="itemSection">	
						<div id="MarketValueDiv">
							<label for="MarketValue">Property Val:</label><br/>
							<input type="text" id="MarketValue" name="MarketValue" style="width: 75px" value="<?= $lead->MarketValue ? $lead->MarketValue : ""; ?>"/>
							<span class="print_only"><?= $lead->MarketValue ? $lead->MarketValue : ""; ?></span>
						</div>	
						<div id="NewHomeValueDiv">
							<label for="NewHomeValue">New Purch Val:</label><br/>
							<input type="text" id="NewHomeValue" name="NewHomeValue" style="width: 85px" value="<?= $lead->NewHomeValue ? $lead->NewHomeValue : ""; ?>"/>
							<span class="print_only"><?= $lead->NewHomeValue ? $lead->NewHomeValue : ""; ?></span>
						</div>	
						<div id="DownPaymentDiv">
							<label for="DownPayment">Down Payment:</label><br/>
							<input type="text" id="DownPayment" name="DownPayment" style="width: 85px" value="<?= $lead->DownPayment ? $lead->DownPayment : ""; ?>"/>
							<span class="print_only"><?= $lead->DownPayment ? $lead->DownPayment : ""; ?></span>
						</div>
					</div>
					<div class="itemSection">	
						<div id="CashOutDiv">
							<label for="CashOut">Cashout:</label><br/>
							<input type="text" id="CashOut" name="CashOut" style="width: 75px" value="<?= $lead->CashOut ? $lead->CashOut : ""; ?>"/>
							<span class="print_only"><?= $lead->CashOut ? $lead->CashOut : ""; ?></span>
						</div>
						<div id="Mortgage1BalanceDiv">
							<label for="Mortgage1Balance">Exist Loan Amt:</label><br/>
							<input type="text" id="Mortgage1Balance" name="Mortgage1Balance" style="width: 85px" value="<?= $lead->Mortgage1Balance ? $lead->Mortgage1Balance : ""; ?>"/>
							<span class="print_only"><?= $lead->Mortgage1Balance ? $lead->Mortgage1Balance : ""; ?></span>
						</div>
						<div id="Mortgage2BalanceDiv">
							<label for="Mortgage2Balance">Exist 2nd Amt:</label><br/>
							<input type="text" id="Mortgage2Balance" name="Mortgage2Balance" style="width: 85px" value="<?= $lead->Mortgage2Balance ? $lead->Mortgage2Balance : ""; ?>"/>
							<span class="print_only"><?= $lead->Mortgage2Balance ? $lead->Mortgage2Balance : ""; ?></span>
						</div>
					</div>
					<div class="itemSection">	
						<div id="RateTypeDiv">
							<label for="RateType">Desired Rate Type:</label><br/>
							<input type="text" id="RateType" name="RateType" style="width: 120px" value="<?= $lead->RateType; ?>"/>
							<span class="print_only"><?= $lead->RateType; ?></span>
						</div>
						<div id="Mortgage1TermDiv">
							<label for="Mortgage1Term">Current Rate Type:</label><br/>
							<input type="text" id="Mortgage1Term" name="Mortgage1Term" style="width: 120px" value="<?= $lead->Mortgage1Term; ?>"/>
							<span class="print_only"><?= $lead->Mortgage1Term; ?></span>
						</div>
					</div>
				</div>
				<div id="RightBottomSection">
					<div class="itemSection">
						<div id="FollowUpDiv">
							<label for="FollowUp">Schedule<br/>Follow up</label><br/>
							<a href="#" 
								onclick="wopen('add_appointment.php?id=<?= $lead->SubmissionListingID; ?>', 'add_appointment', 350, 435); return false;"
								title="<?= $appt ? "Edit" : "Add" ?> Reminder"><img src="images/appt_icon_big<?= $appt ? "_set" : "" ?>.png" border="0" /></a>
						</div>
						<div id="LoanCompDiv" onclick="document.getElementById('LoanCompForm').submit(); return false;">
							Refi<br/> Proposal
						</div>
						<div id="CreateOppDiv">
							<label for="CreateOpp">Create<br/>Opportunity</label><br/>
							<a href="#" 
								onclick="wopen('edit_opportunity.php?id=0&contactid=<?= $lead->SubmissionListingID; ?>', 'edit_opportunity', 350, 435); return false;"
								title="Create Opportunity"><img src="images/opp_icon.png" border="0" /></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	<div class="sectionHeader">
		Notes
	</div>
	<div id="CommentsDiv" class="sectionDiv">
		<textarea id="Comments" name="Comments" class="no_print"><?= $lead->Comments ?></textarea>
		<span class="print_only"><?= $lead->Comments; ?></span>
	</div>
	<div class="subSectionHeader breakpage">
		History
	</div>
	<div id="HistoryDiv" class="sectionDiv">
		<div id="EventTypeDiv">
			<table class="event_table" >
				<tr class="event_table rowheader" >
					<th class="event_table">
						Event Type
					</th>
					<th class="event_table">
						Event Time
					</th>
					<th class="event_table">
						Notes
					</th>
					<th class="event_table">
						User
					</th>
				</tr>
<?php
	$i = 0;
	foreach($allEvents as $event)
	{
		$i++;
?>
				<tr class="event_table<?= $i%2==0 ? ' roweven' : ' rowodd' ?>">
					<td class="event_table">
						<?= $event->EventDescription ?>
					</td>
					<td class="event_table">
						<?= strtotime($event->EventDate) ? date("m/d/Y g:i:s a", strtotime($event->EventDate)) : ""; ?>
					</td>
					<td class="event_table">
						<?= $event->EventNotes ?>
					</td>
					<td class="event_table">
						<?= $event->UserFullname ?>
					</td>
				</tr>
<?php	
	}
?>
			</table>
		</div>
	</div>
	<div class="subSectionHeader breakpage">
		Opportunities
	</div>
	<div id="OpportunityDiv" class="sectionDiv">
		<table class="event_table" >
			<tr class="event_table rowheader" >
				<th class="event_table">
					Stage
				</th>
				<th class="event_table">
					Type
				</th>
				<th class="event_table">
					Value
				</th>
				<th class="event_table">
					Create Date
				</th>
				<th class="event_table">
					Est. Close Date
				</th>
				<th class="event_table">
					&nbsp;
				</th>
			</tr>
<?php
	if(sizeof($oppts)>0) {
		$i = 0;
		foreach($oppts as $oppt)
		{
			$i++;
?>
			<tr class="event_table<?= $i%2==0 ? ' roweven' : ' rowodd' ?>">
				<td class="event_table">
					<?= $oppt->OpportunityStageName ?>
				</td>
				<td class="event_table">
					<?= $oppt->OpportunityTypeName ?>
				</td>
				<td class="event_table">
					$ <?= number_format($oppt->Value); ?>
				</td>
				<td class="event_table">
					<?= strtotime($oppt->CreateDate) ? date("m/d/Y", strtotime($oppt->CreateDate)) : "" ?>
				</td>
				<td class="event_table">
					<?= strtotime($oppt->EstimatedCloseDate) ? date("m/d/Y", strtotime($oppt->EstimatedCloseDate)) : "" ?>
				</td>
				<td class="event_table">
					<a href="#"
						onclick="wopen('edit_opportunity.php?id=<?= $oppt->OpportunityID; ?>', 'edit_opportunity', 350, 435); return false;">edit</a>
				</td>
			</tr>
<?php	
		}
	}
	else {
?>
			<tr class="event_table rowodd">
				<td colspan="6" style="text-align: center;">
					No Opportunities have been defined for this lead
				</td>
			</tr>
<?php	
	}
?>
		</table>
	</div>
	<center>
		<div class="itemSection">
			<div class="buttonSection">
				<input type="submit" value="Update" name="Submit"/>&nbsp;&nbsp;
				<input type="submit" value="Update & Close" name="Submit"/>&nbsp;&nbsp;
<?php 
	if($_GET['popup'] == 'true')
	{
	}
	else
	{ 
		if($isSubAdmin)
		{
?>
				<input type="submit" value="Delete" name="Submit" onclick="return confirm('Are you sure you want to delete this lead?')"/>&nbsp;&nbsp;
<?php
		}
?>
				<input type="submit" value="Cancel" name="Submit"/>
<?php
	}
?>
			</div>
		</div>
	</center>
	</div>
</form>
</div>	
<form method="post" action="<?= $REAL_TAG_URL ?>" target="_blank" id="RealTagForm">
	<input type="hidden" name="name" value="<?= $lead->FirstName . " " . $lead->LastName ?>" />
	<input type="hidden" name="address" value="<?= $lead->Address1 ?>" />
	<input type="hidden" name="zip" value="<?= substr($lead->Zip, 0, 5) ?>" />
</form>
<form method="post" action="<?= $LOAN_COMP_URL ?>" target="_blank" id="LoanCompForm">
	<input type="hidden" name="custName" value="<?= $lead->FirstName . " " . $lead->LastName ?>" />
	<input type="hidden" name="custAddr" value="<?= $lead->Address1 ?>" />
	<input type="hidden" name="custCSZ" value="<?= "{$lead->City}, {$lead->State} {$lead->Zip}" ?>" />
	<input type="hidden" name="custEmail" value="<?= $lead->Email ?>" />
	
	
	<input type="hidden" name="loName" value="<?= $currentUser->FirstName . " " . $currentUser->LastName ?>" />
	<input type="hidden" name="loTollFreePhone" value="<?= $currentUser->ContactPhone ?>" />
	<input type="hidden" name="loDirectPhone" value="<?= $currentUser->ContactCell ?>" />
	<input type="hidden" name="loFax" value="<?= $currentUser->ContactFax ?>" />
	<input type="hidden" name="loEmail" value="<?= $currentUser->ContactEmail ?>" />
		
	<input type="hidden" name="fMortBal" value="<?= $lead->Mortgage1Balance ?>" />
	<input type="hidden" name="fPmt" value="<?= $lead->Mortgage1Payment ?>" />
	<input type="hidden" name="sMortBal" value="<?= $lead->Mortgage2Balance ?>" />
	<input type="hidden" name="sPmt" value="<?= $lead->Mortgage2Payment ?>" />
	<input type="hidden" name="pTaxBal" value="<?= $lead->PropertyTaxes ?>" />
	<input type="hidden" name="pTaxPmt" value="<?= number_format($lead->PropertyTaxes / 12, 2); ?>" />
	<input type="hidden" name="insBal" value="<?= $lead->AnnualInsurance; ?>" />
	<input type="hidden" name="insPmt" value="<?= number_format($lead->AnnualInsurance / 12, 2); ?>" />
	<input type="hidden" name="oLiabBal" value="<?= $lead->OtherDebt1Balance + $lead->OtherDebt2Balance + $lead->OtherDebt3Balance; ?>" />
	<input type="hidden" name="oLiabPmt" value="<?= $lead->OtherDebt1Payment + $lead->OtherDebt2Payment + $lead->OtherDebt3Payment; ?>" />
	
	<input type="hidden" name="cHomeVal" value="<?= $lead->MarketValue; ?>" />
	
	<input type="hidden" name="transferIn" value="1" />
</form>
<form method="post" action="<?= $PURCH_PROP_URL ?>" target="_blank" id="PurchPropForm">
	<input type="hidden" name="custName" value="<?= $lead->FirstName . " " . $lead->LastName ?>" />
	<input type="hidden" name="custAddr" value="<?= $lead->Address1 ?>" />
	<input type="hidden" name="custCSZ" value="<?= "{$lead->City}, {$lead->State} {$lead->Zip}" ?>" />
	<input type="hidden" name="custEmail" value="<?= $lead->Email ?>" />
	
	
	<input type="hidden" name="loName" value="<?= $currentUser->FirstName . " " . $currentUser->LastName ?>" />
	<input type="hidden" name="loTollFreePhone" value="<?= $currentUser->ContactPhone ?>" />
	<input type="hidden" name="loDirectPhone" value="<?= $currentUser->ContactCell ?>" />
	<input type="hidden" name="loFax" value="<?= $currentUser->ContactFax ?>" />
	<input type="hidden" name="loEmail" value="<?= $currentUser->ContactEmail ?>" />
		
	<input type="hidden" name="fMortBal" value="<?= $lead->Mortgage1Balance ?>" />
	<input type="hidden" name="fPmt" value="<?= $lead->Mortgage1Payment ?>" />
	<input type="hidden" name="sMortBal" value="<?= $lead->Mortgage2Balance ?>" />
	<input type="hidden" name="sPmt" value="<?= $lead->Mortgage2Payment ?>" />
	<input type="hidden" name="pTaxBal" value="<?= $lead->PropertyTaxes ?>" />
	<input type="hidden" name="pTaxPmt" value="<?= number_format($lead->PropertyTaxes / 12, 2); ?>" />
	<input type="hidden" name="insBal" value="<?= $lead->AnnualInsurance; ?>" />
	<input type="hidden" name="insPmt" value="<?= number_format($lead->AnnualInsurance / 12, 2); ?>" />
	<input type="hidden" name="oLiabBal" value="<?= $lead->OtherDebt1Balance + $lead->OtherDebt2Balance + $lead->OtherDebt3Balance; ?>" />
	<input type="hidden" name="oLiabPmt" value="<?= $lead->OtherDebt1Payment + $lead->OtherDebt2Payment + $lead->OtherDebt3Payment; ?>" />
	
	<input type="hidden" name="cHomeVal" value="<?= $lead->MarketValue; ?>" />
	
	<input type="hidden" name="transferIn" value="1" />
</form>
<div style="display: none;" class="detail_div" id="detailDiv">
	<div style="text-align: center; background: #CCC; padding: 3px 0px 3px 0px; font-weight: bold;">
		Please Choose a detailed description for<br/>
		<span id="shortStatus"></span>
	</div>
	<div style="width: 394px; text-align: center; padding: 25px 0px 15px 0px;">
		<select name="detailStatusID" id="detailStatusID">
		</select>
	</div>
	<div style="width: 394px; text-align: center; background: #FFF; padding-bottom: 15px;">
		<a href="#" onclick="submitDetailedContactStatus();">OK</a>
	</div>
</div>
</body>
</html>