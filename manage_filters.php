<?php
	require("../printable/include/mysql.inc.php");
	require("../printable/include/optimize.printable.inc.php");
	require("globals.php");
	
	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	
	$portal = new OptimizePortal($COMPANY_ID, $db);
	
	$currentUser = $portal->UserAccess($_SESSION['currentuserid']);
	$isSuper = $portal->CheckPriv($currentUser->UserID, 'supervisor');
			
	if(!$portal->CheckPriv($currentUser->UserID, 'subadmin')) {
		header( "Location: " . $portal->CurrentCompany->Website . "home.php?message=" . urlencode( "Accessed Denied." ) );
		die();
	}	
	
	if(!isset($_GET['groupid']) || $_GET['groupid'] == 0) {
		$gid = 'all';
	} else {
		$gid = $_GET['groupid']; 
	}
	
	// Select all filters for this company
	$filters = $portal->GetFilters($gid);
	
	$gs = $portal->GetCompanyGroups();

	$groups = array();
	
	$groups[0] = new Group();
	$groups[0]->GroupID = 0;
	$groups[0]->GroupName = "All";
	
	foreach($gs as $g)
	{
		$groups[$g->GroupID] = $g;
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	<title>
        <?= $portal->CurrentCompany->CompanyName ?> :: Manage Filters
    </title>
    <meta http-equiv="content-type" content="text/html;charset=utf-8"/>

    <?php include("components/bootstrap.php") ?>

    <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico"/>

    <script src="js/func.js"></script>
</head>
<body bgcolor="#FFFFFF">
<?php include("components/header.php") ?>
<div id="body">
	 <?php
    $CURRENT_PAGE = "Home";
    include("components/navbar.php");
    ?>
    <?php if (isset($_GET['message'])): ?>
        <div class="container">
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?= $_GET['message']; ?>
            </div>
        </div>
    <?php endif; ?>

    <div id="controlPanelContainer" class="row">
        <div id="folderTreeContainer" class="col-md-offset-2 col-md-2 panel panel-default">
            <?php include("components/controlpanel_tree.php"); ?>
        </div>

        <div class="sectionHeader col-md-7">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Control Panel - Manage Filters</h3>
                </div>
                <div class="panel-body">
                    <div id="detailContainer">
                        <div class="row col-md-4 panel">
                        	 <form method="get" action="<?= $_SERVER['PHP_SELF']; ?>"> 
										<input type="hidden" name="filter" value="1" />
										<label for="groupid">Filter:</label>
										<select name="groupid" onchange="this.form.submit();">
										<?php
											foreach($groups as $g)
											{
										?>
										<option value="<?= $g->GroupID ?>" <?= $g->GroupID == $_GET['groupid'] ? 'SELECTED' : '' ?>><?= $g->GroupName ?></option>
										<?php	} ?>
										</select>
									</form>
								</div>
								<div id="actionBar" class="row">
								<br/>
                           <a href="add_filter.php" class="btn btn-default btn-sm actionButtonRight">Add Filter</a>
                        </div>
                        <div class="table-responsive row">
                            <table width="100%" class="message_table table table-striped table-hover table-bordered">
                                <thead>
                                <tr class="filter_table rowheader" id="HeaderRow">
                                    <th class="filter_table">ID</th>
                                    <th class="filter_table">Filter Name</th>
                                    <th class="filter_table">Group</th>
                                    <th class="filter_table">Edit</th>
                                    <th class="filter_table">Delete</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                 $filter = new Filter();
											foreach($filters as $filter)
											{
                                        ?>
                                        <tr class="filter_table<?= $i % 2 == 0 ? ' roweven' : ' rowodd' ?>">
                                            <td class="filter_table" align="right">
                                                <?= $filter->FilterID ?>
                                            </td>
                                            <td class="filter_table">
                                                <?= $filter->FilterName ?>
                                            </td>
                                            <td class="filter_table">
                                                <?= $groups[$filter->GroupID]->GroupName ?>
                                            </td>
                                            <td class="filter_table">
                                                <a class="btn btn-default btn-sm actionButtonRight" href="edit_filter.php?filterid=<?= $filter->FilterID ?>">Edit</a>
                                            </td>
                                            <td class="filter_table">
                                                <a class="btn btn-default btn-sm actionButtonRight" href="edit_filter.php?filterid=<?= $filter->FilterID ?>&action=delete" onclick="return confirm('Are you sure you want to delete this filter?\nThis cannot be undone.');">Delete</a>
                                            </td> 
                                        </tr>
                                    <?php
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
           </div>
       </div>
   </div>
</div>
<?php include("components/footer.php") ?>
</body>
</html>