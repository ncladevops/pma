<?php
	require("../../printable/include/mysql.inc.php");
	require("../../printable/include/optimize.printable.inc.php");
	require("../globals.php");
	
//	error_reporting(0);
	
	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	
	$portal = new OptimizePortal($COMPANY_ID, $db);
	
	// Loop Email Reminders
	$reminders = $portal->GetAllOpenNotifies('email');
	foreach($reminders as $appt)
	{
		$portal->EmailReminder($appt);
	}