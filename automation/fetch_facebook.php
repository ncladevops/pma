<?php
	require("../../printable/include/mysql.inc.php");
	require("../../printable/include/optimize.printable.inc.php");
	require("../globals.php");
	require '../../printable/include/component/facebook/facebook.php';
	
	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	
	$portal = new OptimizePortal($COMPANY_ID, $db);
	
	$errors = array();
	$timeFrame = "-90 days";
	
	$us = $portal->GetCompanyUsers();
	$users = array();
	foreach($us as $u) {
		$users[$u->UserID] = $u;
	}
	
	
	$ps = $portal->GetCompanyPostingTransactions(date("Y-m-d", strtotime($timeFrame)), null, 'facebook');	
	$postings = array();
	$fetchArray = array();
	foreach ($ps as $p) {
		list($fbID, $pID) = explode("_", $p->ExternalID);
		$fetchArray[$p->UserID] = $fbID;
		$postings[$p->ExternalID] = $p;
	}
	
	foreach($fetchArray as $userID=>$facebookID) {
		
		$facebook = new Facebook(array(
		  'appId'  => $FACEBOOK_APPID,
		  'secret' => $FACEBOOK_SECRET,
		));	
		
		try {
			$query = "/fql?q=" .
				urlencode('SELECT post_id, app_id, comment_info, like_info, share_count, created_time FROM stream WHERE source_id = "' . $facebookID . '" AND app_id = "' . $FACEBOOK_APPID . '" AND created_time > "' . strtotime($timeFrame) . '" LIMIT 1000') .
				"&access_token=" . $users[$userID]->FacebookToken;
				
			$ret_obj = $facebook->api($query, 'GET');
			
			foreach($ret_obj['data'] as $d) {
				$p = $postings[$d["post_id"]];
				$p->CommentCount = $d["comment_info"]["comment_count"];
				$p->LikeCount = $d["like_info"]["like_count"];
				$p->ShareCount = $d["share_count"];
								
				$portal->UpdatePostTransaction($p);
			}
		}
		catch(FacebookApiException $e) {
			$errors[] = print_r($e, true);
		}
		
	}
	
	echo "Errors: ";
	echo "<xmp>";
	var_dump($errors);
	echo "</xmp>";