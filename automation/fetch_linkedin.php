<?php
	require("../../printable/include/mysql.inc.php");
	require("../../printable/include/optimize.printable.inc.php");
	require("../globals.php");
	require '../../printable/include/component/linkedin/linkedin.php';
	
	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	
	$portal = new OptimizePortal($COMPANY_ID, $db);
		
	$errors = array();
	
	$timeFrame = "-90 days";
	
	$us = $portal->GetCompanyUsers();
	$users = array();
	foreach($us as $u) {
		$users[$u->UserID] = $u;
	}
	
	
	$ps = $portal->GetCompanyPostingTransactions(date("Y-m-d", strtotime($timeFrame)), null, 'linkedin');	
	$postings = array();
	$fetchArray = array();
	foreach ($ps as $p) {
		list($fbID, $pID) = explode("_", $p->ExternalID);
		$fetchArray[$p->UserID] = $users[$p->UserID];
		$postings[$p->ExternalID] = $p;
	}
	
	echo "<xmp>";
	var_dump($postings);
	echo "</xmp>";
		
	foreach($fetchArray as $userID=>$user) {
		
		$oauth_callback = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
		$linkedin = new LinkedIn($LINKEDIN_KEY, $LINKEDIN_SECRET, $oauth_callback );
		
		list($t,$s) = explode(":",$user->LinkedinToken);
		$linkedin->access_token = new OAuthConsumer($t, $s, 1);
		
		$shares = $response = new SimpleXMLElement($linkedin->getShares("-90 days"));
				
		if($response->status != '401') {
			foreach($shares->update as $update) {
				echo "Key: " . $update->{'update-key'} . "<br/>";
				echo "Comments: " . $update->{'update-comments'}['total'] . "<br/>";
				echo "Likes: " . $update->{'num-likes'} . "<br/>*******<br/>";	
				
				$p = $postings[(string)$update->{'update-key'}];
				echo "<xmp>";
				var_dump($p);
				echo "</xmp>";
				
				$p->CommentCount = $update->{'update-comments'}['total'];
				$p->LikeCount = $update->{'num-likes'};
				
				echo "<xmp>";
				var_dump($p);
				echo "</xmp>";
								
				$portal->UpdatePostTransaction($p);
			}
		}
		else {
			$errors[] = $response->message;
		}
		
	}
	
	echo "Errors: ";
	echo "<xmp>";
	var_dump($errors);
	echo "</xmp>";
	