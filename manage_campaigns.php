<?php
require("../printable/include/mysql.inc.php");
require("../printable/include/optimize.printable.inc.php");
require("globals.php");

$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);

$portal = new OptimizePortal($COMPANY_ID, $db);

$currentUser = $portal->GetUser($_SESSION['currentuserid']);

if (!$portal->CheckPriv($currentUser->UserID, 'subadmin')) {
    header("Location: " . $portal->CurrentCompany->Website . "home.php?message=" . urlencode("Accessed Denied."));
    die();
}

$sortCols = array('ID' => 'SubmissionListingTypeID', 'Campaign Name' => 'ListingType', 'Report Name' => 'ListingDescription', 'OutsideID' => 'OutsideID', 'Lead Source' => 'ListingSourceName',
    'Group' => 'GroupID',
    'Extension' => 'nosort', 'Edit/<br>View' => 'nosort', 'Enable/<br>Disable' => 'nosort'
);

if (array_search($_GET['sortby'], $sortCols) === false) {
    $_GET['sortby'] = "SubmissionListingTypeID";
}

//WHAT'S THE POINT!?? ~brandin
/*if($portal->CheckPriv($currentUser->UserID, 'admin')) {
    $campaigns = $portal->GetSubmissionListingTypes('0', $_GET['sortby'], 'all', 'UserManageable = 0 || submissionlistingtype.SubmissionListingSourceID = 9999');
} else {
    $campaigns = $portal->GetSubmissionListingTypes('0', $_GET['sortby'], 'all', 'UserManageable = 0 || submissionlistingtype.SubmissionListingSourceID = 9999');
}*/
$campaigns = $portal->GetSubmissionListingTypes('all', $_GET['sortby'], 'all', '1');

$gs = $portal->GetCompanyGroups();
$groups = array();
foreach ($gs as $g) {
    $groups[$g->GroupID] = $g;
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title>
        <?= $portal->CurrentCompany->CompanyName ?> :: Manage Campaigns
    </title>
    <meta http-equiv="content-type" content="text/html;charset=utf-8"/>

    <?php include("components/bootstrap.php") ?>

    <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico"/>

    <script src="js/func.js"></script>
</head>
<body bgcolor="#FFFFFF">
<?php include("components/header.php") ?>
<div id="body">
    <?php
    $CURRENT_PAGE = "Home";
    include("components/navbar.php");
    ?>
    <?php if (isset($_GET['message'])): ?>
        <div class="container">
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?= $_GET['message']; ?>
            </div>
        </div>
    <?php endif; ?>

    <div id="controlPanelContainer" class="row">
        <div id="folderTreeContainer" class="col-md-offset-2 col-md-2 panel panel-default">
            <?php include("components/controlpanel_tree.php"); ?>
        </div>

        <div class="sectionHeader col-md-7">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Control Panel - Campaigns</h3>
                </div>
                <div class="panel-body">
                    <div id="detailContainer">
                        <div id="actionBar" class="row">
                            <a href="add_campaign.php" class="btn btn-default btn-sm actionButtonRight">Add Campaign</a>
                        </div>
                        <div class="table-responsive row">
                            <table width="100%" class="message_table table table-striped table-hover table-bordered">
                                <thead>

                                <tr class="campaign_table rowheader" id="HeaderRow">
                                    <?php
                                    foreach ($sortCols as $k => $v) {
                                        if ($v != "nosort" && $_GET['sortby'] != $v) {
                                            $getTemp = $_GET;
                                            $getTemp['sortby'] = $v;
                                            $getString = build_get_query($getTemp);
                                            ?>
                                            <th class="campaign_table">
                                                <a href="<?= $_SERVER['PHP_SELF'] . "?" . $getString ?>">
                                                    <?= $k ?>
                                                </a>
                                            </th>
                                            <?php
                                        } else {
                                            ?>
                                            <th class="campaign_table"><?= $k ?></th>
                                            <?php
                                        }
                                    }
                                    ?>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if (is_array($campaigns)) {
                                    $i = 0;
                                    foreach ($campaigns as $c) {
                                        $i++;
                                        if (sizeof($c->Extensions) > 0)
                                            $nextRing = $c->Extensions[$c->LeadCounter % sizeof($c->Extensions)];
                                        else
                                            $nextRing = "**NONE**";
                                        ?>
                                        <tr class="campaign_table<?= $i % 2 == 0 ? ' roweven' : ' rowodd' ?>">
                                            <td class="campaign_table" align="right">
                                                <?= $c->SubmissionListingTypeID ?>
                                            </td>
                                            <td class="campaign_table">
                                                <?= $c->ListingType ?>
                                            </td>
                                            <td class="campaign_table">
                                                <?= $c->ListingDescription ?>
                                            </td>
                                            <td class="campaign_table">
                                                <?= $c->OutsideID ?>
                                            </td>
                                            <td class="campaign_table">
                                                <?php
                                                if ($c->UserManageable) {
                                                    ?>
                                                    <a href="#"
                                                       onclick="wopen('uploadcontacts.php?listid=<?= $c->SubmissionListingTypeID ?>', 'upload_lead', 840, 450); return false;">
                                                        upload leads
                                                    </a>
                                                    <?php
                                                } else {
                                                    echo $c->ListingSourceName;
                                                }
                                                ?>
                                            </td>
                                            <td class="campaign_table">
                                                <?= isset($groups[$c->GroupID]) ? $groups[$c->GroupID]->GroupName : "All" ?>
                                            </td>
                                            <td class="campaign_table">
                                                <?= $nextRing ?>
                                            </td>
                                            <td class="campaign_table" align="right">
                                                <a href="edit_campaign.php?id=<?= $c->SubmissionListingTypeID ?>">
                                                    edit
                                                </a>
                                            </td>
                                            <td class="campaign_table" align="right">
                                                <?php
                                                if ($c->Disabled == 1) { ?>
                                                    <a class="label label-danger"
                                                       href="edit_campaign.php?disable=true&id=<?= $c->SubmissionListingTypeID ?>">Disable</a>
                                                    <?php
                                                } else { ?>
                                                    <a class="label label-success"
                                                       href="edit_campaign.php?enable=true&id=<?= $c->SubmissionListingTypeID ?>">Enable</a>
                                                    <?php
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include("components/footer.php") ?>
</body>
</html>