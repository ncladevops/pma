<?php
require("../printable/include/mysql.inc.php");
require("../printable/include/optimize.printable.inc.php");
require("globals.php");
include("components/class.myatomparser.php");
require '../printable/include/component/twitter/codebird.php';

$MAX_DASHBOARDS = 5;

$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);

$portal = new OptimizePortal($COMPANY_ID, $db);

$currentUser = $portal->UserAccess($_SESSION['currentuserid']);
$isSuper = $portal->CheckPriv($currentUser->UserID, 'supervisor');

if (!$currentUser) {
    header("Location: login.php?message=" . urlencode("Not logged in or login error."));
    die();
}

$dashboard = $portal->GetDashboard($HOMEPAGE_DBID);
$reports = $portal->GetDashBoardReports($dashboard->DashboardID);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <title>
            <?= $portal->CurrentCompany->CompanyName ?> :: Welcome
        </title>
        <meta http-equiv="content-type" content="text/html;charset=utf-8" />
        <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />		

        <link rel="stylesheet" href="slider/default.css" type="text/css" media="screen" />
        <script  src="js/swfobject.js"></script>	

        <script  src="js/func.js"></script>	

        <script type='text/javascript' src='http://yui.yahooapis.com/2.9.0/build/utilities/utilities.js'></script>
        <link rel="stylesheet" href="../printable/include/js/jquery-ui/smoothness/jquery-ui.min.css" />
        <script type='text/javascript' src='../printable/include/js/jquery-1.10.2.min.js'></script>
        <script type='text/javascript' src='../printable/include/js/jquery-ui.js'></script>

        <?php include("reports/report_dashboard_includes.php") ?>

        <script type='text/javascript' src='../printable/include/js/jquery-1.10.2.min.js'></script>
        <script type='text/javascript' src='../printable/include/js/jquery-ui.js'></script>
        <link type="text/css" media="all" rel="stylesheet" href="css/home2.css?v=2" />
        <script type="text/javascript">

            var dashboard_transactionURL = "dashboard_trans.php";
            var appointmentTableRequest;
            var appointmentTableSimpleRequest;

            var report_loaderURL = "reports/load_report.php";
            var reportRequest = new Array();

            function LoadDashboards()
            {
                LoadAppointmentTableSimple();
                LoadCalendar();
            }

            function LoadCalendar()
            {
                document.getElementById("CalendarTable_Dashboard").innerHTML = "<iframe src='appt_calendar_sim.php' style='border: 0px; width: 100%; height: 300px;' scrolling='no'></iframe>";
            }

            function LoadAppointmentTableSimple()
            {
                document.getElementById("AppointmentTableSimple_Dashboard").innerHTML = "<img src='images/loading.gif' />"

                appointmentTableSimpleRequest = GetXmlHttpObject();
                if (appointmentTableSimpleRequest == null)
                {
                    alert("Your browser does not support AJAX!");
                    return;
                }
                var url = dashboard_transactionURL;
                url = url + "?dashboard=AppointmentTableSimp";
                url = url + "&sid=" + Math.random();
                appointmentTableSimpleRequest.onreadystatechange = appointmentTableSimpleRequestStateChanged;
                appointmentTableSimpleRequest.open("GET", url, true);
                appointmentTableSimpleRequest.send(null);
            }

            function appointmentTableSimpleRequestStateChanged()
            {
                if (appointmentTableSimpleRequest.readyState == 4)
                {
                    document.getElementById("AppointmentTableSimple_Dashboard").innerHTML = appointmentTableSimpleRequest.responseText;
                }
            }


        </script>

        <?php include("components/bootstrap.php") ?>



    </head>
    <body bgcolor="#FFFFFF" onload="LoadDashboards();">
        <?php include("components/header.php") ?>
        <?php
        $CURRENT_PAGE = "Home";
        include("components/navbar.php");
        ?>


        <?php if (isset($_GET['message'])): ?>
            <div class="container">
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?= $_GET['message']; ?>
                </div>
            </div>
        <?php endif; ?>

        <div id="page_content" class="row">

            <div id="left_panel" class="col-md-3">

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-share"></i> Quick Links</h3>
                    </div>
                    <div class="panel-body">
                        <ul class="nav nav-pills nav-justified">
                            <li><?= $portal->GetCustomMessage('QuickLink1', false, $currentUser->GroupID)->Message; ?></li>
                            <li><?= $portal->GetCustomMessage('QuickLink2', false, $currentUser->GroupID)->Message; ?></li>
                            <li><?= $portal->GetCustomMessage('QuickLink3', false, $currentUser->GroupID)->Message; ?></li>
                        </ul>


                        <ul class="nav nav-pills nav-justified">
                            <li><?= $portal->GetCustomMessage('QuickLink4', false, $currentUser->GroupID)->Message; ?></li>
                            <li><?= $portal->GetCustomMessage('QuickLink5', false, $currentUser->GroupID)->Message; ?></li>
                            <li><?= $portal->GetCustomMessage('QuickLink6', false, $currentUser->GroupID)->Message; ?></li>
                        </ul>

                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><?= $portal->GetCustomMessage('MessageBoardHeader', false, $currentUser->GroupID)->Message; ?>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <?php
                        //$url = "http://$currentUser->Username:optimize@{$BB_LINK}feed.php?auth=http";
                        //$atom_parser = new myAtomParser($url);
                        //$output = $atom_parser->getOutput();	# returns string containing HTML
                        echo $output;
                        ?>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><?= $portal->GetCustomMessage('BrandNewsHeader', false, $currentUser->GroupID)->Message; ?>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <?php
//Codebird::setConsumerKey($TWITTER_KEY, $TWITTER_SECRET);
//Codebird::setBearerToken($TWITTER_BEARER);
//$cb = Codebird::getInstance();
//echo $cb->display_searched_tweets('OptifiNow');
                        ?>
                    </div>

                </div>
            </div>


            <div id="main_content" class="col-md-6">
                <!-- Carousel
                    ================================================== -->
                <div id="myCarousel" class="carousel slide hidden-xs">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
                        <li data-target="#myCarousel" data-slide-to="3"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="item active">
                            <img src="slider/images/slide-1.png" data-src="" alt="First slide">
                                <div class="container">
                                    <div class="carousel-caption">
                                    </div>
                                </div>
                        </div>
                        <div class="item">
                            <img src="slider/images/slide-2.png" data-src="" alt="First slide">
                                <div class="container">
                                    <div class="carousel-caption">
                                    </div>
                                </div>
                        </div>
                        <div class="item">
                            <img src="slider/images/slide-3.png" data-src="" alt="First slide">
                                <div class="container">
                                    <div class="carousel-caption">
                                    </div>
                                </div>
                        </div>
                        <div class="item">
                            <img src="slider/images/slide-4.png" data-src="" alt="First slide">
                                <div class="container">
                                    <div class="carousel-caption">
                                    </div>
                                </div>
                        </div>
                    </div>
                    <a class="left carousel-control" href="#myCarousel" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
                    <a class="right carousel-control" href="#myCarousel" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
                </div><!-- /.carousel -->
            </div>
            <div id="right_panel" class="col-md-3">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <i class="glyphicon glyphicon-time"></i> My Appointments
                        </h3>

                    </div>
                    <div id="AppointmentTableSimple_Dashboard" class="panel-body">
                        <img class="alt-spinner" src="images/loading.gif" />
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">

                        <h3 class="panel-title">
                            <i class="glyphicon glyphicon-calendar"></i> My Calendar
                            <div "tomorrowCalendarDiv" class="calendarLinkDiv" onclick="wopen('appt_calendar.php?defaultView=agendaMonth', 'appt_calendar', 800, 600);">
                                <img src="images/glyphicons_month.png" height="18" width="18" title="Month"/>
                            </div>
                            <div "weekCalendarDiv" class="calendarLinkDiv" onclick="wopen('appt_calendar.php?defaultView=agendaWeek', 'appt_calendar', 800, 600);">
                                <img src="images/glyphicons_week.png" height="18" width="18" title="Week"/>
                            </div>
                            <div "todayCalendarDiv" class="calendarLinkDiv" onclick="wopen('appt_calendar.php?defaultView=agendaDay', 'appt_calendar', 800, 600);">
                                <img src="images/glyphicons_day.png" height="18" width="18" title="Day"/>
                            </div>
                        </h3>
                    </div>
                    <div id="CalendarTable_Dashboard" class="panel-body">
                        <img class="alt-spinner" src="images/loading.gif" />
                    </div>
                </div>
            </div>	
        </div>

        <div class="row">
            <div class="container col-md-2">
                <select class="form-control" onchange="window.location = 'home2.php';">
                    <option SELECTED>Legacy Version</option>
                    <option>New Version</option>
                </select>
            </div>
        </div>

        <?php include("components/footer.php") ?>


    </body>
</html>