<?php
	require("../printable/include/mysql.inc.php");
	require("../printable/include/optimize.printable.inc.php");
	require("globals.php");
	include("components/class.myatomparser.php");
	require '../printable/include/component/twitter/codebird.php';
	
	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	
	$portal = new OptimizePortal($COMPANY_ID, $db);
	
	$currentUser = $portal->UserAccess($_SESSION['currentuserid']);
	$isSuper = $portal->CheckPriv($currentUser->UserID, 'supervisor');
			
	if(!$currentUser)
	{
		header("Location: login.php?message=" .urlencode("Not logged in or login error."));
		die();
	}	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
	<title>
		Optimize on Demand :: Welcome
	</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />		
	<link type="text/css" media="all" rel="stylesheet" href="style.css" />
	<link type="text/css" media="all" rel="stylesheet" href="css/home.css" />
	<link type="text/css" media="all" rel="stylesheet" href="css/dashboard.css" />
    <link rel="stylesheet" href="slider/nivo-slider.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="slider/default.css" type="text/css" media="screen" />
	<script  src="js/func.js"></script>	
	<script  src="js/swfobject.js"></script>	
<script language="JavaScript" type="text/JavaScript">
	var dashboard_transactionURL = "dashboard_trans.php";
	var appointmentTableRequest;
	
	function LoadDashboards()
	{
		LoadAppointmentTable();
	}

	function LoadAppointmentTable()
	{
		document.getElementById("AppointmentTable_Dashboard").innerHTML = "<img src='images/loading.gif' />"
				
		appointmentTableRequest=GetXmlHttpObject();
		if (appointmentTableRequest==null)
		{
			alert ("Your browser does not support AJAX!");
			return;
		} 
		var url=dashboard_transactionURL;
		url=url+"?dashboard=AppointmentTable";
		url=url+"&sid="+Math.random();
		appointmentTableRequest.onreadystatechange=appointmentTableRequestStateChanged;
		appointmentTableRequest.open("GET",url,true);
		appointmentTableRequest.send(null);
	}

	function appointmentTableRequestStateChanged()
	{
		if (appointmentTableRequest.readyState==4)
		{
			document.getElementById("AppointmentTable_Dashboard").innerHTML = appointmentTableRequest.responseText;
		}
	}
    swfobject.embedSWF("https://121communication.com/public_images/OptifiNow/demo_animated.swf", "flashDiv", "750", "275", "9.0.0");
</script>
</head>
<body bgcolor="#FFFFFF" onload="LoadDashboards();">
<div id="page">
<?php include("components/header.php") ?>
<div id="body">
	<?php 
		$CURRENT_PAGE = "Home";
		include("components/navbar.php"); 
	?>
	<div class="error"><?= $_GET['message']; ?></div>
	<div id="home_container">
		<div id="side_panel">
			<div class="quicklinks">
				<div class="quicklinks_menu" style="padding: 2px 0px 2px 15px; font-weight:bold; ">
					QUICK LINKS
				</div>
				<div style="margin-left:2px;">
					<?= $portal->GetCustomMessage('QuickLink1', false, $currentUser->GroupID)->Message; ?>
					<?= $portal->GetCustomMessage('QuickLink2', false, $currentUser->GroupID)->Message; ?>
					<?= $portal->GetCustomMessage('QuickLink3', false, $currentUser->GroupID)->Message; ?>
					<?= $portal->GetCustomMessage('QuickLink4', false, $currentUser->GroupID)->Message; ?>
					<?= $portal->GetCustomMessage('QuickLink5', false, $currentUser->GroupID)->Message; ?>
				</div>
				<div class="quicklinks_msg" style="margin-top:20px;">
					<div style="font-weight:bold; margin-bottom:5px;">
						MESSAGE BOARD
					</div>
					<div class="quicklinks_msg_content" style="overflow: auto;">
<?php
	//$url = "http://$currentUser->Username:optimize@{$BB_LINK}feed.php?auth=http";
	//$atom_parser = new myAtomParser($url);

	//$output = $atom_parser->getOutput();	# returns string containing HTML
	//echo $output;
?>
					</div>
				</div>
				<div class="quicklinks_msg" style="margin-top:10px;">
					<div style="font-weight:bold; margin-bottom:5px;">
						TWITTER
					</div>
					<div class="quicklinks_msg_content" style="overflow: auto;" id="brandNewsDiv">
<?php
	\Codebird\Codebird::setConsumerKey($TWITTER_KEY, $TWITTER_SECRET);
	\Codebird\Codebird::setBearerToken($TWITTER_BEARER);
	
	$cb = \Codebird\Codebird::getInstance();
	
	echo $cb->display_searched_tweets('OptifiNow');
?>
					</div>
				</div>
			</div>
		</div>
		<div id="main_panel">
			<div id="home_panel_1" class="home_panel">
				<div style="width:500px;">
                        <div class="slider-wrapper theme-default" style="width:725px;">
                            <div id="slider" class="nivoSlider" style="width:725px; height:266px;">
                                <img src="slider/images/slide-1.jpg" width="725" height="266" />
                                <img src="slider/images/slide-2.jpg" width="725" height="266" />
                                <img src="slider/images/slide-3.jpg" width="725" height="266" />
                                <img src="slider/images/slide-4.jpg" width="725" height="266" />
                                <img src="slider/images/slide-5.jpg" width="725" height="266" />
                            </div>
                        </div>
</div>

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script type="text/javascript" src="slider/scripts/jquery.nivo.slider.js"></script>
    <script type="text/javascript">
    $(window).load(function() {
        $('#slider').nivoSlider({pauseTime: 8000, controlNav: false});
    });
    </script>
			</div>
			<div id="AppointmentTable_Container">
				<center>
					<div class="homeHeader">My Appointments</div>
					<div id="AppointmentTable_Dashboard">
					
					</div>
				</center>
			</div>
		</div>		
	</div>
</div>
</div>
</body>
</html>