<?php
	die();
	require("../../printable/include/mysql.inc.php");
	require("../../printable/include/optimize.printable.inc.php");
	require("../globals.php");
	require("demo_lookups.php");
	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	
	$portal = new OptimizePortal($COMPANY_ID, $db);
		
	// Get Demo Leads
	$sqlstring = "SELECT
						SubmissionListingID, DateAdded, UserID
					FROM
						submissionlisting
					WHERE
						Email = 'demo_lead@optifinow.com';";
	$lead_result = $portal->mysqlDB->Query($sqlstring);
	
	for($i = 0; $i < mysql_num_rows($lead_result); $i++) {
		list($leadID, $dateAdded, $userID) = mysql_fetch_array($lead_result);
		$changeTime = date("Y-m-d h:i:s", strtotime($dateAdded . " +" . rand(100, (60*60*24)) . "seconds"));
		$statusTypeID = $STATUSTYPE_LOOKUP[rand(0, sizeof($STATUSTYPE_LOOKUP) - 1)];
		
		$sqlstring = "UPDATE
							submissionlisting
						SET
							ContactStatusTypeID = '$statusTypeID',
							LastStatusChange = '$changeTime',
							LastTouched = '$changeTime'
						WHERE
							SubmissionListingID = '$leadID'
						LIMIT 1;";
		$portal->mysqlDB->Query($sqlstring);
						
		$sqlstring = "INSERT INTO
						changelog
							(`Table`, `Field`, `Key`, ChangeTime, NewValue,
							OldValue, UserID, HistoryReport)
						VALUES
							('submissionlisting',
							'ContactStatusTypeID',
							'$leadID',
							'$changeTime',
							'$statusTypeID',
							
							'',
							'$userID',
							'1');";
		$portal->mysqlDB->Query($sqlstring);
		
		
	}
	
