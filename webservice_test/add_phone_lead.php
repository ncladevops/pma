<?php

	$url = "http://127.0.0.1/optimizeod/webservice/phone.php";

	if($_POST['Submit'] == 'Add Lead') {
		
		// Build XML
		$xml = 
'<?xml version="1.0" encoding="utf-8" ?>
<Leads>
<Client></Client>
<Lead>
<Id>' . date("YmdHis") . '</Id>
<First>' . $_POST['First'] . '</First>
<Last>' . $_POST['Last'] . '</Last>
<Street_Address>' . $_POST['Street_Address'] . '</Street_Address>
<City>' . $_POST['City'] . '</City>
<State>' . $_POST['State'] . '</State>
<Zip>' . $_POST['Zip'] . '</Zip>
<Home_Phone>' . $_POST['Home_Phone'] . '</Home_Phone>
<Work_Phone>' . $_POST['Work_Phone'] . '</Work_Phone>
<Email_Address>' . $_POST['Email_Address'] . '</Email_Address>
</Lead>
</Leads>
';
		
		// Setup and send request to URL
		// initialize curl session
		$ch = curl_init($url);
				
		// set options
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt( $ch, CURLOPT_POST, 1);
		curl_setopt( $ch, CURLOPT_POSTFIELDS, $xml);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: close'));
				
				
		// send request
		$status = curl_exec ($ch);
		
		echo curl_error($ch);
				
		curl_close($ch);
	}
?>
<html>
<head>
	<title>Add Phone Lead</title>
<style>
body {
	font-family: Arial, Helvatica, sans-serif;	
	font-size: 12px;
}
legend {
	font-size: 20px;
	font-weight: bold;
}
label {
	display: block;
	float: left;
	width: 100px;
	font-size: 11px;
	font-weight: bold;
	padding-top: 3px;	
}
</style>
</head>
<body>
<form method='post' action='<?= $_SERVER['PHP_SELF'] ?>'>
<?php
	if($_POST['Submit'] == 'Add Lead') {
?>
<div style="font-size: 14px; color: #000066;">
	<strong>Last Status:</strong> <?= $status; ?> 
</div>
<?php 
	} 
?>
<fieldset>
	<legend>Add Test Phone Lead</legend>
	<div>
		<label>First Name</label>
		<input type='text' name='First' value="<?= $_POST['First'] ?>" style="width: 200px;"/>
	</div>
	<div>
		<label>Last Name</label>
		<input type='text' name='Last' value="<?= $_POST['Last'] ?>" style="width: 200px;"/>
	</div>
	<div>
		<label>Address</label>
		<input type='text' name='Street_Address' value="<?= $_POST['Street_Address'] ?>" style="width: 300px;"/>
	</div>
	<div>
		<label>City, ST Zip</label>
		<input type='text' name='City' value="<?= $_POST['City'] ?>" style="width: 150px;"/>, 
		<input type='text' name='State' value="<?= $_POST['State'] ?>" style="width: 45px;"/> 
		<input type='text' name='Zip' value="<?= $_POST['Zip'] ?>"  style="width: 96px;"/>
	</div>
	<div>
		<label>Primary Phone</label>
		<input type='text' name='Home_Phone' value="<?= $_POST['Home_Phone'] ?>" style="width: 100px;"/>
	</div>
	<div>
		<label>Secondary Phone</label>
		<input type='text' name='Work_Phone' value="<?= $_POST['Work_Phone'] ?>" style="width: 100px;"/>
	</div>
	<div>
		<label>Email</label>
		<input type='text' name='Email_Address' value="<?= $_POST['Email_Address'] ?>" style="width: 200px;"/>
	</div>
	<div>
		<input type="submit" name="Submit" value="Add Lead"/>
	</div>
</fieldset>
</form>
</body>
</html>