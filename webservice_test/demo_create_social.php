<?php
	require("../../printable/include/mysql.inc.php");
	require("../../printable/include/optimize.printable.inc.php");
	require("../globals.php");
	require("demo_lookups.php");
	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	
	$portal = new OptimizePortal($COMPANY_ID, $db);
	
	// Get Demo Users
	$us = $portal->GetCompanyUsers();
	$userIDs = array();
	foreach($us as $u) {
		if(substr($u->Username, 0, 9) == "demo_user")
			$userIDs[] = $u->UserID;
	}
	
	// Get list of postings
	$postings = $portal->GetPostings($portal->CurrentCompany->CompanyID, $_GET['section']);
	$posting_types = array("facebook", "twitter", "linkedin");
	
	// Create Postings
	foreach($userIDs as $uID) {
		foreach($postings as $post) {
			foreach($posting_types as $type) {
				if(rand(1, 100) > 55) {
					$sqlstring = "INSERT INTO
									socialpostingtransaction
										(SocialPostingID, UserID, PostingTime,
										PostingType, ExternalID, CommentCount,
										LikeCount, ShareCount)
									VALUES
										('{$post->SocialPostingID}',
										'$uID',
										'" . date("Y-m-d h:i:s", strtotime("2013-05-01 +" . rand(100, (60*60*24*31)) . "seconds")) . "',
										
										'$type',
										'TESTPOST',
										'" . rand(0, 50) . "',
										
										'" . rand(0, 100) . "',
										'" . rand(0, 75) . "');";
					$social_result = $portal->mysqlDB->Query($sqlstring);
				}
			}
		}
	}
	
	
