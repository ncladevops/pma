<?php

$FIRSTNAME_LOOKUP = array("James", "Mary", "John", "Patricia", "Robert", "Elizabeth", "Michael", "Jennifer", 
							"William", "Linda", "David", "Barbara", "Richard", "Susan", "Joseph", "Margaret", 
							"Charles", "Jessica", "Thomas", "Dorothy", "Christopher", "Sarah", "Daniel", "Karen", 
							"Matthew", "Nancy", "Donald", "Betty", "Anthony", "Lisa", "Paul", "Sandra", "Mark", 
							"Helen", "George", "Donna", "Steven", "Ashley", "Kenneth", "Kimberly", "Andrew", 
							"Carol", "Edward", "Michelle", "Brian", "Amanda", "Joshua", "Emily", "Kevin", 
							"Melissa", "Ronald", "Laura", "Timothy", "Deborah", "Jason", "Stephanie", "Jeffrey", 
							"Rebecca", "Gary", "Sharon", "Ryan", "Cynthia", "Nicholas", "Ruth", "Eric", "Kathleen", 
							"Stephen", "Anna", "Jacob", "Shirley", "Larry", "Amy", "Frank", "Angela", "Jonathan", 
							"Virginia", "Scott", "Brenda", "Justin", "Pamela", "Raymond", "Catherine", "Brandon", 
							"Katherine", "Gregory", "Nicole", "Samuel", "Christine", "Patrick", "Janet", "Benjamin", 
							"Debra", "Jack", "Carolyn", "Dennis", "Samantha", "Jerry", "Rachel", "Alexander", 
							"Heather", "Tyler", "Maria", "Douglas", "Diane", "Henry", "Frances", "Peter", "Joyce", 
							"Walter", "Julie", "Aaron", "Martha", "Jose", "Joan", "Adam", "Evelyn", "Harold", 
							"Kelly", "Zachary");
							
$LASTNAME_LOOKUP = array("Smith", "Johnson", "Williams", "Brown", "Jones", "Miller", "Davis", "Garcia", 
							"Rodriguez", "Wilson", "Martinez", "Anderson", "Taylor", "Thomas", "Hernandez", 
							"Moore", "Martin", "Jackson", "Thompson", "White", "Lopez", "Lee", "Gonzalez", 
							"Harris", "Clark", "Lewis", "Robinson", "Walker", "Perez", "Hall", "Young", "Allen", 
							"Sanchez", "Wright", "King", "Scott", "Green", "Baker", "Adams", "Nelson", "Hill", 
							"Ram�rez", "Campbell", "Mitchell", "Roberts", "Carter", "Phillips", "Evans", "Turner", 
							"Torres", "Parker", "Collins", "Edwards", "Stewart", "Flores", "Morris", "Nguyen", 
							"Murphy", "Rivera", "Cook", "Rogers", "Morgan", "Peterson", "Cooper", "Reed", "Bailey", 
							"Bell", "Gomez", "Kelly", "Howard", "Ward", "Cox", "Diaz", "Richardson", "Wood", 
							"Watson", "Brooks", "Bennett", "Gray", "James", "Reyes", "Cruz", "Hughes", "Price", 
							"Myers");
					
$STATE_LOOKUP = array("AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DE", "FL", "GA", "HI", "ID", "IL", "IN", "IA", "KS", 
						"KY", "LA", "ME", "MD", "MA", "MI", "MN", "MS", "MO", "MT", "NE", "NV", "NH", "NJ", "NM", "NY", 
						"NC", "ND", "OH", "OK", "OR", "PA", "RI", "SC", "SD", "TN", "TX", "UT", "VT", "VA", "WA", "WV", 
						"WI", "WY");
						
$LISTINGTYPE_LOOKUP = array(569, 570);

$STATUSTYPE_LOOKUP = array(134, 135, 136, 137, 138);

$LETTER_LOOKUP = array("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", 
						"P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z");