<?php
	require("../../printable/include/mysql.inc.php");
	require("../../printable/include/optimize.printable.inc.php");
	require("../globals.php");
	require("demo_lookups.php");
	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	
	$portal = new OptimizePortal($COMPANY_ID, $db);
	
	$leadCount = 262;
	
	// Get Demo Users
	$us = $portal->GetCompanyUsers();
	$userIDs = array();
	foreach($us as $u) {
		if(substr($u->Username, 0, 9) == "demo_user")
			$userIDs[] = $u->UserID;
	}
	
	// Create Leads
	for($i = 0; $i < $leadCount; $i++) {		
		$sqlstring = "INSERT INTO
						submissionlisting
							(UserID, DateAdded, CassChecked, CassInvalid, Expired,
							ListingType, Company, FirstName, LastName, Address1,
							Address2, City, State, Zip, Email,
							Phone, Phone2)
						VALUES
							('" . $userIDs[rand(0, sizeof($userIDs) - 1)] . "',
							'2013-05-" . rand(1,31) . "',
							'1',
							'0',
							'0',
							
							'" . $LISTINGTYPE_LOOKUP[rand(0, sizeof($LISTINGTYPE_LOOKUP) - 1)] . "',
							'" . $LETTER_LOOKUP[rand(0, sizeof($LETTER_LOOKUP) - 1)] . " Company',
							'" . $FIRSTNAME_LOOKUP[rand(0, sizeof($FIRSTNAME_LOOKUP) - 1)] . "',
							'" . $LASTNAME_LOOKUP[rand(0, sizeof($LASTNAME_LOOKUP) - 1)] . "',
							'16031 Carmenita Rd',
							
							'Suite " . $LETTER_LOOKUP[rand(0, sizeof($LETTER_LOOKUP) - 1)] . "',
							'Cerritos',
							'" . $STATE_LOOKUP[rand(0, sizeof($STATE_LOOKUP) - 1)] . "',
							'90703-2208',
							'demo_lead@optifinow.com',
							
							'" . rand(100, 999) . rand(100,999) . rand(1000, 9999) . "',
							'" . rand(100, 999) . rand(100,999) . rand(1000, 9999) . "')";
		$portal->mysqlDB->Query($sqlstring);
	}
	
