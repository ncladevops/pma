<?php
	die();
	require("../../printable/include/mysql.inc.php");
	require("../../printable/include/optimize.printable.inc.php");
	require("../globals.php");
	require("demo_lookups.php");
	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	
	$portal = new OptimizePortal($COMPANY_ID, $db);
	
	$WELCOME_EMAIL_NAME = "Optifi Welcome Email 1";
	$GLANCE_EMAIL_NAME = "Email At A Glance Brochure";
		
	// Get Demo Leads
	$sqlstring = "SELECT
						SubmissionListingID, DateAdded, UserID
					FROM
						submissionlisting
					WHERE
						Email = 'demo_lead@optifinow.com';";
	$lead_result = $portal->mysqlDB->Query($sqlstring);
	
	for($i = 0; $i < mysql_num_rows($lead_result); $i++) {
		list($leadID, $dateAdded, $userID) = mysql_fetch_array($lead_result);
		$deliveredPercent = rand(1,100);
		
		// Welcome Email
		if($deliveredPercent > 10) {
			$openedPercent = rand(1,100);
			$clickedPercent = rand(1,100);
			
			// Insert Delivered Event
			$sqlstring = "INSERT INTO
							contactevent
								(ContactID, ContactEventTypeID, EventTime,
								EventNotes, UserID)
							VALUES
								('$leadID',
								'0',
								'$dateAdded',
								
								'Successfully sent $WELCOME_EMAIL_NAME',
								'$userID')";
			$send_result = $portal->mysqlDB->Query($sqlstring);
			
			if($openedPercent > 20) {
				// Insert Opened Event
				$openTime = date("Y-m-d h:i:s", strtotime($dateAdded . " +" . rand(100, (60*60*24)) . "seconds"));
				$sqlstring = "INSERT INTO
								contactevent
									(ContactID, ContactEventTypeID, EventTime,
									EventNotes, UserID)
								VALUES
									('$leadID',
									'185',
									'$openTime',
									
									'$WELCOME_EMAIL_NAME opened',
									'$userID')";
				$opened_result = $portal->mysqlDB->Query($sqlstring);
				
				if($clickedPercent > 20) {
					// Insert Clicked Event
					$clickTime = date("Y-m-d h:i:s", strtotime($openTime . " +" . rand(20, 120) . "seconds"));
					$sqlstring = "INSERT INTO
									contactevent
										(ContactID, ContactEventTypeID, EventTime,
										EventNotes, UserID)
									VALUES
										('$leadID',
										'187',
										'$clickTime',
										
										'$WELCOME_EMAIL_NAME link clicked: http://www.optifinow.com',
										'$userID')";
					$clicked_result = $portal->mysqlDB->Query($sqlstring);
				}
			}
			
		}
		else {
			// Insert Not Delivered Event
			$sqlstring = "INSERT INTO
							contactevent
								(ContactID, ContactEventTypeID, EventTime,
								EventNotes, UserID)
							VALUES
								('$leadID',
								'0',
								'$dateAdded',
								
								'Error Sending $WELCOME_EMAIL_NAME',
								'$userID')";
			$send_result = $portal->mysqlDB->Query($sqlstring);
		}
		
		// At a glance brochure
		if($deliveredPercent > 45) {
			$openedPercent = rand(1,100);
			$clickedPercent = rand(1,100);
			
			// Insert Delivered Event
			$sqlstring = "INSERT INTO
							contactevent
								(ContactID, ContactEventTypeID, EventTime,
								EventNotes, UserID)
							VALUES
								('$leadID',
								'0',
								'$dateAdded',
								
								'Successfully sent $GLANCE_EMAIL_NAME',
								'$userID')";
			$send_result = $portal->mysqlDB->Query($sqlstring);
			
			if($openedPercent > 20) {
				// Insert Opened Event
				$openTime = date("Y-m-d h:i:s", strtotime($dateAdded . " +" . rand(100, (60*60*24)) . "seconds"));
				$sqlstring = "INSERT INTO
								contactevent
									(ContactID, ContactEventTypeID, EventTime,
									EventNotes, UserID)
								VALUES
									('$leadID',
									'185',
									'$openTime',
									
									'$GLANCE_EMAIL_NAME opened',
									'$userID')";
				$opened_result = $portal->mysqlDB->Query($sqlstring);
				
				if($clickedPercent > 20) {
					// Insert Clicked Event
					$clickTime = date("Y-m-d h:i:s", strtotime($openTime . " +" . rand(20, 120) . "seconds"));
					$sqlstring = "INSERT INTO
									contactevent
										(ContactID, ContactEventTypeID, EventTime,
										EventNotes, UserID)
									VALUES
										('$leadID',
										'187',
										'$clickTime',
										
										'$GLANCE_EMAIL_NAME link clicked: http://www.optifinow.com',
										'$userID')";
					$clicked_result = $portal->mysqlDB->Query($sqlstring);
				}
			}
		}
		
		
	}
	
