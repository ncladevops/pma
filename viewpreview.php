<?php
	require("../printable/include/mysql.inc.php");
	require("../printable/include/optimize.printable.inc.php");
	require("globals.php");
		
	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	
	$portal = new PrintablePortal($COMPANY_ID, $db);
	
	$currentUser = $portal->GetUser(0, $_GET['email']);
	
	$contactEventType = $portal->LookupContactEventType($_GET['Activity'], $currentUser->GroupID);
	
	if(!$currentUser) {
		die("Invalid User");
	}
	
	$contact = new stdClass();
	
	$contact->UserFullname 	= $currentUser->FirstName . " " . $currentUser->LastName;
	$contact->UserEmail 	= $currentUser->ContactEmail;
	$contact->UserTitle 	= $currentUser->Title;
	$contact->UserPhone 	= $currentUser->ContactPhone;
	$contact->UserCell 		= $currentUser->ContactCell;
	$contact->UserFax 		= $currentUser->ContactFax;
	$contact->UserAddress 	= $currentUser->Address;
	$contact->UserAddress2 	= $currentUser->Address2;
	$contact->UserCity 		= $currentUser->City;
	$contact->UserState 	= $currentUser->State;
	$contact->UserZip 		= $currentUser->Zipcode;
	$contact->LogoName 		= $currentUser->LogoName;
	$contact->UserFirstName = $currentUser->FirstName;
	$contact->UserLastName 	= $currentUser->LastName;
	$contact->OtherID 		= $currentUser->OtherID;
	$contact->UserWebsite 	= $currentUser->Website;
	
	$contact->Category		= $_GET['Category'];
	$contact->Segment		= $_GET['Segment'];
	
	$contact->EventType		= $contactEventType->EventType;
	$contact->ContactEventTypeID		= $contactEventType->ContactEventTypeID;
		
	$product = $portal->GetFirstManuallyTriggeredProduct($contact);
	
	if(!$product) {
		die("To generate a preview, please make the following selections:<br/><ol><li>Category</li><li>Segment</li><li>Delivery Method</li></ol>");
	}
?>
<img src="<?= str_replace("/demo/", "", $portal->CurrentCompany->Website) . $product->ThumbnailPath; ?>" />
<!-- <?php var_dump($product) ?> -->