<?php
	require("../printable/include/mysql.inc.php");
	require("../printable/include/optimize.printable.inc.php");
	require("globals.php");
	
	if($_POST['popup_window'] == 'true')
	{
		$_GET['popup'] = true;
	}
	
	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	
	$portal = new OptimizePortal($COMPANY_ID, $db);
	
	$currentUser = $portal->GetUser($_SESSION['currentuserid']);
		
	if(!$currentUser)
	{
		header("Location: login.php?message=" .urlencode("Not logged in or login error."));
		die();
	}
	
	$isSubAdmin = $portal->CheckPriv($currentUser->UserID, 'subadmin');
	$isAdmin = $portal->CheckPriv($currentUser->UserID, 'admin');
	
	if($isSubAdmin)
	{
		$userString = "all";
	}
	else 
	{
		$userString = $currentUser->UserID;
	}
	
	$lead = $portal->GetOptimizeContact($_GET['id'], $userString);
	
	$lead_step_ids = explode(";", $_SESSION['lead_step_ids']);
		
	if(!$lead)
	{
		header("Location: home.php?message=" . urlencode("Lead not Available"));
		die();
	}
	
	if($_POST['Submit'] == 'Update' || $_POST['Submit'] == 'Update & Close')
	{
		$_POST['DOB'] = strtotime("1/1/" . $_POST['DOB']) ? date("Y-1-1", strtotime("1/1/" . $_POST['DOB'])) : "";
		$_POST['CoDOB'] = strtotime("1/1/" . $_POST['CoDOB']) ? date("Y-1-1", strtotime("1/1/" . $_POST['CoDOB'])) : "";
		
	
		if($_POST['ContactStatusTypeID'] == 1 && !$isSubAdmin)
		{
			$_POST['ContactStatusTypeID'] = $lead->ContactStatusTypeID;
		}
		
		$oldStatus = $lead->ContactStatusTypeID;
		foreach($_POST as $k=>$v)
		{
			$lead->$k = $v;
		}
		
		if($isSubAdmin && $_POST['NewUserID'] != $lead->UserID)
		{
			$lead->UserID = $_POST['NewUserID'];
		}
		else
		{
			$lead->UserID = $currentUser->UserID;
		}
		if(array_search($_POST['ContactStatusTypeID'], $PRE_QUAL_STATUSES) !== false
			&& $oldStatus != $lead->ContactStatusTypeID)
		{
			$lead->UserID = $PRE_QUAL_USERID;
		}
		elseif(array_search($_POST['ContactStatusTypeID'], $MARKETING_STATUSES) !== false
			&& $oldStatus != $lead->ContactStatusTypeID)
		{
			$lead->UserID = $MARKETING_USERID;
		}
		elseif($EXPIRED_STATUSTYPE_ID == $_POST['ContactStatusTypeID']
				&& $oldStatus != $lead->ContactStatusTypeID)
		{
			$lead->UserID = $LEADSTORE_USERID;		
		}
				
		$portal->UpdateOptimizeContact($lead);
		
		if($_POST['Submit'] == 'Update & Close')
		{
			header('Location: list_lead.php?message=' . urlencode("Lead updated successfully."));
			die();
		}
		
	}
	elseif($_POST['Submit'] == 'Cancel')
	{
		header('Location: list_lead.php?message=' . urlencode("Action Canceled. Lead not updated."));
		die();
	}
	elseif($_POST['Submit'] == 'Delete' && $isSubAdmin)
	{
		$portal->DeleteContact($lead->SubmissionListingID, ($isSubAdmin ? $lead->UserID : $currentUser->UserID));
		
		header('Location: list_lead.php?message=' . urlencode("Lead deleted successfully."));
		die();
	}
	
	$csts = $portal->GetContactStatusTypes();
	$contactStatuseTypes = array();
	$shortStatuses = array();
	$shortStatusDetails = array();
	foreach($csts as $cst) {
		if(!$cst->Disabled) {
			$shortStatuses[$cst->ShortName] = (strlen($cst->DetailName) > 0 ? 1 : 0);
			$shortStatusDetails[$cst->ShortName][] = $cst->ContactStatus;
			$contactStatuseTypes[$cst->ContactStatus] = $cst;
		}
	}
	$contactEventTypes = $portal->GetContactEventTypes();
	$cetLookup = array();
	foreach($contactEventTypes as $cet)
	{
		$cetLookup[$cet->ContactEventTypeID] = $cet->EventType;
	}
	
  	$allEvents = $portal->GetContactHistory($lead->SubmissionListingID);
  	
  	$users = $portal->GetCompanyUsers();
  	$slt = $portal->GetSubmissionListingType($lead->ListingType);
	$appt = $portal->GetOpenAppointment($lead->SubmissionListingID, $currentUser->UserID);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
	<title>
		<?= $portal->CurrentCompany->CompanyName ?> Leads on Demand :: Edit Lead Detailed
	</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />		
	<link type="text/css" media="all" rel="stylesheet" href="style.css" />
	<link type="text/css" media="all" rel="stylesheet" href="css/edit_lead_detail.css" />
	<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />
	<script  src="js/func.js"></script>
<script language="JavaScript" type="text/JavaScript">
	function updateContactEvent(id, actionName)
	{
		document.getElementById("LastActionSpan").innerHTML = actionName;
	}
</script>
</head>
<body bgcolor="#FFFFFF">
<form method="post" action="<?= $_SERVER['PHP_SELF'] ?>?id=<?= $lead->SubmissionListingID; ?>">
<?php include("components/header.php") ?>
<div id="body">
	<?php 
		$CURRENT_PAGE = "Leads";
		include("components/navbar.php"); 
	?>
	<div class="error">
		<?= $_GET['message']; ?>
	</div>
	<div id="EditLeadDiv">
	<center>
		<div class="itemSection">
			<div class="buttonSection">
				<input type="submit" value="Update" name="Submit"/>&nbsp;&nbsp;
<?php
	if($isSubAdmin)
	{
?>
				<input type="submit" value="Delete" name="Submit" onclick="return confirm('Are you sure you want to delete this lead?')"/>&nbsp;&nbsp;
<?php
	}
?>
				<input type="submit" value="Cancel" name="Submit"/>
			</div>
		</div>
	</center>
	<center>
		<div class="itemSection" id="StatusSectionDiv">
			<div id="ContactStatusTypeIDDiv">
				<label for="ContactStatusTypeID">Status:</label><br/>
				<select name="ContactStatusTypeID">
					<option value="0"></option>
<?php
	foreach($contactStatuseTypes as $cst)
	{
		if((int)$cst->Disabled == 1 && $cst->ContactStatusTypeID != $lead->ContactStatusTypeID) continue;
?>
					<option 
							value="<?= $cst->ContactStatusTypeID ?>" 
							<?= $cst->ContactStatusTypeID == $lead->ContactStatusTypeID ? "SELECTED" : "" ?>>
						<?= $cst->ContactStatus ?>
					</option>
<?php
	}
	if($isSubAdmin)
	{
?>
					<option 
							value="<?= $EXPIRED_STATUSTYPE_ID ?>" 
							<?= $EXPIRED_STATUSTYPE_ID == $lead->ContactStatusTypeID ? "SELECTED" : "" ?>>
						Lead Store
					</option>
<?php
	}
?>
				</select>
				<span class="print_only"><?= $lead->ContactStatus ?></span>
			</div>
			<div id="LastContactEventTypeIDDiv">
				<label for="LastContactEventTypeID">Last Action:</label><br/>
				<span id="LastActionSpan"><?= $cetLookup[$lead->LastContactEvent->ContactEventTypeID] ? $cetLookup[$lead->LastContactEvent->ContactEventTypeID] : "No action taken." ?></span>&nbsp;
					<a href="#" onclick="wopen('add_action.php?id=<?= $lead->SubmissionListingID; ?>', 'add_action', 350, 350); return false;" class="no_print">New Action</a>
			</div>
		</div>
	</center>
	<div class="sectionHeader">
		Lead Details <a href="edit_lead.php?id=<?= $lead->SubmissionListingID; ?>" class="no_print">(view summary)</a>
	</div>
	<div id="Section0Div" class="sectionDiv">
		<div class="itemSection">
			<div id="ListingTypeDiv">
				<label for="ListingType">Lead Source:</label><br/>
<?php
	if($slt->UserManageable == "1")
	{
		$slts = $portal->GetSubmissionListingTypes(0, 'SortOrder, SubmissionListingTypeID', 'all', 'UserManageable = 1');
?>
				<select name="ListingType" id="ListingType">
<?php
	foreach($slts as $slt)
	{
?>
					<option value="<?= $slt->SubmissionListingTypeID?>"
							<?= $slt->SubmissionListingTypeID == $lead->ListingType ? "SELECTED" : "" ?>>
						<?= $slt->ListingType ?>
					</option>
<?php
	}
?>
				</select>
				<span class="print_only"><?= $lead->ListingTypeName  ?></span>
			</div>
			<div id="OtherListingTypeDiv">
				<label for="OtherListingType">Campaign Code:</label><br/>
				<input type="text" id="OtherListingType" name="OtherListingType" style="width: 100px" value="<?= $lead->OtherListingType; ?>"/>
				<span class="print_only"><?= $lead->OtherListingType ?></span>
<?php
	}
	else
	{
		echo $lead->ListingTypeName;		
	}
?>
			</div>
			<div id="NewUserIDDiv">
				<label for="NewUserID">Assigned To:</label><br/>
<?php
	if($isSubAdmin)
	{
?>
				<select name="NewUserID" id="NewUserID">
<?php
	foreach($users as $u)
	{
?>
					<option value="<?= $u->UserID ?>" <?= $u->UserID == $lead->UserID ? "SELECTED" : "" ?>>
						<?= $u->FirstName . " " . $u->LastName ?>
					</option>
<?php
	}
?>
				</select>
				<span class="print_only"><?= $lead->UserFullname ?></span>
<?php
	}
	elseif($lead->ContactStatusTypeID != 1)
	{
		echo $lead->UserFullname;		
	}
?>
			</div>
		</div>
	</div>
	<div class="sectionHeader">
		I. Type of Mortgage and Terms of Loan 
	</div>
	<div id="Section1Div" class="sectionDiv">
		<div class="itemSection">
			<div id="LoanAmountDiv">
				<label for="LoanAmount">Loan Amount:</label><br/>
				<input type="text" id="LoanAmount" name="LoanAmount" style="width: 100px" value="<?= $lead->LoanAmount ? $lead->LoanAmount : ""; ?>"/>
				<span class="print_only"><?= $lead->LoanAmount ? $lead->LoanAmount : ""; ?></span>
			</div>
			<div id="LoanTimeframeDiv">
				<label for="LoanTimeframe">Loan Timeframe:</label><br/>
				<input type="text" id="LoanTimeframe" name="LoanTimeframe" style="width: 130px" value="<?= $lead->LoanTimeframe; ?>"/>
				<span class="print_only"><?= $lead->LoanTimeframe ?></span>
			</div>
			<div id="RateTypeDiv">
				<label for="RateType">Rate Type:</label><br/>
				<input type="text" id="RateType" name="RateType" style="width: 130px" value="<?= $lead->RateType; ?>"/>
				<span class="print_only"><?= $lead->RateType ?></span>
			</div>
		</div>
		<div class="itemSection">
			<div id="CashOutDiv">
				<label for="CashOut">Cashout:</label><br/>
				<input type="text" id="CashOut" name="CashOut" style="width: 100px" value="<?= $lead->CashOut ? $lead->CashOut : ""; ?>"/>
				<span class="print_only"><?= $lead->CashOut ? $lead->CashOut : ""; ?></span>
			</div>
			<div id="FHALoanDiv">
				<label for="FHALoan">Current FHA Loan:</label><br/>
				<input type="text" id="FHALoan" name="FHALoan" style="width: 100px" value="<?= $lead->FHALoan ? $lead->FHALoan : ""; ?>"/>
				<span class="print_only"><?= $lead->FHALoan ? $lead->FHALoan : ""; ?></span>
			</div>
			<div id="SignedContractDiv">
				<label for="SignedContract">Signed Contract:</label><br/>
				<input type="text" id="SignedContract" name="SignedContract" style="width: 100px" value="<?= $lead->SignedContract ? $lead->SignedContract : ""; ?>"/>
				<span class="print_only"><?= $lead->SignedContract ? $lead->SignedContract : ""; ?></span>
			</div>			
		</div>
	</div>
	<div class="sectionHeader">
		II. Property Information and Purpose of Loan 
	</div>
	<div id="Section2Div" class="sectionDiv">
		<div class="itemSection">
			<div id="PropertyAddressDiv">
				<label for="PropertyAddress">Street Address:</label><br/>
				<input type="text" id="PropertyAddress" name="PropertyAddress" style="width: 415px" value="<?= $lead->PropertyAddress; ?>"/>
				<span class="print_only"><?= $lead->PropertyAddress ?></span>
			</div>
			<div id="PropertyFoundDiv">
				<label for="PropertyFound">Property Found:</label><br/>
				<label class="radio_label <?= $lead->PropertyFound == 'Yes' ? '' : 'no_print'; ?>">Yes</label><input type="radio" name="PropertyFound" id="PropertyFound_Yes" value="Yes" <?= $lead->PropertyFound == 'Yes' ? 'CHECKED' : ''; ?> />
				<label class="radio_label <?= $lead->PropertyFound == 'No' ? '' : 'no_print'; ?>">No</label><input type="radio" name="PropertyFound" id="PropertyFound_No" value="No" <?= $lead->PropertyFound == 'No' ? 'CHECKED' : ''; ?> />
			</div>
		</div>
		<div class="itemSection">
			<div id="PropertyCityDiv">
				<label for="PropertyCity">City:</label><br/>
				<input type="text" id="PropertyCity" name="PropertyCity" style="width: 250px" value="<?= $lead->PropertyCity; ?>"/>
				<span class="print_only"><?= $lead->PropertyCity ?></span>
			</div>
			<div id="PropertyStateDiv">
				<label for="PropertyState">ST:</label><br/>
				<input type="text" id="PropertyState" name="PropertyState" style="width: 35px" value="<?= $lead->PropertyState; ?>"/>
				<span class="print_only"><?= $lead->PropertyState ?></span>
			</div>
			<div id="PropertyZipDiv">
				<label for="PropertyZip">Zip:</label><br/>
				<input type="text" id="PropertyZip" name="PropertyZip" style="width: 100px" value="<?= $lead->PropertyZip; ?>"/>
				<span class="print_only"><?= $lead->PropertyZip ?></span>
			</div>
			<div id="PropertyTypeDiv">
				<label for="PropertyType">Property Type:</label><br/>
				<select name="PropertyType" id="PropertyType">
					<option value=""></option>
					<option value="SFR" <?= $lead->PropertyType == 'SFR' ? 'SELECTED' : ''; ?>>Single Family Detached</option>					
					<option value="Condo" <?= $lead->PropertyType == 'Condo' ? 'SELECTED' : ''; ?>>Condo</option>					
					<option value="High-rise Condo" <?= $lead->PropertyType == 'High-rise Condo' ? 'SELECTED' : ''; ?>>High-rise Condo</option>					
					<option value="Log Cabin" <?= $lead->PropertyType == 'Log Cabin' ? 'SELECTED' : ''; ?>>Log Cabin</option>					
					<option value="1-4 Family" <?= $lead->PropertyType == '1-4 Family' ? 'SELECTED' : ''; ?>>1-4 Family</option>					
					<option value="Townhouse" <?= $lead->PropertyType == 'Townhouse' ? 'SELECTED' : ''; ?>>Townhouse</option>					
				</select>
				<span class="print_only"><?= $lead->PropertyType ?></span>
			</div>
		</div>
		<div class="itemSection">
			<div id="LoanPurposeDiv">
				<label for="LoanPurpose">Mortgage Type:</label><br/>
				<select name="LoanPurpose" id="LoanPurpose" style="width: 260px" >
					<option value=""></option>
					<option value="Conventional" <?= $lead->LoanPurpose == 'Conventional' ? 'SELECTED' : ''; ?>>Conventional</option>			
					<option value="FHA" <?= $lead->LoanPurpose == 'FHA' ? 'SELECTED' : ''; ?>>FHA</option>			
					<option value="VA" <?= $lead->LoanPurpose == 'VA' ? 'SELECTED' : ''; ?>>VA</option>			
					<option value="Jumbo" <?= $lead->LoanPurpose == 'Jumbo' ? 'SELECTED' : ''; ?>>Jumbo</option>			
				</select>
				<span class="print_only"><?= $lead->LoanPurpose; ?></span>
			</div>
		</div>
		<div class="itemSection">
			<div id="LoanTypeDiv">
				<label for="LoanType">Loan Type Request:</label><br/>
				<input type="text" id="LoanType" name="LoanType" style="width: 180px" value="<?= $lead->LoanType ? $lead->LoanType : ""; ?>"/>
				<span class="print_only"><?= $lead->LoanType ? $lead->LoanType : ""; ?></span>
			</div>
			<div id="OwnerOccupiedDiv">
				<label for="OwnerOccupied">Owner Occupied:</label><br/>
				<label class="radio_label <?= $lead->OwnerOccupied == 'Yes' ? '' : 'no_print'; ?>">Yes</label><input type="radio" name="OwnerOccupied" id="OwnerOccupied_Yes" value="Yes" <?= $lead->OwnerOccupied == 'Yes' ? 'CHECKED' : ''; ?> />
				<label class="radio_label <?= $lead->OwnerOccupied == 'No' ? '' : 'no_print'; ?>">Investment</label><input type="radio" name="OwnerOccupied" id="OwnerOccupied_No" value="No" <?= $lead->OwnerOccupied == 'No' ? 'CHECKED' : ''; ?> />
				<label class="radio_label <?= $lead->OwnerOccupied == 'Second Home' ? '' : 'no_print'; ?>">Second Home</label><input type="radio" name="OwnerOccupied" id="OwnerOccupied_Second_Home" value="Second Home" <?= $lead->OwnerOccupied == 'Second Home' ? 'CHECKED' : ''; ?> />
			</div>
		</div>
		<div class="itemSection">
			<div id="PurchaseYearDiv">
				<label for="PurchaseYear">Year Purchased:</label><br/>
				<input type="text" id="PurchaseYear" name="PurchaseYear" style="width: 100px" value="<?= $lead->PurchaseYear ? $lead->PurchaseYear : ""; ?>"/>
				<span class="print_only"><?= $lead->PurchaseYear ? $lead->PurchaseYear : ""; ?></span>
			</div>
			<div id="PurchasePriceDiv">
				<label for="PurchasePrice">Purchase Price:</label><br/>
				<input type="text" id="PurchasePrice" name="PurchasePrice" style="width: 120px" value="<?= $lead->PurchasePrice ? $lead->PurchasePrice : ""; ?>"/>
				<span class="print_only"><?= $lead->PurchasePrice ?></span>
			</div>
			<div id="MarketValueDiv">
				<label for="MarketValue">Market Value:</label><br/>
				<input type="text" id="MarketValue" name="MarketValue" style="width: 120px" value="<?= $lead->MarketValue ? $lead->MarketValue : ""; ?>"/>
				<span class="print_only"><?= $lead->MarketValue ? $lead->PurchasePrice : ""; ?></span>
			</div>
			<div id="NewHomeValueDiv">
				<label for="NewHomeValue">New Home Value:</label><br/>
				<input type="text" id="NewHomeValue" name="NewHomeValue" style="width: 120px" value="<?= $lead->NewHomeValue ? $lead->NewHomeValue : ""; ?>"/>
				<span class="print_only"><?= $lead->NewHomeValue ? $lead->NewHomeValue : ""; ?></span>
			</div>
		</div>
		<div class="itemSection">
			<div id="BedroomCountDiv">
				<label for="BedroomCount">Bedroom Count:</label><br/>
				<input type="text" id="BedroomCount" name="BedroomCount" style="width: 100px" value="<?= $lead->BedroomCount ? $lead->BedroomCount : ""; ?>"/>
				<span class="print_only"><?= $lead->BedroomCount ? $lead->BedroomCount : ""; ?></span>
			</div>
			<div id="BathroomCountDiv">
				<label for="BathroomCount">Bathroom Count:</label><br/>
				<input type="text" id="BathroomCount" name="BathroomCount" style="width: 100px" value="<?= $lead->BathroomCount ? $lead->BathroomCount : ""; ?>"/>
				<span class="print_only"><?= $lead->BathroomCount ? $lead->BathroomCount : ""; ?></span>
			</div>
			<div id="SquareFootageDiv">
				<label for="SquareFootage">Square Footage:</label><br/>
				<input type="text" id="SquareFootage" name="SquareFootage" style="width: 100px" value="<?= $lead->SquareFootage ? $lead->SquareFootage : ""; ?>"/>
				<span class="print_only"><?= $lead->SquareFootage ? $lead->SquareFootage : ""; ?></span>
			</div>
		</div>
	</div>
	<div class="sectionHeader">
		III. Borrower Information 
	</div>
	<div id="Section3Div" class="sectionDiv">
		<div id="BorrowerDiv">
			<div class="itemSection itemHeader">
				Borrower
			</div>
			<div class="itemSection">
				<div id="FirstNameDiv">
					<label for="FirstName">First Name:</label><br/>
					<input type="text" id="FirstName" name="FirstName" style="width: 95px" value="<?= $lead->FirstName; ?>"/>
					<span class="print_only"><?= $lead->FirstName ?></span>
				</div>
				<div id="LastNameDiv">
					<label for="LastName">Last Name:</label><br/>
					<input type="text" id="LastName" name="LastName" style="width: 110px" value="<?= $lead->LastName; ?>"/>
					<span class="print_only"><?= $lead->LastName ?></span>
				</div>
				<div id="DOBDiv">
					<label for="DOB">Year of Birth:</label>
					<input type="text" id="DOB" name="DOB" style="width: 65px" value="<?= (strtotime($lead->DOB) ? date("Y", strtotime($lead->DOB)) : ""); ?>" />
					<span class="print_only"><?= (strtotime($lead->DOB) ? date("Y", strtotime($lead->DOB)) : ""); ?></span>
				</div>
			</div>
			<div class="itemSection">
				<div id="Address1Div">
					<label for="Address1">Street Address:</label><br/>
					<input type="text" id="Address1" name="Address1" style="width: 315px" value="<?= $lead->Address1; ?>"/>
					<span class="print_only"><?= $lead->Address1 ?></span>
				</div>
			</div>
			<div class="itemSection">
				<div id="CityDiv">
					<label for="City">City:</label><br/>
					<input type="text" id="City" name="City" style="width: 160px" value="<?= $lead->City; ?>"/>
					<span class="print_only"><?= $lead->City ?></span>
				</div>
				<div id="StateDiv">
					<label for="State">ST:</label><br/>
					<input type="text" id="State" name="State" style="width: 35px" value="<?= $lead->State; ?>"/>
					<span class="print_only"><?= $lead->State ?></span>
				</div>
				<div id="ZipDiv">
					<label for="Zip">Zip:</label><br/>
					<input type="text" id="Zip" name="Zip" style="width: 100px" value="<?= $lead->Zip; ?>"/>
					<span class="print_only"><?= $lead->Zip ?></span>
				</div>
			</div>
		</div>
		<div id="CoBorrowerDiv">
			<div class="itemSection itemHeader">
				Co-Borrower
			</div>
			<div class="itemSection">
				<div id="FirstName2Div">
					<label for="FirstName">Co-First Name:</label><br/>
					<input type="text" id="FirstName2" name="FirstName2" style="width: 95px" value="<?= $lead->FirstName2; ?>"/>
					<span class="print_only"><?= $lead->FirstName2 ?></span>
				</div>
				<div id="LastName2Div">
					<label for="FirstName">Last Name:</label><br/>
					<input type="text" id="LastName2" name="LastName2" style="width: 110px" value="<?= $lead->LastName2; ?>"/>
					<span class="print_only"><?= $lead->LastName2 ?></span>
				</div>
				<div id="CoDOBDiv">
					<label for="CoDOB">Year of Birth:</label>
					<input type="text" id="CoDOB" name="CoDOB" style="width: 65px" value="<?= (strtotime($lead->CoDOB) ? date("Y", strtotime($lead->CoDOB)) : ""); ?>" />
					<span class="print_only"><?= (strtotime($lead->CoDOB) ? date("Y", strtotime($lead->CoDOB)) : ""); ?></span>
				</div>
			</div>
			<div class="itemSection">
				<div id="CoAddressDiv">
					<label for="CoAddress">Street Address:</label><br/>
					<input type="text" id="CoAddress" name="CoAddress" style="width: 315px" value="<?= $lead->CoAddress; ?>"/>
					<span class="print_only"><?= $lead->CoAddress ?></span>
				</div>
			</div>
			<div class="itemSection">
				<div id="CoCityDiv">
					<label for="CoCity">City:</label><br/>
					<input type="text" id="CoCity" name="CoCity" style="width: 160px" value="<?= $lead->CoCity; ?>"/>
					<span class="print_only"><?= $lead->CoCity ?></span>
				</div>
				<div id="CoStateDiv">
					<label for="CoState">ST:</label><br/>
					<input type="Cotext" id="CoState" name="CoState" style="width: 35px" value="<?= $lead->CoState; ?>"/>
					<span class="print_only"><?= $lead->CoState ?></span>
				</div>
				<div id="CoZipDiv">
					<label for="CoZip">Zip:</label><br/>
					<input type="text" id="CoZip" name="CoZip" style="width: 100px" value="<?= $lead->CoZip; ?>"/>
					<span class="print_only"><?= $lead->CoZip ?></span>
				</div>
			</div>
		</div>
		<div id="OtherBorrowerDiv">
			<div class="itemSection">
				<div id="ContactTimeDiv">
					<label for="ContactTime">Best Contact Time:</label><br/>
					<input type="text" id="ContactTime" name="ContactTime" style="width: 415px" value="<?= $lead->ContactTime; ?>"/>
					<span class="print_only"><?= $lead->ContactTime ?></span>
				</div>
			</div>
			<div class="itemSection">
				<div id="PhoneDiv">
					<label for="Phone">Home Phone: (<a href="#" onclick="wopen('trigger_call.php?id=<?= $lead->SubmissionListingID; ?>&field=Phone', 'add_action', 350, 285); return false;">call</a>)</label><br/>
					<input type="text" id="Phone" name="Phone" style="width: 120px" value="<?= $lead->Phone; ?>"/>
					<span class="print_only"><?= $lead->Phone ?></span>
				</div>
				<div id="Phone2Div">
					<label for="Phone2">Cell Phone: (<a href="#" onclick="wopen('trigger_call.php?id=<?= $lead->SubmissionListingID; ?>&field=Phone2', 'add_action', 350, 285); return false;">call</a>)</label><br/>
					<input type="text" id="Phone2" name="Phone2" style="width: 120px" value="<?= $lead->Phone2; ?>"/>
					<span class="print_only"><?= $lead->Phone2 ?></span>
				</div>
				<div id="FaxDiv">
					<label for="Fax">Fax:</label><br/>
					<input type="text" id="Fax" name="Fax" style="width: 120px" value="<?= $lead->Fax; ?>"/>
					<span class="print_only"><?= $lead->Fax ?></span>
				</div>
			</div>
			<div class="itemSection">
				<div id="EmailDiv">
					<label for="Email">Email Address:</label><br/>
					<input type="text" id="Email" name="Email" style="width: 350px" value="<?= $lead->Email; ?>"/>
					<span class="print_only"><?= $lead->Email ?></span>
				</div>
			</div>
			<div class="itemSection">
				<div id="MaritalStatusDiv">
					<label for="MaritalStatus">Marital Status:</label>
					<input type="text" id="MaritalStatus" name="MaritalStatus" style="width: 125px" value="<?= $lead->MaritalStatus; ?>" />
					<span class="print_only"><?= $lead->MaritalStatus ?></span>
				</div>
				<div id="SpouseFirstNameDiv">
					<label for="SpouseFirstName">Spouse First Name:</label><br/>
					<input type="text" id="SpouseFirstName" name="SpouseFirstName" style="width: 130px" value="<?= $lead->SpouseFirstName; ?>"/>
					<span class="print_only"><?= $lead->SpouseFirstName ?></span>
				</div>
				<div id="SpouseFirstNameDiv">
					<label for="SpouseLastName">Last Name:</label><br/>
					<input type="text" id="SpouseLastName" name="SpouseLastName" style="width: 130px" value="<?= $lead->SpouseLastName; ?>"/>
					<span class="print_only"><?= $lead->SpouseLastName ?></span>
				</div>
			</div>
		</div>		
	</div>
	<div class="sectionHeader">
		IV. Employment Information 
	</div>
	<div id="Section4Div" class="sectionDiv">
		<div id="BorrowerEmpDiv">
			<div class="itemSection itemHeader">
				Borrower
			</div>
			<div class="itemSection">
				<div id="EmployerNameDiv">
					<label for="EmployerName">Borrower Employer's Name:</label><br/>
					<input type="text" id="EmployerName" name="EmployerName" style="width: 190px" value="<?= $lead->EmployerName; ?>"/>
					<span class="print_only"><?= $lead->EmployerName ?></span>
				</div>
				<div id="TimeOnJobDiv">
					<label for="TimeOnJob">Time On Job:</label>
					<input type="text" id="TimeOnJob" name="TimeOnJob" style="width: 125px" value="<?= $lead->TimeOnJob; ?>" />
					<span class="print_only"><?= $lead->TimeOnJob ?></span>
				</div>
			</div>
			<div class="itemSection">
				<div id="EmployerAddressDiv">
					<label for="EmployerAddress">Employer Address:</label><br/>
					<input type="text" id="EmployerAddress" name="EmployerAddress" style="width: 320px" value="<?= $lead->EmployerAddress; ?>"/>
					<span class="print_only"><?= $lead->EmployerAddress ?></span>
				</div>
			</div>
			<div class="itemSection">
				<div id="EmployerCityDiv">
					<label for="EmployerCity">City:</label><br/>
					<input type="text" id="EmployerCity" name="EmployerCity" style="width: 165px" value="<?= $lead->EmployerCity; ?>"/>
					<span class="print_only"><?= $lead->EmployerCity ?></span>
				</div>
				<div id="EmployerStateDiv">
					<label for="EmployerState">ST:</label><br/>
					<input type="text" id="EmployerState" name="EmployerState" style="width: 35px" value="<?= $lead->EmployerState; ?>"/>
					<span class="print_only"><?= $lead->EmployerState ?></span>
				</div>
				<div id="EmployerZipDiv">
					<label for="EmployerZip">Zip:</label><br/>
					<input type="text" id="EmployerZip" name="EmployerZip" style="width: 100px" value="<?= $lead->EmployerZip; ?>"/>
					<span class="print_only"><?= $lead->EmployerZip ?></span>
				</div>
			</div>
			<div class="itemSection">
				<div id="OccupationDiv">
					<label for="Occupation">Occupation:</label><br/>
					<input type="text" id="Occupation" name="Occupation" style="width: 120px" value="<?= $lead->Occupation; ?>"/>
					<span class="print_only"><?= $lead->Occupation ?></span>
				</div>
				<div id="BFSDiv">
					<label for="BFS">BFS:</label><br/>
					<label class="radio_label <?= $lead->BFS == '1' ? '' : 'no_print'; ?>">Yes</label><input type="radio" name="BFS" id="BFS_Yes" value="1" <?= $lead->BFS == '1' ? 'CHECKED' : ''; ?> />
					<label class="radio_label <?= $lead->BFS == '0' ? '' : 'no_print'; ?>">No</label><input type="radio" name="BFS" id="BFS_No" value="0" <?= $lead->BFS == '0' ? 'CHECKED' : ''; ?> />
				</div>
				<div id="WorkPhoneDiv">
					<label for="WorkPhone">Work Phone: (<a href="#" onclick="wopen('trigger_call.php?id=<?= $lead->SubmissionListingID; ?>&field=WorkPhone', 'add_action', 350, 285); return false;">call</a>)</label><br/>
					<input type="text" id="WorkPhone" name="WorkPhone" style="width: 100px" value="<?= $lead->WorkPhone; ?>"/>
					<span class="print_only"><?= $lead->WorkPhone ?></span>
				</div>
			</div>
		</div>
		<div id="CoBorrowerEmpDiv">
			<div class="itemSection itemHeader">
				Co-Borrower
			</div>
			<div class="itemSection">
				<div id="CoEmployerNameDiv">
					<label for="CoEmployerName">Co-Borrower Employer's Name:</label><br/>
					<input type="text" id="CoEmployerName" name="CoEmployerName" style="width: 190px" value="<?= $lead->CoEmployerName; ?>"/>
					<span class="print_only"><?= $lead->CoEmployerName ?></span>
				</div>
				<div id="CoTimeOnJobDiv">
					<label for="CoTimeOnJob">Time On Job:</label>
					<input type="text" id="CoTimeOnJob" name="CoTimeOnJob" style="width: 125px" value="<?= $lead->CoTimeOnJob; ?>" />
					<span class="print_only"><?= $lead->CoTimeOnJob ?></span>
				</div>
			</div>
			<div class="itemSection">
				<div id="CoEmployerAddressDiv">
					<label for="CoEmployerAddress">Employer Address:</label><br/>
					<input type="text" id="CoEmployerAddress" name="CoEmployerAddress" style="width: 320px" value="<?= $lead->CoEmployerAddress; ?>"/>
					<span class="print_only"><?= $lead->CoEmployerAddress ?></span>
				</div>
			</div>
			<div class="itemSection">
				<div id="CoEmployerCityDiv">
					<label for="CoEmployerCity">City:</label><br/>
					<input type="text" id="CoEmployerCity" name="CoEmployerCity" style="width: 165px" value="<?= $lead->CoEmployerCity; ?>"/>
					<span class="print_only"><?= $lead->CoEmployerCity ?></span>
				</div>
				<div id="CoEmployerStateDiv">
					<label for="CoEmployerState">ST:</label><br/>
					<input type="text" id="CoEmployerState" name="CoEmployerState" style="width: 35px" value="<?= $lead->CoEmployerState; ?>"/>
					<span class="print_only"><?= $lead->CoEmployerState ?></span>
				</div>
				<div id="CoEmployerZipDiv">
					<label for="CoEmployerZip">Zip:</label><br/>
					<input type="text" id="CoEmployerZip" name="CoEmployerZip" style="width: 100px" value="<?= $lead->CoEmployerZip; ?>"/>
					<span class="print_only"><?= $lead->CoEmployerZip ?></span>
				</div>
			</div>
			<div class="itemSection">
				<div id="CoOccupationDiv">
					<label for="CoOccupation">Occupation:</label><br/>
					<input type="text" id="CoOccupation" name="CoOccupation" style="width: 120px" value="<?= $lead->CoOccupation; ?>"/>
					<span class="print_only"><?= $lead->CoOccupation ?></span>
				</div>
				<div id="CoBFSDiv">
					<label for="CoBFS">BFS:</label><br/>
					<label class="radio_label <?= $lead->CoBFS == '1' ? '' : 'no_print'; ?>">Yes</label><input type="radio" name="CoBFS" id="CoBFS_Yes" value="1" <?= $lead->CoBFS == '1' ? 'CHECKED' : ''; ?> />
					<label class="radio_label <?= $lead->CoBFS == '0' ? '' : 'no_print'; ?>">No</label><input type="radio" name="CoBFS" id="CoBFS_No" value="0" <?= $lead->CoBFS == '0' ? 'CHECKED' : ''; ?> />
				</div>
				<div id="CoWorkPhoneDiv">
					<label for="CoWorkPhone">Work Phone: (<a href="#" onclick="wopen('trigger_call.php?id=<?= $lead->SubmissionListingID; ?>&field=CoWorkPhone', 'add_action', 350, 285); return false;">call</a>)</label><br/>
					<input type="text" id="CoWorkPhone" name="CoWorkPhone" style="width: 100px" value="<?= $lead->CoWorkPhone; ?>"/>
					<span class="print_only"><?= $lead->CoWorkPhone ?></span>
				</div>
			</div>
		</div>
	</div>
	<div class="sectionHeader">
		V. Monthly Income and Combined Housing Expenses 
	</div>
	<div id="Section5Div" class="sectionDiv">
		<div id="IncomeDiv">
			<div class="itemSection itemHeader">
				Income
			</div>
			<div class="lineItem">Borrower GMI</div>
			<input type="text" id="GMI" name="GMI" style="width: 120px" value="<?= $lead->GMI ? $lead->GMI : ""; ?>"/>
			<span class="print_only"><?= $lead->GMI ? $lead->GMI : ""; ?></span><br/>
			<div class="lineItem">Co-Borrower GMI</div>
			<input type="text" id="CoGMI" name="CoGMI" style="width: 120px" value="<?= $lead->CoGMI ? $lead->CoGMI : ""; ?>"/>
			<span class="print_only"><?= $lead->CoGMI ? $lead->CoGMI : ""; ?></span>
		</div>
		<div id="ExpenseDiv">
			<div class="itemSection itemHeader">
				Expenses
			</div>
			<div class="lineItem">First Mortgage</div>
			<input type="text" id="Mortgage1Payment" name="Mortgage1Payment" style="width: 120px" value="<?= $lead->Mortgage1Payment ? $lead->Mortgage1Payment : ""; ?>"/>
			<span class="print_only mortgageSectionSpan"><?= $lead->Mortgage1Payment ? $lead->Mortgage1Payment : ""; ?></span><br/>
			<div class="lineItem">Second Mortgage</div>
			<input type="text" id="Mortgage2Payment" name="Mortgage2Payment" style="width: 120px" value="<?= $lead->Mortgage2Payment ? $lead->Mortgage2Payment : ""; ?>"/>
			<span class="print_only mortgageSectionSpan"><?= $lead->Mortgage2Payment ? $lead->Mortgage2Payment : ""; ?></span><br/>
			<div class="lineItem">Property Taxes</div>
			<input type="text" id="PropertyTaxes" name="PropertyTaxes" style="width: 120px" value="<?= $lead->PropertyTaxes ? $lead->PropertyTaxes : ""; ?>"/>
			<span class="print_only"><?= $lead->PropertyTaxes ? $lead->PropertyTaxes : ""; ?></span><br/>
			<div class="lineItem">Annual Insurance</div>
			<input type="text" id="AnnualInsurance" name="AnnualInsurance" style="width: 120px" value="<?= $lead->AnnualInsurance ? $lead->AnnualInsurance : ""; ?>"/>
			<span class="print_only"><?= $lead->AnnualInsurance ? $lead->AnnualInsurance : ""; ?></span><br/>
			<div class="lineItem">HOA Dues</div>
			<input type="text" id="HOADues" name="HOADues" style="width: 120px" value="<?= $lead->HOADues ? $lead->HOADues : ""; ?>"/>
			<span class="print_only"><?= $lead->HOADues ? $lead->HOADues : ""; ?></span>
		</div>
	</div>
	<div class="sectionHeader">
		VI. Assests and Liabilities 
	</div>
	<div id="Section6Div" class="sectionDiv">
		<div class="itemSection">
			<div id="DownPaymentDiv">
				<label for="DownPayment">Down Payment:</label><br/>
				<input type="text" id="DownPayment" name="DownPayment" style="width: 100px" value="<?= $lead->DownPayment ? $lead->DownPayment : ""; ?>"/>
				<span class="print_only"><?= $lead->DownPayment ? $lead->DownPayment : ""; ?></span>
			</div>
			<div id="AssetsAmountDiv">
				<label for="AssetsAmount">Assets Amount:</label><br/>
				<input type="text" id="AssetsAmount" name="AssetsAmount" style="width: 100px" value="<?= $lead->AssetsAmount ? $lead->AssetsAmount : ""; ?>"/>
				<span class="print_only"><?= $lead->AssetsAmount ? $lead->AssetsAmount : ""; ?></span>
			</div>
		</div>
		<div class="longItemSection">
			<div class="mortgageSection">
				<div>
					1st Mortgage
				</div>
				<div>
					<label class="mortgageSectionLabel" for="Mortgage1Lender">Lender</label> 
					<input type="text" id="Mortgage1Lender" name="Mortgage1Lender" style="width: 120px" value="<?= $lead->Mortgage1Lender; ?>"/>
					<span class="print_only mortgageSectionSpan"><?= $lead->Mortgage1Lender ?></span>
				</div>
				<div>
					<label class="mortgageSectionLabel" for="Mortgage1Balance">Balance</label> 
					<input type="text" id="Mortgage1Balance" name="Mortgage1Balance" style="width: 120px" value="<?= $lead->Mortgage1Balance ? $lead->Mortgage1Balance : ""; ?>"/>
					<span class="print_only mortgageSectionSpan"><?= $lead->Mortgage1Balance ? $lead->Mortgage1Balance : ""; ?></span>
				</div>
				<div>
					<label class="mortgageSectionLabel" for="Mortgage1Rate">Rate</label> 
					<input type="text" id="Mortgage1Rate" name="Mortgage1Rate" style="width: 120px" value="<?= $lead->Mortgage1Rate ? $lead->Mortgage1Rate : ""; ?>"/>
					<span class="print_only mortgageSectionSpan"><?= $lead->Mortgage1Rate ? $lead->Mortgage1Rate : ""; ?></span>
				</div>
				<div>
					<label class="mortgageSectionLabel" for="Mortgage1Term">Term</label> 
					<input type="text" id="Mortgage1Term" name="Mortgage1Term" style="width: 120px" value="<?= $lead->Mortgage1Term; ?>"/>
					<span class="print_only mortgageSectionSpan"><?= $lead->Mortgage1Term ?></span>
				</div>
			</div>
			<div class="mortgageSection">
				<div>
					2nd Mortgage
				</div>
				<div>
					<label class="mortgageSectionLabel" for="Mortgage2Lender">Lender</label> 
					<input type="text" id="Mortgage2Lender" name="Mortgage2Lender" style="width: 120px" value="<?= $lead->Mortgage2Lender; ?>"/>
					<span class="print_only mortgageSectionSpan"><?= $lead->Mortgage2Lender ?></span>
				</div>
				<div>
					<label class="mortgageSectionLabel" for="Mortgage2Balance">Balance</label> 
					<input type="text" id="Mortgage2Balance" name="Mortgage2Balance" style="width: 120px" value="<?= $lead->Mortgage2Balance ? $lead->Mortgage2Balance : ""; ?>"/>
					<span class="print_only mortgageSectionSpan"><?= $lead->Mortgage2Balance ? $lead->Mortgage2Balance : ""; ?></span>
				</div>
				<div>
					<label class="mortgageSectionLabel" for="Mortgage2Rate">Rate</label> 
					<input type="text" id="Mortgage2Rate" name="Mortgage2Rate" style="width: 120px" value="<?= $lead->Mortgage2Rate ? $lead->Mortgage2Rate : ""; ?>"/>
					<span class="print_only mortgageSectionSpan"><?= $lead->Mortgage2Rate ? $lead->Mortgage2Rate : ""; ?></span>
				</div>
				<div>
					<label class="mortgageSectionLabel" for="Mortgage2Term">Term</label> 
					<input type="text" id="Mortgage2Term" name="Mortgage2Term" style="width: 120px" value="<?= $lead->Mortgage2Term; ?>"/>
					<span class="print_only mortgageSectionSpan"><?= $lead->Mortgage2Term ?></span>
				</div>
			</div>
			<div class="debtSection">
				<div>
					Other Current Debt
				</div>
				<div>
					<label class="debtSectionLabel" for="OtherDebt1Payment">Payment</label> 
					<input type="text" id="OtherDebt1Payment" name="OtherDebt1Payment" style="width: 80px" value="<?= $lead->OtherDebt1Payment ? $lead->OtherDebt1Payment : ""; ?>"/>
					<span class="print_only debtSectionSpan"><?= $lead->OtherDebt1Payment ? $lead->OtherDebt1Payment : ""; ?></span>
					<label class="debtSectionLabel" for="OtherDebt1Balance">Balance</label> 
					<input type="text" id="OtherDebt1Balance" name="OtherDebt1Balance" style="width: 80px" value="<?= $lead->OtherDebt1Balance ? $lead->OtherDebt1Balance : ""; ?>"/>
					<span class="print_only debtSectionSpan"><?= $lead->OtherDebt1Balance ? $lead->OtherDebt1Balance : ""; ?></span>
				</div>
				<div>
					<label class="debtSectionLabel" for="OtherDebt2Payment">Payment</label> 
					<input type="text" id="OtherDebt2Payment" name="OtherDebt2Payment" style="width: 80px" value="<?= $lead->OtherDebt2Payment ? $lead->OtherDebt2Payment : ""; ?>"/>
					<span class="print_only debtSectionSpan"><?= $lead->OtherDebt2Payment ? $lead->OtherDebt2Payment : ""; ?></span>
					<label class="debtSectionLabel" for="OtherDebt2Balance">Balance</label> 
					<input type="text" id="OtherDebt2Balance" name="OtherDebt2Balance" style="width: 80px" value="<?= $lead->OtherDebt2Balance ? $lead->OtherDebt2Balance : ""; ?>"/>
					<span class="print_only debtSectionSpan"><?= $lead->OtherDebt2Balance ? $lead->OtherDebt2Balance : ""; ?></span>
				</div>
				<div>
					<label class="debtSectionLabel" for="OtherDebt3Payment">Payment</label> 
					<input type="text" id="OtherDebt3Payment" name="OtherDebt3Payment" style="width: 80px" value="<?= $lead->OtherDebt3Payment ? $lead->OtherDebt3Payment : ""; ?>"/>
					<span class="print_only debtSectionSpan"><?= $lead->OtherDebt3Payment ? $lead->OtherDebt3Payment : ""; ?></span>
					<label class="debtSectionLabel" for="OtherDebt3Balance">Balance</label> 
					<input type="text" id="OtherDebt3Balance" name="OtherDebt3Balance" style="width: 80px" value="<?= $lead->OtherDebt3Balance ? $lead->OtherDebt3Balance : ""; ?>"/>
					<span class="print_only debtSectionSpan"><?= $lead->OtherDebt3Balance ? $lead->OtherDebt3Balance : ""; ?></span>
				</div>
				<div>
					<div id="FormerBankruptcyDiv">
						<label for="FormerBankruptcy">Former Bankruptcy:</label><br/>
						<label class="radio_label <?= $lead->FormerBankruptcy == '1' ? '' : 'no_print'; ?>">Yes</label><input type="radio" name="FormerBankruptcy" id="FormerBankruptcy_Yes" value="1" <?= $lead->FormerBankruptcy == '1' ? 'CHECKED' : ''; ?> />
						<label class="radio_label <?= $lead->FormerBankruptcy == '0' ? '' : 'no_print'; ?>">No</label><input type="radio" name="FormerBankruptcy" id="FormerBankruptcy_No" value="0" <?= $lead->FormerBankruptcy == '0' ? 'CHECKED' : ''; ?> />
					</div>
					<div id="BankruptcyTypeDiv">
						<label for="BankruptcyType">Chapter:</label><br/>
						<select name="BankruptcyType" id="BankruptcyType">
							<option value=""></option>
							<option value="11" <?= $lead->BankruptcyType == '11' ? 'SELECTED' : ''; ?>>11</option>
							<option value="7" <?= $lead->BankruptcyType == '7' ? 'SELECTED' : ''; ?>>7</option>
							<option value="Foreclosure" <?= $lead->BankruptcyType == 'Foreclosure' ? 'SELECTED' : ''; ?>>Foreclosure</option>
						</select>
					<span class="print_only debtSectionSpan"><?= $lead->BankruptcyType ?></span>
					</div>
				</div>
				<div>
					<div id="BankruptcyYearDiv">
						<label for="BankruptcyYear">Year Filed:</label><br/>
						<input type="text" id="BankruptcyYear" name="BankruptcyYear" style="width: 90px" value="<?= $lead->BankruptcyYear ? $lead->BankruptcyYear : ""; ?>"/>
						<span class="print_only debtSectionSpan"><?= $lead->BankruptcyYear ? $lead->BankruptcyYear : ""; ?></span>
					</div>
					<div id="JudgementsDiv">
						<label for="Judgements">Judgements or Liens:</label><br/>
						<label class="radio_label <?= $lead->Judgements == '1' ? '' : 'no_print'; ?>">Yes</label><input type="radio" name="Judgements" id="Judgements_Yes" value="1" <?= $lead->Judgements == '1' ? 'CHECKED' : ''; ?> />
						<label class="radio_label <?= $lead->Judgements == '0' ? '' : 'no_print'; ?>">No</label><input type="radio" name="Judgements" id="Judgements_No" value="0" <?= $lead->Judgements == '0' ? 'CHECKED' : ''; ?> />
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="sectionHeader">
		Misc Info
	</div>
	<div id="SectionXDiv" class="sectionDiv">
		<div class="itemSection">
			<div id="NeedPurchaseRealtorDiv">
				<label for="NeedPurchaseRealtor">Need Purchase Realtor:</label><br/>
				<label class="radio_label <?= $lead->NeedPurchaseRealtor == '1' ? '' : 'no_print'; ?>">Yes</label><input type="radio" name="NeedPurchaseRealtor" id="NeedPurchaseRealtor_Yes" value="1" <?= $lead->NeedPurchaseRealtor == '1' ? 'CHECKED' : ''; ?> />
				<label class="radio_label <?= $lead->NeedPurchaseRealtor == '0' ? '' : 'no_print'; ?>">No</label><input type="radio" name="NeedPurchaseRealtor" id="NeedPurchaseRealtor_No" value="0" <?= $lead->NeedPurchaseRealtor == '0' ? 'CHECKED' : ''; ?> />
			</div>
			<div id="RealtorNameDiv">
				<label for="RealtorName">Realtor Name:</label><br/>
				<input type="text" id="RealtorName" name="RealtorName" style="width: 100px" value="<?= $lead->RealtorName; ?>"/>
				<span class="print_only"><?= $lead->RealtorName ?></span>
			</div>
			<div id="RealtorPhoneDiv">
				<label for="RealtorPhone">Realtor Phone:</label><br/>
				<input type="text" id="RealtorPhone" name="RealtorPhone" style="width: 100px" value="<?= $lead->RealtorPhone; ?>"/>
				<span class="print_only"><?= $lead->RealtorPhone ?></span>
			</div>
			<div id="NeedSellRealtor">
				<label for="NeedSellRealtor">Need Sell Realtor:</label><br/>
				<label class="radio_label <?= $lead->NeedSellRealtor == '1' ? '' : 'no_print'; ?>">Yes</label><input type="radio" name="NeedSellRealtor" id="NeedSellRealtor_Yes" value="1" <?= $lead->NeedSellRealtor == '1' ? 'CHECKED' : ''; ?> />
				<label class="radio_label <?= $lead->NeedSellRealtor == '0' ? '' : 'no_print'; ?>">No</label><input type="radio" name="NeedSellRealtor" id="NeedSellRealtor_No" value="0" <?= $lead->NeedSellRealtor == '0' ? 'CHECKED' : ''; ?> />
			</div>
		</div>
	</div>
	<div class="sectionHeader">
		Notes
	</div>
	<div id="CommentsDiv" class="sectionDiv">
		<textarea id="Comments" name="Comments" class="no_print"><?= $lead->Comments ?></textarea>
		<span class="print_only"><?= $lead->Comments ?></span>
	</div>
	<div class="subSectionHeader">
		History
	</div>
	<div id="HistoryDiv" class="sectionDiv">
		<div id="EventTypeDiv">
			<table class="event_table" >
				<tr class="event_table rowheader" >
					<th class="event_table">
						Event Type
					</th>
					<th class="event_table">
						Event Time
					</th>
					<th class="event_table">
						Notes
					</th>
					<th class="event_table">
						User
					</th>
				</tr>
<?php
	$i = 0;
	foreach($allEvents as $event)
	{
		$i++;
?>
				<tr class="event_table<?= $i%2==0 ? ' roweven' : ' rowodd' ?>">
					<td class="event_table">
						<?= $event->EventDescription ?>
					</td>
					<td class="event_table">
						<?= strtotime($event->EventDate) ? date("m/d/Y g:i:s a", strtotime($event->EventDate)) : ""; ?>
					</td>
					<td class="event_table">
						<?= $event->EventNotes ?>
					</td>
					<td class="event_table">
						<?= $event->UserFullname ?>
					</td>
				</tr>
<?php	
	}
?>
			</table>
		</div>
	<center>
		<div class="itemSection">
			<div class="buttonSection">
				<input type="submit" value="Update" name="Submit"/>&nbsp;&nbsp;
<?php
	if($isSubAdmin)
	{
?>
				<input type="submit" value="Delete" name="Submit" onclick="return confirm('Are you sure you want to delete this lead?')"/>&nbsp;&nbsp;
<?php
	}
?>
				<input type="submit" value="Cancel" name="Submit"/>
			</div>
		</div>
	</center>
	</div>
</div>
</div>	
</form>
</body>
</html>