<?php
require("../printable/include/mysql.inc.php");
require("../printable/include/optimize.printable.inc.php");
require("globals.php");

$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);

$portal = new OptimizePortal($COMPANY_ID, $db);

$currentUser = $portal->UserAccess($_SESSION['currentuserid']);
$isSuper = $portal->CheckPriv($currentUser->UserID, 'supervisor');

if (!$portal->CheckPriv($currentUser->UserID, 'subadmin')) {
    header("Location: " . $portal->CurrentCompany->Website . "home.php?message=" . urlencode("Accessed Denied."));
    die();
}

if (!isset($_GET['groupid']) || $_GET['groupid'] == 0) {
    $gid = 'All';
} else {
    $gid = $_GET['groupid'];
}

// Select all messages for this company
$messages = $portal->GetCompanyMessages($currentUser->GroupID == 0 ? $gid : $currentUser->GroupID);

$gs = $portal->GetCompanyGroups();

$groups = array();

$groups[0] = new Group();
$groups[0]->GroupID = 0;
$groups[0]->GroupName = "All";

foreach ($gs as $g) {
    $groups[$g->GroupID] = $g;
}

$messageNames = $portal->GetCompanyMessageNames();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <title>
            <?= $portal->CurrentCompany->CompanyName ?> :: Manage Custom Messages
        </title>
        <meta http-equiv="content-type" content="text/html;charset=utf-8" />

        <?php include("components/bootstrap.php") ?>

    </head>
    <body bgcolor="#FFFFFF">
        <div id="page">
            <?php include("components/header.php") ?>
            <div id="body">
                <?php
                $CURRENT_PAGE = "Home";
                include("components/navbar.php");
                ?>

                <?php if (isset($_GET['message'])): ?>
                    <div class="container">
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <?= $_GET['message']; ?>
                        </div>
                    </div>
                <?php endif; ?> 

                <div id="controlPanelContainer" class="row">
                    <div id="folderTreeContainer" class="col-md-offset-2 col-md-2 panel panel-default">
                        <?php include("components/controlpanel_tree.php"); ?>
                    </div>

                    <div class="sectionHeader col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Control Panel - Custom Messages</h3>
                            </div>
                            <div class="panel-body">
                                <div id="detailContainer">
                                    <div id="actionBar" class="row">
                                        <form method="get" action="<?= $_SERVER['PHP_SELF']; ?>"> 
                                            <input type="hidden" name="filter" value="1" />
                                            <div>
                                                <?php
                                                if ($portal->CheckPriv($currentUser->UserID, 'admin')) {
                                                    ?>
                                                    <a href="add_message.php" class="btn btn-default btn-sm actionButton">Add Message</a>
                                                    <?php
                                                }
                                                ?>
                                                <span class="actionButtonRight">
                                                    Filter:
                                                    <select name="message" class="input-sm form-submenu" onchange="this.form.submit();">
                                                        <option value="all">All Messages</option>
                                                        <?php
                                                        foreach ($messageNames as $m) {
                                                            ?>
                                                            <option value="<?= $m ?>" <?= $m == $_GET['message'] ? 'SELECTED' : '' ?>><?= $m ?></option>
                                                            <?php
                                                        }
                                                        ?>
                                                    </select>
                                                    <?php
                                                    if ($currentUser->GroupID == 0) {
                                                        ?>
                                                        <select name="groupid" onchange="this.form.submit();">
                                                            <?php
                                                            foreach ($groups as $g) {
                                                                ?>
                                                                <option value="<?= $g->GroupID ?>" <?= $g->GroupID == $_GET['groupid'] ? 'SELECTED' : '' ?>><?= $g->GroupName ?></option>
                                                                <?php
                                                            }
                                                            ?>
                                                        </select>
                                                        <?php
                                                    }
                                                    ?>
                                                </span>
                                            </div>
                                        </form>	
                                    </div>
                                    <div class="table-responsive row">
                                        <table width="100%" class="message_table table table-striped table-hover table-bordered">
                                            <tr class="message_table rowheader">
                                                <td class="message_table">
                                                    ID
                                                </td>
                                                <td class="message_table">
                                                    Message Name
                                                </td>
                                                <td class="message_table">
                                                    Group
                                                </td>
                                                <td class="message_table">
                                                    Edit
                                                </td>
                                            </tr>
                                            <?php
                                            $message = new Message();
                                            foreach ($messages as $message) {
                                                if (($portal->CheckPriv($currentUser->UserID, 'admin') || $message->GroupID != 0) &&
                                                        (!isset($_GET['filter']) || ($_GET['message'] == 'all' || $_GET['message'] == $message->MessageName))) {
                                                    ?>
                                                    <tr class="message_table">
                                                        <td align="left" width="15" class="message_table">
                                                            <?= $message->MessageID ?>
                                                        </td>
                                                        <td align="left" width="120" class="message_table">
                                                            <?= $message->MessageName ?>
                                                        </td>
                                                        <td align="left" width="120" class="message_table">
                                                            <?= $groups[$message->GroupID]->GroupName ?>
                                                        </td>
                                                        <td align="left" width="60" class="message_table">
                                                            <a class="blueLink" href="edit_message.php?messageid=<?= $message->MessageID ?>"><span class="glyphicon glyphicon-pencil"></span> edit</a>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

            </div>

            <?php include("components/footer.php") ?>

    </body>
</html>