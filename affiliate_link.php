<?php
	require("../printable/include/mysql.inc.php");
	require("../printable/include/optimize.printable.inc.php");
	require("globals.php");
	
	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	
	$portal = new OptimizePortal($COMPANY_ID, $db);
	
	$currentUser = $portal->GetUser($_SESSION['currentuserid']);
		
	if(!$currentUser)
	{
		header("Location: login.php?message=" .urlencode("Not logged in or login error."));
		die();
	}
	
	if($portal->CheckPriv($currentUser->UserID, 'subadmin'))
	{
		$userString = "all";
	}
	else 
	{
		$userString = $currentUser->UserID;
	}
	
	$lead = $portal->GetOptimizeContact($_GET['contactid'], $userString);
		
	if(!$lead)
	{
		die("Invalid ID");
	}
	
	if($_POST['Submit'] == 'Create Link') {
		$portal->CreateAffiliateLink($lead->SubmissionListingID, $_POST['AffiliateID'], $_POST['IncomingAffiliate']);
		
		echo "<html><body onload='window.close()'></body></html>";
		die();
	}	
  	
  	$categoryValues = $portal->GetLookupValues('Category', $currentUser->GroupID);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
	<title>
		<?= $portal->CurrentCompany->CompanyName ?> :: Affiliate Referral
	</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />		
	<link type="text/css" media="all" rel="stylesheet" href="style.css" />	
	<link type="text/css" media="all" rel="stylesheet" href="css/affiliate_link.css" />
	<script src="js/jscalendar/js/jscal2.js"></script>
	<script src="js/jscalendar/js/lang/en.js"></script>
	<link rel="stylesheet" type="text/css" href="js/jscalendar/css/jscal2.css" />
	<link rel="stylesheet" type="text/css" href="js/jscalendar/css/steel/steel.css" />
<script language="JavaScript" type="text/JavaScript">
		var search_transactionURL = "printable_trans.php";
		var searchRequest;

		function searchAffiliates()
		{
			document.getElementById("affiliateTableDiv").innerHTML = "<img src='images/loading.gif' /><br/>Searching...";	

			searchRequest=GetXmlHttpObject();
			if (searchRequest==null)
			{
				alert ("Your browser does not support AJAX!");
				return;
			} 
			var url=search_transactionURL;
			url=url+"?action=GetAffiliateTable";
			url=url+"&filterCategory="+encodeURI(document.getElementById("filterCategory").value);
			url=url+"&filterSearchValue="+encodeURI(document.getElementById("filterSearchValue").value);
			url=url+"&filterSearchField="+encodeURI(document.getElementById("filterSearchField").value);
			url=url+"&sid="+Math.random();
			searchRequest.onreadystatechange=searchRequestStateChanged;
			searchRequest.open("GET",url,true);
			searchRequest.send(null);
		}

		function searchRequestStateChanged()
		{
			if (searchRequest.readyState==4)
			{
				document.getElementById("affiliateTableDiv").innerHTML = searchRequest.responseText;
			}
		}
	</script>	
<script type="text/javascript">//<![CDATA[

      var cal = Calendar.setup({
          onSelect: function(cal) { cal.hide() },
          bottomBar: true,
          dateFormat : "%m/%d/%Y"
      });

//]]></script>	
	<script  src="js/func.js"></script>	
</head>
<body onload="searchAffiliates()">
<form method="post" action="<?= $_SERVER['PHP_SELF'] . "?contactid=$lead->SubmissionListingID"; ?>">
	<div class="sectionHeader">
		New Referral
	</div>
	<div id="AffiliateLinkDiv" class="sectionDiv">
		<div class="itemSection">
			<div id="ContactNameDiv">
				<label for="ContactName">Lead Name:</label><br/>
				<?= "$lead->FirstName $lead->LastName" ?>
			</div>
		</div>
		<div class="itemSection">
			<div id="IncomingAffiliateDiv">
				<label for="IncomingAffiliate">Referral Type:</label><br/>
					<select name="IncomingAffiliate" id="OpportunityStageID">
						<option value="1">From Affiliate</option>
						<option value="0">To Affiliate</option>
					</select>
			</div>
		</div>
		<div class="subSectionHeader">
			Choose Affiliate
		</div>
		<div id="filterSection">
			<div id="filterCategoryDiv">
				<select name="filterCategory" id="filterCategory">
					<option value="all">- All Categories -</option>
<?php
	foreach($categoryValues as $val) {
?>
					<option value="<?= $val->LookupValue ?>"><?= $val->LookupLabel ?></option>
<?php 
	} 
?>
				</select>
			</div>
			<div id="filterSearchDiv">
				<input type="text" name="filterSearchValue" id="filterSearchValue" />
				<select name="filterSearchField" id="filterSearchField">
					<option value="FirstName">First Name</option>
					<option value="LastName">Last Name</option>
					<option value="City">City</option>
					<option value="Zip">Zip</option>
					<option value="Phone">Phone</option>
				</select>
				<input type="button" name="filterGo" value="Go" onclick="searchAffiliates()" />
			</div>
		</div>
		<div id="affiliateTableDiv">			
		</div>
	</div>	
	<div class="buttonDiv">
		<div class="buttonSection">
			<input type="submit" value="Create Link" name="Submit" id="Submit"/>&nbsp;&nbsp;<input type="button" value="Cancel" onclick="window.close()" />
		</div>
	</div>
</form>
</body>
</html>