<?php
	require("../printable/include/mysql.inc.php");
	require("../printable/include/optimize.printable.inc.php");
	require("globals.php");
	
	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	
	$portal = new OptimizePortal($COMPANY_ID, $db);
		
	$currentUser = $portal->UserAccess($_SESSION['currentuserid']);
	
	// Check login
	if( !$currentUser ) 
	{
		header( "Location: login.php?message=" . urlencode( "Not logged in or login error.  Please try again." ) );
		die();
  	}
	
		
	$lead = $portal->GetOptimizeContact($_GET['slid'], $currentUser->UserID);
	
	if(!$lead) {
		die("Invalid Lead/Contact");
	}
  	
  	$_GET['section'] = intval($_GET['section']);
	  	
  	// Get Cats
  	$rootCats = $portal->GetCategories('root', $_GET['section'], true, $currentUser->GroupID);
  	
  	$cats = array();
  	$cat = new FileCategory();  	
  	foreach($rootCats as $cat)
  	{
  		$cats[] = $portal->GetCategory($cat->CategoryID, true);
  	} 	
  	  	 				
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
	<title>
		<?= $portal->CurrentCompany->CompanyName ?> :: Email File
	</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />		
	<link rel="stylesheet" href="include/foldertree/css/folder-tree-static.css" type="text/css"/>
	<link rel="stylesheet" href="include/foldertree/css/context-menu.css" type="text/css"/>
	<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />
	<script type="text/javascript" src="include/foldertree/js/ajax.js"></script>
	<script type="text/javascript" src="include/foldertree/js/folder-tree-static.js"></script>
	<script type="text/javascript" src="include/foldertree/js/context-menu.js"></script>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />	
	<script  src="js/func.js"></script>
	<script  src="js/email_public.js"></script>
<script language="JavaScript" type="text/JavaScript">

var transactionURL = "printable_trans.php";
var filesRequest;
var nCategoryID;

function loadTipImage(fileID)
{
	document.getElementById("toolTipBody").innerHTML = "<img src='loadthumb.php?fileid=" + fileID + "&fitwidth=200'>";
}

function loadFiles(categoryID)
{
	nCategoryID = categoryID;
	document.getElementById("filelist").innerHTML = '<div style="text-align: center; padding-top: 100px;"><img src="images/loading.gif" /><br/>Loading ...</div>';
	
	filesRequest=GetXmlHttpObject()
	if (filesRequest==null)
	{
		alert ("Your browser does not support AJAX!");
		return;
	} 
	var url=transactionURL;
	url=url+"?action=GetFileTable&categoryid=" + categoryID + "&sortBy=" + document.getElementById('sort_by').value;
	url=url+"&PublicOnly=true";
	url=url+"&sid="+Math.random();
	filesRequest.onreadystatechange=filesRequestStateChanged;
	filesRequest.open("GET",url,true);
	filesRequest.send(null);	
}

function filesRequestStateChanged()
{
	if (filesRequest.readyState==4)
	{
		document.getElementById("filelist").innerHTML = filesRequest.responseText;
	}	
}

function EmailPublicContent(fileID, fileLink, emailSubject, emailBody) {
	TriggerPublicContentEmail("<?= $lead->Email; ?>", <?= intval($lead->SubmissionListingID); ?>, fileID, fileLink, emailSubject, emailBody);
}
</script>	
	<link type="text/css" media="all" rel="stylesheet" href="style.css" />
	<link type="text/css" media="all" rel="stylesheet" href="css/download_file.css" />
</head>
<body bgcolor="#FFFFFF">
<div id="page">
<div id="body">
	<div class="error"><?= $_GET['message']; ?></div>
	<div id="fileContainer">
		<div style="text-align:right; width:95%;">
<?php
	$aSortOptions = array('Description' => 'Description', 'LastEdit' => 'Last Update');
?>
			<select name="sort_by" id="sort_by" onchange="loadFiles(nCategoryID)">
				<option value="">Sort By</option>
<?php 
	foreach($aSortOptions as $sField => $sTitle) 
	{ 
?>
			<option value="<?php echo $sField; ?>" <?php if($_GET['sort_by'] == $sField) echo "SELECTED";?>><?php echo $sTitle; ?></option>
<?php 
	} 
?>
			</select>
		</div>
		<div id="folderTreeContainer">
			<ul id="dhtmlgoodies_tree" class="dhtmlgoodies_tree">
<?php
	$fc = new FileCategory();
	foreach($cats as $fc)
	{
		echo $fc->printList();
	}
?>
			</ul>
		</div>
		<div id="filelistContainer">
			<div id="filelist">
		  	</div>					
		</div>
	</div>
</div>
</div>
<div id="debug_div" style="display: none"></div>
<div id="tipDiv" style="position:absolute; visibility:hidden; z-index:100"></div>
<script language="JavaScript" type="text/JavaScript">
<?= isset($_GET['category']) ? "loadFiles('" . $_GET['category'] . "');" : "" ?>
</script>
</body>
</html>