<?php
require("../printable/include/mysql.inc.php");
require("../printable/include/optimize.printable.inc.php");
require("globals.php");

$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);

$portal = new OptimizePortal($COMPANY_ID, $db);

$currentUser = $portal->GetUser($_SESSION['currentuserid']);

if (!$currentUser) {
    header("Location: login.php?message=" . urlencode("Not logged in or login error."));
    die();
}

if ($portal->CheckPriv($currentUser->UserID, 'subadmin')) {
    $userString = "all";
} else {
    $userString = $currentUser->UserID;
}


if (isset($_GET['contactID'])) {
    echo $_GET['contactID'];
    //header( "Location: view_appointment.php?contactID=$_GET['contactID']");
    //die();
}

// Set last Notifty
$appt->LastNotifyTime = date("Y-m-d H:i:s");
$portal->UpdateAppointment($appt);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <title>
<?= $portal->CurrentCompany->CompanyName ?> Marketing on Demand :: <?= "$currentUser->LastName's Appointments" ?>
        </title>
        <meta http-equiv="content-type" content="text/html;charset=utf-8" />		
        <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />
        <script src="js/func.js"></script>	

<?php include("components/bootstrap.php") ?>

    </head>
    <body onload="<?= $close == true ? "window.close();" : ($error == 1 ? "alert('$errorText')" : "" ) ?>">
        <?php
        $currentUser = $portal->GetUser($_SESSION['currentuserid']);
        $appt = $portal->GetAppointments($currentUser->UserID);
        ?>
        <div class="sectionHeader"><h3><?= "$currentUser->FirstName $currentUser->LastName's " ?> Appointments</h3></div>
        <div id="PopAppointmentDiv" class="sectionDiv well">
            <b><center>Click on an Appointment Time to edit the Appointment</center></b>
            <table cellpadding="5" width="100%" class="table table-striped table-hover table-bordered">
                <tr class="colHead" size="2em">
                    <td>Contact Name</td>
                    <td>Company</td>
                    <td>Appointment Time</td>
                    <td>Delete</td>
                </tr><?php
        $i = 0;
        foreach ($appt as $a) {
            $i++;

            $currentContact = $portal->GetOptimizeContact($a->ContactID, $currentUser->UserID);

            ($a->PastDue) ? $pastdue='color: red;' : $pastdue='';
            $aptime = date('n/j/Y g:i a', strtotime($a->AppointmentTime));
            ?>
                    <tr class="itemSection<?= $i % 2 == 0 ? 'even' : 'odd' ?>">
                        <td>
                            <a href="view_appointment.php?contactID=<?= $currentContact->SubmissionListingID . '">' . $currentContact->FirstName . " " . $currentContact->LastName ?></a>
                               </td>
                               <td>
                    <?= $currentContact->Company ?>
                               </td>
                               <td>
                               <a style="<?= $pastdue ?>" href = "#" onclick="wopen('edit_appointment.php?id=<?= $a->ContactAppointmentID ?>', 'edit', 350, 450);" title="Edit"><?= $aptime ?></a>
                            <a href = "getvcal.php?caid=<?= $a->ContactAppointmentID ?>"><img src="images/outlook-icon.gif" height="15px" width="15px"></a>
                        </td>
                        <td>
                            <a href = "delete_appointment.php?id=<?= $a->ContactAppointmentID ?>">Delete</a>
                        </td>
                    </tr>


    <?php
    /* 			echo $c->SubmissionListingID;
      echo "<br/>";
      echo "ConAppID = $a->ContactAppointmentID";
      echo "<br/>";
      echo "ConID = $a->ContactID";
      echo "<br/>";

      echo '<a href="pop_appointment.php?id=';
      echo "$a->ContactAppointmentID";
      echo '">';
      echo "$a->AppointmentTime";
      echo '</a>';
      echo "<br/>"; */
}
?>
            </table>
        </div>

                <?php include("components/bootstrap-footer.php") ?>

    </body>
</html>