<html>
<head>
<title>Test</title>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
		  <link rel="stylesheet" type="text/css" media="all" href="custom_dropdown/jquery.selectbox.css" />
		  <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />
		  <script type="text/javascript" src="custom_dropdown/jquery.selectbox-0.6.1.js"></script>
<script>
this.screenshotPreview = function(){	
	/* CONFIG */
		
		xOffset = 10;
		yOffset = 30;
		
		// these 2 variable determine popup's distance from the cursor
		// you might want to adjust to get the right result
		
	/* END CONFIG */
	$("span.jquery-selectbox-item").hover(function(e){
		this.t = this.title;
		this.title = "";	
		var c = "";
		$("body").append("<p id='screenshot'><img src='images/"+ this.t +"' />"+ c +"</p>");								 
		$("#screenshot")
			.css("top",(e.pageY - xOffset) + "px")
			.css("left",(e.pageX + yOffset) + "px")
			.css("z-index","9999999")
			.fadeIn("fast");
    },
	function(){
		this.title = this.t;	
		$("#screenshot").remove();
    });	
	$("span.jquery-selectbox-item").mousemove(function(e){
		$("#screenshot")
			.css("top",(e.pageY - xOffset) + "px")
			.css("left",(e.pageX + yOffset) + "px");
	});			
};

// starting the script on page load
$(document).ready(function(){
	$("#select_test").selectbox();
	screenshotPreview();
});
</script>
<style>

/*  */

#screenshot{
	position:absolute;
	border:1px solid #ccc;
	background:#333;
	padding:5px;
	display:none;
	color:#fff;
	}

/*  */

</style>

</head>
<body>
<select id="select_test">
	<option value="Test1" title="off_small.png" class="screenshot">Test 1</option>
	<option value="Test2" title="on_small.png" class="screenshot">Test 2</option>
	<option value="Test3" title="on_small3.png" class="screenshot">Test 3</option>
</select>
</body>
</html>