<?php
	require("../printable/include/mysql.inc.php");
	require("../printable/include/carnegie.printable.inc.php");
	require("globals.php");
	
	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	
	$portal = new CarnegiePortal($COMPANY_ID, $db);
	
	$currentUser = $portal->GetUser($_SESSION['currentuserid']);
	
	if($currentUser)
	{
		header("Location: home.php?message=" .urlencode("You are already logged in"));
	}
		
	if(isset($_GET['username']))
	{
		$_POST['username'] = $_GET['username'];
		$_POST['password'] = $_GET['password'];
	}
	
	$userID = $portal->CheckPass($_POST['username'], $_POST['password']);

	if( !$userID ) 
	{
		header( "Location: login.php?message=" . urlencode( "Unknown account or bad password." ) );
		die();
	}
	else 
	{
		$currentUser = $portal->GetUser($userID);
	
		if($currentUser->Disabled > 0) //don't allow login if account is still pending
		{
			header( "Location: login.php?message=" . urlencode( "Account is deactivated." ) );
			die();
		}
		else {
		$_SESSION['currentuserid'] = $currentUser->UserID;
		$_SESSION['currentcompanyid'] = $COMPANY_ID;
		$_SESSION['logout'] = $LOGOUT_STRING;
		$_SESSION['site'] = $portal->CurrentCompany->Website;
		$_SESSION['currentuserid_' . $COMPANY_ID] = "";
		$_SESSION['currentcompanyid_' . $COMPANY_ID] = "";
		$_SESSION['logout_' . $COMPANY_ID] = "";
		$_SESSION['site_' . $COMPANY_ID] = "";
		
		// Set permissions
		$perms = $portal->GetPermissionList();
		foreach($perms as $perm)
		{
			if($portal->CheckPriv($currentUser->UserID, $perm->PermissionName))
			{
				$_SESSION[$perm->PermissionName] = 1;
			}
		}

		$oldAccess = $currentUser->LastAccess;

		// Update last web access
		/*
		$lastAccess = date("Y-m-d H:i:s");
		$currentUser->LastAccess = $lastAccess;
		*/
		
		$portal->StartUserSession($currentUser->UserID);

		$portal->UpdateUserLastAccess($currentUser->UserID); //populate LastAccess column
		
		header( "Location: home.php" );
		die();
		}
		
	}

?>