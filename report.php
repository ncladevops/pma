<?php
require("../printable/include/mysql.inc.php");
require("../printable/include/optimize.printable.inc.php");
require("globals.php");

$reportTypes = array(
    "legacyLeadReport" => array("Lead Status Report" => "reports/lead_status.php",
        "User Lead Report" => "reports/user_lead.php",
        "Bad Lead Report" => "reports/bad_lead.php",
        "All Lead Report" => "reports/all_lead.php",
        "Lead Transfer Report" => "reports/transfer_lead.php",
        "Report by Team" => "reports/team_summary.php",
        "Lead Distribution" => "reports/lead_dist.php",
        "Team Conversion" => "reports/team_conversion.php",
        "Time to Contact" => "reports/contact_time.php"),
    "contactReport" => array("Sales Status Report" => "reports/sales_status.php",
        "All Sales" => "reports/all_sales.php"),
    "legacyMarketReport" => array("User Activity" => "",
        "Product Activity" => "",
        "Social Graph" => "reports/SocialonDemand-SocialGraphs.pdf",
        "Social Engagement" => "reports/SocialonDemand-SocialEngagement.pdf"),
    "customReport" => array());

$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);

$portal = new OptimizePortal($COMPANY_ID, $db);

$currentUser = $portal->UserAccess($_SESSION['currentuserid']);
$isSuper = $portal->CheckPriv($currentUser->UserID, 'supervisor');
$isAdmin = $portal->CheckPriv($currentUser->UserID, 'admin');
$isReport = $portal->CheckPriv($currentUser->UserID, 'report');
$isCorpMan = $portal->CheckPriv($currentUser->UserID, 'corporatemanager');
$isRegMan = $portal->CheckPriv($currentUser->UserID, 'regionmanager');
$isDivMan = $portal->CheckPriv($currentUser->UserID, 'divisionmanager');


if ($isAdmin || $isCorpMan || $isRegMan || $isDivMan) {
    $isMan = true;
} else {
    $isMan = false;
}

/*
  if (!$portal->CheckPriv($currentUser->UserID, 'report')) {
  header("Location: " . $portal->CurrentCompany->Website . "home.php?message=" . urlencode("Accessed Denied."));
  die();
  } */

$userGroup = $portal->GetGroup($currentUser->GroupID);

$ds = $portal->GetDashboards();
$reportDescriptions = array();
foreach ($ds as $d) {
    if ($d->ShowReport && !$d->Disabled) {
        switch ($d->DashboardCategoryID) {
            case 1:
                $reportTypes['dashboardReport'][$d->DashboardName] = "reports/report_dashboard.php?dbid=" . $d->DashboardID;
                $reportDescriptions['dashboardReport'][$d->DashboardName] = $d->DashboardDescription;
                break;
            case 2:
                $reportTypes['contactReport'][$d->DashboardName] = "reports/report_dashboard.php?dbid=" . $d->DashboardID;
                $reportDescriptions['contactReport'][$d->DashboardName] = $d->DashboardDescription;
                break;
            case 3:
                $reportTypes['marketReport'][$d->DashboardName] = "reports/report_dashboard.php?dbid=" . $d->DashboardID;
                $reportDescriptions['marketReport'][$d->DashboardName] = $d->DashboardDescription;
                break;
            case 4:
                $reportTypes['myReport'][$d->DashboardName] = "reports/report_dashboard.php?dbid=" . $d->DashboardID;
                $reportDescriptions['myReport'][$d->DashboardName] = $d->DashboardDescription;
                break;
            case 5:
                $reportTypes['customReport'][$d->DashboardName] = "reports/report_dashboard.php?dbid=" . $d->DashboardID;
                $reportDescriptions['customReport'][$d->DashboardName] = $d->DashboardDescription;
                break;
        }
    }
}

if ($isReport) {
    if ($isAdmin) {
        $reportTypes['adminReport']['Bad Lead Report'] = 'reports/bad_lead.php'; //addition 8/26/13 as requested by Sarah
        $reportDescriptions['adminReport']['Bad Lead Report'] = 'A tabular report listing of all leads that have been given a status that are deemed to be Bad Leads that have been removed from the users view on the lead listing page'; //addition 8/28/13 as requested by Sarah
        $reportTypes['adminReport']['All Lead Report'] = 'reports/all_lead.php';
        $reportDescriptions['adminReport']['All Lead Report'] = 'A tabular report listing all of the information for all leads in a specified date range. Can be exported for manipulating in Excel';
        $reportTypes['adminReport']['Accounting Report'] = 'reports/admin_accounting_summary.php';
        $reportTypes['adminReport']['Product Activity'] = 'reports/admin_product_activity.php';
        $reportTypes['adminReport']['User Activity'] = 'reports/admin_user_activity.php';
        $reportTypes['adminReport']['Active User'] = 'reports/active_user.php';
        if($COMPANY_ID == 46) {
            $reportTypes['adminReport']['Waterfall Report'] = 'reports/waterfall/index.php';
            $reportDescriptions['adminReport']['Waterfall Report'] = 'Waterfall report description.';
        }

    }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title>
        <?= $portal->CurrentCompany->CompanyName ?> :: Reports
    </title>
    <meta http-equiv="content-type" content="text/html;charset=utf-8"/>
    <link type="text/css" media="all" rel="stylesheet" href="style.css"/>
    <link type="text/css" media="all" rel="stylesheet" href="css/report.css"/>
    <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico"/>
    <script type='text/javascript' src='../printable/include/js/jquery-1.10.2.min.js'></script>
    <script type='text/javascript' src='../printable/include/js/jquery-ui.js'></script>
    <script src="js/func.js"></script>
    <script language="JavaScript" type="text/JavaScript">

        $(document).ready(function () {
            // Handler for .ready() called.

            $('#dhtmlgoodies_tree').click(function () {
                $('.placeholder-image').hide();
            });

            $('.list-group-item').on('click', function (e) {
                var previous = $(this).closest(".list-group").children(".active");
                previous.removeClass('active'); // previous list-item
                $(e.target).addClass('active'); // activated list-item
            });

        });

        function loadReportTable(reportDivName) {
            <?php
            foreach ($reportTypes as $repType => $reports) {
                ?>
            document.getElementById("<?= $repType ?>Div").style.display = "none";
            <?php
        }
        ?>
            document.getElementById(reportDivName).style.display = "";
        }
    </script>

    <?php include("components/bootstrap.php") ?>

</head>
<body bgcolor="#FFFFFF" onload="loadReportTable('myReportDiv');">
<?php include("components/header.php") ?>
<?php
$CURRENT_PAGE = "Reports";
include("components/navbar.php");
?>
<?php if (isset($_GET['message'])): ?>
    <div class="container">
        <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?= $_GET['message']; ?>
        </div>
    </div>
<?php endif; ?>

<div class="row">

    <div class="page-header">
        <h3>
            <div class="row">
                <div class="pull-left">Reports</div>
                <!-- <div class="pull-right"><a href="#" class="btn btn-default btn-sm"><i class="glyphicon glyphicon-book"></i> View Manual</a></div> -->
            </div>
        </h3>
    </div>

    <div class="col-md-3">
        <?php include("components/report_tree.php"); ?>
    </div>
    <div class="col-md-8">
        <div class="panel panel-default">
            <!-- Default panel contents -->
            <div class="panel-heading">
                <h3 class="panel-title">Reports</h3>
            </div>

            <?php
            foreach ($reportTypes as $repType => $reports) {
                ?>
                <div id="<?= $repType ?>Div" class="list-group" style="display: none;">
                    <?php
                    $i = 0;
                    foreach ($reports as $repName => $repLink) {
                        ?>
                        <a href="<?= $repLink ?>" target="_blank" class="list-group-item">
                            <h4 class="list-group-item-heading">
                                <div class="row">
                                    <div class="col-md-11"><?= $repName ?></div>
                                    <div class="col-md-1">
                                        <button class="btn btn-default btn-sm"><i
                                                class="glyphicon glyphicon-zoom-in"></i></button>
                                    </div>
                                </div>
                            </h4>
                            <p class="list-group-item-text"><?= $reportDescriptions[$repType][$repName] ?></p>
                        </a>

                        <?php
                        $i++;
                    }
                    ?>
                </div>
                <?php
            }
            ?>
        </div>

    </div>
</div>
<?php include("components/footer.php") ?>
</body>
</html>