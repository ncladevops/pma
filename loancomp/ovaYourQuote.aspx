﻿<%@ Page Language="C#" Debug="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
private double cTotBal, cTotPmt, dcltv, dfmortbal, dsmortbal, dcHomeVal;
private string scTotBal, scTotPmt;
private string mEquity1, mEquity2, mEquity3;
private string bwEquity1, bwEquity2, bwEquity3;
private string mLoanAmt1, mLoanAmt2, mLoanAmt3;
private string bwLoanAmt1, bwLoanAmt2, bwLoanAmt3;
private string mSettle1, mSettle2, mSettle3;
private string bwSettle1, bwSettle2, bwSettle3;
private string mPrePaids1, mPrePaids2, mPrePaids3;
private string bwPrePaids1, bwPrePaids2, bwPrePaids3;
private string mIntRate1, mIntRate2, mIntRate3;
private string bwIntRate1, bwIntRate2, bwIntRate3;
private string mTerm1, mTerm2, mTerm3;
private string bwTerm1, bwTerm2, bwTerm3;
private string mLoanType1, mLoanType2, mLoanType3;
private string mPoints1, mPoints2, mPoints3;
private string mEstPmnt1, mEstPmnt2, mEstPmnt3;
private string bwEstPmnt1, bwEstPmnt2, bwEstPmnt3;
private string mPropTax1, mPropTax2, mPropTax3;
private string bwPropTax1, bwPropTax2, bwPropTax3;
private string mHzrdIns1, mHzrdIns2, mHzrdIns3;
private string mhIns1, mhIns2, mhIns3;
private string bwhIns1, bwhIns2, bwhIns3;
private string mfIns1, mfIns2, mfIns3;
private string bwfIns1, bwfIns2, bwfIns3;
private string mmIns1, mmIns2, mmIns3;
private string bwmIns1, bwmIns2, bwmIns3;
private string mTotal1, mTotal2, mTotal3;
private string bwTotal1, bwTotal2, bwTotal3;
private string estCashBack1, estCashBack2, estCashBack3;
private string mSavings1, mSavings2, mSavings3;
private string aSavings1, aSavings2, aSavings3;
private string aIntSavings1, aIntSavings2, aIntSavings3;
private string mAPR1, mAPR2, mAPR3;
private string bwAPR1, bwAPR2, bwAPR3;
private string totSavings1, totSavings2, totSavings3, debtPaidOff1;
private double[] pgmRates;
private string[] loanPgms;

private string cust_Name, cust_Addr, cust_CSZ, cust_Email;

private string lo_Name, lo_TollFreePhone, lo_Fax, lo_Email;

private string f_MortBal, f_Pmt;
private string s_MortBal, s_Pmt;
private string p_mTaxPmt;
private string ins_mPmt;
private string o_LiabBal, o_LiabPmt;
private string hzrd_mInsPmt;
private string c_HomeVal;

private string opt_Hdr1, term1, points1, rate1, settle1;
private string opt_Hdr2, term2, points2, rate2, settle2;
private string opt_Hdr3, term3, points3, rate3, settle3;
private string today = DateTime.Now.ToString("M/d/yyyy");
		
protected void Page_Load(object sender, EventArgs e)
{
	//get form element values and update calculations
	Page.Validate();
	if (Page.IsValid)
	{
		//createPgmRates();
		getLoanValues();
		getPreparerValues();
		getProspectValues();
		calcOption1();
		calcOption2();
		calcOption3();
	}
}
	
private void createPgmRates()
{
	//create rate table values for loan programs
	pgmRates = new double[6];
	loanPgms = new string[6]; 

	loanPgms[0] = "30YF1P"; pgmRates[0] = 3.875;
	loanPgms[1] = "30YF0P"; pgmRates[1] = 4.125;
	loanPgms[2] = "30YF0P0C"; pgmRates[2] = 4.625;
	loanPgms[3] = "15YF1P"; pgmRates[3] = 3.125;
	loanPgms[4] = "15YF0P"; pgmRates[4] = 3.5;
	loanPgms[5] = "15YF0P0C"; pgmRates[5] = 3.875;
}

private void getLoanValues()
{
	f_MortBal = cvtNumeric(Request.Form["fMortBal"]);
	dfmortbal = cvtRtnNumeric(f_MortBal);
	s_MortBal = cvtNumeric(Request.Form["sMortBal"]);
	dsmortbal = cvtRtnNumeric(s_MortBal);
	f_Pmt = cvtNumeric(Request.Form["fPmt"]);
	s_Pmt = cvtNumeric(Request.Form["sPmt"]);
	p_mTaxPmt = cvtNumeric(Request.Form["pTaxPmt"]);
	hzrd_mInsPmt = cvtNumeric(Request.Form["hzrdInsPmt"]);
	ins_mPmt = cvtNumeric(Request.Form["insPmt"]);
	o_LiabBal = cvtNumeric(Request.Form["oLiabBal"]);
	o_LiabPmt = cvtNumeric(Request.Form["oLiabPmt"]);
	c_HomeVal = cvtNumeric(Request.Form["cHomeVal"]);
	c_HomeVal = cvtNumeric(Request.Form["cHomeVal"]);
	dcHomeVal = cvtRtnNumeric(c_HomeVal);
	cTotBal = 0;
	if (f_MortBal != "") { cTotBal += Convert.ToDouble(f_MortBal); }
	if (s_MortBal != "") { cTotBal += Convert.ToDouble(s_MortBal); }
	if (o_LiabBal != "") { cTotBal += Convert.ToDouble(o_LiabBal); }
	scTotBal = "$"+cvtNumeric(Convert.ToString(cTotBal));
	cTotPmt = 0;
	if (f_Pmt != "") { cTotPmt += Convert.ToDouble(f_Pmt); }
	if (s_Pmt != "") { cTotPmt += Convert.ToDouble(s_Pmt); }
	if (p_mTaxPmt != "") { cTotPmt += Convert.ToDouble(p_mTaxPmt); }
	if (hzrd_mInsPmt != "") { cTotPmt += Convert.ToDouble(hzrd_mInsPmt); }
	if (ins_mPmt != "") { cTotPmt += Convert.ToDouble(ins_mPmt); }
	if (o_LiabPmt != "") { cTotPmt += Convert.ToDouble(o_LiabPmt); }
	scTotPmt = "$"+cvtNumeric(Convert.ToString(cTotPmt));
}
		
private void getPreparerValues()
{
	lo_Name = Convert.ToString(Request.Form["loName"]).ToUpper();
	lo_TollFreePhone = cvtPhoneNum(Convert.ToString(Request.Form["loTollFreePhone"]));
	lo_Fax = cvtPhoneNum(Convert.ToString(Request.Form["loFax"]));
	lo_Email = Convert.ToString(Request.Form["loEmail"]);
}

private void getProspectValues()
{
	cust_Name = Request.Form["custName"].ToUpper();
	cust_Addr = Request.Form["custAddr"];
	cust_CSZ = Request.Form["custCSZ"];
	cust_Email = Request.Form["custEmail"];
}
	
private string cvtPhoneNum(string phone)
{
	//convert phone number to aaa-nnn-nnnn
	if (phone == null || phone == "") { return ""; }
	Regex digitsOnly = new Regex(@"[^\d]");
	phone = digitsOnly.Replace(phone, "");
	if (phone == "") { return phone; }
	ulong iphone = Convert.ToUInt64(phone);
	return iphone.ToString("###-###-####");
}

private string cvtNumeric(string val)
{
	//convert numeric to 1,000,000.00 format
	if (val == null || val == "") { return ""; }
	Regex digitsDecOnly = new Regex(@"[^\d\.]");
	val = digitsDecOnly.Replace(val, "");
	if (val == "") { return val; }
	double ival = Convert.ToDouble(val);
	return ival.ToString("#,#.00");
}

private string cvtNumeric(string val, int nocents)
{
	//convert numeric to 1,000,000.00 format
	if (val == null || val == "") { return ""; }
	Regex digitsDecOnly = new Regex(@"[^\d\.]");
	val = digitsDecOnly.Replace(val, "");
	if (val == "") { return val; }
	double ival = Convert.ToDouble(val);
	return ival.ToString("#,#");
}

private double cvtRtnNumeric(string val)
{
	//convert numeric to 1,000,000.00 format
	if (val == null || val == "") { return 0; }
	Regex digitsDecOnly = new Regex(@"[^\d\.]");
	val = digitsDecOnly.Replace(val, "");
	if (val == "") { return 0; }
	double ival = Convert.ToDouble(val);
	return ival;
}

private double calcAPR(double loanAmt, double arate, double mterm, double fees, double payment)
{
	//estimate APR
	float yearlyPmts = 12;
//	double q = Math.Log(1 + 1 / mterm) / Math.Log(2); //calculate logs of loan term in months
//	double i = Math.Pow((Math.Pow((1 + (payment / (loanAmt-fees))), (1 / q)) - 1), q) - 1; //estimate apr
//	double apr = i*(double)yearlyPmts;
	
	double FC = payment * mterm - loanAmt+fees;
	double apr = yearlyPmts * ((double)95 * mterm + (double)9) * FC / (yearlyPmts * mterm * (mterm + (double)1) * ((double)4 * (loanAmt - fees) + FC));
	double oapr = apr;
	apr -= 0.00005; //30yr offset for calc errors
	if (mterm==180) { apr -= 0.0001; }	//15 year offset
	if (apr < arate) { apr = oapr; }
	if (apr < arate) { apr = arate; }
	//Response.Write("<br><b><font color='red'>" + FC + "</font></b>");
	return apr;
}

private string convPointString(string strPoints)
{
	//convert point text string so that LO can have a text note instead of a point value when points are zero
	Regex charOnly = new Regex(@"[^\D]");
	string cpoint = charOnly.Replace(strPoints, "");
	if (cpoint == "" || cpoint == ".")
	{
		double points = cvtRtnNumeric(strPoints);
		cpoint = Convert.ToString(points) + " pt(s)";
	}
	else
	{
		if (strPoints.ToLower().Trim().Replace("  "," ") == "no cost")
		{
			cpoint = strPoints;
		}
		else
		{
			cpoint = "0";
		}
	}
	return cpoint;
}
	
private void calcOption1()
{
	term1 = Convert.ToString(Request.Form["optHdr1"]);
	int term = Convert.ToInt32(cvtRtnNumeric(term1));

	if (term > 0)
	{
		mLoanType1 = Convert.ToString(Request.Form["optLoanType1"]);
		if (mLoanType1.ToLower() == "arm")
		{
			term = 30;
		}
		points1 = Convert.ToString(Request.Form["tb_Points1"]);
		rate1 = Convert.ToString(Request.Form["tb_Rate1"]);
		mPoints1 = convPointString(points1);
		double points = cvtRtnNumeric(mPoints1);
		double rate = cvtRtnNumeric(rate1);
		mTerm1 = term.ToString() + " yrs";
		mIntRate1 = rate.ToString("##.000") + "%";

		string man_mLoanAmt1 = cvtNumeric(Request.Form["tb_mLoanAmt1"]);
		double dLoanAmt;
		if (man_mLoanAmt1 == "")
		{
			dLoanAmt = dfmortbal + dsmortbal + cvtRtnNumeric(o_LiabBal);
		}
		else
		{
			dLoanAmt = cvtRtnNumeric(man_mLoanAmt1);
		}
		
		settle1 = Convert.ToString(Request.Form["tb_mSettle1"]);
		double dSettle = 0;
		if (settle1 == "")
		{
			//dSettle = (dLoanAmt * (points / 100));
		}
		else
		{
			Regex digitsDecOnly = new Regex(@"[^\d\.]");
			settle1 = digitsDecOnly.Replace(settle1, "");
			dSettle = Convert.ToDouble(settle1);
		}
		mSettle1 = "$" + dSettle.ToString("#,#.00");
		bwSettle1 = mSettle1;

		//equity
		dcltv = dLoanAmt * 100 / dcHomeVal;
			
		int yrPayments = 12; //number of payments in a year
		int mterm = term * yrPayments; //convert to months of loan term
		double arate=rate/100; //convert percent to float number
		double totValue = Math.Pow((1 + arate / yrPayments), mterm);

		string p_TaxPmt = cvtRtnNumeric(p_mTaxPmt).ToString("#,#.00");
		
		string hzrd_InsPmt = cvtRtnNumeric(hzrd_mInsPmt).ToString("#,#.00");

		mhIns1 = "$" + cvtRtnNumeric(ins_mPmt).ToString("#,#.00");

		double payment = 0;
		if (mLoanType1.ToLower() == "int only")
		{
			payment = ((dLoanAmt * arate) / 12);
		}
		else
		{
			payment = dLoanAmt * (totValue * arate) / (yrPayments * (totValue - 1));
		}
		bwEstPmnt1 = "$" + (payment / 2).ToString("#,#.00");
		mEstPmnt1 = "$" + payment.ToString("#,#.00");
		
		string man_mTotal1 = cvtNumeric(Request.Form["tb_mTotPmnt1"]);
		if (man_mTotal1 == "")
		{
			mTotal1 = "$" + (cvtRtnNumeric(mEstPmnt1) + cvtRtnNumeric(p_TaxPmt) + cvtRtnNumeric(ins_mPmt) + cvtRtnNumeric(hzrd_mInsPmt)).ToString("#,#.00");
		}
		else
		{
			mTotal1 = "$" + cvtRtnNumeric(man_mTotal1).ToString("#,#.00");
		}

		bwTotal1 = "$" + ((cvtRtnNumeric(mEstPmnt1) + cvtRtnNumeric(p_TaxPmt) + cvtRtnNumeric(ins_mPmt) + cvtRtnNumeric(hzrd_mInsPmt)) / 2).ToString("#,#.00");

		string man_estCashBack1 = cvtRtnNumeric(Request.Form["tb_EstCashBack1"]).ToString("#.00");
		if (man_estCashBack1 == ".00")
		{
			double dCashBack = dLoanAmt - (dfmortbal + dsmortbal + cvtRtnNumeric(o_LiabBal) + cvtRtnNumeric(Request.Form["tb_mPrePaids1"]) + dSettle);
			if (dCashBack > 0) { man_estCashBack1 = dCashBack.ToString(); }			
		}
		estCashBack1 = cvtRtnNumeric(man_estCashBack1).ToString("#,#.00;(#,#.00)");

		string man_mSavings1 = cvtRtnNumeric(Request.Form["tb_MonthlySavings1"]).ToString("#.00");
		if (man_mSavings1 == ".00")
		{
			mSavings1 = "$" + (cTotPmt - cvtRtnNumeric(mTotal1)).ToString("#,#.00;(#,#.00)");
		}
		else
		{
			mSavings1 = cvtRtnNumeric(man_mSavings1).ToString("#,#.00;(#,#.00)");
		}

		string man_aSavings1 = cvtRtnNumeric(Request.Form["tb_AnnualSavings1"]).ToString("#.00");
		if (man_aSavings1 == ".00")
		{
			aSavings1 = "$" + ((cTotPmt - cvtRtnNumeric(mTotal1)) * 12).ToString("#,#.00;(#,#.00)");
		}
		else
		{
			aSavings1 = cvtRtnNumeric(man_aSavings1).ToString("#,#.00;(#,#.00)");
		}

		double fees = dSettle;
		
		string man_mAPR1 = cvtRtnNumeric(Request.Form["tb_mEstAPR1"]).ToString("#.0000");
		if (man_mAPR1 == ".0000")
		{
			double apr = calcAPR(dLoanAmt, arate, mterm, fees, payment);
			mAPR1 = (apr * (double)100).ToString("#,#.000") + "%";
		}
		else
		{
			mAPR1 = cvtRtnNumeric(man_mAPR1).ToString("#,#.000") + "%";
		}
		
		//only for calc1
		totSavings1 = "$" + ((cTotPmt - cvtRtnNumeric(mTotal1)) * 12 * term).ToString("#,#.00;(#,#.00)");
		
		mEquity1 = cvtNumeric(dcltv.ToString(), 0) + "%";
		bwEquity1 = mEquity1;
		mLoanAmt1 = "$" + cvtNumeric(dLoanAmt.ToString());
		bwLoanAmt1 = mLoanAmt1;
		
		mPropTax1 = "$" + cvtNumeric(p_TaxPmt);
		bwPropTax1 = "$" + cvtNumeric((cvtRtnNumeric(p_TaxPmt)/2).ToString());
		mHzrdIns1 = "$" + cvtNumeric(hzrd_InsPmt);
		bwhIns1 = "$" + cvtNumeric((cvtRtnNumeric(ins_mPmt) / 2).ToString());

		mPrePaids1 = "$" + cvtRtnNumeric(Request.Form["tb_mPrePaids1"]).ToString("#.00");
	}
}

private void calcOption2()
{
	term2 = Convert.ToString(Request.Form["optHdr2"]);
	int term = Convert.ToInt32(cvtRtnNumeric(term2));

	if (term > 0)
	{
		mLoanType2 = Convert.ToString(Request.Form["optLoanType2"]);
		if (mLoanType2.ToLower() == "arm")
		{
			term = 30;
		}
		points2 = Convert.ToString(Request.Form["tb_Points2"]);
		rate2 = Convert.ToString(Request.Form["tb_Rate2"]);
		mPoints2 = convPointString(points2);
		double points = cvtRtnNumeric(mPoints2);
		double rate = cvtRtnNumeric(rate2);
		mTerm2 = term.ToString() + " yrs";
		mIntRate2 = rate.ToString("##.000") + "%";

		string man_mLoanAmt2 = cvtNumeric(Request.Form["tb_mLoanAmt2"]);
		double dLoanAmt;
		if (man_mLoanAmt2 == "")
		{
			dLoanAmt = dfmortbal + dsmortbal + cvtRtnNumeric(o_LiabBal);
		}
		else
		{
			dLoanAmt = cvtRtnNumeric(man_mLoanAmt2);
		}

		settle2 = Convert.ToString(Request.Form["tb_mSettle2"]);
		double dSettle = 0;
		if (settle2 == "")
		{
			//dSettle = (dLoanAmt * (points / 100));
		}
		else
		{
			Regex digitsDecOnly = new Regex(@"[^\d\.]");
			settle2 = digitsDecOnly.Replace(settle2, "");
			dSettle = Convert.ToDouble(settle2);
		}
		mSettle2 = "$" + dSettle.ToString("#,#.00");
		bwSettle2 = mSettle2;

		//equity
		dcltv = dLoanAmt * 100 / dcHomeVal;

		int yrPayments = 12;
		int mterm = term * yrPayments; //convert to months
		double arate = rate / 100; //convert percent to float number
		double totValue = Math.Pow((1 + arate / yrPayments), mterm);

		string p_TaxPmt = cvtRtnNumeric(p_mTaxPmt).ToString("#,#.00");

		string hzrd_InsPmt = cvtRtnNumeric(hzrd_mInsPmt).ToString("#,#.00");

		mhIns2 = "$" + cvtRtnNumeric(ins_mPmt).ToString("#,#.00");

		double payment = 0;
		if (mLoanType2.ToLower() == "int only")
		{
			payment = ((dLoanAmt * arate) / 12);
		}
		else
		{
			payment = dLoanAmt * (totValue * arate) / (yrPayments * (totValue - 1));
		}
		bwEstPmnt2 = "$" + (payment / 2).ToString("#,#.00");
		mEstPmnt2 = "$" + payment.ToString("#,#.00");

		string man_mTotal2 = cvtNumeric(Request.Form["tb_mTotPmnt2"]);
		if (man_mTotal2 == "")
		{
			mTotal2 = "$" + (cvtRtnNumeric(mEstPmnt2) + cvtRtnNumeric(p_TaxPmt) + cvtRtnNumeric(ins_mPmt) + cvtRtnNumeric(hzrd_mInsPmt)).ToString("#,#.00");
		}
		else
		{
			mTotal2 = "$" + cvtRtnNumeric(man_mTotal2).ToString("#,#.00");
		}

		bwTotal2 = "$" + ((cvtRtnNumeric(mEstPmnt2) + cvtRtnNumeric(p_TaxPmt) + cvtRtnNumeric(ins_mPmt) + cvtRtnNumeric(hzrd_mInsPmt)) / 2).ToString("#,#.00");

		string man_estCashBack2 = cvtRtnNumeric(Request.Form["tb_EstCashBack2"]).ToString("#.00");
		if (man_estCashBack2 == ".00")
		{
			double dCashBack = dLoanAmt - (dfmortbal + dsmortbal + cvtRtnNumeric(o_LiabBal) + cvtRtnNumeric(Request.Form["tb_mPrePaids2"]) + dSettle);
			if (dCashBack > 0) { man_estCashBack2 = dCashBack.ToString(); }
		}
		estCashBack2 = cvtRtnNumeric(man_estCashBack2).ToString("#,#.00;(#,#.00)");

		string man_mSavings2 = cvtRtnNumeric(Request.Form["tb_MonthlySavings2"]).ToString("#.00");
		if (man_mSavings2 == ".00")
		{
			mSavings2 = "$" + (cTotPmt - cvtRtnNumeric(mTotal2)).ToString("#,#.00;(#,#.00)");
		}
		else
		{
			mSavings2 = cvtRtnNumeric(man_mSavings2).ToString("#,#.00;(#,#.00)");
		}

		string man_aSavings2 = cvtRtnNumeric(Request.Form["tb_AnnualSavings2"]).ToString("#.00");
		if (man_aSavings2 == ".00")
		{
			aSavings2 = "$" + ((cTotPmt - cvtRtnNumeric(mTotal2)) * 12).ToString("#,#.00;(#,#.00)");
		}
		else
		{
			aSavings2 = cvtRtnNumeric(man_aSavings2).ToString("#,#.00;(#,#.00)");
		}

		double fees = dSettle;

		string man_mAPR2 = cvtRtnNumeric(Request.Form["tb_mEstAPR2"]).ToString("#.0000");
		if (man_mAPR2 == ".0000")
		{
			double apr = calcAPR(dLoanAmt, arate, mterm, fees, payment);
			mAPR2 = (apr * (double)100).ToString("#,#.000") + "%";
		}
		else
		{
			mAPR2 = cvtRtnNumeric(man_mAPR2).ToString("#,#.000") + "%";
		}

		mEquity2 = cvtNumeric(dcltv.ToString(), 0) + "%";
		bwEquity2 = mEquity2;
		mLoanAmt2 = "$" + cvtNumeric(dLoanAmt.ToString());
		bwLoanAmt2 = mLoanAmt2;

		mPropTax2 = "$" + cvtNumeric(p_TaxPmt);
		bwPropTax2 = "$" + cvtNumeric((cvtRtnNumeric(p_TaxPmt) / 2).ToString());
		mHzrdIns2 = "$" + cvtNumeric(hzrd_InsPmt);
		bwhIns2 = "$" + cvtNumeric((cvtRtnNumeric(ins_mPmt) / 2).ToString());

		mPrePaids2 = "$" + cvtRtnNumeric(Request.Form["tb_mPrePaids2"]).ToString("#.00");
	}
}

private void calcOption3()
{
	term3 = Convert.ToString(Request.Form["optHdr3"]);
	int term = Convert.ToInt32(cvtRtnNumeric(term3));

	if (term > 0)
	{
		mLoanType3 = Convert.ToString(Request.Form["optLoanType3"]);
		if (mLoanType3.ToLower() == "arm")
		{
			term = 30;
		}
		points3 = Convert.ToString(Request.Form["tb_Points3"]);
		rate3 = Convert.ToString(Request.Form["tb_Rate3"]);
		mPoints3 = convPointString(points3);
		double points = cvtRtnNumeric(mPoints3);
		double rate = cvtRtnNumeric(rate3);
		mTerm3 = term.ToString() + " yrs";
		mIntRate3 = rate.ToString("##.000") + "%";

		string man_mLoanAmt3 = cvtNumeric(Request.Form["tb_mLoanAmt3"]);
		double dLoanAmt;
		if (man_mLoanAmt3 == "")
		{
			dLoanAmt = dfmortbal + dsmortbal + cvtRtnNumeric(o_LiabBal);
		}
		else
		{
			dLoanAmt = cvtRtnNumeric(man_mLoanAmt3);
		}

		settle3 = Convert.ToString(Request.Form["tb_mSettle3"]);
		double dSettle = 0;
		if (settle3 == "")
		{
			//dSettle = (dLoanAmt * (points / 100));
		}
		else
		{
			Regex digitsDecOnly = new Regex(@"[^\d\.]");
			settle3 = digitsDecOnly.Replace(settle3, "");
			dSettle = Convert.ToDouble(settle3);
		}
		mSettle3 = "$" + dSettle.ToString("#,#.00");
		bwSettle3 = mSettle3;

		//equity
		dcltv = dLoanAmt * 100 / dcHomeVal;

		int yrPayments = 12;
		int mterm = term * yrPayments; //convert to months
		double arate = rate / 100; //convert percent to float number
		double totValue = Math.Pow((1 + arate / yrPayments), mterm);

		string p_TaxPmt = cvtRtnNumeric(p_mTaxPmt).ToString("#,#.00");

		string hzrd_InsPmt = cvtRtnNumeric(hzrd_mInsPmt).ToString("#,#.00");

		mhIns3 = "$" + cvtNumeric(ins_mPmt);

		double payment = 0;
		if (mLoanType3.ToLower() == "int only")
		{
			payment = ((dLoanAmt * arate) / 12);
		} else {
			payment = dLoanAmt * (totValue * arate) / (yrPayments * (totValue - 1));
		}
		bwEstPmnt3 = "$" + (payment / 2).ToString("#,#.00");
		mEstPmnt3 = "$" + payment.ToString("#,#.00");

		string man_mTotal3 = cvtNumeric(Request.Form["tb_mTotPmnt3"]);
		if (man_mTotal3 == "")
		{
			mTotal3 = "$" + (cvtRtnNumeric(mEstPmnt3) + cvtRtnNumeric(p_TaxPmt) + cvtRtnNumeric(ins_mPmt) + cvtRtnNumeric(hzrd_mInsPmt)).ToString("#,#.00");
		}
		else
		{
			mTotal3 = "$" + cvtRtnNumeric(man_mTotal3).ToString("#,#.00");
		}

		bwTotal3 = "$" + ((cvtRtnNumeric(mEstPmnt3) + cvtRtnNumeric(p_TaxPmt) + cvtRtnNumeric(ins_mPmt) + cvtRtnNumeric(hzrd_mInsPmt)) / 2).ToString("#,#.00");

		string man_estCashBack3 = cvtRtnNumeric(Request.Form["tb_EstCashBack3"]).ToString("#.00");
		if (man_estCashBack3 == ".00")
		{
			double dCashBack = dLoanAmt - (dfmortbal + dsmortbal + cvtRtnNumeric(o_LiabBal) + cvtRtnNumeric(Request.Form["tb_mPrePaids3"]) + dSettle);
			if (dCashBack > 0) { man_estCashBack3 = dCashBack.ToString(); }
		}
		estCashBack3 = cvtRtnNumeric(man_estCashBack3).ToString("#,#.00;(#,#.00)");

		string man_mSavings3 = cvtRtnNumeric(Request.Form["tb_MonthlySavings3"]).ToString("#.00");
		if (man_mSavings3 == ".00")
		{
			mSavings3 = "$" + (cTotPmt - cvtRtnNumeric(mTotal3)).ToString("#,#.00;(#,#.00)");
		}
		else
		{
			mSavings3 = cvtRtnNumeric(man_mSavings3).ToString("#,#.00;(#,#.00)");
		}

		string man_aSavings3 = cvtRtnNumeric(Request.Form["tb_AnnualSavings3"]).ToString("#.00");
		if (man_aSavings3 == ".00")
		{
			aSavings3 = "$" + ((cTotPmt - cvtRtnNumeric(mTotal3)) * 12).ToString("#,#.00;(#,#.00)");
		}
		else
		{
			aSavings3 = cvtRtnNumeric(man_aSavings3).ToString("#,#.00;(#,#.00)");
		}

		double fees = dSettle;

		string man_mAPR3 = cvtRtnNumeric(Request.Form["tb_mEstAPR3"]).ToString("#.0000");
		if (man_mAPR3 == ".0000")
		{
			double apr = calcAPR(dLoanAmt, arate, mterm, fees, payment);
			mAPR3 = (apr * (double)100).ToString("#,#.000") + "%";
		}
		else
		{
			mAPR3 = cvtRtnNumeric(man_mAPR3).ToString("#,#.000") + "%";
		}

		mEquity3 = cvtNumeric(dcltv.ToString(), 0) + "%";
		bwEquity3 = mEquity3;
		mLoanAmt3 = "$" + cvtNumeric(dLoanAmt.ToString());
		bwLoanAmt3 = mLoanAmt3;

		mPropTax3 = "$" + cvtNumeric(p_TaxPmt);
		bwPropTax3 = "$" + cvtNumeric((cvtRtnNumeric(p_TaxPmt) / 2).ToString());
		mHzrdIns3 = "$" + cvtNumeric(hzrd_InsPmt);
		bwhIns3 = "$" + cvtNumeric((cvtRtnNumeric(ins_mPmt) / 2).ToString());

		mPrePaids3 = "$" + cvtRtnNumeric(Request.Form["tb_mPrePaids3"]).ToString("#.00");
	}
}
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Loan Options</title>
    <meta name="robots" content="noindex,nofollow,noarchive,nosnippet,nodp,noydir" />
    <style type="text/css">
		body {
			background:white; color:black;
			margin-left:10%; margin-right:10%;
			font-size:64%; font-family:Arial,"Times New Roman",Georgia,Serif;
		}
		table, td, th{ border: 0px solid black; }
		table { padding:0; border-collapse:collapse; width:100%; }
		tr { vertical-align:top; text-align:center; }
		td { white-space:nowrap; }
		a { text-decoration: none; }
		.addrHdr { font-size:1.25em; font-weight:bold; vertical-align:middle; text-align:center; color:#82715c; }
		.advHdr { font-family:Lucida Sans, Franklin Gothic Demi, Arial Black; font-weight:bold; font-size:11pt; color:#007db1; }
		.savingsHdr { font-family:Lucida Sans, Franklin Gothic Demi, Arial Black; font-size:14pt; color:#007db1; text-align:left; }
		.savingsValHdr { font-family:Lucida Sans, Franklin Gothic Demi, Arial Black; font-size:14pt; color:#231f20; text-align:left; }
		.sectionHdr { font-family:Arial Black, Franklin Gothic Demi, Arial; font-weight:bold; font-size:8pt; color:#f96400; text-align:left; }
		.midTxt { text-align:center; font-size: 0.95em; }
		.vmidTxt { text-align:center; font-size: 0.9em; }
		.bmidTxt { text-align:center; font-weight:bold; font-size: 0.95em; }
		.blgMidTxt { text-align:center; font-weight:bold; font-size: 1.15em; }
		.bleftTxt { text-align:left; font-weight:bold; font-size: 0.95em; }
		.blgLeftTxt { text-align:left; font-weight:bold; font-size: 1.15em; }
		.leftTxt { text-align:left; font-size: 0.95em; }
		.lgLeftTxt { text-align:left; font-size: 1.15em; }
		.vleftTxt { text-align:left; font-size: 0.9em; }
		.vleft2Txt { text-align:left; font-size: 0.95em; }
		.bvleftTxt { text-align:left; font-size: 0.9em; font-weight:bold; }
		.bvlgLeftTxt { text-align:left; font-size: 1.1698em; font-weight:bold; }
		.ileftTxt { text-align:left; font-style:italic; vertical-align:middle; }
		.ileftTxt2 { text-align:left; font-style:italic; vertical-align:middle; height:14px; }
		.ileftTxt3 { text-align:left; font-style:italic; font-weight:bold; vertical-align:middle; height:16.5px; }
		.irightTxt3 { text-align:right; font-style:italic; font-weight:bold; vertical-align:middle; height:16.5px; }
		.ixleftTxt3 { text-align:left; font-style:italic; font-weight:bold; vertical-align:middle; height:20px; font-size:1.5em; }
		.ixrightTxt3 { color:#f96400; text-align:right; font-style:italic; font-weight:bold; vertical-align:middle; height:20px; font-size:1.5em; }
		.blgMidTxt2 { text-align:center; font-weight:bold; vertical-align:middle; height:16.5px; }
		.bxlgMidTxt2 { text-align:center; font-weight:bold; vertical-align:middle; height:20px; font-size:1.5em; }
		.ilgLeftTxt { text-align:left; font-style:italic; font-size:1.15em; }
		.rightTxt { text-align:right; font-size: 0.9em; }
		.spaceTxt { text-align:right; font-size: 0.4em; }
		.space2Txt { text-align:right; font-size: 0.35em; }
		.spaceLgTxt { font-size: 1.25em; }
		.blgRightTxt { text-align:right; font-size: 1.1em; font-weight:bold; }
		.noteTxt { font-size:75%; font-style:italic; }
		.tbBox { border:2px solid #007db1; }
		.tbBoxOrg { border:0px; background: url(images/orgGradient.png) repeat; }
		.tbBoxOrg2 { border:0px; background: url(images/orgGradient1.png) repeat; }
		.tbBoxBlu { border:0px; background: url(images/bluGradient.png) repeat; }
		.tbBoxBlu2 { border:0px; background: url(images/bluGradient1.png) repeat; }
		.tbBoxGry { border:0px; background: url(images/gryGradient.png) repeat; }
    </style>
</head>
<body>
    <table cellpadding="0" cellspacing="0" border="0" style="width:700px;"><tr><td>
			<table cellpadding="0" cellspacing="0" border="0"><tr>
				<td colspan="2" align="left" valign="bottom"><img src="images/mortProp.png" alt="" height="43" width="329" />
				<br /><img src="images/clear.gif" width="1" height="20px" /><br /><img src="images/extTimeSens.png" alt="" height="18" weight="231" />
				<br /><img src="images/clear.gif" width="1" height="20px" /><br />
					<table cellpadding="0" cellspacing="0" border="0">
						<tr style="font-family:Arial Black, Franklin Gothic Demi, Arial; font-size:8pt; color:#7c6a55"><td align="left">PREPARED FOR:</td><td width="7%">&nbsp;</td><td align="left">PRESENTED BY:</td></tr>
						<tr style="font-family:Arial, Lucidia Fax, Franklin Gothic Medium; font-size:8pt; color:#363436"><td align="left"><%=cust_Name%></td><td width="7%">&nbsp;</td><td align="left"><%=lo_Name%></td></tr>
						<tr style="font-family:Arial, Lucidia Fax, Franklin Gothic Medium; font-size:8pt; color:#363436"><td align="left"><%=cust_Addr%></td><td width="7%">&nbsp;</td><td align="left">Fx <%=lo_Fax%></td></tr>
						<tr style="font-family:Arial, Lucidia Fax, Franklin Gothic Medium; font-size:8pt; color:#363436"><td align="left"><%=cust_CSZ%></td><td width="7%">&nbsp;</td><td align="left"><%=lo_Email%></td></tr>
					</table>
				</td>
				<td><img src="images/ovalogo.png" border="0" alt="Ovation Logo" height="162" width="294" /></td>
			</tr></table>
		</td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr style="background-color:#007db1;"><td><img src="images/clear.gif" height="2" alt="" /></td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td align="left"><span class="sectionHdr">AT A GLANCE</span></td></tr>
		<tr><td><img src="images/clear.gif" height="4" width="1" alt="" /></td></tr>
		<tr><td>
			<table cellpadding="0" cellspacing="0" border="0">
			<tr><td width="10%">&nbsp;</td>
			<td class="tbBox" width="40%">
				<table cellpadding="0" cellspacing="0" border="0">
					<tr class="addrHdr"><td class="savingsHdr"><img src="images/clear.gif" height="6" alt="" /><br />&nbsp;&nbsp;&nbsp;MONTHLY SAVINGS:</td><td width="1%">&nbsp;</td><td class="savingsValHdr"><%=mSavings1%></td><td width="1%">&nbsp;</td></tr>
					<tr class="addrHdr"><td class="savingsHdr"><img src="images/clear.gif" height="6" alt="" /><br />&nbsp;&nbsp;&nbsp;ANNUAL SAVINGS:</td><td width="1%">&nbsp;</td><td class="savingsValHdr"><%=aSavings1%></td><td width="1%">&nbsp;</td></tr>
					<tr class="addrHdr"><td class="savingsHdr"><img src="images/clear.gif" height="6" alt="" /><br />&nbsp;&nbsp;&nbsp;TOTAL SAVINGS:</td><td width="1%">&nbsp;</td><td class="savingsValHdr"><%=totSavings1%></td><td width="1%">&nbsp;</td></tr>
					<tr class="addrHdr"><td class="savingsHdr"><img src="images/clear.gif" height="6" alt="" /><br />&nbsp;&nbsp;&nbsp;DEBT PAID OFF:</td><td width="1%">&nbsp;</td><td class="savingsValHdr">$<%=o_LiabBal%></td><td width="1%">&nbsp;</td></tr>
					<tr class="addrHdr"><td class="savingsHdr"><img src="images/clear.gif" height="6" alt="" /><br />&nbsp;&nbsp;&nbsp;**CASH IN HAND:</td><td width="1%">&nbsp;</td><td class="savingsValHdr">$<%=estCashBack1%></td><td width="1%">&nbsp;</td></tr>
					<tr class="addrHdr"><td class="savingsHdr">&nbsp;</td><td width="3%">&nbsp;</td><td colspan="2" class="noteTxt">*Based on Loan Option #1&nbsp;&nbsp;</td></tr>
					<tr class="spaceTxt"><td colspan="3">&nbsp;</td></tr>
				</table>
			</td><td width="5%"><img src="images/clear.gif" width="5%" height="1" alt="" /></td><td class="tbBox" width="35%">
				<table cellpadding="0" cellspacing="0" border="0">
					<tr class="addrHdr"><td>&nbsp;&nbsp;&nbsp;Mortgage Liens</td><td>&nbsp;</td><td align="right">Balance</td><td>&nbsp;</td><td align="right">Payment</td><td>&nbsp;</td></tr>
					<tr><td class="ileftTxt">&nbsp;Existing 1st Mortgage</td><td>&nbsp;</td><td class="rightTxt">
						<%=f_MortBal.Replace(".00","")%>
						</td><td>&nbsp;</td><td class="rightTxt">
						<%=f_Pmt.Replace(".00","")%>
						</td><td>&nbsp;</td></tr>
					<tr><td class="ileftTxt">&nbsp;Existing 2nd Mortgage</td><td>&nbsp;</td><td class="rightTxt">
						<%=s_MortBal.Replace(".00","")%>
						</td><td>&nbsp;</td><td class="rightTxt">
						<%=s_Pmt.Replace(".00","")%>
						</td><td>&nbsp;</td></tr>
					<tr><td class="ileftTxt">&nbsp;Property Taxes</td><td>&nbsp;</td><td class="rightTxt">
						. . . . . . . .
						</td><td>&nbsp;</td><td class="rightTxt">
						<%=p_mTaxPmt.Replace(".00","")%>
						</td><td>&nbsp;</td></tr>
					<tr><td class="ileftTxt">&nbsp;Hazard Insurance</td><td>&nbsp;</td><td class="rightTxt">
						. . . . . . . .
						</td><td>&nbsp;</td><td class="rightTxt">
						<%=hzrd_mInsPmt.Replace(".00","")%>
						</td><td>&nbsp;</td></tr>
					<tr><td class="ileftTxt">&nbsp;PMI, Flood, etc.</td><td>&nbsp;</td><td class="rightTxt">
						. . . . . . . .
						</td><td>&nbsp;</td><td class="rightTxt">
						<%=ins_mPmt.Replace(".00","")%>
						</td><td>&nbsp;</td></tr>
					<tr class="spaceTxt"><td>&nbsp;</td><td colspan="4"></td></tr>
					<tr class="addrHdr"><td width="27%">&nbsp;&nbsp;&nbsp;Debt to be paid off:</td>
						<td colspan="4">&nbsp;</td></tr>
					<tr><td class="ileftTxt">&nbsp;Credit Cards, Auto, etc.</td><td>&nbsp;</td><td class="rightTxt">
						<%=o_LiabBal.Replace(".00","")%>
						</td><td>&nbsp;</td><td class="rightTxt">
						<%=o_LiabPmt.Replace(".00","")%>
						</td><td>&nbsp;</td></tr>
					<tr class="spaceTxt"><td colspan="5">&nbsp;</td></tr>
					<tr class="spaceTxt"><td colspan="5">&nbsp;</td></tr>
					<tr style="font-family:Arial Black, Arial; border-bottom:2px solid black; color:#f96400; font-weight:bold;"><td class="blgLeftTxt">&nbsp;TOTALS</td><td>&nbsp;</td><td class="blgRightTxt"><%=scTotBal.Replace(".00", "")%></td><td>&nbsp;</td><td class="blgRightTxt"><%=scTotPmt.Replace(".00", "")%></td><td>&nbsp;</td></tr>
					<tr class="spaceTxt"><td colspan="5">&nbsp;</td></tr>
					<tr class="spaceTxt"><td colspan="5">&nbsp;</td></tr>
					<tr><td class="blgLeftTxt" colspan="2">&nbsp;Current Home Value</td><td class="blgRightTxt">
						<%=c_HomeVal.Replace(".00","")%>
						</td><td>&nbsp;</td></tr>
					<tr class="spaceTxt"><td colspan="5">&nbsp;</td>
					</tr>
				</table>
			</td><td width="10%">&nbsp;
			</td></tr>
			</table>
		</td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr style="background-color:#007db1;"><td><img src="images/clear.gif" height="2" alt="" /></td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td align="left">
			<table cellpadding="0" cellspacing="0" border="0">
			<tr><td width="20">
				<span class="sectionHdr">YOUR 3 LOAN OPTIONS</span>
			</td><td><img src="images/clear.gif" width="3" height="1" alt="" /></td><td width="160">
				<img src="images/loanOption1a.png" width="100%" height="36" alt="Option #1" style="border:0px; border-bottom:3px solid #c4b8aa;" />
			</td><td><img src="images/clear.gif" width="8" height="1" alt="" /></td><td width="160">
				<img src="images/loanOption2a.png" width="100%" height="36" alt="Option #2" style="border:0px; border-bottom:3px solid #c4b8aa;" />
			</td><td><img src="images/clear.gif" width="8" height="1" alt="" /></td><td width="160">
				<img src="images/loanOption3a.png"width="100%" height="36" alt="Option #3" style="border:0px; border-bottom:3px solid #c4b8aa;" />
			</td><td><img src="images/clear.gif" width="3" height="1" alt="" />
			</td></tr>
			<tr><td>
				<table cellpadding="0" cellspacing="0" border="0">
					<tr><td><img src="images/clear.gif" width="1" height="4" alt="" /></td></tr>
					<tr><td class="ixrightTxt3">&nbsp;Term (Years)</td></tr>
					<tr><td class="irightTxt3">&nbsp;Loan Type</td></tr>
					<tr><td class="irightTxt3">&nbsp;Points</td></tr>
					<tr><td class="irightTxt3">&nbsp;Interest Rate (%)</td></tr>
					<tr><td class="spaceTxt">&nbsp;</td></tr>
					<tr><td class="irightTxt3">&nbsp;Est. Equity (CLTV%)</td></tr>
					<tr><td class="irightTxt3">&nbsp;Est. Loan Amount</td></tr>
					<tr><td class="irightTxt3">&nbsp;Est. Settlement</td></tr>
					<tr><td class="irightTxt3">&nbsp;Est. 3rd Party Fees</td></tr>
					<tr><td class="spaceTxt">&nbsp;</td></tr>
					<tr><td class="ixrightTxt3">&nbsp;Est. Payment (P&amp;I)</td></tr>
					<tr><td class="irightTxt3">&nbsp;Property Taxes</td></tr>
					<tr><td class="irightTxt3">&nbsp;Hazard Insurance</td></tr>
					<tr><td class="irightTxt3">&nbsp;PMI, Flood, etc.</td></tr>
					<tr><td class="spaceTxt"><div style="border-bottom-color:Black; border-bottom-style:double; border-bottom-width:90%;">&nbsp;</div></td></tr>
					<tr><td class="ixrightTxt3">&nbsp;*MONTHLY PAYMENT</td></tr>
					<tr><td class="irightTxt3">&nbsp;Est. APR</td></tr>
					<tr><td class="spaceTxt">&nbsp;</td></tr>
					<tr><td class="spaceTxt"><div style="border-bottom-color:Black; border-bottom-style:double; border-bottom-width:90%;">&nbsp;</div></td></tr>
					<tr><td class="ixrightTxt3">&nbsp;**Est Cash in Hand</td></tr>
					<tr><td class="ixrightTxt3">&nbsp;Monthly Savings!</td></tr>
					<tr><td class="irightTxt3">&nbsp;Annual Savings!</td></tr>
					<tr><td><img src="images/clear.gif" width="1" height="4" alt="" /></td></tr>
				</table>
			</td><td><img src="images/clear.gif" width="8" height="1" alt="" /></td><td class="tbBoxOrg">
				<table cellpadding="0" cellspacing="0" border="0">
					<tr><td><img src="images/clear.gif" width="1" height="4" alt="" /></td></tr>
					<tr><td class="bxlgMidTxt2"><%=mTerm1%></td></tr>
					<tr><td class="blgMidTxt2"><%=mLoanType1%></td></tr>
					<tr><td class="blgMidTxt2"><%=mPoints1%></td></tr>
					<tr><td class="blgMidTxt2"><%=mIntRate1%></td></tr>
					<tr><td class="spaceTxt">&nbsp;</td></tr>
					<tr><td class="blgMidTxt2"><%=mEquity1%></td></tr>
					<tr><td class="blgMidTxt2"><%=mLoanAmt1%></td></tr>
					<tr><td class="blgMidTxt2"><%=mSettle1%></td></tr>
					<tr><td class="blgMidTxt2"><%=mPrePaids1%></td></tr>
					<tr><td class="spaceTxt">&nbsp;</td></tr>
					<tr><td class="bxlgMidTxt2"><%=mEstPmnt1%></td></tr>
					<tr><td class="blgMidTxt2"><%=mPropTax1%></td></tr>
					<tr><td class="blgMidTxt2"><%=mHzrdIns1%></td></tr>
					<tr><td class="blgMidTxt2"><%=mhIns1%></td></tr>
					<tr><td class="spaceTxt"><div style="border-bottom-color:Black; border-bottom-style:double; border-bottom-width:90%;">&nbsp;</div></td></tr>
					<tr><td class="bxlgMidTxt2"><%=mTotal1%>&nbsp;</td></tr>
					<tr><td class="blgMidTxt2"><%=mAPR1%>&nbsp;</td></tr>
					<tr><td class="spaceTxt">&nbsp;</td></tr>
					<tr><td class="spaceTxt"><div style="border-bottom-color:Black; border-bottom-style:double; border-bottom-width:90%;">&nbsp;</div></td></tr>
					<tr><td class="bxlgMidTxt2" colspan="3"><%=estCashBack1%>&nbsp;</td></tr>
					<tr><td class="bxlgMidTxt2" colspan="3"><%=mSavings1%>&nbsp;</td></tr>
					<tr><td class="blgMidTxt2" colspan="3"><%=aSavings1%>&nbsp;</td></tr>
					<tr><td><img src="images/clear.gif" width="1" height="4" alt="" /></td></tr>
				</table>
			</td><td><img src="images/clear.gif" width="8" height="1" alt="" /></td><td class="tbBoxBlu">
				<table cellpadding="0" cellspacing="0" border="0">
					<tr><td><img src="images/clear.gif" width="1" height="4" alt="" /></td></tr>
					<tr><td class="bxlgMidTxt2"><%=mTerm2%></td></tr>
					<tr><td class="blgMidTxt2"><%=mLoanType2%></td></tr>
					<tr><td class="blgMidTxt2"><%=mPoints2%></td></tr>
					<tr><td class="blgMidTxt2"><%=mIntRate2%></td></tr>
					<tr><td class="spaceTxt">&nbsp;</td></tr>
					<tr><td class="blgMidTxt2"><%=mEquity2%></td></tr>
					<tr><td class="blgMidTxt2"><%=mLoanAmt2%></td></tr>
					<tr><td class="blgMidTxt2"><%=mSettle2%></td></tr>
					<tr><td class="blgMidTxt2"><%=mPrePaids2%></td></tr>
					<tr><td class="spaceTxt">&nbsp;</td></tr>
					<tr><td class="bxlgMidTxt2"><%=mEstPmnt2%></td></tr>
					<tr><td class="blgMidTxt2"><%=mPropTax2%></td></tr>
					<tr><td class="blgMidTxt2"><%=mHzrdIns2%></td></tr>
					<tr><td class="blgMidTxt2"><%=mhIns2%></td></tr>
					<tr><td class="spaceTxt"><div style="border-bottom-color:Black; border-bottom-style:double; border-bottom-width:90%;">&nbsp;</div></td></tr>
					<tr><td class="bxlgMidTxt2"><%=mTotal2%>&nbsp;</td></tr>
					<tr><td class="blgMidTxt2"><%=mAPR2%>&nbsp;</td></tr>
					<tr><td class="spaceTxt">&nbsp;</td></tr>
					<tr><td class="spaceTxt"><div style="border-bottom-color:Black; border-bottom-style:double; border-bottom-width:90%;">&nbsp;</div></td></tr>
					<tr><td class="bxlgMidTxt2" colspan="3"><%=estCashBack2%>&nbsp;</td></tr>
					<tr><td class="bxlgMidTxt2" colspan="3"><%=mSavings2%>&nbsp;</td></tr>
					<tr><td class="blgMidTxt2" colspan="3"><%=aSavings2%>&nbsp;</td></tr>
					<tr><td><img src="images/clear.gif" width="1" height="4" alt="" /></td></tr>
				</table>
			</td><td><img src="images/clear.gif" width="8" height="1" alt="" /></td><td class="tbBoxGry">
				<table cellpadding="0" cellspacing="0" border="0">
					<tr><td><img src="images/clear.gif" width="1" height="4" alt="" /></td></tr>
					<tr><td class="bxlgMidTxt2"><%=mTerm3%></td></tr>
					<tr><td class="blgMidTxt2"><%=mLoanType3%></td></tr>
					<tr><td class="blgMidTxt2"><%=mPoints3%></td></tr>
					<tr><td class="blgMidTxt2"><%=mIntRate3%></td></tr>
					<tr><td class="spaceTxt">&nbsp;</td></tr>
					<tr><td class="blgMidTxt2"><%=mEquity3%></td></tr>
					<tr><td class="blgMidTxt2"><%=mLoanAmt3%></td></tr>
					<tr><td class="blgMidTxt2"><%=mSettle3%></td></tr>
					<tr><td class="blgMidTxt2"><%=mPrePaids3%></td></tr>
					<tr><td class="spaceTxt">&nbsp;</td></tr>
					<tr><td class="bxlgMidTxt2"><%=mEstPmnt3%></td></tr>
					<tr><td class="blgMidTxt2"><%=mPropTax3%></td></tr>
					<tr><td class="blgMidTxt2"><%=mHzrdIns3%></td></tr>
					<tr><td class="blgMidTxt2"><%=mhIns3%></td></tr>
					<tr><td class="spaceTxt"><div style="border-bottom-color:Black; border-bottom-style:double; border-bottom-width:90%;">&nbsp;</div></td></tr>
					<tr><td class="bxlgMidTxt2"><%=mTotal3%>&nbsp;</td></tr>
					<tr><td class="blgMidTxt2"><%=mAPR3%>&nbsp;</td></tr>
					<tr><td class="spaceTxt">&nbsp;</td></tr>
					<tr><td class="spaceTxt"><div style="border-bottom-color:Black; border-bottom-style:double; border-bottom-width:90%;">&nbsp;</div></td></tr>
					<tr><td class="bxlgMidTxt2" colspan="3"><%=estCashBack3%>&nbsp;</td></tr>
					<tr><td class="bxlgMidTxt2" colspan="3"><%=mSavings3%>&nbsp;</td></tr>
					<tr><td class="blgMidTxt2" colspan="3"><%=aSavings3%>&nbsp;</td></tr>
					<tr><td><img src="images/clear.gif" width="1" height="4" alt="" /></td></tr>
				</table>
			</td><td><img src="images/clear.gif" width="3" height="1" alt="" />
			</td></tr>
			</table>
		</td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr style="background-color:#007db1;"><td><img src="images/clear.gif" height="2" /></td></tr>
		<tr><td class="noteTxt" style="text-align:left; line-height:12px;">*Taxes and insurance included</td></tr>
		<tr><td class="noteTxt" style="text-align:left; line-height:8px;">**Assumes your property taxes and homeowner's insurance are paid and no impound</td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td align="left"><span class="sectionHdr">THE OVATION ADVANTAGE</span></td></tr>
		<tr><td><img src="images/clear.gif" height="2" /></td></tr>
		<tr><td>
			<table cellpadding="0" cellspacing="0" border="0">
			<tr><td><img src="images/clear.gif" width="80" height="1" alt="" /></td><td align="left" class="advHdr">
				<img src="images/Checkmark.png" width="23" height="19" alt="" />Fast Loan Funding<br />
				<img src="images/Checkmark.png" width="23" height="19" alt="" />Conventional, FHA, VA, and Jumbo Loans<br />
				<img src="images/Checkmark.png" width="23" height="19" alt="" />Experienced Mortgage Banking Professionals
			</td><td><img src="images/clear.gif" width="50" height="1" alt="" /></td><td align="left" class="advHdr">
				<img src="images/Checkmark.png" width="23" height="19" alt="" />Low Rates and Fees<br />
				<img src="images/Checkmark.png" width="23" height="19" alt="" />Great Customer Service<br />
				<img src="images/Checkmark.png" width="23" height="19" alt="" />Excellent BBB Rating
			</td><td><img src="images/clear.gif" width="80" height="1" alt="" />
			</td></tr>
			</table>
		</td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr style="background-color:#007db1;"><td><img src="images/clear.gif" height="2" /></td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td align="center">
			<table cellpadding="0" cellspacing="0" border="0">
			<tr><td>
				<table cellpadding="0" cellspacing="0" border="0">
					<tr style="font-family:Arial Black, Franklin Gothic Demi, Arial; font-size:8pt; color:#7c6a55"><td align="left">OVATION HOME LOANS</td></tr>
					<tr style="font-family:Arial, Lucidia Fax, Franklin Gothic Medium; font-size:8pt; color:#363436"><td align="left">3130 Harbor Blvd.</td></tr>
					<tr style="font-family:Arial, Lucidia Fax, Franklin Gothic Medium; font-size:8pt; color:#363436"><td align="left">Suite 300</td></tr>
					<tr style="font-family:Arial, Lucidia Fax, Franklin Gothic Medium; font-size:8pt; color:#363436"><td align="left">Santa Ana, CA 92704</td></tr>
					<tr style="font-family:Arial, Lucidia Fax, Franklin Gothic Medium; font-size:8pt; color:#363436"><td align="left">866-264-7599</td></tr>
				</table>
			</td><td><img src="images/clear.gif" width="35" height="1" alt="" /></td><td>
				<table cellpadding="0" cellspacing="0" border="0">
					<tr style="font-family:Arial Black, Franklin Gothic Demi, Arial; font-size:8pt; color:#7c6a55"><td align="left">YOUR LOAN OFFICER</td></tr>
					<tr style="font-family:Arial, Lucidia Fax, Franklin Gothic Medium; font-size:8pt; color:#363436"><td align="left"><%=lo_Name%></td></tr>
					<tr style="font-family:Arial, Lucidia Fax, Franklin Gothic Medium; font-size:8pt; color:#363436"><td align="left">Fx <%=lo_Fax%></td></tr>
					<tr style="font-family:Arial, Lucidia Fax, Franklin Gothic Medium; font-size:8pt; color:#363436"><td align="left"><%=lo_Email%></td></tr>
				</table>
			</td><td><img src="images/clear.gif" width="15" height="1" alt="" /></td><td>
				<table cellpadding="0" cellspacing="0" border="0">
					<tr style="font-family:Arial Black, Franklin Gothic Demi, Arial; font-size:8pt; color:#7c6a55"><td align="left">APPLY ONLINE</td></tr>
					<tr style="font-family:Arial, Lucidia Fax, Franklin Gothic Medium; font-size:8pt; color:#363436"><td align="left">www.OVATIONHOMELOANS.com</td></tr>
				</table>
			</td><td><img src="images/clear.gif" width="15" height="1" alt="" /></td><td>
				<table cellpadding="0" cellspacing="0" border="0">
					<tr style="font-family:Arial Black, Franklin Gothic Demi, Arial; font-size:8pt; color:#7c6a55"><td align="left">TOLL FREE PHONE</td></tr>
					<tr style="font-family:Arial, Lucidia Fax, Franklin Gothic Medium; font-size:8pt; color:#363436"><td align="left"><%=lo_TollFreePhone%></td></tr>
				</table>
			</td></tr>
			</table>
		</td></tr>
		<tr><td align="right" valign="middle">
			<table cellpadding="0" cellspacing="0" border="0">
			<tr style="text-align:right; vertical-align:middle;"><td>
				<table cellpadding="0" cellspacing="0" border="0">
					<tr style="font-family:Arial; font-size:6pt; color:#666666; vertical-align:middle;"><td align="right" valign="middle">&nbsp;<br />
					OVATION HOME LOANS IS A DIVISION OF CARNEGIE MORTGAGE LLC, A WHOLLY-OWNED SUBSIDIARY OF GRAND BANK, N.A.
					</td></tr>
					<tr style="font-family:Arial; font-size:6pt; color:#666666; vertical-align:middle;"><td align="right" valign="middle">
					THE RATE ABOVE IS BASED ON INDUSTRY RATES AS OF <%=today%>. RATES SUBJECT TO CHANGE.
					</TD><tr style="font-family:Arial; font-size:6pt; color:#666666; vertical-align:middle;"><td align="right" valign="middle">
					PREPAID INTEREST AND IMPOUNDS FOR TAXES/INSURANCE ARE NOT INCLUDED IN THIS ESTIMATE.
					</td></tr>
				</table>
			</td>
			<td>
				<img src="images/equalhousinglender.png" border="0" alt="Equal Housing Lender" />
				<img src="images/bbb.jpg" border="0" alt="BBB" />
			</td></tr>
			</table>
		</td><td width="5px">&nbsp;</td></tr>		
    </table>
</body>
</html>
