﻿<%@ Page Language="C#" Debug="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
private readonly string roTblWidth="60em";
	
protected void Page_Load(object sender, EventArgs e)
{
	loadDataFromPost();
	//get form element values and update calculations
	Page.Validate();
	if (Page.IsValid)
	{
	}
}

protected void Page_Transfer(object sender, EventArgs e)
{
	//on submit create quote and display for printing
	Server.Transfer("ovaYourQuote_6.aspx", true);
}

protected void copyDataTo2(object sender, EventArgs e)
{
		//copy data from option #1 to option #2
	optHdr2.SelectedIndex = optHdr1.SelectedIndex;
	optLoanType2.SelectedIndex = optLoanType1.SelectedIndex;
	tb_Points2.Text = tb_Points1.Text;
	tb_Rate2.Text = tb_Rate1.Text;
	tb_mLoanAmt2.Text = tb_mLoanAmt1.Text;
	tb_mSettle2.Text = tb_mSettle1.Text;
	tb_mPrePaids2.Text = tb_mPrePaids1.Text;
	tb_PMI2.Text = tb_PMI1.Text;
}

protected void copyDataTo3(object sender, EventArgs e)
{
	//copy data from option #2 to option #3
	optHdr3.SelectedIndex = optHdr2.SelectedIndex;
	optLoanType3.SelectedIndex = optLoanType2.SelectedIndex;
	tb_Points3.Text = tb_Points2.Text;
	tb_Rate3.Text = tb_Rate2.Text;
	tb_mLoanAmt3.Text = tb_mLoanAmt2.Text;
	tb_mSettle3.Text = tb_mSettle2.Text;
	tb_mPrePaids3.Text = tb_mPrePaids2.Text;
	tb_PMI3.Text = tb_PMI2.Text;
}
	
protected void loadTestData(object sender, EventArgs e)
{
	//load test data for testing purposes
	custName.Text = "Jill Prospect";
	custAddr.Text = "123 Main";
	custCSZ.Text = "City, ST  12345";
	custEmail.Text = "jpropsect@somecomp.com";
	
	loName.Text = "Joe Loanofficer";
	loTollFreePhone.Text = "888-123-4567 x9876";
	loFax.Text = "949-111-1112";
	loEmail.Text = "jloanofficer@ovationhomeloans.com";

	fMortBal.Text = "127,500";
	fPmt.Text = "1,400.00";
	sMortBal.Text = "27,500";	
	sPmt.Text = "300.00";
	pTaxPmt.Text = "100.00";
	hzrdInsPmt.Text = "75.00";
	pmiPmt.Text = "103.00";
	oLiabBal.Text = "25,000";
	oLiabPmt.Text = "525.00";
	cHomeVal.Text = "300,000";

	optHdr1.SelectedValue = "30";
	optLoanType1.SelectedValue="Fixed";
	tb_Points1.Text = "1.5";
	tb_Rate1.Text = "3.875";
	tb_mLoanAmt1.Text = "220,000";
	tb_mSettle1.Text = "3,800";
	tb_mPrePaids1.Text = "4,630";
	tb_PMI1.Text = "147";
}

private double cvtRtnNumeric(string val)
{
	//convert numeric to 1,000,000.00 format
	if (val == null || val == "") { return 0; }
	Regex digitsDecOnly = new Regex(@"[^\d\.]");
	val = digitsDecOnly.Replace(val, "");
	if (val == "") { return 0; }
	double ival = Convert.ToDouble(val);
	return ival;
}

protected void fMortBal_TextChanged(object sender, EventArgs e)
{
	//update est loan amount in #1 when mortgage values changed or entered
	eLoanAmt_Update();
	fPmt.Focus();
}

protected void sMortBal_TextChanged(object sender, EventArgs e)
{
	//update est loan amount in #1 when mortgage values changed or entered
	eLoanAmt_Update();
	sPmt.Focus();
}

protected void oLiabBal_TextChanged(object sender, EventArgs e)
{
	//update est loan amount in #1 when mortgage values changed or entered
	eLoanAmt_Update();
	oLiabPmt.Focus();
}
	
protected void eLoanAmt_Update()
{
	//update est loan amount in #1 when mortgage values changed or entered
	double eLoanAmt = cvtRtnNumeric(fMortBal.Text) + cvtRtnNumeric(sMortBal.Text) + cvtRtnNumeric(oLiabBal.Text);
	tb_mLoanAmt1.Text = eLoanAmt.ToString("#,#.00");
}

protected void loadDataFromPost() 
{
    if (Page.Request.Form["transferIn"] != null)
    {
        custName.Text = Page.Request.Form["custName"];
        custAddr.Text = Page.Request.Form["custAddr"];
        custCSZ.Text = Page.Request.Form["custCSZ"];
        custEmail.Text = Page.Request.Form["custEmail"];

        loName.Text = Page.Request.Form["loName"];
        loTollFreePhone.Text = Page.Request.Form["loTollFreePhone"];
        //loDirectPhone.Text = Page.Request.Form["loDirectPhone"];
        loFax.Text = Page.Request.Form["loFax"];
        loEmail.Text = Page.Request.Form["loEmail"];

        fMortBal.Text = Page.Request.Form["fMortBal"];
        fPmt.Text = Page.Request.Form["fPmt"];
        sMortBal.Text = Page.Request.Form["sMortBal"];
        sPmt.Text = Page.Request.Form["sPmt"];
        //pTaxBal.Text = Page.Request.Form["pTaxBal"];
        pTaxPmt.Text = Page.Request.Form["pTaxPmt"];
        //hzrdInsBal.Text = Page.Request.Form["hzrdInsBal"];
        hzrdInsPmt.Text = Page.Request.Form["hzrdInsPmt"];
        //insBal.Text = Page.Request.Form["insBal"];
        //insPmt.Text = Page.Request.Form["insPmt"];
        oLiabBal.Text = Page.Request.Form["oLiabBal"];
        oLiabPmt.Text = Page.Request.Form["oLiabPmt"];
        cHomeVal.Text = Page.Request.Form["cHomeVal"];
    }

}
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Loan Options</title>
    <meta name="robots" content="noindex,nofollow,noarchive,nosnippet,nodp,noydir" />
	<script type="text/javascript">
		function resetFields() {
			var inputs = document.getElementsByTagName('input');
			var selects = document.getElementsByTagName('select');
			for (var k = 0; k < inputs.length; k++) {
				var input = inputs[k]
				if (input.type == 'text') {
					if (input.value != '') { input.value = ''; }
				}
			}
			for (k = 0; k < selects.length; k++) {
				var ele = selects[k]
				if (ele.type == 'select-one') {
					ele.selectedIndex = -1;
				}
			}
		}

		function displayBackgroundInstructions() {
			newWin = window.open('displayBackgroundInstructions.html', 'bgroundInstr', 'width=700,height=700,scrollbars=yes,resizable=yes,menubar=yes')
			if (newWin.focus) { newWin.focus() }
			return false;
		}
	</script>
    <style type="text/css">
		body {
			background:white; color:black;
			margin-left:10%; margin-right:10%;
			font-size:64%; font-family:Arial,"Times New Roman",Georgia,Serif;
		}
		table, td, th{ border: 0px solid black; }
		table { padding:0; border-collapse:collapse; width:100%; }
		tr { vertical-align:top; text-align:center; }
		td { white-space:nowrap; }
		a { text-decoration: none; }
		.sectionHdr { font-family:Arial Black, Franklin Gothic Demi, Arial; font-weight:bold; font-size:8pt; color:#f96400; text-align:left; }
		.addrHdr { font-size:1.25em; font-family:Arial Black, Franklin Gothic Demi, Arial; font-weight:bold; vertical-align:middle; text-align:center; color:#f96400; }
		.addrHdrG { font-size:1.25em; font-family:Arial Black, Franklin Gothic Demi, Arial; font-weight:bold; vertical-align:middle; text-align:center; color:#7c6a55; }
		.addrHdrW { font-size:1.25em; font-weight:bold; vertical-align:middle; text-align:center; color:#fff; }
		.boldTxt { font-weight:bold; }
		.midTxt { text-align:center; font-size: 0.95em; }
		.vmidTxt { text-align:center; font-size: 0.9em; }
		.bmidTxt { text-align:center; font-weight:bold; font-size: 0.95em; }
		.blgMidTxt { text-align:center; font-weight:bold; font-size: 1.15em; }
		.bleftTxt { text-align:left; font-weight:bold; font-size: 0.95em; }
		.blgLeftTxt { text-align:left; font-weight:bold; font-size: 1.15em; }
		.leftTxt { text-align:left; font-size: 0.95em; }
		.lgLeftTxt { text-align:left; font-size: 1.15em; }
		.vleftTxt { text-align:left; font-size: 0.9em; }
		.vleft2Txt { text-align:left; font-size: 0.95em; }
		.bvleftTxt { text-align:left; font-size: 0.9em; font-weight:bold; }
		.bvlgLeftTxt { text-align:left; font-size: 1.1698em; font-weight:bold; }
		.ileftTxt { text-align:left; font-style:italic; vertical-align:middle; }
		.ileftTxt2 { text-align:left; font-style:italic; vertical-align:middle; height:14px; }
		.ileftTxt3 { text-align:left; font-style:italic; font-weight:bold; vertical-align:middle; height:16.5px; }
		.irightTxt3 { text-align:right; font-style:italic; font-weight:bold; vertical-align:middle; height:16.5px; }
		.blgMidTxt2 { text-align:center; font-weight:bold; vertical-align:middle; height:16.5px; }
		.ilgLeftTxt { text-align:left; font-style:italic; font-size:1.15em; }
		.rightTxt { text-align:right; font-size: 0.9em; }
		.lbRightTxt { text-align:right; font-size: 1.1em; }
		.spaceTxt { text-align:right; font-size: 0.4em; }
		.space2Txt { text-align:right; font-size: 0.35em; }
		.spaceLgTxt { font-size: 1.25em; }
		.brightTxt { text-align:right; font-size: 0.9em; font-weight:bold; }
		.blgRightTxt { text-align:right; font-size: 1.1em; font-weight:bold; }
		.noteTxt { font-size:75%; font-style:italic; }
		.ltNoteTxt { font-size:75%; text-align:left; font-style:italic; }
		.ctrImg { text-align:center; vertical-align:middle }
		.box { border: 2px solid black; width:100%; empty-cells:show; padding:2px; }
		.cbox { border: 2px solid white; width:100%; empty-cells:show; }
		.boxd { border: 2px solid black; height:17em; empty-cells:show; }
		.boxd2 { border: 2px solid black; height:12em; empty-cells:show; }
		.obox { border: 2px solid black; width:100%; height:2em; empty-cells:show; pading:-4px; }
		.ctbox1 { border:solid; border-width:thin; width:100%; border-color:White; height:2em; }
		.ctbox { border: 2px solid white; width:100%; height:2em; empty-cells:show; }
		.tbBoxOrg { border:0px; background: url(images/orgGradient.png) repeat; }
		.tbBoxBlu { border:0px; background: url(images/bluGradient.png) repeat; }
		.tbBoxGry { border:0px; background: url(images/gryGradient.png) repeat; }
    </style>
</head>
<body>
    <form id="frm1" runat="server">
    <div>
    <table cellpadding="0" cellspacing="0" border="0" style="width:<%=roTblWidth%>;"><tr><td>
		<table cellpadding="0" cellspacing="0" border="0"><tr>
			<td width="33%"><img src="images/ovaLogo2.png" border="0" alt="Ovation Logo" height="64" width="185" /></td>
			<td width="34%"><img src="images/ovation-logo.png" border="0" alt="Ovation Logo" height="62" width="175" /></td>
			<td width="33%" class="addrHdrG">3130 Harbor Blvd.<br />Suite 300<br />Santa Ana, CA 92704</td>
		</tr></table>
	</td></tr>
	<tr><td>&nbsp;</td></tr>
	<tr><td>
		<table cellpadding="0" cellspacing="0" border="0">
		<tr><td class="boxd2">
			<table cellpadding="0" cellspacing="0" border="0">
				<tr><td class="addrHdr" colspan="4">Prospect Contact<br />&nbsp;</td></tr>
				<tr><td class="ileftTxt" width="35%"><img src="images/clear.gif" border="0" width="10em" />Prepared for:</td><td>&nbsp;</td><td colspan="2" class="leftTxt" width="80%">
					<asp:TextBox ID="custName" runat="server" AutoPostBack="False" Width="190" 
						Wrap="False" Height="1.1em" Font-Size="X-Small" />
					</td></tr>
				<tr><td class="ileftTxt" width="35%"><img src="images/clear.gif" border="0" width="10em" />Property Address:</td><td>&nbsp;</td><td colspan="2" class="leftTxt">
					<asp:TextBox ID="custAddr" runat="server" AutoPostBack="False" Width="190" 
						Wrap="False" Height="1.1em" Font-Size="X-Small" />
					</td></tr>
				<tr><td class="ileftTxt" width="35%"><img src="images/clear.gif" border="0" width="10em" />City, State, Zip:</td><td>&nbsp;</td><td colspan="2" class="leftTxt">
					<asp:TextBox ID="custCSZ" runat="server" AutoPostBack="False" Width="190" 
						Wrap="False" Height="1.1em" Font-Size="X-Small" />
					</td></tr>
				<tr><td class="ileftTxt" width="35%"><img src="images/clear.gif" border="0" width="10em" />Customer Email:</td><td>&nbsp;</td><td colspan="2" class="leftTxt">
					<asp:TextBox ID="custEmail" runat="server" AutoPostBack="False" Width="190" 
						Wrap="False" Height="1.1em" Font-Size="X-Small" />
					</td></tr>
			</table>
		</td>
		<td class="boxd2">
			<table cellpadding="0" cellspacing="0" border="0">
				<tr><td class="addrHdr" colspan="4">Loan Officer Contact<br />&nbsp;</td></tr>
				<tr><td class="ileftTxt" width="30%">&nbsp;&nbsp;Prepared by:</td><td>&nbsp;</td><td colspan="2" class="leftTxt" width="80%">
					<asp:TextBox ID="loName" runat="server" AutoPostBack="False" Width="195" 
						Wrap="False" Height="1.1em" Font-Size="X-Small" />&nbsp;&nbsp;
					</td></tr>
				<tr><td class="ileftTxt" width="30%">&nbsp;&nbsp;Toll Free Phone:</td><td>&nbsp;</td><td colspan="2" class="leftTxt">
					<asp:TextBox ID="loTollFreePhone" runat="server" AutoPostBack="False" Width="195" 
						Wrap="False" Height="1em" Font-Size="X-Small" />&nbsp;&nbsp;
					</td></tr>
				<tr><td class="ileftTxt" width="30%">&nbsp;&nbsp;Fax:</td><td>&nbsp;</td><td colspan="2" class="leftTxt">
					<asp:TextBox ID="loFax" runat="server" AutoPostBack="False" Width="195" 
						Wrap="False" Height="1.1em" Font-Size="X-Small" />&nbsp;&nbsp;
					</td></tr>
				<tr><td class="ileftTxt" width="30%">&nbsp;&nbsp;Email:</td><td>&nbsp;</td><td colspan="2" class="lgLeftTxt">
					<asp:TextBox ID="loEmail" runat="server" AutoPostBack="False" Width="195" 
						Wrap="False" Height="1.1em" Font-Size="X-Small" />&nbsp;&nbsp;
					</td></tr>
			</table>
		</td></tr>
		<tr><td><img src="images/clear.gif" width="1" height="4" alt="" /></td></tr>
		<tr><td colspan="2" class="boxd">
			<table cellpadding="0" cellspacing="0" border="0">
				<tr class="addrHdr"><td width="27%">Mortgage Liens</td><td width="3%">&nbsp;</td><td width="37%">Balance</td><td width="3%">&nbsp;</td><td width="20%">Payment</td><td width="10%">&nbsp;</td></tr>
				<tr><td class="ileftTxt">&nbsp;Existing 1st Mortgage</td><td>&nbsp;</td><td width="36%">
					<asp:TextBox ID="fMortBal" runat="server" AutoPostBack="true" Width="90%" 
						Wrap="False" Height="1em" Font-Size="XX-Small" OnTextChanged="fMortBal_TextChanged" />
					</td><td width="5%">&nbsp;</td><td class="rightTxt" width="20%">
					<asp:TextBox ID="fPmt" runat="server" AutoPostBack="False" Width="90%" 
						Wrap="False" Height="1em" Font-Size="XX-Small" />
					</td><td>&nbsp;</td></tr>
				<tr><td class="ileftTxt">&nbsp;Existing 2nd Mortgage</td><td>&nbsp;</td><td>
					<asp:TextBox ID="sMortBal" runat="server" AutoPostBack="true" Width="90%" 
						Wrap="False" Height="1em" Font-Size="XX-Small" OnTextChanged="sMortBal_TextChanged" />
					</td><td>&nbsp;</td><td class="rightTxt">
					<asp:TextBox ID="sPmt" runat="server" AutoPostBack="False" Width="90%" 
						Wrap="False" Height="1em" Font-Size="XX-Small" />
					</td><td>&nbsp;</td></tr>
				<tr><td class="ileftTxt">&nbsp;Property Taxes</td><td>&nbsp;</td><td>
					. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
					</td><td>&nbsp;</td><td class="rightTxt">
					<asp:TextBox ID="pTaxPmt" runat="server" AutoPostBack="False" Width="90%" 
						Wrap="False" Height="1em" Font-Size="XX-Small" />
					</td><td>&nbsp;</td></tr>
				<tr><td class="ileftTxt">&nbsp;Hazard Ins, Flood Ins, etc.</td><td>&nbsp;</td><td>
					. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
					</td><td>&nbsp;</td><td class="rightTxt">
					<asp:TextBox ID="hzrdInsPmt" runat="server" AutoPostBack="False" Width="90%" 
						Wrap="False" Height="1em" Font-Size="XX-Small" />
					</td><td>&nbsp;</td></tr>
				<tr><td class="ileftTxt">&nbsp;Existing PMI</td><td>&nbsp;</td><td>
					. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
					</td><td>&nbsp;</td><td class="rightTxt">
					<asp:TextBox ID="pmiPmt" runat="server" AutoPostBack="False" Width="90%" 
						Wrap="False" Height="1em" Font-Size="XX-Small" />
					</td><td>&nbsp;</td></tr>
				<tr class="spaceTxt"><td>&nbsp;</td><td colspan="4"></td></tr>
				<tr><td class="addrHdr">&nbsp;Debt to be paid off:</td>
					<td>&nbsp;</td><td>&nbsp;</td><td class="rightTxt">&nbsp;
					</td><td>&nbsp;</td></tr>
				<tr><td class="ileftTxt">&nbsp;Credit Cards, Auto, etc.</td><td>&nbsp;</td><td>
					<asp:TextBox ID="oLiabBal" runat="server" AutoPostBack="true" Width="90%" 
						Wrap="False" Height="1em" Font-Size="XX-Small" OnTextChanged="oLiabBal_TextChanged" />
					</td><td>&nbsp;</td><td class="rightTxt">
					<asp:TextBox ID="oLiabPmt" runat="server" AutoPostBack="False" Width="90%" 
						Wrap="False" Height="1em" Font-Size="XX-Small" />
					</td>
					<td>&nbsp;</td>
				</tr>
				<tr class="spaceTxt"><td colspan="5">&nbsp;</td></tr>
				<tr><td class="blgLeftTxt" colspan="2">&nbsp;Current Home Value</td><td>
					<asp:TextBox ID="cHomeVal" runat="server" AutoPostBack="False" Width="90%" 
						Wrap="False" Height="1em" Font-Size="XX-Small" />
					</td><td>&nbsp;</td></tr>
				<!--<tr class="spaceTxt"><td colspan="5">&nbsp;</td></tr>-->
			</table>
		</td></tr>
		<tr><td colspan="2">
			<table cellpadding="0" cellspacing="0" border="0">
			<tr><td><img src="images/clear.gif" width="1" height="6" alt="" /></td></tr>
			<tr><td>
				<span class="sectionHdr">YOUR 3 LOAN OPTIONS</span>
			</td><td><img src="images/clear.gif" width="10" height="1" alt="" /></td><td width="160">
				<img src="images/loanOption1a.png" width="160" height="36" alt="Option #1" style="border:0px; border-bottom:2px solid white;" />
			</td><td><img src="images/clear.gif" width="10" height="1" alt="" /></td><td width="160">
				<img src="images/loanOption2a.png" width="160" height="36" alt="Option #2" style="border:0px; border-bottom:2px solid white;" />
			</td><td><img src="images/clear.gif" width="10" height="1" alt="" /></td><td width="160">
				<img src="images/loanOption3a.png"width="160" height="36" alt="Option #3" style="border:0px; border-bottom:2px solid white;" />
			</td></tr>
			<tr><td>
				<table cellpadding="0" cellspacing="0" border="0">
					<tr><td class="addrHdr"><img src="images/clear.gif" border="0" width="120px" height="20px" /></td></tr>
					<tr><td class="irightTxt3" style="height:20px;">Term (Years)&nbsp;</td></tr>
					<tr><td class="irightTxt3" style="height:20px;">Loan Type&nbsp;</td></tr>
					<tr><td class="irightTxt3" style="height:20px;">Points&nbsp;</td></tr>
					<tr><td class="irightTxt3" style="height:20px;">Interest Rate (%)&nbsp;</td></tr>
					<tr><td class="spaceTxt"><img src="images/clear.gif" border="0" width="1px" height="4px" /></td></tr>
					<tr><td class="irightTxt3" style="height:20px;">Est. Loan Amount&nbsp;</td></tr>
					<tr><td class="irightTxt3" style="height:20px;">Est. Settlement&nbsp;</td></tr>
					<tr><td class="irightTxt3" style="height:20px;">Est. 3rd Party Fees&nbsp;</td></tr>
					<tr><td class="irightTxt3" style="height:20px;">Monthly PMI&nbsp;</td></tr>
					<tr><td class="spaceTxt"><img src="images/clear.gif" border="0" width="1px" height="4px" /></td></tr>
				</table>
			</td><td>&nbsp;</td><td class="tbBoxOrg">
				<table cellpadding="0" cellspacing="0" border="0">
					<tr><td class="addrHdr" style="height:20px;">
						<asp:Button ID="Copy2Button" runat="server" Text="Copy to Option #2" ToolTip="Copy data to Option #2" OnClick="copyDataTo2" Height="20px" Width="120px" Font-Size="X-Small" />
					</td></tr>
					<tr><td class="blgMidTxt2" style="height:20px;">
						<asp:DropDownList ID="optHdr1" Font-Size="X-Small" runat="server" Width="12em">
						<asp:ListItem Value=""></asp:ListItem>
						<asp:ListItem Value="3">3 Yr</asp:ListItem>
						<asp:ListItem Value="5">5 Yr</asp:ListItem>
						<asp:ListItem Value="7">7 Yr</asp:ListItem>
						<asp:ListItem Value="10">10 Yr</asp:ListItem>
						<asp:ListItem Value="15">15 Yr</asp:ListItem>
						<asp:ListItem Value="20">20 Yr</asp:ListItem>
						<asp:ListItem Value="25">25 Yr</asp:ListItem>
						<asp:ListItem Value="30">30 Yr</asp:ListItem>
						</asp:DropDownList>
					</td></tr>
					<tr><td class="blgMidTxt2" style="height:20px;">
						<asp:DropDownList ID="optLoanType1" Font-Size="X-Small" runat="server" Width="12em">
						<asp:ListItem Value=""></asp:ListItem>
						<asp:ListItem Value="Fixed">Fixed</asp:ListItem>
						<asp:ListItem Value="Arm">Arm</asp:ListItem>
						<asp:ListItem Value="Int Only">Int Only</asp:ListItem>
						<asp:ListItem Value="FHA">FHA</asp:ListItem>
						<asp:ListItem Value="FHA">VA</asp:ListItem>
						</asp:DropDownList>
					</td></tr>
					<tr><td class="blgMidTxt2" style="height:20px;"><asp:TextBox ID="tb_Points1" runat="server" AutoPostBack="False" Width="70" 
							Wrap="False" Height="1.1em" Font-Size="X-Small" /></td></tr>
					<tr><td class="blgMidTxt2" style="height:20px;"><asp:TextBox ID="tb_Rate1" runat="server" AutoPostBack="False" Width="70" 
							Wrap="False" Height="1.1em" Font-Size="X-Small" /></td></tr>
					<tr><td class="spaceTxt"><img src="images/clear.gif" border="0" width="1px" height="4px" /></td></tr>
					<tr><td class="blgMidTxt2" style="height:20px;"><asp:TextBox ID="tb_mLoanAmt1" runat="server" AutoPostBack="False" Width="70" 
							Wrap="False" Height="1.1em" Font-Size="X-Small" /></td></tr>
					<tr><td class="blgMidTxt2" style="height:20px;"><asp:TextBox ID="tb_mSettle1" runat="server" AutoPostBack="False" Width="70" 
							Wrap="False" Height="1.1em" Font-Size="X-Small" /></td></tr>
					<tr><td class="blgMidTxt2" style="height:20px;"><asp:TextBox ID="tb_mPrePaids1" runat="server" AutoPostBack="False" Width="70" 
							Wrap="False" Height="1.1em" Font-Size="X-Small" /></td></tr>
					<tr><td class="blgMidTxt2" style="height:20px;"><asp:TextBox ID="tb_PMI1" runat="server" AutoPostBack="False" Width="70" 
							Wrap="False" Height="1.1em" Font-Size="X-Small" /></td></tr>
					<tr><td class="spaceTxt"><img src="images/clear.gif" border="0" width="1px" height="4px" /></td></tr>
				</table>
			</td><td>&nbsp;</td><td class="tbBoxBlu">
				<table cellpadding="0" cellspacing="0" border="0">
					<tr><td class="addrHdr" style="height:20px;">
						<asp:Button ID="Copy3Button" runat="server" Text="Copy to Option #3" ToolTip="Copy data to Option #3" OnClick="copyDataTo3" Height="20px" Width="120px" Font-Size="X-Small" />
					</td></tr>
					<tr><td class="blgMidTxt2" style="height:20px;">
						<asp:DropDownList ID="optHdr2" Font-Size="X-Small" runat="server" Width="12em">
						<asp:ListItem Value=""></asp:ListItem>
						<asp:ListItem Value="3">3 Yr</asp:ListItem>
						<asp:ListItem Value="5">5 Yr</asp:ListItem>
						<asp:ListItem Value="7">7 Yr</asp:ListItem>
						<asp:ListItem Value="10">10 Yr</asp:ListItem>
						<asp:ListItem Value="15">15 Yr</asp:ListItem>
						<asp:ListItem Value="20">20 Yr</asp:ListItem>
						<asp:ListItem Value="25">25 Yr</asp:ListItem>
						<asp:ListItem Value="30">30 Yr</asp:ListItem>
						</asp:DropDownList>
					</td></tr>
					<tr><td class="blgMidTxt2" style="height:20px;">
						<asp:DropDownList ID="optLoanType2" Font-Size="X-Small" runat="server" Width="12em">
						<asp:ListItem Value=""></asp:ListItem>
						<asp:ListItem Value="Fixed">Fixed</asp:ListItem>
						<asp:ListItem Value="Arm">Arm</asp:ListItem>
						<asp:ListItem Value="Int Only">Int Only</asp:ListItem>
						<asp:ListItem Value="FHA">FHA</asp:ListItem>
						<asp:ListItem Value="FHA">VA</asp:ListItem>
						</asp:DropDownList>
					</td></tr>
					<tr><td class="blgMidTxt2" style="height:20px;"><asp:TextBox ID="tb_Points2" runat="server" AutoPostBack="False" Width="70" 
							Wrap="False" Height="1.1em" Font-Size="X-Small" /></td></tr>
					<tr><td class="blgMidTxt2" style="height:20px;"><asp:TextBox ID="tb_Rate2" runat="server" AutoPostBack="False" Width="70" 
							Wrap="False" Height="1.1em" Font-Size="X-Small" /></td></tr>
					<tr><td class="spaceTxt"><img src="images/clear.gif" border="0" width="1px" height="4px" /></td></tr>
					<tr><td class="blgMidTxt2" style="height:20px;"><asp:TextBox ID="tb_mLoanAmt2" runat="server" AutoPostBack="False" Width="70" 
							Wrap="False" Height="1.1em" Font-Size="X-Small" /></td></tr>
					<tr><td class="blgMidTxt2" style="height:20px;"><asp:TextBox ID="tb_mSettle2" runat="server" AutoPostBack="False" Width="70" 
							Wrap="False" Height="1.1em" Font-Size="X-Small" /></td></tr>
					<tr><td class="blgMidTxt2" style="height:20px;"><asp:TextBox ID="tb_mPrePaids2" runat="server" AutoPostBack="False" Width="70" 
							Wrap="False" Height="1.1em" Font-Size="X-Small" /></td></tr>
					<tr><td class="blgMidTxt2" style="height:20px;"><asp:TextBox ID="tb_PMI2" runat="server" AutoPostBack="False" Width="70" 
							Wrap="False" Height="1.1em" Font-Size="X-Small" /></td></tr>
					<tr><td class="spaceTxt"><img src="images/clear.gif" border="0" width="1px" height="4px" /></td></tr>
				</table>
			</td><td>&nbsp;</td><td class="tbBoxGry">
				<table cellpadding="0" cellspacing="0" border="0">
					<tr><td class="addrHdr"><img src="images/clear.gif" border="0" width="120px" height="20px" /></td></tr>
					<tr><td class="blgMidTxt2" style="height:20px;">
						<asp:DropDownList ID="optHdr3" Font-Size="X-Small" runat="server" Width="12em">
						<asp:ListItem Value=""></asp:ListItem>
						<asp:ListItem Value="3">3 Yr</asp:ListItem>
						<asp:ListItem Value="5">5 Yr</asp:ListItem>
						<asp:ListItem Value="7">7 Yr</asp:ListItem>
						<asp:ListItem Value="10">10 Yr</asp:ListItem>
						<asp:ListItem Value="15">15 Yr</asp:ListItem>
						<asp:ListItem Value="20">20 Yr</asp:ListItem>
						<asp:ListItem Value="25">25 Yr</asp:ListItem>
						<asp:ListItem Value="30">30 Yr</asp:ListItem>
						</asp:DropDownList>
					</td></tr>
					<tr><td class="blgMidTxt2" style="height:20px;">
						<asp:DropDownList ID="optLoanType3" Font-Size="X-Small" runat="server" Width="12em">
						<asp:ListItem Value=""></asp:ListItem>
						<asp:ListItem Value="Fixed">Fixed</asp:ListItem>
						<asp:ListItem Value="Arm">Arm</asp:ListItem>
						<asp:ListItem Value="Int Only">Int Only</asp:ListItem>
						<asp:ListItem Value="FHA">FHA</asp:ListItem>
						<asp:ListItem Value="FHA">VA</asp:ListItem>
						</asp:DropDownList>
					</td></tr>
					<tr><td class="blgMidTxt2" style="height:20px;"><asp:TextBox ID="tb_Points3" runat="server" AutoPostBack="False" Width="70" 
							Wrap="False" Height="1.1em" Font-Size="X-Small" /></td></tr>
					<tr><td class="blgMidTxt2" style="height:20px;"><asp:TextBox ID="tb_Rate3" runat="server" AutoPostBack="False" Width="70" 
							Wrap="False" Height="1.1em" Font-Size="X-Small" /></td></tr>
					<tr><td class="spaceTxt"><img src="images/clear.gif" border="0" width="1px" height="4px" /></td></tr>
					<tr><td class="blgMidTxt2" style="height:20px;"><asp:TextBox ID="tb_mLoanAmt3" runat="server" AutoPostBack="False" Width="70" 
							Wrap="False" Height="1.1em" Font-Size="X-Small" /></td></tr>
					<tr><td class="blgMidTxt2" style="height:20px;"><asp:TextBox ID="tb_mSettle3" runat="server" AutoPostBack="False" Width="70" 
							Wrap="False" Height="1.1em" Font-Size="X-Small" /></td></tr>
					<tr><td class="blgMidTxt2" style="height:20px;"><asp:TextBox ID="tb_mPrePaids3" runat="server" AutoPostBack="False" Width="70" 
							Wrap="False" Height="1.1em" Font-Size="X-Small" /></td></tr>
					<tr><td class="blgMidTxt2" style="height:20px;"><asp:TextBox ID="tb_PMI3" runat="server" AutoPostBack="False" Width="70" 
							Wrap="False" Height="1.1em" Font-Size="X-Small" /></td></tr>
					<tr><td class="spaceTxt"><img src="images/clear.gif" border="0" width="1px" height="4px" /></td></tr>
				</table>
			</td></tr>
			</table>
		</td></tr>
		<tr><td colspan="2">
			<table border="0" cellpadding="0" cellspacing="0" width="100%"><tr style="vertical-align:middle; text-align:center;">
			<tr><td><img src="images/clear.gif" width="1" height="4" alt="" /></td></tr>
			<tr><td>
				<table><tr><td width="20%">
						
					</td><td align="center" width="60%">
						<asp:Button ID="refresh" runat="server" OnClick="Page_Transfer" Height="1.9em" Width="9em" Text="Create Quote" Font-Size="Small" />
					</td><td align="right" width="20%">
					
				</td></tr></table>
			</td></tr>
			<tr><td>&nbsp;</td></tr>
			<tr><td><a href="#1" onclick="displayBackgroundInstructions()">Click Here for instructions on how to print background colors...</a></td></tr>
			</tr></table>
		</td></tr>
		</table>
    </td></tr>
    <tr><td>&nbsp;</td></tr>
    <tr><td>
    </td></tr>
	<tr><td >
	<img src="images/equalhousinglender.png" border="0" alt="Equal Housing Lender" />
	<img src="images/bbb.jpg" border="0" alt="BBB" />
	</td></tr>		
    </table>
    </div>
    </form>
</body>
</html>
