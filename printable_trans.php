<?php

require("../printable/include/mysql.inc.php");
require("../printable/include/leadsondemand.printable.inc.php");
require("../printable/include/optimize.printable.inc.php");
require("globals.php");

$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);

$portal = new OptimizePortal($COMPANY_ID, $db);
$portal2 = new LeadsOnDemandPortal($COMPANY_ID, $db);

// Querry to get user info
$currentUser = $portal->GetUser($_SESSION['currentuserid']);
$isSubAdmin = $portal->CheckPriv($currentUser->UserID, 'subadmin');
$isAdmin = $portal->CheckPriv($currentUser->UserID, 'admin');
$isCorpMan = $portal->CheckPriv($currentUser->UserID, 'corporatemanager');
$isRegMan = $portal->CheckPriv($currentUser->UserID, 'regionmanager');
$isDivMan = $portal->CheckPriv($currentUser->UserID, 'divisionmanager');
if ($isAdmin || $isCorpMan || $isRegMan || $isDivMan)
    $isMan = true;
else
    $isMan = false;

header('Content-type:text/xml');

// Check login
if (!$currentUser) {
    echo "<Result><Status>Timeout</Status></Result>";
    die();
}

switch ($HTTP_GET_VARS['action']) {
    case 'GetPostingTable':
        echo $portal->GetPostingTable($currentUser->UserID, $_GET['socialcategoryid'], $_GET['sortBy'], $_GET['search']);
        break;
    case 'GetEditPostingTable':
        echo $portal->GetEditPostingTable($currentUser->UserID, $_GET['section'], $_GET['socialcategoryid'], $_GET['search']);
        break;
    case 'GetFileTable':
        echo $portal->GetFileTableNew($currentUser->UserID, $_GET['categoryid'], $_GET['sortBy'], $_GET['search']);
        break;
    case 'GetEditFileTable':
        echo $portal->GetEditFileTable($currentUser->UserID, $_GET['section'], $_GET['categoryid'], $_GET['search']);
        break;
    case 'GetPostInfo':
        $logins = $portal->GetUserLogins($currentUser->UserID);
        $login = array_shift($logins);
        $posting = $portal->GetPosting($_GET['socialpostingid'], $currentUser->Username, $login->LoginID);

        if ($posting) {
            echo "<Result>";
            echo "<Status>Success</Status>";
            echo "<StatusDetail>File info fetched.</StatusDetail>";
            echo $posting->toXML();
            echo "</Result>";
        } else {
            echo "<Result>";
            echo "<Status>Fail</Status>";
            echo "<StatusDetail>Invalid File.</StatusDetail>";
            echo "</Result>";
        }

        break;
    case 'GetFileInfo':
        $logins = $portal->GetUserLogins($currentUser->UserID);
        $login = array_shift($logins);
        $file = $portal->GetFile($_GET['id'], $currentUser->Username, $login->LoginID);

        if ($file) {
            echo "<Result>";
            echo "<Status>Success</Status>";
            echo "<StatusDetail>File info fetched.</StatusDetail>";
            echo $file->toXML();
            echo "</Result>";
        } else {
            echo "<Result>";
            echo "<Status>Fail</Status>";
            echo "<StatusDetail>Invalid File.</StatusDetail>";
            echo "</Result>";
        }

        break;
    case 'LogPublicContentEmail':
        $lead = $portal->GetOptimizeContact($_GET['slid'], 'all');
        $file = $portal->GetFile($_GET['fileid']);
        $contactEvent = $portal->GetContactEvent(
            $portal->AddContactEvent($lead->SubmissionListingID, 234, $currentUser->UserID));

        $contactEvent->EventNotes = "ContentOnDemand $file->Description Sent";

        $portal->UpdateContactEvent($contactEvent);
        echo "<Result><Status>Success</Status></Result>";
        break;
    case 'LogContactEvent':
        if ($isSubAdmin) {
            $userString = 'all';
        } else {
            $userString = $currentUser->UserID;
        }
        $oldContact = $portal->GetOptimizeContact($_GET['slid'], $userString);
        $ceID = $portal->LogContactEvent($_GET['slid'], $_GET['etid'], ($oldContact->PublicContact == 1 || $isSubAdmin ? $oldContact->UserID : $currentUser->UserID));
        $newContact = $portal->GetOptimizeContact($_GET['slid'], $userString);
        $portal->CheckFieldTrigger($oldContact, $newContact);

        if ($ceID) {
            echo "<Result><Status>OK</Status><EventTypeID>" . $_GET['etid'] . "</EventTypeID><ContactEventID>$ceID</ContactEventID></Result>";
        } else {
            echo "<Result><Status>Fail</Status></Result>";
        }
        break;
    case 'UpdateContactStatus':
        $lead = $portal->GetOptimizeContact($_GET['slid'], 'all');
        $cst = $portal->GetContactStatusType($_GET['cstid']);

        if (!$lead) {
            echo "<Result><Status>Fail</Status><StatusDetail>Lead not found</StatusDetail></Result>";
            die();
        }
        if (!$cst) {
            echo "<Result><Status>Fail</Status><StatusDetail>Invalid Status Type</StatusDetail></Result>";
            die();
        }

        $isAdmin = $portal->CheckPriv($currentUser->UserID, 'admin');
        $isCorpMan = $portal->CheckPriv($currentUser->UserID, 'corporatemanager');
        $isRegMan = $portal->CheckPriv($currentUser->UserID, 'regionmanager');
        $isDivMan = $portal->CheckPriv($currentUser->UserID, 'divisionmanager');
        if ($isAdmin || $isCorpMan || $isRegMan || $isDivMan)
            $isMan = true;
        else
            $isMan = false;

        if ($lead->UserID != $currentUser->UserID
            && $lead->Public != 1
            && !$isMan
        ) {
            echo "<Result>";
            echo "<Status>Fail</Status>";
            echo "<StatusDetail>You do not have permission to update this lead. Please refresh this lead listing this lead may have been re-assigned.</StatusDetail>";
            echo "<cstid_old>" . $lead->ContactStatusTypeID . "</cstid_old>";
            echo "<slid>" . $lead->SubmissionListingID . "</slid>";
            echo "</Result>";
            die();
        }

        $lead->ContactStatusTypeID = $_GET['cstid'];

        if ($cst->ReassignUserID != 0) {
            $lead->UserID = $cst->ReassignUserID;
        }
        $portal->UpdateOptimizeContact($lead);
        $user = $portal->GetUser($lead->UserID);

        if ($cst->EmailUserID != 0) {
            $portal2->EmailContactStatusChange($lead->SubmissionListingID, $cst->EmailUserID);
        }

        echo "<Result>";
        echo "<Status>Success</Status>";
        echo "<StatusDetail>Status updated.</StatusDetail>";
        echo "<cstid_old>" . $lead->ContactStatusTypeID . "</cstid_old>";
        echo "<slid>" . $lead->SubmissionListingID . "</slid>";
        echo "<owner>" . $user->FirstName . " " . $user->LastName . "</owner>";
        echo "</Result>";
        die();

        break;
    case 'CheckReminder':
        $appt = $portal->GetAllOpenNotifies('popup', $currentUser->UserID);
        if (sizeof($appt) > 1) {
            echo "<Result>";
            echo "<Status>PendingContactAppointment</Status>";
            echo "<StatusDetail>New Notification Pending.</StatusDetail>";
            echo "<ContactAppointmentID>Multiple</ContactAppointmentID>";
            echo "</Result>";
        } else if ($appt[0]->NotifyType) {
            echo "<Result>";
            echo "<Status>PendingContactAppointment</Status>";
            echo "<StatusDetail>New Notification Pending.</StatusDetail>";
            echo "<ContactAppointmentID>" . $appt[0]->ContactAppointmentID . "</ContactAppointmentID>";
            echo "</Result>";
        } else {
            echo "<Result>";
            echo "<Status>NoPendingContactAppointment</Status>";
            echo "<StatusDetail>There is no Notification Pending.</StatusDetail>";
            echo "</Result>";
        }
        break;
    case 'GetAddLeadTable':
        $limit = 50;
        $_GET['start'] = intval($_GET['start']);
        if ($_GET['start'] < 0)
            $_GET['start'] = 0;
        switch ($ADDSEARCH_OPTIONS[$_GET['SearchType']]['Type']) {
            case 'Search':
                if ($_GET['slid'] != "") {
                    $lead = $portal->GetOptimizeContact($_GET['slid'], 'all');
                    $leads = $lead ? array($lead) : array();
                } elseif ($_GET['phone'] != "") {
                    $leads = $portal->GetOptimizeContacts('all', 'all', "(Phone LIKE '" . mysql_real_escape_string($_GET['phone']) . "'
																					OR Phone2 LIKE '" . mysql_real_escape_string($_GET['phone']) . "'
																					OR WorkPhone LIKE '" . mysql_real_escape_string($_GET['phone']) . "')", true, 'DateAdded DESC', $limit, $_GET['start']);
                } elseif ($_GET['email'] != "") {
                    $leads = $portal->GetOptimizeContacts('all', 'all', "Email LIKE '" . mysql_real_escape_string($_GET['email']) . "'", true, 'DateAdded DESC', $limit, $_GET['start']);
                } else {
                    $leads = array();
                }
                break;
            case 'Transfer':
                $leads = $portal->GetOptimizeContacts('all', 'all', "contactstatustype.ContactStatusTypeID = '" . intval($ADDSEARCH_OPTIONS[$_GET['SearchType']]['TransferStatusID']) . "'", true, 'DateAdded DESC', $limit, $_GET['start']);

                break;
            case 'Store':
                if (!strtotime($_GET['start_range'])) {
                    $_GET['start_range'] = date("Y-m-1");
                }
                $startWhere = "submissionlisting.DateAdded >= '" . date("Y-m-d", strtotime($_GET['start_range'])) . " 00:00:00'";
                if (!strtotime($_GET['end_range'])) {
                    $_GET['end_range'] = date("Y-m-d");
                }
                $endWhere = "submissionlisting.DateAdded <= '" . date("Y-m-d", strtotime($_GET['end_range'])) . " 23:59:59'";

                $slid_where = $_GET['slid'] != "" ? "submissionlisting.SubmissionListingID = '" . intval($_GET['slid']) . "'" : "1";
                $phone_where = $_GET['phone'] != "" ? "(REPLACE(REPLACE(REPLACE(REPLACE(Phone,' ',''),'(',''),')',''),'-','') LIKE '%" . mysql_real_escape_string(stripslashes(preg_replace("/[^0-9]/", '', $_GET['phone']))) . "%'
                                                        OR REPLACE(REPLACE(REPLACE(REPLACE(Phone2,' ',''),'(',''),')',''),'-','') LIKE '%" . mysql_real_escape_string(stripslashes(preg_replace("/[^0-9]/", '', $_GET['phone']))) . "%'
                                                        OR REPLACE(REPLACE(REPLACE(REPLACE(WorkPhone,' ',''),'(',''),')',''),'-','') LIKE '%" . mysql_real_escape_string(stripslashes(preg_replace("/[^0-9]/", '', $_GET['phone']))) . "%')" : "1";
                $email_where = $_GET['email'] != "" ? "Email LIKE '" . mysql_real_escape_string($_GET['email']) . "'" : "1";
                $lname_where = $_GET['lname'] != "" ? "submissionlisting.LastName LIKE '" . mysql_real_escape_string($_GET['lname']) . "%'" : "1";
                $eid_where = $_GET['eid'] != "" ? "ExternalID LIKE '" . mysql_real_escape_string($_GET['eid']) . "%'" : "1";
                $state_where = $_GET['State'] != "" ? "submissionlisting.State LIKE '" . mysql_real_escape_string($_GET['State']) . "'" : "1";
                $sltid_where = $_GET['sltid'] != "" ? "submissionlistingtype.SubmissionListingTypeID LIKE '" . mysql_real_escape_string($_GET['sltid']) . "'" : "1";

                /* Unusued Fields */
                //$lnum_where = $_GET['lnum'] != "" ? "LoanNumber LIKE '" . mysql_real_escape_string($_GET['lnum']) . "%'" : "1";
                //$lclass_where = $_GET['ListingClass'] != "" ? "submissionlistingtype.ListingClass LIKE '" . mysql_real_escape_string($_GET['ListingClass']) . "'" : "1";
                //$llev_where = $_GET['ListingLevel'] != "" ? "submissionlistingtype.ListingLevel LIKE '" . mysql_real_escape_string($_GET['ListingLevel']) . "'" : "1";

                if ($isMan) {

                    $state_lookup_where = "1";

                    $group_where = "1";
                } else {
                    $profiles = $portal->GetUserProfileList($currentUser->UserID);
                    $pArray = array();

                    foreach ($profiles as $p) {
                        $pArray[] = "'$p->ProfileName'";
                    }

                    if (sizeof($pArray) > 0)
                        $state_lookup_where = "submissionlisting.State IN (" . implode(", ", $pArray) . ")";
                    else
                        $state_lookup_where = "0";

                    $group_where = "(submissionlistingtype.GroupID = 0 || submissionlistingtype.GroupID = '" . intval($currentUser->GroupID) . "')";
                }

                if ($isCorpMan)
                    $area_where = "1";
                else if ($isRegMan)
                    $area_where = "(submissionlistingtype.RegionID = '" . $currentUser->RegionID . "' OR submissionlistingtype.RegionID = 0)";
                else
                    $area_where = "(submissionlistingtype.DivisionID = '" . $currentUser->DivisionID . "' OR submissionlistingtype.RegionID = 0 OR (submissionlistingtype.RegionID = '" . $currentUser->RegionID . "' AND submissionlistingtype.DivisionID = 0))";

                /* Removed lclass and lley
                $store_where = "(($startWhere) AND ($endWhere) AND ($slid_where) AND ($phone_where) AND ($group_where) AND ($state_lookup_where) AND
									($area_where) AND ($email_where) AND ($lname_where) AND ($lnum_where) AND ($state_where) AND ($lclass_where) AND ($llev_where))
									AND contactstatustype.ContactStatusTypeID = '" . intval($ADDSEARCH_OPTIONS[$_GET['SearchType']]['StoreStatusID']) . "'";
                */

                $store_where = "(($startWhere) AND ($endWhere) AND ($slid_where) AND ($phone_where) AND ($group_where) AND ($state_lookup_where) AND
									($area_where) AND ($email_where) AND ($lname_where) AND ($eid_where) AND ($state_where) AND ($sltid_where))
									AND contactstatustype.ContactStatusTypeID = '" . intval($ADDSEARCH_OPTIONS[$_GET['SearchType']]['StoreStatusID']) . "'";

                $leads = $portal->GetOptimizeContacts('all', 'all', $store_where, true, 'DateAdded DESC', $limit, $_GET['start']);

                $csts = $portal->GetContactStatusTypes('all', 1, true);
                $contactStatusesTypes = array();
                $shortStatuses = array();
                $shortStatusDetails = array();
                foreach ($csts as $cst) {
                    if (!$cst->Disabled) {
                        $shortStatuses[$cst->ShortName] = (strlen($cst->DetailName) > 0 ? 1 : 0);
                        $shortStatusDetails[$cst->ShortName][] = $cst->ContactStatus;
                        $contactStatusesTypes[$cst->ContactStatus] = $cst;
                    }
                }
                $contactEventTypes = $portal->GetContactEventTypes();
                $cetLookup = array();
                foreach ($contactEventTypes as $cet) {
                    $cetLookup[$cet->ContactEventTypeID] = $cet->EventType;
                }

                break;
            default:
                $leads = array();
                break;
        }
        $prevLink = "";
        $nextLink = "";
        if ($_GET['start'] > 0) {
            $prevLink = "<li><a href='#' aria-label='Previous' onclick='searchLeads(" . ($_GET['start'] - $limit) . "); return false;'>&laquo; Prev</a></li>";
        }
        if (sizeof($leads) >= $limit) {
            $nextLink = "<li><a href='#' aria-label='Next' onclick='searchLeads(" . ($_GET['start'] + $limit) . "); return false;'>Next &raquo;</a></li>";
        }
        ?>
        <table class="search_table table table-striped table-hover table-bordered">
            <thead>
            <tr class="search_table rowheader">
                <?php
                if ($ADDSEARCH_OPTIONS[$_GET['SearchType']]['Type'] == 'Store') {
                    ?>
                    <th class="search_table" width="18">&nbsp;</th>
                    <?php
                }
                ?>
                <th class="search_table">ID</th>
                <th class="search_table">Mailer Code</th>
                <th class="search_table">Date Added</th>
                <th class="search_table">First Name</th>
                <th class="search_table">Last Name</th>
                <th class="search_table">Phone</th>
                <th class="search_table">Work Phone</th>
                <th class="search_table">Mobile Phone</th>
                <th class="search_table">ST</th>
                <th class="search_table">Next Appt Time</th>
                <th class="search_table">Contact Event</th>
                <th class="search_table">Status</th>
                <th class="search_table">Last Action Date</th>
                <?php if (sizeof($leads) <= 0): ?>
                    <th class="search_table">Loan Officer</th>
                <?php else: ?>
                    <th class="search_table">&nbsp;</th>
                <?php endif; ?>
            </tr>
            </thead>
            <tbody>
            <?php
            if (sizeof($leads) <= 0) {
                ?>

                <?php
                /* Greenlink Customization - search for assigned leads when it's not found on the Lead Store */

                $slid_where = $_GET['slid'] != "" ? "submissionlisting.SubmissionListingID = '" . intval($_GET['slid']) . "'" : "1";
                $phone_where = $_GET['phone'] != "" ? "(REPLACE(REPLACE(REPLACE(REPLACE(Phone,' ',''),'(',''),')',''),'-','') LIKE '%" . mysql_real_escape_string(stripslashes(preg_replace("/[^0-9]/", '', $_GET['phone']))) . "%'
                                                        OR REPLACE(REPLACE(REPLACE(REPLACE(Phone2,' ',''),'(',''),')',''),'-','') LIKE '%" . mysql_real_escape_string(stripslashes(preg_replace("/[^0-9]/", '', $_GET['phone']))) . "%'
                                                        OR REPLACE(REPLACE(REPLACE(REPLACE(WorkPhone,' ',''),'(',''),')',''),'-','') LIKE '%" . mysql_real_escape_string(stripslashes(preg_replace("/[^0-9]/", '', $_GET['phone']))) . "%')" : "1";
                $email_where = $_GET['email'] != "" ? "Email LIKE '" . mysql_real_escape_string($_GET['email']) . "'" : "1";
                $lname_where = $_GET['lname'] != "" ? "submissionlisting.LastName LIKE '" . mysql_real_escape_string($_GET['lname']) . "%'" : "1";
                $eid_where = $_GET['eid'] != "" ? "ExternalID LIKE '" . mysql_real_escape_string($_GET['eid']) . "%'" : "1";
                $state_where = $_GET['State'] != "" ? "submissionlisting.State LIKE '" . mysql_real_escape_string($_GET['State']) . "'" : "1";
                $sltid_where = $_GET['sltid'] != "" ? "submissionlistingtype.SubmissionListingTypeID LIKE '" . mysql_real_escape_string($_GET['sltid']) . "'" : "1";

                $global_where = "(($slid_where) AND ($phone_where) AND ($group_where) AND
									($email_where) AND ($lname_where) AND ($eid_where) AND ($state_where) AND ($sltid_where))";

                $assignedleads = $portal->GetOptimizeContacts('all', 'all', $global_where, true, 'DateAdded DESC', $limit);
                ?>

                <?php if (sizeof($assignedleads) <= 0): ?>
                    <tr>
                        <td colspan='14' align="center">
                            There were no leads that matched your search criteria from the global search.
                        </td>
                    </tr>
                <?php else: ?>
                    <tr>
                        <td colspan='14' align="center">
                            There were no leads that matched your search criteria from the lead store.
                        </td>
                    </tr>
                    <?php foreach ($assignedleads as $lead):
                        $contactHistory = $portal->GetContactHistory($lead->SubmissionListingID);
                        $lastCH = $contactHistory[sizeof($contactHistory) - 1];
                        $appt = $portal->GetOpenAppointment($lead->SubmissionListingID, 'all');
                        ?>

                        <tr>
                            <td class="search_table" align="center">&nbsp;</td>
                            <td class="search_table" align="center"><?= $lead->SubmissionListingID ?></td>
                            <td class="search_table" align="center"><?= $lead->ExternalID ?></td>
                            <td class="search_table"><?= date("m/d/Y g:i a", strtotime($lead->DateAdded)) ?></td>
                            <td class="search_table"><?= $lead->FirstName ?></td>
                            <td class="search_table"><?= $lead->LastName ?></td>
                            <td class="search_table">
                                <!--<a href="ciscotel:<?= preg_replace("/[^0-9]/", '', $lead->Phone) ?>" id="ciscoCall1"  submissionListingID="<?= $lead->SubmissionListingID; ?>" class="ciscoCall">-->
                                <a onclick="click2call(<?= $lead->SubmissionListingID; ?>,<?= preg_replace("/[^0-9]/", '', $lead->Phone) ?>)"
                                   submissionListingID="<?= $lead->SubmissionListingID; ?>" class="click2call"
                                   dispatch="<?= preg_replace("/[^0-9]/", '', $lead->Phone) ?>">
                                    <?= format_phone($lead->Phone) ?>
                                </a>
                            </td>
                            <td class="search_table">
                                <!--<a href="ciscotel:<?= preg_replace("/[^0-9]/", '', $lead->Phone2) ?>" id="ciscoCall1"  submissionListingID="<?= $lead->SubmissionListingID; ?>" class="ciscoCall">-->
                                <a onclick="click2call(<?= $lead->SubmissionListingID; ?>,<?= preg_replace("/[^0-9]/", '', $lead->Phone2) ?>)"
                                   submissionListingID="<?= $lead->SubmissionListingID; ?>" class="click2call"
                                   dispatch="<?= preg_replace("/[^0-9]/", '', $lead->Phone2) ?>">
                                    <?= format_phone($lead->Phone2) ?>
                                </a>
                            </td>
                            <td class="search_table">
                                <!--<a href="ciscotel:<?= preg_replace("/[^0-9]/", '', $lead->Phone2) ?>" id="ciscoCall1"  submissionListingID="<?= $lead->SubmissionListingID; ?>" class="ciscoCall">-->
                                <a onclick="click2call(<?= $lead->SubmissionListingID; ?>,<?= preg_replace("/[^0-9]/", '', $lead->Phone2) ?>)"
                                   submissionListingID="<?= $lead->SubmissionListingID; ?>" class="click2call"
                                   dispatch="<?= preg_replace("/[^0-9]/", '', $lead->Phone2) ?>">
                                    <?= format_phone($lead->Phone2) ?>
                                </a>
                            </td>
                            <td class="search_table"><?= $lead->State ?></td>
                            <td class="search_table"><?= $appt ? date("m/d/Y g:i a", strtotime($appt->AppointmentTime)) : "" ?></td>
                            <td class="search_table">
                                <?= $cetLookup[$lead->LastContactEvent->ContactEventTypeID] == "" ? "" : $cetLookup[$lead->LastContactEvent->ContactEventTypeID] ?>
                            </td>
                            <td class="search_table">
                                <?php foreach ($contactStatusesTypes as $statusName => $status): ?>
                                    <?php if ($status->ContactStatusTypeID == $lead->ContactStatusTypeID): ?>
                                        <?= $statusName ?>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </td>
                            <td class="search_table">
                                <?= strtotime($lastCH->EventDate) ? date("m/d/Y g:i a", strtotime($lastCH->EventDate)) : "" ?>
                            </td>
                            <td class="search_table">
                                <?= $lead->UserFullname; ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
                <?php
            } else {
                $i = 0;
                foreach ($leads as $lead) {
                    $contactHistory = $portal->GetContactHistory($lead->SubmissionListingID);
                    $lastCH = $contactHistory[sizeof($contactHistory) - 1];
                    $appt = $portal->GetOpenAppointment($lead->SubmissionListingID, 'all');
                    $i++;
                    ?>
                    <tr class="search_table table<?= $i % 2 == 0 ? ' roweven' : ' rowodd' ?>">
                        <?php
                        if ($ADDSEARCH_OPTIONS[$_GET['SearchType']]['Type'] == 'Store') {
                            ?>
                                    <td class="search_table"><input type="checkbox" name="multileadid[<?= $i ?>]" id="multileadid[<?= $i ?>]" value="<?= $lead->SubmissionListingID ?>" /></th>
                            <?php
                        }
                        ?>
                        <td class="search_table" align="center"><?= $lead->SubmissionListingID ?></td>
                        <td class="search_table" align="center"><?= $lead->ExternalID ?></td>
                        <td class="search_table"><?= date("m/d/Y g:i a", strtotime($lead->DateAdded)) ?></td>
                        <td class="search_table"><?= $lead->FirstName ?></td>
                        <td class="search_table"><?= $lead->LastName ?></td>
                        <td class="search_table">
                            <!--<a href="ciscotel:<?= preg_replace("/[^0-9]/", '', $lead->Phone) ?>" id="ciscoCall1"  submissionListingID="<?= $lead->SubmissionListingID; ?>" class="ciscoCall">-->
                            <a onclick="click2call(<?= $lead->SubmissionListingID; ?>,<?= preg_replace("/[^0-9]/", '', $lead->Phone) ?>)"
                               submissionListingID="<?= $lead->SubmissionListingID; ?>" class="click2call"
                               dispatch="<?= preg_replace("/[^0-9]/", '', $lead->Phone) ?>">
                                <?= format_phone($lead->Phone) ?>
                            </a>
                        </td>
                        <td class="search_table">
                            <!--<a href="ciscotel:<?= preg_replace("/[^0-9]/", '', $lead->Phone2) ?>" id="ciscoCall1"  submissionListingID="<?= $lead->SubmissionListingID; ?>" class="ciscoCall">-->
                            <a onclick="click2call(<?= $lead->SubmissionListingID; ?>,<?= preg_replace("/[^0-9]/", '', $lead->Phone2) ?>)"
                               submissionListingID="<?= $lead->SubmissionListingID; ?>" class="click2call"
                               dispatch="<?= preg_replace("/[^0-9]/", '', $lead->Phone2) ?>">
                                <?= format_phone($lead->Phone2) ?>
                            </a>
                        </td>
                        <td class="search_table">
                            <!--<a href="ciscotel:<?= preg_replace("/[^0-9]/", '', $lead->Phone2) ?>" id="ciscoCall1"  submissionListingID="<?= $lead->SubmissionListingID; ?>" class="ciscoCall">-->
                            <a onclick="click2call(<?= $lead->SubmissionListingID; ?>,<?= preg_replace("/[^0-9]/", '', $lead->Phone2) ?>)"
                               submissionListingID="<?= $lead->SubmissionListingID; ?>" class="click2call"
                               dispatch="<?= preg_replace("/[^0-9]/", '', $lead->Phone2) ?>">
                                <?= format_phone($lead->Phone2) ?>
                            </a>
                        </td>
                        <td class="search_table"><?= $lead->State ?></td>
                        <td class="search_table"><?= $appt ? date("m/d/Y g:i a", strtotime($appt->AppointmentTime)) : "" ?></td>
                        <td class="search_table">
                            <?= $cetLookup[$lead->LastContactEvent->ContactEventTypeID] == "" ? "" : $cetLookup[$lead->LastContactEvent->ContactEventTypeID] ?>
                        </td>
                        <td class="search_table">
                            <?php foreach ($contactStatusesTypes as $statusName => $status): ?>
                                <?php if ($status->ContactStatusTypeID == $lead->ContactStatusTypeID): ?>
                                    <?= $statusName ?>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </td>
                        <td class="search_table">
                            <?= strtotime($lastCH->EventDate) ? date("m/d/Y g:i a", strtotime($lastCH->EventDate)) : "" ?>
                        </td>
                        <td class="search_table">
                            <a href="add_lead.php?id=<?= $lead->SubmissionListingID ?>&type=<?= urlencode($_GET['SearchType']); ?>"
                               onclick="return confirm('You are about to take possession of this lead. Click OK to proceed.')"
                               title="Take Lead" data-toggle="tooltip"><i class="glyphicon glyphicon-edit"></i></a>
                        </td>
                    </tr>
                    <?php
                }
            }
            ?>
            <tr>
                <td colspan="14" align="center">
                    <nav>
                        <ul class="pagination">
                            <?= "$prevLink $nextLink" ?>
                        </ul>
                    </nav>
                </td>
            </tr>
            </tbody>
        </table>
        <?php
        if ($ADDSEARCH_OPTIONS[$_GET['SearchType']]['Type'] == 'Store' && sizeof($leads) > 0) {
            ?>
            <div style="text-align: center;"><input class="btn btn-default btn-md center-block" type="button"
                                                    value="Add Checked Leads"
                                                    onclick="addLeads(<?= intval($i); ?>, '<?= urlencode($_GET['SearchType']); ?>')"/>
            </div>
            <?php
        }
        break;
    case 'LookupOptions':
        echo "<Result><Status>OK</Status><RowNumber>" . $_GET['RowNumber'] . "</RowNumber><Fields>";
        $fields = $portal->GetLookupValues($_GET['FieldName']);
        foreach ($fields as $f) {
            echo "<Field><Label>$f->LookupLabel</Label><Value>$f->LookupValue</Value></Field>";
        }
        echo "</Fields></Result>";
        break;
    default:
        echo "<Result><Status>Timeout</Status></Result>";
        break;
}

?>