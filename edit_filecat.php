<?php
require("../printable/include/mysql.inc.php");
require("../printable/include/optimize.printable.inc.php");
require("globals.php");

$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);

$portal = new OptimizePortal($COMPANY_ID, $db);

$currentUser = $portal->UserAccess($_SESSION['currentuserid']);
$isSubAdmin = $portal->CheckPriv($currentUser->UserID, 'subadmin');

// Check login
if (!$isSubAdmin) {
    header("Location: " . $portal->CurrentCompany->Website . "login.php?message=" . urlencode("Not logged in or login error.  Please try again."));
    die();
}

if (!isset($_GET['section'])) {
    $_GET['section'] = 0;
}

// Get Current Category
$currentCategory = $portal->GetCategory($_GET['catid']);

if (!$currentCategory) {
    echo "Invalid Category";
    die();
}

if ($_POST['Submit'] == 'Update') {
    $currentCategory->CategoryName = $_POST['CategoryName'];
    $currentCategory->CategoryDesc = $_POST['CategoryDesc'];
    $currentCategory->Parent = $_POST['Parent'];
    if ($portal->CheckPriv($currentUser->UserID, 'admin'))
        $currentCategory->GroupID = $_POST['GroupID'];

    $portal->UpdateCategory($currentCategory);

    header("Location: manage_filecats.php?section=" . $_GET['section'] . "&message=" . urlencode("Folder Updated."));
    die();
}
elseif ($_POST['Submit'] == 'Cancel') {
    header("Location: manage_filecats.php?section=" . $_GET['section'] . "&message=" . urlencode("Action Canceled. Folder not updated."));
    die();
} elseif ($_POST['Submit'] == 'Delete' && $currentCategory->CategoryID != 69) {
    $portal->DeleteCategory($currentCategory);

    header("Location: manage_filecats.php?section=" . $_GET['section'] . "&message=" . urlencode("Folder Deleted."));
    die();
}

$categories = $portal->GetCategories('all', $_GET['section'], true, 'all');
$gs = $portal->GetCompanyGroups();

$groups = array();

$groups[0] = new Group();
$groups[0]->GroupID = 0;
$groups[0]->GroupName = "All";

foreach ($gs as $g) {
    $groups[$g->GroupID] = $g;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <title>
            <?= $portal->CurrentCompany->CompanyName ?> :: Edit File Category
        </title>
        <meta http-equiv="content-type" content="text/html;charset=utf-8" />		
        <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />
        <script  src="js/func.js"></script>	
        <?php include("components/bootstrap.php") ?>
    </head>
    <body bgcolor="#FFFFFF">
        <div id="page">
            <?php include("components/header.php") ?>
            <div id="body">
                <?php
                $CURRENT_PAGE = "Home";
                include("components/navbar.php");
                ?>
                <form method="post" action="<?= $_SERVER['PHP_SELF'] ?>?catid=<?= $currentCategory->CategoryID ?>&section=<?= $_GET['section'] ?>">
                    <?php if (isset($_GET['message'])): ?>
                        <div class="container">
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <?= $_GET['message']; ?>
                            </div>
                        </div>
                    <?php endif; ?> 
                    <div id="FileCatDiv" class="well container">
                        <div class="sectionHeader"><h2>Edit File Category</h2></div>
                        <div class="sectionDiv">
                            <div class="itemSection row">
                                <div id="CategoryNameDiv" class="form-group col-md-3">
                                    <label for="CategoryName">Folder Name:</label><br/>
                                    <input type="text" class="form-control input-sm" name="CategoryName" value="<?= $currentCategory->CategoryName ?>" size="45"/>
                                </div>
                            </div>
                            <div class="itemSectionLong row">
                                <div id="CategoryDescDiv" class="form-group col-md-6">
                                    <label for="CategoryDesc">Description:</label><br/>
                                    <textarea name="CategoryDesc" class="form-control input-sm" rows="4" cols="48"><?= $currentCategory->CategoryDesc ?></textarea>
                                </div>
                            </div>
                            <div class="itemSection row">
                                <div id="ParentDiv" class="form-group col-md-3">
                                    <label for="ParentName">Parent Folder:</label><br/>
                                    <select class="form-control input-sm" name="Parent">
                                        <option value="0"> - Root - </option>
                                        <?php
                                        $category = new FileCategory();
                                        foreach ($categories as $category) {
                                            if ($category->CategoryID != $currentCategory->CategoryID && ($portal->CheckPriv($currentUser->UserID, 'admin') || $category->GroupID != 0)) {
                                                ?>
                                                <option value="<?= $category->CategoryID ?>" <?= $category->CategoryID == $currentCategory->Parent ? "SELECTED" : "" ?>>
                                                    <?= str_repeat('&nbsp;&nbsp;&nbsp;', $portal->GetCategoryLevel($category->CategoryID)) . $category->CategoryName ?>
                                                </option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <?php
                            if ($portal->CheckPriv($currentUser->UserID, 'admin')) {
                                ?>
                                <div class="itemSection row">
                                    <div id="ParentDiv" class="form-group col-md-3">
                                        <label for="GroupID">Group:</label><br/>
                                        <select class="form-control input-sm" name="GroupID">
                                            <?php
                                            foreach ($groups as $g) {
                                                ?>
                                                <option value="<?= $g->GroupID ?>" <?= $g->GroupID == $currentCategory->GroupID ? "SELECTED" : "" ?>>
                                                    <?= $g->GroupName ?>
                                                </option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>
                            <div class="itemSection row">
                                <div class="buttonSection">
                                    <input type="submit" value="Update" class="btn btn-info btn-sm" name="Submit"/>&nbsp;&nbsp;<input type="submit" value="Cancel" class="btn btn-default btn-sm" name="Submit"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <?php include("components/footer.php") ?>
    </body>
</html>