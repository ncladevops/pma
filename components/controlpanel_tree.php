<ul id="dhtmlgoodies_tree" class="nav nav-pills nav-stacked bs-sidebar">
    <li id="dhtmlgoodies_treeNodeusermanage_node">
        <a href="#" id="usermanage_node_link" onclick="return false;"><i class="glyphicon glyphicon-user"></i> User
            Management</a>
        <ul class="nav">
            <li id="node"><a href="manage_users.php" id="edituser_node_link"><i class="glyphicon glyphicon-expand"></i>
                    Edit Users</a></li>
            <li id="node_1_2"><a href="manage_extensions.php" id="node_1_2_link"><i
                        class="glyphicon glyphicon-expand"></i> Edit Workgroups</a></li>
        </ul>
    </li>
    <?php if (isset($optifinowGlobalSetting['feature']['ContentOnDemand']) && $optifinowGlobalSetting['feature']['ContentOnDemand'] == TRUE): ?>
        <li id="dhtmlgoodies_treeNodefilemanage_node">
            <a href="#" id="node_2_link" onclick="return false;"><i class="glyphicon glyphicon-file"></i> File
                Management</a>
            <ul class="nav">
                <li id="node_2_1"><a href="manage_filecats.php?section=0" id="node_2_1_link"><i
                            class="glyphicon glyphicon-expand"></i> Edit File Categories</a></li>
                <li id="node_2_2"><a href="manage_filedl.php?section=0" id="node_2_2_link"><i
                            class="glyphicon glyphicon-expand"></i> Edit Files</a></li>
            </ul>
        </li>
    <?php endif; ?>
    <li id="dhtmlgoodies_treeNodesitemanage_node">
        <a href="#" id="node_3_link" onclick="return false;"><i class="glyphicon glyphicon-list-alt"></i> Site
            Management</a>
        <ul class="nav">
            <li id="node_3_1"><a href="manage_messages.php" id="node_3_1_link"><i
                        class="glyphicon glyphicon-expand"></i> Custom Messages</a></li>
            <li id="node_3_2"><a href="manage_campaigns.php" id="node_3_2_link"><i
                        class="glyphicon glyphicon-expand"></i> Manage Campaigns</a></li>
            <li id="node_3_4"><a href="manage_filters.php" id="node_3_4_link"><i class="glyphicon glyphicon-expand"></i>
                    Manage Filters</a></li>
            <li id="node_3_3"><a href="print_management" id="node_3_3_link"><i class="glyphicon glyphicon-expand"></i>
                    Manage Products</a></li>
        </ul>
    </li>
    <?php if (isset($optifinowGlobalSetting['feature']['SocialOnDemand']) && $optifinowGlobalSetting['feature']['SocialOnDemand'] == TRUE): ?>
        <li id="dhtmlgoodies_treeNodesocialmanage_node">
            <a href="#" id="node_4_link" onclick="return false;"><i class="glyphicon glyphicon-share"></i> Social
                Management</a>
            <ul class="nav">
                <li id="node_4_1"><a href="manage_socialcats.php?section=0" id="node_4_1_link"><i
                            class="glyphicon glyphicon-expand"></i> Manage Categories</a></li>
                <li id="node_4_2"><a href="manage_socialpost.php?section=0" id="node_4_2_link"><i
                            class="glyphicon glyphicon-expand"></i> Manage Postings</a></li>
            </ul>
        </li>
    <?php endif; ?>
</ul>