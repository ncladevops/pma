<div id="optifiheader">
    <a class="navbar-brand" href="#"><?= $portal->GetCustomMessage('HeaderLeft', false, $currentUser->GroupID)->Message; ?></a>

    <ul class="nav nav-pills">
        <li class="pull-right"><a href="help.php"><i class="glyphicon glyphicon-question-sign"></i></a></li>
        <?php if ($currentUser): ?>
            <li class="dropdown pull-right">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#"><?= $currentUser->Username ?><span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                    <li><a href="edit_user.php"><i class="glyphicon glyphicon-user"></i> Profile</a></li>
                    <?php if ($portal->CheckPriv($currentUser->UserID, 'subadmin')): ?>
                        <li><a href="controlpanel.php"><i class="glyphicon glyphicon-cog"></i> Control Panel</a></li>
                    <?php endif; ?>
                    <?php if ($portal->CheckPriv($currentUser->UserID, 'admin')): ?>
                        <li><a href="admin/"><i class="glyphicon glyphicon-list-alt"></i> Admin</a></li>
                    <?php endif; ?>
                    <li><a href="login.php?logout=true"><i class="glyphicon glyphicon-off"></i> Logout</a></li>
                </ul>
            </li>
        <?php endif; ?>

        <?php if (isset($optifinowGlobalSetting['custom']['SMSOnDemand']) && $optifinowGlobalSetting['custom']['SMSOnDemand'] == TRUE): ?>

        <?php $usermessages = $portal->GetUserSms($currentUser->UserID, 'Response', 1); ?>

        <li class="pull-right"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-bell"></i>  <span class="badge"><?php echo sizeof($messages); ?></span></a>
            <ul class="dropdown-menu">
                <?php foreach ($usermessages as $um): ?>
                    <li class="sms-notif">
                        <a href="edit_lead.php?id=<?php echo $um->SubmissionListingID; ?>">
                            <span class="glyphicon glyphicon-phone"></span> from <b><?php echo $um->LeadName; ?></b>
                            &nbsp; <span class="sms-time"> <?php echo date("m/d/y H:iA", strtotime($um->Time)); ?></span>
                        </a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </li>

        <?php endif; ?>

    </ul>
</div>