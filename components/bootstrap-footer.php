<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="assets/js/jquery.js"></script>

<script src="dist/js/bootstrap.min.js"></script>

<!-- jQuery UI -->
<script type='text/javascript' src='../printable/include/js/jquery-ui.js'></script>

<!-- DataTable -->
<script type='text/javascript' src='js/DataTables/media/js/jquery.dataTables.min.js'></script>
<script type='text/javascript' src='js/DataTables/media/js/dataTables.bootstrap.min.js'></script>

<!-- rich text editor library -->
<script  type='text/javascript' src='js/jquery.cleditor.min.js'></script>

<!-- Chosen -->
<script type="text/javascript" src="js/chosen/chosen.jquery.min.js"></script>

<!-- Custom JS -->
<script type='text/javascript' src='js/optifinow-custom.js?v='></script>