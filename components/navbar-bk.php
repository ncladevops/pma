<?php
	include_once('reminder.php');
	// TODO Setup timeout 
	// include_once('timeout.php'); 
	if($currentUser)
	{
		$logins = $portal->GetUserLogins($_SESSION['currentuserid']);
	}
	$rightSpacerMargin = 247;
	
	if($currentUser->GroupID != $SALESFORCE_GROUPID) {
?>
<form name="sso_forum_2" id="sso_forum_2" method="POST" target="_blank" action="forum/ucp.php?mode=login">
	<input type="hidden" name="username" value="<?= $currentUser->Username ?>">
	<input type="hidden" name="password" value="optimize">
	<input type="hidden" name="autologin" value="1">
	<input type="hidden" name="login" value="1">
</form>
<div id="navbar_container">
	<div id="left_navbar_div">
		<div class="navbar_item_left">
			<div class="navbar_item_left_container">
				<?= $portal->GetCustomMessage('NavbarLink1', false, $currentUser->GroupID)->Message; ?>
			</div>
		</div>
		<div class="navbar_item_left">
			<div class="navbar_item_left_container">
				<?= $portal->GetCustomMessage('NavbarLink2', false, $currentUser->GroupID)->Message; ?>
			</div>
		</div>
		<div class="navbar_item_left<?= $CURRENT_PAGE == 'Social' ? '_selected' : '' ?>">
			<div class="navbar_item_left_container">
				<?= $portal->GetCustomMessage('NavbarLink3', false, $currentUser->GroupID)->Message; ?>
			</div>
		</div>
	</div>
	<div id="navbar_div">
		<div class="navbar_item<?= $CURRENT_PAGE == 'Home' ? '_selected' : '' ?>" 
			onclick="window.location = 'home.php';"
			style="width: 35px; padding-top: 7px; height: 23px;">
			<a href="home.php">
				Home
			</a>
		</div>
<?php
	if($portal->CheckPriv($currentUser->UserID, 'marketingOnDemand')) 
	{
		$rightSpacerMargin += 105;
?>
		<div class="navbar_item<?= $CURRENT_PAGE == 'Marketing' ? '_selected' : '' ?>" 
			onclick="window.location = 'redirect.php?loginid=<?= $logins[$MAIN_STORE_ID]->LoginID; ?>';"
			style="width: 75px;">
			<a href="redirect.php?loginid=<?= $logins[$MAIN_STORE_ID]->LoginID; ?>">
				Marketing<br/>onDemand
			</a>
		</div>
<?php
	}
	if($portal->CheckPriv($currentUser->UserID, 'salesOnDemand')) 
	{
		$rightSpacerMargin += 100;
?>
		<div class="navbar_item<?= $CURRENT_PAGE == 'Contacts' ? '_selected' : '' ?>" 
			onclick="window.location = 'contacts.php';"
			style="width: 70px;">
			<a href="contacts.php">
				Sales<br/>onDemand
			</a>
		</div>
<?php
	}
	if($portal->CheckPriv($currentUser->UserID, 'leadsOnDemand')) 
	{
		$rightSpacerMargin += 85;
?>
		<div class="navbar_item<?= $CURRENT_PAGE == 'Leads' ? '_selected' : '' ?>" 
			onclick="window.location = 'list_lead.php';"
			style="width: 55px;">
			<a href="list_lead.php">
				Leads<br/>onDemand
			</a>
		</div> 
<?php
	}
?>
		<div class="navbar_item<?= $CURRENT_PAGE == 'Files' ? '_selected' : '' ?>" 
			onclick="window.location = 'download_file.php';"
			style="width: 65px;">
			<a href="download_file.php">
				Content<br/>onDemand
			</a>
		</div>
<?php
	if($portal->CheckPriv($currentUser->UserID, 'report')) 
	{
		$rightSpacerMargin += 95;
?>
		
		<div class="navbar_item<?= $CURRENT_PAGE == 'Reports' ? '_selected' : '' ?>" 
			onclick="window.location = 'report.php';"
			style="width: 65px;">
			<a href="report.php">
				Reports<br/>onDemand
			</a>
		</div>
<?php
	} 
?>
		<div class="navbar_item navbar_rightspacer" style="margin-left: <?= $rightSpacerMargin; ?>px;">
		</div>
	</div>
</div>
<?php
	}
?>