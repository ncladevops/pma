<div class="list-group">
  <a href="#" id="MyReportDivtab" class="list-group-item active" onclick="loadReportTable('myReportDiv'); return false;">My Reports</a>
  <?php if($isSuper || $isMan): ?>
    <a href="#" id="leadReportDivtab" class="list-group-item" onclick="loadReportTable('dashboardReportDiv'); return false;">Lead Reports</a>
    <a href="#" id="contactReportDivtab" class="list-group-item" onclick="loadReportTable('contactReportDiv'); return false;">Contact Reports</a>
    <a href="#" id="MarketReportDivtab" class="list-group-item" onclick="loadReportTable('marketReportDiv'); return false;">Marketing Reports</a>
    <a href="#" id="CustomReportDivtab" class="list-group-item" onclick="loadReportTable('customReportDiv'); return false;">Custom Reports</a>
  <?php endif; ?>
  <?php if($isAdmin): ?>
    <a href="#" id="adminReportDivtab" class="list-group-item" onclick="loadReportTable('adminReportDiv'); return false;">Admin Reports</a>
  <?php endif; ?>
</div>