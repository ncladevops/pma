<meta http-equiv="X-UA-Compatible" content="IE=9" />
<div id="quickbar">
<?php 
	if($currentUser) {
?>
	Welcome <a href="#" onclick="showQuickbarMenu(); return false;"><?= $currentUser->Username ?></a>
	| <a href="help.php">Help</a>
<script type="text/javascript">
	function showQuickbarMenu() 
	{
		document.getElementById("quickbarMenu").style.display = "";
	}
	function hideQuickbarMenu() 
	{
		document.getElementById("quickbarMenu").style.display = "none";
	}	
</script>
	 <div id="quickbarMenu" style="display: none;">
	 	<div id="quickbarMenuHeader"><img src="images/close.png"/ onclick="hideQuickbarMenu()"/></div>
	 	<div onclick="window.location='edit_user.php'" class="quickbarMenuItem">Profile</div>
<?php 
		if($portal->CheckPriv($currentUser->UserID, 'subadmin')) 
		{
?>
	 	<div onclick="window.location='controlpanel.php'" class="quickbarMenuItem">Control Panel</div>
<?php 
		}
		if($portal->CheckPriv($currentUser->UserID, 'admin')) 
		{
?>
	 	<div onclick="window.location='admin/'" class="quickbarMenuItem">Admin</div>
<?php 
		}
?>
	 	<div onclick="window.location='login.php?logout=true'" class="quickbarMenuItem">Logout</div>
	 </div>
<?php
	} else {
?>
	Please Login
	| Help
<?php 
	}
?>
</div>
<div id="header">
	<div id="header_image_left">
		<?= $portal->GetCustomMessage('HeaderLeft', false, $currentUser->GroupID)->Message; ?>
	</div>
	<div id="header_image_right">
		<?= $portal->GetCustomMessage('HeaderRight', false, $currentUser->GroupID)->Message; ?>
	</div>
</div>