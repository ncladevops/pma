<?php
include_once('reminder.php');
// TODO Setup timeout 
// include_once('timeout.php'); 
if ($currentUser) {
    $logins = $portal->GetUserLogins($_SESSION['currentuserid']);
}
?>
<form name="sso_forum_2" id="sso_forum_2" method="POST" target="_blank" action="forum/ucp.php?mode=login">
    <input type="hidden" name="username" value="<?= $currentUser->Username ?>">
    <input type="hidden" name="password" value="optimize">
    <input type="hidden" name="autologin" value="1">
    <input type="hidden" name="login" value="1">
</form>
<!-- Static navbar -->
<div class="navbar navbar-default">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
    </div>
    <div class="navbar-collapse collapse">
        <ul class="nav navbar-nav">
            <li <?= $CURRENT_PAGE == 'Home' ? 'class="active"' : '' ?>><a href="home.php">Home</a></li>
            <?php if ($portal->CheckPriv($currentUser->UserID, 'leadsOnDemand') &&
            	$optifinowGlobalSetting['feature']['LeadsOnDemand']==TRUE): ?>
                <li <?= $CURRENT_PAGE == 'Leads' ? 'class="active"' : '' ?>><a href="list_lead.php">Leads onDemand</a></li>
            <?php endif; ?>
            <?php if ($portal->CheckPriv($currentUser->UserID, 'salesOnDemand') &&
            	$optifinowGlobalSetting['feature']['SalesOnDemand']==TRUE): ?>
                <li <?= $CURRENT_PAGE == 'Contacts' ? 'class="active"' : '' ?>><a href="contacts.php">Sales onDemand</a></li>
            <?php endif; ?>
            <?php if ($portal->CheckPriv($currentUser->UserID, 'marketingOnDemand') &&
            	$optifinowGlobalSetting['feature']['MarketingOnDemand']==TRUE): ?>
                <li class="hidden-xs" <?= $CURRENT_PAGE == 'Marketing' ? 'class="active"' : '' ?>><a href="marketing.php">Marketing onDemand</a></li>
            <?php endif; ?>
            <?php if ($optifinowGlobalSetting['feature']['ContentOnDemand']==TRUE): ?>
            <li <?= $CURRENT_PAGE == 'Files' ? 'class="active"' : '' ?>><a href="download_file.php">Content onDemand</a></li>
            <?php endif; ?>
            <?php if ($optifinowGlobalSetting['feature']['SocialOnDemand']==TRUE): ?>
            <li <?= $CURRENT_PAGE == 'Social' ? 'class="active"' : '' ?>>
                <a href="post_social.php">Social onDemand</a>
            </li>
            <?php endif; ?>
            <?php if ($optifinowGlobalSetting['feature']['CollaborateOnDemand']==TRUE): ?>
            <li>
                <a href="#" onclick="document.getElementById('sso_forum_2').submit()">Collaborate onDemand</a>
            </li>
            <?php endif; ?>
            <?php if ($portal->CheckPriv($currentUser->UserID, 'report')&&
            	$optifinowGlobalSetting['feature']['ReportsOnDemand']==TRUE): ?>
                <li class="hidden-xs <?= $CURRENT_PAGE == 'Reports' ? 'active' : '' ?>"><a href="report.php">Reports onDemand</a></li>
            <?php endif; ?>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li><?= str_replace("<br/>", "", $portal->GetCustomMessage('NavbarLink1', false, $currentUser->GroupID)->Message); ?></li>
            <li><?= str_replace("<br/>", " ", $portal->GetCustomMessage('NavbarLink2', false, $currentUser->GroupID)->Message); ?></li>
            <li><?= str_replace("<br/>", " ", $portal->GetCustomMessage('NavbarLink3', false, $currentUser->GroupID)->Message); ?></li>
        </ul>
    </div><!--/.nav-collapse -->
</div>