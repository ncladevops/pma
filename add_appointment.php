<?php
require("../printable/include/mysql.inc.php");
require("../printable/include/optimize.printable.inc.php");
require("globals.php");

$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);

$portal = new OptimizePortal($COMPANY_ID, $db);

$currentUser = $portal->GetUser($_SESSION['currentuserid']);

if (!$currentUser) {
    header("Location: login.php?message=" . urlencode("Not logged in or login error."));
    die();
}

$lead = $portal->GetOptimizeContact($_GET['id'], $currentUser->UserID);

if (!$lead) {
    die("Invalid Contact ID");
}


$appt = new ContactAppoinment();
$appt->ContactID = $lead->SubmissionListingID;
$appt->UserID = $currentUser->UserID;
$appt->AppointmentTime = date("m/d/Y g:00 a", strtotime("+1 hour"));
$appt->NotifyTime = date("m/d/Y g:00 a", strtotime("+1 hour"));
$appt->Notes = $_POST['Notes'];

if (strtotime($appt->NotifyTime)) {
    $dateDiff = intval((strtotime($appt->AppointmentTime) - strtotime($appt->NotifyTime)) / 60);
}

$error = 0;
$errorText = "";
$close = false;
if ($_POST['Submit'] == 'Set') {

    $appt->AppointmentTime = date("m/d/Y g:i a", strtotime($_POST['AppointmentDate'] . " " . $_POST['AppointmentHour'] . ":" . $_POST['AppointmentMinute'] . " " . $_POST['AppointmentOrd']));
    $appt->NotifyTime = date("m/d/Y g:i a", strtotime("$appt->AppointmentTime -" . intval($_POST['ReminderWarning']) . " minutes"));
    $appt->Notes = $_POST['Notes'];
    $appt->NotifyType = $_POST['NotifyType'];

    if (strtotime($appt->AppointmentTime) < strtotime("now") || strtotime($appt->NotifyTime) < strtotime("now")) {
        $error = 1;
        $errorText = "You cannot schedule a reminder for the past.";
    } else {
        if ($appt->ContactAppointmentID) {
            $portal->UpdateAppointment($appt);
        } else {
            $AppointmentID = $portal->CreateAppointment($appt);

            $ceID = $portal->LogContactEvent($appt->ContactID, $APPOINTMENT_EVENTTYPE_ID, $currentUser->UserID);

            $ce = $portal->GetContactEvent($ceID);

            if ($ce) {
                $ce->EventTime = date("Y-m-d H:i:s");
                $ce->EventNotes = "Set Online $appt->NotifyType Reminder.";

                if ($portal->UpdateContactEvent($ce)) {
                    $close = true;
                } else {
                    $_GET['message'] = "Can't update event!";
                }
            } else {
                $_GET['message'] = "Can't add event!";
            }
        }
        $close = true;
    }
}
/*
  $aptmenttime = date("Y-m-d H:i:s",
  strtotime($_POST['AppointmentDate'] . " " . $_POST['AppointmentHour'] . ":" . $_POST['AppointmentMinute'] . " " . $_POST['AppointmentOrd']));
  echo "aptmenttime is $aptmenttime";
  echo "<br/>";
  echo "appt->ContactID is $appt->ContactID";

  $newAppointmentID = $portal->GetAppIdFromTime($aptmenttime, $appt->ContactID);

  echo "<br/>";
  echo $newAppointmentID;
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <title>
            <?= $portal->CurrentCompany->CompanyName ?>  :: Add Reminder for <?= "$lead->FirstName $lead->LastName" ?>
        </title>
        <meta http-equiv="content-type" content="text/html;charset=utf-8" />		
        <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />	

        <?php include("components/bootstrap.php") ?>

    </head>
    <body onload="<?= $close == true ? "parent.location='download_vcs.php?id=$AppointmentID'" : ($error == 1 ? "alert('$errorText')" : "" ) ?>">
        <form method="post" action="<?= $_SERVER['PHP_SELF'] ?>?id=<?= $lead->SubmissionListingID; ?>" class="form-inline" role="form">
            <div class="sectionHeader">
                <h3>Add Reminder</h3>
            </div>
            <div id="AddAppointmentDiv" class="sectionDiv well">
                <div class="itemSection">
                    <div id="ContactNameDiv">
                        <label for="ContactName">Contact Name:</label><br/>
                        <?= "$lead->FirstName $lead->LastName" ?>
                    </div>
                </div>
                <div class="itemSection">
                    <div id="AppointmentDateDiv">
                        <label for="AppointmentDate">Appointment Date:</label><br/>
                        <input type="text" id="AppointmentDate" name="AppointmentDate" class="datepicker form-control input-sm" value="<?= date("m/d/Y", strtotime($appt->AppointmentTime)) ?>" />
                    </div>
                </div>
                <div class="itemSection">
                    <div id="AppointmentTimeDiv" class="form-group col-xs-4">
                        <label class="control-label" for="AppointmentTime">Time:</label>
                        <select class="form-control input-sm" name="AppointmentHour">
                            <?php
                            for ($h = 1; $h <= 12; $h++) {
                                ?>
                                <option 
                                    value="<?= $h ?>" 
                                    <?= $h == date("g", strtotime($appt->AppointmentTime)) ? "SELECTED" : "" ?>><?= $h ?></option>
                                    <?php
                                }
                                ?>
                        </select>
                    </div>
                    <div id="AppointmentTimeDiv2" class="form-group col-xs-4">
                        <label for="AppointmentMinute">&nbsp;</label>
                        <select class="form-control input-sm" name="AppointmentMinute">
                            <?php
                            for ($m = 0; $m <= 60; $m+=5) {
                                ?>
                                <option 
                                    value="<?= sprintf("%02d", $m) ?>" 
                                    <?= $m == date("i", strtotime($appt->AppointmentTime)) ? "SELECTED" : "" ?>><?= sprintf("%02d", $m) ?></option>
                                    <?php
                                }
                                ?>
                        </select>
                    </div>
                    <div id="AppointmentTimeDiv3" class="form-group col-xs-4">
                        <label for="AppointmentOrd">&nbsp;</label>
                        <select class="form-control input-sm" name="AppointmentOrd">
                            <option	value="am" 
                                    <?= "am" == date("a", strtotime($appt->AppointmentTime)) ? "SELECTED" : "" ?>>am</option>
                            <option	value="pm" 
                                    <?= "pm" == date("a", strtotime($appt->AppointmentTime)) ? "SELECTED" : "" ?>>pm</option>
                        </select>
                    </div>
                </div>
                <div class="itemSection">
                    <div id="ReminderWarningDiv">
                        <label for="ReminderWarning">Reminder:</label><br/>
                        <select class="form-control input-sm" name="ReminderWarning">
                            <option value="none">None</option>
                            <option value="5" <?= $dateDiff == 5 ? "SELECTED" : "" ?>>5 Minutes</option>
                            <option value="10" <?= $dateDiff == 10 ? "SELECTED" : "" ?>>10 Minutes</option>
                            <option value="15" <?= $dateDiff == 15 ? "SELECTED" : "" ?>>15 Minutes</option>
                            <option value="20" <?= $dateDiff == 20 ? "SELECTED" : "" ?>>20 Minutes</option>
                            <option value="30" <?= $dateDiff == 30 ? "SELECTED" : "" ?>>30 Minutes</option>
                            <option value="60" <?= $dateDiff == 60 ? "SELECTED" : "" ?>>1 hour</option>
                            <option value="120" <?= $dateDiff == 120 ? "SELECTED" : "" ?>>2 hours</option>
                        </select>
                    </div>
                    <div id="ReminderTypeDiv">
                        <label for="NotifyType">Reminder Type:</label><br/>
                        <select class="form-control input-sm" name="NotifyType">
                            <option value="popup" <?= $appt->NotifyType == 'popup' ? "SELECTED" : "" ?>>Pop Up</option>
                            <option value="email" <?= $appt->NotifyType == 'email' ? "SELECTED" : "" ?>>Email</option>
                        </select>
                    </div>
                </div>
                <div class="longItemSection">
                    <div id="EventNotesDiv">
                        <label for="Notes">Notes:</label><br/>
                        <textarea name="Notes" rows="8" style="width: 95%;"><?= $appt->Notes ?></textarea>
                    </div>
                </div>
            </div>	
            <div class="buttonDiv">
                <div class="buttonSection">
                    <input type="submit" value="Set" name="Submit" class="btn btn-primary" id="Submit"/>&nbsp;&nbsp;<input type="button" class="btn btn-default" value="Cancel" onclick="window.close()" />
                </div>
            </div>
        </form>

        <?php include("components/bootstrap-footer.php") ?>

    </body>
</html>