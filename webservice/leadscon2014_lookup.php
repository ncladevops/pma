<?php
	$lookup_fields = array();
	
	$lookup_fields['FirstName'] = 'firstname';
	$lookup_fields['LastName'] = 'lastname';
	$lookup_fields['Company'] = 'company';
	$lookup_fields['Phone2'] = 'mobilenumber';
	$lookup_fields['Email'] = 'email';
	$lookup_fields['Category'] = 'category';
	$lookup_fields['Segment'] = 'segment';
	$lookup_fields['Comments'] = 'notes';
	
	