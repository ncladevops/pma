<?php
	$lookup_fields = array();
	
	$lookup_fields['FirstName'] = 'first_name';
	$lookup_fields['LastName'] = 'last_name';
	$lookup_fields['Company'] = 'company';
	$lookup_fields['Title'] = 'title';
	$lookup_fields['Phone'] = 'phone';
	$lookup_fields['Email'] = 'email';
	$lookup_fields['State'] = 'state';
	$lookup_fields['Segment'] = 'industry';
	
	