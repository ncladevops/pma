<?php

require("../../printable/include/mysql.inc.php");
require("../../printable/include/leadsondemand.printable.inc.php");
require("../globals.php");
require("sms_twilio_lookup.php");


$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);

$portal = new LeadsOnDemandPortal($COMPANY_ID, $db);

$postText = trim(file_get_contents('php://input'));

$sms = new Sms();

foreach ($lookup_fields as $k => $v) {
    if (is_array($v)) {
        $sms->$k = $v['map'][$_POST[$v['field']]];
    } else {
        $sms->$k = $_POST[$v];
    }
}

$sms->Type = "Response";
$sms->Time = date("Y-m-d H:i:s");

$smsid = $portal->AddSms($sms);

if ($smsid) {

    $wherequery = "REPLACE(submissionlisting.Phone2,'-','') LIKE '%" . mysql_real_escape_string(stripslashes(str_replace('+1', '', str_replace('-', '', $sms->From)))) . "%'";

    $lead = $portal->GetLeadsOnDemandContacts('all', 'all', $wherequery, false, 'submissionlisting.SubmissionListingID DESC', 1, 0);

    if ($lead) {
        $sms->SubmissionListingID = $lead[0]->SubmissionListingID;
        $sms->UserID = $lead[0]->UserID;

        $portal->UpdateSms($sms, $smsid);

        // Write info to contact log
        $contactEvent = $portal->GetContactEvent(
                $portal->LogContactEvent($lead[0]->SubmissionListingID, 0, $lead[0]->UserID));

        $contactEvent->EventNotes = "SMS response received";

        $portal->UpdateContactEvent($contactEvent);
    }
}

$SMSUnsubscribe = 0;
if ($sms->Body === "STOP") {
    $SMSUnsubscribe = 1;
    $SMSUnsubscribeDate = date("Y-m-d H:i:s");
    $portal->Unsubscribe_Contact($sms->SubmissionListingID, $SMSUnsubscribe, $SMSUnsubscribeDate);
    if ($portal->Unsubscribe_Check($sms->SubmissionListingID)) {
        header('Content-type:text/xml');
        echo "<Response><Sms>You are already unsubscribed. Text START to resubscribe.</Sms></Response>";
        die();
    } else {
        header('Content-type:text/xml');
        echo "<Response><Sms>You have succesfully been unsubscribed. Text START to resubscribe.</Sms></Response>";
        die();
    }
} else if ($sms->Body === "START") {
    $SMSUnsubscribe = 0;
    $SMSUnsubscribeDate = date("Y-m-d H:i:s");
    $portal->Unsubscribe_Contact($sms->SubmissionListingID, $SMSUnsubscribe, $SMSUnsubscribeDate);
    if ($portal->Unsubscribe_Check($sms->SubmissionListingID)) {
        header('Content-type:text/xml');
        echo "<Response><Sms>You have successfully been re-subscribed. Reply STOP to unsubscribe.</Sms></Response>";
        die();
    } else {
        header('Content-type:text/xml');
        echo "<Response><Sms>You are already subscribed. Reply STOP to unsubscribe.</Sms></Response>";
        die();
    }
} else {
    header('Content-type:text/xml');
    echo "<Response><Sms></Sms></Response>";
    die();
}
