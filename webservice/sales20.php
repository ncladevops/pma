<?php
	require("../../printable/include/mysql.inc.php");
	require("../../printable/include/optimize.printable.inc.php");
	require("../globals.php");
	require("sales20_lookup.php");
	
	$LEAD_SOURCE_ID = 55;
		
	error_reporting(0);
	
	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	
	$portal = new OptimizePortal($COMPANY_ID, $db);
	
	$postText = trim(file_get_contents('php://input'));
	
	$leadSource = $portal->GetSubmissionListingSource($LEAD_SOURCE_ID);
	$listingType = $portal->GetSourceListingType($leadSource->SubmissionListingSourceID, $_POST[$leadSource->KeyField]);
	
	$lead = new OptimizeContact();
	
	$lead->ListingType = $listingType->SubmissionListingTypeID;
	$lead->UserID = $portal->GetNextLeadAssignment();
	$lead->ContactStatusTypeID = $listingType->DefaultContactStatusTypeID;

	foreach($lookup_fields as $k=>$v)
	{
		if(is_array($v))
		{
			$lead->$k = $v['map'][$_POST[$v['field']]];
		}
		else
		{
			$lead->$k = $_POST[$v];
		}
	}
	
	$lID = $portal->AddOptimizeContact($lead, $postText);
	$lead = $portal->GetOptimizeContact($lID, $lead->UserID);

	if($lead)
	{

		$cet = $portal->GetContactEventType($_POST['event']);
		
		if($cet)
		{
			$ceID = $portal->LogContactEvent($lead->SubmissionListingID, $cet->ContactEventTypeID, $lead->UserID); //42074 = OptifiNow Team (Marketing Admin)
			
			$ce = $portal->GetContactEvent($ceID);
			
			if($ce)
			{
				$ce->EventTime = date("Y-m-d H:i:s");
				$ce->EventNotes = "Sales 2.0";

				if($portal->UpdateContactEvent($ce))
				{
					$close = true;
				}
				
			}
		}
				
		echo 
'SUCCESS';
		die();			
	}
	
	echo 
'FAIL';

