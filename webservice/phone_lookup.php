<?php
	$lookup_fields = array();
	
	$lookup_fields['ExternalID'] = 'Id';
	$lookup_fields['FirstName'] = 'First';
	$lookup_fields['LastName'] = 'Last';
	$lookup_fields['Phone'] = 'Home_Phone';
	$lookup_fields['Phone2'] = 'Work_Phone';
	$lookup_fields['WorkPhone'] = 'Work_Phone';
	$lookup_fields['Email'] = 'Email_Address';
	$lookup_fields['Address1'] = 'Street_Address';
	$lookup_fields['City'] = 'City';
	$lookup_fields['State'] = 'State';
	$lookup_fields['Zip'] = 'Zip';	