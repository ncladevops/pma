<?php
	require("../../printable/include/mysql.inc.php");
	require("../../printable/include/optimize.printable.inc.php");
	require("../globals.php");
	require("optifiweb_lookup.php");
	
	$LEAD_SOURCE_ID = 29;
		
//	error_reporting(0);
	
	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	
	$portal = new OptimizePortal($COMPANY_ID, $db);
	
	$postText = trim(file_get_contents('php://input'));
	
	$leadSource = $portal->GetSubmissionListingSource($LEAD_SOURCE_ID);
	$listingType = $portal->GetSourceListingType($leadSource->SubmissionListingSourceID, $_POST[$leadSource->KeyField]);
	
	$lead = new OptimizeContact();
	
	$lead->ListingType = $listingType->SubmissionListingTypeID;
	$lead->UserID = $portal->GetNextLeadAssignment();
	$lead->ContactStatusTypeID = $listingType->DefaultContactStatusTypeID;
	
	foreach($lookup_fields as $k=>$v)
	{
		if(is_array($v))
		{
			$lead->$k = $v['map'][$_POST[$v['field']]];
		}
		else
		{
			$lead->$k = $_POST[$v];
		}
	}
	
	$lID = $portal->AddOptimizeContact($lead, $postText);
	$lead = $portal->GetOptimizeContact($lID, $lead->UserID);
	
	if($lead)
	{
		$ext = $portal->GetNextExtension($listingType->SubmissionListingTypeID);
		
		if($ext)
		{
			
		}
				
		echo 
'SUCCESS';
		die();			
	}
	
	echo 
'FAIL';

