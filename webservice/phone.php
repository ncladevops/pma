<?php
	require("../../printable/include/mysql.inc.php");
	require("../../printable/include/optimize.printable.inc.php");
	require("../globals.php");
	require("phone_lookup.php");
	include_once("../../printable/include/component/switchvoxtransfer.php");
	
	$LEAD_SOURCE_ID = 27;
		
//	error_reporting(0);
	
	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	
	$portal = new OptimizePortal($COMPANY_ID, $db);
	
	$postText = trim(file_get_contents('php://input'));

	try 
	{
		$xml = new SimpleXMLElement($postText);
	} 
	catch(Exception $e)
	{
		echo 'FAIL: Invalid XML';
		die();
	}
	
	if($xml->Lead->Email_Address == '') {
		echo 'FAIL: Email Address Required';
		die();
	}
	
	$leadSource = $portal->GetSubmissionListingSource($LEAD_SOURCE_ID);
	$listingType = $portal->GetSourceListingType($leadSource->SubmissionListingSourceID, $xml->Lead->{$leadSource->KeyField});
		
	$lead = new OptimizeContact();
	
	$lead->ListingType = $listingType->SubmissionListingTypeID;
	$lead->UserID = $portal->GetNextLeadAssignment($listingType->GroupID);
	$lead->ContactStatusTypeID = $listingType->DefaultContactStatusTypeID;
	
	foreach($lookup_fields as $k=>$v)
	{
		if(is_array($v))
		{
			$lead->$k = $v['map'][(string) $xml->Lead->$v['field']];
		}
		else
		{
			$lead->$k = (string) $xml->Lead->$v;
		}
	}
	
	$lead->PublicContact = 1;
						
	$lID = $portal->AddOptimizeContact($lead, $postText);
	$lead = $portal->GetOptimizeContact($lID, $lead->UserID);
	
	if($lead)
	{
		$ext = $portal->GetNextExtension($listingType->SubmissionListingTypeID);
		
		if($ext)
		{
			$stSystem = new SwitchvoxTransferSystem($SWITCHVOX_TRANSFER_URL, $SWITCHVOX_TRANSFER_USER, $SWITCHVOX_TRANSFER_PASS);
			
			$portal->MakePhoneSystemCall($stSystem, $lead, $ext);
		}
		
		echo 'SUCCESS';
		die();			
	}
	
	echo 'FAIL';

