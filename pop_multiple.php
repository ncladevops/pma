<?php
require("../printable/include/mysql.inc.php");
require("../printable/include/optimize.printable.inc.php");
require("globals.php");

$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);

$portal = new OptimizePortal($COMPANY_ID, $db);

$currentUser = $portal->GetUser($_SESSION['currentuserid']);

if (!$currentUser) {
    header("Location: login.php?message=" . urlencode("Not logged in or login error."));
    die();
}

if ($portal->CheckPriv($currentUser->UserID, 'subadmin')) {
    $userString = "all";
} else {
    $userString = $currentUser->UserID;
}

// Set last Notifty
$appt->LastNotifyTime = date("Y-m-d H:i:s");
$portal->UpdateAppointment($appt);

if ($_POST['Submit'] == 'Postpone') {
    $appt->NotifyTime = date("Y-m-d H:i:s", strtotime("+" . intval($_POST['ReminderWarning']) . " minutes"));
    $portal->UpdateAppointment($appt);
    $close = true;
} elseif ($_POST['Submit'] == 'Dismiss') {
    $appt->NotifyTime = "0000-00-00";
    $portal->UpdateAppointment($appt);
    $close = true;
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title>
        <?= $portal->CurrentCompany->CompanyName ?> Marketing on Demand :: Reminder
        for <?= "$lead->FirstName $lead->LastName" ?>
    </title>
    <meta http-equiv="content-type" content="text/html;charset=utf-8"/>
    <link type="text/css" media="all" rel="stylesheet" href="style.css"/>
    <link type="text/css" media="all" rel="stylesheet" href="css/pop_appointment.css"/>
    <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico"/>
    <script src="js/func.js"></script>
</head>
<body onload="<?= $close == true ? "window.close();" : ($error == 1 ? "alert('$errorText')" : "") ?>">
<?php
$currentUser = $portal->GetUser($_SESSION['currentuserid']);
$appt = $portal->GetAllOpenNotifies('popup', $currentUser->UserID);
?>
<div class="sectionHeader"><?= "$currentUser->FirstName $currentUser->LastName's " ?> Reminders</div>
<div id="PopAppointmentDiv" class="sectionDiv">
    <b>
        <center>Click on an Appointment Time to view the Reminder</center>
    </b>
    <table cellpadding="5" width="100%">
        <tr class="colHead" size="2em">
            <td>Contact Name</td>
            <td>Company</td>
            <td>Appointment Time</td>
        </tr><?php
        $i = 0;
        foreach ($appt as $a) {
            $i++;
            $currentContact = $portal->GetOptimizeContact($a->ContactID, $currentUser->UserID);

            $aptime = date('n/j/Y g:i a', strtotime($a->AppointmentTime));
            ?>
        <tr class="itemSection<?= $i % 2 == 0 ? 'even' : 'odd' ?>">
            <td>
                <?= $currentContact->FirstName . " " . $currentContact->LastName ?>
            </td>
            <td>
                <?= $currentContact->Company ?>
            </td>
            <td>
                <a href="#"
                   onclick="wopen('pop_appointment.php?id=<?= $a->ContactAppointmentID ?>', 'reminder', 350, 450);"
                   title="Reminder"><?= $aptime ?></a>
            </td>
            </tr><?php
        }
        ?>
    </table>
</div>
</body>
</html>