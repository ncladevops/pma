<?php
require("../printable/include/mysql.inc.php");
require("../printable/include/optimize.printable.inc.php");
require ("../printable/include/component/twilio-php/Services/Twilio.php");
require("globals.php");

$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);

$portal = new OptimizePortal($COMPANY_ID, $db);

$currentUser = $portal->GetUser($_SESSION['currentuserid']);

if (!$currentUser) {
    header("Location: login.php?message=" . urlencode("Not logged in or login error."));
    die();
}

$lead = $portal->GetOptimizeContact($_GET['id'], $currentUser->UserID);

if (!$lead) {
    die("Invalid Contact ID");
}

if ($portal->CurrentCompany->TwilioNumber == null || $portal->CurrentCompany->TwilioNumber == '') {
    die("No SMS number linked to account");
}

if ($lead->Phone2 == '' || $lead->Phone2 == null) {
    die("Invalid Lead Cell Phone Number");
}

if ($portal->Unsubscribe_Check($lead->SubmissionListingID)) {
    die("The number specified has been Unsubscribed.");
}

$messages = $portal->GetContactSms($lead->SubmissionListingID);

if ($_POST['Submit'] == 'Send') {

    $message = $_POST['message'];

    $lead = $portal->GetOptimizeContact($_GET['id'], $currentUser->UserID);

    $account_sid = $portal->CurrentCompany->TwilioAccountSID;
    $auth_token = $portal->CurrentCompany->TwilioAuthToken;
    $client = new Services_Twilio($account_sid, $auth_token);

    if ($lead->Phone2 != '') {
        $res = $client->account->messages->create(array(
            'To' => $lead->Phone2,
            'From' => $portal->CurrentCompany->TwilioNumber,
            'Body' => $message
        ));
        
// Record error if send fails
        $stat_code = $res->status;
        $stat_message = "Queued for sending";
    } else {
// Record error if invalid email
        $stat_code = "Invalid Phone Number";
        $stat_message = "The phone number specified was invalid.";
    }

    $success = $stat_code == "queued" ? true : false;

    // Write info to contact log
    $contactEvent = $portal->GetContactEvent(
            $portal->LogContactEvent($lead->SubmissionListingID, 0, $currentUser->UserID));

    if ($success) {
        $contactEvent->EventNotes = "SMS successfully sent - Manual";
    } else {
        $contactEvent->EventNotes = "Error sending SMS - Manual";
    }

    $time = date("Y-m-d H:i:s");

    //Get SMS Instance
    $sms = new Sms();

    $sms->MessageSid = $res->sid;
    $sms->AccountSid = $res->account_sid;
    $sms->SubmissionListingID = $lead->SubmissionListingID;
    $sms->CompanyID = $COMPANY_ID;
    $sms->From = $res->from;
    $sms->To = $res->to;
    $sms->Body = $res->body;
    $sms->NumMedia = $res->num_media;
    $sms->Time = $time;
    $sms->Type = "Manual Send";
    $sms->UserID = $currentUser->UserID;

    $smsid = $portal->AddSms($sms);

    $portal->UpdateContactEvent($contactEvent);
    
    //Send closing text
            $res2 = $client->account->messages->create(array(
            'To' => $lead->Phone2,
            'From' => $portal->CurrentCompany->TwilioNumber,
            'Body' => "Reply STOP to unsubscribe. Msg & Data rates may apply."
        ));
            
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <title>
            <?= $portal->CurrentCompany->CompanyName ?>  :: Send SMS to <?= "$lead->FirstName $lead->LastName" ?>
        </title>
        <meta http-equiv="content-type" content="text/html;charset=utf-8" />		
        <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />	

        <?php include("components/bootstrap.php") ?>

    </head>

    <body>

        <?php if ($_POST['Submit'] == 'Send'): ?>

            <div id="SMSDiv" class="well">
                <strong>SMS message successfully sent to <?= $lead->FirstName . " " . $lead->LastName ?> on <?= $time ?> </strong><br/>
            </div>

        <?php else: ?>
            <!-- Nav tabs -->
            <ul class="nav nav-tabs">
                <li class="active"><a href="#send" data-toggle="tab">Send</a></li>
                <li><a href="#history" data-toggle="tab">History</a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane active" id="send">

                    <form action="<?= $_SERVER['PHP_SELF'] ?>?id=<?= $lead->SubmissionListingID . (isset($_GET['index']) ? "&index=" . $_GET['index'] : ""); ?>" method="post" role="form">
                        <div class="form-group">
                            <label for="Recipient"></label>
                            <input type="text" class="hidden-print form-control input-sm" readonly value="To: <?php echo $lead->FirstName . ' ' . $lead->LastName; ?> ( <?php echo format_phone($lead->Phone2); ?> )"></input>
                        </div>
                        <div class="form-group">
                            <label for="Message">Message</label>
                            <textarea id="message" name="message" class="hidden-print form-control input-sm"></textarea>
                        </div>
                        <div class="form-group">
                            <center><input type="submit" value="Send" name="Submit" class="btn btn-success btn-sm"/></center>
                        </div>
                    </form>

                </div>
                <div class="tab-pane" id="history">
                        <table class="event_table table table-striped table-hover table-bordered">
                            <thead style="font-weight: bold;">
                                <tr>
                                    <td>Sender</td>
                                    <td>Text</td>
                                    <td>Time</td>
                                </tr>

                            </thead>
                            <tbody>

                                <?php foreach ($messages as $m): ?>
                                    <tr>
                                        <?php if ($m->Type != ""): ?>
                                            <?php
                                            if ($m->Type == "Response") {
                                                $label = 'warning';
                                            } else if ($m->Type == "Manual Send") {
                                                $label = 'info';
                                            } else {
                                                $label = 'default';
                                            }
                                            ?>
                                            <td align="center"><span class="label label-<?= $label ?>"><?= ($m->Type == "Response") ? $m->LeadName : $m->UserName ?></span></td>
                                        <?php else: ?>
                                            <td align="center"><span class="label label-default"><?= (str_replace('+1', '', str_replace('-', '', $m->From)) == str_replace('+1', '', str_replace('-', '', $lead->Phone2))) ? $lead->FirstName . ' ' . $lead->LastName : $currentUser->FirstName . ' ' . $currentUser->LastName ?></span></td>
                                        <?php endif; ?>
                                        <td align="left" style="min-width: 200px;"><?php echo $m->Body; ?></td>
                                        <td align="center"><i><?php echo date('Y-m-d g:ia', strtotime($m->Time)); ?></i></td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                </div>
            </div>

        <?php endif; ?>

        <?php include("components/bootstrap-footer.php") ?>

    </body>
</html>