<?php
	require("../printable/include/mysql.inc.php");
	require("../printable/include/optimize.printable.inc.php");
	require("globals.php");
		
	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	
	$portal = new OptimizePortal($COMPANY_ID, $db);
	
	// Querry to get user info
	$currentUser = $portal->GetUser($_SESSION['currentuserid']);
	$isSubAdmin = $portal->CheckPriv($currentUser->UserID, 'subadmin');
	
	// Build lookup for Event Types
  	$cetTemp = $portal->GetContactEventTypes();
  	$contactEventTypes = array();
  	
  	$cet = new ContactEventType();
  	foreach ($cetTemp as $cet)
  	{
  		$contactEventTypes[$cet->ContactEventTypeID] = $cet;
  	}
	
	// Check login
	if( !$currentUser ) 
	{
		die("Invalid User");
  	}

  	$event = $portal->GetContactEvent($_GET['ceid']);
  	
  	if(!$event)
  	{
  		die("Invalid Event");
  	}
  	
	if($isSubAdmin) {
		$userString = 'all';
	}
	else {
		$userString = $currentUser->UserID;
	}
  	$contact = $portal->GetContact($event->ContactID, $userString);
  	
  	if(!$contact)
  	{
  		die("Invalid Contact");
  	}
  	  	
  	$closeWindow = false;
  	
  	// if submit was pressed update info
  	if($_POST['Submit'] == 'Update')
  	{
  		$error = 0;
  		$errorText = "";
  		  		  		
  		if($error == 0)
  		{
	  		$event->EventNotes = $_POST['EventNotes'];
	  		
	  		$portal->UpdateContactEvent($event);
	  		
	  		$closeWindow = true;
	  		
  		}
  		 		
  	}
  	elseif ($_POST['Submit'] == 'Cancel')
  	{
  		$closeWindow = true;
  	}
  		
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	<title>
		<?= $portal->CurrentCompany->CompanyName ?> :: Edit Contact Info
	</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />		
	<link type="text/css" media="all" rel="stylesheet" href="style.css" />
	<link type="text/css" media="all" rel="stylesheet" href="css/setnotes.css" />
	<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />
	<script  src="include/js/func.js"></script>
</head>
<body onload="<?= $error == 1 ? "alert('$errorText')" : "" ?><?= $closeWindow ? "window.close()" : "" ?>">
<div id="body">
	<form method="post" action="<?= $_SERVER['PHP_SELF'] ?>?ceid=<?= $event->ContactEventID ?>">
	<div id="headerText">
		<?= $contact->Company ?> - <?= $contact->FirstName . " " . $contact->LastName ?>
	</div>
	<div id="detailDiv">
		Add Notes for Status Change <b><?= $contactEventTypes[$event->ContactEventTypeID]->EventType ?></b>
	  	<textarea name="EventNotes" id="EventNotes" rows="7" style="width: 100%;"></textarea>
	</div>
	<div id="buttonDiv">
		<input type="submit" name="Submit" value="Update" />&nbsp;&nbsp;&nbsp;<input type="button" value="Cancel" onclick="window.close();" />
	</div>
	</form>
</div>
</body>
</html>