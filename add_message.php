<?php
require("../printable/include/mysql.inc.php");
require("../printable/include/optimize.printable.inc.php");
require("globals.php");

$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);

$portal = new OptimizePortal($COMPANY_ID, $db);

$currentUser = $portal->UserAccess($_SESSION['currentuserid']);
$isSuper = $portal->CheckPriv($currentUser->UserID, 'supervisor');

if (!$portal->CheckPriv($currentUser->UserID, 'subadmin')) {
    header("Location: " . $portal->CurrentCompany->Website . "home.php?message=" . urlencode("Accessed Denied."));
    die();
}

$gs = $portal->GetCompanyGroups();

$groups = array();

$groups[0] = new Group();
$groups[0]->GroupID = 0;
$groups[0]->GroupName = "Default";

foreach ($gs as $g) {
    $groups[$g->GroupID] = $g;
}

$messageNames = $portal->GetCompanyMessageNames();

// Check for Button Pressed
if (isset($_POST['Add'])) {
    $messageID = $portal->AddMessage($_POST['message'], $_POST['groupid']);

    header("Location: edit_message.php?messageid=$messageID");
} else if (isset($_POST['Cancel'])) {
    header("Location: manage_messages.php?status=" . urlencode("Message add cancelled"));
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <title>
            <?= $portal->CurrentCompany->CompanyName ?> :: Add Custom Message
        </title>
        <meta http-equiv="content-type" content="text/html;charset=utf-8" />		
        <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />
        <script  src="js/func.js"></script>	
        <?php include("components/bootstrap.php") ?>
    </head>
    <body bgcolor="#FFFFFF">
        <div id="page">
            <?php include("components/header.php") ?>
            <div id="body">
                <?php
                $CURRENT_PAGE = "Home";
                include("components/navbar.php");
                ?>
                <form name="form1" method="post" action="<?= $_SERVER['PHP_SELF'] ?>">
                    
                <?php if (isset($_GET['message'])): ?>
                    <div class="container">
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <?= $_GET['message']; ?>
                        </div>
                    </div>
                <?php endif; ?> 
                    
                    <div id="AddMessageDiv" class="well container">
                        <div class="sectionHeader">
                            <h2>Add New Message</h2>
                        </div>
                        <div class="sectionDiv">
                            <div class="itemSection row">
                                <div id="MessageNameDiv" class="form-group col-md-3">
                                    <label>Message Name:</label>
                                    <select name="message" class="form-control input-sm">
                                        <?php
                                        foreach ($messageNames as $m) {
                                            ?>
                                            <option value="<?= $m ?>"><?= $m ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="itemSection row">
                                <div id="GroupNameDiv" class="form-group col-md-3">
                                    <label>Group Name:</label>
                                    <select name="groupid" class="form-control input-sm">
                                        <?php
                                        foreach ($groups as $g) {
                                            ?>
                                            <option value="<?= $g->GroupID ?>"><?= $g->GroupName ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>		
                        <center>
                            <div class="itemSection row">
                                <div class="buttonSection">
                                    <input type="submit" class="btn btn-info btn-sm" value="Add" name="Add" id="updateButton"/>&nbsp;&nbsp;<input type="submit" value="Cancel" class="btn btn-default btn-sm" name="Cancel"/>
                                </div>
                            </div>
                        </center>
                    </div>
                </form>
            </div>
        </div>
        <?php include("components/footer.php") ?>
    </body>
</html>