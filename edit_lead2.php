<?php
require("../printable/include/mysql.inc.php");
require("../printable/include/optimize.printable.inc.php");
require("../printable/include/component/fanniemae.php");
require("globals.php");

if ($_POST['popup_window'] == 'true') {
    $_GET['popup'] = true;
}

$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);

$portal = new OptimizePortal($COMPANY_ID, $db);

$currentUser = $portal->GetUser($_SESSION['currentuserid']);

if (!$currentUser) {
    header("Location: login.php?message=" . urlencode("Not logged in or login error."));
    die();
}

$isAdmin = $portal->CheckPriv($currentUser->UserID, 'admin');
$isCorpMan = $portal->CheckPriv($currentUser->UserID, 'corporatemanager');
$isRegMan = $portal->CheckPriv($currentUser->UserID, 'regionmanager');
$isDivMan = $portal->CheckPriv($currentUser->UserID, 'divisionmanager');
if ($isAdmin || $isCorpMan || $isRegMan || $isDivMan)
    $isMan = true;
else
    $isMan = false;

$lead = $portal->GetOptimizeContact($_GET['id'], $currentUser->UserID);

$lead_step_ids = explode(";", $_SESSION['lead_step_ids']);

if (!$lead) {
    header("Location: home.php?message=" . urlencode("Lead not Available"));
    die();
}

if ($_POST['Submit'] == 'Update' || $_POST['Submit'] == 'Update & Close') {
    $_POST['DOB'] = strtotime("1/1/" . $_POST['DOB']) ? date("Y-1-1", strtotime("1/1/" . $_POST['DOB'])) : "";
    $_POST['CoDOB'] = strtotime("1/1/" . $_POST['CoDOB']) ? date("Y-1-1", strtotime("1/1/" . $_POST['CoDOB'])) : "";


    if ($_POST['ContactStatusTypeID'] == 1 && !$isMan) {
        $_POST['ContactStatusTypeID'] = $lead->ContactStatusTypeID;
    }

    $oldStatus = $lead->ContactStatusTypeID;
    foreach ($_POST as $k => $v) {
        $lead->$k = $v;
    }

    if ($isMan && $_POST['NewUserID'] != $lead->UserID) {
        $lead->UserID = $_POST['NewUserID'];
    } else {
        $lead->UserID = $currentUser->UserID;
    }


    $portal->UpdateOptimizeContact($lead);

    if ($_POST['Submit'] == 'Update & Close') {
        header('Location: list_lead.php?message=' . urlencode("Lead updated successfully."));
        die();
    }
} elseif ($_POST['Submit'] == 'Cancel') {
    header('Location: list_lead.php?message=' . urlencode("Action Canceled. Lead not updated."));
    die();
} elseif ($_POST['Submit'] == 'Delete' && $isMan) {
    $portal->DeleteContact($lead->SubmissionListingID, ($isMan ? $lead->UserID : $currentUser->UserID));

    header('Location: list_lead.php?message=' . urlencode("Lead deleted successfully."));
    die();
}
if ($_POST['Submit'] == 'Export to FannieMae File') {
    $fm = new FannieMae($lead);
    $outString = $fm->generateFNM();

    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header('Content-type: application/force-download');
    header("Content-Transfer-Encoding: binary");
    header("Content-Disposition: attachment; filename=\"$lead->LastName.fnm\";");
    header("Content-Transfer-Encoding: binary");

    echo $outString;
    die;
}

$csts = $portal->GetContactStatusTypes('all', 1, true);
$contactStatusesTypes = array();
$shortStatuses = array();
$shortStatusDetails = array();
foreach ($csts as $cst) {
    if (!$cst->Disabled) {
        $shortStatuses[$cst->ShortName] = (strlen($cst->DetailName) > 0 ? 1 : 0);
        $shortStatusDetails[$cst->ShortName][] = $cst->ContactStatus;
        $contactStatusesTypes[$cst->ContactStatus] = $cst;
    }
}
$contactEventTypes = $portal->GetContactEventTypes();
$cetLookup = array();
foreach ($contactEventTypes as $cet) {
    $cetLookup[$cet->ContactEventTypeID] = $cet->EventType;
}

$allEvents = $portal->GetContactHistory($lead->SubmissionListingID);

$messages = $portal->GetContactSms($lead->SubmissionListingID);

$users = $portal->GetCompanyUsers();
$slt = $portal->GetSubmissionListingType($lead->ListingType);
$appt = $portal->GetOpenAppointment($lead->SubmissionListingID, $currentUser->UserID);

$categoryValues = $portal->GetLookupValues('Category', $currentUser->GroupID);
$segmentValues = $portal->GetLookupValues('Segment', $currentUser->GroupID);
$campaignValues = $portal->GetLookupValues('Campaign', $currentUser->GroupID);

$oppts = $portal->GetOpportunities($lead->SubmissionListingID);

$contactHistory = $portal->GetContactHistory($lead->SubmissionListingID);
$lastCH = $contactHistory[sizeof($contactHistory) - 1];

//Timezone Fixes
$originaltimezone = date('e');

$dadded = new DateTime($lead->DateAdded, new DateTimeZone($originaltimezone));  //get date / time base on string saved and server's timezone
$dtouched = new DateTime($lastCH->EventDate, new DateTimeZone($originaltimezone));  //get date / time base on string saved and server's timezone
if (!empty($currentUser->UserTimezoneOffset)) {
    $dadded->setTimezone(new DateTimeZone($currentUser->UserTimezoneOffset)); //update date / time with the user's preferred timezone (for UserTimezoneOffset)
    $dtouched->setTimezone(new DateTimeZone($currentUser->UserTimezoneOffset)); //update date / time with the user's preferred timezone (for UserTimezoneOffset)
}
$lead->DateAdded = $dadded->format("m/d/Y g:i:s a");
$lastCH->EventDate = $dtouched->format("m/d/Y g:i:s a");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title>
        <?= $portal->CurrentCompany->CompanyName ?> Leads on Demand :: Edit Lead
    </title>
    <meta http-equiv="content-type" content="text/html;charset=utf-8"/>
    <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico"/>
    <script src="js/func.js"></script>
    <script src="js/contact_status.js"></script>

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>

    <script language="JavaScript" type="text/JavaScript">
        function updateContactEvent(id, actionName) {
            document.getElementById("LastActionSpan").innerHTML = actionName;
        }

        var statusDetails = new Array();
        <?php
        foreach ($shortStatusDetails as $shortStat => $details) {
            ?>
        statusDetails["<?= $shortStat ?>"] = new Array();
        <?php
        foreach ($details as $det) {
            ?>
        statusDetails["<?= $shortStat ?>"][<?= intval($contactStatusesTypes[$det]->ContactStatusTypeID) ?>] = "<?= $contactStatusesTypes[$det]->DetailName ?>";
        <?php
    }
}
?>
        function updateContactStatus(cstid) {
            if (isNaN(cstid)) {
                showDetailedContactStatus(cstid);
                return;
            }
            document.getElementById("ContactStatusTypeID").value = cstid;
        }
        function showDetailedContactStatus(cstid) {
            var statusSelect = document.getElementById("detailStatusID");
            document.getElementById("shortStatus").innerText = cstid;
            var i = 0;
            statusSelect.options.length = i;
            for (var id in statusDetails[cstid]) {
                statusSelect.options.length++;
                statusSelect[i].value = id;
                statusSelect[i].text = statusDetails[cstid][id];
                i++;
            }
            document.getElementById("detailDiv").style.display = "";
        }
        function submitDetailedContactStatus() {
            updateContactStatus(document.getElementById("detailStatusID").value);
            document.getElementById("detailDiv").style.display = "none";
        }
    </script>
    <?php include("components/bootstrap.php") ?>

    <link type="text/css" href="js/chosen/chosen.css" rel="stylesheet" media="all"/>
</head>
<body bgcolor="#FFFFFF">
<?php
if ($_GET['popup'] == 'true') {

} else {
    include("components/header.php");
}
?>
<div id="body">
    <?php
    if ($_GET['popup'] == 'true') {
        include("components/popupbar.php");
    } else {
        $CURRENT_PAGE = "Leads";
        include("components/navbar.php");
    }
    ?>
    <form method="post"
          action="<?= $_SERVER['PHP_SELF'] ?>?id=<?= $lead->SubmissionListingID . (isset($_GET['index']) ? "&index=" . $_GET['index'] : ""); ?>">
        <?php if (isset($_GET['message'])): ?>
            <div class="container">
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?= $_GET['message']; ?>
                </div>
            </div>
        <?php endif; ?>
        <div class="row">
            <div id="EditLeadDiv" class="col-md-9">


                <div class="row">

                    <div class="row">

                        <div class="panel panel-default">
                            <div class="panel-body body-as-header">
                                <span class="pull-left">
                            <h4>Lead ID: <?= $lead->SubmissionListingID; ?></h4>
                                </span>

                                <span class="pull-right">
                                    <strong>Creation
                                        Date:</strong> <?= strtotime($lead->DateAdded) ? date("m/d/Y g:i a", strtotime($lead->DateAdded)) : "" ?>
                                </span>
                            </div>
                        </div>

                    </div>

                    <div class="itemSection row">
                        <div id="FirstNameDiv" class="form-group col-md-6">
                            <label for="FirstName">Lead First Name:</label><br/>
                            <input type="text" class="form-control input-sm" id="FirstName"
                                   name="FirstName"
                                   value="<?= $lead->FirstName; ?>"/>
                            <span class="visible-print"><?= $lead->FirstName ?></span>
                        </div>
                        <div id="LastNameDiv" class="form-group col-md-6">
                            <label for="LastName">Last Name:</label><br/>
                            <input type="text" class="form-control input-sm" id="LastName"
                                   name="LastName"
                                   value="<?= $lead->LastName; ?>"/>
                            <span class="visible-print"><?= $lead->LastName ?></span>
                        </div>
                    </div>
                    <div class="itemSection row">
                        <div id="CompanyDiv" class="form-group col-md-6">
                            <label for="Company">Company:</label><br/>
                            <input type="text" class="form-control input-sm" id="Company" name="Company"
                                   value="<?= $lead->Company ?>"/>
                            <span class="visible-print"><?= $lead->Company ?></span>
                        </div>
                        <div id="PosDiv" class="form-group col-md-6">
                            <label for="Title">Position/Title:</label>
                            <input type="text" class="form-control input-sm" id="Title" name="Title"
                                   value="<?= $lead->Title ?>"/>
                            <span class="visible-print"><?= $lead->Title ?></span>
                        </div>
                    </div>

                    <div class="itemSection row">
                        <div id="ListingTypeDiv" class="form-group col-md-4">
                            <label for="ListingType">Lead Source:</label><br/>
                            <?php
                            if ($slt->UserManageable == "1") {
                            $slts = $portal->GetSubmissionListingTypes(0, 'SortOrder, SubmissionListingTypeID', 'all', 'UserManageable = 1 && submissionlistingtype.SubmissionListingSourceID = 9999');
                            ?>
                            <select class="form-control input-sm" name="ListingType" id="ListingType">
                                <?php
                                foreach ($slts as $slt) {
                                    ?>
                                    <option value="<?= $slt->SubmissionListingTypeID ?>"
                                        <?= $slt->SubmissionListingTypeID == $lead->ListingType ? "SELECTED" : "" ?>>
                                        <?= $slt->ListingType ?>
                                    </option>
                                <?php
                                }
                                ?>
                            </select>
                            <span class="visible-print"><?= $lead->ListingTypeName ?></span>
                        </div>

                        <div id="OtherListingTypeDiv" class="form-group col-md-4">
                            <label for="OtherListingType">Campaign Code:</label><br/>
                            <input type="text" class="form-control input-sm" id="OtherListingType"
                                   name="OtherListingType" value="<?= $lead->OtherListingType; ?>"/>
                            <span class="visible-print"><?= $lead->OtherListingType ?></span>
                            <?php
                            } else {
                                echo $lead->ListingTypeName;
                            }
                            ?>
                        </div>

                        <div id="NewUserIDDiv" class="form-group col-md-4">
                            <label for="NewUserID">Assigned To:</label><br/>
                            <?php
                            if ($isMan) {
                                ?>
                                <select class="form-control input-sm" name="NewUserID"
                                        id="NewUserID">
                                    <?php
                                    foreach ($users as $u) {
                                        if ($isCorpMan || ($isRegMan && $u->RegionID == $currentUser->RegionID) || ($isDivMan && $u->DivisionID == $currentUser->DivisionID)) {
                                            ?>
                                            <option
                                                value="<?= $u->UserID ?>" <?= $u->UserID == $lead->UserID ? "SELECTED" : "" ?>>
                                                <?= $u->FirstName . " " . $u->LastName ?>
                                            </option>
                                        <?php
                                        }
                                    }
                                    ?>
                                </select>
                                <span class="visible-print"><?= $lead->UserFullname ?></span>
                            <?php
                            } elseif ($lead->ContactStatusTypeID != 1) {
                                echo $lead->UserFullname;
                            }
                            ?>
                        </div>

                    </div>


                </div>

                <div class="row">

                    <div class="tab-well well" role="tabpanel">

                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#info" aria-controls="info" role="tab"
                                                                      data-toggle="tab"><h5>Info</h5></a></li>
                            <li role="presentation"><a href="#details" aria-controls="details" role="tab"
                                                       data-toggle="tab">
                                    <h5>Details</h5></a></li>
                            <li role="presentation"><a href="#notes" aria-controls="notes" role="tab" data-toggle="tab">
                                    <h5>
                                        Notes</h5></a></li>
                            <li role="presentation"><a href="#messages" aria-controls="messages" role="tab"
                                                       data-toggle="tab"><h5>
                                        SMS</h5></a></li>
                            <li role="presentation"><a href="#opportunities" aria-controls="opportunities" role="tab"
                                                       data-toggle="tab"><h5>
                                        Opportunities</h5></a></li>
                            <li role="presentation"><a href="#history" aria-controls="history" role="tab"
                                                       data-toggle="tab">
                                    <h5>History</h5></a></li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="info">

                                <div class="subSectionHeader">
                                    <h4>Lead Info</h4>
                                </div>

                                <div id="CustomerInfoDiv" class="row">
                                    <div id="MainSection" class="col-md-12">
                                        <div class="itemSectionDouble row">
                                            <div id="Address1Div" class="form-group col-md-6">
                                                <label for="Address1">Street Address:</label>
                                                <input type="text" class="form-control input-sm" id="Address1"
                                                       name="Address1"
                                                       value="<?= $lead->Address1; ?>"/>
                                            </div>
                                            <div id="CityDiv" class="form-group col-md-6">
                                                <label for="City">City:</label><br/>
                                                <input type="text" class="form-control input-sm" id="City" name="City"
                                                       value="<?= $lead->City; ?>"/>
                                                <span class="visible-print"><?= $lead->City ?></span>
                                            </div>
                                        </div>
                                        <div class="itemSection row">
                                            <div id="Address2Div" class="form-group col-md-6">
                                                <label for="Address2">Address 2:</label>
                                                <input type="text" class="form-control input-sm" id="Address2"
                                                       name="Address2"
                                                       value="<?= $lead->Address2; ?>"/>
                                                <span class="visible-print"><?= $lead->Address2 ?></span>
                                            </div>

                                            <div id="StateDiv" class="form-group col-md-3">
                                                <label for="State">ST:</label><br/>
                                                <input type="text" class="form-control input-sm" id="State" name="State"
                                                       value="<?= $lead->State; ?>"/>
                                                <span class="visible-print"><?= $lead->State ?></span>
                                            </div>
                                            <div id="ZipDiv" class="form-group col-md-3">
                                                <label for="Zip">Zip:</label><br/>
                                                <input type="text" class="form-control input-sm" id="Zip" name="Zip"
                                                       value="<?= $lead->Zip; ?>"/>
                                                <span class="visible-print"><?= $lead->Zip ?></span>
                                            </div>
                                        </div>
                                        <div class="itemSection row">
                                            <div id="PhoneDiv" class="form-group col-md-4">
                                                <label for="Phone">
                                                    Work Phone:
                                                    (<a href="#"
                                                        onclick="wopen('trigger_call.php?id=<?= $lead->SubmissionListingID; ?>&field=WorkPhone', 'add_action', 350, 285);
                                                            return false;">call</a>)
                                                </label><br/>
                                                <input type="text" class="form-control input-sm" id="WorkPhone"
                                                       name="WorkPhone"
                                                       value="<?= $lead->WorkPhone; ?>"/>
                                                <span class="visible-print"><?= $lead->WorkPhone ?></span>
                                            </div>
                                            <div id="Phone2Div" class="form-group col-md-4">
                                                <label for="Phone2">
                                                    Mobile Phone:
                                                    (<a href="#"
                                                        onclick="wopen('trigger_call.php?id=<?= $lead->SubmissionListingID; ?>&field=Phone2', 'add_action', 350, 285);
                                                            return false;">call</a> / <a href="#"
                                                                                         onclick="wopen('send_sms.php?id=<?= $lead->SubmissionListingID; ?>', 'send_sms', 400, 350);
                                                                                             return false;"
                                                                                         title="Send SMS">sms <i
                                                            class="glyphicon glyphicon-phone"></i></a> )
                                                    <?php if ($portal->Unsubscribe_Check($lead->SubmissionListingID)): ?>
                                                        <span class="glyphicon glyphicon glyphicon-ban-circle"
                                                              data-toggle="tooltip"
                                                              title="Unsubscribed" style="color: red;"></span>
                                                    <?php else: ?>
                                                        <span class="glyphicon glyphicon glyphicon-ok-circle"
                                                              data-toggle="tooltip"
                                                              title="Subscribed" style="color: green;"></span>
                                                    <?php endif; ?>
                                                </label><br/>
                                                <input type="text" class="form-control input-sm" id="Phone2"
                                                       name="Phone2"
                                                       value="<?= $lead->Phone2; ?>"/>
                                                <span class="visible-print"><?= $lead->Phone2 ?></span>
                                            </div>
                                            <div id="FaxDiv" class="form-group col-md-4">
                                                <label for="Fax">Fax:</label><br/>
                                                <input type="text" class="form-control input-sm" id="Fax" name="Fax"
                                                       value="<?= $lead->Fax; ?>"/>
                                                <span class="visible-print"><?= $lead->Fax ?></span>
                                            </div>
                                        </div>
                                        <div class="itemSection row">
                                            <div id="EmailDiv" class="form-group col-md-6">
                                                <label for="Email">Email Address:</label><br/>
                                                <input type="text" class="form-control input-sm" id="Email" name="Email"
                                                       value="<?= $lead->Email; ?>"/>
                                                <span class="visible-print"><?= $lead->Email ?></span>
                                            </div>
                                            <div id="WebsiteDiv" class="form-group col-md-6">
                                                <label for="Website">Website:</label><br/>
                                                <input type="text" class="form-control input-sm" id="Website"
                                                       name="CompanyWebsite"
                                                       value="<?= $lead->CompanyWebsite; ?>"/>
                                                <span class="visible-print"><?= $lead->CompanyWebsite ?></span>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                            <!-- End Info -->


                            <div role="tabpanel" class="tab-pane" id="details">

                                <div class="sectionHeader">
                                    <h4>Lead Details</h4>
                                </div>


                                <div id="DetailDiv" class="sectionDiv">

                                    <div class="itemSection row">
                                        <div id="CategoryDiv" class="form-group col-md-4">
                                            <label for="Category">Category:</label><br/>
                                            <select class="form-control input-sm" name="Category">
                                                <option value=""></option>
                                                <?php
                                                foreach ($categoryValues as $val) {
                                                    ?>
                                                    <option
                                                        value="<?= $val->LookupValue ?>" <?= $val->LookupValue == $lead->Category ? 'SELECTED' : '' ?>><?= $val->LookupLabel ?></option>
                                                <?php
                                                }
                                                ?>
                                            </select>
                                            <span class="visible-print"><?= $lead->Category ?></span>
                                        </div>
                                        <div id="SegmentDiv" class="form-group col-md-4">
                                            <label for="Segment">Segment:</label><br/>
                                            <select class="form-control input-sm" name="Segment">
                                                <option value=""></option>
                                                <?php
                                                foreach ($segmentValues as $val) {
                                                    ?>
                                                    <option
                                                        value="<?= $val->LookupValue ?>" <?= $val->LookupValue == $lead->Segment ? 'SELECTED' : '' ?>><?= $val->LookupLabel ?></option>
                                                <?php
                                                }
                                                ?>
                                            </select>
                                            <span class="visible-print"><?= $lead->Segment ?></span>
                                        </div>
                                        <div id="CampaignDiv" class="form-group col-md-4">
                                            <label for="Campaign">Outbound Campaign:</label><br/>
                                            <select class="form-control input-sm" name="Campaign">
                                                <option value=""></option>
                                                <?php
                                                foreach ($campaignValues as $val) {
                                                    ?>
                                                    <option
                                                        value="<?= $val->LookupValue ?>" <?= $val->LookupValue == $lead->Campaign ? 'SELECTED' : '' ?>><?= $val->LookupLabel ?></option>
                                                <?php
                                                }
                                                ?>
                                            </select>
                                            <span class="visible-print"><?= $lead->Campaign ?></span>
                                        </div>
                                    </div>

                                    <div id="CustomerInfoDiv" class="row">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div id="HomeAddress1Div" class="form-group col-md-6">
                                                    <label class="infolabel" for="HomeAddress">Home Address</label>
                                                    <input type="text" class="form-control input-sm" name="HomeAddress1"
                                                           value="<?= $lead->HomeAddress1 ?>" size="50"/>
                                                </div>
                                                <div id="HomeCityDiv" class="form-group col-md-6">
                                                    <label class="infolabel" for="HomeCity">City</label>
                                                    <input type="text" class="form-control input-sm" name="HomeCity"
                                                           value="<?= $lead->HomeCity ?>" size="25"/>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div id="HomeAddress2Div" class="form-group col-md-6">
                                                    <label class="infolabel" for="HomeAddress2">Address 2</label>
                                                    <input type="text" class="form-control input-sm" name="HomeAddress2"
                                                           value="<?= $lead->HomeAddress2 ?>" size="50"/>
                                                </div>
                                                <div id="HomeStateDiv" class="form-group col-md-3">
                                                    <label class="infolabel" for="HomeState">State</label>
                                                    <input type="text" class="form-control input-sm" name="HomeState"
                                                           value="<?= $lead->HomeState ?>" size="2"/>
                                                </div>
                                                <div id="HomeZipDiv" class="form-group col-md-3">
                                                    <label class="infolabel" for="HomeZip">Zip</label>
                                                    <input type="text" class="form-control input-sm" name="HomeZip"
                                                           value="<?= $lead->HomeZip ?>" size="10"/>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div id="PhoneDiv" class="form-group col-md-6">
                                                    <label class="infolabel" for="Phone">Home Phone</label>
                                                    <input type="text" class="form-control input-sm" name="Phone"
                                                           value="<?= $lead->Phone ?>"/>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div id="AnniversaryDiv" class="form-group col-md-6">
                                                    <label class="infolabel" for="Anniversary">Anniversary</label>
                                                    <input type="text" class="datepicker form-control input-sm"
                                                           id="Anniversary" name="Anniversary"
                                                           value="<?= (strtotime($lead->Anniversary) ? date("m/d/Y", strtotime($lead->Anniversary)) : '') ?>"/>
                                                </div>
                                                <div id="BirthdateDiv" class="form-group col-md-6">
                                                    <label for="Birthdate">Birthday</label>
                                                    <input type="text" class="datepicker form-control input-sm"
                                                           id="Birthdate" name="Birthdate"
                                                           value="<?= (strtotime($lead->Birthdate) ? date("Y", strtotime($lead->Birthdate)) : ""); ?>"/>
                                                <span
                                                    class="visible-print"><?= (strtotime($lead->Birthdate) ? date("Y", strtotime($lead->Birthdate)) : ""); ?></span>
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!-- End Details -->


                            <div role="tabpanel" class="tab-pane" id="notes">

                                <h4>Notes</h4>

                                <textarea id="Comments" name="Comments" rows="5"
                                          class="hidden-print form-control input-sm"><?= $lead->Comments ?></textarea>
                                <span class="visible-print"><?= $lead->Comments; ?></span>


                            </div>
                            <!-- End Notes -->


                            <div role="tabpanel" class="tab-pane" id="messages">

                                <h4>Messages
                                    <small>
                                        <a href="#"
                                           onclick="wopen('send_sms.php?id=<?= $lead->SubmissionListingID; ?>', 'send_sms', 400, 350);
                                               return false;" title="Send SMS">sms <i
                                                class="glyphicon glyphicon-phone"></i></a>
                                    </small>
                                </h4>

                                <div class="table-responsive">
                                    <table class="event_table table table-striped table-hover table-bordered">
                                        <thead style="font-weight: bold;">
                                        <tr>
                                            <td>Sender</td>
                                            <td>Text</td>
                                            <td>Time</td>
                                        </tr>

                                        </thead>
                                        <tbody>

                                        <?php foreach ($messages as $m): ?>
                                            <tr>
                                                <?php if ($m->Type != ""): ?>
                                                    <?php
                                                    if ($m->Type == "Response") {
                                                        $label = 'warning';
                                                    } else if ($m->Type == "Manual Send") {
                                                        $label = 'info';
                                                    } else {
                                                        $label = 'default';
                                                    }
                                                    ?>
                                                    <td align="center"><span
                                                            class="label label-<?= $label ?>"><?= ($m->Type == "Response") ? $m->LeadName : $m->UserName ?></span>
                                                    </td>
                                                <?php else: ?>
                                                    <td align="center"><span
                                                            class="label label-default"><?= (str_replace('+1', '', str_replace('-', '', $m->From)) == str_replace('+1', '', str_replace('-', '', $lead->Phone2))) ? $lead->FirstName . ' ' . $lead->LastName : $currentUser->FirstName . ' ' . $currentUser->LastName ?></span>
                                                    </td>
                                                <?php endif; ?>
                                                <td align="left" style="min-width: 200px;"><?php echo $m->Body; ?></td>
                                                <td align="center">
                                                    <i><?php echo date('Y-m-d g:ia', strtotime($m->Time)); ?></i>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- End Messages -->

                            <div role="tabpanel" class="tab-pane" id="opportunities">

                                <div class="subSectionHeader">
                                    <h4>Opportunities</h4>
                                </div>
                                <div id="OpportunityDiv" class="sectionDiv table-responsive row">
                                    <table class="event_table table table-striped table-hover table-bordered">
                                        <tr class="event_table rowheader">
                                            <th class="event_table">
                                                Stage
                                            </th>
                                            <th class="event_table">
                                                Type
                                            </th>
                                            <th class="event_table">
                                                Value
                                            </th>
                                            <th class="event_table">
                                                Create Date
                                            </th>
                                            <th class="event_table">
                                                Est. Close Date
                                            </th>
                                            <th class="event_table">
                                                &nbsp;
                                            </th>
                                        </tr>
                                        <?php
                                        if (sizeof($oppts) > 0) {
                                            $i = 0;
                                            foreach ($oppts as $oppt) {
                                                $i++;
                                                ?>

                                                <?php
                                                $date = new DateTime($oppt->CreateDate, new DateTimeZone($originaltimezone));  //get date / time base on string saved and server's timezone
                                                if (!empty($currentUser->UserTimezoneOffset)) {
                                                    $date->setTimezone(new DateTimeZone($currentUser->UserTimezoneOffset)); //update date / time with the user's preferred timezone (for UserTimezoneOffset)
                                                }
                                                $oppt->CreateDate = $date->format("m/d/Y g:i:s a");

                                                $date = new DateTime($oppt->EstimatedCloseDate, new DateTimeZone($originaltimezone));  //get date / time base on string saved and server's timezone
                                                if (!empty($currentUser->UserTimezoneOffset)) {
                                                    $date->setTimezone(new DateTimeZone($currentUser->UserTimezoneOffset)); //update date / time with the user's preferred timezone (for UserTimezoneOffset)
                                                }
                                                $oppt->EstimatedCloseDate = $date->format("m/d/Y g:i:s a");
                                                ?>


                                                <tr class="event_table<?= $i % 2 == 0 ? ' roweven' : ' rowodd' ?>">
                                                    <td class="event_table">
                                                        <?= $oppt->OpportunityStageName ?>
                                                    </td>
                                                    <td class="event_table">
                                                        <?= $oppt->OpportunityTypeName ?>
                                                    </td>
                                                    <td class="event_table">
                                                        $ <?= number_format($oppt->Value); ?>
                                                    </td>
                                                    <td class="event_table">
                                                        <?= strtotime($oppt->CreateDate) ? date("m/d/Y", strtotime($oppt->CreateDate)) : "" ?>
                                                    </td>
                                                    <td class="event_table">
                                                        <?= strtotime($oppt->EstimatedCloseDate) ? date("m/d/Y", strtotime($oppt->EstimatedCloseDate)) : "" ?>
                                                    </td>
                                                    <td class="event_table">
                                                        <a href="#"
                                                           onclick="wopen('edit_opportunity.php?id=<?= $oppt->OpportunityID; ?>', 'edit_opportunity', 350, 600);
                                                               return false;">edit</a>
                                                    </td>
                                                </tr>
                                            <?php
                                            }
                                        } else {
                                            ?>
                                            <tr class="event_table rowodd">
                                                <td colspan="6" style="text-align: center;">
                                                    No Opportunities have been defined for this lead
                                                </td>
                                            </tr>
                                        <?php
                                        }
                                        ?>
                                    </table>
                                </div>


                            </div>
                            <!-- End Opportunity -->

                            <div role="tabpanel" class="tab-pane" id="history">

                                <div id="EventTypeDiv" class="table-responsive">
                                    <table class="event_table table table-striped table-hover table-bordered">
                                        <tr class="event_table rowheader">
                                            <th class="event_table">
                                                Event Type
                                            </th>
                                            <th class="event_table">
                                                Event Time
                                            </th>
                                            <th class="event_table">
                                                Notes
                                            </th>
                                            <th class="event_table">
                                                User
                                            </th>
                                        </tr>

                                        <?php
                                        $i = 0;
                                        foreach ($allEvents as $event) {
                                            $i++;
                                            ?>

                                            <?php
                                            $date = new DateTime($event->EventDate, new DateTimeZone($originaltimezone));  //get date / time base on string saved and server's timezone
                                            if (!empty($currentUser->UserTimezoneOffset)) {
                                                $date->setTimezone(new DateTimeZone($currentUser->UserTimezoneOffset)); //update date / time with the user's preferred timezone (for UserTimezoneOffset)
                                            }
                                            $event->EventDate = $date->format("m/d/Y g:i:s a");
                                            ?>

                                            <tr class="event_table<?= $i % 2 == 0 ? ' roweven' : ' rowodd' ?>">
                                                <td class="event_table">
                                                    <?= $event->EventDescription ?>
                                                </td>
                                                <td class="event_table">
                                                    <?= strtotime($event->EventDate) ? date("m/d/Y g:i:s a", strtotime($event->EventDate)) : ""; ?>
                                                </td>
                                                <td class="event_table">
                                                    <?= $event->EventNotes ?>
                                                </td>
                                                <td class="event_table">
                                                    <?= $event->UserFullname ?>
                                                </td>
                                            </tr>
                                        <?php
                                        }
                                        ?>
                                    </table>
                                </div>

                            </div>
                        </div>

                    </div>


                </div>

                </br>

                <div class="row">

                    <center>
                        <div class="itemSection row">
                            <div class="buttonSection">
                                <input type="submit" class="btn btn-default" value="Export to FannieMae File"
                                       name="Submit"/>&nbsp;&nbsp;
                                <input type="submit" class="btn btn-info" value="Update" name="Submit"/>&nbsp;&nbsp;
                                <input type="submit" class="btn btn-info" value="Update & Close" name="Submit"/>&nbsp;&nbsp;
                                <?php
                                if ($_GET['popup'] == 'true') {

                                } else { ?>
                                    <input type="submit" class="btn btn-default" value="Cancel" name="Submit"/>

                                    <?php if ($isMan) {
                                        ?>
                                        <input type="submit" class="btn btn-danger" value="Delete" name="Submit"
                                               onclick="return confirm('Are you sure you want to delete this lead?')"/>&nbsp;&nbsp;
                                    <?php
                                    }
                                    ?>

                                <?php
                                }
                                ?>
                            </div>
                        </div>
                    </center>

                </div>

            </div>
            <!-- tabs div -->

            <div class="col-md-3">
                <div id="actions">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Actions</h3>
                        </div>
                        <div class="panel-body">

                            <div class="row">
                                <div id="ContactStatusTypeIDDiv" class="form-group col-md-12">
                                    <label for="ContactStatusTypeID">Status:</label><br/>
                                <span style="text-align: left;">
                                					<select name="ContactStatusTypeID" id="ContactStatusTypeID"
                                                            class="form-submenu input-sm contactStatus">
                                                        <?php
                                                        foreach ($contactStatusesTypes as $statusName => $status) {
                                                            ?>
                                                            <option
                                                                value="<?= $status->ContactStatusTypeID ?>" <?= $status->ContactStatusTypeID == $lead->ContactStatusTypeID ? "SELECTED" : "" ?>>
                                                                <?= $statusName ?>
                                                            </option>
                                                        <?php
                                                        }
                                                        ?>
                                                    </select>
                                </span>
                                    <span class="visible-print"><?= $lead->ContactStatus ?></span>
                                </div>
                            </div>

                            <div class="row">
                                <div id="LastContactEventTypeIDDiv" class="form-group col-md-12">
                                    <label for="LastContactEventTypeID">Last Action:</label>

                                    <div class="input-group" id="LastActionSpan">
                                        <span class="form-control">
                                            <?= $cetLookup[$lead->LastContactEvent->ContactEventTypeID] ? $cetLookup[$lead->LastContactEvent->ContactEventTypeID] : "No action taken." ?>
                                            </span>
                                        <span class="input-group-btn">
                                        <a href="#" class="btn btn-primary"
                                           onclick="wopen('add_action.php?id=<?= $lead->SubmissionListingID; ?>', 'add_action', 350, 500);
                                               return false;" class="hidden-print">New Action</a>
                                            </span>
                                    </div>
                                </div>

                                <div class="row">
                                    <div id="LastActionDiv" class="form-group col-md-12">
                                        <label for="LastActionDiv">Last Action Date:</label>

                                        <div class="input-group">
                                            <?= strtotime($lastCH->EventDate) ? date("m/d/Y g:i a", strtotime($lastCH->EventDate)) : "" ?>
                                        </div>
                                    </div>


                                </div>

                                <br/>

                                <div class="itemSection row">
                                    <div class="col-md-2">
                                        <a href="#" class="btn btn-primary"
                                           onclick="wopen('add_appointment.php?id=<?= $lead->SubmissionListingID; ?>', 'add_appointment', 350, 435);
                                               return false;" data-toggle="tooltip" data-placement="top"
                                           title="<?= $appt ? "Edit" : "Add" ?> Reminder"><span
                                                class="glyphicon glyphicon-time" aria-hidden="true"></span></a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="#" class="btn btn-primary"
                                           onclick="wopen('edit_opportunity.php?id=0&contactid=<?= $lead->SubmissionListingID; ?>', 'edit_opportunity', 350, 600);
                                               return false;" data-toggle="tooltip" data-placement="top"
                                           title="Create Opportunity"><span class="glyphicon glyphicon-usd"
                                                                            aria-hidden="true"></span></a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="#" class="btn btn-primary"
                                           onclick="wopen('email_file.php?slid=<?= $lead->SubmissionListingID; ?>', 'choose_content', 900, 600);
                                               return false;" data-toggle="tooltip" data-placement="top"
                                           title="Email ContentOnDemand"><span
                                                class="glyphicon glyphicon-envelope"
                                                aria-hidden="true"></span></a>
                                    </div>
                                </div>

                                <br/><br/>

                                <div class="buttonSection row">
                                    <?php
                                    if (isset($_GET['index']) && isset($lead_step_ids[intval($_GET['index']) - 1])) {
                                        ?>
                                        <a href="edit_lead.php?id=<?= $lead_step_ids[intval($_GET['index']) - 1] ?>&index=<?= intval($_GET['index']) - 1 ?>">
                                            &lt;- Prev</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <?php
                                    }
                                    ?>
                                    <div class="form-group col-md-6">
                                        <input type="submit" class="btn btn-info btn-block" value="Update"
                                               name="Submit"/>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <input type="submit" class="btn btn-info btn-block" value="Update & Close"
                                               name="Submit"/>
                                    </div>
                                    <?php
                                    if ($_GET['popup'] == 'true') {

                                    } else { ?>
                                        <div class="form-group col-md-6">
                                            <input type="submit" class="btn btn-default btn-block" value="Cancel"
                                                   name="Submit"/>
                                        </div>

                                        <?php if ($isMan) {
                                            ?>
                                            <div class="form-group col-md-6">
                                                <input type="submit" class="btn btn-danger btn-block" value="Delete"
                                                       name="Submit"
                                                       onclick="return confirm('Are you sure you want to delete this lead?')"/>
                                            </div>
                                        <?php
                                        }
                                        ?>

                                    <?php
                                    }
                                    if (isset($_GET['index']) && isset($lead_step_ids[intval($_GET['index']) + 1])) {
                                        ?>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a
                                            href="edit_lead.php?id=<?= $lead_step_ids[intval($_GET['index']) + 1] ?>&index=<?= intval($_GET['index']) + 1 ?>">Next
                                            -&gt;</a>
                                    <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end buttons -->

                </div>
                <!-- actions div -->

            </div>
            <!-- row -->
    </form>
    <form method="post" action="<?= $REAL_TAG_URL ?>" target="_blank" id="RealTagForm">
        <input type="hidden" name="name" value="<?= $lead->FirstName . " " . $lead->LastName ?>"/>
        <input type="hidden" name="address" value="<?= $lead->Address1 ?>"/>
        <input type="hidden" name="zip" value="<?= substr($lead->Zip, 0, 5) ?>"/>
    </form>
    <form method="post" action="<?= $LOAN_COMP_URL ?>" target="_blank" id="LoanCompForm">
        <input type="hidden" name="custName" value="<?= $lead->FirstName . " " . $lead->LastName ?>"/>
        <input type="hidden" name="custAddr" value="<?= $lead->Address1 ?>"/>
        <input type="hidden" name="custCSZ" value="<?= "{$lead->City}, {$lead->State} {$lead->Zip}" ?>"/>
        <input type="hidden" name="custEmail" value="<?= $lead->Email ?>"/>


        <input type="hidden" name="loName" value="<?= $currentUser->FirstName . " " . $currentUser->LastName ?>"/>
        <input type="hidden" name="loTollFreePhone" value="<?= $currentUser->ContactPhone ?>"/>
        <input type="hidden" name="loDirectPhone" value="<?= $currentUser->ContactCell ?>"/>
        <input type="hidden" name="loFax" value="<?= $currentUser->ContactFax ?>"/>
        <input type="hidden" name="loEmail" value="<?= $currentUser->ContactEmail ?>"/>

        <input type="hidden" name="fMortBal" value="<?= $lead->Mortgage1Balance ?>"/>
        <input type="hidden" name="fPmt" value="<?= $lead->Mortgage1Payment ?>"/>
        <input type="hidden" name="sMortBal" value="<?= $lead->Mortgage2Balance ?>"/>
        <input type="hidden" name="sPmt" value="<?= $lead->Mortgage2Payment ?>"/>
        <input type="hidden" name="pTaxBal" value="<?= $lead->PropertyTaxes ?>"/>
        <input type="hidden" name="pTaxPmt" value="<?= number_format($lead->PropertyTaxes / 12, 2); ?>"/>
        <input type="hidden" name="insBal" value="<?= $lead->AnnualInsurance; ?>"/>
        <input type="hidden" name="insPmt" value="<?= number_format($lead->AnnualInsurance / 12, 2); ?>"/>
        <input type="hidden" name="oLiabBal"
               value="<?= $lead->OtherDebt1Balance + $lead->OtherDebt2Balance + $lead->OtherDebt3Balance; ?>"/>
        <input type="hidden" name="oLiabPmt"
               value="<?= $lead->OtherDebt1Payment + $lead->OtherDebt2Payment + $lead->OtherDebt3Payment; ?>"/>

        <input type="hidden" name="cHomeVal" value="<?= $lead->MarketValue; ?>"/>

        <input type="hidden" name="transferIn" value="1"/>
    </form>
    <form method="post" action="<?= $PURCH_PROP_URL ?>" target="_blank" id="PurchPropForm">
        <input type="hidden" name="custName" value="<?= $lead->FirstName . " " . $lead->LastName ?>"/>
        <input type="hidden" name="custAddr" value="<?= $lead->Address1 ?>"/>
        <input type="hidden" name="custCSZ" value="<?= "{$lead->City}, {$lead->State} {$lead->Zip}" ?>"/>
        <input type="hidden" name="custEmail" value="<?= $lead->Email ?>"/>


        <input type="hidden" name="loName" value="<?= $currentUser->FirstName . " " . $currentUser->LastName ?>"/>
        <input type="hidden" name="loTollFreePhone" value="<?= $currentUser->ContactPhone ?>"/>
        <input type="hidden" name="loDirectPhone" value="<?= $currentUser->ContactCell ?>"/>
        <input type="hidden" name="loFax" value="<?= $currentUser->ContactFax ?>"/>
        <input type="hidden" name="loEmail" value="<?= $currentUser->ContactEmail ?>"/>

        <input type="hidden" name="fMortBal" value="<?= $lead->Mortgage1Balance ?>"/>
        <input type="hidden" name="fPmt" value="<?= $lead->Mortgage1Payment ?>"/>
        <input type="hidden" name="sMortBal" value="<?= $lead->Mortgage2Balance ?>"/>
        <input type="hidden" name="sPmt" value="<?= $lead->Mortgage2Payment ?>"/>
        <input type="hidden" name="pTaxBal" value="<?= $lead->PropertyTaxes ?>"/>
        <input type="hidden" name="pTaxPmt" value="<?= number_format($lead->PropertyTaxes / 12, 2); ?>"/>
        <input type="hidden" name="insBal" value="<?= $lead->AnnualInsurance; ?>"/>
        <input type="hidden" name="insPmt" value="<?= number_format($lead->AnnualInsurance / 12, 2); ?>"/>
        <input type="hidden" name="oLiabBal"
               value="<?= $lead->OtherDebt1Balance + $lead->OtherDebt2Balance + $lead->OtherDebt3Balance; ?>"/>
        <input type="hidden" name="oLiabPmt"
               value="<?= $lead->OtherDebt1Payment + $lead->OtherDebt2Payment + $lead->OtherDebt3Payment; ?>"/>

        <input type="hidden" name="cHomeVal" value="<?= $lead->MarketValue; ?>"/>

        <input type="hidden" name="transferIn" value="1"/>
    </form>
    <div style="display: none;" class="detail_div" id="detailDiv">
        <div style="text-align: center; background: #CCC; padding: 3px 0px 3px 0px; font-weight: bold;">
            Please Choose a detailed description for<br/>
            <span id="shortStatus"></span>
        </div>
        <div style="width: 394px; text-align: center; padding: 25px 0px 15px 0px;">
            <select class="form-control input-sm" name="detailStatusID" id="detailStatusID">
            </select>
        </div>
        <div style="width: 394px; text-align: center; background: #FFF; padding-bottom: 15px;">
            <a href="#" onclick="submitDetailedContactStatus();">OK</a>
        </div>
    </div>

    <?php include("components/footer.php") ?>

</body>
</html>