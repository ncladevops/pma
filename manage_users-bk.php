<?php
	require("../printable/include/mysql.inc.php");
	require("../printable/include/optimize.printable.inc.php");
	require("globals.php");
	
	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	
	$portal = new OptimizePortal($COMPANY_ID, $db);
	
	$currentUser = $portal->UserAccess($_SESSION['currentuserid']);
	
	if(!$portal->CheckPriv($currentUser->UserID, 'subadmin'))
	{
		header( "Location: " . $portal->CurrentCompany->Website . "home.php?message=" . urlencode( "Accessed Denied." ) );
		die();
	}
	
	$sortCols = array( 'ID' => 'u.UserID',  'First Name' => 'u.FirstName, u.LastName', 'Last Name' => 'u.LastName, u.FirstName', 
  					'Username/Email' => 'u.Username', 'Supervisor' => 'su.Username', 'Edit/View' => 'nosort'
  					);
  	$searchFields = array( 'ID' => 'u.UserID', 'LastName' => 'u.LastName',
  							'Username' => 'u.Username', 'Supervisor' => 'su.Username' );
  	
  	if(array_search($_GET['sortby'], $sortCols) === false)
	{
		$_GET['sortby'] = "u.LastName, u.FirstName";
	}
	
	if(!isset($searchFields[$_GET['searchfield']]))
	{
		$whereString = "1";
	}
	else
	{
		$whereString = $searchFields[$_GET['searchfield']] . " LIKE '%" . mysql_real_escape_string(stripslashes($_GET['search'])) . "%'";
	}
	
	$users = $portal->GetCompanyUsers("$whereString AND u.GroupID = '$currentUser->GroupID'", $_GET['sortby']);	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
	<title>
		<?= $portal->CurrentCompany->CompanyName ?> :: Manage Users
	</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<link rel="stylesheet" href="include/foldertree/css/folder-tree-static.css" type="text/css"/>
	<link rel="stylesheet" href="include/foldertree/css/context-menu.css" type="text/css"/>
	<script type="text/javascript" src="include/foldertree/js/ajax.js"></script>
	<script type="text/javascript" src="include/foldertree/js/folder-tree-static.js"></script>
	<script type="text/javascript" src="include/foldertree/js/context-menu.js"></script>			
	<link type="text/css" media="all" rel="stylesheet" href="style.css" />
	<link type="text/css" media="all" rel="stylesheet" href="css/controlpanel.css" />
	<link type="text/css" media="all" rel="stylesheet" href="css/manage_users.css" />
	<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />
	<script  src="js/func.js"></script>	
<script language="JavaScript" type="text/JavaScript">
	SetExpandedNodes('usermanage_node');
</script>
</head>
<body bgcolor="#FFFFFF">
<div id="page">
<?php include("components/header.php") ?>
<div id="body">
	<?php 
		$CURRENT_PAGE = "Home";
		include("components/navbar.php"); 
	?>
	<div class="error"><?= $_GET['message']; ?></div>
	<div id="controlPanelContainer">
		<div class="sectionHeader">
			Control Panel - Edit Users
		</div>
		<div id="folderTreeContainer">
			<?php include("components/controlpanel_tree.php"); ?>
		</div>
		<div id="detailContainer">	
<?php
	$getTemp = $_GET;
	unset($getTemp['search']);
	unset($getTemp['searchfield']);
	$getString = build_get_query($getTemp);
?>	
		<form method="get" action="<?= $_SERVER['PHP_SELF'] . "?" . $getString ?>>">
			<div class="backNav" style="height: 30px;">
				<div style="float: right; text-align: right;">	
					<label for="search">Search:</label> 
					<input type="text" name="search" style="width: 55px;" value="<?= $_GET['search'] ?>" />
					in field 
					<select name="searchfield">
<?php
	foreach($searchFields as $k=>$v)
	{
?>
						<option value="<?= $k ?>" <?= $k == $_GET['searchfield'] ? "SELECTED" : "" ?>>
							<?= $k ?>
						</option>				
<?php
	}
?>
					</select>
					<input type="submit" name="Submit" value="Search" />
				</div>
			</div>
		</form>
		<div style="overflow: auto;" id="UserDiv">
		<table class="user_table" >
			<thead>
			<tr class="user_table rowheader" id="HeaderRow">
<?php
	foreach($sortCols as $k=>$v)
	{
		if($v != "nosort" && $_GET['sortby'] != $v)
		{
			$getTemp = $_GET;
			$getTemp['sortby'] = $v;
			$getString = build_get_query($getTemp);
?>
				<th class="user_table">
					<a href="<?= $_SERVER['PHP_SELF'] . "?" . $getString ?>">
						<?= $k ?>
					</a>
				</th>
<?php		
		}
		else
		{
?>
				<th class="user_table"><?= $k ?></th>
<?php
		}
	}
?>
			</tr>
			</thead>
			<tbody>
<?php
	if(is_array($users))
	{
		$i = 0;
		foreach($users as $u)
		{
			$i++;
?>
			<tr class="user_table<?= $i%2==0 ? ' roweven' : ' rowodd' ?>">
				<td class="user_table" align="right">
					<?= $u->UserID ?>
				</td>
				<td class="user_table">
					<?= $u->FirstName ?>
				</td>
				<td class="user_table">
					<?= $u->LastName ?>
				</td>
				<td class="user_table">
					<?= $u->Username ?>
				</td>
				<td class="user_table">
					<?= $u->SupervisorEmail ?>
				</td>
				<td class="user_table" align="right">
					<a href="edit_user.php?id=<?= $u->UserID ?>">
						edit
					</a>
				</td>
			</tr>
<?php
		}		
	}
?>
			</tbody>
		</table>
		</div>
		</div>
	</div>
</div>
</div>
</body>
</html>