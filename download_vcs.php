<?php
require("../printable/include/mysql.inc.php");
require("../printable/include/optimize.printable.inc.php");
require("globals.php");

$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);

$portal = new OptimizePortal($COMPANY_ID, $db);

$currentUser = $portal->GetUser($_SESSION['currentuserid']);

$userString = "all";

$appt = $portal->GetAppointment($_GET['id'], "all");

$contact = $portal->GetOptimizeContact($appt->ContactID, $currentUser->UserID);

if (!$contact) {
    die("You do not have permission to this appointment.");
}

$time = date('n/j/Y g:i a', strtotime($appt->AppointmentTime));
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <title>
<?= $portal->CurrentCompany->CompanyName ?> :: Appointment Created
        </title>
            <?php include("components/bootstrap.php") ?>
    </head>
    <body>
        <div id="FileDiv" class="well">
            <strong>You have created an appointment with <?= $contact->FirstName . " " . $contact->LastName ?> on <?= $time ?> </strong><br/>
            <div class="itemSection">
                <div id="DescriptionDiv">
                    <b><a href = "getvcal.php?caid=<?= $appt->ContactAppointmentID ?>">Click Here To Download This Appointment to Outlook</a></b><br/>
                </div>
            </div><br/>
            <center>
                <a href = "view_appointment.php?contactID=<?= $appt->ContactID ?>">View all Appointments with <?= $contact->FirstName . " " . $contact->LastName ?></a><br/>
                <a href = "view_appointments.php">View all Appointments</a>
            </center>
        </div>

<?php include("components/bootstrap-footer.php") ?>
    </body>
</html>