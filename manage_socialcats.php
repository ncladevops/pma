<?php
require("../printable/include/mysql.inc.php");
require("../printable/include/optimize.printable.inc.php");
require("globals.php");

$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);

$portal = new OptimizePortal($COMPANY_ID, $db);

$currentUser = $portal->UserAccess($_SESSION['currentuserid']);
$isSubAdmin = $portal->CheckPriv($currentUser->UserID, 'subadmin');

// Check login
if (!$isSubAdmin) {
    header("Location: " . $portal->CurrentCompany->Website . "login.php?message=" . urlencode("Not logged in or login error.  Please try again."));
    die();
}

if (!isset($_GET['section'])) {
    header("Location: controlpanel.php");
}

// Get list of categories
$cats = $portal->GetSocialCategories('all', $_GET['section'], true, $currentUser->GroupID == 0 ? 'all' : $currentUser->GroupID);

$categories = array();
$category = new SocialCategory();
foreach ($cats as $category) {
    $categories[$category->SocialCategoryID] = $category;
}

if ($_POST['Submit'] == 'Add Category') {
    $newCatID = $portal->AddSocialCategory('New Category', $_GET['section']);
    $cat = $portal->GetCategory($newCatID, false);
    $cat->GroupID = $currentUser->GroupID;

    $portal->UpdateCategory($cat);

    header("Location: edit_socialcat.php?catid=$newCatID&section=" . $_GET['section']);
    die();
}

$gs = $portal->GetCompanyGroups();

$groups = array();

$groups[0] = new Group();
$groups[0]->GroupID = 0;
$groups[0]->GroupName = "All";

foreach ($gs as $g) {
    $groups[$g->GroupID] = $g;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <title>
            <?= $portal->CurrentCompany->CompanyName ?> :: Manage Social Category
        </title>
        <meta http-equiv="content-type" content="text/html;charset=utf-8" />

        <?php include("components/bootstrap.php") ?>

    </head>
    <body bgcolor="#FFFFFF">
        <div id="page">
            <?php include("components/header.php") ?>
            <div id="body">
                <?php
                $CURRENT_PAGE = "Home";
                include("components/navbar.php");
                ?>
                <?php if (isset($_GET['message'])): ?>
                    <div class="container">
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <?= $_GET['message']; ?>
                        </div>
                    </div>
                <?php endif; ?> 

                <div id="controlPanelContainer" class="row">
                    <div id="folderTreeContainer" class="col-md-offset-2 col-md-2 panel panel-default">
                        <?php include("components/controlpanel_tree.php"); ?>
                    </div>

                    <div class="sectionHeader col-md-7">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Control Panel - Edit Social Categories</h3>
                            </div>
                            <div class="panel-body">
                                <div id="detailContainer">
                                    <div id="actionBar" class="row">
                                        <form method="post" action="<?= $_SERVER['PHP_SELF'] ?>?section=<?= $_GET['section'] ?>" id="addCategoryForm">
                                            <input type="hidden" name="Submit" value="Add Category" />
                                            <a class="actionButton btn btn-default btn-sm" href="#" onclick="document.getElementById('addCategoryForm').submit()">Add Category</a>
                                        </form>
                                    </div>
                                    <div class="table-responsive row">
                                        <table width="100%" class="message_table table table-striped table-hover table-bordered">
                                            <thead>
                                                <tr class="rowheader">
                                                    <td>
                                                        ID
                                                    </td>
                                                    <td>
                                                        Category Name
                                                    </td>
                                                    <td>
                                                        Group
                                                    </td>
                                                    <td>
                                                        Category Desc
                                                    </td>
                                                    <td>
                                                        Date Created
                                                    </td>
                                                    <td>
                                                        Edit
                                                    </td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $category = new SocialCategory();
                                                $i = 0;
                                                foreach ($categories as $category) {
                                                    if ($portal->CheckPriv($currentUser->UserID, 'admin') || $category->GroupID != 0) {
                                                        ?>
                                                        <tr class='<?= $i++ % 2 == 0 ? 'roweven' : 'rowodd' ?>'>
                                                            <td align="right">
                                                                <?= $category->SocialCategoryID ?>
                                                            </td>
                                                            <td align="left">
                                                                <?= $portal->GetSocialCategoryPathString($category->ParentCategoryID) ?>\<?= $category->CategoryName ?>
                                                            </td>
                                                            <td align="left">
                                                                <?= $groups[$category->GroupID]->GroupName ?>
                                                            </td>
                                                            <td>
                                                                <?= substr($category->CategoryDesc, 0, 100) . (strlen($category->CategoryDesc) > 100 ? "..." : "") ?>
                                                            </td>
                                                            <td>
                                                                <?= date('m/d/Y', strtotime($category->DateCreated)); ?>
                                                            </td>
                                                            <td>
                                                                <a class="btn btn-default btn-sm" href="edit_socialcat.php?catid=<?= $category->SocialCategoryID ?>&section=<?= $_GET['section'] ?>"><span class="glyphicon glyphicon-pencil"></span> Edit</a>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

            </div>

            <?php include("components/footer.php") ?>

    </body>
</html>