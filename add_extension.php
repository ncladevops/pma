<?php
require("../printable/include/mysql.inc.php");
require("../printable/include/optimize.printable.inc.php");
require("globals.php");

$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);

$portal = new OptimizePortal($COMPANY_ID, $db);

$currentUser = $portal->GetUser($_SESSION['currentuserid']);

$isSubAdmin = $portal->CheckPriv($currentUser->UserID, 'subadmin');

if (!$isSubAdmin) {
    header("Location: " . $portal->CurrentCompany->Website . "home.php?message=" . urlencode("Accessed Denied."));
    die();
}

if ($_POST['Submit'] == 'Cancel') {
    header('Location: manage_extensions.php?message=' . urlencode("Action Canceled. Workgroup not updated."));
    die();
} elseif ($_POST['Extension']) {
    if ($portal->AddExtension($_POST['ExtensionName'], $_POST['Extension'])) {
        header('Location: manage_extensions.php?message=' . urlencode("Action Successful. Workgroup has been updated."));
        die();
    }

    $_GET['message'] = "The Workgroup " . $_POST['Extension'] . " already exists!";
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <title>
            <?= $portal->CurrentCompany->CompanyName ?> :: Add New Workgroup
        </title>
        <meta http-equiv="content-type" content="text/html;charset=utf-8" />		
        <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />
        <?php include("components/bootstrap.php") ?>
    </head>
    <body bgcolor="#FFFFFF">
        <?php include("components/header.php") ?>
        <div id="body">
            <?php
            $CURRENT_PAGE = "Home";
            include("components/navbar.php");
            ?>
            <form method="post" action="<?= $_SERVER['PHP_SELF'] ?>">
                
                <?php if (isset($_GET['message'])): ?>
                    <div class="container">
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <?= $_GET['message']; ?>
                        </div>
                    </div>
                <?php endif; ?> 
                
                <div id="ExtensionInfoDiv" class="container well">
                    <div class="sectionHeader">
                        <h2>Add New Workgroup</h2>
                    </div>
                    <div class="sectionDiv">
                        <div class="itemSection row">
                            <div id="ExtensionNameDiv" class="form-group col-md-3">
                                <label for="ExtensionName">Workgroup Name:</label>
                                <input type="text" id="ExtensionName" class="form-control input-sm" name="ExtensionName" />
                            </div>
                        </div>
                        <div class="itemSection row">
                            <div id="ExtensionNameDiv" class="form-group col-md-3">
                                <label for="ExtensionName">Workgroup Extension:</label>
                                <input type="text" class="form-control input-sm" id="Extension" name="Extension"  />
                            </div>
                        </div>
                    </div>
                    <center>
                        <div class="itemSection">
                            <div class="buttonSection">
                                <input type="submit" class="btn btn-info btn-sm" value="Add Workgroup" name="Submit"/>&nbsp;&nbsp;<input type="submit" class="btn btn-default btn-sm" value="Cancel" name="Submit"/>
                            </div>
                        </div>
                    </center>
                </div>
            </form>
        </div>
        <?php include("components/footer.php") ?>
    </body>
</html>