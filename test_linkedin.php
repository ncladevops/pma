<?php
	require("../printable/include/mysql.inc.php");
	require("../printable/include/optimize.printable.inc.php");
	require("globals.php");
	require '../printable/include/component/linkedin/linkedin.php';
	
	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	
	$portal = new OptimizePortal($COMPANY_ID, $db);
	
	$currentUser = $portal->UserAccess($_SESSION['currentuserid']);
	$isSuper = $portal->CheckPriv($currentUser->UserID, 'supervisor');
			
	if(!$currentUser)
	{
		die("Not logged in or login error.");
	}
	
	$oauth_callback = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
	$linkedin = new LinkedIn($LINKEDIN_KEY, $LINKEDIN_SECRET, $oauth_callback );
	//$linkedin->debug = true;
		
	if (!isset($_SESSION['linkedin_oauth_token'])) {
		if(strlen($currentUser->LinkedinToken) <= 0) {
			$linkedin->getRequestToken();
			
			// store the token
			$_SESSION['linkedin_oauth_token'] = $linkedin->request_token->key;
			$_SESSION['linkedin_oauth_token_secret'] = $linkedin->request_token->secret;
			$_SESSION['linkedin_oauth_verify'] = true;
			
			// redirect to auth website
			header("Location: " . $linkedin->generateAuthorizeUrl());
			die();
		} else {
			// Authorize User
			list($t,$s) = explode(":",$currentUser->LinkedinToken);
			$linkedin->access_token = new OAuthConsumer($t, $s, 1);
			$response = new SimpleXMLElement($linkedin->getProfile("~:(id,first-name,last-name,headline,picture-url)"));
			
			// if authorized set session
			if($response->status != '401') {
				$_SESSION['linkedin_oauth_token'] = $t;
				$_SESSION['linkedin_oauth_token_secret'] = $s;
			}
			// else clear credentials
			else {
				$currentUser->LinkedinToken = "";
				$portal->UpdateUser($currentUser);
			}
			
			header('Location: ' . basename(__FILE__));
			die();
		}

	} elseif (isset($_GET['oauth_verifier']) && isset($_SESSION['linkedin_oauth_verify'])) {
		// verify the token
		$linkedin->request_token = new OAuthConsumer($_SESSION['linkedin_oauth_token'], $_SESSION['linkedin_oauth_token_secret'], 1);
        $linkedin->oauth_verifier = $_GET['oauth_verifier'];
		unset($_SESSION['linkedin_oauth_verify']);

		// get the access token
        $linkedin->getAccessToken($_GET['oauth_verifier']);
		
		// store the token (which is different from the request token!)
		$_SESSION['linkedin_oauth_token'] = $linkedin->access_token->key;
		$_SESSION['linkedin_oauth_token_secret'] = $linkedin->access_token->secret;
		
		// store the token with the user
		$currentUser->LinkedinToken = $linkedin->access_token->key . ":" . $linkedin->access_token->secret;
		$portal->UpdateUser($currentUser);

		// send to same URL, without oauth GET parameters
		header('Location: ' . basename(__FILE__));
		die();
	}

	// assign access token on each page load
	$linkedin->access_token = new OAuthConsumer($_SESSION['linkedin_oauth_token'], $_SESSION['linkedin_oauth_token_secret'], 1);
	
	if($_POST['submit'] == 'Post to LinkedIn') {
		$response = $linkedin->share($_POST['post_text']);
		
		echo '<xmp>';
		echo "Share Response\n";
		echo $response;
		echo '</xmp>';
	}
	
	$xml_response = $linkedin->getProfile("~:(id,first-name,last-name,headline,picture-url)");

    echo '<xmp>';
    echo 'My Profile Info';
    echo $xml_response;
    echo '</xmp>';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
	<title>
		OptifiNow :: Publish onDemand
	</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />		
	<link type="text/css" media="all" rel="stylesheet" href="style.css" />
	<script  src="js/func.js"></script>	

</head>
<body bgcolor="#FFFFFF" onload="">
<form action="<?= $_SERVER['PHP_SELF'] ?>" method="POST">
	<b>Post:</b><br/>
	<textarea name="post_text"></textarea>
	<input type="submit" value="Post to LinkedIn" name="submit"/>
</form>
</body>
</html>