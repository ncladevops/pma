<?php
require("../printable/include/mysql.inc.php");
require("../printable/include/printable.inc.php");
require("globals.php");

$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);

$portal = new PrintablePortal($COMPANY_ID, $db);

// Querry to get user info
$currentUser = $portal->GetUser($_SESSION['currentuserid']);
$currentGroup = $portal->GetGroup($currentUser->GroupID);

// Get Appointment
$currentAppointment = $portal->GetAppointment($_GET['caid'], 'all');
$contact = $portal->GetContact($currentAppointment->ContactID, $currentUser->UserID);

if(!$contact)
{
    die("Invalid Appointment");
}

$Filename = "ContactAppointment" . $_GET['caid'] . ".vcs";
//header("Content-Type: text/x-vCalendar");
//header("Content-Disposition: inline; filename=$Filename");

header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header('Content-type: text/x-vCalendar');
header("Content-Transfer-Encoding: binary");
header("Content-Disposition: inline; filename=\"{$Filename}\";" );
header("Content-Transfer-Encoding: binary");

$calendarDetail = "Company Name: $contact->Company\r"
    . "Contact: $contact->FirstName $contact->LastName\r"
    . "Phone: $contact->Phone\r"
    . "Address:\r"
    . "$contact->Address1\r"
    . "$contact->City, $contact->State $contact->Zip\r"
    . "Notes: \r $currentAppointment->Notes\r";

$DescDump = str_replace("\r", "=0D=0A", $calendarDetail);

$vCalStart = date('Ymd\THi00', strtotime($currentAppointment->AppointmentTime . intval($currentUser->UserTimezoneOffset) * -1 . " hours"));

// Set end to 1 hour later
$vCalEnd = date('Ymd\THi00', strtotime($currentAppointment->AppointmentTime . (intval($currentUser->UserTimezoneOffset) - 1) * -1 . " hours"));
?>
BEGIN:VCALENDAR
VERSION:1.0
PRODID:TWCBC OnDemand Web Calendar
BEGIN:VEVENT
SUMMARY:OptifiNow Appointment: <?= "$contact->Company $contact->FirstName $contact->LastName\n"; ?>
DESCRIPTION;ENCODING=QUOTED-PRINTABLE:<?= $DescDump . "\n"; ?>
DTSTART:<?= $vCalStart . "\n"; ?>
DTEND:<?= $vCalEnd . "\n"; ?>
END:VEVENT
END:VCALENDAR

