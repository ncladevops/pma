<?php
	require("../printable/include/mysql.inc.php");
	require("../printable/include/optimize.printable.inc.php");
	require("globals.php");
	include("components/class.myatomparser.php");
	require '../printable/include/component/twitter/codebird.php';

	$MAX_DASHBOARDS = 5;
		
	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	
	$portal = new OptimizePortal($COMPANY_ID, $db);
	
	$currentUser = $portal->UserAccess($_SESSION['currentuserid']);
	$isSuper = $portal->CheckPriv($currentUser->UserID, 'supervisor');
			
	if(!$currentUser)
	{
		header("Location: login.php?message=" .urlencode("Not logged in or login error."));
		die();
	}	

	$dashboard = $portal->GetDashboard($HOMEPAGE_DBID);
	$reports = $portal->GetDashBoardReports($dashboard->DashboardID);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
	<title>
		<?= $portal->CurrentCompany->CompanyName ?> :: Welcome
	</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />		
	
    <link rel="stylesheet" href="slider/nivo-slider.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="slider/default.css" type="text/css" media="screen" />
	<script  src="js/swfobject.js"></script>	
	
	<script  src="js/func.js"></script>	
	
	<script type='text/javascript' src='http://yui.yahooapis.com/2.9.0/build/utilities/utilities.js'></script>
	<link rel="stylesheet" href="../printable/include/js/jquery-ui/smoothness/jquery-ui.min.css" />
	<script type='text/javascript' src='../printable/include/js/jquery-1.10.2.min.js'></script>
	<script type='text/javascript' src='../printable/include/js/jquery-ui.js'></script>
  
  <?php include("reports/report_dashboard_includes.php") ?>

	<script type='text/javascript' src='../printable/include/js/jquery-1.10.2.min.js'></script>
	<script type='text/javascript' src='../printable/include/js/jquery-ui.js'></script>
	<link type="text/css" media="all" rel="stylesheet" href="style.css" />
	<link type="text/css" media="all" rel="stylesheet" href="css/home2.css?v=2" />
	<link type="text/css" media="all" rel="stylesheet" href="css/dashboard.css" />

<script type="text/javascript">
$( document ).ready(function() {
    $('#FilterDashboardStart').datepicker();
    $('#FilterDashboardEnd').datepicker();

$( document ).tooltip();

});

	var dashboard_transactionURL = "dashboard_trans.php";
	var appointmentTableRequest;
	var appointmentTableSimpleRequest;
			
	var report_loaderURL = "reports/load_report.php";
	var reportRequest = new Array();
	
	function LoadDashboards()
	{
			LoadAppointmentTableSimple();
            LoadCalendar();
            LoadDashboardReports();
	}

	function LoadCalendar()
	{
	document.getElementById("CalendarTable_Dashboard").innerHTML = "<iframe src='appt_calendar_sim.php' style='border: 0px; width: 295px; height: 252px;' scrolling='no'></iframe>";
	}
	
	function LoadAppointmentTableSimple()
	{
		document.getElementById("AppointmentTableSimple_Dashboard").innerHTML = "<img src='images/loading.gif' />"
				
		appointmentTableSimpleRequest=GetXmlHttpObject();
		if (appointmentTableSimpleRequest==null)
		{
			alert ("Your browser does not support AJAX!");
			return;
		} 
		var url=dashboard_transactionURL;
		url=url+"?dashboard=AppointmentTableSimp";
		url=url+"&sid="+Math.random();
		appointmentTableSimpleRequest.onreadystatechange=appointmentTableSimpleRequestStateChanged;
		appointmentTableSimpleRequest.open("GET",url,true);
		appointmentTableSimpleRequest.send(null);
	}

	function appointmentTableSimpleRequestStateChanged()
	{
		if (appointmentTableSimpleRequest.readyState==4)
		{
			document.getElementById("AppointmentTableSimple_Dashboard").innerHTML = appointmentTableSimpleRequest.responseText;
		}
	}

	     function show(id) {
		document.getElementById("spinner" + id).style.display = 'none';
	     }

            function LoadDashboardReports() {

            var startDate = document.getElementById("FilterDashboardStart").value;
            var endDate = document.getElementById("FilterDashboardEnd").value;
			var url;
			var u;
<?php
	for ($i = 0; $i < $MAX_DASHBOARDS; $i++) {
		if (isset($reports[$i])) {
			if ($reports[$i]->UserBased) {
				$userString = "&sv_RepName=" . urlencode($currentUser->FirstName . " " . $currentUser->LastName);
			} else {
				$userString = "";
			}
?>
			u = encodeURIComponent('<?= $reports[$i]->URL ?>' + "?cmd=search&so_DateAdded=BETWEEN&sv_DateAdded=" + encodeURIComponent(startDate) + "&sv2_DateAdded=" + encodeURIComponent(endDate) + '<?= $userString ?>' + "&export=print");
			reportRequest[<?= $i ?>]=GetXmlHttpObject();
            if (reportRequest[<?= $i ?>]==null)
            {
            alert ("Your browser does not support AJAX!");
            return;
            }
			
			url = report_loaderURL;
			url=url+"?url=" + u;
            url=url+"&sid="+Math.random();
            reportRequest[<?= $i ?>].onreadystatechange=reportRequestStateChanged;
            reportRequest[<?= $i ?>].open("GET",url,true);
            reportRequest[<?= $i ?>].send(null);
			
            document.getElementById("DashboardDiv" + <?= $i + 1 ?>).innerHTML = "<div class='Header' title='<?= $reports[$i]->ReportDescription ?>'><?= $reports[$i]->ReportName ?></div> <img id='spinner<?= $i + 1 ?>' class='spinner' src='images/loading.gif' /> <div id='reportContainer<?= $i + 1 ?>' style='display: none'></div>";
<?php
		}
	}
?>		
            }
			
			function reportRequestStateChanged()
            {
				for(var i = 0; i < <?= $MAX_DASHBOARDS ?>; i++) {
					if (reportRequest[i] && reportRequest[i].readyState==4)
					{
						var cont = document.getElementById("reportContainer" + (i + 1));
						cont.innerHTML = reportRequest[i].responseText;
						cont.style.display = "";
						document.getElementById("spinner" + (i + 1)).style.display = "none";
						
						var arr = cont.getElementsByTagName('script')
						for (var n = 0; n < arr.length; n++)
							eval(arr[n].innerHTML)//run script inside div
					}
				}
            }
			
</script>
</head>
<body bgcolor="#FFFFFF" onload="LoadDashboards();">
	<div id="page">
		<?php include("components/header.php") ?>
		<div id="body">
		<?php
			$CURRENT_PAGE = "Home";
			include("components/navbar.php");
		?>
		<div class="error"><?= $_GET['message']; ?></div>
		<div id="home_container">
			<div id="side_panel">
				<div id="quicklinks" class="Container">
					<div class="Header">
						Quick Links
						</div>
						<table width="100%">
							<tr>
								<td align="center" width="93"><?= $portal->GetCustomMessage('QuickLink1', false, $currentUser->GroupID)->Message; ?></td>
								<td align="center" width="93"><?= $portal->GetCustomMessage('QuickLink2', false, $currentUser->GroupID)->Message; ?></td>
								<td align="center" width="93"><?= $portal->GetCustomMessage('QuickLink3', false, $currentUser->GroupID)->Message; ?></td>
                            </tr>
							<tr>
								<td align="center" width="93"><?= $portal->GetCustomMessage('QuickLink4', false, $currentUser->GroupID)->Message; ?></td>
								<td align="center" width="93"><?= $portal->GetCustomMessage('QuickLink5', false, $currentUser->GroupID)->Message; ?></td>
								<td align="center" width="93"><?= $portal->GetCustomMessage('QuickLink6', false, $currentUser->GroupID)->Message; ?></td>
							</tr>
						</table>
					</div>
					<div class="Container">
						<div class="Header">
							<?= $portal->GetCustomMessage('MessageBoardHeader', false, $currentUser->GroupID)->Message; ?>
						</div>
						<div class="quicklinks_msg_content" style="overflow: auto;">
<?php
	$url = "http://$currentUser->Username:optimize@{$BB_LINK}feed.php?auth=http";
	$atom_parser = new myAtomParser($url);

	$output = $atom_parser->getOutput();	# returns string containing HTML
	echo $output;
?>
						</div>
					</div>
					<div class="Container">
						<div class="Header">
							<div id="apptHeader">
								<?= $portal->GetCustomMessage('BrandNewsHeader', false, $currentUser->GroupID)->Message; ?>
							</div>
						</div>
						<div class="quicklinks_msg_content" style="overflow: auto;" id="brandNewsDiv">
<?php
	Codebird::setConsumerKey($TWITTER_KEY, $TWITTER_SECRET);
	Codebird::setBearerToken($TWITTER_BEARER);
	
	$cb = Codebird::getInstance();
	
	echo $cb->display_searched_tweets('OptifiNow');
?>
                                   
						</div>
					</div>
					<select onchange="window.location='home.php';">
						<option>Legacy Version</option>
						<option SELECTED>New Version</option>
					</select>
				</div>
				<div id="main_panel">
					<div id="Dashboard_Container">
						<div id="DashboardDiv1" class="dashboardDiv Container">
							<img class="spinner" src="images/loading.gif" />
						</div>
						<div id="DashboardDiv2" class="dashboardDiv Container">
							<img class="spinner" src="images/loading.gif" />
						</div>
						<div id="DashboardDiv3" class="dashboardDiv Container">
							<img class="spinner" src="images/loading.gif" />
						</div>
						<div id="DashboardDiv4" class="smallDashboardDiv Container">
							<img class="spinner" src="images/loading.gif" />
						</div>
						<div id="DashboardDiv5" class="smallDashboardDiv Container">
							<img class="spinner" src="images/loading.gif" />
						</div>
						<div id="DashboardFilter" style="float: left;" class="Container">
							From: <input type="text" name="FilterDashboardStart" id="FilterDashboardStart" value="<?= date("m/d/Y", strtotime("-1 month")); ?>" /> 
							To: <input type="text" name="FilterDashboardEnd" id="FilterDashboardEnd" value="<?= date("m/d/Y"); ?>" />
							<input type="button" value="Go" onclick="LoadDashboardReports();"/>
						</div>
					</div>
					<div id="RightPanel">
						<div id="AppointmentTable_Container" class="Container">
							<div class="Header">
								<div id="apptHeader">
									My Appointments
								</div>
							</div>
							<div id="AppointmentTableSimple_Dashboard">
								<img class="alt-spinner" src="images/loading.gif" />
							</div>
						</div>
						<div id="CalendarTable_Container" class="Container">
							<div class="Header">
								<div "tomorrowCalendarDiv" class="calendarLinkDiv" onclick="wopen('appt_calendar.php?defaultView=agendaMonth', 'appt_calendar', 800, 600);">
									<img src="images/glyphicons_month.png" height="18" width="18" title="Month"/>
								</div>
								<div "weekCalendarDiv" class="calendarLinkDiv" onclick="wopen('appt_calendar.php?defaultView=agendaWeek', 'appt_calendar', 800, 600);">
									<img src="images/glyphicons_week.png" height="18" width="18" title="Week"/>
								</div>
								<div "todayCalendarDiv" class="calendarLinkDiv" onclick="wopen('appt_calendar.php?defaultView=agendaDay', 'appt_calendar', 800, 600);">
									<img src="images/glyphicons_day.png" height="18" width="18" title="Day"/>
								</div>
								<div id="apptHeader">
									My Calendar
								</div>
							</div>
							<div id="CalendarTable_Dashboard">
								<img class="alt-spinner" src="images/loading.gif" />
							</div>
						</div>
					</div>
				</div>	
			</div>
		</div>
	</div>
</body>
</html>