<?php
require("../printable/include/mysql.inc.php");
require("../printable/include/optimize.printable.inc.php");
require("globals.php");

$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);

$portal = new OptimizePortal($COMPANY_ID, $db);

// Querry to get user info
$currentUser = $portal->GetUser($_SESSION['currentuserid']);
$campaign = $portal->GetSubmissionListingType($_GET['listid']);

// Check login
if (!$currentUser) {
    header("Location: login.php?message=" . urlencode("Not logged in or login error.  Please try again."));
    die();
}

if (!$campaign || $campaign->UserManageable == 0) {
    die("Invalid Campaign ID");
}

$fieldnamearray = array('none' => 'Do Not Import', 'Company' => 'Company',
    'FirstName' => 'First Name', 'LastName' => 'Last Name',
    'Address1' => 'Address', 'Address2' => 'Address 2',
    'Address3' => 'Address 3', 'City' => 'City',
    'State' => 'State', "Zip" => "Zip",
    "Phone" => "Home Phone", "Fax" => "Fax",
    "Title" => "Title", "Prefix" => "Prefix",
    'WorkPhone' => 'Work Phone', 'CellPhone' => 'Mobile Phone',
    'CompanyWebsite' => 'CompanyWebsite', 'Email' => 'Email',
    'HomeAddress1' => 'Home Address', 'HomeAddress2' => 'Home Address 2',
    'HomeAddress3' => 'Home Address 3', 'HomeCity' => 'Home City',
    'HomeState' => 'Home State', "HomeZip" => "Home Zip",
    'Comments' => 'Comments', 'Category' => 'Category',
    'Segment' => 'Segment', 'Birthdate' => 'Birthdate',
    'Anniversary' => 'Anniversary', 'Campaign' => 'Campaign',
    'PublicContact' => 'Is Public (y/n)');
$fieldsrequired = array('LastName');

if ($_POST['Submit'] == 'Upload' && $_FILES['uploadedfile']['name'] != '') {
    // make sure this is a csv
    // get file extension
    $ext = substr(strrchr($_FILES['uploadedfile']['name'], "."), 1);

    if (strcasecmp($ext, 'csv') == 0) {
        // copy file to temp folder
        $tempFileName = $_FILES['uploadedfile']['tmp_name'];
        // create name for file
        $savedFileName = 'C:\Temp\\' . md5($evalSys->currentCompany->CompanyID . rand() . date("c") . rand()) . ".tmp";

        // move to temp	
        move_uploaded_file($tempFileName, $savedFileName);

        $_SESSION['uploadfilename'] = $savedFileName;
        $_SESSION['headerflag'] = ($_POST['headerflag'] == 1 ? "1" : "0");
    } else {
        $errorText = "File must be a csv file.";
    }
} elseif ($_POST['Submit'] == 'Cancel' || $_POST['Submit'] == '<- Back' || $_POST['Submit'] == 'Finish') {
    if (is_file($_SESSION['uploadfilename'])) {
        unlink($_SESSION['uploadfilename']);
    }
    $_SESSION['uploadfilename'] = '';
    $_SESSION['headerflag'] = '';
    $close_window = ($_POST['Submit'] == '<- Back' ? false : true);
} elseif ($_POST['Submit'] == 'Continue ->') {
    // Make sure fields are not double mapped
    $fieldmap = $_POST['fieldmap'];
    $field_error = 0;
    $usedFields = array();
    $errorFields = array();
    $reqFieldErrors = array();
    $fieldLookup = array();

    for ($i = 0; $i < count($fieldmap); $i++) {
        $fieldLookup[$fieldmap[$i]] = $i;
        if ($usedFields[$fieldmap[$i]] != 0 && $fieldmap[$i] != 'none') {
            $field_error = 1;
            $errorFields[$i] = 1;
            $errorText = "Only one File field can be matched to each System field.";
        } else {
            $usedFields[$fieldmap[$i]] = 1;
        }
    }

    // Make Sure required fields are mapped
    foreach ($fieldsrequired as $req) {
        if ($usedFields[$req] != 1) {
            $reqFieldErrors[] = $fieldnamearray[$req];
        }
    }
    if (sizeof($reqFieldErrors) > 0) {
        $field_error = 1;
        $errorText = "The following fields are required: " . implode(", ", $reqFieldErrors);
    }

    if ($field_error == 0) {
        // Check out number of new and existing users
        $newContacts = 0;
        $contactErrors = 0;
        // open file for reading
        $file = fopen($_SESSION['uploadfilename'], "r");

        // if headerflag burn first line
        if ($_SESSION['headerflag'] == 1) {
            $line = fgetcsv($file, 1000, ",", "\"");
        }

        while (!feof($file)) {
            $line = fgetcsv($file, 1000, ",", "\"");
            if (sizeof($line) > 1 || $line[0] != "") {
                // Check if req fields are present
                $e = 0;
                foreach ($fieldsrequired as $req) {
                    if (strlen($line[$fieldLookup[$req]]) <= 0) {
                        $e = 1;
                    }
                }
                if ($e == 1) {
                    $contactErrors++;
                } else {
                    $newContacts++;
                }
            }
        }
    }
} elseif ($_POST['Submit'] == 'Import Now') {
    $fieldLookup = $_POST['fieldLookup'];

    // open file for reading
    $file = fopen($_SESSION['uploadfilename'], "r");

    // if headerflag burn first line
    if ($_SESSION['headerflag'] == 1) {
        $line = fgetcsv($file, 1000, ",", "\"");
    }

    while (!feof($file)) {
        $line = fgetcsv($file, 1000, ",", "\"");

        // Check if req fields are present
        $e = 0;
        foreach ($fieldsrequired as $req) {
            if (strlen($line[$fieldLookup[$req]]) <= 0) {
                $e = 1;
            }
        }
        if ($e == 0) {

            $contact = new OptimizeContact();

            $contact->Company = $line[$fieldLookup['Company']];
            $contact->FirstName = $line[$fieldLookup['FirstName']];
            $contact->LastName = $line[$fieldLookup['LastName']];
            $contact->Address1 = $line[$fieldLookup['Address1']];
            $contact->Address2 = $line[$fieldLookup['Address2']];
            $contact->Address3 = $line[$fieldLookup['Address3']];
            $contact->City = $line[$fieldLookup['City']];
            $contact->State = $line[$fieldLookup['State']];
            $contact->Zip = $line[$fieldLookup['Zip']];
            $contact->Phone = $line[$fieldLookup['Phone']];
            $contact->Fax = $line[$fieldLookup['Fax']];
            $contact->Title = $line[$fieldLookup['Title']];
            $contact->Prefix = $line[$fieldLookup['Prefix']];
            $contact->WorkPhone = $line[$fieldLookup['WorkPhone']];
            $contact->CellPhone = $line[$fieldLookup['CellPhone']];
            $contact->CompanyWebsite = $line[$fieldLookup['CompanyWebsite']];
            $contact->Email = $line[$fieldLookup['Email']];
            $contact->HomeAddress1 = $line[$fieldLookup['HomeAddress1']];
            $contact->HomeAddress2 = $line[$fieldLookup['HomeAddress2']];
            $contact->HomeAddress3 = $line[$fieldLookup['HomeAddress3']];
            $contact->HomeCity = $line[$fieldLookup['HomeCity']];
            $contact->HomeState = $line[$fieldLookup['HomeState']];
            $contact->HomeZip = $line[$fieldLookup['HomeZip']];
            $contact->Comments = $line[$fieldLookup['Comments']];
            $contact->Category = $line[$fieldLookup['Category']];
            $contact->Segment = $line[$fieldLookup['Segment']];
            $contact->Birthdate = $line[$fieldLookup['Birthdate']];
            $contact->Anniversary = $line[$fieldLookup['Anniversary']];
            $contact->Campaign = $line[$fieldLookup['Campaign']];

            if ($line[$fieldLookup['PublicContact']] == 'y' || $line[$fieldLookup['PublicContact']] == 'Y') {
                $contact->PublicContact = 1;
            } else {
                $contact->PublicContact = 0;
            }


            $contact->UserID = $currentUser->UserID;
            $contact->ListingType = $campaign->SubmissionListingTypeID;
                                        
                                        //If Campaign status has default set
                                        if($campaign->DefaultContactStatusTypeID > 0) {
                                        $contact->ContactStatusTypeID = $campaign->DefaultContactStatusTypeID;
                                        }

            if ($campaign->SubmissionListingTypeID == $currentGroup->DefaultSLT) {
                $contact->OtherListingType = 'hot';
            }

            $slID = $portal->AddOptimizeContact($contact);
        }
    }

    $thankyou = true;
}

if ($_SESSION['uploadfilename'] != '') {
    // open file for reading
    $file = fopen($_SESSION['uploadfilename'], "r");

    // read first line
    $templatefields = fgetcsv($file, 1000, ",", "\"");
    $samplefields = fgetcsv($file, 1000, ",", "\"");

    // trim whitespace
    for ($i = 0; $i < count($templatefields); $i++) {
        $templatefields[$i] = ($_SESSION['headerflag'] == 1 ? trim($templatefields[$i]) : "Column " . ($i + 1));
        $samplefields[$i] = trim($samplefields[$i]);
    }

    fclose($file);
}

if ($_GET['template'] == 'true') {
    $templatefilename = "template.csv";
    //do download
    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header('Content-type: application/force-download');
    header("Content-Transfer-Encoding: binary");
    header("Content-Disposition: attachment; filename=\"template.csv\";");
    header("Content-Transfer-Encoding: binary");
    header("Content-Length: " . filesize($templatefilename));

    $fd = fopen($templatefilename, "rb");
    while (!feof($fd)) {
        print(fread($fd, 8192));
    }
    fclose($fd);

    exit;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <title>
            <?= $portal->CurrentCompany->CompanyName ?> Upload <?= $campaign->ListingType ?>
        </title>
        <meta http-equiv="content-type" content="text/html;charset=utf-8" />		
        <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />
        <script  src="include/js/func.js"></script>

        <?php include("components/bootstrap.php") ?>

    </head>
    <body bgcolor="#FFFFFF" onload="<?= $close_window ? "window.close();" : "" ?>" style="margin-left: 10px;">
        <form method="post" action="<?= $_SERVER['PHP_SELF'] ?>?listid=<?= $campaign->SubmissionListingTypeID ?>" enctype="multipart/form-data" >
            <div id="UploadContactsDiv" class="well">
                <div class="page-header">
                    <h2>Upload <?= $campaign->ListingType ?></h2>
                </div>
                <div class="sectionDiv">
                    <div class="itemSection">

                        <?php if (isset($errorText)): ?>
                            <div class="container">
                                <div class="alert alert-danger alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <?= $errorText; ?>
                                </div>
                            </div>
                        <?php endif; ?>        

                        <?php
                        if ($_SESSION['uploadfilename'] == '') {
                            ?>
                            <div class="sectionContent">
                                Use the form below to upload a CSV file with your contact information.<br/>

                                <a href="<?= $_SERVER['PHP_SELF'] ?>?template=true&listid=<?= $campaign->SubmissionListingTypeID ?>">Or click here to download a template.</a>
                            </div>
                            &nbsp;<br/>
                            <?= $campaign->ListingType ?> File: <input type="file" class="form-control" name="uploadedfile"/><br/>
                            <input type="hidden" name="headerflag" value="1" /> <!-- First Row contains Header Information.<br/>-->
                            &nbsp;<br/>
                            <input name="Submit" type="submit" value="Upload" class="btn btn-primary"/>&nbsp;&nbsp;&nbsp;<input name="Submit" type="submit" value="Cancel" class="btn btn-default"/>
                            <?php
                        } elseif ($_POST['Submit'] == 'Continue ->' && $field_error == 0) {
                            ?>
                            <div class="subSectionHeader">Confirm Import</div>
                            <table width="35%" align="center">
                                <tr>
                                    <td align="left">
                                        <b>Contact Count:</b>
                                    </td>
                                    <td align="right">
                                        <?= $newContacts + $contactErrors ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <b>Errors:</b>
                                    </td>
                                    <td align="right">
                                        <?= $contactErrors ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <b>Total Import:</b>
                                    </td>
                                    <td align="right">
                                        <b><?= $newContacts ?></b>
                                    </td>
                                </tr>
                            </table>
                            &nbsp;<br/>
                            <input type="submit" name="Submit" class="btn btn-primary" value="Import Now"/>&nbsp;&nbsp;&nbsp;<input name="Submit" type="submit" class="btn btn-default" value="Cancel"/>
                            <?php
                            foreach ($fieldLookup as $k => $v) {
                                ?>
                                <input type="hidden" name="fieldLookup[<?= $k ?>]" value="<?= $v ?>"/>
                                <?php
                            }
                        } elseif ($thankyou) {
                            ?>
                            <div class="subSectionHeader">Upload Complete</div><br />
                            <input name="Submit" type="submit" value="Finish" class="btn btn-primary" />
                            <?php
                        } else {
                            ?>
                            <div class="subSectionHeader">Map Fields</div>
                            <div class="sectionContent">
                                Map your data file fields to the template fields by changing the
                                dropdown menus below.
                            </div>
                            <table class="mapTable">
                                <tr class="mapTable tableHeader">
                                    <td class="mapTable">
                                        <b>Data File Fields</b>
                                    </td>
                                    <td class="mapTable" width="120">
                                        <b>Template Fields</b>
                                    </td>
                                    <td class="mapTable">
                                        <b>Sample Data</b>
                                    </td>
                                </tr>
                                <?php
                                for ($i = 0; $i < count($templatefields); $i++) {
                                    ?>
                                    <tr class="tableDetailOdd">
                                        <td align="right" class="mapTable" style="padding-right: 5px;">
                                            <b><?= $templatefields[$i] ?> <i class="glyphicon glyphicon-arrow-right"></i></b>
                                        </td>
                                        <td align="center" class="mapTable" style="padding-right: 5px; padding-left: 5px;">
                                            <select class="form-control input-sm" name="fieldmap[<?= $i ?>]">
                                                <?php
                                                foreach ($fieldnamearray as $k => $v) {
                                                    ?>
                                                    <option value="<?= $k ?>" <?=
                                                    ($field_error == 1 ?
                                                            ($fieldmap[$i] == $k ? 'SELECTED' : '') :
                                                            ($templatefields[$i] == $k || $templatefields[$i] == $v ? 'SELECTED' : '' ) )
                                                    ?>><?= $v ?></option>
                                                            <?php
                                                        }
                                                        ?>
                                            </select>
                                        </td>
                                        <td align="left" class="mapTable" style="padding-left: 5px; color: #AAAAAA;">
                                            <?= $samplefields[$i] ?>
                                        </td>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </table>
                            &nbsp;<br />
                            <div class="buttonDiv">
                                <button name="Submit" type="submit" value="<- Back" class="btn btn-default"><i class="glyphicon glyphicon-chevron-left"></i> Back</button>&nbsp;&nbsp;&nbsp;<button name="Submit" type="submit" value="Continue ->" class="btn btn-info">Continue <i class="glyphicon glyphicon-chevron-right"></i> </button>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
        </form>

        <?php include("components/bootstrap-footer.php") ?>

    </body>
</html>