<?php
	require("globals.php");	
		
	$link = mysql_connect( $dbhost, $dbuser, $dbpass )
		or die( "<h1>Database offline or unavailable.</h1>" );
	mysql_select_db( $dbdatabase );
		
	// login check
	$sqlstring  = "SELECT 
						login.LoginID, user.UserID, company.Website
					FROM
						user
					JOIN
						login
						ON user.UserID = login.UserID
					JOIN
						companyuser
						ON user.UserID = companyuser.UserID
					JOIN
						company
						ON companyuser.CompanyID = company.CompanyID
					WHERE
						user.Username = '" . mysql_real_escape_string(stripslashes($_GET['u'])) . "'
						AND company.SecurityToken = '" . mysql_real_escape_string(stripslashes($_GET['token'])) . "'
						AND companyuser.CompanyID = $COMPANY_ID;";
	
	//echo $sqlstring;
	$account_result = mysql_query( $sqlstring )
		or die( "<h1>Errors retieving account information.</h1><br>\n" . mysql_error()); 


	if( mysql_num_rows( $account_result ) == 0 ) 
	{
		die("Invalid Login!");
	} 
	
	list($loginID, $userID, $website) = mysql_fetch_array($account_result);
	
	$_SESSION['currentuserid'] = $userID;
	$_SESSION['currentcompanyid'] = $COMPANY_ID;
	$_SESSION['logout'] = $LOGOUT_STRING;
	$_SESSION['site'] = $website;
	$_SESSION['currentuserid_' . $COMPANY_ID] = $userID;
	$_SESSION['currentcompanyid_' . $COMPANY_ID] = $COMPANY_ID;
	$_SESSION['logout_' . $COMPANY_ID] = $LOGOUT_STRING;
	$_SESSION['site_' . $COMPANY_ID] = $website;
	
	header("Location: redirect.php?loginid=$loginID");
	
	mysql_close( $link );

?>