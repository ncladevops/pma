<?php
	require("globals.php");
	require '../printable/include/component/twitter/codebird.php';
	
	\Codebird\Codebird::setConsumerKey($TWITTER_KEY, $TWITTER_SECRET);
	\Codebird\Codebird::setBearerToken($TWITTER_BEARER);
	
	$cb = \Codebird\Codebird::getInstance();
	
	$tweets = $cb->display_searched_tweets('sdpadres');
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
	<title>
		OptifiNow :: Test Tweets
	</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />		
	<link type="text/css" media="all" rel="stylesheet" href="style.css" />
	<script  src="js/func.js"></script>	
<style>
#codebird .tweet_container {
	border-bottom: 1px solid #AAA;
	padding: 5px;
}

#codebird .tweet_header {
	font-weight: bold;
}
</style>
</head>
<body bgcolor="#FFFFFF" onload="">
<div style="width: 300px;">
	<?= $tweets ?>
</div>
</body>
</html>
