<?php
require("../printable/include/mysql.inc.php");
require("../printable/include/optimize.printable.inc.php");
require("contacts_demo_lookup.php");
require("globals.php");

// Create sort Array
$viewOptions = array('sales' => 'Sales View', 'phone' => 'Phone List', 'company' => 'Company List', 'optimize' => 'Optimize List', 'marketing' => 'Marketing List');
$sortCols = array('phone' =>
    array('&nbsp;' => 'nosort', 'Full name' => 'submissionlisting.FirstName, submissionlisting.LastName', 'Company' => 'submissionlisting.Company', 'File As' => 'submissionlisting.LastName, submissionlisting.FirstName',
        'BusinessPhone' => 'nosort', 'Business Fax' => 'nosort', 'Home Phone' => 'nosort',
        'Mobile Phone' => 'nosort', '' => 'nosort'),
    'company' =>
        array('&nbsp;' => 'nosort', 'Company' => 'submissionlisting.Company', 'Full name' => 'submissionlisting.FirstName, submissionlisting.LastName',
            'Business Address' => 'submissionlisting.Address1', 'Work Phone' => 'nosort', 'Fax' => 'nosort', 'MobilePhone' => 'nosort', '' => 'nosort'),
    'optimize' =>
        array('&nbsp;' => 'nosort', 'Company' => 'submissionlisting.Company', 'Full name' => 'submissionlisting.FirstName, submissionlisting.LastName',
            'Work Phone' => 'nosort', 'events' => 'events', '' => 'nosort'),
    'sales' =>
        array('<!-- Checkbox -->&nbsp;' => 'nosort', 'ID' => 'submissionlisting.SubmissionListingID', 'Date Added' => 'DateAdded', 'Company' => 'Company', 'First Name' => 'FirstName',
    'Last Name' => 'LastName', 'Work Phone' => 'nosort', 'Cell Phone' => 'nosort',
    'State' => 'State', 'Status' => 'contactstatustype.SortOrder', 'Contact Event' => 'ContactEventTypeID',
    'Last Action Date' => 'submissionlisting.LastTouched', '&nbsp' => 'nosort'),
    'marketing' =>
        array('&nbsp;' => 'nosort', '' => 'nosort', 'Stage' => 'submissionlisting.ContactStatusTypeID', 'Company' => 'submissionlisting.Company',
            'Full name' => 'submissionlisting.FirstName, submissionlisting.LastName', 'Work Phone' => 'nosort')
);
$searchFields = array('First Name' => 'submissionlisting.FirstName', 'Last Name' => 'submissionlisting.LastName',
    'Business Name' => 'submissionlisting.Company', 'Email' => 'Email',
    'Work Phone' => 'WorkPhone', 'City' => 'submissionlisting.City',
    'State' => 'submissionlisting.State', 'Zip' => 'submissionlisting.Zip',
    'Category' => 'Category', 'Segment' => 'Segment',
    'Campaign' => 'Campaign');
//	$marketingCols = array('Direct Mail' => 'nosort', 'Email' => 'nosort', 'Case Study' => 'nosort', 'Webinar' => 'nosort', 'Infographic' => 'nosort',
//							'Presentation' => 'nosort', 'Whitepaper' => 'nosort', 'Newsletter' => 'nosort', 'Prod. Flyer' => 'nosort');
$marketingCols = array('Direct Mail' => 'nosort', 'Email' => 'nosort', 'Case Study' => 'nosort', 'Webinar' => 'nosort');
$sortCols['marketing'] = array_merge($sortCols['marketing'], $marketingCols);

$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);

$portal = new OptimizePortal($COMPANY_ID, $db);

$currentUser = $portal->GetUser($_SESSION['currentuserid']);

// Check login
if (!$currentUser) {
    header("Location: login.php?message=" . urlencode("Not logged in or login error.  Please try again."));
    die();
}

//Change page's timezone for date call functions (for default start and end date range)
$originaltimezone = date('e');
if (!empty($currentUser->UserTimezoneOffset)) {
    date_default_timezone_set($currentUser->UserTimezoneOffset); //update page timezone with the user's preferred timezone
}

if (sizeof($_GET) <= 0) {
    header("Location: " . $_SERVER['PHP_SELF'] . "?saved=1&" . $_SESSION['lastquery']);
    die();
} elseif (sizeof($_GET) == 1 && isset($_GET['message'])) {
    header("Location: " . $_SERVER['PHP_SELF'] . "?message=" . $_GET['message'] . "&" . $_SESSION['lastquery']);
    die();
}

$leadsPerPage = 25;
$_GET['offset'] = intval($_GET['offset']) < 0 ? 0 : intval($_GET['offset']);

if (!isset($viewOptions[$_GET['view']])) {
    $_GET['view'] = 'sales';
}

if (array_search($_GET['sortby'], $sortCols[$_GET['view']]) === false) {
    $_GET['sortby'] = "DateAdded DESC";
}

if ($_GET['letterfilter'] != '' && $_GET['letterfilter'] != 'All') {
    $letterString = "(submissionlisting.LastName LIKE '" . mysql_real_escape_string(stripslashes(substr($_GET['letterfilter'], 0, 1))) . "%')";
} else {
    $letterString = "1";
}
if (!isset($searchFields[$_GET['searchfield']]) || strlen($_GET['search']) <= 0) {
    $whereString = "1";
} else {
    $whereString = $searchFields[$_GET['searchfield']] . " LIKE '%" . mysql_real_escape_string(stripslashes($_GET['search'])) . "%'";
}

$userGroup = $portal->GetGroup($currentUser->GroupID);
$currentCampaign = $portal->GetSubmissionListingType($userGroup->DefaultSLT);
$contacts = $portal->GetOptimizeContacts($currentUser->UserID, $currentCampaign->SubmissionListingTypeID, "$letterString AND $whereString", false, $_GET['sortby'], $leadsPerPage, $_GET['offset']);
$leadcount = $portal->CountOptimizeContacts($currentUser->UserID, $currentCampaign->SubmissionListingTypeID, "$letterString AND $whereString");

$tempGet = $_GET;
unset($tempGet['message']);
unset($tempGet['saved']);
$_SESSION['lastquery'] = build_get_query($tempGet);
$cets = $portal->GetContactEventTypes($currentUser->GroupID, 2);
$cetLookup = array();
foreach ($cets as $cet) {
    $cetLookup[$cet->ContactEventTypeID] = $cet->EventType;
}
$csts = $portal->GetContactStatusTypes($currentUser->GroupID, 2, true);
$contactStatusesTypes = array();
$shortStatuses = array();
$shortStatusDetails = array();
foreach ($csts as $cst) {
    if (!$cst->Disabled) {
        $shortStatuses[$cst->ShortName] = (strlen($cst->DetailName) > 0 ? 1 : 0);
        $shortStatusDetails[$cst->ShortName][] = $cst->ContactStatus;
        $contactStatusesTypes[$cst->ContactStatus] = $cst;
    }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title>
        <?= $portal->CurrentCompany->CompanyName ?> :: My Contacts
    </title>
    <meta http-equiv="content-type" content="text/html;charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8"/>

    <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico"/>
    <script src="js/func.js"></script>
    <script src="js/contact_status.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
    <script language="JavaScript" type="text/JavaScript">

        var rowCount = <?= intval(sizeof($contacts)) ?>;
        var transactionURL = "printable_trans.php";
        var contactEventRequest;

        function DeleteSelected() {
            var idArray = new Array();
            for (i = 0; i < rowCount; i++) {
                var checkbox = document.getElementById("checkbox_" + i)
                if (checkbox && checkbox.checked) {
                    idArray.push(checkbox.value);
                }
            }

            if (idArray.length <= 0) {
                alert("Please Select at least one record to delete.");
            }
            else {
                if (confirm("Are you sure you want to delete all " + idArray.length + " record(s) this cannot be undone.")) {
                    document.location = "deletecontacts.php?delete=" + idArray.join(",");
                }
            }
        }

        function CloneSelected() {
            var idArray = new Array();
            for (i = 0; i < rowCount; i++) {
                var checkbox = document.getElementById("checkbox_" + i)
                if (checkbox && checkbox.checked) {
                    idArray.push(checkbox.value);
                }
            }

            if (idArray.length != 1) {
                alert("Please Select one record to clone.");
            }
            else {
                wopen('addcontactsmall.php?clone=' + idArray[0], 'edit_contact', 600, 720);
            }
        }

        function logContactEvent(contactID, eventTypeID) {

            <?php
            foreach ($cets as $cet) {
                ?>
            if (document.getElementById("event_" + contactID + "_<?= $cet->ContactEventTypeID ?>"))
                document.getElementById("event_" + contactID + "_<?= $cet->ContactEventTypeID ?>").checked = false;
            <?php
        }
        ?>
            if (document.getElementById("event_" + contactID + "_" + eventTypeID))
                document.getElementById("event_" + contactID + "_" + eventTypeID).checked = true;


            contactEventRequest = GetXmlHttpObject()
            if (contactEventRequest == null) {
                alert("Your browser does not support AJAX!");
                return;
            }
            var url = transactionURL;
            url = url + "?action=LogContactEvent&slid=" + contactID;
            url = url + "&etid=" + eventTypeID;
            url = url + "&sid=" + Math.random();
            contactEventRequest.onreadystatechange = contactEventStateChanged;
            contactEventRequest.open("GET", url, true);
            contactEventRequest.send(null);
        }

        function contactEventStateChanged() {
            if (contactEventRequest.readyState == 4) {
                var xmlDoc = contactEventRequest.responseXML.documentElement;

                // Check if session timedout
                if (xmlDoc.getElementsByTagName("Status")[0].childNodes[0].nodeValue == 'Timeout') {
                    parent.location = "login.php?logout=true&message='Your+Session+has+timedout.+Please+Login+Again.";
                    return;
                }
                if (xmlDoc.getElementsByTagName("Status")[0].childNodes[0].nodeValue == 'Fail') {
                    alert("Unable to set the Contact Event for that Contact");
                }

                <?php
    // Build string of Notes Prompts
                $notesArray = array();
                $notesString = "";
                foreach ($cets as $cet) {
                    if ($cet->Notes == 1) {
                        $notesArray[] = 'xmlDoc.getElementsByTagName("EventTypeID")[0].childNodes[0].nodeValue == "' . $cet->ContactEventTypeID . '"';
                    }
                }
                $notesString = implode(" || ", $notesArray);
                if ($notesString != '') {
                    ?>
                if (<?= $notesString ?>) {
                    wopen('setnotes.php?ceid=' + xmlDoc.getElementsByTagName("ContactEventID")[0].childNodes[0].nodeValue, 'Set_Notes', 400, 275);
                }
                <?php
            }
            ?>
            }

        }

        function ToggleSearch() {
            if (document.getElementById("searchBar").style.display == "none") {
                document.getElementById("searchBar").style.display = "";
            }
            else {
                document.getElementById("searchBar").style.display = "none";
                document.getElementById("search").value = "";
            }
        }

        var statusDetails = new Array();
        <?php
        foreach ($shortStatusDetails as $shortStat => $details) {
            ?>
        statusDetails["<?= $shortStat ?>"] = new Array();
        <?php
        foreach ($details as $det) {
            ?>
        statusDetails["<?= $shortStat ?>"][<?= intval($contactStatusesTypes[$det]->ContactStatusTypeID) ?>] = "<?= $contactStatusesTypes[$det]->DetailName ?>";
        <?php
    }
}
?>
        function showDetailedContactStatus(slid, cstid) {
            document.getElementById("detailContactID").value = slid;
            var statusSelect = document.getElementById("detailStatusID");
            document.getElementById("shortStatus").innerText = cstid;

            var i = 0;
            statusSelect.options.length = i;
            for (var id in statusDetails[cstid]) {
                statusSelect.options.length++;
                statusSelect[i].value = id;
                statusSelect[i].text = statusDetails[cstid][id];
                i++;
            }
            document.getElementById("detailDiv").style.display = "";
        }
        function submitDetailedContactStatus() {
            updateContactStatus(document.getElementById("detailContactID").value, document.getElementById("detailStatusID").value);
            document.getElementById("detailDiv").style.display = "none";
        }

    </script>

    <?php include("components/bootstrap.php") ?>

    <link type="text/css" href="js/chosen/chosen.css" rel="stylesheet" media="all"/>

</head>
<body bgcolor="#FFFFFF">
<?php include("components/header.php") ?>
<?php
$CURRENT_PAGE = "Contacts";
include("components/navbar.php");
?>

<?php if (isset($_GET['message'])): ?>
    <div class="container">
        <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?= $_GET['message']; ?>
        </div>
    </div>
<?php endif; ?>

<div class="row">

    <form method="get" class="form-inline" action="<?= $_SERVER['PHP_SELF'] ?>">

        <div class="col-md-6 pull-left">
            <ul class="nav nav-pills pull-left hidden-xs">
                <li><a href="#"
                       onclick="wopen('addcontactsmall.php?listid=<?= $currentCampaign->SubmissionListingTypeID ?>', 'edit_contact', 600, 720);
                           return false;">
                        <i class="glyphicon glyphicon-plus"></i> Add New Contact
                    </a></li>
                <li><a href="#" onclick="DeleteSelected();
                                   return false;">
                        <i class="glyphicon glyphicon-trash"></i> Delete Contacts
                    </a></li>
                <li><a href="#"
                       onclick="wopenscroll('uploadcontacts.php?listid=<?= $currentCampaign->SubmissionListingTypeID ?>', 'upload_contact', 600, 720);
                           return false;">
                        <i class="glyphicon glyphicon-upload"></i> Upload Contacts
                    </a></li>
                <li><a href="#" onclick="ToggleSearch();
                                   return false;">
                        <i class="glyphicon glyphicon-search"></i> Search
                    </a></li>
                <li><a href="#" class="actionButtonRight" onclick="window.location = window.location;
                            return false;">
                        <i class="glyphicon glyphicon-refresh"></i> Refresh
                    </a></li>
                <li><a href="#" onclick="CloneSelected();
                                   return false;">
                        <i class="glyphicon glyphicon-share-alt"></i> Clone
                    </a></li>
            </ul>
        </div>

        <?php
        $getTemp = $_GET;
        unset($getTemp['view']);
        $getString = build_get_query($getTemp);
        ?>

        <div class="col-md-6 pull-right">
            <div class="form-group pull-right">
                <label for="view">View:</label>
                <select class="form-control input-sm" name="view" id="view" onchange="this.form.submit()">
                    <?php
                    foreach ($viewOptions as $k => $v) {
                        ?>
                        <option value="<?= $k ?>" <?= $k == $_GET['view'] ? "SELECTED" : "" ?>>
                            <?= $v ?>
                        </option>
                    <?php
                    }
                    ?>
                </select>
            </div>
        </div>


        <?php
        foreach ($getTemp as $k => $v) {
            ?>
            <input type='hidden' name="<?= $k ?>" value="<?= $v ?>"/>


        <?php
        }
        ?>


    </form>

    <?php
    $getTemp = $_GET;
    unset($getTemp['search']);
    unset($getTemp['searchfield']);
    $getString = build_get_query($getTemp);
    ?>
</div>

<div id="searchBar" <?= $_GET['search'] == "" ? "style='display: none'" : "" ?> class="container row">
    <form method="get" class="form-inline" role="form" action="<?= $_SERVER['PHP_SELF'] . "?" . $getString ?>>">
        <div class="form-group col-md-3">
            <label for="search">Search:</label>
            <input type="search" name="search" class="form-control input-sm" value="<?= $_GET['search'] ?>"
                   placeholder="Search"/>
        </div>
        <div class="form-group col-md-2 chosen-inline">
            <label>in field: &nbsp;</label>
            <select name="searchfield" class="form-control input-sm">
                <?php
                foreach ($searchFields as $k => $v) {
                    ?>
                    <option value="<?= $k ?>" <?= $k == $_GET['searchfield'] ? "SELECTED" : "" ?>>
                        <?= $k ?>
                    </option>
                <?php
                }
                ?>
            </select>
        </div>
        <div class="form-group col-md-2 col-md-offset-1">
            <input type="submit" name="Submit" value="Search" class="form-control btn btn-info btn-sm"/>
            <a href="<?= $_SERVER['PHP_SELF'] ?>?saved=1">clear</a>
        </div>
    </form>
</div>

<div class="row">
    <div class="table-responsive col-md-12">
        <table class="table table-striped table-hover table-bordered">
            <thead>
            <tr class="">
                <?php
                foreach ($sortCols[$_GET['view']] as $k => $v) {
                    if ($v == "events") {
                        foreach ($cets as $et) {
                            ?>
                            <th class="contactsTable eventLabel">
                                <?= $et->EventTypeShort; ?>
                            </th>
                        <?php
                        }
                    } elseif ($v != "nosort" && $_GET['sortby'] != $v) {
                        $getTemp = $_GET;
                        $getTemp['sortby'] = $v;
                        $getString = build_get_query($getTemp);
                        ?>
                        <th class="contactsTable">
                            <a href="<?= $_SERVER['PHP_SELF'] . "?" . $getString ?>">
                                <?= $k ?>
                            </a>
                        </th>
                    <?php
                    } else {
                        ?>
                        <th class="contactsTable"><?= $k ?></th>
                    <?php
                    }
                }
                ?>
            </tr>
            </thead>

            <tbody>

            <?php
            if (is_array($contacts)) {
                $i = 0;
                foreach ($contacts as $c) {
                    $i++;
                    ?>

                    <?php
                    $date = new DateTime($c->DateAdded, new DateTimeZone($originaltimezone));  //get date / time base on string saved and server's timezone
                    if (!empty($currentUser->UserTimezoneOffset)) {
                        $date->setTimezone(new DateTimeZone($currentUser->UserTimezoneOffset)); //update date / time with the user's preferred timezone (for UserTimezoneOffset)
                    }
                    $c->DateAdded = $date->format("m/d/Y g:i a");
                    ?>

                    <tr>
                        <?php
                        if ($_GET['view'] == 'phone') {
                            ?>
                            <td class="contactsTable checkboxLabel" align="center">
                                <input type="checkbox" id="checkbox_<?= $i - 1 ?>" name="checkbox_<?= $i - 1 ?>"
                                       value="<?= $c->SubmissionListingID; ?>"/>
                            </td>
                            <td class="contactsTable" align="left">
                            	<a href="editcontact.php?id=<?= $c->SubmissionListingID ?>" title="Edit Contact" data-toggle="tooltip">
                                <?= "$c->FirstName $c->LastName" ?>
                              </a>
                            </td>
                            <td class="contactsTable" align="left">
                                <?= "$c->Company" ?>
                            </td>
                            <td class="contactsTable" align="left">
                            	<a href="editcontact.php?id=<?= $c->SubmissionListingID ?>" title="Edit Contact" data-toggle="tooltip">
                                <?= "$c->LastName, $c->FirstName" ?>
                              </a>
                            </td>
                            <td class="contactsTable" align="left">
                                <?= format_phone($c->WorkPhone) ?>
                            </td>
                            <td class="contactsTable" align="left">
                                <?= format_phone($c->Fax) ?>
                            </td>
                            <td class="contactsTable" align="left">
                                <?= format_phone($c->Phone) ?>
                            </td>
                            <td class="contactsTable" align="left">
                                <?= format_phone($c->CellPhone) ?>
                            </td>
                            <td class="contactsTable" align="center">

                                <a href="editcontact.php?id=<?= $c->SubmissionListingID ?>" title="Edit Contact"
                                   data-toggle="tooltip"><i
                                        class="glyphicon glyphicon-edit"></i></a>
                                <a href="mailto:<?= $c->Email; ?>" data-toggle="tooltip"
                                   title="Send Email"><i class="glyphicon glyphicon-envelope"></i></a>
                                <a href="#"
                                   onclick="wopen('send_sms.php?id=<?= $c->SubmissionListingID; ?>', 'send_sms', 400, 350);
                                       return false;" data-toggle="tooltip"
                                   title="Send SMS"><i class="glyphicon glyphicon-phone"></i></a>
                                <a href="#"
                                   onclick="wopen('view_appointment.php?contactID=<?= $c->SubmissionListingID ?>', 'add_appointment', 350, 435);
                                       return false;" data-toggle="tooltip"
                                   title="<?= $appt ? "Edit" : "Add" ?> Reminder"><?= $appt ? '<i class="glyphicon glyphicon-calendar"></i>' : '<i class="glyphicon glyphicon-time"></i>' ?></a>
                                <a href="#"
                                   onclick="wopen('pop_history.php?id=<?= $c->SubmissionListingID; ?>', 'pop_history', 850, 450);
                                       return false;"
                                   title="Lead History" data-toggle="tooltip"><span
                                        class="glyphicon glyphicon-list-alt"></span></a>

                                <?php
                                if ($userGroup->GroupName == 'Demo Group') {
                                    ?>
                                    <a href='images/postcards.jpg' target="blank"><img src="images/print_icon.gif"
                                                                                       border="0"/></a>
                                <?php
                                }
                                ?>
                            </td>
                        <?php
                        } elseif ($_GET['view'] == 'company') {
                            ?>
                            <td class="contactsTable checkboxLabel" align="center">
                                <input type="checkbox" id="checkbox_<?= $i - 1 ?>" name="checkbox_<?= $i - 1 ?>"
                                       value="<?= $c->SubmissionListingID; ?>"/>
                            </td>
                            <td class="contactsTable checkboxCell" align="left">
                                <?= "$c->Company" ?>
                            </td>
                            <td class="contactsTable" align="left">
                            	<a href="editcontact.php?id=<?= $c->SubmissionListingID ?>" title="Edit Contact" data-toggle="tooltip">
                                <?= "$c->FirstName $c->LastName" ?>
                              </a>
                            </td>
                            <td class="contactsTable" align="left">
                                <?= $c->Address1 != "" ? "{$c->Address1}, {$c->City}, {$c->State} {$c->Zip}" : "" ?>
                            </td>
                            <td class="contactsTable" align="left">
                                <?= format_phone($c->WorkPhone) ?>
                            </td>
                            <td class="contactsTable" align="left">
                                <?= format_phone($c->Fax) ?>
                            </td>
                            <td class="contactsTable" align="left">
                                <?= format_phone($c->CellPhone) ?>
                            </td>
                            <td class="contactsTable" align="center">
                                <a href="editcontact.php?id=<?= $c->SubmissionListingID ?>" title="Edit Contact"
                                   data-toggle="tooltip"><i
                                        class="glyphicon glyphicon-edit"></i></a>
                                <a href="mailto:<?= $c->Email; ?>" data-toggle="tooltip"
                                   title="Send Email"><i class="glyphicon glyphicon-envelope"></i></a>
                                <a href="#"
                                   onclick="wopen('send_sms.php?id=<?= $c->SubmissionListingID; ?>', 'send_sms', 400, 350);
                                       return false;" data-toggle="tooltip"
                                   title="Send SMS"><i class="glyphicon glyphicon-phone"></i></a>
                                <a href="#"
                                   onclick="wopen('view_appointment.php?contactID=<?= $c->SubmissionListingID ?>', 'add_appointment', 350, 435);
                                       return false;" data-toggle="tooltip"
                                   title="<?= $appt ? "Edit" : "Add" ?> Reminder"><?= $appt ? '<i class="glyphicon glyphicon-calendar"></i>' : '<i class="glyphicon glyphicon-time"></i>' ?></a>
                                <a href="#"
                                   onclick="wopen('pop_history.php?id=<?= $c->SubmissionListingID; ?>', 'pop_history', 850, 450);
                                       return false;"
                                   title="Lead History" data-toggle="tooltip"><span
                                        class="glyphicon glyphicon-list-alt"></span></a>
                                <?php
                                if ($userGroup->GroupName == 'Demo Group') {
                                    ?>
                                    <a href='images/postcards.jpg' target="blank"><img src="images/print_icon.gif"
                                                                                       border="0"/></a>
                                <?php
                                }
                                ?>
                            </td>
                        <?php
                        } elseif ($_GET['view'] == 'optimize') {
                            ?>
                            <td class="contactsTable checkboxLabel" align="center">
                                <input type="checkbox" id="checkbox_<?= $i - 1 ?>" name="checkbox_<?= $i - 1 ?>"
                                       value="<?= $c->SubmissionListingID; ?>"/>
                            </td>
                            <td class="contactsTable" align="left">
                                <?= "$c->Company" ?>
                            </td>
                            <td class="contactsTable" align="left">
                            	<a href="editcontact.php?id=<?= $c->SubmissionListingID ?>" title="Edit Contact" data-toggle="tooltip">
                                <?= "$c->FirstName $c->LastName" ?>
                              </a>
                            </td>
                            <td class="contactsTable" align="left">
                                <?= format_phone($c->WorkPhone) ?>
                            </td>
                            <?php
                            foreach ($cets as $et) {
                                ?>
                                <td class="contactsTable eventCell">
                                    <input
                                        type="radio" <?= $c->LastContactEvent->ContactEventTypeID == $et->ContactEventTypeID ? 'CHECKED' : '' ?>
                                        name="event_<?= $c->SubmissionListingID ?>_<?= $et->ContactEventTypeID ?>"
                                        id="event_<?= $c->SubmissionListingID ?>_<?= $et->ContactEventTypeID ?>"
                                        onclick="logContactEvent('<?= $c->SubmissionListingID ?>', '<?= $et->ContactEventTypeID ?>');<?= $et->Appointment == 1 ? "wopen('add_appointment.php?id=$c->SubmissionListingID', 'Set_Appointment', 450, 450);" : "" ?>"/>
                                </td>
                            <?php
                            }
                            ?>
                            <td class="contactsTable" align="center">
                                <a href="editcontact.php?id=<?= $c->SubmissionListingID ?>" title="Edit Contact"
                                   data-toggle="tooltip"><i
                                        class="glyphicon glyphicon-edit"></i></a>
                                <a href="mailto:<?= $c->Email; ?>" data-toggle="tooltip"
                                   title="Send Email"><i class="glyphicon glyphicon-envelope"></i></a>
                                <a href="#"
                                   onclick="wopen('send_sms.php?id=<?= $c->SubmissionListingID; ?>', 'send_sms', 400, 350);
                                       return false;" data-toggle="tooltip"
                                   title="Send SMS"><i class="glyphicon glyphicon-phone"></i></a>
                                <a href="#"
                                   onclick="wopen('view_appointment.php?contactID=<?= $c->SubmissionListingID ?>', 'add_appointment', 350, 435);
                                       return false;" data-toggle="tooltip"
                                   title="<?= $appt ? "Edit" : "Add" ?> Reminder"><?= $appt ? '<i class="glyphicon glyphicon-calendar"></i>' : '<i class="glyphicon glyphicon-time"></i>' ?></a>
                                <a href="#"
                                   onclick="wopen('pop_history.php?id=<?= $c->SubmissionListingID; ?>', 'pop_history', 850, 450);
                                       return false;"
                                   title="Lead History" data-toggle="tooltip"><span
                                        class="glyphicon glyphicon-list-alt"></span></a>
                                <?php
                                if ($userGroup->GroupName == 'Demo Group') {
                                    ?>
                                    <a href='images/postcards.jpg' target="blank"><img src="images/print_icon.gif"
                                                                                       border="0"/></a>
                                <?php
                                }
                                ?>
                            </td>
                        <?php
                        } elseif ($_GET['view'] == 'sales') {
                            $contactHistory = $portal->GetContactHistory($c->SubmissionListingID);
                            $lastCH = $contactHistory[sizeof($contactHistory) - 1];
                            ?>

                            <?php
                            $lastdate = new DateTime($lastCH->EventDate, new DateTimeZone($originaltimezone));  //get date / time base on string saved and server's timezone
                            if (!empty($currentUser->UserTimezoneOffset)) {
                                $lastdate->setTimezone(new DateTimeZone($currentUser->UserTimezoneOffset)); //update date / time with the user's preferred timezone (for UserTimezoneOffset)
                            }
                            $lastCH->EventDate = $lastdate->format("m/d/Y g:i a");
                            ?>

                            <td class="contactsTable checkboxLabel" align="center">
                                <input type="checkbox" id="checkbox_<?= $i - 1 ?>" name="checkbox_<?= $i - 1 ?>"
                                       value="<?= $c->SubmissionListingID; ?>"/>
                            </td>
                            <td class="contactsTable" align="right">
                                <?= $c->SubmissionListingID ?>
                            </td>
                            <td class="contactsTable" align="right">
                                <?= strtotime($c->DateAdded) ? date("m/d/Y g:i a", strtotime($c->DateAdded)) : "" ?>
                            </td>
                            <td class="contactsTable">
                                <?= $c->Company ?>
                            </td>
                            <td class="contactsTable">
                            	<a href="editcontact.php?id=<?= $c->SubmissionListingID ?>" title="Edit Contact" data-toggle="tooltip">
                                <?= $c->FirstName ?>
                              </a>
                            </td>
                            <td class="contactsTable">
                           	<a href="editcontact.php?id=<?= $c->SubmissionListingID ?>" title="Edit Contact" data-toggle="tooltip">
                                <?= $c->LastName ?>
                              </a>
                            </td>
                            <td align="left" style="white-space: nowrap;">
                                <?= format_phone($c->WorkPhone) ?>
                            </td>
                            <td align="left" style="white-space: nowrap;">
                                <?= format_phone($c->Phone2) ?>
                            </td>
                            <td align="center" style="white-space: nowrap;">
                                <?= $c->State ?>
                            </td>
                            <td style="width: 200px;">
                                <select name="cstid_<?= $c->SubmissionListingID ?>"
                                        id="cstid_<?= $c->SubmissionListingID ?>"
                                        onchange="updateContactStatus('<?= $c->SubmissionListingID ?>', this.value);"
                                        class="form-submenu input-sm contactStatus">
                                    <?php
                                    foreach ($contactStatusesTypes as $statusName => $status) {
                                        ?>
                                        <option
                                            value="<?= $status->ContactStatusTypeID ?>" <?= $status->ContactStatusTypeID == $c->ContactStatusTypeID ? "SELECTED" : "" ?>>
                                            <?= $statusName ?>
                                        </option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </td>
                            <td align="left">
                                <a href="#"
                                   onclick="wopen('add_action.php?id=<?= $c->SubmissionListingID; ?>', 'add_action', 350, 450);
                                       return false;">
                                    <i class="glyphicon glyphicon-star" title="Add New"></i>
                                </a>
                                <span
                                    id="cetid_<?= $c->SubmissionListingID ?>"><?= $lastCH->EventDescription == "" ? "" : $lastCH->EventDescription ?></span>
                                <!-- <?php var_dump($lastCH); ?>-->
                            </td>
                            <td>
                                <?= strtotime($lastCH->EventDate) ? date("m/d/Y g:i a", strtotime($lastCH->EventDate)) : "" ?>
                            </td>
                            <td align="center">
                                <a href="editcontact.php?id=<?= $c->SubmissionListingID ?>" title="Edit Contact"
                                   data-toggle="tooltip"><i
                                        class="glyphicon glyphicon-edit"></i></a>
                                <a href="mailto:<?= $c->Email; ?>" data-toggle="tooltip"
                                   title="Send Email"><i class="glyphicon glyphicon-envelope"></i></a>
                                <a href="#"
                                   onclick="wopen('send_sms.php?id=<?= $c->SubmissionListingID; ?>', 'send_sms', 400, 350);
                                       return false;" data-toggle="tooltip"
                                   title="Send SMS"><i class="glyphicon glyphicon-phone"></i></a>
                                <a href="#"
                                   onclick="wopen('view_appointment.php?contactID=<?= $c->SubmissionListingID ?>', 'add_appointment', 350, 435);
                                       return false;" data-toggle="tooltip"
                                   title="<?= $appt ? "Edit" : "Add" ?> Reminder"><?= $appt ? '<i class="glyphicon glyphicon-calendar"></i>' : '<i class="glyphicon glyphicon-time"></i>' ?></a>
                                <a href="#"
                                   onclick="wopen('pop_history.php?id=<?= $c->SubmissionListingID; ?>', 'pop_history', 850, 450);
                                       return false;"
                                   title="Lead History" data-toggle="tooltip"><span
                                        class="glyphicon glyphicon-list-alt"></span></a>
                                <?php
                                if ($userGroup->GroupName == 'Demo Group') {
                                    ?>
                                    <a href='images/postcards.jpg' target="blank"><img src="images/print_icon.gif"
                                                                                       border="0"/></a>
                                <?php
                                }
                                ?>
                            </td>
                        <?php
                        } elseif ($_GET['view'] == 'marketing') {
                            ?>
                                        <td  checkboxLabel" align="center">
                                             <input type="checkbox" id="checkbox_<?= $i - 1 ?>" name="checkbox_<?= $i - 1 ?>" value="<?= $c->SubmissionListingID; ?>" />
                                        </td>
                                        <td align="center" nowrap="nowrap">
                                <a href="editcontact.php?id=<?= $c->SubmissionListingID ?>" title="Edit Contact"
                                   data-toggle="tooltip"><i
                                        class="glyphicon glyphicon-edit"></i></a>
                                <a href="mailto:<?= $c->Email; ?>" data-toggle="tooltip"
                                   title="Send Email"><i class="glyphicon glyphicon-envelope"></i></a>
                                <a href="#"
                                   onclick="wopen('send_sms.php?id=<?= $c->SubmissionListingID; ?>', 'send_sms', 400, 350);
                                       return false;" data-toggle="tooltip"
                                   title="Send SMS"><i class="glyphicon glyphicon-phone"></i></a>
                                <a href="#"
                                   onclick="wopen('view_appointment.php?contactID=<?= $c->SubmissionListingID ?>', 'add_appointment', 350, 435);
                                       return false;" data-toggle="tooltip"
                                   title="<?= $appt ? "Edit" : "Add" ?> Reminder"><?= $appt ? '<i class="glyphicon glyphicon-calendar"></i>' : '<i class="glyphicon glyphicon-time"></i>' ?></a>
                                <a href="#"
                                   onclick="wopen('pop_history.php?id=<?= $c->SubmissionListingID; ?>', 'pop_history', 850, 450);
                                       return false;"
                                   title="Lead History" data-toggle="tooltip"><span
                                        class="glyphicon glyphicon-list-alt"></span></a>
                                               <?php
                            if ($userGroup->GroupName == 'Demo Group') {
                                ?>
                                <a href='images/postcards.jpg' target="blank"><img src="images/print_icon.gif"
                                                                                   border="0"/></a>
                            <?php
                            }
                            ?>
                                        </td>
                                        <td align="left">
                                            <select
                                                style="color: <?= isset($statColors[$contactStatusesTypes[$c->ContactStatus]->ShortName]) ? $statColors[$contactStatusesTypes[$c->ContactStatus]->ShortName] : "black" ?>"
                                                name="cstid_<?= $c->SubmissionListingID ?>"
                                                id="cstid_<?= $c->SubmissionListingID ?>"
                                                onchange="updateContactStatus('<?= $c->SubmissionListingID ?>', this.value);">
                                                    <?php
                            $shown = false;
                            foreach ($shortStatuses as $stat => $isShort) {
                                $sColor = isset($statColors[$stat]) ? $statColors[$stat] : "black";
                                if ($isShort) {
                                    $shown = (array_search($c->ContactStatus, $shortStatusDetails[$stat]) === false) ? $shown : true;
                                    ?>
                                    <option style="color: <?= $sColor ?>"
                                            value="<?= $stat ?>"
                                        <?= array_search($c->ContactStatus, $shortStatusDetails[$stat]) === false ? '' : 'SELECTED' ?>>
                                        <?= $stat ?>
                                    </option>
                                <?php
                                } else {
                                    $shown = ($contactStatusesTypes[$stat]->ContactStatusTypeID == $c->ContactStatusTypeID) ? true : $shown;
                                    ?>
                                    <option style="color: <?= $sColor ?>"
                                            value="<?= $contactStatusesTypes[$stat]->ContactStatusTypeID ?>"
                                        <?= $contactStatusesTypes[$stat]->ContactStatusTypeID == $c->ContactStatusTypeID ? 'SELECTED' : '' ?>>
                                        <?= $stat ?>
                                    </option>
                                <?php
                                }
                            }
                            if (!$shown) {
                                ?>
                                <option value="<?= $c->ContactStatusTypeID ?>" selected="selected">
                                    <?= $c->ContactStatus ?>
                                </option>
                            <?php
                            }
                            ?>
                                            </select>
                                        </td>
                                        <td align="left">
                                            <?= "$c->Company" ?>
                                        </td>
                                        <td align="left">
                                        	<a href="editcontact.php?id=<?= $c->SubmissionListingID ?>" title="Edit Contact" data-toggle="tooltip">
                                            <?= "$c->LastName, $c->FirstName" ?>
                                          </a>
                                        </td>
                                        <td align="left" nowrap="nowrap">
                                            <?= format_phone($c->WorkPhone) ?>
                                        </td>
                                        <?php
                            foreach ($marketingCols as $mcName => $v) {
                                ?>

                                <td align="left">
                                    <select class="custom_select">
                                        <option>- Please Choose -</option>
                                        <?php
                                        foreach ($demoLookup[$c->ContactStatus][$mcName] as $optName => $optImage) {
                                            ?>
                                            <option title='<?= $optImage; ?>'><?= $optName; ?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </td>
                            <?php
                            }
                            ?>
                        <?php }
                        ?>
                    </tr>
                <?php
                }
            }
            ?>

            </tbody>

        </table>
    </div>

</div>
<div class="row">
    <div class="paginationNav">
        <ul class="pagination">
            <?= build_page_string($_SERVER['PHP_SELF'], $_GET, $leadsPerPage, $_GET['offset'], $leadcount); ?>
        </ul>
    </div>
</div>
<?php date_default_timezone_set($originaltimezone); //reset the changed timezone to the original server timezone  ?>

<?php include("components/footer.php") ?>

<script type="text/javascript" src="js/chosen/chosen.jquery.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $(".contactStatus").chosen();
    });
</script>

</body>
</html>