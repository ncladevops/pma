<?php
	require("../printable/include/mysql.inc.php");
	require("../printable/include/optimize.printable.inc.php");
	require("globals.php");
	
	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	
	$portal = new OptimizePortal($COMPANY_ID, $db);
	
	$currentUser = $portal->GetUser($_SESSION['currentuserid']);
	
	$isSubAdmin = $portal->CheckPriv($currentUser->UserID, 'subadmin');
	$isAdmin = $portal->CheckPriv($currentUser->UserID, 'admin');
				
	if(!$isSubAdmin)
	{
		header( "Location: " . $portal->CurrentCompany->Website . "home.php?message=" . urlencode( "Accessed Denied." ) );
		die();
	}
		
	$groups = $portal->GetCompanyGroups();
		
	if($_POST['Submit'] == 'Cancel')
	{
		header('Location: manage_filters.php?message=' . urlencode("Action Canceled. Filter not updated."));
		die();
	}
	elseif($_POST['FilterName'])
	{
		$filterID = $portal->CreateFilter($_POST['FilterName']);
		
		$filter = $portal->GetFilter($filterID);
		
		$filter->GroupID = $_POST['GroupID'];
		
		$portal->UpdateFilter($filter);
		
		header("Location: edit_filter.php?filterid=$filterID");
		die();
	}
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	<title>
        <?= $portal->CurrentCompany->CompanyName ?> :: Add New Filter
    </title>
    <meta http-equiv="content-type" content="text/html;charset=utf-8"/>

    <?php include("components/bootstrap.php") ?>

    <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico"/>

    <script src="js/func.js"></script>
</head>
<body bgcolor="#FFFFFF">
<?php include("components/header.php") ?>
<div id="body">
	<?php
    $CURRENT_PAGE = "Home";
    include("components/navbar.php");
    ?>
    <?php if (isset($_GET['message'])): ?>
        <div class="container">
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?= $_GET['message']; ?>
            </div>
        </div>
    <?php endif; ?>
    <div id="controlPanelContainer" class="row">
        <div id="folderTreeContainer" class="col-md-offset-2 col-md-2 panel panel-default">
            <?php include("components/controlpanel_tree.php"); ?>
        </div>
        <div class="sectionHeader col-md-7">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Control Panel - Add Filter</h3>
                </div>
                <div class="panel-body">
                    <div id="detailContainer">
                        <div class="row">
                            <form method="post" action="<?= $_SERVER['PHP_SELF'] ?>">
                            <div style="width: 300px;">
                            <label for="SubmissionListingSourceID">Group</label>
									 <select id="GroupID" name="GroupID">
										<option value="0">All</option>
									   <?php
									   foreach($groups as $g)
									   {
									   ?>
										<option value="<?= $g->GroupID?>"
										<?= $g->GroupID == $campaign->GroupID ? "SELECTED" : "" ?>>
										<?= $g->GroupName ?>
										</option>
										<?php } ?>
									 </select>
									 </div><br/>
									 <label for="FilterName">Filter Name:</label>
									 <input type="text" id="FilterName" name="FilterName" style="width: 250px" /><br/><br/>
									 <input type="submit" value="Add Filter" name="Submit"/>&nbsp;&nbsp;
									 <input type="submit" value="Cancel" name="Submit"/>
                            </form>
                        </div>
                    </div>
                </div>
           </div>
       </div>
   </div>
</div>
<?php include("components/footer.php") ?>
</body>
</html>