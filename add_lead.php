<?php
require("../printable/include/mysql.inc.php");
require("../printable/include/optimize.printable.inc.php");
require("globals.php");

$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);

$portal = new OptimizePortal($COMPANY_ID, $db);

$currentUser = $portal->GetUser($_SESSION['currentuserid']);

if (!$currentUser) {
    header("Location: login.php?message=" . urlencode("Not logged in or login error."));
    die();
}

if ($_GET['clone']) {
    $cloneContact = $portal->GetOptimizeContact($_GET['clone'], $currentUser->UserID);

    if (!cloneContact) {
        die("Invalid Contact to Clone.");
    }

    $cloneContact->SubmissionListingID = "";

    $slID = $portal->AddOptimizeContact($cloneContact);

    header("Location: edit_lead.php?id=$slID");
    die();
}

if ($_POST['Submit'] == 'Add Lead' && $_POST['LastName'] != "") {
    $contact = new OptimizeContact();

    $contact->FirstName = $_POST['FirstName'];
    $contact->LastName = $_POST['LastName'];
    $contact->ListingType = $_POST['ListingType'];
    $contact->OtherListingType = $_POST['OtherListingType'];
    $contact->UserID = $currentUser->UserID;
    $contact->ContactStatusTypeID = $CONTACTED_STATUSTYPE_ID;

    $slID = $portal->AddOptimizeContact($contact);

    if (!$slID) {
        header("Location: home.php?message=" . urlencode("Error adding lead."));
        die();
    }

    header("Location: edit_lead.php?id=$slID");
    die();
} elseif ($_GET['id'] != "") {
    $lead = $portal->GetOptimizeContact($_GET['id'], 'all');

    if (!$lead) {
        header("Location: list_lead.php?message=" . urlencode("Invalid Lead Id."));
        die();
    }

    if (isset($ADDSEARCH_OPTIONS[$_GET['type']]['Type'])) {
        //Update Date Assigned when claiming lead
        $lead->DateAssigned = date("Y-m-d H:i:s");

        switch ($ADDSEARCH_OPTIONS[$_GET['type']]['Type']) {
            case "Search":
                $lead->ContactStatusTypeID = $CONTACTED_STATUSTYPE_ID;
                $lead->ListingType = $ADDSEARCH_OPTIONS[$_GET['type']]['ListingType'];
                $lead->UserID = $currentUser->UserID;
                $lead->DateAdded = date("Y-m-d H:i:s");
                break;
            case "Transfer":
                if ($lead->ContactStatusTypeID != $ADDSEARCH_OPTIONS[$_GET['type']]['TransferStatusID']) {
                    header("Location: list_lead.php?message=" . urlencode("Invalid Lead Id."));
                    die();
                }
                $lead->ContactStatusTypeID = $CONTACTED_STATUSTYPE_ID;
                $lead->UserID = $currentUser->UserID;
                break;
            case "Store":
                if ($lead->ContactStatusTypeID != $ADDSEARCH_OPTIONS[$_GET['type']]['StoreStatusID']) {
                    header("Location: list_lead.php?message=" . urlencode("Invalid Lead Id."));
                    die();
                }
                $lead->ContactStatusTypeID = $CONTACTED_STATUSTYPE_ID;
                $lead->UserID = $currentUser->UserID;
                break;
            default:
                break;
        }
    }

    $portal->UpdateOptimizeContact($lead);

    header("Location: edit_lead.php?id={$lead->SubmissionListingID}");
    die();

} elseif ($_GET['multileadids'] != "") {
    $leadIDs = explode(",", $_GET['multileadids']);

    foreach ($leadIDs as $id) {
        $lead = $portal->GetOptimizeContact($id, 'all');

        switch ($ADDSEARCH_OPTIONS[$_GET['type']]['Type']) {
            case "Search":
                $lead->ContactStatusTypeID = $CONTACTED_STATUSTYPE_ID;
                $lead->ListingType = $ADDSEARCH_OPTIONS[$_GET['type']]['ListingType'];
                $lead->UserID = $currentUser->UserID;
                $lead->DateAdded = date("Y-m-d H:i:s");
                break;
            case "Transfer":
                if ($lead->ContactStatusTypeID != $ADDSEARCH_OPTIONS[$_GET['type']]['TransferStatusID']) {
                    header("Location: list_lead.php?message=" . urlencode("Invalid Lead Id."));
                    die();
                }
                $lead->ContactStatusTypeID = $CONTACTED_STATUSTYPE_ID;
                $lead->UserID = $currentUser->UserID;
                break;
            case "Store":
                if ($lead->ContactStatusTypeID == $ADDSEARCH_OPTIONS[$_GET['type']]['StoreStatusID']) {
                    $lead->ContactStatusTypeID = $CONTACTED_STATUSTYPE_ID;
                    $lead->UserID = $currentUser->UserID;
                }
                break;
            default:
                break;
        }

        $portal->UpdateOptimizeContact($lead);
    }

    header("Location: list_lead.php?message=" . urlencode("Leads transferred successfully"));
    die();

} elseif ($_POST['Submit'] == 'Cancel') {
    header("Location: list_lead.php?message=" . urlencode("Action Canceled. No lead added."));
    die();
}

$slts = $portal->GetSubmissionListingTypes(0, 'SortOrder, SubmissionListingTypeID', 'all', '(UserManageable = 1 AND submissionlistingtype.SubmissionListingSourceID = 9999) && disabled = 0');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title>
        <?= $portal->CurrentCompany->CompanyName ?> Leads on Demand :: Add New Lead
    </title>
    <meta http-equiv="content-type" content="text/html;charset=utf-8"/>
    <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico"/>
    <?php include("components/bootstrap.php") ?>
</head>
<body bgcolor="#FFFFFF"
      onload="<?= $_GET['searchtype'] == "Store" ? "toggleSearch('Lead Store');searchLeads(0);" : "" ?>">
<?php include("components/header.php") ?>
<div id="body">
    <div class="row">
        <?php
        $CURRENT_PAGE = "Leads";
        include("components/navbar.php");
        ?>
        <form method="post" action="<?= $_SERVER['PHP_SELF'] ?>">
            <div id="AddLeadDiv" class="col-md-offset-3 col-md-6 well">
                <h2>Add New Lead</h2>

                <div class="sectionDiv">
                    <div class="form-group">
                        <label for="ListingType">Lead Source:</label>
                        <select name="ListingType" id="ListingType" style="width: 250px" class="form-control input-sm">
                            <?php
                            foreach ($slts as $slt) {
                                ?>
                                <option value="<?= $slt->SubmissionListingTypeID ?>">
                                    <?= $slt->ListingType ?>
                                </option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="OtherListingType">Campaign Code:</label>
                        <input type="text" id="OtherListingType" name="OtherListingType" style="width: 250px"
                               class="form-control input-sm"/>
                    </div>
                    <div class="form-group">
                        <label for="FirstName">Lead First Name:</label>
                        <input type="text" id="FirstName" name="FirstName" style="width: 200px"
                               class="form-control input-sm"/>
                    </div>
                    <div class="form-group">
                        <label for="LastName">Lead Last Name:</label>
                        <input type="text" id="LastName" name="LastName" style="width: 200px"
                               class="form-control input-sm"/>
                    </div>
                    <div>
                        <input type="submit" value="Add Lead" name="Submit" class="btn btn-info"/>&nbsp;&nbsp;<input
                            type="submit" value="Cancel" name="Submit" class="btn btn-default"/>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<?php include("components/footer.php") ?>

</body>
</html>