<?php
require("../printable/include/mysql.inc.php");
require("../printable/include/optimize.printable.inc.php");
require("globals.php");

$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);

$portal = new OptimizePortal($COMPANY_ID, $db);

$currentUser = $portal->GetUser($_SESSION['currentuserid']);

if (!$portal->CheckPriv($currentUser->UserID, 'subadmin')) {
    header("Location: " . $portal->CurrentCompany->Website . "home.php?message=" . urlencode("Accessed Denied."));
    die();
}

$sortCols = array('ID' => 'ExtensionID', 'Workgroup Name' => 'ExtensionName', 'Workgroup Extension' => 'Extension',
    'Campaign Count' => 'nosort', 'Delete' => 'nosort'
);

if (array_search($_GET['sortby'], $sortCols) === false) {
    $_GET['sortby'] = "Extension";
}

$extensions = $portal->GetExtensions($_GET['sortby'])
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <title>
            <?= $portal->CurrentCompany->CompanyName ?> :: Manage Workgroups
        </title>
        <meta http-equiv="content-type" content="text/html;charset=utf-8" />		
        <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />	
        <script  src="js/func.js"></script>	

        <?php include("components/bootstrap.php") ?>

    </head>
    <body bgcolor="#FFFFFF">
        <?php include("components/header.php") ?>
        <div id="body">
            <?php
            $CURRENT_PAGE = "Control Panel";
            include("components/navbar.php");
            ?>

            <?php if (isset($_GET['message'])): ?>
                <div class="container">
                    <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <?= $_GET['message']; ?>
                    </div>
                </div>
            <?php endif; ?> 

            <div id="controlPanelContainer" class="row">
                <div id="folderTreeContainer" class="col-md-offset-2 col-md-2 panel panel-default">
                    <?php include("components/controlpanel_tree.php"); ?>
                </div>

                <div class="sectionHeader col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Control Panel - Edit Workgroups</h3>
                        </div>
                        <div class="panel-body">
                            <div id="detailContainer">
                                    <div id="addDiv" class="row">
                                        <a class="btn btn-sm btn-default" href="add_extension.php">Add Workgroup</a>
                                    </div>
                                <div id="ExtensionDiv" class="table-responsive row">
                                    <table class="extension_table table table-striped table-hover table-bordered" >
                                        <thead>
                                            <tr class="extension_table rowheader">
                                                <?php
                                                foreach ($sortCols as $k => $v) {
                                                    if ($v != "nosort" && $_GET['sortby'] != $v) {
                                                        $getTemp = $_GET;
                                                        $getTemp['sortby'] = $v;
                                                        $getString = build_get_query($getTemp);
                                                        ?>
                                                        <th class="extension_table">
                                                            <a href="<?= $_SERVER['PHP_SELF'] . "?" . $getString ?>">
                                                                <?= $k ?>
                                                            </a>
                                                        </th>
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <th class="extension_table"><?= $k ?></th>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if (is_array($extensions)) {
                                                $i = 0;
                                                foreach ($extensions as $e) {
                                                    $i++;
                                                    $camps = $portal->GetSubmissionListingTypes(0, "SortOrder", "all", "ExtensionList LIKE '%" . intval($e->Extension) . ";%'")
                                                    ?>
                                                    <tr class="extension_table <?= $i % 2 == 0 ? ' roweven' : ' rowodd' ?>">
                                                        <td class="extension_table" align="right">
                                                            <?= $e->ExtensionID ?>
                                                        </td>
                                                        <td class="extension_table">
                                                            <?= $e->ExtensionName ?>
                                                        </td>
                                                        <td class="extension_table">
                                                            <?= $e->Extension ?>
                                                        </td>
                                                        <td class="extension_table">
                                                            <?= sizeof($camps) ?>
                                                        </td>
                                                        <td class="extension_table" align="right">
                                                            <a href="delete_extension.php?id=<?= $e->ExtensionID ?>">
                                                                delete
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php include("components/footer.php") ?>

    </body>
</html>