<?php
require("../printable/include/mysql.inc.php");
require("../printable/include/optimize.printable.inc.php");
require("globals.php");

$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);

$portal = new OptimizePortal($COMPANY_ID, $db);

$currentUser = $portal->GetUser($_SESSION['currentuserid']);

//Not used in Optimized
//$currentUser = $portal->UserAccess($_SESSION['currentuserid']);
//$currentGroup = $portal->GetGroup($currentUser->GroupID);

if (!$currentUser) {
    header("Location: login.php?message=" . urlencode("Not logged in or login error."));
    die();
}

$isAdmin = $portal->CheckPriv($currentUser->UserID, 'admin');
$isCorpMan = $portal->CheckPriv($currentUser->UserID, 'corporatemanager');
$isRegMan = $portal->CheckPriv($currentUser->UserID, 'regionmanager');
$isDivMan = $portal->CheckPriv($currentUser->UserID, 'divisionmanager');
if ($isAdmin || $isCorpMan || $isRegMan || $isDivMan)
    $isMan = true;
else
    $isMan = false;

$lead = $portal->GetOptimizeContact($_GET['id'], $currentUser->UserID);

if (!$lead) {
    die("Lead not Available");
}

if (!$isMan && ($lead->ContactStatusTypeID == $NEW_STATUSTYPE_ID || $lead->ContactStatusTypeID == $TRANSFER_STATUSTYPE_ID || $lead->ContactStatusTypeID == $NEW_DEMAND_STATUSTYPE_ID)) {
    die("Please take control of this lead before you attempt to view it.");
}

$allEvents = $portal->GetContactHistory($lead->SubmissionListingID, "DESC");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title>
        <?= $portal->CurrentCompany->CompanyName ?> :: Leads on Demand :: History
        for <?= "$lead->FirstName $lead->LastName" ?>
    </title>
    <meta http-equiv="content-type" content="text/html;charset=utf-8"
    / >
    <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico"/>

    <?php include("components/bootstrap.php") ?>

</head>
<body bgcolor="#FFFFFF">
<div id="body">
    <div id="EditLeadDiv" style="width: 100%;">
        <div class="error">
            <?= $_GET['message']; ?>
        </div>
        <div class="subSectionHeader breakpage">
            History (<?= "$lead->FirstName $lead->LastName" ?>)
        </div>
        <div id="HistoryDiv" class="sectionDiv">
            <div id="EventTypeDiv" class="table-responsive">
                <table class="table table-striped table-hover table-bordered">
                    <tr class="event_table rowheader">
                        <th class="event_table">
                            Event Type
                        </th>
                        <th class="event_table">
                            Event Time
                        </th>
                        <th class="event_table">
                            Notes
                        </th>
                        <th class="event_table">
                            User
                        </th>
                    </tr>
                    <?php
                    $i = 0;
                    foreach ($allEvents as $event) {
                        $i++;
                        ?>
                        <tr class="event_table<?= $i % 2 == 0 ? ' roweven' : ' rowodd' ?>">
                            <td class="event_table">
                                <?= $event->EventDescription ?>
                            </td>
                            <td class="event_table">
                                <?= strtotime($event->EventDate) ? date("m/d/Y g:i:s a", strtotime($event->EventDate)) : ""; ?>
                            </td>
                            <td class="event_table">
                                <?= $event->EventNotes ?>
                            </td>
                            <td class="event_table">
                                <?= $event->UserFullname ?>
                            </td>
                        </tr>
                    <?php
                    }
                    ?>
                </table>
            </div>
        </div>
    </div>
</div>

<?php include("components/bootstrap-footer.php") ?>

</body>
</html>