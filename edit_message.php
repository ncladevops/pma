<?php
require("../printable/include/mysql.inc.php");
require("../printable/include/optimize.printable.inc.php");
require("globals.php");

$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);

$portal = new OptimizePortal($COMPANY_ID, $db);

$currentUser = $portal->UserAccess($_SESSION['currentuserid']);
$isSuper = $portal->CheckPriv($currentUser->UserID, 'supervisor');

if (!$portal->CheckPriv($currentUser->UserID, 'subadmin')) {
    header("Location: " . $portal->CurrentCompany->Website . "home.php?message=" . urlencode("Accessed Denied."));
    die();
}

$message = $portal->GetCustomMessage($_GET['messageid'], true);

// if message doesn't exist redirect
if (!$message) {
    header("Location: manage_messages.php?status=" . urlencode("MessageID not found"));
}

// Check for Button Pressed
if ($_POST['Submit'] == 'Update') {
    $message->Message = $_POST['Message'];
    $portal->UpdateMessage($message);

    header("Location: manage_messages.php?status=" . urlencode("Message edited successfully"));
} else if ($_POST['Submit'] == 'Cancel') {
    header("Location: manage_messages.php?status=" . urlencode("Message edit cancelled"));
}

$gs = $portal->GetCompanyGroups();

$groups = array();

$groups[0] = new Group();
$groups[0]->GroupID = 0;
$groups[0]->GroupName = "All";

foreach ($gs as $g) {
    $groups[$g->GroupID] = $g;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <title>
            <?= $portal->CurrentCompany->CompanyName ?> :: Edit Custom Message
        </title>
        <meta http-equiv="content-type" content="text/html;charset=utf-8" />		
        <script  src="js/func.js"></script>	

        <!-- jQuery and jQuery UI -->
        <script src="js/elrte/js/jquery-1.6.1.min.js" type="text/javascript" charset="utf-8"></script>
        <script src="js/elrte/js/jquery-ui-1.8.13.custom.min.js" type="text/javascript" charset="utf-8"></script>
        <!-- elRTE -->
        <script src="js/elrte/js/elrte.min.js" type="text/javascript" charset="utf-8"></script>
        <link rel="stylesheet" href="js/elrte/css/smoothness/jquery-ui-1.8.13.custom.css" type="text/css" media="screen" charset="utf-8">


            <link rel="stylesheet" href="js/elrte/css/elrte.min.css" type="text/css" media="screen" charset="utf-8">
                <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />

                <?php include("components/bootstrap.php") ?>

                </head>
                <body bgcolor="#FFFFFF">
                    <div id="page">
                        <?php include("components/header.php") ?>
                        <div id="body">
                            <?php
                            $CURRENT_PAGE = "Home";
                            include("components/navbar.php");
                            ?>
                            <form name="form1" method="post" action="<?= $_SERVER['PHP_SELF'] ?>?messageid=<?= $message->MessageID; ?>">

                                <?php if (isset($_GET['message'])): ?>
                                    <div class="container">
                                        <div class="alert alert-danger alert-dismissable">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                            <?= $_GET['message']; ?>
                                        </div>
                                    </div>
                                <?php endif; ?> 

                                <div id="AddMessageDiv" class="well container">
                                    <div class="sectionHeader">
                                        <h2>Edit Message</h2>
                                    </div>
                                    <div class="sectionDiv">
                                        <div class="itemSection row">
                                            <div id="MessageNameDiv" class="form-group col-md-3">
                                                <label>Message Name:</label>
                                                <?= $message->MessageName ?>
                                            </div>
                                        </div>
                                        <div class="itemSection row">
                                            <div id="GroupNameDiv" class="form-group col-md-3">
                                                <label>Group Name:</label>
                                                <?= $groups[$message->GroupID]->GroupName ?>
                                            </div>
                                        </div>
                                        <div class="longItemSection row">
                                            <div id="MessageDiv" class="form-group col-md-12">
                                                <label for="ListingDescription">Message:</label><br/>
                                                <textarea class="cleditor" id="Message" name="Message" rows="3"><?= $message->Message ?></textarea>
                                            </div>
                                        </div>
                                    </div>		
                                    <center>
                                        <div class="itemSection row">
                                            <div class="buttonSection">
                                                <input type="submit" value="Update" name="Submit" class="btn btn-info btn-sm" id="updateButton"/>&nbsp;&nbsp;<input type="submit" value="Cancel" class="btn btn-default btn-sm" name="Submit"/>
                                            </div>
                                        </div>
                                    </center>
                                </div>
                            </form>
                        </div>
                    </div>
                    <?php include("components/footer.php") ?>
                </body>
                </html>