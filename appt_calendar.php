<?php
	require("../printable/include/mysql.inc.php");
	require("../printable/include/optimize.printable.inc.php");
	require("globals.php");
	
	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	
	$portal = new OptimizePortal($COMPANY_ID, $db);
	
	$viewOptions = array("month", "basicWeek", "basicDay", "agendaWeek", "agendaDay");
	
	$currentUser = $portal->GetUser($_SESSION['currentuserid']);

		//Change page's timezone for date call functions (for default start and end date range)
	$originaltimezone = date('e');
	if(!empty($currentUser->UserTimezoneOffset)){
	date_default_timezone_set($currentUser->UserTimezoneOffset); //update page timezone with the user's preferred timezone
	}
	
	if(!$currentUser)
	{
		header("Location: login.php?message=" .urlencode("Not logged in or login error."));
		die();
	}
	
	/* if($portal->CheckPriv($currentUser->UserID, 'corporatemanager')) {
		$appts = $portal->GetAppointments("all", "2011-01-01");
	}
	else if($portal->CheckPriv($currentUser->UserID, 'regionmanager')) {
		$appts = $portal->GetRegionAppointments($currentUser->RegionID, "2011-01-01");
	}
	else if($portal->CheckPriv($currentUser->UserID, 'divisionmanager')) {
		$appts = $portal->GetDivisionAppointments($currentUser->DivisionID, "2011-01-01");
	}
	else {  	} */
		$appts = $portal->GetAppointments($currentUser->UserID, "2011-01-01");

	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
	<title>
		<?= $portal->CurrentCompany->CompanyName ?> :: Appointment Calendar
	</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />		
	<link type="text/css" media="all" rel="stylesheet" href="style.css" />	
	<link type="text/css" media="all" rel="stylesheet" href="css/appt_calendar.css" />
	<link rel='stylesheet' type='text/css' href='../printable/include/js/fullcalendar/fullcalendar.css' />
	<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />
	<script type='text/javascript' src='../printable/include/js/fullcalendar/jquery-1.9.1.min.js'></script>
	<script type='text/javascript' src='../printable/include/js/fullcalendar/fullcalendar.js'></script>
	<script  src="js/func.js"></script>	
<script type='text/javascript'>
	$(document).ready(function() {

		$('#calendar').fullCalendar({
			header: {
				left:   'prev,next today tomorrow',
				center: 'title',
				right:  'agendaDay,agendaWeek,month'
			},
<?php
	if(sizeof($appts) > 0) {
?>
			events: [
<?php
		foreach($appts as $a) {
?>

<?php
$date = new DateTime($a->AppointmentTime, new DateTimeZone($originaltimezone));  //get date / time base on string saved and server's timezone
if(!empty($currentUser->UserTimezoneOffset)){
$date->setTimezone(new DateTimeZone($currentUser->UserTimezoneOffset)); //update date / time with the user's preferred timezone (for UserTimezoneOffset)
}
$Y = $date->format("Y");
$n = $date->format("n");
$j = $date->format("j");
$G = $date->format("G");
$i = $date->format("i");
?>
				{
					id: <?= $a->ContactAppointmentID; ?>,
					title: 'Appt: <?= $a->ContactName; ?>',
					start: new Date(<?= $Y ?>,
									<?= $n-1 ?>,
									<?= $j ?>,
									<?= $G ?>,
									<?= $i ?>),
					allDay: false,
					url: '<?= $portal->CurrentCompany->Website?>/edit_appointment.php?id=<?= $a->ContactAppointmentID; ?>'
				},
<?php
		}
?>
			],
			eventClick: function(event) {
				if(event.url) {
					wopen(event.url, 'edit_appointment', 450, 450);
					return false;
				}
			},
<?php
	}
?>
			defaultView: "<?= (array_search($_GET['defaultView'], $viewOptions) === false) ? "month" : $_GET['defaultView'] ?>",
			year: <?= strtotime($_GET['defaultDate']) ? date("Y", strtotime($_GET['defaultDate'])) : date("Y") ?>,
			month: <?= (strtotime($_GET['defaultDate']) ? date("n", strtotime($_GET['defaultDate'])) : date("n")) - 1 ?>,
			date: <?= (strtotime($_GET['defaultDate']) ? date("j", strtotime($_GET['defaultDate'])) : date("j")) ?>
		});

	});
</script>
</head>
<body>
<div id="calendar"></div>
<?php date_default_timezone_set($originaltimezone); //reset the changed timezone to the original server timezone ?>
</body>
</html>