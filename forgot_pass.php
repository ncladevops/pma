<?php
	require("../printable/include/mysql.inc.php");
	require("../printable/include/optimize.printable.inc.php");
	require("globals.php");
	
	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	$portal = new OptimizePortal($COMPANY_ID, $db);
	$currentUser = $portal->UserAccess($_SESSION['currentuserid']);
	if($currentUser) {
		header("Location: home.php?message=" .urlencode("You are already logged in"));
		die();
	}
	
	// Check to see if the form has been submitted
	if( isset($_POST['Submit'])) {
		// Check that email exists
		if( $portal->ResetPass(mysql_real_escape_string($_POST['email']), $STORE_PW) ) {
			// redirect user back to login page with message
			header( "Location: login.php?message=" . urlencode( "Your new password has been sent to " . $_POST['email'] . "." ) );
			die();
		} else {
			$_GET['message'] = "That Email does not exist in the system.";			
		}
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	<title>
		<?= $portal->CurrentCompany->CompanyName ?> :: Forgot Password
	</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />		
	<link type="text/css" media="all" rel="stylesheet" href="style.css" />	
	<link type="text/css" media="all" rel="stylesheet" href="css/login.css" />	
</head>
<body bgcolor="#FFFFFF" onload="document.forms.login_form.username.focus();">
<div id="body">
	<form method="post" action="forgot_pass.php" name="login_form">
		<div id="login_container" style="padding-top: 0px; text-align: center;">
			<div style="padding-top: 100px;">
				<h2>Reset Password</h2>
				<div class="error"><?= $_GET['message'] ?></div>
				<div class="details">
					Use the form below to have a new password sent to your email.
				</div>
				<input type="text" name="email" /><br/>
				<input type="submit" name="Submit" value="Reset Now" /> <input type="button" onclick="parent.location='forgot_pass.php'" value="Cancel"/> 
			</div>
		</div>
	</form>
</div>
</body>
</html>