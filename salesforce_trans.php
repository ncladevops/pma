<?php
	require("../printable/include/mysql.inc.php");
	require("../printable/include/optimize.printable.inc.php");
	require("../printable/include/contactfactory.inc.php");
	require_once('../soaplib/nusoap.php');
	require("globals.php");
	
	//error_reporting(0);
	
	$DUPLICATE_SECONDS_RANGE = 120;
		
	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	
	$portal = new OptimizePortal($COMPANY_ID, $db);
	$factory = new ContactFactory($db);

	// Create the server instance
	$server = new soap_server();

	// Turn on Debug
	//$server->debug_flag = true;
	
	// Initialize WSDL support
	
	$server->configureWSDL('BCOnDemand', 'urn:BCOnDemand',false, 'document');
	
	// Register the data structures used by the service
	// *************************************
	// *** GetUserInfo Structures           ***
	// ************************************* 
	$server->wsdl->addComplexType(
	    'GetUserInfo',
	    'complexType',
	    'struct',
	    'sequence',
	    '',
	    array(
	        'AppKey' => array('name' => 'AppKey', 'type' => 'xsd:string'),
	    	'Email' => array('name' => 'Email', 'type' => 'xsd:string')
	    )
	);
	$server->wsdl->addComplexType(
	    'UserInfo',
	    'complexType',
	    'struct',
	    'sequence',
	    '',
	    array(
	    	'UserID' => array('name' => 'UserID', 'type' => 'xsd:string'),
	        'PrintBudget' => array('name' => 'PrintBudget', 'type' => 'xsd:string'),
	        'Reports' => array('name' => 'Reports', 'type' => 'xsd:string'),
	        'ControlPanel' => array('name' => 'ControlPanel', 'type' => 'xsd:string'),
	    )
	);
	function GetUserInfo($appKey, $email) {
		global $portal;
		
		$portal->LogSoapCall(trim(file_get_contents('php://input')));
		
		if(is_array($appKey)) {
			$a = $appKey;
			$appKey = $a['AppKey'];
			$email = $a['Email'];
		}
				
		if($portal->CurrentCompany->SecurityToken != $appKey) {
			return new soap_fault('soap:Client', "Invalid AppKey", "The AppKey that is specified for this client is invalid please contact support.");
		}
		
		// Check if rep exists
		$currentUser = $portal->GetUser(0, $email);
		
		if(!$currentUser || $currentUser->Disabled != 0) {
			$userInfo = array('UserID' => 0, 
								'PrintBudget' => 0,
								'Reports' => 'N',
								'ControlPanel' => 'N');
		} else {
			$controlPanel = ($portal->CheckPriv($currentUser->UserID, 'subadmin') ? 'Y' : 'N');
			$reports = ($portal->CheckPriv($currentUser->UserID, 'report') ? 'Y' : 'N');
			$userInfo = array('UserID' => $currentUser->UserID, 
								'PrintBudget' => $currentUser->PrintBudget,
								'Reports' => $reports,
								'ControlPanel' => $controlPanel); // Disable Control Panel
//								'ControlPanel' => 'N');
			$portal->UpdateUserLastAccess($currentUser->UserID);
		}
		
		return $userInfo;
	}
	// Register the methods to expose
	$server->register('GetUserInfo',                    // method name
	    array('GetUserInfo' => 'tns:GetUserInfo'),          // input parameters
	    array('UserInfo' => 'tns:UserInfo'),    // output parameters
	    'urn:bcondemand',                         // namespace
	    'urn:bcondemand#GetUserInfo',                   // soapaction
	    'document',                                    // style
	    'literal',                                // use
	    'Get the ID and print budget of the user specified by email.'        // documentation
	);
	
	// *************************************
	// *** GetDropDownOptions Structures           ***
	// ************************************* 
	$server->wsdl->addComplexType(
	    'GetDropdownOptions',
	    'complexType',
	    'struct',
	    'sequence',
	    '',
	    array(
	        'AppKey' => array('name' => 'AppKey', 'type' => 'xsd:string'),
	        'Field' => array('name' => 'Field', 'type' => 'xsd:string'),
	    	'Email' => array('name' => 'Email', 'type' => 'xsd:string')
	    )
	);
	$server->wsdl->addComplexType(
	    'DropdownOptions',
	    'complexType',
	    'struct',
	    'sequence',
	    '',
	    array(
	        'DropdownOptions' => array('minOccurs' => '0', 'maxOccurs' => 'unbounded', 'name' => 'DropdownOptions', 'type' => 'tns:DropdownOption' ),
	    )
	);
	$server->wsdl->addComplexType(
	    'DropdownOption',
	    'complexType',
	    'struct',
	    'sequence',
	    '',
	    array(
	    	'DropdownValue' => array('name' => 'DropdownValue', 'type' => 'xsd:string'),
	        'DropdownLabel' => array('name' => 'DropdownLabel', 'type' => 'xsd:string'),
	    )
	);
	function GetDropdownOptions($appKey, $field, $email) {
		global $portal;
		
		if(is_array($appKey)) {
			$a = $appKey;
			$appKey = $a['AppKey'];
			$field = $a['Field'];
			$email = $a['Email'];
		}
				
		if($portal->CurrentCompany->SecurityToken != $appKey) {
			return new soap_fault('soap:Client', "Invalid AppKey", "The AppKey that is specified for this client is invalid please contact support.");
		}
		
		// Check if rep exists
		$currentUser = $portal->GetUser(0, $email);
		if(!$currentUser || $currentUser->Disabled != 0) {
			// Create SoapFault for invalid user
			return new soap_fault('soap:Client', "Invalid User", 'You do not have permission to access this resource. Please contact your supervisor.');
		}
		
		$lookups = $portal->GetUserLookupValues($field, $currentUser->UserID, $currentUser->GroupID);
		
		$optArray = array();
		
		if(sizeof($lookups) > 0) {
			foreach($lookups as $l) {
				if($l->LookupLabel != '')
					$optArray[] = array("DropdownValue" => $l->LookupValue, "DropdownLabel" => $l->LookupLabel);
			}
		} else {
			$optArray[] = array("DropdownValue" => "", "DropdownLabel" => "No options defined.");
		}		 
		
		return array('DropdownOptions' => $optArray);
	}
	// Register the methods to expose
	$server->register('GetDropdownOptions',                    // method name
	    array('GetDropdownOptions' => 'tns:GetDropdownOptions'),          // input parameters
	    array('GetDropdownOptionsResponse' => 'tns:DropdownOptions'),    // output parameters
	    'urn:bcondemand',                         // namespace
	    'urn:bcondemand#GetDropdownOptions',                   // soapaction
	    'document',                                    // style
	    'literal',                                // use
	    'Get the drop down options for the field provided.'        // documentation
	);
	
	// *************************************
	// *** MailingRequest Structures           ***
	// ************************************* 
	$server->wsdl->addComplexType(
	    'MailingRequest',
	    'complexType',
	    'struct',
	    'sequence',
	    '',
	    array(
	        'AppKey' => array('name' => 'AppKey', 'type' => 'xsd:string'),
	        'ContactInfo' => array('name' => 'ContactInfo', 'type' => 'tns:BCODContact'),
	        'RepInfo' => array('name' => 'RepInfo', 'type' => 'tns:BCODRep'),
	        'AppointmentDate' => array('name' => 'AppointmentDate', 'type' => 'xsd:string'),
	        'Segment' => array('name' => 'Segment', 'type' => 'xsd:string'),
	        'Category' => array('name' => 'Category', 'type' => 'xsd:string'),
	        'Delivery' => array('name' => 'Delivery', 'type' => 'xsd:string'),
	        'Activity' => array('name' => 'Activity', 'type' => 'xsd:string')
	    )
	);
	$server->wsdl->addComplexType(
	    'BCODContact',
	    'complexType',
	    'struct',
	    'sequence',
	    '',
	    array(
	        'ID' => array('name' => 'ID', 'type' => 'xsd:string'),
	        'FirstName' => array('name' => 'FirstName', 'type' => 'xsd:string'),
	        'LastName' => array('name' => 'LastName', 'type' => 'xsd:string'),
	        'Title' => array('name' => 'Title', 'type' => 'xsd:string'),
	        'CompanyName' => array('name' => 'CompanyName', 'type' => 'xsd:string'),
	        'Address' => array('name' => 'Address', 'type' => 'xsd:string'),
	        'Address2' => array('name' => 'Address2', 'type' => 'xsd:string'),
	        'City' => array('name' => 'City', 'type' => 'xsd:string'),
	        'State' => array('name' => 'State', 'type' => 'xsd:string'),
	        'Zip' => array('name' => 'Zip', 'type' => 'xsd:string'),
	        'Email' => array('name' => 'Zip', 'type' => 'xsd:string')
	    )
	);
	$server->wsdl->addComplexType(
	    'BCODRep',
	    'complexType',
	    'struct',
	    'sequence',
	    '',
	    array(
	        'FirstName' => array('name' => 'FirstName', 'type' => 'xsd:string'),
	        'LastName' => array('name' => 'LastName', 'type' => 'xsd:string'),
	        'Title' => array('name' => 'Title', 'type' => 'xsd:string'),
	        'Phone' => array('name' => 'Phone', 'type' => 'xsd:string'),
	        'Cell' => array('name' => 'Cell', 'type' => 'xsd:string'),
	        'Fax' => array('name' => 'Fax', 'type' => 'xsd:string'),
	        'Email' => array('name' => 'Email', 'type' => 'xsd:string')
	    )
	);
	$server->wsdl->addComplexType(
	    'MailingRequestStatus',
	    'complexType',
	    'struct',
	    'sequence',
	    '',
	    array(
	    	'Status' => array('name' => 'Status', 'type' => 'xsd:string'),
	        'RequestID' => array('name' => 'RequestID', 'type' => 'xsd:string'),
	        'PrintBudget' => array('name' => 'PrintBudget', 'type' => 'xsd:string'),
	    )
	);
	function MailingRequest($appKey, $bcodContact, $bcodRep, $appDate, $segment, $category, $delivery, $activity) {
		global $portal, $factory, $SALESFORCE_GROUP, $DUPLICATE_SECONDS_RANGE;
		
		$portal->LogSoapCall(trim(file_get_contents('php://input')));
		
		if(is_array($appKey)) {
			$a = $appKey;
			$appKey = $a['AppKey'];
			$bcodContact = $a['ContactInfo'];
			$bcodRep = $a['RepInfo'];
			$appDate = $a['AppointmentDate'];
			$segment = $a['Segment'];
			$category = $a['Category'];
			$delivery = $a['Delivery'];
			$activity = $a['Activity'];
		}
				
		if($portal->CurrentCompany->SecurityToken != $appKey) {
			return new soap_fault('soap:Client', "Invalid AppKey", "The AppKey that is specified for this client is invalid please contact support.");
		}
				
		// Check if rep exists
		$currentUser = $portal->GetUser(0, $bcodRep['Email']);
		$currentGroup = $portal->GetGroup($currentUser->GroupID);
		if(!$currentUser || $currentUser->Disabled != 0) {
			// Create SoapFault for invalid user
			return array('Status' => 'You do not have permission to access this resource. Please contact your supervisor.', 'RequestID' => '0');
		}
		$portal->UpdateUserLastAccess($currentUser->UserID);
		
		// Throw Error if budget is zero
		if($currentUser->PrintBudget <= 0) {
			// Create SoapFault for zero budget
			return array('Status' => 'You have reached your allowed budget. Please contact your supervisor.', 'RequestID' => '0');
		}
						
		$contactEventType = $portal->LookupContactEventType($activity, $currentGroup->GroupID);
		
		if(!$contactEventType) {
			// Create SoapFault for invalid user
			return array('Status' => 'There was a problem creating a contact event for that Lead. Please notify support:' . $currentGroup->GroupID, 'RequestID' => '0');
		}
		
		// Check if contact exists
		$contact = $portal->GetOptimizeContact($bcodContact['ID'], $currentUser->UserID);
				
		if(!$contact) {
			// Create Contact
			$contact = new OptimizeContact();
			
			$contact->UserID	= $currentUser->UserID;
			$contact->ExternalID = $bcodContact['ID'];
			$contact->ListingType = $currentGroup->DefaultSLT;
			$contact->Company 	= $bcodContact['CompanyName'];
			$contact->Title 	= $bcodContact['Title'];
			$contact->FirstName = $bcodContact['FirstName'];
			$contact->LastName 	= $bcodContact['LastName'];
			$contact->Address1 	= $bcodContact['Address'];
			$contact->Address2 	= $bcodContact['Address2'];
			$contact->City 		= $bcodContact['City'];
			$contact->State 	= $bcodContact['State'];
			$contact->Zip 		= $bcodContact['Zip'];
			$contact->Category	= $category;
			$contact->Segment 	= $segment;
			$contact->Email		= $bcodContact['Email'];
			$contact->OtherListingType = 'hot';
						
			$contactID = $portal->AddOptimizeContact($contact);
		} else {			
			$contactID = $contact->SubmissionListingID;
			
			// Check if a duplicate With Creative
//			$testNotes = "Tagline: $tagline\nCreative: $creative\nOffer: $offer\nDelivery: $delivery";
//			if($contactEventType->ContactEventTypeID == $contact->LastContactEvent->ContactEventTypeID
//					&& $contact->LastContactEvent->EventNotes == $testNotes
//					&& strtotime($contact->LastContactEvent->EventCreatedTime) > strtotime("-{$DUPLICATE_SECONDS_RANGE} seconds")) {
//				return array('Status' => 'Success', 'RequestID' => $contactID, 'PrintBudget' => $currentUser->PrintBudget);
//			}
			
			// Check if a duplicate Without Creative
			if(strtotime($contact->LastContactEvent->EventCreatedTime) > strtotime("-{$DUPLICATE_SECONDS_RANGE} seconds")) {
				return array('Status' => 'Success', 'RequestID' => $contactID, 'PrintBudget' => $currentUser->PrintBudget);
			}
			
			$contact->Company 	= $bcodContact['CompanyName'];
			$contact->Title 	= $bcodContact['Title'];
			$contact->FirstName = $bcodContact['FirstName'];
			$contact->LastName 	= $bcodContact['LastName'];
			$contact->Address1 	= $bcodContact['Address'];
			$contact->Address2 	= $bcodContact['Address2'];
			$contact->City 		= $bcodContact['City'];
			$contact->State 	= $bcodContact['State'];
			$contact->Zip 		= $bcodContact['Zip'];
			$contact->Category	= $category;
			$contact->Segment 	= $segment;
			$contact->Email		= $bcodContact['Email'];
			
			$portal->UpdateOptimizeContact($contact);
		}
		
		// Generate Contact Event
		$ceID = $portal->LogContactEvent($contact->SubmissionListingID, $contactEventType->ContactEventTypeID, $currentUser->UserID);
			
		$ce = $portal->GetContactEvent($ceID);
			
		if($ce)
		{
			$ce->EventTime = date("Y-m-d H:i:s", strtotime($appDate));
			$ce->EventNotes = "Category: $category\nSegment: $segment\nDelivery: $delivery";
				
			if(!$portal->UpdateContactEvent($ce))
			{
				return array('Status' => 'There was a problem updating a contact event for that Lead. Please notify support', 'RequestID' => '0');
			}
		}
		else
		{
			return array('Status' => 'There was a problem creating a contact event for that Lead. Please notify support', 'RequestID' => '0');
		}
		
		// Generate mail event
		$cost = $portal->CheckManualTrigger($factory->GetContactWithDetails($contact->SubmissionListingID));
		
		// Calculate new PrintBudget
		$currentUser->PrintBudget = $currentUser->PrintBudget - ($cost * $currentGroup->CostMultiplier);
		
		$portal->UpdateUser($currentUser);
		
		$budget = $currentUser->PrintBudget < 0 ? 0 : $currentUser->PrintBudget;
		
		// Return Status and Mail Event ID	
		return array('Status' => 'Success', 'RequestID' => $contactID, 'PrintBudget' => $budget);	
		
	}
	// Register the methods to expose
	$server->register('MailingRequest',                    // method name
	    array('MailingRequest' => 'tns:MailingRequest'),          // input parameters
	    array('MailingRequestStatus' => 'tns:MailingRequestStatus'),    // output parameters
	    'urn:bcondemand',                         // namespace
	    'urn:bcondemand#MailingRequest',                   // soapaction
	    'document',                                    // style
	    'literal',                                // use
	    'Generates the mailing request for the Contact and Rep provided.'        // documentation
	);
	
	// *************************************
	// *** MultiMailingRequest Structures           ***
	// ************************************* 
	$server->wsdl->addComplexType(
	    'MultiMailingRequest',
	    'complexType',
	    'struct',
	    'sequence',
	    '',
	    array(
	        'AppKey' => array('name' => 'AppKey', 'type' => 'xsd:string'),
	    	'MultiContactInfo' => array('minOccurs' => '1', 'maxOccurs' => 'unbounded', 'name' => 'MultiContactInfo', 'type' => 'tns:BCODContact' ),
	        'RepInfo' => array('name' => 'RepInfo', 'type' => 'tns:BCODRep'),
	        'AppointmentDate' => array('name' => 'AppointmentDate', 'type' => 'xsd:string'),
	        'Segment' => array('name' => 'Segment', 'type' => 'xsd:string'),
	        'Category' => array('name' => 'Category', 'type' => 'xsd:string'),
	        'Delivery' => array('name' => 'Delivery', 'type' => 'xsd:string'),
	        'Activity' => array('name' => 'Activity', 'type' => 'xsd:string')
	    )
	);
	$server->wsdl->addComplexType(
	    'MultiMailingRequestStatus',
	    'complexType',
	    'struct',
	    'sequence',
	    '',
	    array(
	    	'Status' => array('name' => 'Status', 'type' => 'xsd:string'),
	        'RequestID' => array('name' => 'RequestID', 'type' => 'xsd:string'),
	        'PrintBudget' => array('name' => 'PrintBudget', 'type' => 'xsd:string'),
	    )
	);
	function MultiMailingRequest($appKey, $bcodContacts, $bcodRep, $appDate, $segment, $category, $delivery, $activity) {
		global $portal, $factory, $SALESFORCE_GROUP, $DUPLICATE_SECONDS_RANGE;
		
		$portal->LogSoapCall(trim(file_get_contents('php://input')));
		
		if(is_array($appKey)) {
			$a = $appKey;
			$appKey = $a['AppKey'];
			$bcodContacts = $a['MultiContactInfo'];
			$bcodRep = $a['RepInfo'];
			$appDate = $a['AppointmentDate'];
			$segment = $a['Segment'];
			$category = $a['Category'];
			$delivery = $a['Delivery'];
			$activity = $a['Activity'];
		}
				
		if($portal->CurrentCompany->SecurityToken != $appKey) {
			return new soap_fault('soap:Client', "Invalid AppKey", "The AppKey that is specified for this client is invalid please contact support.");
		}
				
		// Check if rep exists
		$currentUser = $portal->GetUser(0, $bcodRep['Email']);
		$currentGroup = $portal->GetGroup($currentUser->GroupID);
		if(!$currentUser || $currentUser->Disabled != 0) {
			// Create SoapFault for invalid user
			return array('Status' => 'You do not have permission to access this resource. Please contact your supervisor.', 'RequestID' => '0');
		}
		$portal->UpdateUserLastAccess($currentUser->UserID);
		
		// Throw Error if budget is zero
		if($currentUser->PrintBudget <= 0) {
			// Create SoapFault for zero budget
			return array('Status' => 'You have reached your allowed budget. Please contact your supervisor.', 'RequestID' => '0');
		}
						
		$contactEventType = $portal->LookupContactEventType($activity, $currentGroup->GroupID);
		
		if(!$contactEventType) {
			// Create SoapFault for invalid user
			return array('Status' => 'There was a problem creating a contact event for that Lead. Please notify support:' . $SALESFORCE_GROUP, 'RequestID' => '0');
		}
		
		$contactIDs = array();
		
		if(isset($bcodContacts['ID'])) {
			$bcodContacts = array($bcodContacts);
		}
		
		// Setup Contacts
		foreach($bcodContacts as $bcodContact) {
		
			// Check if contact exists
			$contact = $portal->GetOptimizeContact($bcodContact['ID'], $currentUser->UserID);
					
			if(!$contact) {
				// Create Contact
				$contact = new TimeWarnerContact();
				
				$contact->UserID	= $currentUser->UserID;
				$contact->ExternalID = $bcodContact['ID'];
				$contact->ListingType = $currentGroup->DefaultSLT;
				$contact->Company 	= $bcodContact['CompanyName'];
				$contact->Title 	= $bcodContact['Title'];
				$contact->FirstName = $bcodContact['FirstName'];
				$contact->LastName 	= $bcodContact['LastName'];
				$contact->Address1 	= $bcodContact['Address'];
				$contact->Address2 	= $bcodContact['Address2'];
				$contact->City 		= $bcodContact['City'];
				$contact->State 	= $bcodContact['State'];
				$contact->Zip 		= $bcodContact['Zip'];
				$contact->Category	= $category;
				$contact->Segment 	= $segment;
				$contact->Email		= $bcodContact['Email'];
				$contact->OtherListingType = 'hot';
							
				$contactID = $portal->AddOptimizeContact($contact);
			} else {			
				$contactID = $contact->SubmissionListingID;
								
				$contact->Company 	= $bcodContact['CompanyName'];
				$contact->Title 	= $bcodContact['Title'];
				$contact->FirstName = $bcodContact['FirstName'];
				$contact->LastName 	= $bcodContact['LastName'];
				$contact->Address1 	= $bcodContact['Address'];
				$contact->Address2 	= $bcodContact['Address2'];
				$contact->City 		= $bcodContact['City'];
				$contact->State 	= $bcodContact['State'];
				$contact->Zip 		= $bcodContact['Zip'];
				$contact->Category	= $category;
				$contact->Segment 	= $segment;
				$contact->Email		= $bcodContact['Email'];
				
				$portal->UpdateOptimizeContact($contact);
			}
			
			// Generate Contact Event
			$ceID = $portal->LogContactEvent($contact->SubmissionListingID, $contactEventType->ContactEventTypeID, $currentUser->UserID);
				
			$ce = $portal->GetContactEvent($ceID);
				
			if($ce)
			{
				$ce->EventTime = date("Y-m-d H:i:s", strtotime($appDate));
				$ce->EventNotes = "Category: $category\nSegment: $segment\nDelivery: $delivery";
					
				if(!$portal->UpdateContactEvent($ce))
				{
					return array('Status' => 'There was a problem updating a contact event for that Lead. Please notify support', 'RequestID' => '0');
				}
			}
			else
			{
				return array('Status' => 'There was a problem creating a contact event for that Lead. Please notify support', 'RequestID' => '0');
			}
			
			$contactIDs[] = $contact->SubmissionListingID;
		}
		
		// Calculate cost of mailing
		$singleCost = $portal->CheckManualTriggerCost($factory->GetContactWithDetails($contactIDs[0]));
		$totalCost = ($singleCost * $currentGroup->CostMultiplier * sizeof($contactIDs));
		
		if($totalCost > $currentUser->PrintBudget) {
			$maxSends = floor($currentUser->PrintBudget / ($singleCost * $currentGroup->CostMultiplier));
			return array('Status' => "You are attempting to send " . sizeof($contactIDs) . " mail pieces. Based upon your available budget and the pieces that was selected, you may only send $maxSends mail pieces.  Please make the necessary adjustments to proceed", 'RequestID' => '0');
		}
		
		foreach($contactIDs as $id) {
				
			// Generate mail event
			$cost = $portal->CheckManualTrigger($factory->GetContactWithDetails($id));
			
			// Calculate new PrintBudget
			$currentUser->PrintBudget = $currentUser->PrintBudget - ($cost * $currentGroup->CostMultiplier);
			
		}
		$portal->UpdateUser($currentUser);
		
		$budget = $currentUser->PrintBudget < 0 ? 0 : $currentUser->PrintBudget;
		
		// Return Status and Mail Event ID	
		return array('Status' => 'Success', 'RequestID' => $contactIDs[0], 'PrintBudget' => $budget);	
		
	}
	// Register the methods to expose
	$server->register('MultiMailingRequest',                    // method name
	    array('MultiMailingRequest' => 'tns:MultiMailingRequest'),          // input parameters
	    array('MultiMailingRequestStatus' => 'tns:MultiMailingRequestStatus'),    // output parameters
	    'urn:bcondemand',                         // namespace
	    'urn:bcondemand#MultiMailingRequest',                   // soapaction
	    'document',                                    // style
	    'literal',                                // use
	    'Generates the mailing requests for the Contacts and Rep provided.'        // documentation
	);
	
	
	
	// Use the request to (try to) invoke the service
	$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
	$server->service($HTTP_RAW_POST_DATA);
?>