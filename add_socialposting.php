<?php
require("../printable/include/mysql.inc.php");
require("../printable/include/optimize.printable.inc.php");
require("globals.php");

$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);

$portal = new OptimizePortal($COMPANY_ID, $db);

$currentUser = $portal->UserAccess($_SESSION['currentuserid']);
$isSubAdmin = $portal->CheckPriv($currentUser->UserID, 'subadmin');

// Check login
if (!$isSubAdmin) {
    header("Location: " . $portal->CurrentCompany->Website . "login.php?message=" . urlencode("Not logged in or login error.  Please try again."));
    die();
}

if ($_POST['Submit'] == 'Save/Create' && $_POST['postingname'] != '') {

    $$posting = new Posting();

    $posting->SocialCategoryID = $_POST['category'];
    $posting->PostingName = $_POST['postingname'];
    $posting->Message = $_POST['message'];
    $posting->ReferenceURL = $_POST['referenceurl'];
    $posting->Tweet = $_POST['tweet'];
    if ($portal->CheckPriv($currentUser->UserID, 'admin')) {
        $posting->GroupID = $_POST['GroupID'];
    }

    if (isset($_POST['facebook'])) {
        $posting->Facebook = 1;
    } else {
        $posting->Facebook = 0;
    }
    if (isset($_POST['linkedin'])) {
        $posting->LinkedIn = 1;
    } else {
        $posting->LinkedIn = 0;
    }
    if (isset($_POST['twitter'])) {
        $posting->Twitter = 1;
    } else {
        $posting->Twitter = 0;
    }

    $pID = $portal->CreateNewPosting($posting, $_GET['section']);

    // if new thumbnail upload
    if ($_FILES['socialimage']['name'] != '') {

        // Copy to output directory
        $tempFileName = $_FILES['socialimage']['tmp_name'];

        // create name for file
        $tName = $_FILES['socialimage']['name'];
        $tPath = $pID . "_$tName";

        echo $tName;
        echo '</br>';
        echo $tPath;

        // move to temp	
        move_uploaded_file($tempFileName, "{$FILES_LOCATION}" . $portal->CurrentCompany->CompanyID . "/social/$tPath");

        $newposting = $portal->GetPosting($pID);
        $newposting->ImageURL = $tPath;

        $portal->UpdatePosting($newposting);
    }



    // direct to edit posting
    header("Location: edit_socialposting.php?postingid=$pID&section=" . $_GET['section']);
    die();
} elseif ($_POST['Submit'] == 'Save/Create' && $_POST['postingname'] == '') {
    $_GET['message'] = "Invalid Posting Name";
} elseif ($_POST['Submit'] == 'Cancel') {
    header("Location: manage_socialpost.php?section=" . $_GET['section']);
    die();
}

$categories = $portal->GetSocialCategories('all', $_GET['section'], true, 'all');
$gs = $portal->GetCompanyGroups();

$groups = array();

$groups[0] = new Group();
$groups[0]->GroupID = 0;
$groups[0]->GroupName = "All";

foreach ($gs as $g) {
    $groups[$g->GroupID] = $g;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <title>
            <?= $portal->CurrentCompany->CompanyName ?> :: Create New Social Posting
        </title>
        <meta http-equiv="content-type" content="text/html;charset=utf-8" />		
        <script  src="js/jquery-1.10.2.min.js"></script>
        <script  src="js/func.js"></script>
        <script  src="js/socialposting.js"></script> 
        <?php include("components/bootstrap.php") ?>
    </head>
    <body bgcolor="#FFFFFF">
        <div id="page">
            <?php include("components/header.php") ?>
            <div id="body">
                <?php
                $CURRENT_PAGE = "Home";
                include("components/navbar.php");
                ?>
                <form method="post" action="<?= $_SERVER['PHP_SELF'] ?>?section=<?= $_GET['section'] ?>" enctype="multipart/form-data" >
                    <?php if (isset($_GET['message'])): ?>
                        <div class="container">
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <?= $_GET['message']; ?>
                            </div>
                        </div>
                    <?php endif; ?> 
                    <div id="AddFileDiv" class="well container">
                        <div class="sectionHeader">
                            <h2>Create New Social Posting</h2>
                        </div>
                        <div class="sectionDiv">
                            <div class="itemSection row">
                                <div id="DescriptionDiv" class="form-group col-md-3">
                                    <label for="postingname">Name:</label><br/>
                                    <input type="text" class="form-control input-sm" name="postingname" value="" size="45" placeholder="Posting Title" required/>
                                </div>
                            </div>
                            <div class="itemSection row">
                                <div id="postto" class="form-group col-md-3">
                                    <label for="postto">Post To:</label><br/>
                                    <input type="checkbox" name="facebook" id="facebookbox" value="1"><img src="http://www.facebook.com/favicon.ico" height="16" width="16"></img>&nbsp;
                                        <input type="checkbox" name="twitter" id="twitterbox" value="1"><img src="http://www.twitter.com/favicon.ico" height="16" width="16"></img>&nbsp;
                                            <input type="checkbox" name="linkedin" id="linkedinbox" value="1"><img src="http://www.linkedin.com/favicon.ico" height="16" width="16"></img>
                                                </div>
                                                </div>
                                                <div class="itemSection row">
                                                    <div id="ReferenceURL" class="form-group col-md-3">
                                                        <label for="referenceurl">Reference URL <span class="info">[<a href="#" title="Reference or source URL, usually a link to a website / page where the information on this post came from. &#10;Will not be included to the post itself.">?</a>]</span> :</label><br/>
                                                        <input id="referenceurl" class="form-control input-sm" type="text" name="referenceurl" value="" size="45" placeholder="URL Reference Link"/>
                                                    </div>
                                                </div>
                                                <div class="itemSection row">
                                                    <div id="Message" class="form-group col-md-6">
                                                        <label for="message">Message:</label>
                                                        <textarea class="form-control input-sm" name="message" id="messagearea" placeholder="Message"></textarea>
                                                    </div>
                                                    <div id="Twitter" class="form-group col-md-6">
                                                        <label class="tweetclass" for="tweet">Twitter (character count: <span id="charcount">0</span>/140):</label>
                                                        <textarea class="tweetclass form-control input-sm" id="tweetarea" name="tweet" placeholder="Tweet Message" maxlength="140"></textarea>
                                                    </div>
                                                </div>
                                                <div class="itemSection row">
                                                    <div id="Image" class="form-group col-md-3">
                                                        <label for="socialimage">Image:</label><br/>
                                                        Upload New: <input type="file" class="form-control input-sm" name="socialimage"/>
                                                    </div>
                                                </div>
                                                <div class="itemSection row">
                                                    <div id="CategoryDiv" class="form-group col-md-3">
                                                        <label for="category">Category:</label><br/>
                                                        <select name="category" class="form-control input-sm" required>
                                                            <option value=""> - Select a category - </option>
                                                            <?php
                                                            $category = new SocialCategory();
                                                            if (sizeof($categories) <= 0) {
                                                                echo '<option value=""> - No Categories - </option>';
                                                            } else {
                                                                foreach ($categories as $category) {
                                                                    if ($portal->CheckPriv($currentUser->UserID, 'admin') || $category->GroupID != 0) {
                                                                        ?>
                                                                        <option value="<?= $category->SocialCategoryID ?>"><?= str_repeat('&nbsp;&nbsp;&nbsp;', $portal->GetSocialCategoryLevel($category->SocialCategoryID)) . $category->CategoryName ?></option>
                                                                        <?php
                                                                    }
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>

                                                <?php
                                                if ($portal->CheckPriv($currentUser->UserID, 'admin')) {
                                                    ?>
                                                    <div class="itemSection row">
                                                        <div id="ParentDiv" class="form-group col-md-3">
                                                            <label for="GroupID">Group:</label><br/>
                                                            <select name="GroupID" class="form-control input-sm" required>
                                                                <?php
                                                                foreach ($groups as $g) {
                                                                    ?>
                                                                    <option value="<?= $g->GroupID ?>">
                                                                        <?= $g->GroupName ?>
                                                                    </option>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <?php
                                                }
                                                ?>
                                                </br>
                                                <div class="itemSection row">
                                                    <div id="Preview" class="form-group col-md-6">
                                                        <label for="preview">Preview:</label><br/>
                                                        <button id="fbbtn" class="btn btn-default btn-sm"><img src="http://www.facebook.com/favicon.ico" height="16" width="16"><img> Preview</button>
                                                                    <button id="ttbtn" class="tweetclass btn btn-default btn-sm"><img src="http://www.twitter.com/favicon.ico" height="16" width="16"><img> Preview</button>
                                                                                <button id="libtn" class="btn btn-default btn-sm"><img src="http://www.linkedin.com/favicon.ico" height="16" width="16"><img> Preview</button>
                                                                                            </div>
                                                                                            </div>
                                                                                            <div class="date-info pull-right">
                                                                                                <small>
                                                                                                Date: <?= date('m/d/Y'); ?>
                                                                                                </small>
                                                                                            </div>
                                                                                            </div>
                                                                                            <center>
                                                                                                <div class="itemSection row">
                                                                                                    <div class="buttonSection">
                                                                                                        <input type="submit" name="Submit" class="btn btn-info btn-sm" value="Save/Create"/>&nbsp;&nbsp;
                                                                                                        <input type="button" value="Cancel" name="Cancel" class="btn btn-default btn-sm" onclick="parent.location = 'manage_socialpost.php?section=<?php echo $_GET['section']; ?>'"/>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </center>
                                                                                            </div>
                                                                                            </form>	
                                                                                            </div>
                                                                                            </div>
                                                                                            <?php include("components/footer.php") ?>
                                                                                            </body>
                                                                                            </html>