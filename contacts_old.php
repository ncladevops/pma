<?php
	require("../printable/include/mysql.inc.php");
	require("../printable/include/optimize.printable.inc.php");
	require("globals.php");
	
	// Create sort Array
  	$viewOptions = array( 'phone' => 'Phone List', 'company' => 'Company List', 'optimize' => 'Optimize List');
  	$sortCols = array( 'phone' => 
  							array('&nbsp;' => 'nosort', 'Full name' => 'submissionlisting.FirstName, submissionlisting.LastName',  'Company' => 'submissionlisting.Company', 'File As' => 'submissionlisting.LastName, submissionlisting.FirstName', 
		  					'BusinessPhone' => 'nosort', 'Business Fax' => 'nosort', 'Home Phone' => 'nosort',
		  					'Mobile Phone'=>'nosort', '' => 'nosort'),
  						'company' =>
  							array( '&nbsp;' => 'nosort', 'Company' => 'submissionlisting.Company', 'Full name' => 'submissionlisting.FirstName, submissionlisting.LastName',   
  							'Business Address' => 'submissionlisting.Address1', 'Work Phone' => 'nosort', 'Fax' => 'nosort', 'MobilePhone' => 'nosort', '' => 'nosort'),
  						'optimize' =>
  							array('&nbsp;' => 'nosort', 'Company' => 'submissionlisting.Company', 'Full name' => 'submissionlisting.FirstName, submissionlisting.LastName',   
  							'Work Phone' => 'nosort', 'events' => 'events', '' => 'nosort')
  					);
  	$searchFields = array( 'First Name' => 'submissionlisting.FirstName', 'Last Name' => 'submissionlisting.LastName',
  							'Business Name' => 'submissionlisting.Company', 'Email' => 'Email',
  							'Work Phone' => 'WorkPhone', 'City' => 'submissionlisting.City',
  							'State' => 'submissionlisting.State', 'Zip' => 'submissionlisting.Zip',
  							'Category' => 'Category', 'Segment' => 'Segment',
  							'Campaign' => 'Campaign');
		
	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	
	$portal = new OptimizePortal($COMPANY_ID, $db);
	
	$currentUser = $portal->GetUser($_SESSION['currentuserid']);
	
	// Check login
	if( !$currentUser ) 
	{
		header( "Location: login.php?message=" . urlencode( "Not logged in or login error.  Please try again." ) );
		die();
  	}
  	
  	if(sizeof($_GET) <= 0)
  	{
  		header("Location: " . $_SERVER['PHP_SELF'] . "?saved=1&" . $_SESSION['lastquery']);
  		die();
  	}
  	elseif (sizeof($_GET) == 1 && isset($_GET['message']) )
  	{
  		header("Location: " . $_SERVER['PHP_SELF'] . "?message=" . $_GET['message']. "&" . $_SESSION['lastquery']);
  		die();
  	}
  	
  	$leadsPerPage = 25;
  	$_GET['offset'] = intval($_GET['offset']) < 0 ? 0 : intval($_GET['offset']);
  	
  	if(!isset($viewOptions[$_GET['view']]))
  	{
  		$_GET['view'] = 'phone';
  	}
  	
  	if(array_search($_GET['sortby'], $sortCols[$_GET['view']]) === false)
	{
		$_GET['sortby'] = "DateAdded DESC";
	}
	
	if($_GET['letterfilter'] != '' && $_GET['letterfilter'] != 'All')
	{
		$letterString = "(submissionlisting.LastName LIKE '" . mysql_real_escape_string(stripslashes(substr($_GET['letterfilter'], 0, 1))) . "%')";
	}
	else
	{
		$letterString = "1";
	}
	if(!isset($searchFields[$_GET['searchfield']]) || strlen($_GET['search']) <= 0)
	{
		$whereString = "1";
	}
	else
	{
		$whereString = $searchFields[$_GET['searchfield']] . " LIKE '%" . mysql_real_escape_string(stripslashes($_GET['search'])) . "%'";
	}
  	
	$userGroup = $portal->GetGroup($currentUser->GroupID);
  	$currentCampaign = $portal->GetSubmissionListingType($userGroup->DefaultSLT);
  	$contacts = $portal->GetOptimizeContacts($currentUser->UserID, $currentCampaign->SubmissionListingTypeID, 
		"$letterString AND $whereString", 
		false, $_GET['sortby'],
		$leadsPerPage, $_GET['offset']);
	$leadcount = $portal->CountOptimizeContacts($currentUser->UserID, $currentCampaign->SubmissionListingTypeID, 
		"$letterString AND $whereString");
	
	$tempGet = $_GET;
	unset($tempGet['message']);
	unset($tempGet['saved']);
	$_SESSION['lastquery'] = build_get_query($tempGet);
	$cets = $portal->GetContactEventTypes($currentUser->GroupID);
  				
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
	<title>
		<?= $portal->CurrentCompany->CompanyName ?> :: My Contacts
	</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" />			
	<link type="text/css" media="all" rel="stylesheet" href="style.css" />		
	<link type="text/css" media="all" rel="stylesheet" href="css/contacts.css" />	
	<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />
	<script src="js/func.js"></script>	
	<script language="JavaScript" type="text/JavaScript">

	var rowCount = <?= intval(sizeof($contacts)) ?>;
	var transactionURL = "printable_trans.php";
	var contactEventRequest;

	function DeleteSelected()
	{
		var idArray = new Array(); 
		for(i = 0; i < rowCount; i++)
		{
			var checkbox = document.getElementById("checkbox_" + i)
			if(checkbox && checkbox.checked)
			{
				idArray.push(checkbox.value);
			}
		}

		if(idArray.length <= 0)
		{
			alert("Please Select at least one record to delete.");
		}
		else
		{
			if(confirm("Are you sure you want to delete all " + idArray.length + " record(s) this cannot be undone."))
			{
				document.location = "deletecontacts.php?delete=" + idArray.join(",");
			}
		}
	}

	function CloneSelected()
	{
		var idArray = new Array(); 
		for(i = 0; i < rowCount; i++)
		{
			var checkbox = document.getElementById("checkbox_" + i)
			if(checkbox && checkbox.checked)
			{
				idArray.push(checkbox.value);
			}
		}

		if(idArray.length != 1)
		{
			alert("Please Select one record to clone.");
		}
		else
		{
			wopen('addcontactsmall.php?clone=' + idArray[0], 'edit_contact', 600, 450);
		}
	}

	function logContactEvent(contactID, eventTypeID)
	{
		
<?php
	foreach($cets as $cet)
	{
?>
		if(document.getElementById("event_" + contactID + "_<?= $cet->ContactEventTypeID ?>"))
			document.getElementById("event_" + contactID + "_<?= $cet->ContactEventTypeID ?>").checked = false;
<?php
	}
?>	
		if(document.getElementById("event_" + contactID + "_" + eventTypeID))
			document.getElementById("event_" + contactID + "_" + eventTypeID).checked = true;
		
		
		contactEventRequest=GetXmlHttpObject()
		if (contactEventRequest==null)
		{
			alert ("Your browser does not support AJAX!");
			return;
		} 
		var url=transactionURL;
		url=url+"?action=LogContactEvent&slid=" + contactID;
		url=url+"&etid="+eventTypeID;
		url=url+"&sid="+Math.random();
		contactEventRequest.onreadystatechange=contactEventStateChanged;
		contactEventRequest.open("GET",url,true);
		contactEventRequest.send(null);
	}

	function contactEventStateChanged()
	{
		if (contactEventRequest.readyState==4)
		{
			var xmlDoc=contactEventRequest.responseXML.documentElement;
			
			// Check if session timedout			
			if(xmlDoc.getElementsByTagName("Status")[0].childNodes[0].nodeValue == 'Timeout')
			{
				parent.location="login.php?logout=true&message='Your+Session+has+timedout.+Please+Login+Again.";
				return;
			}
			if(xmlDoc.getElementsByTagName("Status")[0].childNodes[0].nodeValue == 'Fail')
			{
				alert("Unable to set the Contact Event for that Contact");
			}

<?php
	// Build string of Notes Prompts
	$notesArray = array();
	$notesString = "";
	foreach($cets as $cet)
	{
		if($cet->Notes == 1)
		{
			$notesArray[] = 'xmlDoc.getElementsByTagName("EventTypeID")[0].childNodes[0].nodeValue == "' . $cet->ContactEventTypeID . '"';
		}
	}
	$notesString = implode(" || ", $notesArray);
	if($notesString != '')
	{
?>
			if(<?= $notesString ?>)
			{
				wopen('setnotes.php?ceid=' + xmlDoc.getElementsByTagName("ContactEventID")[0].childNodes[0].nodeValue, 'Set_Notes', 400, 275);
			}
<?php
	}
?>
		}
		
	}

	function ToggleSearch()
	{
		if(document.getElementById("searchBar").style.display == "none")
		{
			document.getElementById("searchBar").style.display = "";
		}
		else
		{
			document.getElementById("searchBar").style.display = "none";
			document.getElementById("search").value = "";
		}
	}
	</script>
</head>
<body bgcolor="#FFFFFF">
<div id="page">
<?php include("components/header.php") ?>
<div id="body">
	<?php 
		$CURRENT_PAGE = "Contacts";
		include("components/navbar.php"); 
		
	?>
    <div id="actionBar">
    <form method="get" action="<?= $_SERVER['PHP_SELF'] ?>">
		<a href="#" class="actionButton"
				onclick="wopen('addcontactsmall.php?listid=<?= $currentCampaign->SubmissionListingTypeID ?>', 'edit_contact', 600, 450); return false;">
			Add New Contact
		</a>
		<a href="#" class="actionButton" onclick="DeleteSelected(); return false;">
			Delete Contacts
		</a>
		<a href="#" class="actionButton"  onclick="wopenscroll('uploadcontacts.php?listid=<?= $currentCampaign->SubmissionListingTypeID ?>', 'upload_contact', 600, 450); return false;">
			Upload Contacts
		</a>
		<a href="#" class="actionButton" onclick="ToggleSearch(); return false;">
			Search
		</a>
		<a href="#" class="actionButton" onclick="CloneSelected(); return false;">
			Clone
		</a>
<?php
	$getTemp = $_GET;
	unset($getTemp['view']);
	$getString = build_get_query($getTemp);
?>
			<span class="actionButtonRight">
	    		View Style:
	    		<select name="view" id="view" onchange="this.form.submit()">
<?php
	foreach($viewOptions as $k=>$v)
	{ 
?>
    				<option value="<?= $k ?>" <?= $k == $_GET['view'] ? "SELECTED" : "" ?>>
						<?= $v ?>
					</option>
<?php
	}
?>
    			</select>
<?php
	foreach($getTemp as $k=>$v)
	{ 
?>
				<input type='hidden' name="<?= $k ?>" value="<?= $v ?>" />
			</span>
<?php 
	}
?>
    </form>
    </div>
    <div id="errorMessage">
<?php
	if ($currentUser->UserID == 39006){
		//var_dump($contacts);
	
	}
	echo $_GET['message'];
	unset($_GET['message']);
?>
	</div>
<?php
	$getTemp = $_GET;
	unset($getTemp['search']);
	unset($getTemp['searchfield']);
	$getString = build_get_query($getTemp);
?>	
	<form method="get" action="<?= $_SERVER['PHP_SELF'] . "?" . $getString ?>>">
		<div id="searchBar" <?= $_GET['search'] == "" ? "style='display: none'" : "" ?>>
			Look for: 
			<input type="text" name="search" id="search" value="<?= $_GET['search'] ?>" />
			in
			<select name="searchfield">
<?php
	foreach($searchFields as $k=>$v)
	{
?>
				<option value="<?= $k ?>" <?= $k == $_GET['searchfield'] ? "SELECTED" : "" ?>>
					<?= $k ?>
				</option>				
<?php
	}
?>
			</select>
	    	<input type="submit" value="Search"/>
	    </div>
	</form>
	<div id="alphabetBar">
<?php
	$getTemp = $_GET;
	$getTemp['letterfilter'] = 'All';
	unset($getTemp['search']);
	unset($getTemp['searchfield']);
	$getString = build_get_query($getTemp);
?>	    			
		<div class="alphabetButton<?= $_GET['letterfilter'] == "All" ? "Selected" : "" ?>"
				onclick="window.location='<?= $_SERVER['PHP_SELF'] . "?" . $getString ?>'">
	    	All
	    </div>
<?php
	$ascii_offset = 65;
	for($i = 0; $i < 26; $i++)
	{
		$getTemp['letterfilter'] = chr($ascii_offset + $i);
		$getString = build_get_query($getTemp);
?>
	    <div class="alphabetButton<?= $_GET['letterfilter'] == chr($ascii_offset + $i) ? "Selected" : "" ?>" 
	    		onclick="window.location='<?= $_SERVER['PHP_SELF'] . "?" . $getString ?>'">
	    	<?= chr($ascii_offset + $i)?>
	    </div>
<?php
	}
	$getTemp['letterfilter'] = 'All';
	$getString = build_get_query($getTemp);
?>
		<div class="alphabetButton<?= $_GET['letterfilter'] == "All" ? "Selected" : "" ?>"
				onclick="window.location='<?= $_SERVER['PHP_SELF'] . "?" . $getString ?>'">
	    	All
	    </div>
	</div>
	<div id="contactsWindow">
		<table class="contactsTable">
			<tr class="contactsTable rowheader">
<?php
	foreach($sortCols[$_GET['view']] as $k=>$v)
	{
		if($v == "events")
		{
			foreach ($cets as $et)
			{
?>
				<th class="contactsTable eventLabel">
					<?= $et->EventTypeShort; ?>
				</th>
<?php
			}
		}
		elseif($v != "nosort" && $_GET['sortby'] != $v)
		{
			$getTemp = $_GET;
			$getTemp['sortby'] = $v;
			$getString = build_get_query($getTemp);
?>
				<th class="contactsTable">
					<a href="<?= $_SERVER['PHP_SELF'] . "?" . $getString ?>">
						<?= $k ?>
					</a>
				</th>
<?php		
		}
		else
		{
?>
				<th class="contactsTable"><?= $k ?></th>
<?php
		}
	}
?>
	    				</tr>
<?php
	if(is_array($contacts))
	{
		$i = 0;
		foreach($contacts as $c)
		{
			$i++;
?>
			<tr class="contactsTable<?= $i%2==0 ? ' roweven' : ' rowodd' ?>">
				<td class="contactsTable checkboxLabel" align="center">
					<input type="checkbox" id="checkbox_<?= $i - 1 ?>" name="checkbox_<?= $i -1  ?>" value="<?= $c->SubmissionListingID; ?>" />
				</td>
<?php
	if($_GET['view'] == 'phone')
	{
?>
				<td class="contactsTable" align="left">
					<?= "$c->FirstName $c->LastName" ?>
				</td>
				<td class="contactsTable" align="left">
					<?= "$c->Company" ?>
				</td>
				<td class="contactsTable" align="left">
					<?= "$c->LastName, $c->FirstName" ?>
				</td>
				<td class="contactsTable" align="left">
					<?= format_phone($c->WorkPhone) ?>
				</td>
				<td class="contactsTable" align="left">
					<?= format_phone($c->Fax) ?>
				</td>
				<td class="contactsTable" align="left">
					<?= format_phone($c->Phone) ?>
				</td>
				<td class="contactsTable" align="left">
					<?= format_phone($c->CellPhone) ?>
				</td>
<?php
	}
	elseif($_GET['view'] == 'company')
	{
?>
				<td class="contactsTable checkboxCell" align="left">
					<?= "$c->Company" ?>
				</td>
				<td class="contactsTable" align="left">
					<?= "$c->FirstName $c->LastName" ?>
				</td>
				<td class="contactsTable" align="left">
					<?= $c->Address1 != "" ? "{$c->Address1}, {$c->City}, {$c->State} {$c->Zip}" : "" ?>
				</td>
				<td class="contactsTable" align="left">
					<?= format_phone($c->WorkPhone) ?>
				</td>
				<td class="contactsTable" align="left">
					<?= format_phone($c->Fax) ?>
				</td>
				<td class="contactsTable" align="left">
					<?= format_phone($c->CellPhone) ?>
				</td>
<?php
	}
	elseif($_GET['view'] == 'optimize')
	{
?>
				<td class="contactsTable" align="left">
					<?= "$c->Company" ?>
				</td>
				<td class="contactsTable" align="left">
					<?= "$c->FirstName $c->LastName" ?>
				</td>
				<td class="contactsTable" align="left">
					<?= format_phone($c->WorkPhone) ?>
				</td>
<?php
		foreach ($cets as $et)
		{
?>
				<td class="contactsTable eventCell">
					<input type="radio" <?= $c->LastContactEvent->ContactEventTypeID == $et->ContactEventTypeID ? 'CHECKED' : '' ?> 
						name="event_<?= $c->SubmissionListingID ?>_<?= $et->ContactEventTypeID ?>" 
						id="event_<?= $c->SubmissionListingID ?>_<?= $et->ContactEventTypeID ?>"
						onclick="logContactEvent('<?= $c->SubmissionListingID ?>', '<?= $et->ContactEventTypeID ?>');" />
				</td>
<?php
		}
	}
?>
				<td class="contactsTable" align="center">
					<a href="#" 
						onclick="wopen('editcontactsmall.php?id=<?= $c->SubmissionListingID ?>', 'edit_contact', 600, 450);"
						title="Edit Contact"><img src="images/edit_icon.gif" border="0" /></a>
					<a href="mailto:<?= $c->Email; ?>" 
						onclick="logContactEvent('<?= $c->SubmissionListingID ?>', '<?= $CONTACTEVENT_EMAIL ?>');"
						title="Send Email"><img src="images/email_icon.gif" border="0" /></a>
					<a href="#" 
						onclick="wopen('view_appointment.php?contactID=<?= $c->SubmissionListingID ?>', 'view_appointment', 450, 450);"
						title="Edit/Add Appointments"><img src="images/appt_icon.gif" border="0" /></a>
<?php
			if($userGroup->GroupName == 'Demo Group') {
?>
					<a href='images/postcards.jpg' target="blank"><img src="images/print_icon.gif" border="0" /></a>
<?php 
			}
?>
				</td>
			</tr>
<?php
		}		
	}
?>
	    </table>
	    <div class="paginationNav"><?= build_page_string($_SERVER['PHP_SELF'], $_GET, $leadsPerPage, $_GET['offset'], $leadcount); ?></div>
	</div>
</div>
</div>
</body>
</html>