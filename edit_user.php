<?php
require("../printable/include/mysql.inc.php");
require("../printable/include/leadsondemand.printable.inc.php");
require("globals.php");
require '../printable/include/component/facebook/facebook.php';
require '../printable/include/component/twitter/codebird.php';
require '../printable/include/component/linkedin/linkedin.php';

$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);

$portal = new LeadsOnDemandPortal($COMPANY_ID, $db);

$currentUser = $portal->UserAccess($_SESSION['currentuserid']);

$isSubAdmin = $portal->CheckPriv($currentUser->UserID, 'subadmin');

if (!$isSubAdmin || !isset($_GET['id'])) {
    $_GET['id'] = $currentUser->UserID;
}

$user = $portal->GetUser($_GET['id']);
$supervisors = $portal->GetSupervisors();
$perms = $portal->GetPermissionList();
$profiles = $portal->GetProfileList();
$regions = $portal->GetCompanyRegions();
$divisions = $portal->GetCompanyDivisions();

if (!$user) {
    header("Location: manage_users.php?message=" . urlencode("Invalid User ID"));
    die();
}

if ($_POST['Submit'] == 'Update' || $_POST['Submit'] == 'Save' || $_POST['Submit'] == 'Submit New User' || $_POST['Submit'] == 'Activate User') {

    if ($isSubAdmin) {
        foreach ($profiles as $prof) {
            if (array_search($prof->ProfileID, $_POST['UserProfiles']) === false)
                $portal->SetProfile($user->UserID, $prof->ProfileID, 0);
            else
                $portal->SetProfile($user->UserID, $prof->ProfileID, 1);
        }
    }

    $origUser = $portal->GetUser($_GET['id']);

    if ($_POST['NewPassword1'] == $_POST['NewPassword2'] && strlen($_POST['NewPassword1']) > 0) {
        $user->Password = $_POST['NewPassword1'];
    }

    if ($_POST['NewPassword1'] != $_POST['NewPassword2'] && strlen($_POST['NewPassword1']) > 0) {
        $_GET['message'] = "Passwords do not match. The account has not been updated.";
    } else {
        if ($isSubAdmin) {
            foreach ($_POST as $k => $v) {
                $user->$k = $v;
            }

            $origPermArray = array();
            $permArray = array();

            foreach ($perms as $perm) {
                if (!is_null($portal->CheckPriv($user->UserID, $perm->PermissionName))) {
                    $origPermArray[$perm->PermissionName] = $portal->CheckPriv($user->UserID, $perm->PermissionName);
                } else {
                    $origPermArray[$perm->PermissionName] = 0;
                }

                if ($perm->PermissionName == 'divisionmanager') {
                    if ($_POST['manager'] == 1) {
                        $permArray[$perm->PermissionName] = 1;
                    } else {
                        $permArray[$perm->PermissionName] = 0;
                    }
                } else if ($perm->PermissionName == 'regionmanager') {
                    if ($_POST['manager'] == 2) {
                        $permArray[$perm->PermissionName] = 1;
                    } else {
                        $permArray[$perm->PermissionName] = 0;
                    }
                } else if ($perm->PermissionName == 'corporatemanager') {
                    if ($_POST['manager'] == 3) {
                        $permArray[$perm->PermissionName] = 1;
                    } else {
                        $permArray[$perm->PermissionName] = 0;
                    }
                } else if ($perm->PermissionName == 'supervisor') {
                    $permArray[$perm->PermissionName] = $_POST['supervisor'];
                } else if (isset($_POST['subadmin']) && $perm->PermissionName == 'subadmin') {
                    $permArray[$perm->PermissionName] = $_POST['subadmin'];
                } else if ($perm->PermissionName == 'report') {
                    $permArray[$perm->PermissionName] = $_POST['report'];
                } else {
                    if (!is_null($portal->CheckPriv($user->UserID, $perm->PermissionName))) {
                        $permArray[$perm->PermissionName] = $portal->CheckPriv($user->UserID, $perm->PermissionName);
                    } else {
                        $permArray[$perm->PermissionName] = 0;
                    }
                }
            }

            $user->CustomProfileAttributes["Phone Toll Free"] = $_POST['PhoneTollFree'];
            $user->CustomProfileAttributes["NMLS Number"] = $_POST['NMLSNumber'];
            $user->CustomProfileAttributes["Extension"] = $_POST['Extension'];
            $user->CustomProfileAttributes["APIPassword"] = $_POST['APIPassword'];

            $portal->UpdateUser($user, $permArray);

            if ($_POST['Submit'] == 'Submit New User' && $user->Disabled != 0) {
                $portal->EmailNewUserCreated($user->UserID, "$currentUser->FirstName $currentUser->LastName");

                $user->RegisteredBy = $currentUser->UserID;
                $user->Disabled = 3;
                $portal->UpdateUser($user);
            }

            if ($user->Disabled == 0) {
                $changes = array();
                $user = $portal->GetUser($_GET['id']);
                foreach ($origUser as $k => $v) {
                    if ($origUser->$k != $user->$k) {
                        $changes[$k] = array('old' => $origUser->$k, 'new' => $user->$k);
                    }
                }

                foreach ($perms as $perm) {
                    if ($origPermArray[$perm->PermissionName] != $permArray[$perm->PermissionName]) {
                        $origPermArray[$perm->PermissionName] == 1 ? $old = "yes" : $old = "no";
                        $permArray[$perm->PermissionName] == 1 ? $new = "yes" : $new = "no";
                        $changes[$perm->PermissionName] = array('old' => $old, 'new' => $new);
                    }
                }
                if (sizeof($changes) > 0) {
                    $super = $portal->GetUser($user->supervisorid);
                    $portal->EmailUserUpdated($user->UserID, $changes, "$currentUser->FirstName $currentUser->LastName", $currentUser->ContactEmail, $super->ContactEmail);
                }
            }

            if ($_POST['Submit'] == 'Activate User' && $user->Disabled == 3) {
                $super = $portal->GetUser($user->supervisorid);
                $creator = $portal->GetUser($user->RegisteredBy);
                $portal->EmailUserActivated($user->UserID, "$creator->FirstName $creator->LastName", "$creator->ContactEmail", "$super->ContactEmail");

                $portal->UpdateUser($user);
                $portal->EnableUser($user->UserID);
            }
        } else {
            $portal->UpdateUser($user);
        }

        if ($isSubAdmin) {
            header('Location: manage_users.php?message=' . urlencode("User updated successfully."));
            die();
        } else {
            header('Location: home.php?message=' . urlencode("Your profile has been updated successfully."));
            die();
        }
    }
} elseif ($_POST['Submit'] == 'Cancel') {
    if ($isSubAdmin) {
        header('Location: manage_users.php?message=' . urlencode("Action Canceled. User not updated."));
        die();
    } else {
        header('Location: home.php?message=' . urlencode("Action Canceled. Profile not updated."));
        die();
    }
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
    <title>
        <?= $portal->CurrentCompany->CompanyName ?> :: User Profile Information
    </title>
    <meta http-equiv="content-type" content="text/html;charset=utf-8"/>
    <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico"/>

    <script language="JavaScript" type="text/JavaScript">
        function initPage() {
            <?= $isSubAdmin ? "" : "DisableInput();" ?>
            <?= $_GET['message'] != "" ? "alert('" . htmlspecialchars($_GET['message']) . "');" : "" ?>
            submitEnabled();
        }

        function DisableInput() {
            var inputs = document.getElementsByTagName('input');

            for (i = 0; i < inputs.length; i++) {
                if (inputs[i].type != 'password' && inputs[i].type != 'submit') {
                    inputs[i].disabled = true;
                }
            }

            var selects = document.getElementsByTagName('select');

            for (i = 0; i < selects.length; i++) {
                selects[i].disabled = true;
            }
        }

        var reqFields = new Array("FirstName", "LastName", "Title", "ContactEmail",
            "Address", "City", "State", "Zipcode", "ContactPhone",
            "OtherAddress", "OtherCity", "OtherState", "OtherZipcode",
            "RegionID", "DivisionID", "supervisorid");

        function submitEnabled() {
            for (field in reqFields) {
                if (document.getElementById(reqFields[field]).value.length == 0) {
                    document.getElementById("userSubmitButton").disabled = true;
                    return;
                }
            }
            if (document.getElementById("userSubmitButton")) {
                document.getElementById("userSubmitButton").disabled = false;
            }
            return;
        }
    </script>

    <?php include("components/bootstrap.php") ?>

</head>
<body bgcolor="#FFFFFF" onload="initPage()">
<div id="page">
    <?php include("components/header.php") ?>
    <div id="body">
        <?php
        $CURRENT_PAGE = "Home";
        include("components/navbar.php");
        ?>
        <form method="post" action="<?= $_SERVER['PHP_SELF'] ?>?id=<?= $user->UserID; ?>" role="form">

            <?php if (isset($_GET['message'])): ?>
                <div class="container">
                    <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <?= $_GET['message']; ?>
                    </div>
                </div>
            <?php endif; ?>

            <div id="UserInfoDiv" class="well container">

                <div class="sectionHeader">
                    <h4>Account Information <?php if ($user->Disabled != 0) { ?>(<span
                            class="alertText">Pending</span>) <?php } ?></h4>
                </div>
                <div class="sectionDiv row container">
                    <fieldset>
                        <div class="itemSection row">
                            <div id="UsernameDiv" class="form-group col-md-3">
                                <label for="Username">Username:</label>
                                <input class="form-control input-sm" type="text" id="Username" name="Username"
                                       value="<?= $user->Username ?>" onchange="submitEnabled();" disabled/>
                            </div>
                            <div id="NewPassword1Div" class="form-group col-md-3 col-md-offset-3 password"
                                 style="display: none;">
                                <label for="NewPassword1">New Password:</label>
                                <input class="form-control input-sm" type="password" id="NewPassword1"
                                       name="NewPassword1" value=""/>
                            </div>
                            <div id="NewPassword2Div" class="form-group col-md-3 password" style="display: none;">
                                <label for="NewPassword2">Confirm Password:</label>
                                <input class="form-control input-sm" type="password" id="NewPassword2"
                                       name="NewPassword2" value=""/>
                            </div>
                            <div id="ChangePassDiv" class="form-group col-md-2 col-md-offset-3">
                                <label>&nbsp;</label>
                                <button id="ChangePassButton" class="form-control btn btn-info">Change Password</button>
                            </div>
                        </div>
                        <div class="itemSection row">
                            <div id="FirstNameDiv" class="form-group col-md-3">
                                <label for="FirstName">First Name: *</label>
                                <input class="form-control input-sm" type="text" id="FirstName" name="FirstName"
                                       value="<?= $user->FirstName; ?>" onchange="submitEnabled();" required/>
                            </div>
                            <div id="LastNameDiv" class="form-group col-md-3">
                                <label for="LastName">Last Name: *</label>
                                <input class="form-control input-sm" type="text" id="LastName" name="LastName"
                                       value="<?= $user->LastName; ?>" onchange="submitEnabled();" required/>
                            </div>

                            <?php $tzones = require 'timezone/timezone_list.php'; ?>

                            <div id="TimezoneDiv" class="form-group col-md-4">
                                <label for="UserTimezoneOffset">User Timezone: *</label>
                                <select class="form-control input-sm" size="1" name="UserTimezoneOffset"
                                        id="UserTimezoneOffset">
                                    <option value="">Select Timezone</option>
                                    <?php foreach ($tzones as $name => $offset): ?>
                                        <option value="<?= $offset ?>" <?php
                                        if ($user->UserTimezoneOffset == $offset) {
                                            echo " SELECTED";
                                        } else if (date_default_timezone_get() == $offset) {
                                            echo " SELECTED";
                                        }
                                        ?>><?= $name ?></option>
                                    <?php endforeach; ?>

                                </select>
                            </div>


                        </div>
                    </fieldset>
                </div>

                <hr class="form-divider">

                <div class="sectionHeader">
                    <h4>Permission Settings <?php if ($user->Disabled != 0) { ?>(<span
                            class="alertText">Pending</span>) <?php } ?></h4>
                </div>
                <div class="sectionDiv row container">
                    <fieldset>
                        <div class="itemSection row">
                            <div id="ReportsDiv" class="form-group col-md-2">
                                <label for="report">Reports:</label>
                                <select class="form-control input-sm" name="report" id="report"
                                        onchange="submitEnabled();" <?= !$isSubAdmin ? "disabled" : "" ?>>
                                    <option value="0">No</option>
                                    <option value="1" <?php
                                    if ($portal->CheckPriv($user->UserID, 'report')) {
                                        echo " SELECTED";
                                    }
                                    ?>>Yes
                                    </option>
                                </select>
                            </div>
                            <div id="ControlPanelDiv" class="form-group col-md-2">
                                <label for="subadmin">Control Panel:</label>
                                <select class="form-control input-sm" name="subadmin" id="subadmin"
                                        onchange="submitEnabled();" <?php
                                if ($currentUser->UserID == $user->UserID) {
                                    echo "disabled";
                                }
                                ?>>
                                    <option value="0">No</option>
                                    <option value="1" <?php
                                    if ($portal->CheckPriv($user->UserID, 'subadmin')) {
                                        echo " SELECTED";
                                    }
                                    ?>>Yes
                                    </option>
                                </select>
                            </div>
                            <div id="ManagerDiv" class="form-group col-md-2">
                                <label for="manager">Manager:</label>
                                <select class="form-control input-sm" name="manager" id="manager"
                                        onchange="submitEnabled();" <?= !$isSubAdmin ? "disabled" : "" ?>>
                                    <option value="0">No</option>
                                    <option value="1" <?php
                                    if ($portal->CheckPriv($user->UserID, 'divisionmanager')) {
                                        echo " SELECTED";
                                    }
                                    ?>>Division
                                    </option>
                                    <option value="2" <?php
                                    if ($portal->CheckPriv($user->UserID, 'regionmanager')) {
                                        echo " SELECTED";
                                    }
                                    ?>>Region
                                    </option>
                                    <option value="3" <?php
                                    if ($portal->CheckPriv($user->UserID, 'corporatemanager')) {
                                        echo " SELECTED";
                                    }
                                    ?>>Corporate
                                    </option>
                                </select>
                            </div>

                            <div id="SupervisorDiv" class="form-group col-md-2">
                                <label for="supervisor">Supervisor:</label>
                                <select class="form-control input-sm" name="supervisor" id="supervisor"
                                        onchange="submitEnabled();" <?= !$isSubAdmin ? "disabled" : "" ?>>
                                    <option value="0">No</option>
                                    <option value="1" <?php
                                    if ($portal->CheckPriv($user->UserID, 'supervisor')) {
                                        echo " SELECTED";
                                    }
                                    ?>>Yes
                                    </option>
                                </select>
                            </div>

                        </div>
                    </fieldset>
                </div>


                <div class="sectionHeader">
                    <h4>Profile Information <?php if ($user->Disabled != 0) { ?>(<span
                            class="alertText">Pending</span>) <?php } ?></h4>
                </div>
                <div class="sectionDiv row container">
                    <div class="itemSection row">
                        <div id="RegionDiv" class="form-group col-md-4">
                            <label for="RegionID">Region: *</label>
                            <select class="form-control input-sm" name="RegionID" id="RegionID"
                                    onchange="submitEnabled();" <?= !$isSubAdmin ? "disabled" : "" ?>>
                                <option value="">Select Region</option>
                                <?php
                                foreach ($regions as $region) {
                                    ?>
                                    <option
                                        value="<?= $region->RegionID ?>"<?php if ($user->RegionID == $region->RegionID) echo " SELECTED" ?> ><?= $region->RegionName ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div id="DivisionDiv" class="form-group col-md-4">
                            <label for="DivisionID">Division: *</label>
                            <select class="form-control input-sm" name="DivisionID" id="DivisionID"
                                    onchange="submitEnabled();" <?= !$isSubAdmin ? "disabled" : "" ?>>
                                <option value="">Select Division</option>
                                <?php
                                foreach ($divisions as $division) {
                                    ?>
                                    <option
                                        value="<?= $division->DivisionID ?>"<?php if ($user->DivisionID == $division->DivisionID) echo " SELECTED" ?>><?= $division->DivisionName ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div id="SupervisorIDDiv" class="form-group col-md-4">
                            <label for="supervisorid">Supervisor: *</label>
                            <select class="form-control input-sm" name="supervisorid" id="supervisorid"
                                    onchange="submitEnabled();" <?= !$isSubAdmin ? "disabled" : "" ?>>
                                <option value="">None</option>
                                <?php
                                foreach ($supervisors as $s) {
                                    ?>
                                    <option
                                        value="<?= $s->UserID ?>" <?= $s->UserID == $user->supervisorid ? "SELECTED" : "" ?>><?= $s->LastName . ", " . $s->FirstName ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="itemSection row">
                        <div id="TitleDiv" class="form-group col-md-3">
                            <label for="Title">Title: *</label>
                            <input class="form-control input-sm" type="text" id="Title" name="Title"
                                   value="<?= $user->Title ?>" onchange="submitEnabled();"/>
                        </div>
                        <div id="ContactEmailDiv" class="form-group col-md-3">
                            <label for="ContactEmail">Email Address: *</label>
                            <input class="form-control input-sm" type="email" id="ContactEmail" name="ContactEmail"
                                   value="<?= $user->ContactEmail; ?>" onchange="submitEnabled();"/>
                        </div>
                        <div id="WebsiteDiv" class="form-group col-md-3">
                            <label for="Website">Website:</label>
                            <input class="form-control input-sm" type="text" id="Website" name="Website"
                                   value="<?= $user->Website; ?>" onchange="submitEnabled();"/>
                        </div>
                        <div id="NMLSNumberDiv" class="form-group col-md-3">
                            <label for="NMLSNumber">Rep ID:</label>
                            <input class="form-control input-sm" type="text" id="NMLSNumber" name="NMLSNumber"
                                   value="<?= $user->CustomProfileAttributes["NMLS Number"]; ?>"
                                   onchange="submitEnabled();"/>
                        </div>
                        <?php
                        if ($isSubAdmin) {
                            ?>
                            <div id="ListingBudgetDiv" class="form-group col-md-3">
                                <label for="ListingBudget">Lead Budget:</label>
                                <input class="form-control input-sm" type="text" id="ListingBudget" name="ListingBudget"
                                       value="<?= $user->ListingBudget; ?>" onchange="submitEnabled();"/>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                    <div class="itemSection row">
                        <div id="AddressDiv" class="form-group col-md-8">
                            <label for="Address">Street Address: *</label>
                            <input class="form-control input-sm" type="text" id="Address" name="Address" class="address"
                                   value="<?= $user->Address; ?>" onchange="submitEnabled();"/>
                        </div>
                    </div>
                    <div class="itemSection row">
                        <div id="CityDiv" class="form-group col-md-4">
                            <label for="City">City: *</label>
                            <input class="address form-control input-sm" type="text" id="City" name="City"
                                   value="<?= $user->City; ?>" onchange="submitEnabled();"/>
                        </div>
                        <div id="StateDiv" class="form-group col-md-2">
                            <label for="State">ST: *</label>
                            <input class="address form-control input-sm" type="text" id="State" name="State"
                                   value="<?= $user->State; ?>" onchange="submitEnabled();"/>
                        </div>
                        <div id="ZipcodeDiv" class="form-group col-md-2">
                            <label for="Zipcode">Zip: *</label>
                            <input class="address form-control input-sm" type="text" id="Zipcode" name="Zipcode"
                                   value="<?= $user->Zipcode; ?>" onchange="submitEnabled();"/>
                        </div>
                    </div>
                    <div class="checkboxSection">
                        <div id="CopyAddressDiv">
                            <input type="checkbox" name="copyaddress" id="copyaddress"></input> <label
                                for="copyaddress"><i>Copy Address</i></label>
                        </div>
                    </div>
                    <div class="itemSection row">
                        <div id="OtherAddressDiv" class="form-group col-md-8">
                            <label for="Address">Return Address: *</label>
                            <input class="returnaddress form-control input-sm" type="text" id="OtherAddress"
                                   name="OtherAddress" value="<?= $user->OtherAddress; ?>" onchange="submitEnabled();"/>
                        </div>
                    </div>
                    <div class="itemSection row">
                        <div id="OtherCityDiv" class="form-group col-md-4">
                            <label for="OtherCity">City: *</label>
                            <input class="returnaddress form-control input-sm" type="text" id="OtherCity"
                                   name="OtherCity" value="<?= $user->OtherCity; ?>" onchange="submitEnabled();"/>
                        </div>
                        <div id="OtherStateDiv" class="form-group col-md-2">
                            <label for="OtherState">ST: *</label>
                            <input class="returnaddress form-control input-sm" type="text" id="OtherState"
                                   name="OtherState" value="<?= $user->OtherState; ?>" onchange="submitEnabled();"/>
                        </div>
                        <div id="OtherZipcodeDiv" class="form-group col-md-2">
                            <label for="OtherZipcode">Zip: *</label>
                            <input class="returnaddress form-control input-sm" type="text" id="OtherZipcode"
                                   name="OtherZipcode" value="<?= $user->OtherZipcode; ?>" onchange="submitEnabled();"/>
                        </div>
                    </div>
                    <div class="itemSection row">
                        <div id="ContactPhoneDiv" class="form-group col-md-2">
                            <label for="ContactPhone">Phone: *</label>
                            <input class="form-control input-sm" type="text" id="ContactPhone" name="ContactPhone"
                                   value="<?= $user->ContactPhone; ?>" onchange="submitEnabled();"/>
                        </div>
                        <div id="ContactCellDiv" class="form-group col-md-2">
                            <label for="ContactCell">Cell Phone:</label>
                            <input class="form-control input-sm" type="text" id="ContactCell" name="ContactCell"
                                   value="<?= $user->ContactCell; ?>" onchange="submitEnabled();"/>
                        </div>
                        <div id="ContactFaxDiv" class="form-group col-md-2">
                            <label for="ContactFax">Fax:</label>
                            <input class="form-control input-sm" type="text" id="ContactFax" name="ContactFax"
                                   value="<?= $user->ContactFax; ?>" onchange="submitEnabled();"/>
                        </div>
                        <div id="PhoneTollFreeDiv" class="form-group col-md-2">
                            <label for="PhoneTollFree">Phone Toll Free:</label>
                            <input class="form-control input-sm" type="text" id="PhoneTollFree" name="PhoneTollFree"
                                   value="<?= $user->CustomProfileAttributes["Phone Toll Free"]; ?>"
                                   onchange="submitEnabled();"/>
                        </div>
                        <div id="ExtensionDiv" class="form-group col-md-2">
                            <label for="Extension">Extension:</label>
                            <input class="form-control input-sm" type="text" id="Extension" name="Extension"
                                   value="<?= $user->CustomProfileAttributes["Extension"]; ?>"
                                   onchange="submitEnabled();"/>
                        </div>
                    </div>
                </div>

                <hr class="form-divider">

                <div class="sectionHeader">
                    <h4>Third Party Tools <?php if ($user->Disabled != 0) { ?>(<span
                            class="alertText">Pending</span>) <?php } ?></h4>
                </div>
                <div class="sectionDiv row container">

                    <div class="itemSection row">

                        <div id="ContactCellDiv" class="form-group col-md-2">
                            <label for="ContactCell">SMS - Twilio Account Sid:</label>
                            <input class="form-control input-sm" type="text" id="TwilioAccountSid"
                                   value="<?= $portal->CurrentCompany->TwilioNumberSid; ?>" disabled/>
                        </div>

                        <div id="ContactCellDiv" class="form-group col-md-2">
                            <label for="ContactCell">SMS - Twilio Phone Number:</label>
                            <input class="form-control input-sm" type="text" id="TwilioPhoneNumber"
                                   value="<?= $portal->CurrentCompany->TwilioNumber; ?>" disabled/>
                        </div>

                        <div id="NMLSNumberDiv" class="form-group col-md-2">
                            <label for="APIPassword">API Password:</label>
                            <input class="form-control input-sm" type="password" id="APIPassword" name="APIPassword"
                                   value="<?= $user->CustomProfileAttributes["APIPassword"]; ?>"
                                   onchange="submitEnabled();"/>
                        </div>

                    </div>


                </div>

                <hr class="form-divider">

                <div class="sectionHeader">
                    <h4>Social Accounts <?php if ($user->Disabled != 0) { ?>(<span
                            class="alertText">Pending</span>) <?php } ?></h4>
                </div>
                <div class="sectionDiv row container">

                    <div class="itemSection container row">
                        <?php
                        $facebook = new Facebook(array(
                            'appId' => $FACEBOOK_APPID,
                            'secret' => $FACEBOOK_SECRET,
                        ));

                        $facebook->setAccessToken($currentUser->FacebookToken);
                        try {
                            $fb_user_profile = $facebook->api('/me', 'GET');
                        } catch (Exception $e) {
                            $fb_user_profile = false;
                        }

                        $loggedin_fb = false;
                        if ($fb_user_profile) {
                            $loggedin_fb = true;
                            $fb_string = $fb_user_profile['name'] . " (<a href='#'>remove</a>)";
                        }
                        ?>
                        <div id="FacebookDiv">
                            <script>
                                function updateFacebookName(name) {
                                    if (name == "") {
                                        document.getElementById('FacebookLoggedInDiv').style.display = "none";
                                        document.getElementById('FacebookLoggedOutDiv').style.display = "";
                                        document.getElementById('FacebookNameSpan').innerHTML = "";
                                    }
                                    else {
                                        document.getElementById('FacebookLoggedInDiv').style.display = "";
                                        document.getElementById('FacebookLoggedOutDiv').style.display = "none";
                                        document.getElementById('FacebookNameSpan').innerHTML = name;
                                    }
                                }
                            </script>
                            <div id="FacebookLoggedInDiv" style="<?= !$loggedin_fb ? "display: none" : "" ?>">
                                <label for="Facebook">Facebook Account:</label> <span
                                    id="FacebookNameSpan"><?= $fb_user_profile['name'] ?></span> (<a href='#' onclick="wopen('link_fb.php?remove=true', 'Link_Social', 400, 300);
                                                return false;">remove</a>)
                            </div>
                            <div id="FacebookLoggedOutDiv" style="<?= $loggedin_fb ? "display: none" : "" ?>">
                                <label for="Facebook">Facebook Account:</label> (<a href='#' onclick="wopen('link_fb.php', 'Link_Social', 400, 300);
                                                return false;">Setup Now</a>)
                            </div>
                        </div>
                        <div id="TwitterDiv">
                            <script>
                                function updateTwitterName(name) {
                                    if (name == "") {
                                        document.getElementById('TwitterLoggedInDiv').style.display = "none";
                                        document.getElementById('TwitterLoggedOutDiv').style.display = "";
                                        document.getElementById('TwitterNameSpan').innerHTML = "";
                                    }
                                    else {
                                        document.getElementById('TwitterLoggedInDiv').style.display = "";
                                        document.getElementById('TwitterLoggedOutDiv').style.display = "none";
                                        document.getElementById('TwitterNameSpan').innerHTML = name;
                                    }
                                }
                            </script>
                            <div id="TwitterLoggedInDiv" style="<?= !$loggedin_tw ? "display: none" : "" ?>">
                                <label for="Twitter">Twitter Account:</label> <span
                                    id="TwitterNameSpan"><?= $_SESSION['twitter_name'] ?></span> (<a href='#' onclick="wopen('link_tw.php?remove=true', 'Link_Social', 400, 300);
                                                return false;">remove</a>)
                            </div>
                            <div id="TwitterLoggedOutDiv" style="<?= $loggedin_tw ? "display: none" : "" ?>">
                                <label for="Twitter">Twitter Account:</label> (<a href='#' onclick="wopen('link_tw.php', 'Link_Social', 400, 300);
                                                return false;">Setup Now</a>)
                            </div>
                        </div>
                        <?php
                        $oauth_callback = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
                        $linkedin = new LinkedIn($LINKEDIN_KEY, $LINKEDIN_SECRET, $oauth_callback);

                        if ($_SESSION['linkedin_name'] == "" && $currentUser->LinkedinToken != "") {
                            list($t, $s) = explode(":", $currentUser->LinkedinToken);
                            $linkedin->access_token = new OAuthConsumer($t, $s, 1);
                            $response = new SimpleXMLElement($linkedin->getProfile("~:(id,first-name,last-name,headline,picture-url)"));

                            // if authorized set session
                            if ($response->status != '401') {
                                $_SESSION['linkedin_name'] = $response->{'first-name'} . " " . $response->{'last-name'};
                            } // else clear credentials
                            else {
                                $currentUser->LinkedinToken = "";
                                $_SESSION['linkedin_name'] = "";
                                $portal->UpdateUser($currentUser);
                            }
                        }

                        $loggedin_li = false;
                        if ($_SESSION['linkedin_name'] != "") {
                            $loggedin_li = true;
                        }
                        ?>
                        <div id="LinkedInDiv">
                            <script>
                                function updateLinkedInName(name) {
                                    if (name == "") {
                                        document.getElementById('LinkedInLoggedInDiv').style.display = "none";
                                        document.getElementById('LinkedInLoggedOutDiv').style.display = "";
                                        document.getElementById('LinkedInNameSpan').innerHTML = "";
                                    }
                                    else {
                                        document.getElementById('LinkedInLoggedInDiv').style.display = "";
                                        document.getElementById('LinkedInLoggedOutDiv').style.display = "none";
                                        document.getElementById('LinkedInNameSpan').innerHTML = name;
                                    }
                                }
                            </script>
                            <div id="LinkedInLoggedInDiv" style="<?= !$loggedin_li ? "display: none" : "" ?>">
                                <label for="LinkedIn"> LinkedIn Account:</label> <span
                                    id="LinkedInNameSpan"><?= $_SESSION['linkedin_name'] ?></span> (<a href='#'
                                                                                                       onclick="wopen('link_li.php?remove=true', 'Link_Social', 400, 300);
                                                return false;">remove</a>)
                            </div>
                            <div id="LinkedInLoggedOutDiv" style="<?= $loggedin_li ? "display: none" : "" ?>">
                                <label for="LinkedIn">LinkedIn Account:</label> (<a href='#' onclick="wopen('link_li.php', 'Link_Social', 400, 300);
                                                return false;">Setup Now</a>)
                            </div>
                        </div>
                    </div>
                </div>

                <hr class="form-divider">

                <div class="sectionHeader">
                    <h4>User Profiles <?php if ($user->Disabled != 0) { ?>(<span
                            class="alertText">Pending</span>) <?php } ?></h4>
                </div>
                <div class="sectionDiv row container">

                    <div class="itemSection container row">

                        <div class="col-md-6">
                            <label for="UserProfiles">Selected Profiles</label>
                            <select class="form-control input-sm" size="1" name="UserProfiles[]"
                                    id="UserProfiles" multiple <?= !$isSubAdmin ? "disabled" : "" ?>>
                                <?php foreach ($profiles as $profile): ?>
                                    <option
                                        value="<?= $profile->ProfileID ?>" <?= $portal->CheckProf($user->UserID, $profile->ProfileID) ? "selected" : "" ?>><?= $profile->ProfileName ?></option>
                                <?php endforeach; ?>

                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="availableprofile">Available Profiles</label>
                            <div class="profile-list">
                                <?php foreach ($profiles as $profile): ?>
                                    <?php if ($portal->CheckProf($user->UserID, $profile->ProfileID)): ?>
                                        <span class="label label-default"><?= $profile->ProfileName ?></span>
                                    <?php else: ?>
                                        <span class="label label-info"><?= $profile->ProfileName ?></span>
                                    <?php endif; ?>
                                    <br/>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>

                </div>

                <hr class="form-divider">

                <center>
                    <div class="itemSection row">
                        <div class="buttonSection">
                            <?php if ($user->Disabled > 0): ?>
                                <input class="btn btn-info btn-sm" type="submit" value="Save"
                                       name="Submit"/>&nbsp;&nbsp;
                            <?php else: ?>
                                <input class="btn btn-info btn-sm" type="submit" value="Update"
                                       name="Submit"/>&nbsp;&nbsp;
                            <?php endif; ?>
                            <?php
                            if ($isSubAdmin) {
                                if ($user->Disabled == 2) {
                                    ?>
                                    <input class="btn btn-info btn-sm" type="submit" value="Submit New User"
                                           name="Submit" id="userSubmitButton"/>&nbsp;&nbsp;
                                    <?php
                                }
                                if ($user->Disabled == 3 && $portal->CheckPriv($currentUser->UserID, 'admin')) {
                                    ?>
                                    <input class="btn btn-info btn-sm" type="submit" value="Activate User" name="Submit"
                                           id="userActivateButton "/>&nbsp;&nbsp;
                                    <?php
                                }
                            }
                            ?>

                            <?php
                            if ($isSubAdmin) {
                                $redirecturl = "manage_users.php?message=" . urlencode("Action Canceled. User not updated.");
                            } else {
                                $redirecturl = "home.php?message=" . urlencode("Action Canceled. Profile not updated.");
                            }
                            ?>


                            <button id="CancelButton" value="Cancel" class="btn btn-default btn-sm"
                                    onclick="location.href = '<?= $redirecturl ?>'">Cancel
                            </button>
                        </div>
                    </div>
                </center>


            </div>
    </div>
    </form>
</div>
<?php include("components/footer.php") ?>
</body>
</html>