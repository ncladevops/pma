<?php
	require("../printable/include/mysql.inc.php");
	require("../printable/include/printable.inc.php");
	require("globals.php");
	require("sso.php");
		
	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	
	$portal = new PrintablePortal($COMPANY_ID, $db);
			
	$currentUser = $portal->GetUser($_SESSION['currentuserid']);
	$isAdmin = $portal->CheckPriv($currentUser->UserID, 'admin');
		
	// Check login
	if( !$currentUser ) 
	{
		header( "Location: login.php?message=" . urlencode( "Not logged in or login error.  Please try again." ) );
		die();
  	}
	
	if($isAdmin && $_GET['userid'] != '') {
		$currentUser = $portal->GetUser($_GET['userid']);
	}

  	$login_row = $portal->GetUsersLogins($HTTP_GET_VARS['loginid'], $currentUser->UserID);
  	
	// Querry to check available logins
	if(!$login_row)
	{
		header( "Location: listing.php?message=" . urlencode( "Store login error.  Please try again." ) );
	}
	
	// Check to see if this store requires an agreement
	$sqlstring = "SELECT 
						*
					FROM 
						storeagreement
					WHERE 
						StoreID = " . $login_row['StoreID'] . ";";
					
	$storeagreement_result = $portal->mysqlDB->Query( $sqlstring );
	
	if(isset($_GET['productid']))
	{
		$productID = $_GET['productid'];
	}
	else 
	{
		$productID = 'none';
	}
	
	// if no agreement than just forward the person
	if(mysql_num_rows($storeagreement_result) <= 0)
	{
		if($SSO)
		{
			$login_string = SingleSignOn($SSO_URL, $SSO_TOKEN, trim($login_row['LoginName'], "\xA0"), $STORE_PW, $portal->CurrentCompany->CompanyID, $db->dblink, true, $currentUser, $productID);
			
			if($login_string == 'Fault')
			{
				header( "Location: listing.php?message=" . urlencode( "Store login error.  Please Contact Support." ) );
				die();
			}
			//echo $login_string;
			header( "Location: $login_string");
			die();
		}
		else 
		{
			$login_string = "Location: http://www2.printable.com/customers/welcome.asp?id=" . 
								trim($login_row['LoginName'], "\xA0") . "&pw=$STORE_PW";
			//echo $login_string;
			header( $login_string );
		}
		
	}
	
	$storeagreement_row = mysql_fetch_array( $storeagreement_result );
					
	// If accept button was pushed add aggreement	
	if(isset($HTTP_POST_VARS['Accept']))
  	{
  		$sqlstring = "INSERT INTO 
						agreement 
							(StoreAgreementID,
							LoginID,
							Date)
						VALUES
							(" . $storeagreement_row['StoreAgreementID']. ",
							" . $login_row['LoginID'] . ",
							'" . date("Y/m/d") . "');";
							
		$aggreement_result = $portal->mysqlDB->Query( $sqlstring )
			or die( "<h1>Errors retieving account information.</h1><br>\n" );
  	}
  	
  	// If decline send back to store 
  	if(isset($HTTP_POST_VARS['Decline']))
  	{
  		header( "Location: listing.php" );
  	}
  	
  	// Check to see if they have already agreed
  	$sqlstring  = "SELECT * 
					FROM agreement 
					WHERE StoreAgreementID='" . $storeagreement_row['StoreAgreementID'] . "' 
					AND LoginID ='" . $login_row['LoginID'] . "'";
	
	$aggreement_result = $portal->mysqlDB->Query( $sqlstring )
			or die( "<h1>Errors retieving account information.</h1><br>\n" );
		
	if(mysql_num_rows($aggreement_result) > 0)
	{
		$login_string = "Location: http://www2.printable.com/customers/welcome.asp?id=" . 
							trim($login_row['LoginName'], "\xA0") . "&pw=lehmansbf";
		//echo $login_string;
		header( $login_string );
		die();
	}
		
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<title>Terms of Use
</title>


<link href="style.css" rel="stylesheet" type="text/css">
<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />
</head>

<body>
<table width="600" border="2" align="center" cellpadding="0" cellspacing="0" bordercolor="#CCCCCC">
  <tr>
    <td background="images/table_bg.gif"><table width="596"  border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td align="left" valign="top"><img src="images/banner.gif" alt="Gateway Title"></td>
        </tr>
    </table>
      <table width="90%"  border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td align="center" valign="top"><p class="center"><h3>AGREEMENT TITLE</h3></p>
            <p class="justify">Put text of Agreement here.</p>
            <form name="form1" method="post" action='redirect.php?loginid=<?= $HTTP_GET_VARS['loginid'];  ?>'>
              <table width="400"  border="0" cellpadding="6" cellspacing="0">
                  <td><input type="submit" name="Accept" value="Accept"></td>
                  <td><input type="submit" name="Decline" value="Decline"></td>
                </tr>
              </table>
            </form>
          </td>
        </tr>
      </table>
    <p class="center">&nbsp;</p></td>
  </tr>
  <tr>
    <td background="images/purple.gif"><p class="bottom">&nbsp;</p>
    	<p class="bottom">� 2005 121 Communications </p></td>
  </tr>
</table>
</body>
</html>