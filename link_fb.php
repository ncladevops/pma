<?php
	require("../printable/include/mysql.inc.php");
	require("../printable/include/optimize.printable.inc.php");
	require("globals.php");
	require '../printable/include/component/facebook/facebook.php';
	
	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	
	$portal = new OptimizePortal($COMPANY_ID, $db);
	
	$currentUser = $portal->UserAccess($_SESSION['currentuserid']);
	$isSuper = $portal->CheckPriv($currentUser->UserID, 'supervisor');
			
	if(!$currentUser)
	{
		die("Not logged in or login error.");
	}
	
	if($_GET['remove'] == 'true') {
		$currentUser->FacebookToken  = "";
		$portal->UpdateUser($currentUser);
?>
<html>
<head><title>Remove Facebook Link</title></head>
<body onload="window.opener.updateFacebookName(''); window.close();">
<a href="#" onclick="window.close()">Click here to close the window</a>
</body>
</html>
<?php
		die();
	}
	
	$facebook = new Facebook(array(
	  'appId'  => $FACEBOOK_APPID,
	  'secret' => $FACEBOOK_SECRET,
	));	
	
	if($facebook->getUserAccessToken()) {
		$currentUser->FacebookToken = $facebook->getUserAccessToken();
		$portal->UpdateUser($currentUser);
		$facebook->setAccessToken($currentUser->FacebookToken);
	}
	else {
		$facebook->setAccessToken($currentUser->FacebookToken);
	}
		
	$fb_user_id = $facebook->getUserFromAccessToken();
	
	try {
		$fb_user_profile = $facebook->api('/me', 'GET');
	} catch(FacebookApiException $e) {	
		$fb_user_id = 0;
	}
	
	if(!$fb_user_id) {
		$login_url = $facebook->getLoginUrl( array(
				   'scope' => 'publish_stream'
				   )); 
		header("Location: $login_url");
	}
?>
<html>
<head><title>Create Facebook Link</title></head>
<body onload="window.opener.updateFacebookName('<?= $fb_user_profile['name'] ?>'); window.close();">
<a href="#" onclick="window.close()">Click here to close the window</a>
</body>
</html>