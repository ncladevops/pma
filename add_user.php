<?php
require("../printable/include/mysql.inc.php");
require("../printable/include/optimize.printable.inc.php");
require("globals.php");

$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);

$portal = new OptimizePortal($COMPANY_ID, $db);

$currentUser = $portal->UserAccess($_SESSION['currentuserid']);

$isSubAdmin = $portal->CheckPriv($currentUser->UserID, 'subadmin');

$supervisors = $portal->GetSupervisors();
$perms = $portal->GetPermissionList();
$profiles = $portal->GetProfileList();
$regions = $portal->GetCompanyRegions();
$divisions = $portal->GetCompanyDivisions();

if (!$isSubAdmin) {
    die("Accessed Denied.");
}

$userExists = false;
if ($_POST['Submit'] == 'Save' || $_POST['Submit'] == 'Submit New User') {

    if (strlen($_POST['Username']) <= 0) {
        die("The Username Field is required.");
    }
    // Check if user exists
    $user = $portal->GetUser(0, $_POST['Username']);
    if (!$user) {
        $user = new User();
        $user->Username = $_POST['Username'];
        $user->FirstName = $_POST['FirstName'];
        $user->LastName = $_POST['LastName'];

        $portal->AddUser($user, array(), array(26));

        $user = $portal->GetUser(0, $_POST['Username']);

        foreach ($_POST as $k => $v) {
            $user->$k = $v;
        }

        //$user->Disabled = 2; used when create form was separated

        $permArray = array();

        foreach ($perms as $perm) {
            if ($perm->PermissionName == 'divisionmanager') {
                if ($_POST['manager'] == 1) {
                    $permArray[$perm->PermissionName] = 1;
                } else {
                    $permArray[$perm->PermissionName] = 0;
                }
            } else if ($perm->PermissionName == 'regionmanager') {
                if ($_POST['manager'] == 2) {
                    $permArray[$perm->PermissionName] = 1;
                } else {
                    $permArray[$perm->PermissionName] = 0;
                }
            } else if ($perm->PermissionName == 'corporatemanager') {
                if ($_POST['manager'] == 3) {
                    $permArray[$perm->PermissionName] = 1;
                } else {
                    $permArray[$perm->PermissionName] = 0;
                }
            } else if ($perm->PermissionName == 'supervisor') {
                $permArray[$perm->PermissionName] = $_POST['supervisor'];
            } else if ($perm->PermissionName == 'subadmin') {
                $permArray[$perm->PermissionName] = $_POST['subadmin'];
            } else if ($perm->PermissionName == 'report') {
                $permArray[$perm->PermissionName] = $_POST['report'];
            } else {
                $permArray[$perm->PermissionName] = $portal->CheckPriv($user->UserID, $perm->PermissionName);
            }
        }

        $user->CustomProfileAttributes["Phone Toll Free"] = $_POST['PhoneTollFree'];
        $user->CustomProfileAttributes["NMLS Number"] = $_POST['NMLSNumber'];
        $user->CustomProfileAttributes["Extension"] = $_POST['Extension'];

        if ($_POST['Submit'] == 'Save') {
            $user->Disabled = 2;
        } else if ($_POST['Submit'] == 'Submit New User') {
            $portal->EmailNewUserCreated($user->UserID, "$currentUser->FirstName $currentUser->LastName");
            $user->RegisteredBy = $currentUser->UserID;
            $user->Disabled = 3;
        }


        $portal->UpdateUser($user, $permArray);

        if ($_POST['Submit'] == 'Save') {
            //header("Location: edit_user.php?id=$user->UserID");
            header('Location: manage_users.php?message=' . urlencode("User saving successful."));
            die();
        } else if ($_POST['Submit'] == 'Submit New User') {
            header('Location: manage_users.php?message=' . urlencode("User submission successful."));
            die();
        }
    } else {
        $userExists = true;
    }
} elseif ($_POST['Submit'] == 'Cancel') {
    header("Location: manage_users.php?message=" . urlencode("Action Canceled"));
    die();
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <title>
            <?= $portal->CurrentCompany->CompanyName ?> :: Add User
        </title>
        <meta http-equiv="content-type" content="text/html;charset=utf-8" />		
        <script type="text/javascript">

            function initPage()
            {
<?= $_GET['message'] != "" ? "alert('" . htmlspecialchars($_GET['message']) . "');" : "" ?>
                submitEnabled();
            }

            var reqFields = new Array("FirstName", "LastName", "Title", "ContactEmail",
                    "Address", "City", "State", "Zipcode", "ContactPhone",
                    "OtherAddress", "OtherCity", "OtherState", "OtherZipcode", "NMLSNumber",
                    "RegionID", "DivisionID", "supervisorid");

            function submitEnabled() {
                for (field in reqFields) {
                    if (document.getElementById(reqFields[field]).value.length == 0) {
                        document.getElementById("userSubmitButton").disabled = true;
                        return;
                    }
                }
                if (document.getElementById("userSubmitButton")) {
                    document.getElementById("userSubmitButton").disabled = false;
                }
                return;
            }
        </script>
        <?php include("components/bootstrap.php") ?>
    </head>
    <body bgcolor="#FFFFFF" onload="initPage()">
        <div id="page">
            <?php include("components/header.php") ?>
            <div id="body">
                <?php
                $CURRENT_PAGE = "Home";
                include("components/navbar.php");
                ?>
                <form method="post" action="<?= $_SERVER['PHP_SELF'] ?>?id=<?= $user->UserID; ?>" role="form">

                    <?php if (isset($_GET['message'])): ?>
                        <div class="container">
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <?= $_GET['message']; ?>
                            </div>
                        </div>
                    <?php endif; ?> 

                    <div id="UserInfoDiv" class="well container">
                        <div class="sectionHeader">
                            <h4>New User Info</h4>
                        </div>
                        <?php
                        if ($userExists) {
                            ?>
                            <div>
                                The account you are attempting to create (<b><?= $user->Username ?></b>) has already been initiated.
                                <ul>
                                    <li><a href="edit_user.php?id=<?= $user->UserID ?>">Click here to edit/view the account</a></li>
                                    <li><a href="add_user.php">Click here to enter a different Username.</a></li>
                                    <li><a href="manage_users.php">Click here to go back to the User Control Panel.</a></li>
                                </ul>
                            </div>		
                            <?php
                        } else {
                            ?>
                            <div class="sectionDiv row container">
                                <fieldset>
                                    <div class="itemSection row">
                                        <div id="UsernameDiv" class="form-group col-md-3">
                                            <label for="Username">Username/Email: *</label>
                                            <input class="form-control input-sm" type="text" id="Username" name="Username" placeholder="Username" required onchange="submitEnabled();"/>
                                        </div>
                                        <div id="NewPassword1Div" class="form-group col-md-offset-3 col-md-6">
                                            <label for="Password">Default Password:</label></br>
                                            <span style="font-style: italic; font-size: 10px;"><?= $DEFAULT_PASSWORD ?></span>
                                            <input class="form-control input-sm" type="hidden" id="Password" name="Password" style="display: none;" value="<?= $DEFAULT_PASSWORD ?>" onchange="submitEnabled();"/>
                                        </div>
                                    </div>
                                    <div class="itemSection row">
                                        <div id="FirstNameDiv" class="form-group col-md-3">
                                            <label for="FirstName">First Name: *</label>
                                            <input class="form-control input-sm" type="text" id="FirstName" name="FirstName" placeholder="First Name" required onchange="submitEnabled();"/>
                                        </div>
                                        <div id="LastNameDiv" class="form-group col-md-3">
                                            <label for="LastName">Last Name: *</label>
                                            <input class="form-control input-sm" type="text" id="LastName" name="LastName" placeholder="Last Name" required onchange="submitEnabled();"/>
                                        </div>

                                        <?php $tzones = require 'timezone/timezone_list.php'; ?>

                                        <div id="TimezoneDiv" class="form-group col-md-4">
                                            <label for="UserTimezoneOffset">User Timezone: *</label>
                                            <select class="form-control input-sm" size="1" name="UserTimezoneOffset" id="UserTimezoneOffset" onchange="submitEnabled();">
                                                <option value="">Select Timezone</option>
                                                <?php foreach ($tzones as $name => $offset): ?>
                                                    <option value="<?= $offset ?>" <?php
                                                    if ($user->UserTimezoneOffset == $offset) {
                                                        echo " SELECTED";
                                                    } else if (date_default_timezone_get() == $offset) {
                                                        echo " SELECTED";
                                                    }
                                                    ?>><?= $name ?></option>
                                                        <?php endforeach; ?>

                                            </select>  
                                        </div>


                                    </div>
                                </fieldset>
                            </div>

                            <hr class="form-divider">

                            <div class="sectionHeader">
                                <h4>Permission Settings <?php if ($user->Disabled != 0) { ?>(<span class="alertText">Pending</span>) <?php } ?></h4>
                            </div>
                            <div class="sectionDiv container row">
                                <fieldset>
                                    <div class="itemSection row">
                                        <div id="ReportsDiv" class="form-group col-md-2">
                                            <label for="report">Reports:</label>
                                            <select class="form-control input-sm" name="report" id="report">
                                                <option value="0">No</option>
                                                <option value="1" <?php
                                                if ($portal->CheckPriv($user->UserID, 'report')) {
                                                    echo " SELECTED";
                                                }
                                                ?>>Yes</option>
                                            </select>
                                        </div>
                                        <div id="ControlPanelDiv" class="form-group col-md-2">
                                            <label for="subadmin">Control Panel:</label>
                                            <select class="form-control input-sm" name="subadmin" id="subadmin" <?php
                                            if ($currentUser->UserID == $user->UserID) {
                                                echo "disabled";
                                            }
                                            ?>>
                                                <option value="0">No</option>
                                                <option value="1" <?php
                                                if ($portal->CheckPriv($user->UserID, 'subadmin')) {
                                                    echo " SELECTED";
                                                }
                                                ?>>Yes</option>
                                            </select>
                                        </div>
                                        <div id="ManagerDiv" class="form-group col-md-2">
                                            <label for="manager">Manager:</label>
                                            <select class="form-control input-sm" name="manager" id="manager">
                                                <option value="0">No</option>
                                                <option value="1" <?php
                                                if ($portal->CheckPriv($user->UserID, 'divisionmanager')) {
                                                    echo " SELECTED";
                                                }
                                                ?>>Division</option>
                                                <option value="2" <?php
                                                if ($portal->CheckPriv($user->UserID, 'regionmanager')) {
                                                    echo " SELECTED";
                                                }
                                                ?>>Region</option>
                                                <option value="3" <?php
                                                if ($portal->CheckPriv($user->UserID, 'corporatemanager')) {
                                                    echo " SELECTED";
                                                }
                                                ?>>Corporate</option>
                                            </select>
                                        </div>

                                        <div id="SupervisorDiv" class="form-group col-md-2">
                                            <label for="supervisor">Supervisor:</label>
                                            <select class="form-control input-sm" name="supervisor" id="supervisor">
                                                <option value="0">No</option>
                                                <option value="1" <?php
                                                if ($portal->CheckPriv($user->UserID, 'supervisor')) {
                                                    echo " SELECTED";
                                                }
                                                ?>>Yes</option>
                                            </select>
                                        </div>

                                    </div>
                                </fieldset>
                            </div>

                            <hr class="form-divider">

                            <div class="sectionHeader">
                                <h4>Profile Information <?php if ($user->Disabled != 0) { ?>(<span class="alertText">Pending</span>) <?php } ?></h4>
                            </div>
                            <div class="sectionDiv container row">
                                <div class="itemSection row">
                                    <div id="RegionDiv" class="form-group col-md-4">
                                        <label for="RegionID">Region: *</label>
                                        <select class="form-control input-sm" name="RegionID" id="RegionID">
                                            <option value="">Select Region</option>
                                            <?php
                                            foreach ($regions as $region) {
                                                ?>
                                                <option value="<?= $region->RegionID ?>"<?php if ($user->RegionID == $region->RegionID) echo " SELECTED" ?> onchange="submitEnabled();"><?= $region->RegionName ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div id="DivisionDiv" class="form-group col-md-4">
                                        <label for="DivisionID">Division: *</label>
                                        <select class="form-control input-sm" name="DivisionID" id="DivisionID" onchange="submitEnabled();">
                                            <option value="">Select Division</option>
                                            <?php
                                            foreach ($divisions as $division) {
                                                ?>
                                                <option value="<?= $division->DivisionID ?>"<?php if ($user->DivisionID == $division->DivisionID) echo " SELECTED" ?>><?= $division->DivisionName ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div id="SupervisorIDDiv" class="form-group col-md-4">
                                        <label for="supervisorid">Supervisor: *</label>
                                        <select class="form-control input-sm" name="supervisorid" id="supervisorid" onchange="submitEnabled();">
                                            <option value="">None</option>
                                            <?php
                                            foreach ($supervisors as $s) {
                                                ?>
                                                <option value="<?= $s->UserID ?>" <?= $s->UserID == $user->supervisorid ? "SELECTED" : "" ?>><?= $s->LastName . ", " . $s->FirstName ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="itemSection row">
                                    <div id="TitleDiv" class="form-group col-md-3">
                                        <label for="Title">Title: *</label>
                                        <input class="form-control input-sm" type="text" id="Title" name="Title" placeholder="Title / Position" onchange="submitEnabled();"/>
                                    </div>
                                    <div id="ContactEmailDiv" class="form-group col-md-3">
                                        <label for="ContactEmail">Email Address: *</label>
                                        <input class="form-control input-sm" type="text" id="ContactEmail" name="ContactEmail" placeholder="Email Address" onchange="submitEnabled();"/>
                                    </div>
                                    <div id="NMLSNumberDiv" class="form-group col-md-3">
                                        <label for="NMLSNumber">Rep ID: *</label>
                                        <input class="form-control input-sm" type="text" id="NMLSNumber" name="NMLSNumber"  placeholder="Rep ID Number:" onchange="submitEnabled();"/>
                                    </div>
                                    <?php
                                    if ($isSubAdmin) {
                                        ?>
                                        <div id="ListingBudgetDiv" class="form-group col-md-3">
                                            <label for="ListingBudget">Lead Budget:</label>
                                            <input class="form-control input-sm" type="text" id="ListingBudget" name="ListingBudget" placeholder="Lead Budget" />
                                        </div>
                                        <?php
                                    }
                                    ?>
                                </div>
                                <div class="itemSection row">
                                    <div id="AddressDiv" class="form-group col-md-8">
                                        <label for="Address">Street Address: *</label> 
                                        <input class="form-control input-sm address" type="text" id="Address" name="Address" placeholder="Street Address" onchange="submitEnabled();"/>
                                    </div>
                                </div>
                                <div class="itemSection row">
                                    <div id="CityDiv" class="form-group col-md-4">
                                        <label for="City">City: *</label>
                                        <input class="form-control input-sm address" type="text" id="City" name="City" placeholder="City" onchange="submitEnabled();"/>
                                    </div>
                                    <div id="StateDiv" class="form-group col-md-2">
                                        <label for="State">ST: *</label>
                                        <input class="form-control input-sm address" type="text" id="State" name="State" placeholder="State" onchange="submitEnabled();"/>
                                    </div>
                                    <div id="ZipcodeDiv" class="form-group col-md-2">
                                        <label for="Zipcode">Zip: *</label>
                                        <input class="form-control input-sm address" type="text" id="Zipcode" name="Zipcode" placeholder="Zip Code"  onchange="submitEnabled();"/>
                                    </div>
                                </div>
                                <div class="checkboxSection">
                                    <div id="CopyAddressDiv">
                                        <input type="checkbox" name="copyaddress" id="copyaddress"></input> <label for="copyaddress"><i>Copy Address</i></label>
                                    </div>
                                </div>
                                <div class="itemSection row">
                                    <div id="OtherAddressDiv" class="form-group col-md-8">
                                        <label for="Address">Return Address: *</label>
                                        <input class="form-control input-sm returnaddress" type="text" id="OtherAddress" name="OtherAddress" placeholder="Return Address"  onchange="submitEnabled();"/>
                                    </div>
                                </div>
                                <div class="itemSection row">
                                    <div id="OtherCityDiv" class="form-group col-md-4">
                                        <label for="OtherCity">City: *</label>
                                        <input class="form-control input-sm returnaddress" type="text" id="OtherCity" name="OtherCity" placeholder="Return City" onchange="submitEnabled();"/>
                                    </div>
                                    <div id="OtherStateDiv" class="form-group col-md-2">
                                        <label for="OtherState">ST: *</label>
                                        <input class="form-control input-sm returnaddress" type="text" id="OtherState" name="OtherState" placeholder="Return State" onchange="submitEnabled();"/>
                                    </div>
                                    <div id="OtherZipcodeDiv" class="form-group col-md-2">
                                        <label for="OtherZipcode">Zip: *</label>
                                        <input class="form-control input-sm returnaddress" type="text" id="OtherZipcode" name="OtherZipcode" placeholder="Return Zip Code" onchange="submitEnabled();"/>
                                    </div>
                                </div>
                                <div class="itemSection row">
                                    <div id="ContactPhoneDiv" class="form-group col-md-2">
                                        <label for="ContactPhone">Phone: *</label>
                                        <input class="form-control input-sm" type="text" id="ContactPhone" name="ContactPhone" placeholder="Phone Number"  onchange="submitEnabled();"/>
                                    </div>
                                    <div id="ContactCellDiv" class="form-group col-md-2">
                                        <label for="ContactCell">Cell Phone:</label>
                                        <input class="form-control input-sm" type="text" id="ContactCell" name="ContactCell" placeholder="Mobile Phone" />
                                    </div>
                                    <div id="ContactFaxDiv" class="form-group col-md-2">
                                        <label for="ContactFax">Fax:</label>
                                        <input class="form-control input-sm" type="text" id="ContactFax" name="ContactFax" placeholder="Fax Number" />
                                    </div> 
                                    <div id="PhoneTollFreeDiv" class="form-group col-md-2">
                                        <label for="PhoneTollFree">Phone Toll Free:</label>
                                        <input class="form-control input-sm" type="text" id="PhoneTollFree" name="PhoneTollFree" placeholder="Phone Toll Free" />
                                    </div>
                                    <div id="ExtensionDiv" class="form-group col-md-2">
                                        <label for="Extension">Extension:</label>
                                        <input class="form-control input-sm" type="text" id="Extension" name="Extension" placeholder="Phone Extension" />
                                    </div>
                                </div>
                            </div>

                            <hr class="form-divider">

                            <center>
                                <div class="itemSection row">
                                    <div class="buttonSection">
                                        <input class="btn btn-default btn-sm" type="submit" value="Save" name="Submit"/>&nbsp;&nbsp;
                                        <input class="btn btn-info btn-sm" type="submit" value="Submit New User" name="Submit" id="userSubmitButton" />&nbsp;&nbsp;
                                        <button id="CancelButton" type="button" value="Cancel" class="btn btn-default btn-sm" onclick="location.href = 'manage_users.php?message=<?= urlencode("Action Canceled") ?>'">Cancel</button>
                                    </div>
                                </div>
                            </center>
                            <?php
                        }
                        ?>
                    </div>
                </form>
            </div>
        </div>
        <?php include("components/footer.php") ?>
    </body>
</html>