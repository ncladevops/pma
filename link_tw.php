<?php
	require("../printable/include/mysql.inc.php");
	require("../printable/include/optimize.printable.inc.php");
	require("globals.php");
	require '../printable/include/component/twitter/codebird.php';
	
	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	
	$portal = new OptimizePortal($COMPANY_ID, $db);
	
	$currentUser = $portal->UserAccess($_SESSION['currentuserid']);
	$isSuper = $portal->CheckPriv($currentUser->UserID, 'supervisor');
			
	if(!$currentUser)
	{
		die("Not logged in or login error.");
	}
	
	if($_GET['remove'] == 'true') {
		$currentUser->TwitterToken  = "";
		$_SESSION['twitter_name'] = "";
		unset($_SESSION['twitter_oauth_token']);
		unset($_SESSION['twitter_oauth_token_secret']);
		$portal->UpdateUser($currentUser);
?>
<html>
<head><title>Remove Twitter Link</title></head>
<body onload="window.opener.updateTwitterName(''); window.close();">
<a href="#" onclick="window.close()">Click here to close the window</a>
</body>
</html>
<?php
		die();
	}
	
	Codebird::setConsumerKey($TWITTER_KEY, $TWITTER_SECRET);	
	$cb = Codebird::getInstance();
	
	if (!isset($_SESSION['twitter_oauth_token'])) {
		if(strlen($currentUser->TwitterToken) <= 0) {
			// get the request token
			$reply = $cb->oauth_requestToken(array(
				'oauth_callback' => 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']
			));

			// store the token
			$cb->setToken($reply->oauth_token, $reply->oauth_token_secret);
			$_SESSION['twitter_oauth_token'] = $reply->oauth_token;
			$_SESSION['twitter_oauth_token_secret'] = $reply->oauth_token_secret;
			$_SESSION['twitter_oauth_verify'] = true;

			// redirect to auth website
			$auth_url = $cb->oauth_authorize();
			header('Location: ' . $auth_url);
			die();
		} else {
			// Authorize User
			list($t,$s) = explode(":",$currentUser->TwitterToken);
			$cb->setToken($t, $s);
			$reply = $cb->account_verifyCredentials();
			
			// if authorized set session
			if($reply->httpstatus != '401') {
				$_SESSION['twitter_oauth_token'] = $t;
				$_SESSION['twitter_oauth_token_secret'] = $s;
				$_SESSION['twitter_name'] = $reply->name;
			}
			// else clear credentials
			else {
				unset($_SESSION['twitter_oauth_token']);
				unset($_SESSION['twitter_oauth_token_secret']);
				$_SESSION['twitter_name'] = "";
				$currentUser->TwitterToken = "";
				$portal->UpdateUser($currentUser);
			}
			
			header('Location: ' . basename(__FILE__));
			die();
		}

	} elseif (isset($_GET['oauth_verifier']) && isset($_SESSION['twitter_oauth_verify'])) {
		// verify the token
		$cb->setToken($_SESSION['twitter_oauth_token'], $_SESSION['twitter_oauth_token_secret']);
		unset($_SESSION['twitter_oauth_verify']);

		// get the access token
		$reply = $cb->oauth_accessToken(array(
			'oauth_verifier' => $_GET['oauth_verifier']
		));

		// store the token (which is different from the request token!)
		$_SESSION['twitter_oauth_token'] = $reply->oauth_token;
		$_SESSION['twitter_oauth_token_secret'] = $reply->oauth_token_secret;
		
		// store the token with the user
		$currentUser->TwitterToken = $reply->oauth_token . ":" . $reply->oauth_token_secret;
		$portal->UpdateUser($currentUser);
		
		list($t,$s) = explode(":",$currentUser->TwitterToken);
		$cb->setToken($t, $s);
		$reply = $cb->account_verifyCredentials();// if authorized set session
		if($reply->httpstatus != '401') {
			$_SESSION['twitter_name'] = $reply->name;
		}
		// else clear credentials
		else {
			unset($_SESSION['twitter_oauth_token']);
			unset($_SESSION['twitter_oauth_token_secret']);
			$_SESSION['twitter_name'] = "";
			$currentUser->TwitterToken = "";
			$portal->UpdateUser($currentUser);
		}

		// send to same URL, without oauth GET parameters
		header('Location: ' . basename(__FILE__));
		die();
	} elseif(isset($_SESSION['twitter_oauth_token']) && $_SESSION['twitter_name'] == "") {
		unset($_SESSION['twitter_oauth_token']);
		unset($_SESSION['twitter_oauth_token_secret']);
	}
?>
<html>
<head><title>Create Twitter Link</title></head>
<body onload="window.opener.updateTwitterName('<?= $_SESSION['twitter_name'] ?>'); window.close();">
<a href="#" onclick="window.close()">Click here to close the window</a>
</body>
</html>