<?php
require("../printable/include/mysql.inc.php");
require("../printable/include/leadsondemand.printable.inc.php");
require("../printable/include/optimize.printable.inc.php");
require("globals.php");

$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);

$portal = new OptimizePortal($COMPANY_ID, $db);
$portallod = new LeadsOnDemandPortal($COMPANY_ID, $db);

$currentUser = $portal->GetUser($_SESSION['currentuserid']);
$currentGroup = $portal->GetGroup($currentUser->GroupID);
$isSubAdmin = $portal->CheckPriv($currentUser->UserID, 'subadmin');

if (!$currentUser) {
    header("Location: login.php?message=" . urlencode("Not logged in or login error."));
    die();
}

$lead = $portal->GetOptimizeContact($_GET['id'], 'all');
if ($lead->ContactStatusTypeID == $NEW_STATUSTYPE_ID) {
    //reassign to action changer
    if ($lead->UserID != $currentUser->UserID) {
        $lead->UserID = $currentUser->UserID;
        $portal->UpdateOptimizeContact($lead);
    }
}

//Lead's current Contact Status
$currentCST = $portal->GetContactStatusType($lead->ContactStatusTypeID);

echo "<!-- LT: $lead->ListingType Def: $currentGroup->DefaultSLT -->";

$close = false;

if ($_POST['Submit'] == 'Add') {
    $cet = $portal->GetContactEventType($_POST['ContactEventTypeID']);

    if ($cet) {

        $ceID = $portal->LogContactEvent($lead->SubmissionListingID, $cet->ContactEventTypeID, $currentUser->UserID);

        $ce = $portal->GetContactEvent($ceID);

        if ($ce) {
            $ce->EventTime = date("Y-m-d H:i:s", strtotime($_POST['EventTime']));
            $ce->EventNotes = $_POST['EventNotes'];

            if ($portal->UpdateContactEvent($ce)) {
                $close = true;
                $newLead = $portal->GetOptimizeContact($_GET['id'], 'all');
                $portal->CheckFieldTrigger($lead, $newLead);

                //Greenlink Custom - Shark Tank
                if ($ce->ContactEventTypeID == 423) {
                    $lead->ListingType = 922; //Campaign: Shark Tank
                    $lead->ContactStatusTypeID = $LEADSTORE_STATUSID;
                    $lead->UserID = $LEADSTORE_USERID;
                } else if ($ce->ContactEventTypeID == 424) {
                    //Greenlink Custom - Open Pool
                    $lead->ListingType = 921; //Campaign: Open Pool
                    $lead->ContactStatusTypeID = $LEADSTORE_STATUSID;
                    $lead->UserID = $LEADSTORE_USERID;
                }


                //Check if event status is set
                if ($cet->EventStatus != 0 && $cet->EventStatus != null) {

                    $cst = $portal->GetContactStatusType($cet->EventStatus);

                    $lead->ContactStatusTypeID = $cet->EventStatus;

                    //Greenlink Custom - Welcome Packet Completed
                    if ($lead->ContactStatusTypeID == 666) {
                        $lead->FundedDate = date('Y-m-d'); //Set Funded Date to current date
                    }

                    //Greenlink Custom - Welcome Packet
                    if (isset($_POST['Mortgage1Balance']) && $_POST['Mortgage1Balance'] != null) {
                        $lead->Mortgage1Balance = $_POST['Mortgage1Balance'];
                    }

                    //Greenlink Custom - Update 2nd Voice Manager
                    if ($cet->ContactEventTypeID == 339) {
                        /* Contact Event: 2nd Voice Completed */
                        if ($lead->UserID == $currentUser->UserID) {
                            //If event change made by current lead owner
                            $lead->SecondVoiceManager = "2nd Voice By Agent";
                        } else {
                            //Get current user's name
                            $lead->SecondVoiceManager = $currentUser->FirstName . ' ' . $currentUser->LastName;
                        }

                    }

                    //Check for Status custom instructions
                    if ($cst->ReassignUserID != 0) {
                        $lead->UserID = $cst->ReassignUserID;
                    }
                    if ($cst->EmailUserID != 0) {
                        $portallod->EmailContactStatusChange($lead->SubmissionListingID, $cst->EmailUserID);
                    }
                }

                $portal->UpdateOptimizeContact($lead);

            } else {
                $_GET['message'] = "Can't update event!";
            }
        } else {
            $_GET['message'] = "Can't add event!";
        }
    } else {
        $_GET['message'] = "Invalid Event Type";
    }
}

$currentGroup = $portal->GetGroup($currentUser->GroupID);
//Ignore Association Events if Group is Management (105) and Administrator (109) from Greenlink
if ($currentCST->AssociateEvents == true && ($currentGroup->GroupID != 105 && $currentGroup->GroupID != 109)) {
    $statusEventTypes = $portal->GetStatusEventList($currentCST->ContactStatusTypeID, $currentGroup->GroupID);
    $contactEventList = $statusEventTypes;
} else {
    $contactEventTypes = $portal->GetContactEventTypes($currentGroup->GroupID, ($lead->ListingType == $currentGroup->DefaultSLT ? 2 : 1), true);
    $contactEventList = $contactEventTypes;
}
echo "<!-- LT: $lead->ListingType Def: $currentGroup->DefaultSLT -->";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title>
        <?= $portal->CurrentCompany->CompanyName ?> :: Add Action for <?= "$lead->FirstName $lead->LastName" ?>
    </title>
    <meta http-equiv="content-type" content="text/html;charset=utf-8"/>
    <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico"/>

    <?php include("components/bootstrap.php") ?>

    <script language="JavaScript" type="text/JavaScript">
        function updateParent(id, actionName, appointment) {
            if (window.opener.updateContactEvent) {
                window.opener.updateContactEvent(id, actionName);
                window.opener.timedRefresh(1);
                if (appointment == 1 || appointment == true) {
                    window.opener.appointmentPopup(id);
                }
                /* Add popup - on Welcome Call Ready status */
                if (actionName == "Contract Received" || actionName == "Signing Rescheduled Welcome") {
                    window.opener.alert('Welcome Call Transfer Must be Completed by a Manager.');
                }
            }
            window.close();
        }
    </script>

</head>
<body
    onload="<?= $close == true ? "updateParent('$lead->SubmissionListingID','$cet->EventType', $cet->Appointment)" : "" ?>">
<form method="post" action="<?= $_SERVER['PHP_SELF'] ?>?id=<?= $lead->SubmissionListingID; ?>">
    <div class="sectionHeader">
        <h3>Add Action</h3>
    </div>
    <div id="AddActionDiv" class="sectionDiv well">
        <div class="itemSection">
            <div id="ContactNameDiv">
                <label for="ContactName">Lead Name:</label><br/>
                <?= "$lead->FirstName $lead->LastName" ?>
            </div>
            <div id="EventTimeDiv">
                <label for="EventTime">Time:</label><br/>
                <?= date("m/d/Y g:i a") ?><input type="hidden" name="EventTime" value="<?= date("Y-m-d H:i:s") ?>"/>
            </div>
        </div>
        <div class="itemSection">
            <div id="ContactEventTypeDiv">
                <label for="ContactEventTypeID">Action:</label><br/>
                <select id="contactEventSelection" class="eventSelection form-control input-sm"
                        name="ContactEventTypeID" required>
                    <option value=""></option>
                    <?php foreach ($contactEventList as $cet): ?>
                        <option value="<?= $cet->ContactEventTypeID ?>">
                            <?= $cet->EventType ?>
                        </option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
        <div class="longItemSection">
            <div id="EventNotesDiv">
                <label for="EventNotes">Notes:</label><br/>
                <textarea id="EventNoteTextArea" name="EventNotes" rows="8" style="width: 95%;" required></textarea>
            </div>
        </div>
        <div id="customFieldDiv" class="form-group" style="display: none;">
            <label for="Title" id="customFieldLabel">Custom Field</label>
            <input type="text" class="form-control input-sm" id="customField" name=""
                   value="" placeholder="Custom Field"/>
        </div>
    </div>
    <div class="itemSection">
        <div class="buttonSection">
            <input id="FormSubmitButton" type="submit" value="Add" class="btn btn-primary" name="Submit" disabled/>&nbsp;&nbsp;<input
                type="button"
                value="Cancel"
                class="btn btn-default"
                onclick="window.close()"/>
        </div>
    </div>
</form>

<?php include("components/bootstrap-footer.php") ?>

<script type="text/javascript">

    /* Field checker making sure both Notes and Action is not empty before the submission */

    var eventVal = null;
    var noteVal = null;
    var customField = null;
    var customFieldVal = null;

    function checkField() {
        if ((eventVal === '' || eventVal === null) || (noteVal === null || noteVal === '') || (customField === true && (customFieldVal === null || customFieldVal === ''))) {
            $('#FormSubmitButton').attr('disabled', 'disabled');
        } else {
            $('#FormSubmitButton').removeAttr('disabled');
        }
    }

    function checkCustomField() {
        /* Check if Welcome Packet */
        if (eventVal == 352) {
            //set custom field to cap unsecured amount
            $('#customFieldDiv').show();
            $('#customFieldLabel').html('CAP Unsecured Debt Amount:');
            $('#customField').attr('name', 'Mortgage1Balance');
            $('#customField').attr('placeholder', 'Estimated Credit Debt');
            customField = true;
        } else {
            //reset custom field div
            $('#customFieldDiv').hide();
            $('#customFieldLabel').html('Custom Field');
            $('#customField').attr('name', '');
            $('#customField').attr('placeholder', 'Custom Field');
            $('#customField').val('');
            customField = null;
            customFieldVal = null;
        }
    }

    // A $( document ).ready() block.
    $(document).ready(function () {
        $('.eventSelection').on('change', function (evt, params) {
            eventVal = params['selected'];
            checkCustomField();
            checkField();
        });
        $(document).on('keyup', '#EventNoteTextArea', function () {
            noteVal = $(this).val();
            checkField();
        });
        $(document).on('change', '#EventNoteTextArea', function () {
            noteVal = $(this).val();
            checkField();
        });
        $(document).on('keyup', '#customField', function () {
            customFieldVal = $(this).val();
            checkField();
        });
        $(document).on('change', '#customField', function () {
            customFieldVal = $(this).val();
            checkField();
        });
    });
</script>

</body>
</html>