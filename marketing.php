<?php
	require("../printable/include/mysql.inc.php");
	require("../printable/include/optimize.printable.inc.php");
	require("globals.php");

	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	
	$portal = new OptimizePortal($COMPANY_ID, $db);
	
	$currentUser = $portal->UserAccess($_SESSION['currentuserid']);
	$isSuper = $portal->CheckPriv($currentUser->UserID, 'supervisor');
			
	if(!$currentUser)
	{
		header("Location: login.php?message=" .urlencode("Not logged in or login error."));
		die();
	} else {
		$logins = $portal->GetUserLogins($_SESSION['currentuserid']);
	}

	/*
	Marcom Central on iframe
	Modified by: Nikko
	Date: 6/12/2014
	Original code: n/a
	Requested by: John
	*/


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
	<title>
		<?= $portal->CurrentCompany->CompanyName ?> :: Marketing onDemand
	</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />	
	<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
	</head>	
<body style="min-height: 800px; min-width: 800px; height: 100%; width: 100%;">
<iframe id="marketing-frame" onload="frameload()" src="redirect.php?loginid=<?= $logins[$MAIN_STORE_ID]->LoginID; ?>" width="100%" height="100%" frameborder="0" scrolling="auto" style="min-height: 800px; min-width: 800px; height: 100%; width: 100%;"></iframe>
</body>


<script type="text/javascript">

function frameload(){
    if($('#marketing-frame').contents().find('#quickbar'))
	{
		top.location.replace('home.php');
	}
}

</script>

</html>