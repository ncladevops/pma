<?php
require("../printable/include/mysql.inc.php");
require("../printable/include/leadsondemand.printable.inc.php");
require("globals.php");


$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);

$portal = new LeadsOnDemandPortal($COMPANY_ID, $db);

$currentUser = $portal->UserAccess($_SESSION['currentuserid']);

$camps = $portal->GetSubmissionListingTypes(0, 'SortOrder DESC, SubmissionListingTypeID', 'all', "((submissionlistingtype.UserManageable = 1 AND submissionlistingtype.SubmissionListingSourceID = 9999) || submissionlistingtype.UserManageable = 0) && Disabled = 0");

if (!$currentUser) {
    header("Location: login.php?message=" . urlencode("Not logged in or login error."));
    die();
}

if ($_POST['Submit'] == 'Add Lead' && $_POST['LastName'] != "") {
    $contact = new LeadsOnDemandContact();

    $contact->FirstName = $_POST['FirstName'];
    $contact->LastName = $_POST['LastName'];
    $contact->ListingType = $_POST['ListingType'];
    $contact->OtherListingType = $_POST['OtherListingType'];
    $contact->UserID = $currentUser->UserID;
    $contact->ContactStatusTypeID = $CONTACTED_STATUSTYPE_ID;

    $slID = $portal->AddLeadsOnDemandContact($contact);

    $contact->ContactStatusTypeID = $CONTACTED_STATUSTYPE_ID;
    $portal->UpdateLeadsOnDemandContact($contact);

    if (!$slID) {
        header("Location: home.php?message=" . urlencode("Error adding lead."));
        die();
    }

    header("Location: edit_lead.php?id=$slID");
    die();
} elseif ($_GET['id'] != "") {
    $lead = $portal->GetLeadsOnDemandContact($_GET['id'], 'all');

    if (!$lead) {
        header("Location: list_lead.php?message=" . urlencode("Invalid Lead Id."));
        die();
    }

    switch ($ADDSEARCH_OPTIONS[$_GET['type']]['Type']) {
        case "Search":
            $lead->ContactStatusTypeID = $CONTACTED_STATUSTYPE_ID;
            $lead->ListingType = $ADDSEARCH_OPTIONS[$_GET['type']]['ListingType'];
            $lead->UserID = $currentUser->UserID;
            $lead->DateAdded = date("Y-m-d H:i:s");
            break;
        case "Transfer":
            if ($lead->ContactStatusTypeID != $ADDSEARCH_OPTIONS[$_GET['type']]['TransferStatusID']) {
                header("Location: list_lead.php?message=" . urlencode("Invalid Lead Id."));
                die();
            }
            $lead->ContactStatusTypeID = $CONTACTED_STATUSTYPE_ID;
            $lead->UserID = $currentUser->UserID;
            break;
        case "Store":
            if ($lead->ContactStatusTypeID != $ADDSEARCH_OPTIONS[$_GET['type']]['StoreStatusID']) {
                header("Location: list_lead.php?message=" . urlencode("Invalid Lead Id."));
                die();
            }
            $lead->ContactStatusTypeID = $ATTEMPTING_STATUSTYPE_ID;
            $lead->UserID = $currentUser->UserID;
            break;
        default:
            break;
    }

    $portal->UpdateLeadsOnDemandContact($lead);

    header("Location: edit_lead.php?id={$lead->SubmissionListingID}");
    die();

} elseif ($_GET['multileadids'] != "") {
    $leadIDs = explode(",", $_GET['multileadids']);

    foreach ($leadIDs as $id) {
        $lead = $portal->GetLeadsOnDemandContact($id, 'all');

        switch ($ADDSEARCH_OPTIONS[$_GET['type']]['Type']) {
            case "Search":
                $lead->ContactStatusTypeID = $CONTACTED_STATUSTYPE_ID;
                $lead->ListingType = $ADDSEARCH_OPTIONS[$_GET['type']]['ListingType'];
                $lead->UserID = $currentUser->UserID;
                $lead->DateAdded = date("Y-m-d H:i:s");
                break;
            case "Transfer":
                if ($lead->ContactStatusTypeID != $ADDSEARCH_OPTIONS[$_GET['type']]['TransferStatusID']) {
                    header("Location: list_lead.php?message=" . urlencode("Invalid Lead Id."));
                    die();
                }
                $lead->ContactStatusTypeID = $CONTACTED_STATUSTYPE_ID;
                $lead->UserID = $currentUser->UserID;
                break;
            case "Store":
                if ($lead->ContactStatusTypeID == $ADDSEARCH_OPTIONS[$_GET['type']]['StoreStatusID']) {
                    $lead->ContactStatusTypeID = $CONTACTED_STATUSTYPE_ID;
                    $lead->UserID = $currentUser->UserID;
                }
                break;
            default:
                break;
        }

        $portal->UpdateLeadsOnDemandContact($lead);
    }

    header("Location: list_lead.php?message=" . urlencode("Leads transferred successfully"));
    die();

} elseif ($_POST['Submit'] == 'Cancel') {
    header("Location: list_lead.php?message=" . urlencode("Action Canceled. No lead added."));
    die();
}

$slts = $portal->GetSubmissionListingTypes(0, 'SortOrder, SubmissionListingTypeID', 'all', 'UserManageable = 1 AND submissionlistingtype.SubmissionListingSourceID = 9999')


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title>
        <?= $portal->CurrentCompany->CompanyName ?> :: Leads on Demand :: Add New Lead
    </title>
    <meta http-equiv="content-type" content="text/html;charset=utf-8"/>
    <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico"/>
    <!---// load jQuery from the GoogleAPIs CDN //--->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js" type="text/javascript"></script>
    <script src="js/click2call.js"></script>
    <script language="JavaScript" type="text/JavaScript">
        var search_types = new Array();
        <?php
            foreach($ADDSEARCH_OPTIONS as $aso=>$aso_options)
            {
        ?>
        search_types["<?= $aso ?>"] = "<?= $aso_options['Type'] ?>";
        <?php
            }
        ?>
        function toggleSearch(value) {
            if (search_types[value] == "Store") {
                document.getElementById("dateSearchDiv").style.display = "";
                document.getElementById("advancedSearchDiv").style.display = "";
                document.getElementById("standardSearchDiv").style.display = "";
            } else if (search_types[value] == "Transfer") {
                document.getElementById("dateSearchDiv").style.display = "none";
                document.getElementById("advancedSearchDiv").style.display = "none";
                document.getElementById("standardSearchDiv").style.display = "none";
            }
            else {
                document.getElementById("dateSearchDiv").style.display = "none";
                document.getElementById("advancedSearchDiv").style.display = "none";
                document.getElementById("standardSearchDiv").style.display = "";
            }

        }

        var search_transactionURL = "printable_trans.php";
        var searchRequest;

        function searchLeads(start) {

            if ($('#sltid').val() == '' && $("#slid").val() == '' && $("#email").val() == '' && $("#phone").val() == '' &&
                $("#lname").val() == '' && $("#eid").val() == '' && $("#State").val() == '') {
                alert('Please enter at least 1 search criteria.');
                return false;
            }

            document.getElementById("searchResultsDiv").innerHTML = "<img src='images/loading.gif' /><br/>Searching...";

            searchRequest = GetXmlHttpObject();
            if (searchRequest == null) {
                alert("Your browser does not support AJAX!");
                return;
            }
            var url = search_transactionURL;
            url = url + "?action=GetAddLeadTable";
            url = url + "&SearchType=" + encodeURI(document.getElementById("SearchType").value);
            url = url + "&slid=" + encodeURI(document.getElementById("slid").value);
            url = url + "&email=" + encodeURI(document.getElementById("email").value);
            url = url + "&phone=" + encodeURI(document.getElementById("phone").value);
            url = url + "&lname=" + encodeURI(document.getElementById("lname").value);
            url = url + "&eid=" + encodeURI(document.getElementById("eid").value);
            url = url + "&State=" + encodeURI(document.getElementById("State").value);
            //url=url+"&ListingClass="+encodeURI(document.getElementById("ListingClass").value);
            //url=url+"&ListingLevel="+encodeURI(document.getElementById("ListingLevel").value);
            url = url + "&sltid=" + encodeURI(document.getElementById("sltid").value);
            url = url + "&start_range=" + encodeURI(document.getElementById("start_range").value);
            url = url + "&end_range=" + encodeURI(document.getElementById("end_range").value);
            url = url + "&start=" + encodeURI(start);
            url = url + "&sid=" + Math.random();
            searchRequest.onreadystatechange = searchRequestStateChanged;
            searchRequest.open("GET", url, true);
            searchRequest.send(null);
        }

        function searchRequestStateChanged() {
            if (searchRequest.readyState == 4) {
                document.getElementById("searchResultsDiv").innerHTML = searchRequest.responseText;
            }
        }

        function addLeads(leadCount, type) {
            var leadString = "";
            for (var i = 1; i <= leadCount; i++) {
                var chkBox = document.getElementById("multileadid[" + i + "]");

                if (chkBox.checked) {
                    leadString += "" + chkBox.value + ",";
                }
            }

            if (leadString.length <= 0) {
                alert("Please Select at least one lead");
                return;
            }

            if (confirm('You are about to take possession of these leads. Click OK to proceed.')) {
                document.location = "<?= $_SERVER['PHP_SELF'] ?>?type=" + type + "&multileadids=" + leadString;
            }
        }
    </script>

    <script src="js/func.js"></script>

    <?php include("components/bootstrap.php") ?>

</head>
<body bgcolor="#FFFFFF"
      onload="<?= $_GET['searchtype'] == "Store" ? "toggleSearch('Lead Store');searchLeads(0);" : "" ?>">
<?php include("components/header.php") ?>
<div id="body">
    <?php
    $CURRENT_PAGE = "Leads";
    include("components/navbar.php");
    ?>
    <form method="post" action="<?= $_SERVER['PHP_SELF'] ?>">

        <div class="well">
            <div id="SearchDiv" style="display: block;">

                <div class="sectionHeader text-center">
                    <strong><h3>Lead Store</h3></strong>
                    <input type="hidden" name="SearchType" id="SearchType" value="Lead Store"/>
                </div>
                <br>

                <div class="row">
                    <div class="sectionDiv">
                        <div id="dateSearchDiv">
                            <div class="col-lg-4 col-lg-offset-2">
                                <div id="leadstore_label" class="col-lg-3 text-right"><label
                                        class="dateLabel">From:</label></div>
                                <div class="leadstore_field col-lg-9"><input name="start_range" id="start_range"
                                                                             type="text"
                                                                             class="datepicker form-control input-sm"
                                                                             value="<?= date("n/j/Y", strtotime("-90 days")); ?>"/>
                                </div>
                            </div>


                            <div class="col-lg-6">
                                <div id="leadstore_label" class="col-lg-1 text-right"><label
                                        class="dateLabel">To:</label></div>
                                <div class="leadstore_field col-lg-6"><input name="end_range" id="end_range" type="text"
                                                                             class="datepicker form-control input-sm"
                                                                             value="<?= date("n/j/Y"); ?>"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>

                <div class="row">
                    <div id="standardSearchDiv" class="col-xs-6">
                        <div class="row">
                            <div class="leadstore_label col-lg-4 text-right"><label for="FirstName">Lead ID:</label>
                            </div>
                            <div class="leadstore_field col-lg-8"><input type="text" id="slid" name="slid"
                                                                         class="form-control input-sm"/></div>
                        </div>
                        <br>

                        <div class="row">
                            <div class="leadstore_label col-lg-4 text-right"><label for="FirstName">or Email:</label>
                            </div>
                            <div class="leadstore_field col-lg-8"><input type="text" id="email" name="email"
                                                                         class="form-control input-sm"/></div>
                        </div>
                        <br>

                        <div class="row">
                            <div class="leadstore_label col-lg-4 text-right"><label for="FirstName">or Phone:</label>
                            </div>
                            <div class="leadstore_field col-lg-8"><input type="text" id="phone" name="phone"
                                                                         class="form-control input-sm"/></div>
                        </div>
                        <br>

                        <div class="row">
                            <div class="leadstore_label col-lg-4 text-right"><label for="FirstName">or Last
                                    Name:</label></div>
                            <div class="leadstore_field col-lg-8"><input type="text" id="lname" name="lname"
                                                                         class="form-control input-sm"/></div>
                        </div>
                        <br>

                        <div class="row">
                            <div class="leadstore_label col-lg-4 text-right"><label for="eid">or Mailer Code:</label>
                            </div>
                            <div class="leadstore_field col-lg-8"><input type="text" id="eid" name="eid"
                                                                         class="form-control input-sm"/></div>
                        </div>
                    </div>

                    <div id="advancedSearchDiv" class="col-xs-6">

                        <div class="row">
                            <div class="leadstore_label col-lg-2 col-lg-offset-3 text-right"><label
                                    for="State">State:</label></div>
                            <div class="leadstore_field col-lg-6"><input type="text" id="State" name="State"
                                                                         style="width: 50px"
                                                                         class="form-control input-sm"/></div>
                        </div>
                        <br>
                        <!---
                        <div class="row">
                            <div class="leadstore_label col-lg-4 col-lg-offset-1 text-right"><label for="ListingClass">Purchase/Refi:</label></div>
                            <div class="leadstore_field col-lg-6">
                                <select class="form-control input-sm" aria-haspopup="true" aria-expanded="true" name="ListingClass" id="ListingClass">
                                    <option value="">All</option>
                                    <option value="Refi">Refi</option>
                                    <option value="Purchase">Exclusive</option>
                                </select>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="leadstore_label col-lg-4 col-lg-offset-1 text-right"><label for="ListingLevel">Shared/Excl:</label></div>
                            <div class="leadstore_field col-lg-6">
                                <select class="form-control input-sm" aria-haspopup="true" aria-expanded="true" name="ListingLevel" id="ListingLevel">
                                    <option value="">All</option>
                                    <option value="Shared">Shared</option>
                                    <option value="Exclusive">Exclusive</option>
                                </select>
                            </div>
                        </div>
                        --->

                        <div class="row">
                            <div class="leadstore_label col-lg-4 col-lg-offset-1 text-right"><label for="sltid">Campaign:</label>
                            </div>
                            <div class="leadstore_field col-lg-6">
                                <select class="form-control input-sm" aria-haspopup="true" aria-expanded="true"
                                        name="sltid" id="sltid">
                                    <option value="">All</option>
                                    <?php
                                    foreach ($camps as $c) {
                                        ?>
                                        <option
                                            value="<?= $c->SubmissionListingTypeID ?>"><?= $c->ListingDescription ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>


                    </div>
                </div>
                <br>

                <div class="row col-lg-12">
                    <div id="searchButtonDiv">
                        <input type="button" value="Search Lead" name="Submit" onclick="searchLeads(0);"
                               class="btn btn-default btn-md center-block"/>
                    </div>

                    <br/><br/>

                    <div id="searchResultsDiv" class="table-responsive"></div>
                </div>


            </div>
        </div>
</div>
<!--container-->
</form>


</div>
<?php include("components/footer.php"); ?>

</body>
</html>