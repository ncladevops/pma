<?php
	require("../printable/include/mysql.inc.php");
	require("../printable/include/optimize.printable.inc.php");
	require("globals.php");
	
	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	
	$portal = new OptimizePortal($COMPANY_ID, $db);

	$extension = str_replace(array(".", "/", "\\", ",", "?"), "", substr($_GET['ext'], 0, 3));
	
	header("Content-type: image/gif");
	
	$filepath = "images/icos/{$extension}_small.gif";
	if(!file_exists($filepath)) {
		$filepath = "images/icos/def_small.gif";
	}
		
	$im = imagecreatefromgif($filepath);
	
	imagegif($im);
	
	imagedestroy($im);