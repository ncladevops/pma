<?php
	require("../printable/include/mysql.inc.php");
	require("../printable/include/optimize.printable.inc.php");
	require("globals.php");
	
	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	
	$portal = new OptimizePortal($COMPANY_ID, $db);
	
	$currentUser = $portal->UserAccess($_SESSION['currentuserid']);
	$isSuper = $portal->CheckPriv($currentUser->UserID, 'supervisor');
			
	if(!$currentUser)
	{
		header("Location: login.php?message=" .urlencode("Not logged in or login error."));
		die();
	}	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
	<title>
		<?= $portal->CurrentCompany->CompanyName ?> :: Coming Soon!
	</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />		
	<link type="text/css" media="all" rel="stylesheet" href="style.css" />
	<link type="text/css" media="all" rel="stylesheet" href="css/home.css" />
	<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />
	<script  src="js/func.js"></script>	
</head>
<body bgcolor="#FFFFFF">
<div id="page">
<?php include("components/header.php") ?>
<div id="body">
	<?php 
		$CURRENT_PAGE = "Home";
		include("components/navbar.php"); 
	?>
	<div class="error"><?= $_GET['message']; ?></div>
	<div id="home_container">
		<div id="main_panel">
			Coming Soon!
		</div>		
	</div>
</div>
</div>
</body>
</html>