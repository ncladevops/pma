<?php
	require("../printable/include/mysql.inc.php");
	require("../printable/include/optimize.printable.inc.php");
	require("globals.php");
	
	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	
	$portal = new OptimizePortal($COMPANY_ID, $db);
	
	$currentUser = $portal->UserAccess($_SESSION['currentuserid']);
	$isSubAdmin = $portal->CheckPriv($currentUser->UserID, 'subadmin');
		
	// Check login
	if(!$isSubAdmin)
	{
		header( "Location: " . $portal->CurrentCompany->Website . "login.php?message=" . urlencode( "Not logged in or login error.  Please try again." ) );
		die();
	}
  	
  	if($_POST['Submit'] == 'Delete')
	{		
		// Make sure file exists and get old names
		$posting = $portal->GetPosting($_GET['postingid']);
		
		if(!$posting)
		{
			print_r($posting); echo 'err';
			echo "Invalid Posting";
			die();
		}
		
		
		// delete old file
		if(is_file("{$FILES_LOCATION}" . $portal->CurrentCompany->CompanyID . "/social/$posting->ImageURL"))
		{
			unlink("{$FILES_LOCATION}" . $portal->CurrentCompany->CompanyID . "/social/$posting->ImageURL");
		}
		
		// delete old thumb
		if(is_file("{$FILES_LOCATION}" . $portal->CurrentCompany->CompanyID . "/social/$posting->ImageURL"))
		{
			unlink("{$FILES_LOCATION}" . $portal->CurrentCompany->CompanyID . "/social/$posting->ImageURL");
		}
		
		
		// update db
		$portal->DeletePosting($posting->SocialPostingID);
			
		// direct back to manage files
		header("Location: manage_socialpost.php?section=" . $_GET['section']);
	}
	
	// Get list of files
	$posting = $portal->GetPosting($_GET['postingid']);
		
	if(!$posting)
	{
		echo "Invalid Posting";
		die();
	}
		
	// Lookup Posting Size
	if($posting->PostingPath != '')
	{
		$postingsize = number_format(filesize("{$FILES_LOCATION}" . $portal->CurrentCompany->CompanyID . "/files/$posting->PostingPath")/1024, 2);
	}
	else 
	{
		$postingsize = '0.00';
	}
		
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
	<title>
		Optimize on Demand :: Delete Posting
	</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />		
	<link type="text/css" media="all" rel="stylesheet" href="style.css" />
	<link type="text/css" media="all" rel="stylesheet" href="css/edit_socialposting.css" />
	<script  src="js/func.js"></script>	
	<link rel="stylesheet" href="thumbview/thumbnailviewer.css" type="text/css" />
</head>
<body bgcolor="#FFFFFF">
<div id="page">
<?php include("components/header.php") ?>
<div id="body">
	<?php 
		$CURRENT_PAGE = "Control Panel";
		include("components/navbar.php"); 
	?>
<form method="post" action="<?= $_SERVER['PHP_SELF']?>?section=<?= $_GET['section'] ?>&postingid=<?= $posting->SocialPostingID ?>" enctype="multipart/form-data" >
	<div class="error"><?= $_GET['message']; ?></div>
	<div id="EditPostingDiv">
		<div class="sectionHeader">
			Delete Social Posting
		</div>
		<div class="sectionDiv">
			<strong>Are you sure you want to delete this posting? All 
			information will be lost and can not be recovered.</strong><br/>&nbsp;<br/>
			<div class="itemSection">
				<div id="DescriptionDiv">
					<label for="description">Name:</label><br/>
					<b><?= $posting->PostingName ?></b>
				</div>
			</div>
			<div class="itemSectionLong">
				<div id="DetailDiv">
					<label for="detail">Message:</label><br/>
					<b><?= $posting->Message ?></b>
				</div>
			</div>
		</div>
		<center>
		<div class="itemSection">
			<div class="buttonSection">
				<input type="submit" name="Submit" value="Delete"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="button" name="Cancel" value="Cancel" onclick="parent.location='manage_socialpost.php?section=<?php echo $_GET['section']; ?>'"/>
			</div>
		</div>
		</center>
	</div>
</form>
</div>
</div>
</body>
</html>