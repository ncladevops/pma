<?php
	require("../printable/include/mysql.inc.php");
	require("../printable/include/optimize.printable.inc.php");
	require("globals.php");
	
	$reportTypes = array(
				"leadReport" => array("Lead Status Report" => "reports/lead_status.php",
										"User Lead Report" => "reports/user_lead.php",
										"Bad Lead Report" => "reports/bad_lead.php",
										"All Lead Report" => "reports/all_lead.php",
										"Lead Transfer Report" => "reports/transfer_lead.php",
										"Report by Team" => "reports/team_summary.php",
										"Lead Distribution" => "reports/lead_dist.php",
										"Team Conversion" => "reports/team_conversion.php",
										"Time to Contact" => "reports/contact_time.php"),
	
				"contactReport" => array("Open Meeting" => "",
											"Last Contact Activity" => ""),
	
				"marketReport" => array("User Activity" => "",
											"Product Activity" => ""),
	
				"customReport" => array());
	
	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	
	$portal = new OptimizePortal($COMPANY_ID, $db);
	
	$currentUser = $portal->UserAccess($_SESSION['currentuserid']);
	$isSuper = $portal->CheckPriv($currentUser->UserID, 'supervisor');
			
	if(!$portal->CheckPriv($currentUser->UserID, 'report'))
	{
		header( "Location: " . $portal->CurrentCompany->Website . "home.php?message=" . urlencode( "Accessed Denied." ) );
		die();
	}
	$userGroup = $portal->GetGroup($currentUser->GroupID);	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
	<title>
		<?= $portal->CurrentCompany->CompanyName ?> :: Reports
	</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />		
	<link type="text/css" media="all" rel="stylesheet" href="style.css" />
	<link rel="stylesheet" href="include/foldertree/css/folder-tree-static.css" type="text/css"/>
	<link rel="stylesheet" href="include/foldertree/css/context-menu.css" type="text/css"/>
	<script type="text/javascript" src="include/foldertree/js/ajax.js"></script>
	<script type="text/javascript" src="include/foldertree/js/folder-tree-static.js"></script>
	<script type="text/javascript" src="include/foldertree/js/context-menu.js"></script>		
	<link type="text/css" media="all" rel="stylesheet" href="style.css" />
	<link type="text/css" media="all" rel="stylesheet" href="css/report.css" />
	<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />
	<script  src="js/func.js"></script>	
<script language="JavaScript" type="text/JavaScript">

	function loadReportTable(reportDivName)
	{
<?php
	foreach($reportTypes as $repType=>$reports) {
?>
		document.getElementById("<?= $repType?>Div").style.display = "none";
<?php 
	}
?>
		document.getElementById(reportDivName).style.display = "";
	}
</script>
</head>
<body bgcolor="#FFFFFF">
<div id="page">
<?php include("components/header.php") ?>
<div id="body">
	<?php 
		$CURRENT_PAGE = "Reports";
		include("components/navbar.php"); 
	?>
	<div class="error"><?= $_GET['message']; ?></div>
	<div id="reportContainer">
		<div class="sectionHeader">
			Reports
		</div>
		<div id="folderTreeContainer">
			<?php include("components/report_tree.php"); ?>
		</div>
		<div id="detailContainer">
<?php
	foreach($reportTypes as $repType=>$reports) { 
?>
			<div id="<?= $repType ?>Div" style="display: none;">
				<table class="reportListTable">
					<tr class="tableHeader reportListTable">
						<th class="reportListTable">Title</th>
						<th class="reportListTable">&nbsp;</th>
					</tr>
<?php
		$i = 0;
		foreach($reports as $repName=>$repLink) {
?>
					
					<tr class="tableDetail<?= ( $i % 2 != 0 ? 'Odd' : '' )?> reportListTable">
						<td class="reportListTable"><strong><?= $repName ?></strong></td>
						<td class="reportListTable" style="width: 50px; text-align: center;"><a href="<?= $repLink ?>" target="_blank"><img src="images/icos/view_large.png" border="0"/></a></td>
					</tr>
<?php 
			$i++;
		} 
?>
				</table>
			</div>	
<?php
	} 
?>					
		</div>
	</div>
</div>
</div>
</body>
</html>