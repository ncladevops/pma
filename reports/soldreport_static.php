<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	<title>
		<?= $portal->CurrentCompany->CompanyName ?> Sold Report
	</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />		
	<link type="text/css" media="all" rel="stylesheet" href="style.css" />
	<script src="../js/jscalendar/js/jscal2.js"></script>
	<script src="../js/jscalendar/js/lang/en.js"></script>
	<link rel="stylesheet" type="text/css" href="../js/jscalendar/css/jscal2.css" />
	<link rel="stylesheet" type="text/css" href="../js/jscalendar/css/win2k/win2k.css" />	
	<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />
<script type="text/javascript">//<![CDATA[

      var cal = Calendar.setup({
          onSelect: function(cal) { cal.hide() },
          bottomBar: true,
          dateFormat : "%m/%d/%Y"
      });

//]]></script>
	</head>
<body bgcolor="#FFFFFF" >
<table width="100%" cellpadding="2" cellspacing="1">
	<tr>
		<td align="left" class="headerTextLarge">
			<form method="get" action="">
				Sold Date: <input type="text" id="start" name="start" value="" size="10">
				<script type="text/javascript">
					cal.manageFields("start", "start", "%m/%d/%Y");
				</script> to 
				<input type="text" id="end" name="end" value="" size="10">
				<script type="text/javascript">
					cal.manageFields("end", "end", "%m/%d/%Y");
				</script>
				<input type="submit" value="Go" name="Submit">
			</form>
		</td>
	</tr>
	<tr>
		<td align="left" class="headerTextLarge">
			<table bgcolor="#666666">
				<tr class="tableHeader" >
					<td width="180">
						Manager
					</td>
					<td width="180">
						Sales Exec
					</td>
					<td width="240">
						Company
					</td>
					<td width="130">
						Call Qty
					</td>
					<td width="120">
						Appt Date
					</td>
					<td width="120">
						Sold Date
					</td><td width="240">
						Comment
					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Dawn Davis					</td>
					<td>
						Agustin Llanos					</td>
					<td>
						SUNSET PLAZA HOTEL					</td>
					<td>
						0					</td>
					<td>
											</td>
					<td>
						02/18/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Paul Turner					</td>
					<td>
						Maria Rice					</td>
					<td>
						BEARCLAW MANUFACTURING					</td>
					<td>
						0					</td>
					<td>
											</td>
					<td>
						02/18/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Paul Turner					</td>
					<td>
						Maria Rice					</td>
					<td>
						G & G FASHION					</td>
					<td>
						0					</td>
					<td>
											</td>
					<td>
						02/18/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Dawn Davis					</td>
					<td>
						Agustin Llanos					</td>
					<td>
						SEPULVEDA BOARDING CARE REHAB					</td>
					<td>
						3					</td>
					<td>
											</td>
					<td>
						02/18/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Peter Yen					</td>
					<td>
						Jimmy Kim					</td>
					<td>
						I E BUSINESS SCHOOL					</td>
					<td>
						0					</td>
					<td>
											</td>
					<td>
						02/18/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Howard Eigenberg					</td>
					<td>
						John Baccus					</td>
					<td>
						DIVERSIFIED LANGUAGE INSTITUTE					</td>
					<td>
						0					</td>
					<td>
											</td>
					<td>
						02/18/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Howard Eigenberg					</td>
					<td>
						John Baccus					</td>
					<td>
						HAPPY TOURS TRAVEL AGENCY					</td>
					<td>
						0					</td>
					<td>
											</td>
					<td>
						02/25/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Howard Eigenberg					</td>
					<td>
						John Baccus					</td>
					<td>
						HOME DEPOT					</td>
					<td>
						0					</td>
					<td>
											</td>
					<td>
						02/25/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Howard Eigenberg					</td>
					<td>
						John Baccus					</td>
					<td>
						HOUSING AUTHORITY OF THE CITY					</td>
					<td>
						0					</td>
					<td>
											</td>
					<td>
						02/25/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Joe Ugalde					</td>
					<td>
						Jim Brown					</td>
					<td>
						Cantor Fitzgerald & Company					</td>
					<td>
						0					</td>
					<td>
											</td>
					<td>
						02/25/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Peter Yen					</td>
					<td>
						Valerie Jordan					</td>
					<td>
						MINT CANYON ELEMENTARY SCHOOL					</td>
					<td>
						0					</td>
					<td>
											</td>
					<td>
						02/25/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Peter Yen					</td>
					<td>
						Valerie Jordan					</td>
					<td>
						MINT CANYON ELEMENTARY SCHOOL					</td>
					<td>
						0					</td>
					<td>
											</td>
					<td>
						02/25/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Robert Tucci					</td>
					<td>
						Cynthia Brewer					</td>
					<td>
						BERRY S PIZZA CAFE					</td>
					<td>
						2					</td>
					<td>
											</td>
					<td>
						03/04/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Paul Turner					</td>
					<td>
						Le Van Moment					</td>
					<td>
						HOLLYWOOD AUTO GLASS					</td>
					<td>
						0					</td>
					<td>
											</td>
					<td>
						03/04/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Peter Yen					</td>
					<td>
						Valerie Jordan					</td>
					<td>
						Academy for Arts & Education					</td>
					<td>
						0					</td>
					<td>
											</td>
					<td>
						03/11/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Robert Tucci					</td>
					<td>
						Cynthia Brewer					</td>
					<td>
						HOLY COW INDIAN EXPRESS					</td>
					<td>
						0					</td>
					<td>
											</td>
					<td>
						03/12/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Robert Tucci					</td>
					<td>
						Cynthia Brewer					</td>
					<td>
						FLOOR COVERING UNLIMITED INC					</td>
					<td>
						0					</td>
					<td>
											</td>
					<td>
						03/16/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Robert Tucci					</td>
					<td>
						Cynthia Brewer					</td>
					<td>
						FLOOR COVERING UNLIMITED INC					</td>
					<td>
						0					</td>
					<td>
											</td>
					<td>
						03/16/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Robert Tucci					</td>
					<td>
						Cynthia Brewer					</td>
					<td>
						FLOOR COVERING UNLIMITED INC					</td>
					<td>
						0					</td>
					<td>
											</td>
					<td>
						03/16/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Robert Tucci					</td>
					<td>
						Cynthia Brewer					</td>
					<td>
						FIELD MANAGEMENT SVC					</td>
					<td>
						1					</td>
					<td>
											</td>
					<td>
						03/20/2009					</td>
					<td>
						SOLD 5 lines, intenet, and vm					</td>
				</tr>
				<tr class="tableDetail">
					<td>
											</td>
					<td>
						Time Warner Cable					</td>
					<td>
						The GDR Group					</td>
					<td>
						3					</td>
					<td>
						04/16/2009					</td>
					<td>
						03/20/2009					</td>
					<td>
						test of sold notes					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Enrico Diaz					</td>
					<td>
						Janet Atkins					</td>
					<td>
						R A PHYSICAL THERAPY REHAB					</td>
					<td>
						1					</td>
					<td>
											</td>
					<td>
						03/23/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Enrico Diaz					</td>
					<td>
						Janet Atkins					</td>
					<td>
						Bahia Corinthian Yacht Club In					</td>
					<td>
						2					</td>
					<td>
											</td>
					<td>
						03/31/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Peter Yen					</td>
					<td>
						Valerie Jordan					</td>
					<td>
						EMPLOYMENT DEVELOPMENT DEPT					</td>
					<td>
						0					</td>
					<td>
											</td>
					<td>
						04/01/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Dawn Davis					</td>
					<td>
						Agustin Llanos					</td>
					<td>
						Holiday Inn Laguna Hills					</td>
					<td>
						2					</td>
					<td>
											</td>
					<td>
						04/01/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Howard Eigenberg					</td>
					<td>
						John Baccus					</td>
					<td>
						ADVANCED CARDIAC CARE CTR					</td>
					<td>
						0					</td>
					<td>
											</td>
					<td>
						04/08/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Paul Turner					</td>
					<td>
						Lance Alarcon					</td>
					<td>
						EMERGENCY LOCKSMITH					</td>
					<td>
						2					</td>
					<td>
											</td>
					<td>
						04/08/2009					</td>
					<td>
						called client wants information
					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Howard Eigenberg					</td>
					<td>
						John Baccus					</td>
					<td>
						JMC DATABASE & COMPUTER					</td>
					<td>
						0					</td>
					<td>
											</td>
					<td>
						04/22/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Howard Eigenberg					</td>
					<td>
						Burt Hekmatnia					</td>
					<td>
						Chase Manhattan Bank					</td>
					<td>
						0					</td>
					<td>
											</td>
					<td>
						04/22/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Howard Eigenberg					</td>
					<td>
						John Baccus					</td>
					<td>
						Nevins Donut					</td>
					<td>
						0					</td>
					<td>
											</td>
					<td>
						04/22/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Howard Eigenberg					</td>
					<td>
						John Baccus					</td>
					<td>
						Pegasus Communications Inc					</td>
					<td>
						0					</td>
					<td>
											</td>
					<td>
						04/22/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Peter Yen					</td>
					<td>
						Valerie Jordan					</td>
					<td>
						TORRANCE WATER DIV					</td>
					<td>
						0					</td>
					<td>
											</td>
					<td>
						04/22/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Howard Eigenberg					</td>
					<td>
						John Baccus					</td>
					<td>
						Rhodes Transportation					</td>
					<td>
						0					</td>
					<td>
											</td>
					<td>
						04/22/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Peter Yen					</td>
					<td>
						John Christiansen					</td>
					<td>
						Crossroads School for Arts and Sciences					</td>
					<td>
						1					</td>
					<td>
											</td>
					<td>
						04/22/2009					</td>
					<td>
						30 Meg Internet and two 100 Meg P2P circuits					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Enrico Diaz					</td>
					<td>
						Janet Atkins					</td>
					<td>
						DATALINK ITS					</td>
					<td>
						1					</td>
					<td>
											</td>
					<td>
						04/29/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Howard Eigenberg					</td>
					<td>
						Burt Hekmatnia					</td>
					<td>
						COMPUTER FACTORY					</td>
					<td>
						0					</td>
					<td>
						04/24/2009					</td>
					<td>
						05/06/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Howard Eigenberg					</td>
					<td>
						Burt Hekmatnia					</td>
					<td>
						FRIENDS-ALZHEIMER CAREGIVERS					</td>
					<td>
						2					</td>
					<td>
											</td>
					<td>
						05/06/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Howard Eigenberg					</td>
					<td>
						Burt Hekmatnia					</td>
					<td>
						LOS ALAMITOS MARKET & LIQUOR					</td>
					<td>
						0					</td>
					<td>
											</td>
					<td>
						05/06/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Paul Turner					</td>
					<td>
						Israel Argueta					</td>
					<td>
						HTI FILTRATION CORP					</td>
					<td>
						1					</td>
					<td>
						04/14/2009					</td>
					<td>
						05/13/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Enrico Diaz					</td>
					<td>
						Brant Bishop					</td>
					<td>
						BIZ FLOW CORP					</td>
					<td>
						1					</td>
					<td>
											</td>
					<td>
						05/13/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Howard Eigenberg					</td>
					<td>
						John Baccus					</td>
					<td>
						SOUTHLAND VALUATION INC					</td>
					<td>
						2					</td>
					<td>
											</td>
					<td>
						05/20/2009					</td>
					<td>
						Sold, but came back not servicable.					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Enrico Diaz					</td>
					<td>
						Peter Perdikakis					</td>
					<td>
						CHRYSLER CORP					</td>
					<td>
						1					</td>
					<td>
											</td>
					<td>
						05/27/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Enrico Diaz					</td>
					<td>
						Peter Perdikakis					</td>
					<td>
						Chevron Corporation					</td>
					<td>
						1					</td>
					<td>
											</td>
					<td>
						05/27/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
											</td>
					<td>
						Time Warner Cable					</td>
					<td>
						NCLA					</td>
					<td>
						5					</td>
					<td>
						06/24/2009					</td>
					<td>
						05/28/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Enrico Diaz					</td>
					<td>
						Brant Bishop					</td>
					<td>
						America West Airlines  Inc					</td>
					<td>
						0					</td>
					<td>
											</td>
					<td>
						06/03/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Robert Tucci					</td>
					<td>
						Randy Geltman					</td>
					<td>
						Aegis Medical Systems Inc					</td>
					<td>
						0					</td>
					<td>
											</td>
					<td>
						06/03/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Peter Yen					</td>
					<td>
						HP Le					</td>
					<td>
						Saint Jeanne de Lestonnac Preschool and					</td>
					<td>
						0					</td>
					<td>
											</td>
					<td>
						06/17/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Howard Eigenberg					</td>
					<td>
						John Baccus					</td>
					<td>
						ADELPHIA					</td>
					<td>
						0					</td>
					<td>
											</td>
					<td>
						06/17/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Enrico Diaz					</td>
					<td>
						Richard Fissel					</td>
					<td>
						STUDIO PHOTO IMAGING INC					</td>
					<td>
						2					</td>
					<td>
											</td>
					<td>
						06/17/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Howard Eigenberg					</td>
					<td>
						Michele D'Amico					</td>
					<td>
						Spirit Cafe & Bookstore					</td>
					<td>
						0					</td>
					<td>
											</td>
					<td>
						06/17/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Howard Eigenberg					</td>
					<td>
						John Baccus					</td>
					<td>
						ALL FIT NATION					</td>
					<td>
						0					</td>
					<td>
											</td>
					<td>
						06/17/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						San Diego Region					</td>
					<td>
						Brian Truesdale					</td>
					<td>
						Kreiss Collection					</td>
					<td>
						0					</td>
					<td>
											</td>
					<td>
						06/18/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						San Diego Sales Director					</td>
					<td>
						Albert Acevedo					</td>
					<td>
						Holiday Inn Express					</td>
					<td>
						0					</td>
					<td>
											</td>
					<td>
						06/18/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						San Diego Region					</td>
					<td>
						Brian Truesdale					</td>
					<td>
						Vanpike Inc					</td>
					<td>
						0					</td>
					<td>
											</td>
					<td>
						06/18/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						San Diego Region					</td>
					<td>
						Brian Truesdale					</td>
					<td>
						Westair Gases & Equipment Inc					</td>
					<td>
						0					</td>
					<td>
											</td>
					<td>
						06/18/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						San Diego Sales Director					</td>
					<td>
						Albert Acevedo					</td>
					<td>
						Jack In The Box					</td>
					<td>
						0					</td>
					<td>
											</td>
					<td>
						06/18/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Howard Eigenberg					</td>
					<td>
						John Baccus					</td>
					<td>
						ARNON DEVELOPMENT					</td>
					<td>
						0					</td>
					<td>
											</td>
					<td>
						06/24/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Howard Eigenberg					</td>
					<td>
						John Baccus					</td>
					<td>
						BB CASTING  COM					</td>
					<td>
						0					</td>
					<td>
											</td>
					<td>
						06/24/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Dawn Davis					</td>
					<td>
						Wayne F. Burt Jr.					</td>
					<td>
						ANAHEIM CENTER INN & SUITES					</td>
					<td>
						0					</td>
					<td>
											</td>
					<td>
						06/26/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Dawn Davis					</td>
					<td>
						Wayne F. Burt Jr.					</td>
					<td>
						AMERICANA MOTEL					</td>
					<td>
						1					</td>
					<td>
											</td>
					<td>
						06/29/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Dawn Davis					</td>
					<td>
						Wayne F. Burt Jr.					</td>
					<td>
						Anaheim Hills Travelodge					</td>
					<td>
						1					</td>
					<td>
											</td>
					<td>
						06/29/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						San Diego Region					</td>
					<td>
						Brian Truesdale					</td>
					<td>
						Admiral Housing Corp					</td>
					<td>
						1					</td>
					<td>
											</td>
					<td>
						06/30/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Dawn Davis					</td>
					<td>
						Jon Dunning					</td>
					<td>
						Ana Mesa Suites					</td>
					<td>
						1					</td>
					<td>
											</td>
					<td>
						07/01/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Dawn Davis					</td>
					<td>
						Jon Dunning					</td>
					<td>
						ANAHEIM LA PALMA INN					</td>
					<td>
						0					</td>
					<td>
											</td>
					<td>
						07/01/2009					</td>
					<td>
						Agustin account					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Dawn Davis					</td>
					<td>
						Jon Dunning					</td>
					<td>
						ANAHEIM NATIONAL INN					</td>
					<td>
						3					</td>
					<td>
											</td>
					<td>
						07/01/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Dawn Davis					</td>
					<td>
						Jon Dunning					</td>
					<td>
						ANAHEIM OVERNITE TRAILER PARK					</td>
					<td>
						0					</td>
					<td>
											</td>
					<td>
						07/01/2009					</td>
					<td>
						Trailer Park already has residential service to several trailers.					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Dawn Davis					</td>
					<td>
						Wayne F. Burt Jr.					</td>
					<td>
						BEST WESTERN					</td>
					<td>
						0					</td>
					<td>
											</td>
					<td>
						07/08/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Dawn Davis					</td>
					<td>
						Wayne F. Burt Jr.					</td>
					<td>
						BEST WESTERN					</td>
					<td>
						0					</td>
					<td>
											</td>
					<td>
						07/08/2009					</td>
					<td>
						Sold BCP					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Robert Hadaway					</td>
					<td>
						Eric Swarts					</td>
					<td>
						ABC RECOVERY CTR					</td>
					<td>
						0					</td>
					<td>
											</td>
					<td>
						07/09/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Robert Hadaway					</td>
					<td>
						Eric Swarts					</td>
					<td>
						ABEL KRIEGER & WILMETH					</td>
					<td>
						0					</td>
					<td>
											</td>
					<td>
						07/09/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Robert Hadaway					</td>
					<td>
						Eric Swarts					</td>
					<td>
						ACCRETIVE LA QUINTA					</td>
					<td>
						0					</td>
					<td>
											</td>
					<td>
						07/09/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Robert Hadaway					</td>
					<td>
						Eric Swarts					</td>
					<td>
						ACCRETIVE LA QUINTA					</td>
					<td>
						0					</td>
					<td>
											</td>
					<td>
						07/09/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Desert Cities Sales Director					</td>
					<td>
						Justin Ulsh					</td>
					<td>
						ACRES CONSULTING					</td>
					<td>
						2					</td>
					<td>
											</td>
					<td>
						07/09/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						San Diego Region					</td>
					<td>
						Brian Truesdale					</td>
					<td>
						ACCELRYS					</td>
					<td>
						0					</td>
					<td>
											</td>
					<td>
						07/09/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						San Diego Region					</td>
					<td>
						Brian Truesdale					</td>
					<td>
						ALPHA LASER CENTER					</td>
					<td>
						0					</td>
					<td>
											</td>
					<td>
						07/09/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Robert Hadaway					</td>
					<td>
						Eric Swarts					</td>
					<td>
						ALFREDO SANDOVAL					</td>
					<td>
						1					</td>
					<td>
											</td>
					<td>
						07/09/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Robert Hadaway					</td>
					<td>
						Eric Swarts					</td>
					<td>
						ANDERSON TRAVEL					</td>
					<td>
						0					</td>
					<td>
											</td>
					<td>
						07/09/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Robert Hadaway					</td>
					<td>
						Eric Swarts					</td>
					<td>
						CARLSON GOLF,PETE					</td>
					<td>
						0					</td>
					<td>
											</td>
					<td>
						07/13/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						San Diego Sales Director					</td>
					<td>
						Steven Artura					</td>
					<td>
						AMORE NEW YORK PIZZERIA					</td>
					<td>
						0					</td>
					<td>
											</td>
					<td>
						07/14/2009					</td>
					<td>
						Sold BCP Triple Play for 24 months.					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Stefani Rosso					</td>
					<td>
						Trish Luna					</td>
					<td>
						BETTERLOANSCOM					</td>
					<td>
						0					</td>
					<td>
											</td>
					<td>
						07/15/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Stefani Rosso					</td>
					<td>
						Trish Luna					</td>
					<td>
						BRISTOL FINANCIAL GROUP INC					</td>
					<td>
						0					</td>
					<td>
											</td>
					<td>
						07/15/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Dawn Davis					</td>
					<td>
						Agustin Llanos					</td>
					<td>
						C I MANAGEMENT					</td>
					<td>
						1					</td>
					<td>
											</td>
					<td>
						07/15/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Robert Hadaway					</td>
					<td>
						Eric Swarts					</td>
					<td>
						COLDWELL BANKER					</td>
					<td>
						0					</td>
					<td>
											</td>
					<td>
						07/16/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Stefani Rosso					</td>
					<td>
						Trish Luna					</td>
					<td>
						FARMERS INSURANCE GROUP					</td>
					<td>
						0					</td>
					<td>
											</td>
					<td>
						07/17/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Robert Hadaway					</td>
					<td>
						Eric Swarts					</td>
					<td>
						DMR PARTNERS					</td>
					<td>
						0					</td>
					<td>
											</td>
					<td>
						07/21/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Howard Eigenberg					</td>
					<td>
						Brian Staller					</td>
					<td>
						ACCURATE MANAGEMENT & ACCTG					</td>
					<td>
						4					</td>
					<td>
											</td>
					<td>
						08/05/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Howard Eigenberg					</td>
					<td>
						Michele D'Amico					</td>
					<td>
						ADVANCED DIGITAL MEDIA					</td>
					<td>
						2					</td>
					<td>
											</td>
					<td>
						08/10/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Howard Eigenberg					</td>
					<td>
						Michele D'Amico					</td>
					<td>
						ADT SECURITY SVC					</td>
					<td>
						1					</td>
					<td>
											</td>
					<td>
						08/10/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Howard Eigenberg					</td>
					<td>
						Michele D'Amico					</td>
					<td>
						ADELSON TESTAN & BRUNDO					</td>
					<td>
						2					</td>
					<td>
											</td>
					<td>
						08/10/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Enrico Diaz					</td>
					<td>
						Brant Bishop					</td>
					<td>
						SHELTON CONSTRUCTION					</td>
					<td>
						2					</td>
					<td>
											</td>
					<td>
						08/19/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Enrico Diaz					</td>
					<td>
						Brant Bishop					</td>
					<td>
						SHEPPARD MULLIN & RICHTER					</td>
					<td>
						2					</td>
					<td>
											</td>
					<td>
						08/19/2009					</td>
					<td>
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Dawn Davis					</td>
					<td>
						Wayne F. Burt Jr.					</td>
					<td>
						BUDGET INN					</td>
					<td>
						0					</td>
					<td>
											</td>
					<td>
						08/26/2009					</td>
					<td>
											</td>
				</tr>
				
			</table>
		</td>
	</tr>
</table>
</body>
</html>
