<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	<title>
		<?= $portal->CurrentCompany->CompanyName ?> Campaign Summary
	</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />		
	<link type="text/css" media="all" rel="stylesheet" href="style.css" />
	<script src="../js/jscalendar/js/jscal2.js"></script>
	<script src="../js/jscalendar/js/lang/en.js"></script>
	<link rel="stylesheet" type="text/css" href="../js/jscalendar/css/jscal2.css" />
	<link rel="stylesheet" type="text/css" href="../js/jscalendar/css/win2k/win2k.css" />
<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />	
	</head>
<body bgcolor="#FFFFFF" >
<table width="100%" cellpadding="2" cellspacing="1">
	<tr>
		<td align="left" class="headerTextLarge">
			Campaign: All
		</td>
	</tr>
	<tr>
		<td align="left" class="headerTextLarge">
			<table bgcolor="#666666">
				<tr class="tableHeader" >
					<td width="180">
						Divisions
					</td>
					<td width="75" align="center">
						Att Calls					</td>
					<td width="75" align="center">
						LM					</td>
					<td width="75" align="center">
						Comp Calls					</td>
					<td width="75" align="center">
						Tot Calls					</td>
					<td width="75" align="center">
						Apts					</td>
					<td width="75" align="center">
						Mail - Call Att					</td>
					<td width="75" align="center">
						Mail - LM					</td>
					<td width="75" align="center">
						Mail - CC					</td>
					<td width="75" align="center">
						Mail - Appt					</td>
					<td width="75" align="center">
						Total Mail					</td>
					<td width="75" align="center">
						Sold					</td>
					<td width="75" align="center">
						Team Total					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						North					</td>
					<td align="right">
						154					</td>
					<td align="right">
						605					</td>
					<td align="right">
						139					</td>
					<td align="right">
						967					</td>
					<td align="right">
						53					</td>
					<td align="right">
						76					</td>
					<td align="right">
						432					</td>
					<td align="right">
						65					</td>
					<td align="right">
						11					</td>
					<td align="right">
						584					</td>
					<td align="right">
						16					</td>
					<td align="right">
						1551					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						East					</td>
					<td align="right">
						4003					</td>
					<td align="right">
						1244					</td>
					<td align="right">
						817					</td>
					<td align="right">
						6261					</td>
					<td align="right">
						154					</td>
					<td align="right">
						3086					</td>
					<td align="right">
						956					</td>
					<td align="right">
						349					</td>
					<td align="right">
						32					</td>
					<td align="right">
						4423					</td>
					<td align="right">
						43					</td>
					<td align="right">
						10684					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						South					</td>
					<td align="right">
						20					</td>
					<td align="right">
						77					</td>
					<td align="right">
						57					</td>
					<td align="right">
						166					</td>
					<td align="right">
						11					</td>
					<td align="right">
						10					</td>
					<td align="right">
						26					</td>
					<td align="right">
						49					</td>
					<td align="right">
						8					</td>
					<td align="right">
						93					</td>
					<td align="right">
						1					</td>
					<td align="right">
						259					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						West					</td>
					<td align="right">
						90					</td>
					<td align="right">
						77					</td>
					<td align="right">
						45					</td>
					<td align="right">
						219					</td>
					<td align="right">
						4					</td>
					<td align="right">
						38					</td>
					<td align="right">
						15					</td>
					<td align="right">
						16					</td>
					<td align="right">
						1					</td>
					<td align="right">
						70					</td>
					<td align="right">
						3					</td>
					<td align="right">
						289					</td>
				</tr>
				<tr class="tableDetailOdd">
					<td>
						Total
					</td>
					<td align="right">
						4267					</td>
					<td align="right">
						2003					</td>
					<td align="right">
						1058					</td>
					<td align="right">
						7613					</td>
					<td align="right">
						222					</td>
					<td align="right">
						3210					</td>
					<td align="right">
						1429					</td>
					<td align="right">
						479					</td>
					<td align="right">
						52					</td>
					<td align="right">
						5170					</td>
					<td align="right">
						63					</td>
					<td align="right">
						12783					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td align="left" class="headerTextLarge">
			<table bgcolor="#666666">
				<tr class="tableHeader" >
					<td width="180">
						Teams
					</td>
					<td width="75" align="center">
						Att Calls					</td>
					<td width="75" align="center">
						LM					</td>
					<td width="75" align="center">
						Comp Calls					</td>
					<td width="75" align="center">
						Tot Calls					</td>
					<td width="75" align="center">
						Apts					</td>
					<td width="75" align="center">
						Mail - Call Att					</td>
					<td width="75" align="center">
						Mail - LM					</td>
					<td width="75" align="center">
						Mail - CC					</td>
					<td width="75" align="center">
						Mail - Appt					</td>
					<td width="75" align="center">
						Total Mail					</td>
					<td width="75" align="center">
						Sold					</td>
					<td width="75" align="center">
						Team Total					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Robert Tucci - North					</td>
					<td align="right">
						68					</td>
					<td align="right">
						365					</td>
					<td align="right">
						76					</td>
					<td align="right">
						549					</td>
					<td align="right">
						32					</td>
					<td align="right">
						20					</td>
					<td align="right">
						307					</td>
					<td align="right">
						33					</td>
					<td align="right">
						6					</td>
					<td align="right">
						366					</td>
					<td align="right">
						8					</td>
					<td align="right">
						915					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Paul Hendrickson - North					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Joe Ugalde - North					</td>
					<td align="right">
						1					</td>
					<td align="right">
						10					</td>
					<td align="right">
						3					</td>
					<td align="right">
						15					</td>
					<td align="right">
						0					</td>
					<td align="right">
						1					</td>
					<td align="right">
						10					</td>
					<td align="right">
						2					</td>
					<td align="right">
						0					</td>
					<td align="right">
						13					</td>
					<td align="right">
						1					</td>
					<td align="right">
						28					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Peter Yen - North					</td>
					<td align="right">
						85					</td>
					<td align="right">
						230					</td>
					<td align="right">
						60					</td>
					<td align="right">
						403					</td>
					<td align="right">
						21					</td>
					<td align="right">
						55					</td>
					<td align="right">
						115					</td>
					<td align="right">
						30					</td>
					<td align="right">
						5					</td>
					<td align="right">
						205					</td>
					<td align="right">
						7					</td>
					<td align="right">
						608					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Paul Turner - East					</td>
					<td align="right">
						188					</td>
					<td align="right">
						117					</td>
					<td align="right">
						192					</td>
					<td align="right">
						544					</td>
					<td align="right">
						42					</td>
					<td align="right">
						99					</td>
					<td align="right">
						70					</td>
					<td align="right">
						38					</td>
					<td align="right">
						5					</td>
					<td align="right">
						212					</td>
					<td align="right">
						5					</td>
					<td align="right">
						756					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Enrico Diaz - East					</td>
					<td align="right">
						3118					</td>
					<td align="right">
						789					</td>
					<td align="right">
						497					</td>
					<td align="right">
						4480					</td>
					<td align="right">
						63					</td>
					<td align="right">
						2724					</td>
					<td align="right">
						639					</td>
					<td align="right">
						233					</td>
					<td align="right">
						21					</td>
					<td align="right">
						3617					</td>
					<td align="right">
						13					</td>
					<td align="right">
						8097					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Howard Eigenberg - East					</td>
					<td align="right">
						697					</td>
					<td align="right">
						338					</td>
					<td align="right">
						128					</td>
					<td align="right">
						1237					</td>
					<td align="right">
						49					</td>
					<td align="right">
						263					</td>
					<td align="right">
						247					</td>
					<td align="right">
						78					</td>
					<td align="right">
						6					</td>
					<td align="right">
						594					</td>
					<td align="right">
						25					</td>
					<td align="right">
						1831					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						South Sales Director - South					</td>
					<td align="right">
						20					</td>
					<td align="right">
						77					</td>
					<td align="right">
						57					</td>
					<td align="right">
						166					</td>
					<td align="right">
						11					</td>
					<td align="right">
						10					</td>
					<td align="right">
						26					</td>
					<td align="right">
						49					</td>
					<td align="right">
						8					</td>
					<td align="right">
						93					</td>
					<td align="right">
						1					</td>
					<td align="right">
						259					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						West Sales Director - West					</td>
					<td align="right">
						90					</td>
					<td align="right">
						77					</td>
					<td align="right">
						45					</td>
					<td align="right">
						219					</td>
					<td align="right">
						4					</td>
					<td align="right">
						38					</td>
					<td align="right">
						15					</td>
					<td align="right">
						16					</td>
					<td align="right">
						1					</td>
					<td align="right">
						70					</td>
					<td align="right">
						3					</td>
					<td align="right">
						289					</td>
				</tr>
				<tr class="tableDetailOdd">
					<td>
						Total
					</td>
					<td align="right">
						4267					</td>
					<td align="right">
						2003					</td>
					<td align="right">
						1058					</td>
					<td align="right">
						7613					</td>
					<td align="right">
						222					</td>
					<td align="right">
						3210					</td>
					<td align="right">
						1429					</td>
					<td align="right">
						479					</td>
					<td align="right">
						52					</td>
					<td align="right">
						5170					</td>
					<td align="right">
						63					</td>
					<td align="right">
						12783					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</body>
</html>