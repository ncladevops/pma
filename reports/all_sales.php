<?php
	require("../../printable/include/mysql.inc.php");
	require("../../printable/include/optimize.printable.inc.php");
	require("../globals.php");
	
	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	
	$portal = new OptimizePortal($COMPANY_ID, $db);
	
	$currentUser = $portal->GetUser($_SESSION['currentuserid']);
	$currentGroup = $portal->GetGroup($currentUser->GroupID);
			
	if(!$portal->CheckPriv($currentUser->UserID, 'report'))
	{
		header( "Location: " . $portal->CurrentCompany->Website . "home.php?message=" . urlencode( "Accessed Denied." ) );
		die();
	}
		
	if(strtotime($_GET['start_range']))
	{
		$startWhere = "submissionlisting.DateAdded >= '" . date("Y-m-d", strtotime($_GET['start_range'])) ." 00:00:00'";
	}
	else
	{
		$startWhere = "submissionlisting.DateAdded >= '" . date("Y-m-d") ." 00:00:00'";
		$_GET['start_range'] = date("Y-m-d");
	}
	
	if(strtotime($_GET['end_range']))
	{
		$endWhere = "submissionlisting.DateAdded <= '" . date("Y-m-d", strtotime($_GET['end_range'])) ." 23:59:59'";
	}
	else
	{
		$endWhere = "submissionlisting.DateAdded <= '" . date("Y-m-d") ." 23:59:59'";
		$_GET['end_range'] = date("Y-m-d");
	}
	
	// Select for Report
	$sqlstring = "SELECT
						contactstatustype.ContactStatus, submissionlisting.DateAdded, submissionlisting.Company,
						submissionlisting.Title, submissionlisting.Prefix, submissionlisting.FirstName, 
						submissionlisting.LastName, submissionlisting.Address1,	submissionlisting.Address2, 
						submissionlisting.Address3, submissionlisting.City, submissionlisting.State,
						submissionlisting.Zip, detaillisting_optimize.CellPhone, detaillisting_optimize.WorkPhone,
						detaillisting_optimize.Fax, detaillisting_optimize.CompanyWebsite, submissionlisting.Email, 
						
						CONCAT(user.FirstName, ' ', user.LastName) AS UserFullName, 
						user.Username AS User_Email, user.Website AS LO_Company,
						user.Title AS User_Title,
						user.Address AS User_Address, 
						user.City AS User_City, user.State AS User_State, user.Zipcode AS User_Zip, 
						user.Company AS User_Website,
						user.ContactPhone, user.ContactCell, submissionlisting.SubmissionListingID as LeadID
					FROM
						submissionlisting
					JOIN
						`user`
						ON user.UserID = submissionlisting.UserID 
					JOIN
						companyuser
						ON user.UserID = companyuser.UserID
					LEFT JOIN
						detaillisting_optimize
						ON submissionlisting.SubmissionListingID = detaillisting_optimize.SubmissionListingID
					LEFT JOIN
						contactevent 
						ON contactevent.ContactEventID = submissionlisting.LastContactEventID
					LEFT JOIN
						contactstatustype
						ON submissionlisting.ContactStatusTypeID = contactstatustype.ContactStatusTypeID
					WHERE
						companyuser.CompanyID = '" . intval($portal->CurrentCompany->CompanyID) . "'
						AND submissionlisting.ListingType = '" . intval($currentGroup->DefaultSLT) . "'
						AND $startWhere
						AND $endWhere
						AND Expired = 0
						AND (contactstatustype.BadContact = 0 OR submissionlisting.ContactStatusTypeID = 0)
					ORDER BY
						submissionlisting.DateAdded;";
	$report_res = $portal->mysqlDB->Query($sqlstring);
	
	$grand_tot = 0;
	$status_tot = array();
	$reportArray = array();
	$colArray = array();
	for($i = 0; $i < mysql_num_rows($report_res); $i++)
	{
		$report_row = mysql_fetch_array($report_res, MYSQL_ASSOC);
		$reportArray[$i] = $report_row;
	}
	
	foreach($reportArray[0] as $k=>$v)
	{
		$colArray[] = $k;
	}
	
	if($_GET['submit'] == 'Export To Excel')
	{
		//do download
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header('Content-type: application/force-download');
		header("Content-Transfer-Encoding: binary");
		header("Content-Disposition: attachment; filename=\"all_sales_report.csv\";" );
		header("Content-Transfer-Encoding: binary");
		
		// Build header row
		foreach($colArray as $col)
		{
			echo '"' . addslashes($col) . '",';
		}
		
		echo "\n";
		
		for($i = 0; $i < sizeof($reportArray); $i++)
		{
			foreach($colArray as $col)
			{
				echo '"' . addslashes($reportArray[$i][$col]) . '",';
			}
			echo "\n";
		}
		
		die();
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
	<title>
		<?= $portal->CurrentCompany->CompanyName ?> :: SOD All Sales Report
	</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<?php
	if($_GET['submit'] != 'Export To Excel')
	{
?>		
	<link type="text/css" media="all" rel="stylesheet" href="report_style.css" />
	<script src="../js/jscalendar/js/jscal2.js"></script>
	<script src="../js/jscalendar/js/lang/en.js"></script>
	<link rel="stylesheet" type="text/css" href="../js/jscalendar/css/jscal2.css" />
	<link rel="stylesheet" type="text/css" href="../js/jscalendar/css/steel/steel.css" />	
	<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />
<script type="text/javascript">//<![CDATA[

      var cal = Calendar.setup({
          onSelect: function(cal) { cal.hide() },
          bottomBar: true,
          dateFormat : "%m/%d/%Y"
      });

//]]></script>
<?php
	}
	else
	{
?>		
	<style type="text/css">
		<?php include("report_style.css"); ?>
	</style>
<?php
	}
?>
</head>
<body onload="<?= ($_GET['submit'] != 'Export To Excel' ? "LoadCampaigns('" . $_GET['slsid'] . "', '" . $_GET['sltid'] . "')" : "") ?>">
<?php
	if($_GET['submit'] != 'Export To Excel')
	{
?>
<form method="get" action="<?= $_SERVER['PHP_SELF'] ?>">
<div id="filter_div">
	<div id="left_filter_div">

	</div>
	<div id="right_filter_div">
		From: <input name="start_range" id="start_range" type="text" class="date_input" value="<?= strtotime($_GET['start_range']) ? date("m/d/Y", strtotime($_GET['start_range'])) : "" ?>" />
		<script type="text/javascript">
			cal.manageFields("start_range", "start_range", "%m/%d/%Y");
		</script>
		To: <input name="end_range" id="end_range" type="text" class="date_input" value="<?= strtotime($_GET['end_range']) ? date("m/d/Y", strtotime($_GET['end_range'])) : "" ?>"/>
		<script type="text/javascript">
			cal.manageFields("end_range", "end_range", "%m/%d/%Y");
		</script>
		<input name="submit" value="Go" type="submit" />
		<input name="submit" value="Export To Excel" type="submit" />
	</div>
</div>
</form>
<?php
	}
?>
<table class="report_table">
	<tr class="report_table rowheader">
<?php
	foreach($colArray as $col)
	{
?>
		<td class="report_table"><?= $col ?></td>
<?php
	}
?>
	</tr>
<?php
	for($i = 0; $i < sizeof($reportArray); $i++)
	{
?>
	<tr class="report_table<?= $i%2==0 ? ' roweven' : ' rowodd' ?>">
<?php
		foreach($colArray as $col)
		{
?>
		<td class="report_table" align="left" nowrap="nowrap"><?= $reportArray[$i][$col] ?></td>
<?php
		}
?>
	</tr>
<?php	
	}
?>
</table>
</body>
</html>