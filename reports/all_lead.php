<?php
	set_time_limit(300);
	ini_set('max_execution_time', 300);
	ini_set('memory_limit', '1024M');
	
	require("../../printable/include/mysql.inc.php");
	require("../../printable/include/optimize.printable.inc.php");
	require("../globals.php");
	
	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	
	$portal = new OptimizePortal($COMPANY_ID, $db);
	
	$currentUser = $portal->GetUser($_SESSION['currentuserid']);
			
	if(!$portal->CheckPriv($currentUser->UserID, 'report'))
	{
		header( "Location: " . $portal->CurrentCompany->Website . "home.php?message=" . urlencode( "Accessed Denied." ) );
		die();
	}

	$isAdmin = $portal->CheckPriv($currentUser->UserID, 'admin');
	$isCorpMan = $portal->CheckPriv($currentUser->UserID, 'corporatemanager');
	$isRegMan = $portal->CheckPriv($currentUser->UserID, 'regionmanager');
	$isDivMan = $portal->CheckPriv($currentUser->UserID, 'divisionmanager');

	if($isAdmin || $isCorpMan || $isRegMan || $isDivMan ) 
	{	$isMan = true;}
	else
	{	$isMan = false;}


		$regions = $portal->GetCompanyRegions();
		$divisions = $portal->GetCompanyDivisions();

		$userWhere = "1";

	// Setup Defaults and load from get string
	$allowRegions = $portal->CheckProf($currentUser->UserID, $REGIONS_PROFILE_ID) || $portal->CheckPriv($currentUser->UserID, 'admin');
	if($isAdmin || $isCorpMan || $allowRegions) {
		$currentRegion = isset($_GET['region_id']) ? $_GET['region_id'] : 'all';
		$regionWhere = $currentRegion == 'all' ? '1' : "user.RegionID = '" . intval($currentRegion) . "'"; 

		// Load options for dropdowns
		$currentDivision = isset($_GET['division_id']) ? $_GET['division_id'] : 'all';
		$divisionWhere = $currentDivision == 'all' ? '1' : "user.DivisionID = '" . intval($currentDivision) . "'";

	}	else if($isDivMan || $isRegMan) {
		$reportRegions = array($portal->GetRegion($currentRegion));
		
			if($isRegMan) {
				$currentRegion = $currentUser->RegionID;
				$currentDivision = isset($_GET['division_id']) ? $_GET['division_id'] : 'all';
				$regionWhere = "user.RegionID = '" . intval($currentRegion) . "'"; 
				$divisionWhere = $currentDivision == 'all' ? '1' : "user.DivisionID = '" . intval($currentDivision) . "'";
			} else if($isDivMan) {
				$currentRegion = $currentUser->RegionID;
				$currentDivision =  $currentUser->DivisionID;
				$regionWhere = "user.RegionID = '" . intval($currentRegion) . "'"; 
				$divisionWhere = "user.DivisionID = '" . intval($currentDivision) . "'"; 
			}
		
	}
	else if(!$isMan) {
		$currentRegion = $currentUser->RegionID;
		$currentDivision =  $currentUser->DivisionID;
		$regionWhere = "1"; 
		$divisionWhere = "1";
		$userWhere ="user.UserID = '" . intval($currentUser->UserID) . "'";	
	}
	if($_GET['slsid'] != '')
	{
		$slsWhere = "submissionlistingtype.SubmissionListingSourceID LIKE '" . intval($_GET['slsid']) . "'";
	}
	else
	{
		$slsWhere = "1";
	}
	if($_GET['sltid'] != '')
	{
		$sltWhere = "submissionlisting.ListingType LIKE '" . intval($_GET['sltid']) . "'";
	}
	else
	{
		$sltWhere = "((submissionlistingtype.UserManageable = 1 AND submissionlistingtype.SubmissionListingSourceID = 9999) || submissionlistingtype.UserManageable = 0)";
	}
	if($_GET['listingLevel'] != '')
	{
		$llWhere = "submissionlistingtype.ListingLevel LIKE '" . mysql_real_escape_string(stripslashes($_GET['listingLevel'])) . "'";
	}
	else
	{
		$llWhere = "1";
	}
	if(strtotime($_GET['start_range']))
	{
		$startWhere = "submissionlisting.DateAdded >= '" . date("Y-m-d", strtotime($_GET['start_range'])) ." 00:00:00'";
	}
	else
	{
		$startWhere = "submissionlisting.DateAdded >= '" . date("Y-m-d", strtotime("-7 days")) ." 00:00:00'";
		$_GET['start_range'] = date("Y-m-d", strtotime("-7 days"));
	}
	
	if(strtotime($_GET['end_range']))
	{
		$endWhere = "submissionlisting.DateAdded <= '" . date("Y-m-d", strtotime($_GET['end_range'])) ." 23:59:59'";
	}
	else
	{
		$endWhere = "submissionlisting.DateAdded <= '" . date("Y-m-d") ." 23:59:59'";
		$_GET['end_range'] = date("Y-m-d");
	}
	
	// Select for Report
	$sqlstring = "SELECT DISTINCT
						submissionlistingtype.ListingDescription AS 'Campaign Report Name',
						submissionlistingtype.LeadCost as 'Lead Cost', 
						contactstatustype.ContactStatus as 'Contact Status', submissionlisting.DateAdded as 'Date Added',
						submissionlisting.FirstName as 'First Name', submissionlisting.LastName as 'Last Name', submissionlisting.Company,
						submissionlisting.Email, 
						submissionlisting.Address1 as Address,	submissionlisting.City, submissionlisting.State,
						submissionlisting.Zip, submissionlisting.Barcode, submissionlisting.Phone as 'Home Phone',
						submissionlisting.Phone2 as 'Mobile Phone', detaillisting_optimize.WorkPhone as 'Work Number',
						detaillisting_optimize.Fax as 'Fax Number',
						CONCAT(user.FirstName, ' ', user.LastName) AS UserFullName, 
						user.Username AS LO_Email, user.Company AS LO_Company,
						user.Title AS LO_Title, SUBSTRING_INDEX(SUBSTRING_INDEX(user.CustomProfileAttributes, ';', 2), ':', -1) AS NMLS_Number,
						user.Address AS LO_Address, 
						user.City AS LO_City, user.State AS LO_State, user.Zipcode AS LO_Zip, 
						user.Website AS LO_Website,
						user.ContactPhone, user.ContactCell, submissionlisting.SubmissionListingID as LeadID,
						SUBSTRING_INDEX(submissionlistingtype.extensionlist, ';', 1) AS OriginalWorkgroup,
						REPLACE(REPLACE(Comments, '\n', ' '), '\r', ' ') AS Notes
					FROM
						submissionlisting
					JOIN
						`user`
						ON user.UserID = submissionlisting.UserID 
					JOIN
						companyuser
						ON user.UserID = companyuser.UserID
					LEFT JOIN
						detaillisting_optimize
						ON submissionlisting.SubmissionListingID = detaillisting_optimize.SubmissionListingID
					LEFT JOIN
						submissionlistingtype
						ON submissionlisting.ListingType = submissionlistingtype.SubmissionLIstingTypeID
					LEFT JOIN
						contactstatustype
						ON submissionlisting.ContactStatusTypeID = contactstatustype.ContactStatusTypeID
					WHERE
						companyuser.CompanyID = '" . intval($portal->CurrentCompany->CompanyID) . "'
						AND $sltWhere
						AND $startWhere
						AND $endWhere
						AND $slsWhere
						AND $llWhere
						AND $regionWhere
						AND $divisionWhere
						AND $userWhere
						AND submissionlistingtype.Disabled = 0
						AND Expired = 0
					ORDER BY
						submissionlistingtype.ListingType, submissionlisting.DateAdded;";
	$report_res = $portal->mysqlDB->Query($sqlstring);
	
	$grand_tot = 0;
	$status_tot = array();
	$reportArray = array();
	$colArray = array();
	for($i = 0; $i < mysql_num_rows($report_res); $i++)
	{
		$report_row = mysql_fetch_array($report_res, MYSQL_ASSOC);
		$reportArray[$i] = $report_row;
	}
	
	foreach($reportArray[0] as $k=>$v)
	{
		$colArray[] = $k;
	}
	
	$camps = $portal->GetSubmissionListingTypes(0, 'SortOrder, SubmissionListingTypeID', 'all', "((submissionlistingtype.UserManageable = 1 AND submissionlistingtype.SubmissionListingSourceID = 9999) || submissionlistingtype.UserManageable = 0)");	
	
	$sources = $portal->GetSubmissionListingSources();
	
	if($_GET['submit'] == 'Export To Excel')
	{
		//do download
		header("refresh:1;url=all_lead.php");
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header('Content-type: application/force-download');
		header("Content-Transfer-Encoding: binary");
		header("Content-Disposition: attachment; filename=\"all_lead_report.csv\";" );
		header("Content-Transfer-Encoding: binary");
		
		// Build header row
		foreach($colArray as $col)
		{
			echo '"' . addslashes($col) . '",';
		}
		
		echo "\n";
		
		for($i = 0; $i < sizeof($reportArray); $i++)
		{
			foreach($colArray as $col)
			{
				echo '"' . addslashes($reportArray[$i][$col]) . '",';
			}
			echo "\n";
		}
		sleep(5);
		die();
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
	<title>
		Leads on Demand :: All Lead Report
	</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<?php
	if($_GET['submit'] != 'Export To Excel')
	{
?>		
	<link type="text/css" media="all" rel="stylesheet" href="report_style.css" />
	<script src="../js/jscalendar/js/jscal2.js"></script>
	<script src="../js/jscalendar/js/lang/en.js"></script>
	<script type="text/javascript">
    function showImage(){
        document.getElementById('picon').style.display='inline';
    }
    </script>
	<link rel="stylesheet" type="text/css" href="../js/jscalendar/css/jscal2.css" />
	<link rel="stylesheet" type="text/css" href="../js/jscalendar/css/steel/steel.css" />	
	<link rel="shortcut icon" type="image/x-icon" href="../images/favicon.ico" />
<script type="text/javascript">//<![CDATA[

      var cal = Calendar.setup({
          onSelect: function(cal) { cal.hide() },
          bottomBar: true,
          dateFormat : "%m/%d/%Y"
      });

//]]></script>
<?php
	}
	else
	{
?>		
	<style type="text/css">
		<?php include("report_style.css"); ?>
	</style>
<?php
	}
?>
</head>
<body onload="<?= ($_GET['submit'] != 'Export To Excel' ? "LoadCampaigns('" . $_GET['slsid'] . "', '" . $_GET['sltid'] . "'); LoadDivision('$currentRegion', '$currentDivision');" : "") ?>">
<?php
	if($_GET['submit'] != 'Export To Excel')
	{
?>
<form method="get" action="<?= $_SERVER['PHP_SELF'] ?>">
<div id="filter_div">
	<div id="left_filter_div">
<script type="text/javascript">
	var region_array = new Array();
<?php
		foreach($regions as $r)
		{
?>
	region_array[<?= $r->RegionID ?>] = new Array();
<?php
		}
?>
</script>

		Region <select name="region_id" id="region_id" onchange="LoadDivision(this.value,'');">
			<option value="all">-- All Regions --</option>
<?php
		foreach($regions as $r) {
?>
			<?php if ($isAdmin || $isCorpMan || $allowRegions) { ?>
			<option value="<?= $r->RegionID ?>" <?= $_GET['region_id'] == $r->RegionID ? 'SELECTED' : '' ?>><?= $r->RegionName ?></option>
			<?php } else if ($currentRegion == $r->RegionID) {?>
			<option value="<?= $r->RegionID ?>" SELECTED><?= $r->RegionName ?></option>
			<?php } ?>
<?php 
		} 
?>
		</select>

<script type="text/javascript">

<?php
		foreach($divisions as $d)
		{
			if($isAdmin || $isCorpMan || $allowRegions) {
?>
				region_array[<?= $d->RegionID ?>][<?= $d->DivisionID ?>] = '<?= $d->DivisionName ?>';
<?php
			}
			else if($isRegMan && $d->RegionID == $currentUser->RegionID)
			{ ?>
				region_array[<?= $d->RegionID ?>][<?= $d->DivisionID ?>] = '<?= $d->DivisionName ?>';
<?php			}
			else if ($d->RegionID == $currentRegion && $d->DivisionID == $currentDivision)
			{ ?>
				region_array[<?= $d->RegionID ?>][<?= $d->DivisionID ?>] = '<?= $d->DivisionName ?>';
<?php			}
		}
?>

	function LoadDivision(regionID, divisionID)
	{


<?php if($isRegMan || $isDivMan || !$isMan): ?>
//for helpful dropdowns

	var regionSelect = document.getElementById("region_id");

	regionSelect[0].value = "all";
	regionSelect[0].text = "-- My Region --";

<?php endif; ?>


		var divisionSelect = document.getElementById("division_id");
		
		if(regionID != 'all')
		{
			divisionSelect.options.length = 1;
			var i = 1;
			

<?php if($isDivMan || !$isMan): ?>
//for helpful dropdowns

	divisionSelect[0].value = "all";
	divisionSelect[0].text = "-- My Division --";

<?php else: ?>

	divisionSelect[0].value = "all";
	divisionSelect[0].text = "-- All Divisions --";

<?php endif; ?>

			for(var id in region_array[regionID])
			{
				divisionSelect.options.length++;
				divisionSelect[i].value = id;
				divisionSelect[i].text = region_array[regionID][id];
				i++;
			}
			divisionSelect.value = divisionID;
		}
		else
		{
			divisionSelect.options.length = 1;
			var i = 1;
<?php if($isDivMan || !$isMan): ?>
//for helpful dropdowns

	divisionSelect[0].value = "all";
	divisionSelect[0].text = "-- My Division --";

<?php else: ?>

	divisionSelect[0].value = "all";
	divisionSelect[0].text = "-- All Divisions --";

<?php endif; ?>
			for(var rid in region_array)
			{
				for(var id in region_array[rid])
				{
					divisionSelect.options.length++;
					divisionSelect[i].value = id;
					divisionSelect[i].text = region_array[rid][id];
					i++;
				}
			}
			divisionSelect.value = divisionID;
		}
	}
</script>
		Division <select name="division_id" id="division_id">
		</select>
<script type="text/javascript">
	var slt_array = new Array();
	slt_array[9999] = new Array();
<?php
		foreach($sources as $s)
		{
?>
	slt_array[<?= $s->SubmissionListingSourceID ?>] = new Array();
<?php
		}
?>
</script>
		<select name="slsid" onchange="LoadCampaigns(this.value, '')">
			<option value="">-- All Sources --</option>
<?php
		foreach($sources as $s)
		{
?>
			<option value="<?= $s->SubmissionListingSourceID ?>" <?= $_GET['slsid'] == $s->SubmissionListingSourceID ? "SELECTED" : "" ?>><?= $s->ListingSourceName ?></option>
<?php
		}
?>
			<option value="9999" <?= $_GET['slsid'] == "9999" ? "SELECTED" : "" ?>>Other Leads</option>
		</select>
<script type="text/javascript">
<?php
		foreach($camps as $c)
		{
			$sourceID = $c->SubmissionListingSourceID != "" ? intval($c->SubmissionListingSourceID) : "999";
?>
	slt_array[<?= $sourceID ?>][<?= $c->SubmissionListingTypeID ?>] = '<?= $c->ListingDescription ?>';
<?php
		}
?>

	function LoadCampaigns(sourceID, campID)
	{
		var campSelect = document.getElementById("sltid");
		
		if(sourceID != '')
		{
			campSelect.options.length = 1;
			var i = 1;
			campSelect[0].value = "";
			campSelect[0].text = "-- All Lead Sources --";
			for(var id in slt_array[sourceID])
			{
				campSelect.options.length++;
				campSelect[i].value = id;
				campSelect[i].text = slt_array[sourceID][id];
				i++;
			}
			campSelect.value = campID;
		}
		else
		{
			campSelect.options.length = 1;
			var i = 1;
			campSelect[0].value = "";
			campSelect[0].text = "-- All Lead Sources --";
			for(var sid in slt_array)
			{
				for(var id in slt_array[sid])
				{
					campSelect.options.length++;
					campSelect[i].value = id;
					campSelect[i].text = slt_array[sid][id];
					i++;
				}
			}
			campSelect.value = campID;
		}
	}
</script>
		<select name="sltid" id="sltid">
			<option value="">-- All Lead Sources --</option>
<?php
		foreach($camps as $c)
		{
?>
			<option value="<?= $c->SubmissionListingTypeID ?>" <?= $_GET['sltid'] == $c->SubmissionListingTypeID ? "SELECTED" : "" ?>><?= $c->ListingDescription ?></option>
<?php
		}
?>
		</select>		
		<div id="filter_row_2">
			<select name="listingLevel">
				<option value="">-- All Campaign Types --</option>
<?php
	foreach($leadLevels as $l)
	{
?>
					<option value="<?= $l ?>"
						<?= $l == $_GET['listingLevel'] ? "SELECTED" : "" ?>>
						<?= $l ?>
					</option>
<?php
	}
?>
			</select>
		</div>
	</div>
	<div id="right_filter_div">
		From: <input name="start_range" id="start_range" type="text" class="date_input" value="<?= strtotime($_GET['start_range']) ? date("m/d/Y", strtotime($_GET['start_range'])) : "" ?>" />
		<script type="text/javascript">
			cal.manageFields("start_range", "start_range", "%m/%d/%Y");
		</script>
		To: <input name="end_range" id="end_range" type="text" class="date_input" value="<?= strtotime($_GET['end_range']) ? date("m/d/Y", strtotime($_GET['end_range'])) : "" ?>"/>
		<script type="text/javascript">
			cal.manageFields("end_range", "end_range", "%m/%d/%Y");
		</script>
		<!--<input name="submit" value="Go" type="submit" />-->
		<input name="submit" value="Export To Excel" type="submit" onclick="showImage()" />
	</div>
</div>
</form>
<?php
	}
	//disabled 3/31/15
	/*
?>
<table class="report_table">
	<tr class="report_table rowheader">
<?php
	foreach($colArray as $col)
	{
?>
		<td class="report_table"><?= $col ?></td>
<?php
	}
?>
	</tr>
<?php
	for($i = 0; $i < sizeof($reportArray); $i++)
	{
?>
	<tr class="report_table<?= $i%2==0 ? ' roweven' : ' rowodd' ?>">
<?php
		foreach($colArray as $col)
		{
?>
		<td class="report_table" align="right"><?= $reportArray[$i][$col] ?></td>
<?php
		}
?>
	</tr>
<?php	
	}
?>
</table>
<?php */ ?>
<div style="text-align: center;">
<p><strong style="font-size: 14px;">Please Note: Depending on your search criteria, your report may take a few minutes to generate.</strong></p>
<img style="display:none;" id="picon" src="images/processingicon.gif">
</div>
</body>
</html>