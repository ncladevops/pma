<?php
require("../../printable/include/mysql.inc.php");
require("../../printable/include/optimize.printable.inc.php");
require("../globals.php");

$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);

$portal = new OptimizePortal($COMPANY_ID, $db);

$currentUser = $portal->GetUser($_SESSION['currentuserid']);

if (!$portal->CheckPriv($currentUser->UserID, 'report')) {
    header("Location: " . $portal->CurrentCompany->Website . "home.php?message=" . urlencode("Accessed Denied."));
    die();
}

$isCorpMan = $portal->CheckPriv($currentUser->UserID, 'corporatemanager');
$isRegMan = $portal->CheckPriv($currentUser->UserID, 'regionmanager');
$isDivMan = $portal->CheckPriv($currentUser->UserID, 'divisionmanager');

$statuses = $portal->GetContactStatusTypes();

$camps = $portal->GetSubmissionListingTypes(0, 'SortOrder, SubmissionListingTypeID', 'all', "((submissionlistingtype.UserManageable = 1 AND submissionlistingtype.SubmissionListingSourceID = 9999) || submissionlistingtype.UserManageable = 0)");
$campLookup = array();
foreach ($camps as $c) {
    $campLookup[$c->SubmissionListingTypeID] = $c->ListingType;
}

$ss = $portal->GetSupervisors();
$supers = array();
$superLookup = array();
foreach ($ss as $s) {
    if ($isCorpMan || ($isRegMan && $s->RegionID == $currentUser->RegionID) || ($isDivMan && $s->DivisionID == $currentUser->DivisionID)) {

        $superLookup[$s->UserID] = "$s->FirstName $s->LastName";
        $supers[] = $s;
    }
}

$us = $portal->GetCompanyUsers();
$users = array();
$userLookup = array();
foreach ($us as $u) {
    if ($isCorpMan || ($isRegMan && $u->RegionID == $currentUser->RegionID) || ($isDivMan && $u->DivisionID == $currentUser->DivisionID)) {

        $userLookup[$u->UserID] = "$u->FirstName $u->LastName";
        $users[] = $u;
    }
}

$states = $portal->GetStateArray();

$sources = $portal->GetSubmissionListingSources();
$sourceLookup = array(9999 => 'Manual Leads');
foreach ($sources as $s) {
    $sourceLookup[$s->SubmissionListingSourceID] = $s->ListingSourceName;
}

$csts = $portal->GetContactStatusTypes('all', 1, true);
$chartStatusLookup = array();
$statusLookup = array();
foreach ($csts as $cst) {
    $statusLookup[$cst->ContactStatusTypeID] = $cst->ContactStatus;
    if ($cst->ShowChart)
        $chartStatusLookup[$cst->ContactStatusTypeID] = $cst->ContactStatus;
}

$regionLookup = array();
if ($isCorpMan) {
    $rs = $portal->GetCompanyRegions();
    foreach ($rs as $r) {
        $regionLookup[$r->RegionID] = $r->RegionName;
    }
}

$divisionLookup = array();
if ($isRegMan || $isCorpMan) {
    $ds = $portal->GetCompanyDivisions();
    foreach ($ds as $d) {
        if ($isCorpMan || $d->RegionID == $currentUser->RegionID) {
            $divisionLookup[$d->DivisionID] = $d->DivisionName;
        }
    }
}

$prods = $portal->GetProducts();
$emailNameLookup = array();
foreach ($prods as $p) {
    if ($p->PrintableProductID == 0)
        $emailNameLookup[$p->ProductName] = $p->ProductName;
}

$posts = $portal->GetPostings();
$postingLookup = array();
foreach ($posts as $p) {
    $postingLookup[$p->PostingName] = $p->PostingName;
}

$postTypeLookup = array("facebook" => "Facebook",
    "twitter" => "Twitter",
    "linkedin" => "LinkedIn");

$dashboard = $portal->GetDashboard($_GET['dbid']);

if (!$dashboard) {
    die("Invalid Dashboard");
}

$reports = $portal->GetDashBoardReports($dashboard->DashboardID);

$startDate = (strtotime($_GET['start_range']) ? $_GET['start_range'] : date("Y-m-d", strtotime("-1 month")));
$_GET['start_range'] = date("m/d/Y", strtotime($startDate));
$endDate = (strtotime($_GET['end_range']) ? $_GET['end_range'] : date("Y-m-d"));
$_GET['end_range'] = date("m/d/Y", strtotime($endDate));
$superWhere = (isset($superLookup[$_GET['supervisorid']]) ? $superLookup[$_GET['supervisorid']] : "");
$campWhere = (isset($campLookup[$_GET['sltid']]) ? $campLookup[$_GET['sltid']] : "");
$sourceWhere = (isset($sourceLookup[$_GET['slsid']]) ? $sourceLookup[$_GET['slsid']] : "");
$userWhere = (isset($userLookup[$_GET['userid']]) ? $userLookup[$_GET['userid']] : "");
$statusStartDate = (strtotime($_GET['status_start_range']) ? $_GET['status_start_range'] : date("Y-m-d", strtotime("-1 month")));
$_GET['status_start_range'] = date("m/d/Y", strtotime($statusStartDate));
$statusEndDate = (strtotime($_GET['status_end_range']) ? $_GET['status_end_range'] : date("Y-m-d"));
$_GET['status_end_range'] = date("m/d/Y", strtotime($statusEndDate));
$chartStatusWhere = (isset($chartStatusLookup[$_GET['chartstatusid']]) ? $chartStatusLookup[$_GET['chartstatusid']] : "");
$statusWhere = (isset($statusLookup[$_GET['statusid']]) ? $statusLookup[$_GET['statusid']] : "");
$regionWhere = (isset($regionLookup[$_GET['regionid']]) ? $_GET['regionid'] : "");
$divisionWhere = (isset($divisionLookup[$_GET['divisionid']]) ? $_GET['divisionid'] : "");
$eventStartDate = (strtotime($_GET['event_start_range']) ? $_GET['event_start_range'] : date("Y-m-d", strtotime("-1 month")));
$_GET['event_start_range'] = date("m/d/Y", strtotime($eventStartDate));
$eventEndDate = (strtotime($_GET['event_end_range']) ? $_GET['event_end_range'] : date("Y-m-d"));
$_GET['event_end_range'] = date("m/d/Y", strtotime($eventEndDate));
$emailNameWhere = (isset($emailNameLookup[$_GET['emailname']]) ? $emailNameLookup[$_GET['emailname']] : "");
$postNameWhere = (isset($postingLookup[$_GET['postname']]) ? $postingLookup[$_GET['postname']] : "");
$postTypeWhere = (isset($postTypeLookup[$_GET['posttype']]) ? $postTypeLookup[$_GET['posttype']] : "");

$queryString = "?cmd=search";
if (array_search('Dates', $dashboard->Filters) !== false) {
    $queryString .= "&so_DateAdded=BETWEEN&sv_DateAdded=" . urlencode(date("m/d/Y", strtotime($startDate))) .
        "&sv2_DateAdded=" . urlencode(date("m/d/Y", strtotime($endDate)));
}
if (array_search('Supervisors', $dashboard->Filters) !== false) {
    $queryString .= "&sv_RepSuperName=" . urlencode($superWhere);
}
if (array_search('Sources', $dashboard->Filters) !== false) {
    $queryString .= "&sv_ListingSourceName=" . urlencode($sourceWhere);
}
if (array_search('Users', $dashboard->Filters) !== false) {
    $queryString .= "&sv_RepName=" . urlencode($userWhere);
}
if (array_search('ChartContactStatus', $dashboard->Filters) !== false) {
    $queryString .= "&sv_ChartContactStatus=" . urlencode($chartStatusWhere);
}
if (array_search('ContactStatus', $dashboard->Filters) !== false) {
    $queryString .= "&sv_ContactStatus=" . urlencode($statusWhere);
}
if (array_search('LeadID', $dashboard->Filters) !== false) {
    $queryString .= "&so_LeadID=LIKE&sv_LeadID=" . urlencode($_GET['leadid']);
}
if (array_search('StatusChangeDates', $dashboard->Filters) !== false) {
    $queryString .= "&so_LastStatusChange=BETWEEN&sv_LastStatusChange=" . urlencode(date("m/d/Y", strtotime($statusStartDate))) .
        "&sv2_LastStatusChange=" . urlencode(date("m/d/Y", strtotime($statusEndDate)));
}
if (array_search('Sources', $dashboard->Filters) !== false) {
    $queryString .= "&sv_ListingTypeName=" . urlencode($campWhere);
}
if (array_search('RepRegionID', $dashboard->Filters) !== false && $regionWhere != "") {
    $queryString .= "&sv_RepRegionID=" . urlencode($regionWhere);
}
if (array_search('RepDivisionID', $dashboard->Filters) !== false && $divisionWhere != "") {
    $queryString .= "&sv_RepDivisionID=" . urlencode($divisionWhere);
}
if (array_search('EventTime', $dashboard->Filters) !== false) {
    $queryString .= "&so_EventTime=BETWEEN&sv_EventTime=" . urlencode(date("m/d/Y", strtotime($eventStartDate))) .
        "&sv2_EventTime=" . urlencode(date("m/d/Y", strtotime($eventEndDate)));
}
if (array_search('EmailName', $dashboard->Filters) !== false) {
    $queryString .= "&sv_EmailName=" . urlencode($emailNameWhere);
}
if (array_search('PostingName', $dashboard->Filters) !== false) {
    $queryString .= "&sv_PostingName=" . urlencode($postNameWhere);
}
if (array_search('PostingType', $dashboard->Filters) !== false) {
    $queryString .= "&sv_PostingType=" . urlencode($postTypeWhere);
}
$queryString .= "&export=print";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title>
        Reports on Demand :: <?= $dashboard->DashboardName ?>
    </title>
    <meta http-equiv="content-type" content="text/html;charset=utf-8"/>
    <?php
    if ($_GET['submit'] != 'Export To Excel') {
        ?>
        <link rel="stylesheet" type="text/css" href="print.css" media="print"/>
        <script type="text/javascript">
            function resizeIframe(iframe) {
                iframe.style.height = iframe.contentWindow.document.body.scrollHeight + "px";
                iframe.height = iframe.contentWindow.document.body.scrollHeight + "px";
            }

        </script>
        <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico"/>

    <?php }
    ?>

    <?php include("bootstrap.php") ?>

</head>
<body
    onload="<?= ($_GET['submit'] != 'Export To Excel' ? "LoadCampaigns('" . $_GET['slsid'] . "', '" . $_GET['sltid'] . "')" : "") ?>">
<?php
if ($_GET['submit'] != 'Export To Excel') {
    ?>
    <div class="row">
        <div id="optifiheader">
            <a class="navbar-brand"
               href="#"><?= $portal->GetCustomMessage('HeaderLeft', false, $currentUser->GroupID)->Message; ?></a>
        </div>
    </div>

    <div class="row">
        <form method="get" action="<?= $_SERVER['PHP_SELF'] ?>">
            <div class="well">
                <div class="row container">
                    <div id="filter_label_div">
                        Report Filter Options:
                    </div>
                </div>
                <div class="row">
                    <div id="left_filter_div" class="col-md-9">
                        <?php
                        if (array_search('Sources', $dashboard->Filters) !== false) {
                            ?>
                            <script type="text/javascript">
                                var slt_array = new Array();
                                slt_array[9999] = new Array();
                                <?php
                                foreach ($sources as $s) {
                                    ?>
                                slt_array[<?= $s->SubmissionListingSourceID ?>] = new Array();
                                <?php
                            }
                            ?>
                            </script>
                            <div class="col-md-3">
                                <select class="form-submenu input-sm" name="slsid"
                                        onchange="LoadCampaigns(this.value, '')" style="width: 200px;">
                                    <option value="">-- All Sources --</option>
                                    <?php
                                    foreach ($sources as $s) {
                                        ?>
                                        <option
                                            value="<?= $s->SubmissionListingSourceID ?>" <?= $_GET['slsid'] == $s->SubmissionListingSourceID ? "SELECTED" : "" ?>><?= $s->ListingSourceName ?></option>
                                        <?php
                                    }
                                    ?>
                                    <option value="9999" <?= $_GET['slsid'] == "9999" ? "SELECTED" : "" ?>>Other Leads
                                    </option>
                                </select>
                            </div>
                            <script type="text/javascript">
                                <?php
                                foreach ($camps as $c) {
                                    $sourceID = $c->SubmissionListingSourceID != "" ? intval($c->SubmissionListingSourceID) : "9999";
                                    ?>
                                slt_array[<?= $sourceID ?>][<?= $c->SubmissionListingTypeID ?>] = '<?= $c->ListingDescription ?>';
                                <?php
                            }
                            ?>

                                function LoadCampaigns(sourceID, campID) {
                                    var campSelect = document.getElementById("sltid");

                                    if (sourceID != '') {
                                        campSelect.options.length = 1;
                                        var i = 1;
                                        campSelect[0].value = "";
                                        campSelect[0].text = "-- All Campaigns --";
                                        for (var id in slt_array[sourceID]) {
                                            campSelect.options.length++;
                                            campSelect[i].value = id;
                                            campSelect[i].text = slt_array[sourceID][id];
                                            i++;
                                        }
                                        campSelect.value = campID;
                                    }
                                    else {
                                        campSelect.options.length = 1;
                                        var i = 1;
                                        campSelect[0].value = "";
                                        campSelect[0].text = "-- All Campaigns --";
                                        for (var sid in slt_array) {
                                            for (var id in slt_array[sid]) {
                                                campSelect.options.length++;
                                                campSelect[i].value = id;
                                                campSelect[i].text = slt_array[sid][id];
                                                i++;
                                            }
                                        }
                                        campSelect.value = campID;
                                    }
                                }
                            </script>
                            <?php
                        }
                        if (array_search('Supervisors', $dashboard->Filters) !== false) {
                            ?>
                            <div class="col-md-3">
                                <select class="form-submenu input-sm" name="supervisorid" style="width: 200px;">
                                    <option value="">-- All Teams --</option>
                                    <?php
                                    foreach ($supers as $s) {
                                        ?>
                                        <option
                                            value="<?= $s->UserID ?>" <?= $_GET['supervisorid'] == $s->UserID ? "SELECTED" : "" ?>><?= "$s->FirstName $s->LastName" ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <?php
                        }
                        if (array_search('Users', $dashboard->Filters) !== false) {
                            ?>
                            <div class="col-md-3">
                                <select class="form-submenu input-sm" name="userid" style="width: 200px;">
                                    <option value="">-- All Users --</option>
                                    <?php
                                    foreach ($users as $u) {
                                        ?>
                                        <option
                                            value="<?= $u->UserID ?>" <?= $_GET['userid'] == $u->UserID ? "SELECTED" : "" ?>><?= "$u->FirstName $u->LastName" ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <?php
                        }
                        if (array_search('PostingType', $dashboard->Filters) !== false) {
                            ?>
                            <div class="col-md-3">
                                <select class="form-submenu input-sm" name="posttype" style="width: 200px;">
                                    <option value="">-- All Post Types --</option>
                                    <?php
                                    foreach ($postTypeLookup as $k => $v) {
                                        ?>
                                        <option
                                            value="<?= $k ?>" <?= $_GET['posttype'] == $k ? "SELECTED" : "" ?>><?= "$v" ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <?php
                        }
                        if (array_search('LeadID', $dashboard->Filters) !== false) {
                            ?>
                            Lead ID: <input name="leadid" id="leadid" type="text" class="input-sm form-submenu"
                                            value="<?= $_GET['leadid'] ?>" placeholder="Enter Lead ID"/>
                            <?php
                        }
                        if (array_search('Dates', $dashboard->Filters) !== false) {
                            ?>
                            Date Added From: <input name="start_range" id="start_range" type="text"
                                                    class="input-sm form-submenu datepicker date_input"
                                                    value="<?= strtotime($_GET['start_range']) ? date("m/d/Y", strtotime($_GET['start_range'])) : "" ?>"/>

                            To: <input name="end_range" id="end_range" type="text"
                                       class="input-sm form-submenu datepicker date_input"
                                       value="<?= strtotime($_GET['end_range']) ? date("m/d/Y", strtotime($_GET['end_range'])) : "" ?>"/>
                            <?php
                        }
                        if (array_search('EventTime', $dashboard->Filters) !== false) {
                            ?>
                            Event Time From: <input name="event_start_range" id="event_start_range" type="text"
                                                    class="input-sm form-submenu datepicker date_input"
                                                    value="<?= strtotime($_GET['event_start_range']) ? date("m/d/Y", strtotime($_GET['event_start_range'])) : "" ?>"/>
                            To: <input name="event_end_range" id="event_end_range" type="text"
                                       class="input-sm form-submenu datepicker date_input"
                                       value="<?= strtotime($_GET['event_end_range']) ? date("m/d/Y", strtotime($_GET['event_end_range'])) : "" ?>"/>
                            <?php
                        }
                        ?>
                        <div id="filter_row_2">
                            <?php
                            if (array_search('Sources', $dashboard->Filters) !== false) {
                                ?>
                                <div class="col-md-3">
                                    <select class="form-submenu input-sm" name="sltid" id="sltid" style="width: 200px;">
                                        <option value="">-- All Campaigns --</option>
                                        <?php
                                        foreach ($camps as $c) {
                                            ?>
                                            <option
                                                value="<?= $c->SubmissionListingTypeID ?>" <?= $_GET['sltid'] == $c->SubmissionListingTypeID ? "SELECTED" : "" ?>><?= $c->ListingDescription ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <?php
                            }
                            if (array_search('States', $dashboard->Filters) !== false) {
                                ?>
                                <div class="col-md-3">
                                    <select class="form-submenu input-sm" name="statecode">
                                        <option value="">-- All States --</option>
                                        <?php
                                        foreach ($states as $k => $v) {
                                            ?>
                                            <option
                                                value="<?= $k ?>" <?= $_GET['statecode'] == $k ? "SELECTED" : "" ?>><?= $k ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <?php
                            }
                            if (array_search('Types', $dashboard->Filters) !== false) {
                                ?>
                                <div class="col-md-3">
                                    <select class="form-submenu input-sm" name="listingLevel">
                                        <option value="">-- All Campaign Types --</option>
                                        <?php
                                        foreach ($leadLevels as $l) {
                                            ?>
                                            <option value="<?= $l ?>"
                                                <?= $l == $_GET['listingLevel'] ? "SELECTED" : "" ?>>
                                                <?= $l ?>
                                            </option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <?php
                            }
                            if (array_search('ChartContactStatus', $dashboard->Filters) !== false) {
                                ?>
                                <div class="col-md-3">
                                    <select class="form-submenu input-sm" name="chartstatusid" style="width: 200px;">
                                        <option value="">-- All Statuses --</option>
                                        <?php
                                        foreach ($chartStatusLookup as $id => $cst) {
                                            ?>
                                            <option
                                                value="<?= $id ?>" <?= $_GET['chartstatusid'] == $id ? "SELECTED" : "" ?>><?= $cst ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <?php
                            }


                            if (array_search('ContactStatus', $dashboard->Filters) !== false) {
                                ?>
                                <div class="col-md-3">
                                    <select class="form-submenu input-sm" name="statusid" style="width: 200px;">
                                        <option value="">-- All Statuses --</option>
                                        <?php
                                        foreach ($statusLookup as $id => $cst) {
                                            ?>
                                            <option
                                                value="<?= $id ?>" <?= $_GET['statusid'] == $id ? "SELECTED" : "" ?>><?= $cst ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <?php
                            }
                            if ($isCorpMan && array_search('RepRegionID', $dashboard->Filters) !== false) {
                                ?>
                                <div class="col-md-3">
                                    <select class="form-submenu input-sm" name="regionid" style="width: 200px;">
                                        <option value="">-- All Regions --</option>
                                        <?php
                                        foreach ($regionLookup as $id => $rName) {
                                            ?>
                                            <option
                                                value="<?= $id ?>" <?= $_GET['regionid'] == $id ? "SELECTED" : "" ?>><?= $rName ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <?php
                            }
                            if (($isRegMan || $isCorpMan) && array_search('RepDivisionID', $dashboard->Filters) !== false) {
                                ?>
                                <div class="col-md-3">
                                    <select class="form-submenu input-sm" name="divisionid" style="width: 200px;">
                                        <option value="">-- All Divisions --</option>
                                        <?php
                                        foreach ($divisionLookup as $id => $dName) {
                                            ?>
                                            <option
                                                value="<?= $id ?>" <?= $_GET['divisionid'] == $id ? "SELECTED" : "" ?>><?= $dName ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <?php
                            }
                            if (array_search('EmailName', $dashboard->Filters) !== false) {
                                ?>
                                <div class="col-md-3">
                                    <select class="form-submenu input-sm" name="emailname" style="width: 200px;">
                                        <option value="">-- All Emails --</option>
                                        <?php
                                        foreach ($emailNameLookup as $eName) {
                                            ?>
                                            <option
                                                value="<?= $eName ?>" <?= $_GET['emailname'] == $eName ? "SELECTED" : "" ?>><?= $eName ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <?php
                            }
                            if (array_search('PostingName', $dashboard->Filters) !== false) {
                                ?>
                                <div class="col-md-3">
                                    <select class="form-submenu input-sm" name="postname" style="width: 200px;">
                                        <option value="">-- All Postings --</option>
                                        <?php
                                        foreach ($postingLookup as $p) {
                                            ?>
                                            <option
                                                value="<?= $p ?>" <?= $_GET['postname'] == $p ? "SELECTED" : "" ?>><?= $p ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <?php
                            }
                            if (array_search('StatusChangeDates', $dashboard->Filters) !== false) {
                                ?>
                                Status Change From: <input name="status_start_range" id="status_start_range" type="text"
                                                           class="input-sm form-submenu datepicker date_input"
                                                           value="<?= strtotime($_GET['status_start_range']) ? date("m/d/Y", strtotime($_GET['status_start_range'])) : "" ?>"/>

                                To: <input name="status_end_range" id="status_end_range" type="text"
                                           class="input-sm form-submenu datepicker date_input"
                                           value="<?= strtotime($_GET['status_end_range']) ? date("m/d/Y", strtotime($_GET['status_end_range'])) : "" ?>"/>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                    <div id="right_filter_div" class="col-md-3 hidden-print">
                        <input name="dbid" value="<?= $_GET['dbid'] ?>" type="hidden"/>
                        <button name="submit" class="btn btn-sm btn-default" type="submit"><i
                                class="glyphicon glyphicon-play"></i> Run Report
                        </button>
                        <button name="print_btn" class="btn btn-sm btn-default" type="button" onclick="print()"><i
                                class="glyphicon glyphicon-print"></i> Print
                        </button>
                        <?php
                        if ($dashboard->ExportExcel) {
                            ?>
                            <button name="export_btn" type="button" class="btn btn-sm btn-default"
                                    onclick="window.location = '<?= $reports[0]->URL . str_replace("export=print", "export=excel", $queryString) ?>'">
                                <i class="glyphicon glyphicon-export"></i> Export
                            </button>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <?php
}
?>

<div id="report_div" class="row container center-block">
    <div class="reportHeader">
        <h3>
            Report: <?= $dashboard->DashboardName ?>
        </h3>
    </div>
    <?php
    if (array_search('LeadID', $dashboard->Filters) !== false && $_GET['leadid'] == "") {
        die("Please enter a Lead ID");
    }
    for ($i = 0; $i < 4; $i++) {
        ?>
        <div id="ReportDashboardBlock<?= $i + 1 ?>" class="dashboardBlock">
            <?php
            if (isset($reports[$i])) {
                if ($reports[$i]->UserBased) {
                    $userString = "&sv_RepName=" . urlencode($currentUser->FirstName . " " . $currentUser->LastName);
                } else {
                    $userString = "";
                }
                $link = "";
                if ($reports[$i]->DetailDashboardID > 0) {
                    $getTemp = $_GET;
                    $getTemp['dbid'] = $reports[$i]->DetailDashboardID;
                    $getString = build_get_query($getTemp);
                    $link = "<a href='" . $_SERVER['PHP_SELF'] . "?" . $getString . "' class='btn btn-xs btn-default hidden-print'><i class='glyphicon glyphicon-list'></i> View Detail</a>";
                } else if ($reports[$i]->SummaryDashboardID > 0) {
                    $getTemp = $_GET;
                    $getTemp['dbid'] = $reports[$i]->SummaryDashboardID;
                    $getString = build_get_query($getTemp);
                    $link = "<a href='" . $_SERVER['PHP_SELF'] . "?" . $getString . "' class='btn btn-xs btn-default hidden-print'><i class='glyphicon glyphicon-stats'></i> View Summary</a>";
                } else {
                    $link = "&nbsp;";
                }

                echo $link . "<br/>\n";
                echo "<!-- " . $reports[$i]->URL . $queryString . " -->\n";
                //echo "<iframe src='load_report.php?iframe=true&url=" . urlencode($reports[$i]->URL . $queryString) . "' style='border: 0px; width: " . $reports[$i]->Width . "; height: " . $reports[$i]->Height . ";' onload='resizeIframe(this)' scrolling='no'></iframe>";
                echo "<iframe src='load_report.php?iframe=true&url=" . urlencode($reports[$i]->URL . "$queryString&$userString" ) . "' style='border: 0px; width: " . $reports[$i]->Width . "; height: " . $reports[$i]->Height . ";' onload='resizeIframe(this)' scrolling='no'></iframe>";
                //echo "<br/>" . $link;
            }
            ?>
        </div>
        <?php
    }
    ?>
    <div id="ReportDateDiv">
        Report Date: <?= date("m/d/Y g:i a") ?>
    </div>
</div>
<?php include("bootstrap-footer.php") ?>
</body>
</html>