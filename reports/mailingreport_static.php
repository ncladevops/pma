<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	<title>
		<?= $portal->CurrentCompany->CompanyName ?> Mailing Report
	</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />		
	<link type="text/css" media="all" rel="stylesheet" href="style.css" />
	<script src="../js/jscalendar/js/jscal2.js"></script>
	<script src="../js/jscalendar/js/lang/en.js"></script>
	<link rel="stylesheet" type="text/css" href="../js/jscalendar/css/jscal2.css" />
	<link rel="stylesheet" type="text/css" href="../js/jscalendar/css/win2k/win2k.css" />	
	<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />

<script type="text/javascript">//<![CDATA[

      var cal = Calendar.setup({
          onSelect: function(cal) { cal.hide() },
          bottomBar: true,
          dateFormat : "%m/%d/%Y"
      });

//]]></script>
	</head>
<body bgcolor="#FFFFFF" >
<table width="100%" cellpadding="2" cellspacing="1">
	<tr>
		<td align="left" class="headerTextLarge">
			<form method="get" action="">
				Campaign: <select name="slt">
					<option value="" >August Leads</option>
					<option value="" >July Leads</option>
					<option value="" selected>June Leads</option>
					<option value="" >April Leads</option>
				</select>
				Event: <select name="et">
					<option value="">All</option>
					<option value="1" >Call Attempted</option>
					<option value="2" >Left Message</option>
					<option value="3" >Call Completed</option>
					<option value="4" >Appointment Set</option>
					<option value="5" >Do Not Call</option>
					<option value="6" >Sold</option>
				</select>
				Mail Date: <input type="text" id="start" name="start" value="08/17/2009" size="10">
				<script type="text/javascript">
					cal.manageFields("start", "start", "%m/%d/%Y");
				</script> to 
				<input type="text" id="end" name="end" value="08/27/2009" size="10">
				<script type="text/javascript">
					cal.manageFields("end", "end", "%m/%d/%Y");
				</script>
				<input type="submit" value="View" name="Submit">
				<input type="submit" value="Export" name="Submit">
			</form>
		</td>
	</tr>
	<tr>
		<td align="left" class="headerTextLarge">
			<table bgcolor="#666666">
				<tr class="tableHeader" >
					<td width="180">
						Contact
					</td>
					<td width="420">
						Contact Address
					</td>
					<td width="240">
						Sales Exec
					</td>
					<td width="130">
						Campaign
					</td>
					<td width="120">
						Contact Type
					</td>
					<td width="120">
						Mail Time
					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						A DENTAL EMERGENCY SVC (. .)					</td>
					<td>
						4332 Slauson Ave, Maywood CA 90270-2800					</td>
					<td>
						Charles McLaurin II					</td>
					<td>
						June Leads					</td>
					<td>
						Call Attempted					</td>
					<td>
						08/18/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						A V DENTAL GRUOP (MICHAEL LEIVEROVITZ)					</td>
					<td>
						38655 9th St E # D # D, Palmdale CA 93550-3814					</td>
					<td>
						Charles McLaurin II					</td>
					<td>
						June Leads					</td>
					<td>
						Call Attempted					</td>
					<td>
						08/18/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						A V SPINAL CARE (. .)					</td>
					<td>
						42283 10th St W, Lancaster CA 93534-7073					</td>
					<td>
						Charles McLaurin II					</td>
					<td>
						June Leads					</td>
					<td>
						Call Attempted					</td>
					<td>
						08/18/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						A V URGENT CARE (JACKIE DOUTRE)					</td>
					<td>
						41210 11th St W Ste K # K, Palmdale CA 93551-1447					</td>
					<td>
						Charles McLaurin II					</td>
					<td>
						June Leads					</td>
					<td>
						Call Attempted					</td>
					<td>
						08/18/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						ACCESS MEDICAL IMAGING BILLING (STEVEN SHAPPIRO)					</td>
					<td>
						155 N San Vicente Blvd, Beverly Hills CA 90211-2303					</td>
					<td>
						Charles McLaurin II					</td>
					<td>
						June Leads					</td>
					<td>
						Call Attempted					</td>
					<td>
						08/18/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						ADVANCE CARE MEDICAL GROUP (. .)					</td>
					<td>
						1330 Fullerton Rd Ste 288 # 288, Rowland Heights CA 91748-1246					</td>
					<td>
						Charles McLaurin II					</td>
					<td>
						June Leads					</td>
					<td>
						Call Attempted					</td>
					<td>
						08/18/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						ADVANCED DERMATOLOGY & COSM (MEKA WILLIAMS)					</td>
					<td>
						23861 McBean Pkwy # E21E21, Valencia CA 91355-2058					</td>
					<td>
						Charles McLaurin II					</td>
					<td>
						June Leads					</td>
					<td>
						Call Attempted					</td>
					<td>
						08/18/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Advanced Medical Analysis LLC (Raymond Koeman)					</td>
					<td>
						1946 S Myrtle Ave, Monrovia CA 91016-4835					</td>
					<td>
						Charles McLaurin II					</td>
					<td>
						June Leads					</td>
					<td>
						Call Attempted					</td>
					<td>
						08/18/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						ADVANCED ORTHOPEDICS (DARLENE MILES)					</td>
					<td>
						23502 Lyons Ave # 202A202A, Newhall CA 91321-2535					</td>
					<td>
						Charles McLaurin II					</td>
					<td>
						June Leads					</td>
					<td>
						Call Attempted					</td>
					<td>
						08/18/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Alex Gladkov DDS (Alex Gladkov)					</td>
					<td>
						6035 Bristol Pkwy Ste 200 # 200, Culver City CA 90230-6653					</td>
					<td>
						Charles McLaurin II					</td>
					<td>
						June Leads					</td>
					<td>
						Call Attempted					</td>
					<td>
						08/18/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Alfonzo Stakeley MD (ALFONZO STAKELY)					</td>
					<td>
						22120 Avalon Blvd, Carson CA 90745-3391					</td>
					<td>
						Charles McLaurin II					</td>
					<td>
						June Leads					</td>
					<td>
						Call Attempted					</td>
					<td>
						08/18/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						STUDIO CITY MARTIAL ARTS (CHADWICK MINGE)					</td>
					<td>
						12418 Ventura Blvd, Studio City CA 91604-2406					</td>
					<td>
						Maria Frew					</td>
					<td>
						July Leads					</td>
					<td>
						Call Attempted					</td>
					<td>
						08/18/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						BEVERLY HILLS UNIVERSITY (ROBERT ZARR)					</td>
					<td>
						13351 Riverside Dr # D900D900, Sherman Oaks CA 91423-2542					</td>
					<td>
						Maria Frew					</td>
					<td>
						July Leads					</td>
					<td>
						Call Attempted					</td>
					<td>
						08/18/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						COMPUTER LADY (AMY OSTROWER)					</td>
					<td>
						4235 Mary Ellen Ave Unit 205 # 205, Studio City CA 91604-1859					</td>
					<td>
						Maria Frew					</td>
					<td>
						July Leads					</td>
					<td>
						Call Attempted					</td>
					<td>
						08/18/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						EASTERN TAE KWON DO MARTIAL (. .)					</td>
					<td>
						6817 Kester Ave, Van Nuys CA 91405-3716					</td>
					<td>
						Maria Frew					</td>
					<td>
						July Leads					</td>
					<td>
						Call Attempted					</td>
					<td>
						08/18/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						ESHLEMAN PIANO STUDIO (ROGER L ESHLEMAN)					</td>
					<td>
						14032 Bessemer St, Van Nuys CA 91401-3512					</td>
					<td>
						Maria Frew					</td>
					<td>
						July Leads					</td>
					<td>
						Call Attempted					</td>
					<td>
						08/18/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						FRANKIAN GNEL (FRANKIAN GNEL)					</td>
					<td>
						9029 Reseda Blvd, Northridge CA 91324-3932					</td>
					<td>
						Maria Frew					</td>
					<td>
						July Leads					</td>
					<td>
						Call Attempted					</td>
					<td>
						08/18/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						GOLF TEC (. .)					</td>
					<td>
						14382 Ventura Blvd, Sherman Oaks CA 91423-2756					</td>
					<td>
						Maria Frew					</td>
					<td>
						July Leads					</td>
					<td>
						Call Attempted					</td>
					<td>
						08/18/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						MICHELLE LANGLEY SCHOOL-BALLET (MICHELLE LANGLEY)					</td>
					<td>
						14252 Ventura Blvd, Sherman Oaks CA 91423-2788					</td>
					<td>
						Maria Frew					</td>
					<td>
						July Leads					</td>
					<td>
						Call Attempted					</td>
					<td>
						08/18/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						NETWORK STUDIO (. .)					</td>
					<td>
						14542 Ventura Blvd Ste 203 # 203, Sherman Oaks CA 91403-5515					</td>
					<td>
						Maria Frew					</td>
					<td>
						July Leads					</td>
					<td>
						Call Attempted					</td>
					<td>
						08/18/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						OPEN STAGE (. .)					</td>
					<td>
						14366 Ventura Blvd, Sherman Oaks CA 91423-2755					</td>
					<td>
						Maria Frew					</td>
					<td>
						July Leads					</td>
					<td>
						Call Attempted					</td>
					<td>
						08/18/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						PINECREST SCHOOLS (JANICE RUDD)					</td>
					<td>
						17081 Devonshire St, Northridge CA 91325-1616					</td>
					<td>
						Maria Frew					</td>
					<td>
						July Leads					</td>
					<td>
						Call Attempted					</td>
					<td>
						08/18/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						PREFERRED COLLEGE OF NURSING (BERNARDITE SANCHEZ)					</td>
					<td>
						6551 Van Nuys Blvd, Van Nuys CA 91401-1442					</td>
					<td>
						Maria Frew					</td>
					<td>
						July Leads					</td>
					<td>
						Call Attempted					</td>
					<td>
						08/18/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						REHAB ACCESS (ETHEL GIBSON)					</td>
					<td>
						15210 Devonshire St, Mission Hills CA 91345-2745					</td>
					<td>
						Maria Frew					</td>
					<td>
						July Leads					</td>
					<td>
						Call Attempted					</td>
					<td>
						08/18/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						SHARE THE RANCH AT EL DSTBRRY (ROBIN RAPAPORT)					</td>
					<td>
						4644 Longridge Ave, Sherman Oaks CA 91423-3220					</td>
					<td>
						Maria Frew					</td>
					<td>
						July Leads					</td>
					<td>
						Call Attempted					</td>
					<td>
						08/18/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Ability First (LORI GANGEMI)					</td>
					<td>
						3812 S Grand Ave, Los Angeles CA 90037-1336					</td>
					<td>
						Charles McLaurin II					</td>
					<td>
						June Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/18/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						ACCESS FOOT SPECIALIST GROUP (CHARLES BLAINE)					</td>
					<td>
						10728 Ramona Blvd Ste D # D, El Monte CA 91731-2601					</td>
					<td>
						Charles McLaurin II					</td>
					<td>
						June Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/18/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						ACCESS MEDICAL GROUP (Tyrone Council)					</td>
					<td>
						4644 Lincoln Blvd Ste 111 # 111, Marina del Rey CA 90292-6374					</td>
					<td>
						Charles McLaurin II					</td>
					<td>
						June Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/18/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						ADVANCED RADIOLOGY-BEVERLY HLS (Jim Difazio)					</td>
					<td>
						8641 Wilshire Blvd Ste 105 # 105, Beverly Hills CA 90211-2919					</td>
					<td>
						Charles McLaurin II					</td>
					<td>
						June Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/18/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Ali Abaian MD Medical Corp (Ali Abaian)					</td>
					<td>
						1234 E Florence Ave, Los Angeles CA 90001-2433					</td>
					<td>
						Charles McLaurin II					</td>
					<td>
						June Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/18/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						ALLERGY MEDICAL CLINIC (Lisa )					</td>
					<td>
						11620 Wilshire Blvd Ste 200 # 200, Los Angeles CA 90025-1767					</td>
					<td>
						Charles McLaurin II					</td>
					<td>
						June Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/18/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						AMERICAN VETERINARY LABS (Alvin )					</td>
					<td>
						2730 N Main St, Los Angeles CA 90031-3321					</td>
					<td>
						Charles McLaurin II					</td>
					<td>
						June Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/18/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						7-24 DRIVING SCHOOL (VICKY TOBO)					</td>
					<td>
						17031 Chatsworth St Ste 101 # 101, Granada Hills CA 91344-5883					</td>
					<td>
						Maria Frew					</td>
					<td>
						July Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/18/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						ANDREA M ROSS MABCET (. .)					</td>
					<td>
						18075 Ventura Blvd, Encino CA 91316-3517					</td>
					<td>
						Maria Frew					</td>
					<td>
						July Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/18/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						ANGELS DRIVING SCHOOL (. .)					</td>
					<td>
						6041 Whitsett Ave, North Hollywood CA 91606-4567					</td>
					<td>
						Maria Frew					</td>
					<td>
						July Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/18/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						ANIMATION CREATIONS (. .)					</td>
					<td>
						14622 Ventura Blvd, Sherman Oaks CA 91403-3600					</td>
					<td>
						Maria Frew					</td>
					<td>
						July Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/18/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						THOR CARTER STUDIO INC (CAMERON THOR)					</td>
					<td>
						12417 Ventura Ct, Studio City CA 91604-2417					</td>
					<td>
						Maria Frew					</td>
					<td>
						July Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/18/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						TALENT TESTING SVC INC (. .)					</td>
					<td>
						8444 Reseda Blvd Ste A # A, Northridge CA 91324-5977					</td>
					<td>
						Maria Frew					</td>
					<td>
						July Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/18/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						T V I Actors Studio (ALAN NUSBAUMM)					</td>
					<td>
						14429 Ventura Blvd Ste 118, Sherman Oaks CA 91423-2672					</td>
					<td>
						Maria Frew					</td>
					<td>
						July Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/18/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						ANISA S SCHOOL OF DANCE (ANISA ZUSMAN)					</td>
					<td>
						14252 Ventura Blvd, Sherman Oaks CA 91423-2788					</td>
					<td>
						Maria Frew					</td>
					<td>
						July Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/18/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						ARMENIAN EVANGELICAL SCHOOLS (HOVSEP INJEJIKIAN)					</td>
					<td>
						13330 Riverside Dr, Sherman Oaks CA 91423-2521					</td>
					<td>
						Maria Frew					</td>
					<td>
						July Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/18/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						ATLAS DRIVING & TRAFFIC SCHOOL (OLGA SOTO)					</td>
					<td>
						8774 Sepulveda Blvd Ste 1 # 1, North Hills CA 91343-5158					</td>
					<td>
						Maria Frew					</td>
					<td>
						July Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/18/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						BARNEY UNGERMANN & ASSOC (ISABELLA SCHWARTZ)					</td>
					<td>
						14545 Sylvan St, Van Nuys CA 91411-2325					</td>
					<td>
						Maria Frew					</td>
					<td>
						July Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/18/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						CHINESE SHAO-LIN CTR (. .)					</td>
					<td>
						9514 Reseda Blvd Ste 5 # 5, Northridge CA 91324-2343					</td>
					<td>
						Maria Frew					</td>
					<td>
						July Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/18/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						CLEARVIEW EDUCATIONAL SVC (. .)					</td>
					<td>
						4480 Kester Ave, Sherman Oaks CA 91403-3638					</td>
					<td>
						Maria Frew					</td>
					<td>
						July Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/18/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						CREATIVE FIRE CLAY STUDIO (. .)					</td>
					<td>
						11061 Swinton Ave, Granada Hills CA 91344-5337					</td>
					<td>
						Maria Frew					</td>
					<td>
						July Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/18/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						DIXIE CANYON AVENUE ELEM SCHL (JUDY DICHTER)					</td>
					<td>
						4220 Dixie Canyon Ave, Sherman Oaks CA 91423-3904					</td>
					<td>
						Maria Frew					</td>
					<td>
						July Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/18/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						DYNAMICS EDUCATION CTR (. .)					</td>
					<td>
						14372 Ventura Blvd, Sherman Oaks CA 91423-2755					</td>
					<td>
						Maria Frew					</td>
					<td>
						July Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/18/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						EDUCARE AMERICA (. .)					</td>
					<td>
						9249 Reseda Blvd, Northridge CA 91324-3143					</td>
					<td>
						Maria Frew					</td>
					<td>
						July Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/18/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						INDEPENDENCE ADAPTIVE DRIVING (. .)					</td>
					<td>
						8345 Reseda Blvd Ste 114, Northridge CA 91324-5928					</td>
					<td>
						Maria Frew					</td>
					<td>
						July Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/18/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						JOHN B MONLUX ELEM SCHOOL (BERT GOVENAR)					</td>
					<td>
						6051 Bellaire Ave, North Hollywood CA 91606-4461					</td>
					<td>
						Maria Frew					</td>
					<td>
						July Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/18/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						KIDS ART INC (. .)					</td>
					<td>
						13401 Ventura Blvd Ste 104 # 104, Sherman Oaks CA 91423-3986					</td>
					<td>
						Maria Frew					</td>
					<td>
						July Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/18/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						KUMON MATH & READING CTR (JANE KIM)					</td>
					<td>
						12215 Ventura Blvd Ste 116 # 116, Studio City CA 91604-2533					</td>
					<td>
						Maria Frew					</td>
					<td>
						July Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/18/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						KUMON-EAST SHERMAN OAKS (VICKIE LEE)					</td>
					<td>
						4820 Fulton Ave, Sherman Oaks CA 91423-2504					</td>
					<td>
						Maria Frew					</td>
					<td>
						July Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/18/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						LYCEE INTERNATIONAL (. .)					</td>
					<td>
						5933 Lindley Ave, Tarzana CA 91356-1723					</td>
					<td>
						Maria Frew					</td>
					<td>
						July Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/18/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						MAKE-UP ACADEMY OF HOLLYWOOD (MONET MANSANO)					</td>
					<td>
						14258 Aetna St, Van Nuys CA 91401-3433					</td>
					<td>
						Maria Frew					</td>
					<td>
						July Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/18/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						NEW HEIGHTS PREPARATORY SCHOOL (. .)					</td>
					<td>
						8756 Canby Ave, Northridge CA 91325-3006					</td>
					<td>
						Maria Frew					</td>
					<td>
						July Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/18/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						NEW HOPE ACADEMY (. .)					</td>
					<td>
						10349 Balboa Blvd Ste 205, Granada Hills CA 91344-7374					</td>
					<td>
						Maria Frew					</td>
					<td>
						July Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/18/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						OSULA EDUCATION CTR (MICHIHIRO NAKASHIMA)					</td>
					<td>
						3921 Laurel Canyon Blvd, Studio City CA 91604-3710					</td>
					<td>
						Maria Frew					</td>
					<td>
						July Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/18/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Piano Play Music Systems Inc (SHARON SHAHEED)					</td>
					<td>
						14237 Ventura Blvd, Sherman Oaks CA 91423-2714					</td>
					<td>
						Maria Frew					</td>
					<td>
						July Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/18/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						SOUTHERN CALIF LA TAE KWON DO (SCOT LEWIS)					</td>
					<td>
						14444 Ventura Blvd, Sherman Oaks CA 91423-2607					</td>
					<td>
						Maria Frew					</td>
					<td>
						July Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/18/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						ST ELISABETH ELEMENTARY SCHOOL (J. Booker)					</td>
					<td>
						6635 Tobias Ave, Van Nuys CA 91405-4614					</td>
					<td>
						Maria Frew					</td>
					<td>
						July Leads					</td>
					<td>
						Appointment Set					</td>
					<td>
						08/18/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						BRIDGES ACADEMY (CARL SABATINO)					</td>
					<td>
						3921 Laurel Canyon Blvd, Studio City CA 91604-3710					</td>
					<td>
						Maria Frew					</td>
					<td>
						July Leads					</td>
					<td>
						Call Completed					</td>
					<td>
						08/18/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						VRMAII (. .)					</td>
					<td>
						8448 Reseda Blvd Ste 107 # 107, Northridge CA 91324-5992					</td>
					<td>
						Maria Frew					</td>
					<td>
						July Leads					</td>
					<td>
						Call Completed					</td>
					<td>
						08/18/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						VECTOR 7 TUTORING (. .)					</td>
					<td>
						8949 Reseda Blvd, Northridge CA 91324-3916					</td>
					<td>
						Maria Frew					</td>
					<td>
						July Leads					</td>
					<td>
						Call Completed					</td>
					<td>
						08/18/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						VALLEY TORAH HIGH SCHOOL (LEO STRIKS)					</td>
					<td>
						12517 Chandler Blvd, Valley Village CA 91607-1925					</td>
					<td>
						Maria Frew					</td>
					<td>
						July Leads					</td>
					<td>
						Call Completed					</td>
					<td>
						08/18/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						SYLVAN LEARNING CTR (NATALIE APIKIAN)					</td>
					<td>
						13553 Ventura Blvd, Sherman Oaks CA 91423-3825					</td>
					<td>
						Maria Frew					</td>
					<td>
						July Leads					</td>
					<td>
						Call Completed					</td>
					<td>
						08/18/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						END RESULT TUTORING (SCOTT PAUKER)					</td>
					<td>
						13719 Ventura Blvd, Sherman Oaks CA 91423-3032					</td>
					<td>
						Maria Frew					</td>
					<td>
						July Leads					</td>
					<td>
						Call Completed					</td>
					<td>
						08/18/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						BARRINGTON PSYCHIATRIC CTR (JAN FRIEDMAN)					</td>
					<td>
						1990 S Bundy Dr Ste 345 # 345, Los Angeles CA 90025-5254					</td>
					<td>
						Charles McLaurin II					</td>
					<td>
						June Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/20/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Barry S Seibel MD Inc (Barry Seibel)					</td>
					<td>
						11620 Wilshire Blvd Ste 711 # 7, Los Angeles CA 90025-1781					</td>
					<td>
						Charles McLaurin II					</td>
					<td>
						June Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/20/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						BEACH CITIES DIALYSIS (LO ANDERSON)					</td>
					<td>
						1045 W Redondo Beach Blvd Ste 105 # 105, Gardena CA 90247-4184					</td>
					<td>
						Charles McLaurin II					</td>
					<td>
						June Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/20/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						CHEMICAL DEPENDENCY PROGRAM (KIM NAKAE)					</td>
					<td>
						3333 Skypark Dr Ste 200, Torrance CA 90505-5035					</td>
					<td>
						Charles McLaurin II					</td>
					<td>
						June Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/20/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						CHRISTOPHER BRAUN DC (CHRISTOPHER BRAUN)					</td>
					<td>
						1180 S Beverly Dr Ste 410, Los Angeles CA 90035-1156					</td>
					<td>
						Charles McLaurin II					</td>
					<td>
						June Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/20/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						CLEARVIEW TREATMENT PROGRAMS (MICHAEL ROY)					</td>
					<td>
						1334 Westwood Blvd Ste 3A, Los Angeles CA 90024-4951					</td>
					<td>
						Charles McLaurin II					</td>
					<td>
						June Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/20/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						CLINICA MEDICA HISPANA MEDICAL (NABILA ALBERTY)					</td>
					<td>
						14906 Paramount Blvd, Paramount CA 90723-3409					</td>
					<td>
						Charles McLaurin II					</td>
					<td>
						June Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/20/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Clinica Popular Medical Group (Daniel Berdakin)					</td>
					<td>
						PO Box 76316, Los Angeles CA 90076-0316					</td>
					<td>
						Charles McLaurin II					</td>
					<td>
						June Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/20/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Clinica Popular Number One (Daniel Bervakin)					</td>
					<td>
						5223 Laurel Canyon Blvd, N Hollywood CA 91607-2709					</td>
					<td>
						Charles McLaurin II					</td>
					<td>
						June Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/20/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						ABLE DESIGN & FABRICATION (LOUIS MANNICK)					</td>
					<td>
						1550 W Mahalo Pl, Compton CA 90220-5422					</td>
					<td>
						Cynthia Brewer					</td>
					<td>
						August Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/20/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						ABF FREIGHT SYSTEM INC (John Demary)					</td>
					<td>
						405 E Alondra Blvd, Compton CA 90221-4354					</td>
					<td>
						Cynthia Brewer					</td>
					<td>
						August Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/20/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						AAA PLATING & INSPECTION INC (Howard Litlle)					</td>
					<td>
						424 E Dixon St, Compton CA 90222-1420					</td>
					<td>
						Cynthia Brewer					</td>
					<td>
						August Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/20/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Aawb Inc (Dena dellollio)					</td>
					<td>
						335 N Maple Dr Ste 351 # 351, Beverly Hills CA 90210-5174					</td>
					<td>
						Cynthia Brewer					</td>
					<td>
						August Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/20/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						AAA INSURANCE (George Burkhart)					</td>
					<td>
						4512 Sepulveda Blvd, Culver City CA 90230-4818					</td>
					<td>
						Cynthia Brewer					</td>
					<td>
						August Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/20/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						A-1 GILBERT TELEPHONE ANSWRNG (PETER MINTZ)					</td>
					<td>
						9304 Civic Center Dr Ste 2 # 2, Beverly Hills CA 90210-3622					</td>
					<td>
						Cynthia Brewer					</td>
					<td>
						August Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/20/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						A T TRAMP (LEE TANAKA)					</td>
					<td>
						9664 Brighton Way, Beverly Hills CA 90210-5126					</td>
					<td>
						Cynthia Brewer					</td>
					<td>
						August Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/20/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						A NICE JEWISH BOY MOVING-STGE (Tony manager)					</td>
					<td>
						9777 Wilshire Blvd, Beverly Hills CA 90212-1910					</td>
					<td>
						Cynthia Brewer					</td>
					<td>
						August Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/20/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						9 TO 5 SEATING (DARIUS MIR)					</td>
					<td>
						2029 E Cashdan St, Rancho Dominguez CA 90220-6424					</td>
					<td>
						Cynthia Brewer					</td>
					<td>
						August Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/20/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						2G DIGITAL (. .)					</td>
					<td>
						10600 Virginia Ave, Culver City CA 90232-3514					</td>
					<td>
						Cynthia Brewer					</td>
					<td>
						August Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/20/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						14-KARATS LTD (Helen Douglas)					</td>
					<td>
						314 S Beverly Dr, Beverly Hills CA 90212-4801					</td>
					<td>
						Cynthia Brewer					</td>
					<td>
						August Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/20/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						STAR REAL ESTATE (DEAN ZITKO)					</td>
					<td>
						16875 Algonquin St, Huntington Beach CA 92649-3810					</td>
					<td>
						Brant Bishop					</td>
					<td>
						January Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/20/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						SUNNY SLOPE TREES (TODD FLAMMER)					</td>
					<td>
						4025 E La Palma Ave Ste 203 # 203, Anaheim CA 92807-1743					</td>
					<td>
						Brant Bishop					</td>
					<td>
						January Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/20/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						SUREFIRE LLC (JOHN MATTHEWS)					</td>
					<td>
						18300 Mount Baldy Cir, Fountain Valley CA 92708-6122					</td>
					<td>
						Brant Bishop					</td>
					<td>
						January Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/20/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						T M A D  Engineers Inc (ALBERT CHIU)					</td>
					<td>
						100 S Anaheim Blvd Ste 150, Anaheim CA 92805-3870					</td>
					<td>
						Brant Bishop					</td>
					<td>
						January Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/20/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Sharp Electronics Corporation (ROSE ORTEGA)					</td>
					<td>
						5901 Bolsa Ave, Huntington Beach CA 92647-2053					</td>
					<td>
						Brant Bishop					</td>
					<td>
						January Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/20/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						SHELLY BMW PARTS (MIKE RIDGE)					</td>
					<td>
						6750 Auto Center Dr, Buena Park CA 90621-2900					</td>
					<td>
						Brant Bishop					</td>
					<td>
						January Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/20/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						SHEPPARD MULLIN & RICHTER (. .)					</td>
					<td>
						650 Town Center Dr Fl 4 # 400, Costa Mesa CA 92626-1993					</td>
					<td>
						Brant Bishop					</td>
					<td>
						January Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/20/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						BRAVO TECH INC ( .)					</td>
					<td>
						6185 Phyllis Dr # D, Cypress CA 90630-5242					</td>
					<td>
						Nicole Knight					</td>
					<td>
						March Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/20/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Brunswick Bowling & Billd Corp (Del Parsons)					</td>
					<td>
						3333 Harbor Blvd, Costa Mesa CA 92626-1501					</td>
					<td>
						Nicole Knight					</td>
					<td>
						March Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/20/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						SOUTHERN CALIFORNIA PIPELINE (JOEL SPEAR)					</td>
					<td>
						250 El Camino Real Ste 208 # 208, Tustin CA 92780-3656					</td>
					<td>
						Brant Bishop					</td>
					<td>
						January Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/20/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						SHELTON CONSTRUCTION (MICHELLE SPENCE)					</td>
					<td>
						12802 Valley View St Ste 9 # 9, Garden Grove CA 92845-2511					</td>
					<td>
						Brant Bishop					</td>
					<td>
						January Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/20/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						STAR REAL ESTATE (TERRY REAY)					</td>
					<td>
						19440 Goldenwest St, Huntington Beach CA 92648-2116					</td>
					<td>
						Brant Bishop					</td>
					<td>
						January Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/20/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						STEPHEN WILSON CONSTRUCTION CO (STEPHEN WILSON)					</td>
					<td>
						1145 E Stanford Ct, Anaheim CA 92805-6822					</td>
					<td>
						Brant Bishop					</td>
					<td>
						January Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/20/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						SUN REAL ESTATE TEAM INC (TONY TALBOT)					</td>
					<td>
						10773 Los Alamitos Blvd, Los Alamitos CA 90720-2309					</td>
					<td>
						Brant Bishop					</td>
					<td>
						January Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/20/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						A & B TOWING (SUSAN KATSUMOTO)					</td>
					<td>
						2956 Randolph Ave, Costa Mesa CA 92626-4312					</td>
					<td>
						Robin Kang					</td>
					<td>
						May Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/20/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						BEACH CITIES DIALYSIS (LO ANDERSON)					</td>
					<td>
						20911 Earl St Ste 160 # 160, Torrance CA 90503-4353					</td>
					<td>
						Charles McLaurin II					</td>
					<td>
						June Leads					</td>
					<td>
						Call Completed					</td>
					<td>
						08/20/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						BEHAVIORAL ASSESSMENT (RICHARD C CERVANTES)					</td>
					<td>
						291 S La Cienega Blvd Ste 308 # 308, Beverly Hills CA 90211-3310					</td>
					<td>
						Charles McLaurin II					</td>
					<td>
						June Leads					</td>
					<td>
						Call Completed					</td>
					<td>
						08/20/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						CHILD & FAMILY GUIDANCE CTR (Kim Barren)					</td>
					<td>
						9650 Zelzah Ave, Northridge CA 91325-2003					</td>
					<td>
						Charles McLaurin II					</td>
					<td>
						June Leads					</td>
					<td>
						Call Completed					</td>
					<td>
						08/20/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						CHILDREN S DENTAL BUILDING (LEE MICHIE)					</td>
					<td>
						11635 South St, Artesia CA 90701-6628					</td>
					<td>
						Charles McLaurin II					</td>
					<td>
						June Leads					</td>
					<td>
						Call Completed					</td>
					<td>
						08/20/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						HOUSE DOCTOR (David. Hoffler.)					</td>
					<td>
						1544 Cotner Ave, Los Angeles CA 90025-3303					</td>
					<td>
						Richard Fissel					</td>
					<td>
						February Leads					</td>
					<td>
						Call Completed					</td>
					<td>
						08/20/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						ISMAIL ASSOCIATES (ISMAIL GERMIYANOGLU)					</td>
					<td>
						11022 Santa Monica Blvd Ste 290 # 290, Los Angeles CA 90025-7531					</td>
					<td>
						Richard Fissel					</td>
					<td>
						February Leads					</td>
					<td>
						Call Completed					</td>
					<td>
						08/20/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						BODYCOTE (GEOFF MONTI)					</td>
					<td>
						7474 Garden Grove Blvd, Westminster CA 92683-2227					</td>
					<td>
						Nicole Knight					</td>
					<td>
						March Leads					</td>
					<td>
						Call Completed					</td>
					<td>
						08/20/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Bowers (Paul Dowdle)					</td>
					<td>
						2002 N Main St, Santa Ana CA 92706-2731					</td>
					<td>
						Nicole Knight					</td>
					<td>
						March Leads					</td>
					<td>
						Call Completed					</td>
					<td>
						08/20/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						BOYD & ASSOC ( )					</td>
					<td>
						2002 N Broadway, Santa Ana CA 92706-2612					</td>
					<td>
						Nicole Knight					</td>
					<td>
						March Leads					</td>
					<td>
						Call Completed					</td>
					<td>
						08/20/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						BURCH FORD (MARTY BURCH)					</td>
					<td>
						PO Box 518, La Habra CA 90633-0518					</td>
					<td>
						Nicole Knight					</td>
					<td>
						March Leads					</td>
					<td>
						Call Completed					</td>
					<td>
						08/20/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						CB Richard Ellis  Inc (Jeff Moore)					</td>
					<td>
						2125 E Katella Ave, Anaheim CA 92806-6072					</td>
					<td>
						Nicole Knight					</td>
					<td>
						March Leads					</td>
					<td>
						Call Completed					</td>
					<td>
						08/20/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						CCECO (CLAUDIA MANAVI)					</td>
					<td>
						130 McCormick Ave Ste 113 # 113, Costa Mesa CA 92626-3316					</td>
					<td>
						Nicole Knight					</td>
					<td>
						March Leads					</td>
					<td>
						Call Completed					</td>
					<td>
						08/20/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Century 21 Beachside Realtors (THOMAS DENNY)					</td>
					<td>
						19671 Beach Blvd Ste 101 # 101, Huntington Beach CA 92648-5902					</td>
					<td>
						Nicole Knight					</td>
					<td>
						March Leads					</td>
					<td>
						Call Completed					</td>
					<td>
						08/20/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						BOWMAN & BROOKE (MARK BERRY)					</td>
					<td>
						879 W 190th St Ste 700, Gardena CA 90248-4227					</td>
					<td>
						Michele D'Amico					</td>
					<td>
						LANorthLeads					</td>
					<td>
						Call Attempted					</td>
					<td>
						08/27/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						BUDGET INN (TOM LAXMUDS)					</td>
					<td>
						9014 Long Beach Blvd, South Gate CA 90280-2855					</td>
					<td>
						Wayne F. Burt Jr.					</td>
					<td>
						Special Prospects					</td>
					<td>
						Call Attempted					</td>
					<td>
						08/27/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						BUDGET LODGE (KELLY .)					</td>
					<td>
						668 Fairway Dr, San Bernardino CA 92408-2707					</td>
					<td>
						Wayne F. Burt Jr.					</td>
					<td>
						Special Prospects					</td>
					<td>
						Call Attempted					</td>
					<td>
						08/27/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						BUENA PARK MOTOR INN (JAMES CHU)					</td>
					<td>
						7921 Orangethorpe Ave, Buena Park CA 90621-3436					</td>
					<td>
						Wayne F. Burt Jr.					</td>
					<td>
						Special Prospects					</td>
					<td>
						Call Attempted					</td>
					<td>
						08/27/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Burlingame Industries Inc (BOB BURLINGAME)					</td>
					<td>
						3546 N Riverside Ave, Rialto CA 92377-3802					</td>
					<td>
						Wayne F. Burt Jr.					</td>
					<td>
						Special Prospects					</td>
					<td>
						Call Attempted					</td>
					<td>
						08/27/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						CALICO MOTEL (DAVID BAKTA)					</td>
					<td>
						500 S Beach Blvd, Anaheim CA 92804-1811					</td>
					<td>
						Wayne F. Burt Jr.					</td>
					<td>
						Special Prospects					</td>
					<td>
						Call Attempted					</td>
					<td>
						08/27/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Temple Beth Shalom (SUSIE AMSTER)					</td>
					<td>
						2625 N Tustin Ave, Santa Ana CA 92705-1099					</td>
					<td>
						Brant Bishop					</td>
					<td>
						January Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/27/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						BUDGET INN (GARY BAJKTA)					</td>
					<td>
						420 S Beach Blvd, Anaheim CA 92804-1809					</td>
					<td>
						Wayne F. Burt Jr.					</td>
					<td>
						Special Prospects					</td>
					<td>
						Left Message					</td>
					<td>
						08/27/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						TSCM (FRANK PAPPANO)					</td>
					<td>
						18281 Gothard St Ste 109 # 109, Huntington Beach CA 92648-1205					</td>
					<td>
						Brant Bishop					</td>
					<td>
						January Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/27/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						TSI PROPERTY SVC (TOM SALAZAR)					</td>
					<td>
						789 W 20th St, Costa Mesa CA 92627-3487					</td>
					<td>
						Brant Bishop					</td>
					<td>
						January Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/27/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Tarbell Realtors (JIM WOLLENBURG)					</td>
					<td>
						16111 Beach Blvd, Huntington Beach CA 92647-3804					</td>
					<td>
						Brant Bishop					</td>
					<td>
						January Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/27/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						TARBELL REALTORS (HAMID KHARRAT)					</td>
					<td>
						321 S State College Blvd, Anaheim CA 92806-4118					</td>
					<td>
						Brant Bishop					</td>
					<td>
						January Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/27/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						TAYLOR & ASSOC ARCHITECTS (LINDA TAYLOR)					</td>
					<td>
						2220 University Dr, Newport Beach CA 92660-3319					</td>
					<td>
						Brant Bishop					</td>
					<td>
						January Leads					</td>
					<td>
						Left Message					</td>
					<td>
						08/27/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						CRITCHFIELD MECHANICAL INC (MICHAEL PEARLMAN)					</td>
					<td>
						130 McCormick Ave Ste 106, Costa Mesa CA 92626-3316					</td>
					<td>
						Nicole Knight					</td>
					<td>
						JulyHotLeads					</td>
					<td>
						Call Completed					</td>
					<td>
						08/27/2009					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						BUDGET INN (SHIRLEY ZHAI)					</td>
					<td>
						10038 Valley Blvd, El Monte CA 91731-1702					</td>
					<td>
						Wayne F. Burt Jr.					</td>
					<td>
						Special Prospects					</td>
					<td>
						Call Completed					</td>
					<td>
						08/27/2009					</td>
				</tr>
				
			</table>
		</td>
	</tr>
</table>
</body>
</html>
