<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	<title>
		<?= $portal->CurrentCompany->CompanyName ?> Campaign Detail - June Leads	</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />		
	<link type="text/css" media="all" rel="stylesheet" href="style.css" />
	<script src="../js/jscalendar/js/jscal2.js"></script>
	<script src="../js/jscalendar/js/lang/en.js"></script>
	<link rel="stylesheet" type="text/css" href="../js/jscalendar/css/jscal2.css" />
	<link rel="stylesheet" type="text/css" href="../js/jscalendar/css/win2k/win2k.css" />	
	<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />
<script type="text/javascript">//<![CDATA[

      var cal = Calendar.setup({
          onSelect: function(cal) { cal.hide() },
          bottomBar: true,
          dateFormat : "%m/%d/%Y"
      });

//]]></script>
	</head>
<body bgcolor="#FFFFFF" >
<table width="100%" cellpadding="2" cellspacing="1">
	<tr>
		<td align="left" class="headerTextLarge">
			<form method="get" action="">
				Campaign: <select name="slt">
					<option value="" >August Leads</option>
					<option value="" >July Leads</option>
					<option value="" selected>June Leads</option>
					<option value="" >April Leads</option>
				</select>
				<input type="text" id="start" name="start" value="" size="10">
				<script type="text/javascript">
					cal.manageFields("start", "start", "%m/%d/%Y");
				</script> to 
				<input type="text" id="end" name="end" value="" size="10">
				<script type="text/javascript">
					cal.manageFields("end", "end", "%m/%d/%Y");
				</script>
				<input type="submit" value="Go" name="Submit">
			</form>
		</td>
	</tr>
	<tr>
		<td align="left" class="headerTextLarge">
			<table bgcolor="#666666">
				<tr class="tableHeader" >
					<td width="180">
						Division
					</td>
					<td width="75" align="center">
						Att Calls					</td>
					<td width="75" align="center">
						LM					</td>
					<td width="75" align="center">
						Comp Calls					</td>
					<td width="75" align="center">
						Tot Calls					</td>
					<td width="75" align="center">
						Apts					</td>
					<td width="75" align="center">
						Mail - Call Att					</td>
					<td width="75" align="center">
						Mail - LM					</td>
					<td width="75" align="center">
						Mail - CC					</td>
					<td width="75" align="center">
						Mail - Appt					</td>
					<td width="75" align="center">
						Total Mail					</td>
					<td width="75" align="center">
						Sold					</td>
					<td width="75" align="center">
						Team Total					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						North					</td>
					<td align="right">
						0					</td>
					<td align="right">
						2					</td>
					<td align="right">
						0					</td>
					<td align="right">
						3					</td>
					<td align="right">
						1					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						1					</td>
					<td align="right">
						1					</td>
					<td align="right">
						0					</td>
					<td align="right">
						4					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						East					</td>
					<td align="right">
						95					</td>
					<td align="right">
						50					</td>
					<td align="right">
						111					</td>
					<td align="right">
						277					</td>
					<td align="right">
						21					</td>
					<td align="right">
						78					</td>
					<td align="right">
						33					</td>
					<td align="right">
						87					</td>
					<td align="right">
						2					</td>
					<td align="right">
						200					</td>
					<td align="right">
						0					</td>
					<td align="right">
						477					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						South					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						West					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
				</tr>
				<tr class="tableDetailOdd">
					<td>
						Total
					</td>
					<td align="right">
						95					</td>
					<td align="right">
						52					</td>
					<td align="right">
						111					</td>
					<td align="right">
						280					</td>
					<td align="right">
						22					</td>
					<td align="right">
						78					</td>
					<td align="right">
						33					</td>
					<td align="right">
						87					</td>
					<td align="right">
						3					</td>
					<td align="right">
						201					</td>
					<td align="right">
						0					</td>
					<td align="right">
						481					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td align="left" class="headerTextLarge">
			<table bgcolor="#666666">
				<tr class="tableHeader" >
					<td width="180">
						Teams
					</td>
					<td width="75" align="center">
						Att Calls					</td>
					<td width="75" align="center">
						LM					</td>
					<td width="75" align="center">
						Comp Calls					</td>
					<td width="75" align="center">
						Tot Calls					</td>
					<td width="75" align="center">
						Apts					</td>
					<td width="75" align="center">
						Mail - Call Att					</td>
					<td width="75" align="center">
						Mail - LM					</td>
					<td width="75" align="center">
						Mail - CC					</td>
					<td width="75" align="center">
						Mail - Appt					</td>
					<td width="75" align="center">
						Total Mail					</td>
					<td width="75" align="center">
						Sold					</td>
					<td width="75" align="center">
						Team Total					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Robert Tucci - North					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Paul Hendrickson - North					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Joe Ugalde - North					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Peter Yen - North					</td>
					<td align="right">
						0					</td>
					<td align="right">
						2					</td>
					<td align="right">
						0					</td>
					<td align="right">
						3					</td>
					<td align="right">
						1					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						1					</td>
					<td align="right">
						1					</td>
					<td align="right">
						0					</td>
					<td align="right">
						4					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Paul Turner - East					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Enrico Diaz - East					</td>
					<td align="right">
						95					</td>
					<td align="right">
						50					</td>
					<td align="right">
						111					</td>
					<td align="right">
						277					</td>
					<td align="right">
						21					</td>
					<td align="right">
						78					</td>
					<td align="right">
						33					</td>
					<td align="right">
						87					</td>
					<td align="right">
						2					</td>
					<td align="right">
						200					</td>
					<td align="right">
						0					</td>
					<td align="right">
						477					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Howard Eigenberg - East					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						South Sales Director - South					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						West Director - West					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
				</tr>
				<tr class="tableDetailOdd">
					<td>
						Total
					</td>
					<td align="right">
						95					</td>
					<td align="right">
						52					</td>
					<td align="right">
						111					</td>
					<td align="right">
						280					</td>
					<td align="right">
						22					</td>
					<td align="right">
						78					</td>
					<td align="right">
						33					</td>
					<td align="right">
						87					</td>
					<td align="right">
						3					</td>
					<td align="right">
						201					</td>
					<td align="right">
						0					</td>
					<td align="right">
						481					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td align="left" class="headerTextLarge">
			<table bgcolor="#666666">
				<tr class="tableHeader" >
					<td width="180">
						Sale Exec's by Team
					</td>
					<td width="75" align="center">
						&nbsp;
					</td>
					<td width="75" align="center">
						&nbsp;
					</td>
					<td width="75" align="center">
						&nbsp;
					</td>
					<td width="75" align="center">
						&nbsp;
					</td>
					<td width="75" align="center">
						&nbsp;
					</td>
					<td width="75" align="center">
						&nbsp;
					</td>
					<td width="75" align="center">
						&nbsp;
					</td>
					<td width="75" align="center">
						&nbsp;
					</td>
					<td width="75" align="center">
						&nbsp;
					</td>
					<td width="75" align="center">
						&nbsp;
					</td>
					<td width="75" align="center">
						&nbsp;
					</td>
					<td width="75" align="center">
						&nbsp;
					</td>
				</tr>
				<tr class="tableDetailOdd">
					<td>
						Robert Tucci					</td>
					<td align="center">
						Att Calls					</td>
					<td align="center">
						LM					</td>
					<td align="center">
						Comp Calls					</td>
					<td align="center">
						Tot Calls					</td>
					<td align="center">
						Apts					</td>
					<td align="center">
						Mail - Call Att					</td>
					<td align="center">
						Mail - LM					</td>
					<td align="center">
						Mail - CC					</td>
					<td align="center">
						Mail - Appt					</td>
					<td align="center">
						Total Mail					</td>
					<td align="center">
						Sold					</td>
					<td align="center">
						Team Total					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Charles McLaurin II					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Cynthia Brewer					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Randy Geltman					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Total
					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
				</tr>
				<tr class="tableDetailOdd">
					<td>
						Paul Hendrickson					</td>
					<td align="center">
						Att Calls					</td>
					<td align="center">
						LM					</td>
					<td align="center">
						Comp Calls					</td>
					<td align="center">
						Tot Calls					</td>
					<td align="center">
						Apts					</td>
					<td align="center">
						Mail - Call Att					</td>
					<td align="center">
						Mail - LM					</td>
					<td align="center">
						Mail - CC					</td>
					<td align="center">
						Mail - Appt					</td>
					<td align="center">
						Total Mail					</td>
					<td align="center">
						Sold					</td>
					<td align="center">
						Team Total					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Total
					</td>
					<td align="right">
											</td>
					<td align="right">
											</td>
					<td align="right">
											</td>
					<td align="right">
											</td>
					<td align="right">
											</td>
					<td align="right">
											</td>
					<td align="right">
											</td>
					<td align="right">
											</td>
					<td align="right">
											</td>
					<td align="right">
											</td>
					<td align="right">
											</td>
					<td align="right">
											</td>
				</tr>
				<tr class="tableDetail">
					<td>
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
				</tr>
				<tr class="tableDetailOdd">
					<td>
						Joe Ugalde					</td>
					<td align="center">
						Att Calls					</td>
					<td align="center">
						LM					</td>
					<td align="center">
						Comp Calls					</td>
					<td align="center">
						Tot Calls					</td>
					<td align="center">
						Apts					</td>
					<td align="center">
						Mail - Call Att					</td>
					<td align="center">
						Mail - LM					</td>
					<td align="center">
						Mail - CC					</td>
					<td align="center">
						Mail - Appt					</td>
					<td align="center">
						Total Mail					</td>
					<td align="center">
						Sold					</td>
					<td align="center">
						Team Total					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Jim Brown					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Total
					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
				</tr>
				<tr class="tableDetailOdd">
					<td>
						Peter Yen					</td>
					<td align="center">
						Att Calls					</td>
					<td align="center">
						LM					</td>
					<td align="center">
						Comp Calls					</td>
					<td align="center">
						Tot Calls					</td>
					<td align="center">
						Apts					</td>
					<td align="center">
						Mail - Call Att					</td>
					<td align="center">
						Mail - LM					</td>
					<td align="center">
						Mail - CC					</td>
					<td align="center">
						Mail - Appt					</td>
					<td align="center">
						Total Mail					</td>
					<td align="center">
						Sold					</td>
					<td align="center">
						Team Total					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Anita Warren					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Kelly Owen					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Maria Frew					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						HP Le					</td>
					<td align="right">
						0					</td>
					<td align="right">
						2					</td>
					<td align="right">
						0					</td>
					<td align="right">
						3					</td>
					<td align="right">
						1					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						1					</td>
					<td align="right">
						1					</td>
					<td align="right">
						0					</td>
					<td align="right">
						4					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Jimmy Kim					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						John Christiansen					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Valerie Jordan					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Dawn Davis					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Total
					</td>
					<td align="right">
						0					</td>
					<td align="right">
						2					</td>
					<td align="right">
						0					</td>
					<td align="right">
						3					</td>
					<td align="right">
						1					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						1					</td>
					<td align="right">
						1					</td>
					<td align="right">
						0					</td>
					<td align="right">
						4					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
				</tr>
				<tr class="tableDetailOdd">
					<td>
						Paul Turner					</td>
					<td align="center">
						Att Calls					</td>
					<td align="center">
						LM					</td>
					<td align="center">
						Comp Calls					</td>
					<td align="center">
						Tot Calls					</td>
					<td align="center">
						Apts					</td>
					<td align="center">
						Mail - Call Att					</td>
					<td align="center">
						Mail - LM					</td>
					<td align="center">
						Mail - CC					</td>
					<td align="center">
						Mail - Appt					</td>
					<td align="center">
						Total Mail					</td>
					<td align="center">
						Sold					</td>
					<td align="center">
						Team Total					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Lance Alarcon					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Israel Argueta					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Le Van Moment					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Maria Rice					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Sino Saraei					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Total
					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
				</tr>
				<tr class="tableDetailOdd">
					<td>
						Enrico Diaz					</td>
					<td align="center">
						Att Calls					</td>
					<td align="center">
						LM					</td>
					<td align="center">
						Comp Calls					</td>
					<td align="center">
						Tot Calls					</td>
					<td align="center">
						Apts					</td>
					<td align="center">
						Mail - Call Att					</td>
					<td align="center">
						Mail - LM					</td>
					<td align="center">
						Mail - CC					</td>
					<td align="center">
						Mail - Appt					</td>
					<td align="center">
						Total Mail					</td>
					<td align="center">
						Sold					</td>
					<td align="center">
						Team Total					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Robin Kang					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Nicole Knight					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Brant Bishop					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Richard Fissel					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Peter Perdikakis					</td>
					<td align="right">
						95					</td>
					<td align="right">
						50					</td>
					<td align="right">
						111					</td>
					<td align="right">
						277					</td>
					<td align="right">
						21					</td>
					<td align="right">
						78					</td>
					<td align="right">
						33					</td>
					<td align="right">
						87					</td>
					<td align="right">
						2					</td>
					<td align="right">
						200					</td>
					<td align="right">
						0					</td>
					<td align="right">
						477					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Janet Atkins					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Troy Stivers					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Total
					</td>
					<td align="right">
						95					</td>
					<td align="right">
						50					</td>
					<td align="right">
						111					</td>
					<td align="right">
						277					</td>
					<td align="right">
						21					</td>
					<td align="right">
						78					</td>
					<td align="right">
						33					</td>
					<td align="right">
						87					</td>
					<td align="right">
						2					</td>
					<td align="right">
						200					</td>
					<td align="right">
						0					</td>
					<td align="right">
						477					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
				</tr>
				<tr class="tableDetailOdd">
					<td>
						Howard Eigenberg					</td>
					<td align="center">
						Att Calls					</td>
					<td align="center">
						LM					</td>
					<td align="center">
						Comp Calls					</td>
					<td align="center">
						Tot Calls					</td>
					<td align="center">
						Apts					</td>
					<td align="center">
						Mail - Call Att					</td>
					<td align="center">
						Mail - LM					</td>
					<td align="center">
						Mail - CC					</td>
					<td align="center">
						Mail - Appt					</td>
					<td align="center">
						Total Mail					</td>
					<td align="center">
						Sold					</td>
					<td align="center">
						Team Total					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Joe Burgos					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Michele D'Amico					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Robert Hadaway					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Brian Staller					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Lee Garrison					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Suzanne Spinosa					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Burt Hekmatnia					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						John Baccus					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Total
					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
				</tr>
				<tr class="tableDetailOdd">
					<td>
						Desert Cities Sales Director					</td>
					<td align="center">
						Att Calls					</td>
					<td align="center">
						LM					</td>
					<td align="center">
						Comp Calls					</td>
					<td align="center">
						Tot Calls					</td>
					<td align="center">
						Apts					</td>
					<td align="center">
						Mail - Call Att					</td>
					<td align="center">
						Mail - LM					</td>
					<td align="center">
						Mail - CC					</td>
					<td align="center">
						Mail - Appt					</td>
					<td align="center">
						Total Mail					</td>
					<td align="center">
						Sold					</td>
					<td align="center">
						Team Total					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Anthony Rivera					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Bill Williams					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Dale Harrentstein					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Brian Saiz					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Justin Ulsh					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Eric Scott					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Total
					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
				</tr>
				<tr class="tableDetailOdd">
					<td>
						San Diego Sales Director					</td>
					<td align="center">
						Att Calls					</td>
					<td align="center">
						LM					</td>
					<td align="center">
						Comp Calls					</td>
					<td align="center">
						Tot Calls					</td>
					<td align="center">
						Apts					</td>
					<td align="center">
						Mail - Call Att					</td>
					<td align="center">
						Mail - LM					</td>
					<td align="center">
						Mail - CC					</td>
					<td align="center">
						Mail - Appt					</td>
					<td align="center">
						Total Mail					</td>
					<td align="center">
						Sold					</td>
					<td align="center">
						Team Total					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Rafi Ahmadi					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Cheryl Allen					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Albert Acevedo					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Gregory Wisniewski					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Carrie Hughes					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Steven Artura					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Joe Ruelas					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						Total
					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
					<td align="right">
						0					</td>
				</tr>
				<tr class="tableDetail">
					<td>
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
					<td align="right">
						&nbsp;
					</td>
				</tr>
				<tr class="tableDetailOdd">
					<td>
						Total
					</td>
					<td align="right">
						95					</td>
					<td align="right">
						52					</td>
					<td align="right">
						111					</td>
					<td align="right">
						280					</td>
					<td align="right">
						22					</td>
					<td align="right">
						78					</td>
					<td align="right">
						33					</td>
					<td align="right">
						87					</td>
					<td align="right">
						3					</td>
					<td align="right">
						201					</td>
					<td align="right">
						0					</td>
					<td align="right">
						481					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</body>
</html>