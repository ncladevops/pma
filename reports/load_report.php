<?php
	$statusColors = array("2. Contacted" => "008080", "Attempting Contact" => "FFA500", "Application Started" => "416990",
							"Application Taken" => "FFFF00", "Submitted to Processing" => "48D1CC", "Loan Approval" => "FA8072",
							"Cleared to close" => "1E90FF", "Funded Loan" => "228B22", "Other" => "D2691E" );
	
	if($_GET['iframe'] != 'true')
		$stripContent = array("<!doctype html>", '<html>', '<head>', '<title></title>', '<body class="yui-skin-sam">','</body>','</html>');
	else
		$stripContent = array();

	$url = $_GET['url'];
	
	$host = substr($url, 0, strrpos($url, '/') + 1);
	$ch = curl_init($url);
	curl_setopt( $ch, CURLOPT_URL, $url);
	curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Connection: close'));
				
	// send request
	$responseText = curl_exec ($ch);
	
	$rnd = rand();
	
	$responseText = str_replace('src="', 'src="' . $host, $responseText);
	$responseText = str_replace('href="', 'href="' . $host, $responseText);
		
	if($_GET['iframe'] != 'true') {
		$responseText = str_replace($stripContent, "", $responseText);
		$responseText = str_replace('ewrExportCharts[ewrExportCharts.length] ', '//ewrExportCharts[ewrExportCharts.length] ', $responseText);
		$responseText = str_replace('function ewr_GetScript(url) ', '//function ewr_GetScript(url) ' . $host, $responseText);
		$responseText = str_replace('chartwidth', 'chartwidth' . $rnd, $responseText);
		$responseText = str_replace('chartheight', 'chartheight' . $rnd, $responseText);
		$responseText = str_replace('chartalign', 'chartalign' . $rnd, $responseText);
		$responseText = str_replace('chartxml', 'chartxml' . $rnd, $responseText);
		$responseText = str_replace('chartid', 'chartid' . $rnd, $responseText);
		$responseText = str_replace('chartswf', 'chartswf' . $rnd, $responseText);
		$responseText = str_replace('jQuery().jquery != "1.7.1"', 'false', $responseText);
	}
	
	foreach($statusColors as $stat=>$col) {
		$pattern = '/<set label=\\\\"' . $stat . '\\\\"(.*?)color=\\\\"(.*?)\\\\"(.*?)/';
		$replace = '<set label=\\"' . $stat . '\\"$1color=\\"' . $col . '\\"$3';
		
		 $responseText = preg_replace($pattern, $replace, $responseText);
	}
	
	echo $responseText;