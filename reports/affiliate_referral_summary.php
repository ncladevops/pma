<?php
	require("../../printable/include/mysql.inc.php");
	require("../../printable/include/optimize.printable.inc.php");
	require("../globals.php");
	
	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	
	$portal = new OptimizePortal($COMPANY_ID, $db);
	
	$currentUser = $portal->GetUser($_SESSION['currentuserid']);
	$isSupervisor = $portal->CheckPriv($currentUser->UserID, 'supervisor');
			
	if(!$currentUser)
	{
		header( "Location: " . $portal->CurrentCompany->Website . "home.php?message=" . urlencode( "Accessed Denied." ) );
		die();
	}
	
	$userLookup = array($currentUser->UserID => $currentUser->LastName . ", " . $currentUser->FirstName);
	$children = $portal->GetChildren($currentUser->UserID);
	foreach($children as $c) {
		$userLookup[$c->UserID] = $c->LastName . ", " . $c->FirstName;
	}
	
	
		
	if($isSupervisor && isset($userLookup[$_GET['userid']]))
	{
		$userWhere = "submissionlisting.UserID = '" . intval($_GET['userid']) . "'";
	}
	else
	{
		$userWhere = "submissionlisting.UserID = '" . intval($currentUser->UserID) . "'";
	}
	if(strtotime($_GET['start_range']))
	{
		$startWhere = "submissionlisting.AffiliateDate >= '" . date("Y-m-d", strtotime($_GET['start_range'])) ." 00:00:00'";
	}
	else
	{
		$startWhere = "1";
	}
	
	if(strtotime($_GET['end_range']))
	{
		$endWhere = "submissionlisting.AffiliateDate <= '" . date("Y-m-d", strtotime($_GET['end_range'])) ." 23:59:59'";
	}
	else
	{
		$endWhere = "1";
	}
	
	// Select for Report
	$sqlstring = "SELECT
						submissionlisting.`AffiliateID`, CONCAT(affiliatelisting.`Company`, ' - ', affiliatelisting.`FirstName`, ' ', affiliatelisting.`LastName`) AS AffiliateName,
						submissionlisting.`IncomingAffiliate`, COUNT(submissionlisting.`SubmissionListingID`) AS ReferralCount
					FROM
						submissionlisting
					JOIN
						companyuser
						ON submissionlisting.`UserID` = companyuser.`UserID`
					JOIN
						submissionlisting AS affiliatelisting
						ON submissionlisting.`AffiliateID` = affiliatelisting.`SubmissionListingID`
					WHERE
						submissionlisting.AffiliateID IS NOT NULL
						AND companyuser.CompanyID = '" . intval($portal->CurrentCompany->CompanyID) . "'
						AND $userWhere
						AND $startWhere
						AND $endWhere
					GROUP BY
						submissionlisting.`AffiliateID`, submissionlisting.`IncomingAffiliate`";
	$report_res = $portal->mysqlDB->Query($sqlstring);
	
	$reportArray = array();
	for($i = 0; $i < mysql_num_rows($report_res); $i++)
	{
		$report_row = mysql_fetch_array($report_res);
		$type = $report_row['IncomingAffiliate'] == 0 ? 'given' : 'received';
		$reportArray[$report_row['AffiliateName']][$type] = $report_row['ReferralCount'];
	}
		
	if($_GET['submit'] == 'Export To Excel')
	{
		//do download
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header('Content-type: application/force-download');
		header("Content-Transfer-Encoding: binary");
		header("Content-Disposition: attachment; filename=\"affiliate_referral_summary_report.xls\";" );
		header("Content-Transfer-Encoding: binary");
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
	<title>
		Leads on Demand :: Affiliate Referral Summary Report
	</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<?php
	if($_GET['submit'] != 'Export To Excel')
	{
?>		
	<link type="text/css" media="all" rel="stylesheet" href="report_style.css" />
	<script src="../js/jscalendar/js/jscal2.js"></script>
	<script src="../js/jscalendar/js/lang/en.js"></script>
	<link rel="stylesheet" type="text/css" href="../js/jscalendar/css/jscal2.css" />
	<link rel="stylesheet" type="text/css" href="../js/jscalendar/css/steel/steel.css" />	
<script type="text/javascript">//<![CDATA[

      var cal = Calendar.setup({
          onSelect: function(cal) { cal.hide() },
          bottomBar: true,
          dateFormat : "%m/%d/%Y"
      });

//]]></script>	
<?php
	}
	else
	{
?>		
	<style type="text/css">
		<?php include("report_style.css"); ?>
	</style>
<?php
	}
?>
</head>
<body onload="<?= ($_GET['submit'] != 'Export To Excel' ? "LoadCampaigns('" . $_GET['slsid'] . "', '" . $_GET['sltid'] . "')" : "") ?>">
<?php
	if($_GET['submit'] != 'Export To Excel')
	{
?>
<form method="get" action="<?= $_SERVER['PHP_SELF'] ?>">
<div id="filter_div">
	<div id="left_filter_div">
<?php
		if($isSupervisor) {
?>
		<select name="userid">
<?php
			foreach($userLookup as $id=>$u)
			{
?>
			<option value="<?= $id ?>" <?= $_GET['userid'] == $id ? "SELECTED" : "" ?>><?= $u ?></option>
<?php
			}
?>
		</select>
<?php
		}
?>
	</div>
	<div id="right_filter_div">
		From: <input name="start_range" id="start_range" type="text" class="date_input" value="<?= strtotime($_GET['start_range']) ? date("m/d/Y", strtotime($_GET['start_range'])) : "" ?>" />
		<script type="text/javascript">
			cal.manageFields("start_range", "start_range", "%m/%d/%Y");
		</script>
		To: <input name="end_range" id="end_range" type="text" class="date_input" value="<?= strtotime($_GET['end_range']) ? date("m/d/Y", strtotime($_GET['end_range'])) : "" ?>"/>
		<script type="text/javascript">
			cal.manageFields("end_range", "end_range", "%m/%d/%Y");
		</script>
		<input name="submit" value="Go" type="submit" />
		<input name="submit" value="Export To Excel" type="submit" />
	</div>
</div>
</form>
<?php
	}
?>
<table class="report_table">
	<tr class="report_table rowheader">
		<td class="report_table">Name</td>
		<td class="report_table">Given</td>
		<td class="report_table">Received</td>
		<td class="report_table">Delta</td>
	</tr>
<?php
	$i = 0;
	foreach($reportArray as $affiliateName=>$ra)
	{
		$i++;
?>
	<tr class="report_table<?= $i%2==0 ? ' roweven' : ' rowodd' ?>">
		<td class="report_table" align="left"><?= $affiliateName ?></td>
		<td class="report_table" align="right"><?= intval($ra['given']) ?></td>
		<td class="report_table" align="right"><?= intval($ra['received']); ?></td>
		<td class="report_table" align="right"><?= intval($ra['received'] - $ra['given']) ?></td>
	</tr>
<?php
	}
?>
</table>
</body>
</html>