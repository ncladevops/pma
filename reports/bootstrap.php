<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- jQuery UI CSS -->
<link rel="stylesheet" href="../../printable/include/js/jquery-ui/smoothness/jquery-ui.min.css"/>

<!-- Bootstrap core CSS -->
<link href="../dist/css/bootstrap.css" rel="stylesheet">

<!-- Bootstrap theme -->
<link href="../dist/css/bootstrap-theme.min.css" rel="stylesheet">

<!-- Bootstrap theme -->
<link href="../css/jquery.cleditor.css" rel="stylesheet">

<!-- Chosen CSS -->
<link type="text/css" href="../js/chosen/chosen.css" rel="stylesheet" media="all"/>

<!-- Custom styles for this template -->
<link href="../css/optifinow-custom.css" rel="stylesheet">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->