<?php
	require("../../printable/include/mysql.inc.php");
	require("../../printable/include/optimize.printable.inc.php");
	require("../globals.php");
		
	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	
	$portal = new OptimizePortal($COMPANY_ID, $db);
	
	$currentUser = $portal->GetUser($_SESSION['currentuserid']);
			
	if(!$portal->CheckPriv($currentUser->UserID, 'report'))
	{
		header( "Location: " . $portal->CurrentCompany->Website . "home.php?message=" . urlencode( "Accessed Denied." ) );
		die();
	}
		
	if(strtotime($_GET['start_range']))
	{
		$startWhere = "cl.ChangeTime >= '" . date("Y-m-d", strtotime($_GET['start_range'])) ." 00:00:00'";
	}
	else
	{
		$startWhere = "cl.ChangeTime >= '" . date("Y-m-d") ." 00:00:00'";
		$_GET['start_range'] = date("Y-m-d");
	}
	
	if(strtotime($_GET['end_range']))
	{
		$endWhere = "cl.ChangeTime <= '" . date("Y-m-d", strtotime($_GET['end_range'])) ." 23:59:59'";
	}
	else
	{
		$endWhere = "cl.ChangeTime <= '" . date("Y-m-d") ." 23:59:59'";
		$_GET['end_range'] = date("Y-m-d");
	}
	
	if(strtotime($_GET['userid']))
	{
		$userWhere = "u.UserID = '" . intval($_GET['userid']) ."'";
	}
	else
	{
		$userWhere = "1";
	}
	
	// Select for Report
	$sqlstring = "SELECT 
						cl.ChangeTime as TranferTime, sl.SubmissionListingID as LeadID,
						CONCAT(u.FirstName, ' ', u.LastName) AS TransferFromName,
						CONCAT(newu.FirstName, ' ', newu.LastName) AS TransferToName, 
						sl.DateAdded, dl.ExternalID, sl.FirstName,
						sl.LastName, sl.Address1, sl.City,
						sl.State, sl.Zip, sl.Barcode,
						sl.Phone, sl.Phone2, dl.MarketValue,
						cst.ContactStatus, dl.LoanAmount
					FROM
						changelog AS cl
					JOIN
						submissionlisting AS sl
						ON cl.Key = sl.SubmissionListingID
					JOIN
						detaillisting_leadsondemand AS dl
						ON dl.SubmissionListingID = sl.SubmissionListingID
					JOIN
						`user` AS u
						ON u.UserID = cl.OldValue
					JOIN
						`user` AS newu
						ON newu.UserID = cl.NewValue
					JOIN
						contactstatustype AS cst
						ON cst.ContactStatusTypeID = sl.ContactStatusTypeID
					WHERE
						cl.Field = 'UserID'
						AND u.GroupID = '13'
						AND $startWhere
						AND $endWhere
						AND $userWhere
					ORDER BY
						cl.ChangeTime;";
	$report_res = $portal->mysqlDB->Query($sqlstring);
	
	$reportArray = array();
	for($i = 0; $i < mysql_num_rows($report_res); $i++)
	{
		$report_row = mysql_fetch_array($report_res, MYSQL_ASSOC);
		$reportArray[$i] = $report_row;
	}
	
	$colArray = array("TranferTime", "TransferFromName", "TransferToName", 
						"ContactStatus","LeadID",
						"DateAdded", "ExternalID", "FirstName",
						"LastName", "Address1", "City",
						"State", "Zip", "Barcode",
						"Phone", "Phone2", "MarketValue", "LoanAmount");
	
	
	$users = $portal->GetCompanyUsers("u.GroupID = '13'");
	
	if($_GET['submit'] == 'Export To Excel')
	{
		//do download
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header('Content-type: application/force-download');
		header("Content-Transfer-Encoding: binary");
		header("Content-Disposition: attachment; filename=\"user_lead_report.xls\";" );
		header("Content-Transfer-Encoding: binary");
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
	<title>
		<?= $portal->CurrentCompany->CompanyName ?> Leads on Demand :: Lead Transfer Report
	</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<?php
	if($_GET['submit'] != 'Export To Excel')
	{
?>		
	<link type="text/css" media="all" rel="stylesheet" href="report_style.css" />
	<script src="../js/jscalendar/js/jscal2.js"></script>
	<script src="../js/jscalendar/js/lang/en.js"></script>
	<link rel="stylesheet" type="text/css" href="../js/jscalendar/css/jscal2.css" />
	<link rel="stylesheet" type="text/css" href="../js/jscalendar/css/steel/steel.css" />	
	<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />
<script type="text/javascript">//<![CDATA[

      var cal = Calendar.setup({
          onSelect: function(cal) { cal.hide() },
          bottomBar: true,
          dateFormat : "%m/%d/%Y"
      });

//]]></script>
<?php
	}
	else
	{
?>		
	<style type="text/css">
		<?php include("report_style.css"); ?>
	</style>
<?php
	}
?>
</head>
<body>
<?php
	if($_GET['submit'] != 'Export To Excel')
	{
?>
<form method="get" action="<?= $_SERVER['PHP_SELF'] ?>">
<div id="filter_div">
	<div id="left_filter_div">
		<select name="userid">
			<option value="">-- All Users --</option>
<?php
		foreach($users as $u)
		{
?>
			<option value="<?= $u->UserID ?>" <?= $_GET['userid'] == $u->UserID ? "SELECTED" : "" ?>><?= $u->LastName . ", ". $u->FirstName ?></option>
<?php
		}
?>
		</select>
	</div>
	<div id="right_filter_div">
		From: <input name="start_range" id="start_range" type="text" class="date_input" value="<?= strtotime($_GET['start_range']) ? date("m/d/Y", strtotime($_GET['start_range'])) : "" ?>" />
		<script type="text/javascript">
			cal.manageFields("start_range", "start_range", "%m/%d/%Y");
		</script>
		To: <input name="end_range" id="end_range" type="text" class="date_input" value="<?= strtotime($_GET['end_range']) ? date("m/d/Y", strtotime($_GET['end_range'])) : "" ?>"/>
		<script type="text/javascript">
			cal.manageFields("end_range", "end_range", "%m/%d/%Y");
		</script>
		<input name="submit" value="Go" type="submit" />
		<input name="submit" value="Export To Excel" type="submit" />
	</div>
</div>
</form>
<?php
	}
?>
<table class="report_table">
	<tr class="report_table rowheader">
<?php
	foreach($colArray as $col)
	{
?>
		<td class="report_table"><?= $col ?></td>
<?php
	}
?>
	</tr>
<?php
	for($i = 0; $i < sizeof($reportArray); $i++)
	{
?>
	<tr class="report_table<?= $i%2==0 ? ' roweven' : ' rowodd' ?>">
<?php
		foreach($colArray as $col)
		{
?>
		<td class="report_table" align="right"><?= $reportArray[$i][$col] ?></td>
<?php
		}
?>
	</tr>
<?php	
	}
?>
</table>
</body>
</html>