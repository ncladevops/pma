<?php
	require("../../printable/include/mysql.inc.php");
	require("../../printable/include/optimize.printable.inc.php");
	require("../globals.php");
	
	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	
	$portal = new OptimizePortal($COMPANY_ID, $db);
	
	$currentUser = $portal->GetUser($_SESSION['currentuserid']);
			
	if(!$portal->CheckPriv($currentUser->UserID, 'report'))
	{
		header( "Location: " . $portal->CurrentCompany->Website . "home.php?message=" . urlencode( "Accessed Denied." ) );
		die();
	}
	
	if($_GET['sltid'] != '')
	{
		$sltWhere = "sl.ListingType LIKE '" . intval($_GET['sltid']) . "'";
	}
	else
	{
		$sltWhere = "1";
	}
	
	if(!strtotime($_GET['start_range']))
	{
		$_GET['start_range'] = date("Y-m-1");
	}
	$startWhere = "submissionlisting.DateAdded >= '" . date("Y-m-d", strtotime($_GET['start_range'])) ." 00:00:00'";
	
	
	if(!strtotime($_GET['end_range']))
	{
		$_GET['end_range'] = date("Y-m-d");
	}
	$endWhere = "submissionlisting.DateAdded <= '" . date("Y-m-d", strtotime($_GET['end_range'])) ." 23:59:59'";
	
	// Select for Report
	$sqlstring = "SELECT
						submissionlistingtype.ListingDescription AS 'Campaign Report Name',
						submissionlistingtype.LeadCost, 
						contactstatustype.ContactStatus, submissionlisting.DateAdded,
						submissionlisting.FirstName, submissionlisting.LastName, submissionlisting.Address1,
						submissionlisting.City, submissionlisting.State,
						submissionlisting.Zip, submissionlisting.Barcode, submissionlisting.Phone,
						submissionlisting.Phone2, detaillisting_leadsondemand.CellPhone, detaillisting_leadsondemand.MarketValue,
						CONCAT(user.FirstName, ' ', user.LastName) AS UserFullName, 
						user.Username AS LO_Email, 'OptifiNow' AS LO_Company,
						user.Title AS LO_Title,
						user.Address AS LO_Address, 
						user.City AS LO_City, user.State AS LO_State, user.Zipcode AS LO_Zip, 
						'www.optifinow.com' AS LO_Website,
						user.ContactPhone, user.ContactCell, ExternalID
					FROM
						submissionlisting
					JOIN
						`user`
						ON user.UserID = submissionlisting.UserID 
					JOIN
						companyuser
						ON user.UserID = companyuser.UserID
					LEFT JOIN
						detaillisting_leadsondemand
						ON submissionlisting.SubmissionListingID = detaillisting_leadsondemand.SubmissionListingID
					LEFT JOIN
						contactevent 
						ON contactevent.ContactEventID = submissionlisting.LastContactEventID
					LEFT JOIN
						submissionlistingtype
						ON submissionlisting.ListingType = submissionlistingtype.SubmissionLIstingTypeID
					LEFT JOIN
						contactstatustype
						ON submissionlisting.ContactStatusTypeID = contactstatustype.ContactStatusTypeID
					WHERE
							companyuser.CompanyID = '" . intval($portal->CurrentCompany->CompanyID) . "'
							AND contactstatustype.BadContact = 1
							AND $startWhere
							AND $endWhere
							AND submissionlistingtype.Disabled = 0
							AND Expired = 0
						ORDER BY
							submissionlistingtype.ListingType, submissionlisting.DateAdded;";
	$report_res = $portal->mysqlDB->Query($sqlstring);
	
	$grand_tot = 0;
	$status_tot = array();
	$reportArray = array();
	$colArray = array();
	for($i = 0; $i < mysql_num_rows($report_res); $i++)
	{
		$report_row = mysql_fetch_array($report_res, MYSQL_ASSOC);
		$reportArray[$i] = $report_row;
	}
	
	if(is_array($reportArray[0])) {
		foreach($reportArray[0] as $k=>$v)
		{
			$colArray[] = $k;
		}
	}
	
	
	if($_GET['submit'] == 'Export To Excel')
	{
		//do download
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header('Content-type: application/force-download');
		header("Content-Transfer-Encoding: binary");
		header("Content-Disposition: attachment; filename=\"user_lead_report.xls\";" );
		header("Content-Transfer-Encoding: binary");
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
	<title>
		<?= $portal->CurrentCompany->CompanyName ?> Leads on Demand :: Bad Lead Report
	</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<?php
	if($_GET['submit'] != 'Export To Excel')
	{
?>		
	<link type="text/css" media="all" rel="stylesheet" href="report_style.css" />
	<script src="../js/jscalendar/js/jscal2.js"></script>
	<script src="../js/jscalendar/js/lang/en.js"></script>
	<link rel="stylesheet" type="text/css" href="../js/jscalendar/css/jscal2.css" />
	<link rel="stylesheet" type="text/css" href="../js/jscalendar/css/steel/steel.css" />	
	<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />
<script type="text/javascript">//<![CDATA[

      var cal = Calendar.setup({
          onSelect: function(cal) { cal.hide() },
          bottomBar: true,
          dateFormat : "%m/%d/%Y"
      });

//]]></script>
<?php
	}
	else
	{
?>		
	<style type="text/css">
		<?php include("report_style.css"); ?>
	</style>
<?php
	}
?>
</head>
<body>
<?php
	if($_GET['submit'] != 'Export To Excel')
	{
?>
<form method="get" action="<?= $_SERVER['PHP_SELF'] ?>">
<div id="filter_div">
	<div id="left_filter_div">
		
	</div>
	<div id="right_filter_div">
		From: <input name="start_range" id="start_range" type="text" class="date_input" value="<?= strtotime($_GET['start_range']) ? date("m/d/Y", strtotime($_GET['start_range'])) : "" ?>" />
		<script type="text/javascript">
			cal.manageFields("start_range", "start_range", "%m/%d/%Y");
		</script>
		To: <input name="end_range" id="end_range" type="text" class="date_input" value="<?= strtotime($_GET['end_range']) ? date("m/d/Y", strtotime($_GET['end_range'])) : "" ?>"/>
		<script type="text/javascript">
			cal.manageFields("end_range", "end_range", "%m/%d/%Y");
		</script>
		<input name="submit" value="Go" type="submit" />
		<input name="submit" value="Export To Excel" type="submit" />
	</div>
</div>
</form>
<?php
	}
?>
<table class="report_table">
	<tr class="report_table rowheader">
<?php
	foreach($colArray as $col)
	{
?>
		<td class="report_table"><?= $col ?></td>
<?php
	}
?>
	</tr>
<?php
	for($i = 0; $i < sizeof($reportArray); $i++)
	{
?>
	<tr class="report_table<?= $i%2==0 ? ' roweven' : ' rowodd' ?>">
<?php
	foreach($colArray as $col)
	{
?>
		<td class="report_table" align="right"><?= $reportArray[$i][$col] ?></td>
<?php
	}
?>
	</tr>
<?php
		$grand_tot += $user_tot;		
	}
?>
</table>
</body>
</html>