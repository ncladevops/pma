<?php
	require("../../printable/include/mysql.inc.php");
	require("../../printable/include/optimize.printable.inc.php");
	require("../globals.php");
	
	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	
	$portal = new OptimizePortal($COMPANY_ID, $db);
	
	$currentUser = $portal->GetUser($_SESSION['currentuserid']);
			
	if(!$portal->CheckPriv($currentUser->UserID, 'report'))
	{
		header( "Location: " . $portal->CurrentCompany->Website . "home.php?message=" . urlencode( "Accessed Denied." ) );
		die();
	}
	
	if($_GET['supervisorid'] != '')
	{
		$superWhere = "u.supervisorid LIKE '" . intval($_GET['supervisorid']) . "'";
	}
	else
	{
		$superWhere = "1";
	}
	if($_GET['slsid'] != '')
	{
		$slsWhere = "slt.SubmissionListingSourceID LIKE '" . intval($_GET['slsid']) . "'";
	}
	else
	{
		$slsWhere = "1";
	}
	if($_GET['sltid'] != '')
	{
		$sltWhere = "sl.ListingType LIKE '" . intval($_GET['sltid']) . "'";
	}
	else
	{
		$sltWhere = "1";
	}
	if($_GET['listingLevel'] != '')
	{
		$llWhere = "slt.ListingLevel LIKE '" . mysql_real_escape_string(stripslashes($_GET['listingLevel'])) . "'";
	}
	else
	{
		$llWhere = "1";
	}
	if(strtotime($_GET['start_range']))
	{
		$startWhere = "sl.DateAdded >= '" . date("Y-m-d", strtotime($_GET['start_range'])) ." 00:00:00'";
	}
	else
	{
		$startWhere = "sl.DateAdded >= '" . date("Y-m-d") ." 00:00:00'";
		$_GET['start_range'] = date("Y-m-d");
	}
	
	if(strtotime($_GET['end_range']))
	{
		$endWhere = "sl.DateAdded <= '" . date("Y-m-d", strtotime($_GET['end_range'])) ." 23:59:59'";
	}
	else
	{
		$endWhere = "sl.DateAdded <= '" . date("Y-m-d") ." 23:59:59'";
		$_GET['end_range'] = date("Y-m-d");
	}
	
	$sortBy = "u.LastName, u.FirstName";
	
	// Select for Report
	$sqlstring = "SELECT
						u.UserID, CONCAT(u.FirstName, ' ', u.LastName) AS LOName, 
						CONCAT(s.FirstName, ' ', s.LastName) AS Team, COALESCE(us.Available, 0) AS Available,
						COALESCE(u.ListingBudget, 0) AS LeadBudget, COALESCE(lc.LeadCount, 0) AS LeadCount
					FROM
						`user` AS u
					JOIN
						companyuser AS cu
						ON u.UserID = cu.UserID
					LEFT JOIN
						`user` AS s
						ON u.supervisorid = s.UserID
					LEFT JOIN
						usersession AS us
						ON us.UserID = u.UserID
					LEFT JOIN
						(SELECT
							cl.UserID, COALESCE(COUNT(cl.ChangeLogID), 0) AS LeadCount
						FROM
							changelog AS cl
						LEFT JOIN
							submissionlisting AS sl
							ON cl.Key = sl.SubmissionListingID
						LEFT JOIN
							submissionlistingtype AS slt
							ON sl.ListingType = slt.SubmissionListingTypeID
						WHERE
							cl.Field = 'ContactStatusTypeID'
							AND cl.Table = 'submissionlisting'
							AND cl.OldValue IN (1, 36, 38, 42, 43)
							AND $startWhere
							AND $endWhere
							AND $sltWhere
							AND $slsWhere
							AND $llWhere
						GROUP BY
							cl.UserID) AS lc
						ON lc.UserID = u.UserID
					WHERE
						cu.CompanyID = '" . intval($portal->CurrentCompany->CompanyID) . "'
						AND u.LastName != ''
						AND $superWhere
					ORDER BY
						$sortBy";
	$report_res = $portal->mysqlDB->Query($sqlstring);
	
	$reportArray = array();
	for($i = 0; $i < mysql_num_rows($report_res); $i++)
	{
		$report_row = mysql_fetch_array($report_res);
		$reportArray[] = $report_row;
	}
	
	$users = $portal->GetCompanyUsers($superWhere);
	
	$statuses = $portal->GetContactStatusTypes();
	
	$camps = $portal->GetSubmissionListingTypes(0, 'SortOrder, SubmissionListingTypeID', 'all', "((submissionlistingtype.UserManageable = 1 AND submissionlistingtype.SubmissionListingSourceID = 9999) || submissionlistingtype.UserManageable = 0)");
	
	$supers = $portal->GetSupervisors();
	
	$sources = $portal->GetSubmissionListingSources();
	
	if($_GET['submit'] == 'Export To Excel')
	{
		//do download
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header('Content-type: application/force-download');
		header("Content-Transfer-Encoding: binary");
		header("Content-Disposition: attachment; filename=\"user_lead_report.xls\";" );
		header("Content-Transfer-Encoding: binary");
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
	<title>
		<?= $portal->CurrentCompany->CompanyName ?> Leads on Demand :: Lead Distribution Report
	</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<?php
	if($_GET['submit'] != 'Export To Excel')
	{
?>		
	<link type="text/css" media="all" rel="stylesheet" href="report_style.css" />
	<script src="../js/jscalendar/js/jscal2.js"></script>
	<script src="../js/jscalendar/js/lang/en.js"></script>
	<link rel="stylesheet" type="text/css" href="../js/jscalendar/css/jscal2.css" />
	<link rel="stylesheet" type="text/css" href="../js/jscalendar/css/steel/steel.css" />
<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />	
<script type="text/javascript">//<![CDATA[

      var cal = Calendar.setup({
          onSelect: function(cal) { cal.hide() },
          bottomBar: true,
          dateFormat : "%m/%d/%Y"
      });

//]]></script>
<?php
	}
	else
	{
?>		
	<style type="text/css">
		<?php include("report_style.css"); ?>
	</style>
<?php
	}
?>
</head>
<body onload="<?= ($_GET['submit'] != 'Export To Excel' ? "LoadCampaigns('" . $_GET['slsid'] . "', '" . $_GET['sltid'] . "')" : "") ?>">
<?php
	if($_GET['submit'] != 'Export To Excel')
	{
?>
<form method="get" action="<?= $_SERVER['PHP_SELF'] ?>">
<div id="filter_div">
	<div id="left_filter_div">
		<select name="supervisorid">
			<option value="">-- All Teams --</option>
<?php
		foreach($supers as $s)
		{
?>
			<option value="<?= $s->UserID ?>" <?= $_GET['supervisorid'] == $s->UserID ? "SELECTED" : "" ?>><?= "$s->FirstName $s->LastName" ?></option>
<?php
		}
?>
		</select>
<script type="text/javascript">
	var slt_array = new Array();
	slt_array[9999] = new Array();
<?php
		foreach($sources as $s)
		{
?>
	slt_array[<?= $s->SubmissionListingSourceID ?>] = new Array();
<?php
		}
?>
</script>
		<select name="slsid" onchange="LoadCampaigns(this.value, '')">
			<option value="">-- All Sources --</option>
<?php
		foreach($sources as $s)
		{
?>
			<option value="<?= $s->SubmissionListingSourceID ?>" <?= $_GET['slsid'] == $s->SubmissionListingSourceID ? "SELECTED" : "" ?>><?= $s->ListingSourceName ?></option>
<?php
		}
?>
			<option value="9999" <?= $_GET['slsid'] == "9999" ? "SELECTED" : "" ?>>Other Leads</option>
		</select>
<script type="text/javascript">
<?php
		foreach($camps as $c)
		{
			$sourceID = $c->SubmissionListingSourceID != "" ? intval($c->SubmissionListingSourceID) : "999";
?>
	slt_array[<?= $sourceID ?>][<?= $c->SubmissionListingTypeID ?>] = '<?= $c->ListingDescription ?>';
<?php
		}
?>

	function LoadCampaigns(sourceID, campID)
	{
		var campSelect = document.getElementById("sltid");
		
		if(sourceID != '')
		{
			campSelect.options.length = 1;
			var i = 1;
			campSelect[0].value = "";
			campSelect[0].text = "-- All Campaigns --";
			for(var id in slt_array[sourceID])
			{
				campSelect.options.length++;
				campSelect[i].value = id;
				campSelect[i].text = slt_array[sourceID][id];
				i++;
			}
			campSelect.value = campID;
		}
		else
		{
			campSelect.options.length = 1;
			var i = 1;
			campSelect[0].value = "";
			campSelect[0].text = "-- All Campaigns --";
			for(var sid in slt_array)
			{
				for(var id in slt_array[sid])
				{
					campSelect.options.length++;
					campSelect[i].value = id;
					campSelect[i].text = slt_array[sid][id];
					i++;
				}
			}
			campSelect.value = campID;
		}
	}
</script>
		<select name="sltid" id="sltid">
			<option value="">-- All Campaigns --</option>
<?php
		foreach($camps as $c)
		{
?>
			<option value="<?= $c->SubmissionListingTypeID ?>" <?= $_GET['sltid'] == $c->SubmissionListingTypeID ? "SELECTED" : "" ?>><?= $c->ListingDescription ?></option>
<?php
		}
?>
		</select>
		<div id="filter_row_2">
			<select name="listingLevel">
				<option value="">-- All Campaign Types --</option>
<?php
	foreach($leadLevels as $l)
	{
?>
					<option value="<?= $l ?>"
						<?= $l == $_GET['listingLevel'] ? "SELECTED" : "" ?>>
						<?= $l ?>
					</option>
<?php
	}
?>
			</select>
		</div>	
	</div>
	<div id="right_filter_div">
		From: <input name="start_range" id="start_range" type="text" class="date_input" value="<?= strtotime($_GET['start_range']) ? date("m/d/Y", strtotime($_GET['start_range'])) : "" ?>" />
		<script type="text/javascript">
			cal.manageFields("start_range", "start_range", "%m/%d/%Y");
		</script>
		To: <input name="end_range" id="end_range" type="text" class="date_input" value="<?= strtotime($_GET['end_range']) ? date("m/d/Y", strtotime($_GET['end_range'])) : "" ?>"/>
		<script type="text/javascript">
			cal.manageFields("end_range", "end_range", "%m/%d/%Y");
		</script>
		<input name="submit" value="Go" type="submit" />
		<input name="submit" value="Export To Excel" type="submit" />
	</div>
</div>
</form>
<?php
	}
?>
<table class="report_table">
	<tr class="report_table rowheader">
		<td class="report_table">LO Name</td>
		<td class="report_table">Team</td>
		<td class="report_table">Leads Taken</td>
		<td class="report_table">Lead Budget</td>
		<td class="report_table">Status</td>
	</tr>
<?php
	$i = 0;
	foreach($reportArray as $ra)
	{
		$i++;
?>
	<tr class="report_table<?= $i%2==0 ? ' roweven' : ' rowodd' ?>">
		<td class="report_table">
			<?= $ra['LOName'] ?>
		</td>
		<td class="report_table">
			<?= $ra['Team'] ?>
		</td>
		<td class="report_table">
			<?= $ra['LeadCount'] ?>
		</td>
		<td class="report_table">
			<?= $ra['LeadBudget'] * getWorkingDays($_GET['start_range'], $_GET['end_range'], array()) ?>
		</td>
		<td class="report_table" align="center">
<?php 
		if($_GET['submit'] != 'Export To Excel')
		{
			echo $ra['Available'] == 1 ? "<img src='../images/on_small.png' />" : "<img src='../images/off_small.png' />";
		}
		else 
		{
			echo $ra['Available'];
		}
?>
		</td>
	</tr>
<?php
		$grand_tot += $ra['LeadCount'];		
	}
?>
	<tr class="report_table rowheader">
		<td class="report_table">&nbsp;</td>
		<td class="report_table">&nbsp;</td>
		<td class="report_table"><?= $grand_tot ?></td>
		<td class="report_table">&nbsp;</td>
		<td class="report_table">&nbsp;</td>
	</tr>
</table>
</body>
</html>