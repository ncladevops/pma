<?php
	require("../../printable/include/mysql.inc.php");
	require("../../printable/include/optimize.printable.inc.php");
	require("../globals.php");
		
	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	
	$portal = new OptimizePortal($COMPANY_ID, $db);
	
	$currentUser = $portal->GetUser($_SESSION['currentuserid']);
			
	if(!$portal->CheckPriv($currentUser->UserID, 'report'))
	{
		header( "Location: " . $portal->CurrentCompany->Website . "home.php?message=" . urlencode( "Accessed Denied." ) );
		die();
	}
		
	if($_GET['supervisorid'] != '')
	{
		$superWhere = "user.supervisorid LIKE '" . intval($_GET['supervisorid']) . "'";
	}
	else
	{
		$superWhere = "1";
	}
	if($_GET['slsid'] == '999' )
	{
		$slsWhere = "submissionlistingtype.SubmissionListingSourceID is null";
	}
	elseif($_GET['slsid'] != '')
	{
		$slsWhere = "submissionlistingtype.SubmissionListingSourceID LIKE '" . intval($_GET['slsid']) . "'";
	}
	else
	{
		$slsWhere = "1";
	}
	if($_GET['sltid'] != '')
	{
		$sltWhere = "submissionlisting.ListingType LIKE '" . intval($_GET['sltid']) . "'";
	}
	else
	{
		$sltWhere = "1";
	}
	if($_GET['listingLevel'] != '')
	{
		$llWhere = "submissionlistingtype.ListingLevel LIKE '" . mysql_real_escape_string(stripslashes($_GET['listingLevel'])) . "'";
	}
	else
	{
		$llWhere = "1";
	}
	if(!strtotime($_GET['start_range']))
	{
		$_GET['start_range'] = date('m/1/Y');
	}
	$startWhere = "changelog.ChangeTime >= '" . date("Y-m-d", strtotime($_GET['start_range'])) ." 00:00:00'";
	
	if(!strtotime($_GET['end_range']))
	{
		$_GET['end_range'] = date('m/d/Y');
	}
	$endWhere = "changelog.ChangeTime <= '" . date("Y-m-d", strtotime($_GET['end_range'])) ." 23:59:59'";	
		
	$statuses = $portal->GetContactStatusTypes();
	
	$camps = $portal->GetSubmissionListingTypes(0, 'SortOrder, SubmissionListingTypeID', 'all', "((submissionlistingtype.UserManageable = 1 AND submissionlistingtype.SubmissionListingSourceID = 9999) || submissionlistingtype.UserManageable = 0)");
	
	$supers = $portal->GetSupervisors();
	
	$sources = $portal->GetSubmissionListingSources();
	
	$users = $portal->GetCompanyUsers();
	
	$teams = array();
	
	foreach($supers as $s)
	{
		$teams[$s->UserID] = array();
		$teams[$s->UserID]['Supervisor'] = $s;
		$teams[$s->UserID]['Members'] = array();
		$teams[$s->UserID]['Totals'] = array();
	}
	
	foreach($users as $u)
	{
		if(isset($teams[$u->supervisorid]))
		{
			$teams[$u->supervisorid]['Members'][$u->UserID] = array();
			$teams[$u->supervisorid]['Members'][$u->UserID]['User'] = $u;
			$teams[$u->supervisorid]['Members'][$u->UserID]['Totals'] = array();
		}
	}
	
	// Select for Report
	$sqlstring = "SELECT
						user.UserID, user.supervisorid, changelog.NewValue AS ContactStatusTypeID, 
						COUNT(submissionlisting.SubmissionListingID) AS LeadCount
					FROM
						changelog
					JOIN
						submissionlisting
						ON changelog.Key = submissionlisting.SubmissionListingID
							AND changelog.Field = 'ContactStatusTypeID'
							AND changelog.OldValue != ''
					JOIN
						submissionlistingtype
						ON submissionlisting.ListingType = submissionlistingtype.SubmissionListingTypeID
					JOIN
						`user`
						ON submissionlisting.UserID = user.UserID
					JOIN
						companyuser
						ON user.UserID = companyuser.UserID AND companyuser.CompanyID = '" . intval($portal->CurrentCompany->CompanyID) . "'
					WHERE
						companyuser.CompanyID = '" . intval($portal->CurrentCompany->CompanyID) . "'
											AND $sltWhere
											AND $startWhere
											AND $endWhere
											AND $slsWhere
											AND $llWhere
					GROUP BY
						submissionlisting.UserID, changelog.NewValue";
	$report_res = $portal->mysqlDB->Query($sqlstring);
	
	$reportArray = array();
	$grand_total = 0;
	for($i = 0; $i < mysql_num_rows($report_res); $i++)
	{
		$report_row = mysql_fetch_array($report_res);
		
		if(isset($teams[$report_row['supervisorid']]['Members'][$report_row['UserID']]))
		{
			$teams[$report_row['supervisorid']]['Members'][$report_row['UserID']]['Totals'][$report_row['ContactStatusTypeID']]
				= $report_row['LeadCount'];
			$teams[$report_row['supervisorid']]['Members'][$report_row['UserID']]['Totals']['GrandTotal']
				+= $report_row['LeadCount'];
			if($report_row['ContactStatusTypeID'] != 1 && $report_row['ContactStatusTypeID'] != 2 )
			{
				$teams[$report_row['supervisorid']]['Members'][$report_row['UserID']]['Totals']['ContactTotal']
					+= $report_row['LeadCount'];
			}
			if($report_row['ContactStatusTypeID'] == 27)
			{
				$teams[$report_row['supervisorid']]['Members'][$report_row['UserID']]['Totals']['LiveContact']
					+= $report_row['LeadCount'];
			}
			if($report_row['ContactStatusTypeID'] == 21 || $report_row['ContactStatusTypeID'] == 4)
			{
				$teams[$report_row['supervisorid']]['Members'][$report_row['UserID']]['Totals']['ApplicationTotal']
					+= $report_row['LeadCount'];
			}
			if($report_row['ContactStatusTypeID'] == 4)
			{
				$teams[$report_row['supervisorid']]['Members'][$report_row['UserID']]['Totals']['SubmittedTotal']
					+= $report_row['LeadCount'];
			}
				
			$teams[$report_row['supervisorid']]['Totals'][$report_row['ContactStatusTypeID']]
				+= $report_row['LeadCount'];
			$teams[$report_row['supervisorid']]['Totals']['GrandTotal']
				+= $report_row['LeadCount'];
			if($report_row['ContactStatusTypeID'] != 1 && $report_row['ContactStatusTypeID'] != 2 )
			{
				$teams[$report_row['supervisorid']]['Totals']['ContactTotal']
					+= $report_row['LeadCount'];
			}
			if($report_row['ContactStatusTypeID'] == 27)
			{
				$teams[$report_row['supervisorid']]['Totals']['LiveContact']
					+= $report_row['LeadCount'];
			}
			if($report_row['ContactStatusTypeID'] == 21 || $report_row['ContactStatusTypeID'] == 4)
			{
				$teams[$report_row['supervisorid']]['Totals']['ApplicationTotal']
					+= $report_row['LeadCount'];
			}
			if($report_row['ContactStatusTypeID'] == 4)
			{
				$teams[$report_row['supervisorid']]['Totals']['SubmittedTotal']
					+= $report_row['LeadCount'];
			}
		}
	}
		
	if($_GET['submit'] == 'Export To Excel')
	{
		//do download
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header('Content-type: application/force-download');
		header("Content-Transfer-Encoding: binary");
		header("Content-Disposition: attachment; filename=\"lead_status_report.xls\";" );
		header("Content-Transfer-Encoding: binary");
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
	<title>
		<?= $portal->CurrentCompany->CompanyName ?> Leads on Demand :: Report by Team
	</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<?php
	if($_GET['submit'] != 'Export To Excel')
	{
?>		
	<link type="text/css" media="all" rel="stylesheet" href="report_style.css" />
	<script src="../js/jscalendar/js/jscal2.js"></script>
	<script src="../js/jscalendar/js/lang/en.js"></script>
	<link rel="stylesheet" type="text/css" href="../js/jscalendar/css/jscal2.css" />
	<link rel="stylesheet" type="text/css" href="../js/jscalendar/css/steel/steel.css" />
	<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />	
<script type="text/javascript">//<![CDATA[

      var cal = Calendar.setup({
          onSelect: function(cal) { cal.hide() },
          bottomBar: true,
          dateFormat : "%m/%d/%Y"
      });

//]]></script>	
<?php
	}
	else
	{
?>		
	<style type="text/css">
		<?php include("report_style.css"); ?>
	</style>
<?php
	}
?>
</head>
<body onload="<?= ($_GET['submit'] != 'Export To Excel' ? "LoadCampaigns('" . $_GET['slsid'] . "', '" . $_GET['sltid'] . "')" : "") ?>">
<?php
	if($_GET['submit'] != 'Export To Excel')
	{
?>
<form method="get" action="<?= $_SERVER['PHP_SELF'] ?>">
<div id="filter_div">
	<div id="left_filter_div">
		<select name="supervisorid">
			<option value="">-- All Teams --</option>
<?php
		foreach($supers as $s)
		{
?>
			<option value="<?= $s->UserID ?>" <?= $_GET['supervisorid'] == $s->UserID ? "SELECTED" : "" ?>><?= "$s->FirstName $s->LastName" ?></option>
<?php
		}
?>
		</select>
<script type="text/javascript">
	var slt_array = new Array();
	slt_array[9999] = new Array();
<?php
		foreach($sources as $s)
		{
?>
	slt_array[<?= $s->SubmissionListingSourceID ?>] = new Array();
<?php
		}
?>
</script>
		<select name="slsid" onchange="LoadCampaigns(this.value, '')">
			<option value="">-- All Sources --</option>
<?php
		foreach($sources as $s)
		{
?>
			<option value="<?= $s->SubmissionListingSourceID ?>" <?= $_GET['slsid'] == $s->SubmissionListingSourceID ? "SELECTED" : "" ?>><?= $s->ListingSourceName ?></option>
<?php
		}
?>
			<option value="9999" <?= $_GET['slsid'] == "9999" ? "SELECTED" : "" ?>>Other Leads</option>
		</select>
<script type="text/javascript">
<?php
		foreach($camps as $c)
		{
			$sourceID = $c->SubmissionListingSourceID != "" ? intval($c->SubmissionListingSourceID) : "999";
?>
	slt_array[<?= $sourceID ?>][<?= $c->SubmissionListingTypeID ?>] = '<?= $c->ListingDescription ?>';
<?php
		}
?>

	function LoadCampaigns(sourceID, campID)
	{
		var campSelect = document.getElementById("sltid");
		
		if(sourceID != '')
		{
			campSelect.options.length = 1;
			var i = 1;
			campSelect[0].value = "";
			campSelect[0].text = "-- All Campaigns --";
			for(var id in slt_array[sourceID])
			{
				campSelect.options.length++;
				campSelect[i].value = id;
				campSelect[i].text = slt_array[sourceID][id];
				i++;
			}
			campSelect.value = campID;
		}
		else
		{
			campSelect.options.length = 1;
			var i = 1;
			campSelect[0].value = "";
			campSelect[0].text = "-- All Campaigns --";
			for(var sid in slt_array)
			{
				for(var id in slt_array[sid])
				{
					campSelect.options.length++;
					campSelect[i].value = id;
					campSelect[i].text = slt_array[sid][id];
					i++;
				}
			}
			campSelect.value = campID;
		}
	}
</script>
		<select name="sltid" id="sltid">
			<option value="">-- All Campaigns --</option>
<?php
		foreach($camps as $c)
		{
?>
			<option value="<?= $c->SubmissionListingTypeID ?>" <?= $_GET['sltid'] == $c->SubmissionListingTypeID ? "SELECTED" : "" ?>><?= $c->ListingDescription ?></option>
<?php
		}
?>
		</select>
		<div id="filter_row_2">
			<select name="listingLevel">
				<option value="">-- All Campaign Types --</option>
<?php
	foreach($leadLevels as $l)
	{
?>
					<option value="<?= $l ?>"
						<?= $l == $_GET['listingLevel'] ? "SELECTED" : "" ?>>
						<?= $l ?>
					</option>
<?php
	}
?>
			</select>
		</div>
	</div>
	<div id="right_filter_div">
		From: <input name="start_range" id="start_range" type="text" class="date_input" value="<?= strtotime($_GET['start_range']) ? date("m/d/Y", strtotime($_GET['start_range'])) : "" ?>" />
		<script type="text/javascript">
			cal.manageFields("start_range", "start_range", "%m/%d/%Y");
		</script>
		To: <input name="end_range" id="end_range" type="text" class="date_input" value="<?= strtotime($_GET['end_range']) ? date("m/d/Y", strtotime($_GET['end_range'])) : "" ?>"/>
		<script type="text/javascript">
			cal.manageFields("end_range", "end_range", "%m/%d/%Y");
		</script>
		<input name="submit" value="Go" type="submit" />
		<input name="submit" value="Export To Excel" type="submit" />
	</div>
</div>
</form>
<?php
	}
?>
<div class="report_title">Report by Teams</div>
<table class="report_table">
	<tr class="report_table rowheader">
		<td class="report_table" style='border-style: solid solid none solid;'>
			Leads <?= (strtotime($_GET['start_range']) ? date("m/d", strtotime($_GET['start_range'])) : "")
						. " - " .
						(strtotime($_GET['end_range']) ? date("m/d", strtotime($_GET['end_range'])) : "") ?>
		</td>
<?php
	foreach($teams as $t)
	{
?>
	<td class="report_table" colspan="3" style='border-style: solid solid none solid;'>
		<?= $t['Supervisor']->FirstName . " " . $t['Supervisor']->LastName ?>
	</td>
<?php 
	}
?>
	</tr>
	<tr class="report_table rowheader">
		<td class="report_table" style='border-style: none solid solid solid;'>
			&nbsp;
		</td>
<?php
	foreach($teams as $t)
	{
?>
		<td class="report_table header_small" style='border-style: none none solid solid;'>
			Count
		</td>
		<td class="report_table header_small" style='border-style: none none solid none;' nowrap="nowrap">
			Total Leads
		</td>
		<td class="report_table header_small" style='border-style: none solid solid none;'>
			Percentage
		</td>
<?php 
	}
?>
	</tr>
<?php
	$i = 0;
	foreach($statuses as $stat)
	{
		$i++;
?>
	<tr class="report_table<?= $i%2==0 ? ' roweven' : ' rowodd' ?>">
		<td nowrap="nowrap">
			<?= $stat->ContactStatus ?>
		</td>
<?php
		foreach($teams as $t)
		{
			$count = $t['Totals'][$stat->ContactStatusTypeID];
			$total = $t['Totals']['GrandTotal'];
			$percent = intval($count) > 0 && intval($total) > 0
						? intval($count/$total*100)
						: 0;
			
?>
		<td style='border-left: 1px solid #6699CC;' align="right">
			<?= intval($count) > 0 ? intval($count) : "-" ?>
		</td>
		<td align="right">
			<?= number_format($total) ?>
		</td>
		<td align="right">
			<?= intval($percent) ?>%
		</td>
<?php 
		}
?>
	</tr>
	
<?php 
	}
?>		
	<tr class="report_table rowheader">
		<td class="report_table">
			Total
		</td>
<?php
	foreach($teams as $t)
	{
?>
		<td class="report_table header_small" style='border-style: solid none solid solid;' align="right">
			<?= number_format($t['Totals']['GrandTotal']) ?>
		</td>
		<td class="report_table header_small" style='border-style: solid none solid none;'>
			&nbsp;
		</td>
		<td class="report_table header_small" style='border-style: solid solid solid none;'>
			&nbsp;
		</td>
<?php 
	}
?>
	</tr>
	<tr class="report_table rowheader">
		<td class="report_table">
			Overall Contact Ratio
		</td>
<?php
	foreach($teams as $t)
	{
?>
		<td class="report_table header_small" style='border-style: solid none solid solid;' align="right">
			<?= $t['Totals']['GrandTotal'] > 0 ? number_format($t['Totals']['ContactTotal'] / $t['Totals']['GrandTotal'] * 100) : 0 ?>%
		</td>
		<td class="report_table header_small" style='border-style: solid none solid none;'>
			&nbsp;
		</td>
		<td class="report_table header_small" style='border-style: solid solid solid none;'>
			&nbsp;
		</td>
<?php 
	}
?>
	</tr>	
	<tr class="report_table rowheader">
		<td class="report_table">
			Live Contact Ratio
		</td>
<?php
	foreach($teams as $t)
	{
?>
		<td class="report_table header_small" style='border-style: solid none solid solid;' align="right">
			<?= $t['Totals']['ContactTotal'] > 0 ? number_format($t['Totals']['LiveContact'] / $t['Totals']['ContactTotal'] * 100) : 0 ?>%
		</td>
		<td class="report_table header_small" style='border-style: solid none solid none;'>
			&nbsp;
		</td>
		<td class="report_table header_small" style='border-style: solid solid solid none;'>
			&nbsp;
		</td>
<?php 
	}
?>
	</tr>	
	<tr class="report_table rowheader">
		<td class="report_table">
			Application Ratio
		</td>
<?php
	foreach($teams as $t)
	{
?>
		<td class="report_table header_small" style='border-style: solid none solid solid;' align="right">
			<?= $t['Totals']['LiveContact'] > 0 ? number_format($t['Totals']['ApplicationTotal'] / $t['Totals']['LiveContact'] * 100) : 0 ?>%
		</td>
		<td class="report_table header_small" style='border-style: solid none solid none;'>
			&nbsp;
		</td>
		<td class="report_table header_small" style='border-style: solid solid solid none;'>
			&nbsp;
		</td>
<?php 
	}
?>
	</tr>	
	<tr class="report_table rowheader">
		<td class="report_table">
			Submitted Ratio
		</td>
<?php
	foreach($teams as $t)
	{
?>
		<td class="report_table header_small" style='border-style: solid none solid solid;' align="right">
			<?= $t['Totals']['ApplicationTotal'] > 0 ? number_format($t['Totals']['SubmittedTotal'] / $t['Totals']['ApplicationTotal'] * 100) : 0 ?>%
		</td>
		<td class="report_table header_small" style='border-style: solid none solid none;'>
			&nbsp;
		</td>
		<td class="report_table header_small" style='border-style: solid solid solid none;'>
			&nbsp;
		</td>
<?php 
	}
?>
	</tr>
</table>
<div class="report_title">Report by Team/Loan Officer</div>
<?php
	foreach($teams as $t)
	{
		if($_GET['supervisorid'] == '' || $_GET['supervisorid'] == $t['Supervisor']->UserID )
		{
?>
<div class="report_subtitle"><?= $t['Supervisor']->FirstName . " " . $t['Supervisor']->LastName ?></div>
<table class="report_table">
	<tr class="report_table rowheader">
		<td class="report_table" style='border-style: solid solid none solid;'>
			Leads <?= (strtotime($_GET['start_range']) ? date("m/d", strtotime($_GET['start_range'])) : "")
						. " - " .
						(strtotime($_GET['end_range']) ? date("m/d", strtotime($_GET['end_range'])) : "") ?>
		</td>
<?php
			foreach($t['Members'] as $m)
			{
?>
	<td class="report_table" colspan="3" style='border-style: solid solid none solid;'>
		<?= $m['User']->FirstName . " " . $m['User']->LastName ?>
	</td>
<?php 
			}
?>
	</tr>
	<tr class="report_table rowheader">
		<td class="report_table" style='border-style: none solid solid solid;'>
			&nbsp;
		</td>
<?php
			foreach($t['Members'] as $m)
			{
?>
		<td class="report_table header_small" style='border-style: none none solid solid;'>
			Count
		</td>
		<td class="report_table header_small" style='border-style: none none solid none;' nowrap="nowrap">
			Total Leads
		</td>
		<td class="report_table header_small" style='border-style: none solid solid none;'>
			Percentage
		</td>
<?php 
			}
?>
	</tr>
<?php
			$i = 0;
			foreach($statuses as $stat)
			{
				$i++;
?>
	<tr class="report_table<?= $i%2==0 ? ' roweven' : ' rowodd' ?>">
		<td nowrap="nowrap">
			<?= $stat->ContactStatus ?>
		</td>
<?php
				foreach($t['Members'] as $m)
				{
					$count = $m['Totals'][$stat->ContactStatusTypeID];
					$total = $m['Totals']['GrandTotal'];
					$percent = intval($count) > 0 && intval($total) > 0
								? intval($count/$total*100)
								: 0;			
?>
		<td style='border-left: 1px solid #6699CC;' align="right">
			<?= intval($count) > 0 ? intval($count) : "-" ?>
		</td>
		<td align="right">
			<?= number_format($total) ?>
		</td>
		<td align="right">
			<?= intval($percent) ?>%
		</td>
<?php 
				}
?>
	</tr>
	
<?php 
			}
?>	
	<tr class="report_table rowheader">
		<td class="report_table">
			Total
		</td>
<?php
			foreach($t['Members'] as $m)
			{
?>
		<td class="report_table header_small" style='border-style: solid none solid solid;' align="right">
			<?= number_format($m['Totals']['GrandTotal']) ?>
		</td>
		<td class="report_table header_small" style='border-style: solid none solid none;'>
			&nbsp;
		</td>
		<td class="report_table header_small" style='border-style: solid solid solid none;'>
			&nbsp;
		</td>
<?php 
			}
?>
	</tr>
	<tr class="report_table rowheader">
		<td class="report_table">
			Overall Contact Ratio
		</td>
<?php
	foreach($t['Members'] as $m)
	{
?>
		<td class="report_table header_small" style='border-style: solid none solid solid;' align="right">
			<?= $m['Totals']['GrandTotal'] > 0 ? number_format($m['Totals']['ContactTotal'] / $m['Totals']['GrandTotal'] * 100) : 0 ?>%
		</td>
		<td class="report_table header_small" style='border-style: solid none solid none;'>
			&nbsp;
		</td>
		<td class="report_table header_small" style='border-style: solid solid solid none;'>
			&nbsp;
		</td>
<?php 
	}
?>
	</tr>	
	<tr class="report_table rowheader">
		<td class="report_table">
			Live Contact Ratio
		</td>
<?php
	foreach($t['Members'] as $m)
	{
?>
		<td class="report_table header_small" style='border-style: solid none solid solid;' align="right">
			<?= $m['Totals']['ContactTotal'] > 0 ? number_format($m['Totals']['LiveContact'] / $m['Totals']['ContactTotal'] * 100) : 0 ?>%
		</td>
		<td class="report_table header_small" style='border-style: solid none solid none;'>
			&nbsp;
		</td>
		<td class="report_table header_small" style='border-style: solid solid solid none;'>
			&nbsp;
		</td>
<?php 
	}
?>
	</tr>	
	<tr class="report_table rowheader">
		<td class="report_table">
			Application Ratio
		</td>
<?php
	foreach($t['Members'] as $m)
	{
?>
		<td class="report_table header_small" style='border-style: solid none solid solid;' align="right">
			<?= $m['Totals']['LiveContact'] > 0 ? number_format($m['Totals']['ApplicationTotal'] / $m['Totals']['LiveContact'] * 100) : 0 ?>%
		</td>
		<td class="report_table header_small" style='border-style: solid none solid none;'>
			&nbsp;
		</td>
		<td class="report_table header_small" style='border-style: solid solid solid none;'>
			&nbsp;
		</td>
<?php 
	}
?>
	</tr>	
	<tr class="report_table rowheader">
		<td class="report_table">
			Submitted Ratio
		</td>
<?php
	foreach($t['Members'] as $m)
	{
?>
		<td class="report_table header_small" style='border-style: solid none solid solid;' align="right">
			<?= $m['Totals']['ApplicationTotal'] > 0 ? number_format($m['Totals']['SubmittedTotal'] / $m['Totals']['ApplicationTotal'] * 100) : 0 ?>%
		</td>
		<td class="report_table header_small" style='border-style: solid none solid none;'>
			&nbsp;
		</td>
		<td class="report_table header_small" style='border-style: solid solid solid none;'>
			&nbsp;
		</td>
<?php 
	}
?>
	</tr>
</table>
<?php 
		}
	}
?>	
</body>
</html>