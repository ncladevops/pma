<?php
	require("../../printable/include/mysql.inc.php");
	require("../../printable/include/optimize.printable.inc.php");
	require("../globals.php");
	
	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	
	$portal = new OptimizePortal($COMPANY_ID, $db);
	
	$currentUser = $portal->GetUser($_SESSION['currentuserid']);
	$currentGroup = $portal->GetGroup($currentUser->GroupID);
			
	if(!$portal->CheckPriv($currentUser->UserID, 'report'))
	{
		header( "Location: " . $portal->CurrentCompany->Website . "home.php?message=" . urlencode( "Accessed Denied." ) );
		die();
	}
	
	if($_GET['supervisorid'] != '')
	{
		$superWhere = "u.supervisorid LIKE '" . intval($_GET['supervisorid']) . "'";
	}
	else
	{
		$superWhere = "1";
	}
	
	
	if(strtotime($_GET['start_range']))
	{
		$startWhere = "sl.DateAdded >= '" . date("Y-m-d", strtotime($_GET['start_range'])) ." 00:00:00'";
	}
	else
	{
		$startWhere = "1";
	}
	
	if(strtotime($_GET['end_range']))
	{
		$endWhere = "sl.DateAdded <= '" . date("Y-m-d", strtotime($_GET['end_rangeo'])) ." 23:59:59'";
	}
	else
	{
		$endWhere = "1";
	}
	
	// Select for Report
	$sqlstring = "SELECT
						COALESCE(cst.ContactStatusTypeID, 0) as ContactStatusTypeID, COUNT(sl.SubmissionListingID) AS ListCount, cst.`ContactStatus`
					FROM
						submissionlisting AS sl
					LEFT JOIN
						contactstatustype AS cst
						ON sl.ContactStatusTypeID = cst.ContactStatusTypeID
					JOIN
						companyuser AS cu
						ON cu.UserID = sl.UserID
					JOIN
						user AS u
						ON cu.UserID = u.UserID
					WHERE
						cu.CompanyID = '" . intval($portal->CurrentCompany->CompanyID) . "'
						AND $startWhere
						AND $endWhere
						AND $superWhere
						AND sl.ListingType = '" . intval($currentGroup->DefaultSLT) . "'
						AND sl.Expired = 0
					GROUP BY
						cst.ContactStatusTypeID
					ORDER BY
						cst.ContactStatusTypeID";
	$report_res = $portal->mysqlDB->Query($sqlstring);
	
	$reportArray = array();
	$grand_total = 0;
	for($i = 0; $i < mysql_num_rows($report_res); $i++)
	{
		$report_row = mysql_fetch_array($report_res);
		$reportArray[$report_row['ContactStatusTypeID']] = $report_row['ListCount'];
		$grand_total += $report_row['ListCount'];
	}
		
	$statuses = $portal->GetContactStatusTypes($currentUser->GroupID, 2);
	$s = new ContactStatusType();
	$s->ContactStatusTypeID = 0;
	$s->ContactStatus = "(No Status)";
	$statuses[] = $s;
	
	$supers = $portal->GetSupervisors($currentUser->GroupID);
		
	if($_GET['submit'] == 'Export To Excel')
	{
		//do download
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header('Content-type: application/force-download');
		header("Content-Transfer-Encoding: binary");
		header("Content-Disposition: attachment; filename=\"sales_status_report.xls\";" );
		header("Content-Transfer-Encoding: binary");
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
	<title>
		<?= $portal->CurrentCompany->CompanyName ?> Reports on Demand :: SOD Sales Status Report
	</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<?php
	if($_GET['submit'] != 'Export To Excel')
	{
?>		
	<link type="text/css" media="all" rel="stylesheet" href="report_style.css" />
	<script src="../js/jscalendar/js/jscal2.js"></script>
	<script src="../js/jscalendar/js/lang/en.js"></script>
	<link rel="stylesheet" type="text/css" href="../js/jscalendar/css/jscal2.css" />
	<link rel="stylesheet" type="text/css" href="../js/jscalendar/css/steel/steel.css" />	
	<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />

<script type="text/javascript">//<![CDATA[

      var cal = Calendar.setup({
          onSelect: function(cal) { cal.hide() },
          bottomBar: true,
          dateFormat : "%m/%d/%Y"
      });

//]]></script>	
<?php
	}
	else
	{
?>		
	<style type="text/css">
		<?php include("report_style.css"); ?>
	</style>
<?php
	}
?>
</head>
<body onload="<?= ($_GET['submit'] != 'Export To Excel' ? "LoadCampaigns('" . $_GET['slsid'] . "', '" . $_GET['sltid'] . "')" : "") ?>">
<?php
	if($_GET['submit'] != 'Export To Excel')
	{
?>
<form method="get" action="<?= $_SERVER['PHP_SELF'] ?>">
<div id="filter_div">
	<div id="left_filter_div">
		<select name="supervisorid">
			<option value="">-- All Teams --</option>
<?php
		foreach($supers as $s)
		{
?>
			<option value="<?= $s->UserID ?>" <?= $_GET['supervisorid'] == $s->UserID ? "SELECTED" : "" ?>><?= "$s->FirstName $s->LastName" ?></option>
<?php
		}
?>
		</select>

	</div>
	<div id="right_filter_div">
		From: <input name="start_range" id="start_range" type="text" class="date_input" value="<?= strtotime($_GET['start_range']) ? date("m/d/Y", strtotime($_GET['start_range'])) : "" ?>" />
		<script type="text/javascript">
			cal.manageFields("start_range", "start_range", "%m/%d/%Y");
		</script>
		To: <input name="end_range" id="end_range" type="text" class="date_input" value="<?= strtotime($_GET['end_range']) ? date("m/d/Y", strtotime($_GET['end_range'])) : "" ?>"/>
		<script type="text/javascript">
			cal.manageFields("end_range", "end_range", "%m/%d/%Y");
		</script>
		<input name="submit" value="Go" type="submit" />
		<input name="submit" value="Export To Excel" type="submit" />
	</div>
</div>
</form>
<?php
	}
?>
<table class="report_table">
	<tr class="report_table rowheader">
		<td class="report_table">Status</td>
		<td class="report_table">Count</td>
		<td class="report_table">Total</td>
		<td class="report_table">Percentage</td>
	</tr>
<?php
	$i = 0;
	foreach($statuses as $s)
	{
		$i++;
		$percent = ($grand_total <= 0 ? 0 : $reportArray[$s->ContactStatusTypeID]/$grand_total*100);
?>
	<tr class="report_table<?= $i%2==0 ? ' roweven' : ' rowodd' ?>">
		<td class="report_table" align="left"><?= $s->ContactStatus ?></td>
		<td class="report_table" align="right"><?= intval($reportArray[$s->ContactStatusTypeID]) ?></td>
		<td class="report_table" align="right"><?= intval($grand_total); ?></td>
		<td class="report_table" align="right"><?= number_format($percent, 1) ?>%</td>
	</tr>
<?php
	}
?>
</table>
</body>
</html>