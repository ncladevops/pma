<?php
	require("../../printable/include/mysql.inc.php");
	require("../../printable/include/printable.inc.php");
	require("../globals.php");
	
	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	
	$portal = new PrintablePortal($COMPANY_ID, $db);
		
	$currentUser = $portal->GetUser($_SESSION['currentuserid']);
		
	// Check login
	if(!$portal->CheckPriv($currentUser->UserID, 'subadmin'))
	{
		header( "Location: " . $portal->CurrentCompany->Website . "login.php?message=" . urlencode( "Not logged in or login error.  Please try again." ) );
		die();
	}
	
	$blocks = $portal->GetProductBlocks();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
	<title>
		<?= $portal->CurrentCompany->CompanyName ?> :: Available Product Blocks
	</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />		
	<link type="text/css" media="all" rel="stylesheet" href="../style.css" />		
	<link type="text/css" media="all" rel="stylesheet" href="style.css" />	
	<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />
<script>
	function LoadChoice(id, name)
	{
		window.opener.ChooseBlock(id, name);
	}
</script>	
</head>
<body bgcolor="#FFFFFF">
<table width="100%">
	<tr class="tableHeader">
		<td align="center">
			Block
		</td>
		<td>&nbsp;</td>
	</tr>
<?php
	foreach($blocks as $b)
	{
?>
	<tr style="height: 85px;" class="tableDetail">
		<td align="center">
			<?= $b->BlockName; ?><br/>
			<img src="<?= $b->ThumbnailPath; ?>&thumb=true" />
		</td>
		<td>
			<a href="#" onclick="LoadChoice(<?= $b->ProductBlockID ?>, '<?= $b->BlockName ?>'); window.close();">choose</a>
		</td>
	</tr>
<?php
	}
?>
</table>
</body>
</html>