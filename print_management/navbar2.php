<?php
	include_once('reminder.php');
	// TODO Setup timeout 
	// include_once('timeout.php'); 
	if($currentUser)
	{
		$logins = $portal->GetUserLogins($_SESSION['currentuserid']);
	}
?>
<div id="navbar_container">
	<div id="left_navbar_div">
		<div class="navbar_item navbar_item_left">
			<?= $portal->GetCustomMessage('NavbarLink1', false, $currentUser->GroupID)->Message; ?>
		</div>
		<div class="navbar_item navbar_item_left">
			<?= $portal->GetCustomMessage('NavbarLink2', false, $currentUser->GroupID)->Message; ?>
		</div>
		<div class="navbar_item no_right_border navbar_item_left">
			<?= $portal->GetCustomMessage('NavbarLink3', false, $currentUser->GroupID)->Message; ?>
		</div>
	</div>
	<div id="navbar_div">
		<div class="navbar_item<?= $CURRENT_PAGE == 'Home' ? '_selected' : '' ?>" 
			onclick="window.location = 'home.php';">
			<a href="home.php">
				Home
			</a>
		</div>
		<div class="navbar_item<?= $CURRENT_PAGE == 'Marketing' ? '_selected' : '' ?>" 
			onclick="window.location = 'redirect.php?loginid=<?= $logins[$MAIN_STORE_ID]->LoginID; ?>';">
			<a href="redirect.php?loginid=<?= $logins[$MAIN_STORE_ID]->LoginID; ?>">
				My Marketing
			</a>
		</div>
		<div class="navbar_item<?= $CURRENT_PAGE == 'Contacts' ? '_selected' : '' ?>" 
			onclick="window.location = 'contacts.php';">
			<a href="contacts.php">
				My Contacts
			</a>
		</div>
		<div class="navbar_item<?= $CURRENT_PAGE == 'Leads' ? '_selected' : '' ?>" 
			onclick="window.location = 'list_lead.php';">
			<a href="list_lead.php">
				My Leads
			</a>
		</div>
		<div class="navbar_item<?= $CURRENT_PAGE == 'Files' ? '_selected' : '' ?>" 
			onclick="window.location = 'download_file.php';">
			<a href="download_file.php">
				My Files
			</a>
		</div>
<?php
	if($portal->CheckPriv($currentUser->UserID, 'report')) 
	{
?>
		
		<div class="navbar_item<?= $CURRENT_PAGE == 'Reports' ? '_selected' : '' ?>" 
			onclick="window.location = 'report.php';">
			<a href="report.php">
				Reports
			</a>
		</div>
<?php
	 } 
	if($portal->CheckPriv($currentUser->UserID, 'subadmin')) 
	{
?>
		
		<div class="navbar_item<?= $CURRENT_PAGE == 'Control Panel' ? '_selected' : '' ?>" 
			onclick="window.location = 'controlpanel.php';">
			<a href="controlpanel.php">
				Control Panel
			</a>
		</div>
<?php
	 } 
	 if($portal->CheckPriv($currentUser->UserID, 'admin')) 
	{
?>
		
		<div class="navbar_item" 
			onclick="window.location = '/printable/admin';">
			<a href="/printable/admin">
				Admin
			</a>
		</div>
<?php
	 } 
?>
		<div class="navbar_item<?= $CURRENT_PAGE == 'Profile' ? '_selected' : '' ?>" 
			onclick="window.location = 'edit_user.php'">
			<a href="../edit_user.php">
				My Profile
			</a>
		</div>
		<div class="navbar_item" 
			onclick="window.location = 'login.php?logout=true';">
			<a href="login.php?logout=true">
				Logout
			</a>
		</div>
	</div>
</div>