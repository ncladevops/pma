<?php
	require("../../printable/include/mysql.inc.php");
	require("../../printable/include/optimize.printable.inc.php");
	require("../globals.php");
	
	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	
	$portal = new OptimizePortal($COMPANY_ID, $db);
	
	$currentUser = $portal->UserAccess($_SESSION['currentuserid']);
	$isSuper = $portal->CheckPriv($currentUser->UserID, 'supervisor');
			
	if(!$portal->CheckPriv($currentUser->UserID, 'subadmin'))
	{
		header( "Location: " . $portal->CurrentCompany->Website . "home.php?message=" . urlencode( "Accessed Denied." ) );
		die();
	}	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
	<title>
		<?= $portal->CurrentCompany->CompanyName ?> :: Control Panel
	</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />		
	<link type="text/css" media="all" rel="stylesheet" href="../style.css" />
	<link type="text/css" media="all" rel="stylesheet" href="../css/controlpanel.css" />
	<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />
	<script  src="js/func.js"></script>	
</head>
<body bgcolor="#FFFFFF">
<div id="page">
<?php 
include("header.php")
?>
<?php
						$CURRENTPAGE = 'printmanage';
						include("navbar.php") 
					?>
<div id="body">
	<?php 
		$CURRENT_PAGE = "Control Panel";
		//include("navbar2.php"); 
	?>
	<div class="error"><?= $_GET['message']; ?></div>
	<br/>
	<div class="sectionHeader">
		Campaign Management Control Panel
	</div>
	<br/>
	<div>
		<table cellspacing="0" cellpadding="0" align="center" border="1" bgcolor="dfdfdf">
	<tr>
  		<td>
			<table border="0" cellPadding="0" cellSpacing="0" align="center" width="100%">
				<tr>
					<td>
					
					</td>
				</tr>
			</table> 
			<table cellpadding="0" align="center">
				<tr>
	  				<td>
	  					<div align="center" class="headerTextLarge"><?= $HTTP_GET_VARS['message'] ?></div>
			  			<table width="415" border="0" cellpadding="0" cellspacing="0">
		                  <tr>
		                    <td>
		                    	
		                    </td>
		                  </tr>
		                  <tr>
		                    <td><table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
		                        <tr>
		                          <td>
		                          <ul>
		                            <li><a href="manage_product.php" class="blueLink"><img src="add1.gif" width="48" height="56" align="absmiddle" border="0"/>Manage Products</a></li>
		                            <li><a href="manage_trigger.php" class="blueLink"><img src="add1.gif" width="48" height="56" align="absmiddle" border="0"/>Manage Triggers</a></li>
		                          	<li><a href="proof.php" class="blueLink" onclick="wopen('proof.php', 'upload_proof', 600, 450); return false;"><img src="upload.gif" width="48" height="56" align="absmiddle" border="0"/>Upload Proof File</a></li>
		                          </ul>
		                          </td>
		                        </tr>
		                    </table></td>
		                  </tr>
		                </table>
	  				</td>
         		</tr>
         	</table>
		</td>
	</tr>

</table>
	</div>
</div>
</div>
</body>
</html>