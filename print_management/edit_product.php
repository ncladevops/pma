<?php
require("../../printable/include/mysql.inc.php");
require("../../printable/include/printable.inc.php");
require("../../printable/include/contactfactory.inc.php");
require("../globals.php");

$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);

$portal = new PrintablePortal($COMPANY_ID, $db);
$variableRowsMax = $portal->MAX_VARIABLE_FIELDS;

$currentUser = $portal->GetUser($_SESSION['currentuserid']);
$isAdmin = $portal->CheckPriv($currentUser->UserID, 'admin');

// Check login
if (!$portal->CheckPriv($currentUser->UserID, 'subadmin')) {
    header("Location: " . $portal->CurrentCompany->Website . "login.php?message=" . urlencode("Not logged in or login error.  Please try again."));
    die();
}

$product = $portal->GetProduct($_GET['id']);

$factory = new ContactFactory($db);

$contactFields = $factory->GetContactFields($portal->CurrentCompany->CompanyID);

if (!$product) {
    header("Location: index.php?message=" . urlencode("Invalid Product ID"));
    die();
}

$ts = $portal->GetProductTemplates();
$categories = $portal->GetProductCategories('all', true, 'all');

$templates = array();
foreach ($ts as $t) {
    $templates[$t->ProductTemplateID] = $t;
}

$currentTemplate = $templates[$product->PrintableProductID];

if ($_GET['action'] == 'disable' || $_GET['action'] == 'enable') {
    $product->Disabled = ($_GET['action'] == 'disable' ? 1 : 0);

    $portal->UpdateProduct($product);

    header("Location: manage_product.php?message=" . urlencode("Action Completed. Product " . $_GET['action'] . "d."));
    die();
} elseif ($_GET['action'] == 'delete') {
    $tPath = "{$FILES_LOCATION}" . $portal->CurrentCompany->CompanyID . "/products/{$product->ProductID}.jpg";
    // delete old thumb
    if (is_file($tPath)) {
        unlink($tPath);
    }

    $portal->DeleteProduct($product->ProductID);

    header("Location: manage_product.php?message=" . urlencode("Action Completed. Product deleted."));
    die();
} elseif ($_POST['Submit'] == 'Cancel') {
    header("Location: manage_product.php?message=" . urlencode("Action Canceled. Product not updated."));
    die();
} elseif ($_POST['Submit'] == 'Update') {
    // if new thumbnail upload
    if ($_FILES['thumbnail']['name'] != '') {
        $tPath = "{$FILES_LOCATION}" . $portal->CurrentCompany->CompanyID . "/products/{$product->ProductID}.jpg";
        // delete old thumb
        if (is_file($tPath)) {
            unlink($tPath);
        }

        // Copy to output directory
        $tempFileName = $_FILES['thumbnail']['tmp_name'];

        // move to temp	
        move_uploaded_file($tempFileName, $tPath);
    }

    $product->ProductName = $_POST['ProductName'];
    $product->ProductDescription = $_POST['ProductDescription'];
    $product->PrintableProductID = $_POST['PrintableProductID'];
    $product->FrontFileName = $_POST['FrontFileName'];
    $product->BackFileName = $_POST['BackFileName'];
    $product->ProductCost = str_replace(",", "", $_POST['ProductCost']);
    $product->VariableLabels = $_POST['VariableLabels'];
    $product->MetaData = $_POST['MetaData'];
    $product->PageCount = $_POST['PageCount'];
    $product->ProductCategoryID = $_POST['ProductCategoryID'];
    $product->Substrate = $_POST['Substrate'];
    $product->PrintNotes = $_POST['PrintNotes'];
    $product->IncludeDate = $_POST['IncludeDate'];

    $portal->UpdateProduct($product);

    header("Location: manage_product.php?message=" . urlencode("Action Completed. Product updated."));
    die();
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title>
        <?= $portal->CurrentCompany->CompanyName ?> :: Edit Product
    </title>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <link type="text/css" media="all" rel="stylesheet" href="../style.css" />
    <link type="text/css" media="all" rel="stylesheet" href="style.css" />
    <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />
    <script type="text/javascript" src="../js/jquery-1.10.2.min.js"></script>

    <script>
        var templateCount = new Array();
        <?php
        foreach ($templates as $t) {
            ?>
        templateCount[<?= $t->ProductTemplateID ?>] = <?= intval($t->VariableFieldCount) ?>;
        <?php
    }
    ?>

        function loadRows(templateID)
        {
            for(var i=0; i < <?= $variableRowsMax ?>; i++)
            {
                var row = document.getElementById('variableRow' + i)
                if(i < templateCount[templateID])
                {
                    row.style.display = "";
                }
                else
                {
                    row.style.display = "none";
                }

            }
        }



        function initPage()
        {
            <?= $isAdmin ? "" : "DisableInput();" ?>
        }

        function DisableInput()
        {
            var inputs=document.getElementsByTagName('input');

            for(i=0; i < inputs.length; i++)
            {
                inputs[i].disabled = true;
            }

            var selects=document.getElementsByTagName('select');

            for(i=0; i < selects.length; i++)
            {
                selects[i].disabled = true;
            }

            var areas=document.getElementsByTagName('textarea');

            for(i=0; i < areas.length; i++)
            {
                areas[i].disabled = true;
            }
        }


    </script>

    <script type="text/javascript">
        $( document ).ready(function() {

            $('#text-count').html($('#PrintNotes').val().length);

            $("#insert_var").click(function(e) {
                e.preventDefault();
                var text = $('#PrintNotes').val();
                $('#PrintNotes').val(text + '%' + $('#VariableList').val() + '%');

                $('#text-count').html($('#PrintNotes').val().length);
            });

            $('#PrintNotes').bind('input propertychange', function() {
                $('#text-count').html($(this).val().length);
            });

        });
    </script>

</head>
<body bgcolor="#FFFFFF" onload='initPage();'>
<?php
$CURRENT_PAGE = 'productmanage';
include("header.php");
include("navbar.php");
?>
<table cellspacing="0" cellpadding="0" width="770" align="center" bgcolor="#FFFFFF">
    <tr>
        <td>
            <table cellpadding="0" width="700">
                <tr>
                    <td>
                        <form method="post" action="<?= $_SERVER['PHP_SELF'] ?>?id=<?= $product->ProductID ?>" enctype="multipart/form-data">
                            <div align="center" class="headerTextLarge">Manage Print Product</div>
                            <div align="center" class="headerTextLarge"><?= $_GET['message'] ?></div>
                            <div class="fieldSection">
                                <div class="itemSection">
                                    <label>Product Name:</label><br/>
                                    <input type="text" name="ProductName" value="<?= $product->ProductName; ?>"/>
                                </div>
                                <div class="itemSection">
                                    <label>Product Description:</label><br/>
                                    <textarea rows="4" cols="40" name="ProductDescription"><?= $product->ProductDescription; ?></textarea>
                                </div>
                                <div class="itemSection">
                                    <label>Meta Data:</label><br/>
                                    <textarea rows="4" cols="40" name="MetaData"><?= $product->MetaData; ?></textarea>
                                </div>
                                <div class="itemSection">
                                    <label>Template:</label><br/>
                                    <select name="PrintableProductID" onchange="loadRows(this.value);
                                                    document.getElementById('templateWarning').style.display = '';">
                                        <?php
                                        foreach ($templates as $t) {
                                            ?>
                                            <option value="<?= $t->ProductTemplateID ?>"
                                                <?= $t->ProductTemplateID == $product->PrintableProductID ? "SELECTED" : "" ?>>
                                                <?= $t->TemplateName ?>
                                            </option>
                                            <?php
                                        }
                                        ?>
                                    </select><br/>
                                    <span class="warning" style="display: none;" id="templateWarning">Warning if you change templates you will lose current block mappings!</span>
                                </div>
                                <div class="itemSection">
                                    <label for="ProductCategoryID">Category:</label><br/>
                                    <select name="ProductCategoryID">
                                        <option value="0">- None -</option>
                                        <?php
                                        $category = new ProductCategory();
                                        foreach ($categories as $category) {
                                            if ($portal->CheckPriv($currentUser->UserID, 'admin') || $category->GroupID != 0) {
                                                ?>
                                                <option value="<?= $category->ProductCategoryID ?>" <?= $category->ProductCategoryID == $product->ProductCategoryID ? "SELECTED" : "" ?>>
                                                    <?= str_repeat('&nbsp;&nbsp;&nbsp;', $portal->GetProductCategoryLevel($category->ProductCategoryID)) . $category->ProductCategoryName ?>
                                                </option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="itemSection">
                                    <label>Thumbnail:</label><br/>
                                    <img src="<?= $product->ThumbnailPath; ?>&thumb=true" /><br/>
                                    <?php
                                    if ($isAdmin) {
                                        ?>
                                        New file: <input type="file" name="thumbnail" />
                                        <?php
                                    }
                                    ?>
                                </div>
                                <div class="itemSection" id="EmailIdDiv" <?= $product->PrintableProductID == 0 || $product->PrintableProductID == 44 ? "" : "style='display: none'" ?>>
                                    <label>Exact Target Email ID:</label><br/>
                                    <input type="text" name="FrontFileName" value="<?= $product->FrontFileName; ?>"/>
                                </div>
                                <div class="itemSection" id="EmailSubjectDiv" <?= $product->PrintableProductID == 0 || $product->PrintableProductID == 44 ? "" : "style='display: none'" ?>>
                                    <label>Exact Target Subject:</label><br/>
                                    <input type="text" name="BackFileName" value="<?= $product->BackFileName; ?>" style="width: 250px;" />
                                </div>
                                <div class="itemSection" id="ProductCostDiv">
                                    <label>Product Cost:</label><br/>
                                    <input type="text" name="ProductCost" value="<?= number_format($product->ProductCost, 2); ?>"/>
                                </div>
                                <div class="itemSection" id="ProductCostDiv">
                                    <label>Include Appointment Date:</label><br/>
                                    <input type="checkbox" name="IncludeDate" value="1" <?= $product->IncludeDate == 1 ? "CHECKED" : "" ?>/>
                                </div>
                                <div class="itemSection" id="PageCountDiv">
                                    <label>Page Count:</label><br/>
                                    <input type="text" name="PageCount" value="<?= $product->PageCount ?>"/>
                                </div>
                                <div class="itemSection" id="SubstrateDiv">
                                    <label>Substrate:</label><br/>
                                    <input type="text" name="Substrate" value="<?= $product->Substrate ?>"/>
                                </div>
                                <div class="itemSection" <?= $product->PrintableProductID != 4 ? "style='display: none;'" : "" ?>>
                                    <label>Variables:</label><br/>
                                    <select id="VariableList" name="VariableList">
                                        <option value="">- Select Variable -</option>
                                        <?php
                                        foreach ($contactFields as $k) {
                                            ?>
                                            <option value="<?= $k ?>"><?= $k ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                    <button id="insert_var">Insert</button>
                                </div>
                                <div class="itemSection">
                                    <label><?= $product->PrintableProductID != 4 ? "Print Instructions" : "Message:" ?></label><br/>
                                    <textarea id="PrintNotes" <?= $product->PrintableProductID != 4 ? "rows='4' cols='40'" : "rows='8' cols='80'" ?>  name="PrintNotes"><?= $product->PrintNotes; ?></textarea>
                                    <span style="font-size: 11px; <?= $product->PrintableProductID != 4 ? "display: none;" : "" ?>" title="The sytem will auto segment messages with more than 160 characters">Estimated Character Count: <span id='text-count'>0</span> [<a href="#" onclick="alert('The sytem will auto segment messages with more than 160 characters');">?</a>]</span>
                                </div>
                                <?php
                                if ($isAdmin) {
                                    ?>
                                    <div class="itemSection">
                                        <center>
                                            <input type="submit" value="Update" name="Submit"/><input type="submit" value="Cancel" name="Submit"/>
                                        </center>
                                    </div>
                                    <?php
                                }
                                ?>
                                <div class="itemSection">
                                    <label>Variable Block Mappings:</label><br/>
                                    <table width="100%">
                                        <tr class="tableHeader">
                                            <td>Variable section</td>
                                            <td>Mapping Count</td>
                                            <td>Default Block Image</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <?php
                                        for ($i = 0; $i < $variableRowsMax; $i++) {
                                            $currentTemplate->VariableFieldCount;
                                            $defaultProductBlockMap = $portal->GetDefaultProductBlockMap($product->ProductID, $i);
                                            $label = isset($product->VariableLabels[$i]) ? $product->VariableLabels[$i] : "Section " . ($i + 1);
                                            $bmInfo = $portal->GetProductBlockMappingInfo($product->ProductID, $i);
                                            ?>
                                            <tr id="variableRow<?= $i ?>" class="<?= $i % 2 == 0 ? "tableDetailLight" : "tableDetail" ?>" <?= $i < $currentTemplate->VariableFieldCount ? "" : "style='display: none;'" ?> style="height: 60px;">
                                                <td>
                                                    <input type="text" value="<?= $label ?>" name="VariableLabels[<?= $i ?>]" />
                                                </td>
                                                <td><?= $portal->GetProductBlockCount($product->ProductID, $i); ?></td>
                                                <td align="center">
                                                    <?php
                                                    if ($bmInfo->MappingType != 'option' && $bmInfo->MappingType != 'default') {
                                                        echo "Input Type: {$bmInfo->MappingType}<br/>Max Length: {$bmInfo->MaxLength}<br/>Contact Field: {$bmInfo->ContactField}<br/>Default Value: {$bmInfo->DefaultValue}";
                                                    } else if ($defaultProductBlockMap) {
                                                        ?>
                                                        <a href="<?= $defaultProductBlockMap->BlockThumbnailPath; ?>" target="_blank">
                                                            <img src="<?= $defaultProductBlockMap->BlockThumbnailPath; ?>&thumb=true"  style="vertical-align: middle;" />
                                                        </a>
                                                        <?php
                                                    } else {
                                                        echo "NO DEFAULT SELECTED";
                                                    }
                                                    ?>
                                                </td>
                                                <td><a href="edit_productblock.php?productid=<?= $product->ProductID ?>&variable=<?= $i ?>"><?= $isAdmin ? "edit" : "view" ?></a></td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </table>
                                </div>
                            </div>
                        </form>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>