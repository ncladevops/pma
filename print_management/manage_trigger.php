<?php
	require("../../printable/include/mysql.inc.php");
	require("../../printable/include/printable.inc.php");
	require("../globals.php");
	
	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	
	$portal = new PrintablePortal($COMPANY_ID, $db);
		
	$currentUser = $portal->GetUser($_SESSION['currentuserid']);
		
	// Check login
	if(!$portal->CheckPriv($currentUser->UserID, 'subadmin'))
	{
		header( "Location: " . $portal->CurrentCompany->Website . "login.php?message=" . urlencode( "Not logged in or login error.  Please try again." ) );
		die();
	}
	
	$triggers = $portal->GetTriggers();		
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
	<title>
		<?= $portal->CurrentCompany->CompanyName ?> :: Manage Trigger
	</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />		
	<link type="text/css" media="all" rel="stylesheet" href="../style.css" />		
	<link type="text/css" media="all" rel="stylesheet" href="style.css" />		
	<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />
</head>
<body bgcolor="#FFFFFF">
<?php
	$CURRENTPAGE = 'triggermanage';
	include("header.php");
	include("navbar.php");
?>
<table cellspacing="0" cellpadding="0" width="770" align="center" bgcolor="#FFFFFF">
	<tr>
  		<td>
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td>
					<?php
						 
					?>
					</td>
				</tr>
			</table>
			<br/>
			<table cellpadding="0" width="700" border="0" align="center">
				<tr>
	  				<td>
	  					<div align="center" class="headerTextLarge">Manage Print Triggers</div>
	  					<div align="center" class="headerTextLarge"><?= $_GET['message'] ?></div>
			  			<table width="100%">
			  				<tr class="tableHeader">
			  					<td>
			  						Trigger Name
			  					</td>
			  					<td>
			  						Trigger Type
			  					</td>
			  					<td>
			  						Trigger
			  					</td>
			  					<td>
			  						Criteria
			  					</td>
			  					<td>
			  						Action Count
			  					</td>
			  					<td>
			  						&nbsp;
			  					</td>
			  					<td>
			  						&nbsp;
			  					</td>
			  					<td>
			  						&nbsp;
			  					</td>
			  				</tr>
<?php
	$i = 0;
	if(is_array($triggers))
	{
		foreach($triggers as $trigger)
		{
			$disableLabel = $trigger->Disabled ? "enable" : "disable";
			$productTriggers = $portal->GetProductTriggers($trigger->TriggerID);
?>
							<tr class="<?= $i % 2 == 0 ? "tableDetailLight" : "tableDetail"?>">
								<td><?= $trigger->TriggerName ?></td>
								<td><?= $trigger->TriggerType ?></td>
								<td>
<?php 
			switch ($trigger->TriggerType)
			{
				case "Date":
					echo "{$trigger->TriggerObject} is past {$trigger->TriggerValue} days";
					break;
				case "Field":
					echo "{$trigger->TriggerObject} changed to {$trigger->TriggerValue}";
					break;
				default:
					break;
			}
?>
								</td>
								<td>
<?php
			foreach($trigger->Criteria as $crit)
			{
			
?>
											<?= $crit['LeftValue'] . " " . $crit['CompareValue'] . " " .  $crit['RightValue']?><br/>
<?php
			}
?>
								</td>
								<td align="center">
									<?= sizeof($productTriggers) ?>
								</td>
								<td><a href="edit_trigger.php?id=<?= $trigger->TriggerID ?>">edit</a></td>
								<td>
									<a href="edit_trigger.php?id=<?= $trigger->TriggerID ?>&action=<?= $disableLabel ?>" 
										onclick="return confirm('Are you sure you want to <?= $disableLabel ?> this trigger')">
										<?= $disableLabel ?>
									</a>
								</td>
								<td>
									<a href="edit_trigger.php?id=<?= $trigger->TriggerID ?>&action=delete" 
										onclick="return confirm('Are you sure you want to delete this trigger')">
										delete
									</a>
								</td>
							</tr>
<?php
			$i++;
		}
	}
?>
			  			</table>
			  			<div style="padding-top: 5px;">
			  				<a href="add_trigger.php">Add Trigger</a>
			  			</div>
	  				</td>
         		</tr>
         	</table>
		</td>
	</tr>
</table>
</body>
</html>