<?php
	require("../../printable/include/mysql.inc.php");
	require("../../printable/include/printable.inc.php");
	require("../../printable/include/contactfactory.inc.php");
	require('../../printable/include/component/exacttarget.php');
	require("../globals.php");
	
	$GENERIC_FIELDS = array('LogoName' => 'RepImageFileName', 'UserFirstName' => 'RepFirstName', 'UserLastName' => 'RepLastName', 
								'UserTitle' => 'RepTitle', 'UserPhone' => 'RepPhone', 'UserCell' => 'RepCell', 
								'UserFax' => 'RepFax', 'UserEmail' => 'RepEmail', 'EventDate' => 'AppDate', 
								'EventTime' => 'AppTime', 'FirstName' => 'FirstName', 'LastName' => 'LastName', 
								'Company' => 'BusinessName', 'Address1' => 'MailingAddress', 'Address2' => 'Suite', 
								'City' => 'MailingCity', 'State' => 'MailingState', 'Zip' => 'MailingZIPCode', 
								'Barcode' => 'MailingBarcode', 'UserAddress' => 'ReturnAddress1', 'UserAddress2' => 'ReturnAddress2', 
								'UserCity' => 'ReturnCity', 'UserState' => 'ReturnState', 'UserZip' => 'ReturnZIP');
	$TEMPLATE_FIELDS = array('Company', 'Prefix', 'FirstName', 
								'LastName', 'Title', 'Address1', 
								'Address2', 'City', 'State', 
								'Zip', 'Barcode', 'Email', 
								'Phone', 'Phone2', 'Comments', 
								'Username', 'Vertical', 'NAICSTitle', 
								'Employees', 'Sales', 
								'Creative', 'Offer', 'Delivery', 
								'EventType');
	
	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	
	$portal = new PrintablePortal($COMPANY_ID, $db);
	$contactFactory = new ContactFactory($db);
	$etSys = new ExactTargetSystem($EXACT_TARGET_URL, $EXACT_TARGET_USER, $EXACT_TARGET_PASS);
	
	$MAX_VARIABLE_FIELDS = $portal->MAX_VARIABLE_FIELDS;
	
	$contactFields = $contactFactory->GetContactFields($portal->CurrentCompany->CompanyID);
	$dateFields = $contactFactory->GetDateFields($portal->CurrentCompany->CompanyID);
		
	$currentUser = $portal->GetUser($_SESSION['currentuserid']);
		
	// Check login
	if(!$portal->CheckPriv($currentUser->UserID, 'subadmin'))
	{
		header( "Location: " . $portal->CurrentCompany->Website . "login.php?message=" . urlencode( "Not logged in or login error.  Please try again." ) );
		die();
	}
	
	if($_GET['template'] == 'true') {
		// build CSV file from factory contact
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header('Content-type: application/force-download');
		header("Content-Transfer-Encoding: binary");
		header("Content-Disposition: attachment; filename=\"twcbc_proof_template.csv\";" );
		header("Content-Transfer-Encoding: binary");
		
		$outString = "";
		
		foreach($TEMPLATE_FIELDS as $f) {
			$outString .= '"' . addslashes($f) . '",';
		}
		
		$outString .= "\r\n";
		
		echo $outString;
		die();
	}
	elseif($_POST['Submit'] == 'Upload' && $_FILES['uploadedfile']['name'] != '') {
		// open file
		$file = fopen($_FILES['uploadedfile']['tmp_name'], "r");
		// build mapping from first line
		$lookupMap = array();
		$line = fgetcsv($file, 10000, ",", "\"");
		for($i = 0; $i < sizeof($line); $i++) {
			$k = array_search($line[$i], $contactFields);
			if($k) {
				$lookupMap[$contactFields[$k]] = $i;
			}
			else if($line[$i] == 'Username') {
				$lookupMap['Username'] = $i;
			}
		}
		
		$proofArray = array();
		// loop through records
		while (!feof($file))
		{
			$proofObject = new stdClass();
			$proofObject->DateFields = $dateFields;
			$line = fgetcsv($file, 10000, ",", "\"");
			// If lookup set to load user info load from DB
			if(isset($lookupMap['Username'])) {
				$user = $portal->GetUser(0, $line[$lookupMap['Username']]);
				$proofObject->UserFullname = $user->FirstName . " " . $user->LastName;
				$proofObject->UserEmail = $user->ContactEmail;
				$proofObject->UserTitle = $user->Title;
				$proofObject->UserPhone = $user->ContactPhone;
				$proofObject->UserCell = $user->ContactCell;
				$proofObject->UserFax = $user->ContactFax;
				$proofObject->UserAddress = $user->Address;
				$proofObject->UserAddress2 = $user->Address2;
				$proofObject->UserCity = $user->City;
				$proofObject->UserState = $user->State;
				$proofObject->UserZip = $user->Zipcode;
				$proofObject->LogoName = $user->LogoName;
				$proofObject->UserFirstName = $user->FirstName;
				$proofObject->UserLastName = $user->LastName;
				$proofObject->OtherID = $user->OtherID;
				$proofObject->GroupName = $user->GroupName;
			}
			// loop through fields
			foreach($lookupMap as $k=>$v) {
				$proofObject->$k = $line[$v];
			}
			// add data object to array
			$proofArray[] = $proofObject;
		}
		
		// Loop through proof array and build output
		$templateData = array();
		foreach($proofArray as $proofObject) {
			$proofResult = $portal->CheckProofTrigger($proofObject);
			
			if($proofResult) {
				foreach($proofResult as $p)
					$templateData[$p['TemplateID']][] = $p['TemplateData'];
			}
		}
		
		// Write template data to files
		$outputFiles = array();
		foreach($templateData as $tID=>$tData) {
			$recordCount = 0;
			$template = $portal->GetProductTemplate($tID);
			
			// If Template is Email send messages
			if($tID == 0) {
				$filename = "Email Template";
				
				// Schedule Emails
				foreach($tData as $record) {
					$recordCount++;
					$failedCount = 0;
					
					// Build Contact
					$contact = new stdClass();
					$i = 0;
					foreach ($GENERIC_FIELDS as $k=>$v) {
						$contact->$k = $record[$i];
						$i++;
					}
					$contact->UserFullname = $contact->UserFirstName . " " . $contact->UserLastName;		
					$vFields = array();
					// Build Variable Fields
					for($j = 0; $j < $MAX_VARIABLE_FIELDS; $j++)
					{	
						$vFields[$j] = $record[$i + $j];
					}
					$contact->Email = $record[$i + $j + 1];

					// create Triggered Send
					$res = $etSys->TriggeredSend($contact, $portal->CurrentCompany->ExactTargetClientID, $record[$i + $j],
										$vFields, $record[$i + $j + 2]);
								
					// Update Exact Target Contact
					$res = $etSys->UpdateContact($contact, $portal->CurrentCompany->ExactTargetClientID, $portal->CurrentCompany->ExactTargetListID, $vFields, $record[$i + $j + 2]);
					
					if($res->Results->StatusCode != "OK") {
						$failedCount++;
					}
				}
			}
			else {
				$filename = str_replace(" ", "_", $template->TemplateName) . "_" . date("YmdHis") . ".csv";
				
				$outString = "";
				
				// Build file Header line
				foreach($GENERIC_FIELDS as $k=>$v)
				{
					$outString .= '"' . addslashes($v) . '",';
				}
				for($i = 0; $i < $MAX_VARIABLE_FIELDS; $i++)
				{
					$outString .= '"Variable ' . ($i + 1) . '",';
				}
				$outString .= "\r\n";
				
				// Build file output string
				foreach($tData as $record) {
					$recordCount++;
					foreach($record as $field) {
						$outString .= '"' . addslashes($field) . '",';
					}				
					$outString .= "\r\n";
				}
				
				// Write file to disk
				$file = fopen("proof_output/" . $filename, "w");
				fputs($file, $outString);
				fclose($file);
			}
			
			// save file name and quantity for link
			$outputFiles[$tID]['FileName'] = $filename;
			$outputFiles[$tID]['RecordCount'] = $recordCount;
			$outputFiles[$tID]['FailedCount'] = $failedCount;
		}
		
		$thankyou = true;
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
	<title>
		<?= $portal->CurrentCompany->CompanyName ?> :: Proof Processing
	</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />		
	<link type="text/css" media="all" rel="stylesheet" href="../style.css" />		
	<link type="text/css" media="all" rel="stylesheet" href="style.css" />
	<link type="text/css" media="all" rel="stylesheet" href="proof.css" />	
	<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />
</head>
<body bgcolor="#FFFFFF" onload="<?= $close_window ? "window.close();" : "" ?>">
	<?php
		include("header.php");
		include("navbar.php");
	?>
	<form method="post" action="<?= $_SERVER['PHP_SELF'] ?>" enctype="multipart/form-data" >
	<div id="body">
	  	<div class="pageHeader">Upload Proof File</div>
	  	<div id="errorMessage"><?= $errorText ?></div>
	  	<div id="mainContainer">
<?php
	if($thankyou)
	{
		if(sizeof($outputFiles) <= 0) {
?>
			<div class="sectionHeader">Processing Complete</div><br />
			The input file that was uploaded did not trigger any mail files.<br />
			<input type="button" onclick="window.location='proof.php'" value="Try Again" />&nbsp;&nbsp;&nbsp;<input name="Cancel" type="button" value="Cancel" onclick="window.close();"/>
<?php
		}
		else {
?>
			<div class="sectionHeader">Processing Complete</div><br />
			The following Mail events were triggered:
			<ul>
<?php
			foreach($outputFiles as $oFile) {
				if($oFile['FileName'] != 'Email Template') {
?>
				<li><a href="proof_output/<?= $oFile['FileName'] ?>"><?= $oFile['FileName'] ?></a> <?= $oFile['RecordCount'] ?> records</li>
<?php
				}
				else {
?>
				<li>Email Template sent <?= $oFile['RecordCount'] ?> records (<?= $oFile['FailedCount'] ?> failed) </li>
<?php
				}
			} 
?>
			</ul>
			<input type="button" onclick="window.location='proof.php'" value="Try Again" />&nbsp;&nbsp;&nbsp;<input name="Cancel" type="button" value="Close" onclick="window.close();"/>
<?php
		}
	}
	else
	{
?>
			<div class="sectionContent">
	    		Use the form below to upload a CSV file with your proof database information.<br/>
	    		
	    		<a href="<?= $_SERVER['PHP_SELF']?>?template=true">Or click here to download a template.</a>
	    	</div>
	    	&nbsp;<br/>
	    	Proof File: <input type="file" name="uploadedfile"/><br/>
	    	&nbsp;<br/>
	    	<input name="Submit" type="submit" value="Upload"/>&nbsp;&nbsp;&nbsp;<input name="Cancel" type="button" value="Cancel" onclick="window.close();"/>
<?php
	}
?>
		</div>
	</div>
	</form>
</body>
</html>