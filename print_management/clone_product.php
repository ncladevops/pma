<?php
	require("../../printable/include/mysql.inc.php");
	require("../../printable/include/printable.inc.php");
	require("../globals.php");
	
	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	
	$portal = new PrintablePortal($COMPANY_ID, $db);
		
	$currentUser = $portal->GetUser($_SESSION['currentuserid']);
		
	// Check login
	if(!$portal->CheckPriv($currentUser->UserID, 'subadmin'))
	{
		header( "Location: " . $portal->CurrentCompany->Website . "login.php?message=" . urlencode( "Not logged in or login error.  Please try again." ) );
		die();
	}
		
	$products = $portal->GetProducts();
	
		
	if($_POST['Submit'] == 'Cancel')
	{
		header("Location: manage_product.php?message=". urlencode("Action Canceled. Product not Cloned."));
		die();
	}
	elseif($_POST['Submit'] == 'Clone')
	{
		$prodID = $portal->CloneProduct($_POST['ProductName'], $_POST['CloneID']);
		
		header("Location: edit_product.php?id={$prodID}message=". urlencode("Action Completed. Product Cloned."));
		die();
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
	<title>
		<?= $portal->CurrentCompany->CompanyName ?> :: Clone Product
	</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />		
	<link type="text/css" media="all" rel="stylesheet" href="../style.css" />		
	<link type="text/css" media="all" rel="stylesheet" href="style.css" />	
	<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />
</head>
<body bgcolor="#FFFFFF">
<?php
	include("header.php");
	include("navbar.php");
?>
<table cellspacing="0" cellpadding="0" width="770" align="center" bgcolor="#FFFFFF">
	<tr>
  		<td>
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td>
					<?php
						$CURRENTPAGE = 'productmanage';
					?>
					</td>
				</tr>
			</table>
			<table cellpadding="0" width="700">
				<tr>
	  				<td>
	  					<form method="post" action="<?= $_SERVER['PHP_SELF'] ?>" enctype="multipart/form-data">
	  					<div align="center" class="headerTextLarge">Clone Print Product</div>
	  					<div align="center" class="headerTextLarge"><?= $_GET['message'] ?></div>
			  			<div class="fieldSection">
			  				<div class="itemSection">
			  					<label>New Product Name:</label><br/>
			  					<input type="text" name="ProductName" value="<?= $product->ProductName; ?>"/>
			  				</div>
			  				<div class="itemSection">
			  					<label>Product to Clone:</label><br/>
			  					<select name="CloneID">
<?php
	foreach($products as $p)
	{
		if($p->GroupID == 0)
		{
?>
									<option value="<?= $p->ProductID ?>">
										<?= $p->ProductName ?>
									</option>
<?php
		}
	}
?>
			  					</select><br/>
			  				</div>
			  				<div class="itemSection">
			  					<center>
			  						<input type="submit" value="Clone" name="Submit"/><input type="submit" value="Cancel" name="Submit"/>
			  					</center>
			  				</div>
			  			</div>
			  			</form>
	  				</td>
         		</tr>
         	</table>
		</td>
	</tr>
</table>
</body>
</html>