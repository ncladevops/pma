<script language="JavaScript" type="text/JavaScript">
	var reminder_transactionURL = "printable_trans.php";
	var reminderRequest;

	function checkPendingReminder()
	{	
		reminderRequest=GetXmlHttpObject();
		if (reminderRequest==null)
		{
			alert ("Your browser does not support AJAX!");
			return;
		} 
		var url=reminder_transactionURL;
		url=url+"?action=CheckReminder";
		url=url+"&sid="+Math.random();
		reminderRequest.onreadystatechange=reminderRequestStateChanged;
		reminderRequest.open("GET",url,true);
		reminderRequest.send(null);
	}

	function reminderRequestStateChanged()
	{
		if (reminderRequest.readyState==4)
		{
			var xmlDoc=reminderRequest.responseXML.documentElement;
			
			// Check if session timedout			
			if(xmlDoc.getElementsByTagName("Status")[0].childNodes[0].nodeValue == 'Timeout')
			{
				parent.location="login.php?logout=true&message='Your+Session+has+timedout.+Please+Login+Again.";
				return;
			}
			if(xmlDoc.getElementsByTagName("Status")[0].childNodes[0].nodeValue == 'Fail')
			{
				alert("Unable to change Availability");
			}

			if(xmlDoc.getElementsByTagName("Status")[0].childNodes[0].nodeValue == 'PendingContactAppointment')
			{
				var caID = xmlDoc.getElementsByTagName("ContactAppointmentID")[0].childNodes[0].nodeValue;
				wopen('pop_appointment.php?id=' + caID, 'pop_appointment', 350, 415);
			}

			setTimeout("checkPendingReminder();",60000);
		}
		
	}

	setTimeout("checkPendingReminder();",2000);
</script>