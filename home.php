<?php
require("../printable/include/mysql.inc.php");
require("../printable/include/optimize.printable.inc.php");
require("globals.php");
include("components/class.myatomparser.php");
require '../printable/include/component/twitter/codebird.php';
require("../printable/include/component/rss_php.php");

$MAX_DASHBOARDS = 5;

$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);

$portal = new OptimizePortal($COMPANY_ID, $db);

$currentUser = $portal->UserAccess($_SESSION['currentuserid']);
$isSuper = $portal->CheckPriv($currentUser->UserID, 'supervisor');

if (!$currentUser) {
    header("Location: login.php?message=" . urlencode("Not logged in or login error."));
    die();
}

$dashboard = $portal->GetDashboard($HOMEPAGE_DBID);
$reports = $portal->GetDashBoardReports($dashboard->DashboardID);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title>
        <?= $portal->CurrentCompany->CompanyName ?> :: Welcome
    </title>
    <meta http-equiv="content-type" content="text/html;charset=utf-8"/>
    <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico"/>

    <link rel="stylesheet" href="slider/default.css" type="text/css" media="screen"/>
    <script src="js/swfobject.js"></script>

    <script src="js/func.js"></script>

    <script type='text/javascript' src='http://yui.yahooapis.com/2.9.0/build/utilities/utilities.js'></script>
    <link rel="stylesheet" href="../printable/include/js/jquery-ui/smoothness/jquery-ui.min.css"/>
    <script type='text/javascript' src='../printable/include/js/jquery-1.10.2.min.js'></script>
    <script type='text/javascript' src='../printable/include/js/jquery-ui.js'></script>

    <?php include("reports/report_dashboard_includes.php") ?>

    <script type='text/javascript' src='../printable/include/js/jquery-1.10.2.min.js'></script>
    <script type='text/javascript' src='../printable/include/js/jquery-ui.js'></script>
    <script type="text/javascript">

        var dashboard_transactionURL = "dashboard_trans.php";
        var appointmentTableRequest;
        var appointmentTableSimpleRequest;

        var report_loaderURL = "reports/load_report.php";
        var reportRequest = new Array();

        function LoadDashboards() {
            LoadAppointmentTableSimple();
            LoadCalendar();
        }

        function LoadCalendar() {
            document.getElementById("CalendarTable_Dashboard").innerHTML = "<iframe src='appt_calendar_sim.php' style='border: 0px; width: 100%; height: 300px;' scrolling='no'></iframe>";
        }

        function LoadAppointmentTableSimple() {
            document.getElementById("AppointmentTableSimple_Dashboard").innerHTML = "<img src='images/loading.gif' />"

            appointmentTableSimpleRequest = GetXmlHttpObject();
            if (appointmentTableSimpleRequest == null) {
                alert("Your browser does not support AJAX!");
                return;
            }
            var url = dashboard_transactionURL;
            url = url + "?dashboard=AppointmentTableSimp";
            url = url + "&sid=" + Math.random();
            appointmentTableSimpleRequest.onreadystatechange = appointmentTableSimpleRequestStateChanged;
            appointmentTableSimpleRequest.open("GET", url, true);
            appointmentTableSimpleRequest.send(null);
        }

        function appointmentTableSimpleRequestStateChanged() {
            if (appointmentTableSimpleRequest.readyState == 4) {
                document.getElementById("AppointmentTableSimple_Dashboard").innerHTML = appointmentTableSimpleRequest.responseText;
            }
        }


    </script>

    <?php include("components/bootstrap.php") ?>


</head>
<body bgcolor="#FFFFFF" onload="LoadDashboards();">
<?php include("components/header.php") ?>
<?php
$CURRENT_PAGE = "Home";
include("components/navbar.php");
?>


<?php if (isset($_GET['message'])): ?>
    <div class="container">
        <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?= $_GET['message']; ?>
        </div>
    </div>
<?php endif; ?>

<div id="page_content" class="row">

    <div id="left_panel" class="col-md-3">

        <div class="panel panel-default">
            <div class="panel-heading">

                <h3 class="panel-title">
                    <i class="glyphicon glyphicon-calendar"></i> My Calendar
                            <span class="pull-right">
                            <a id="todayCalendarDiv" class="calendarLinkDiv btn btn-default btn-xs"
                               onclick="wopen('appt_calendar.php?defaultView=agendaDay', 'appt_calendar', 800, 600);"
                               data-toggle="tooltip" title="Day">
                                <img src="images/glyphicons_day.png" height="18" width="18"/>
                            </a>
                            <a id="weekCalendarDiv" class="calendarLinkDiv btn btn-default btn-xs"
                               onclick="wopen('appt_calendar.php?defaultView=agendaWeek', 'appt_calendar', 800, 600);"
                               data-toggle="tooltip" title="Week">
                                <img src="images/glyphicons_week.png" height="18" width="18"/>
                            </a>
                            <a id="tomorrowCalendarDiv" class="calendarLinkDiv btn btn-default btn-xs"
                               onclick="wopen('appt_calendar.php?defaultView=agendaMonth', 'appt_calendar', 800, 600);"
                               data-toggle="tooltip" title="Month">
                                <img src="images/glyphicons_month.png" height="18" width="18"/>
                            </a>
                                </span>
                </h3>
            </div>
            <div id="CalendarTable_Dashboard" class="panel-body">
                <img class="alt-spinner" src="images/loading.gif"/>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?= $portal->GetCustomMessage('BrandNewsHeader', false, $currentUser->GroupID)->Message; ?>
                </h3>
            </div>
            <div class="home-feed panel-body">
                <?php if (isset($optifinowGlobalSetting['feature']['MortgageRSS']) && $optifinowGlobalSetting['feature']['MortgageRSS'] == TRUE): ?>

                    <?php
                    /* New RSS from GDR */

                    $rss = new rss_php;
                    $rss->load('http://www.mortgagedaily.com/rss.xml');
                    $items = $rss->getItems();
                    $amount = 10;
                    for ($x = 0; $x <= 10; $x++) {
                        echo '<strong>' . $items[$x]['title'] . '</strong><br>';
                        echo $items[$x]['description'];
                        echo '<div style="clear: both;"></div>';
                        echo '<hr style="margin: 0px;">';
                    }
                    ?>

                <?php else: ?>

                    <?php
                    Codebird::setConsumerKey($TWITTER_KEY, $TWITTER_SECRET);
                    Codebird::setBearerToken($TWITTER_BEARER);
                    $cb = Codebird::getInstance();
                    echo $cb->display_searched_tweets('OptifiNow');
                    ?>

                <?php endif; ?>
            </div>

        </div>
    </div>


    <div id="main_content" class="col-md-9">

        <div class="panel panel-default">

            <div class="panel-body">
                <ul class="nav nav-pills nav-justified">
                    <li><?= $portal->GetCustomMessage('QuickLink1', false, $currentUser->GroupID)->Message; ?></li>
                    <li><?= $portal->GetCustomMessage('QuickLink2', false, $currentUser->GroupID)->Message; ?></li>
                    <li><?= $portal->GetCustomMessage('QuickLink3', false, $currentUser->GroupID)->Message; ?></li>
                    <li><?= $portal->GetCustomMessage('QuickLink4', false, $currentUser->GroupID)->Message; ?></li>
                    <li><?= $portal->GetCustomMessage('QuickLink5', false, $currentUser->GroupID)->Message; ?></li>
                    <li><?= $portal->GetCustomMessage('QuickLink6', false, $currentUser->GroupID)->Message; ?></li>
                </ul>

            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <i class="glyphicon glyphicon-time"></i> My Appointments
                </h3>

            </div>
            <div id="AppointmentTableSimple_Dashboard" class="panel-body home-feed">
                <img class="alt-spinner" src="images/loading.gif"/>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?= $portal->GetCustomMessage('MessageBoardHeader', false, $currentUser->GroupID)->Message; ?>
                </h3>
            </div>
            <div class="panel-body">
                <?= $portal->GetCustomMessage('MessageBoard', false, $currentUser->GroupID)->Message; ?>
            </div>
        </div>

    </div>
</div>

<?php include("components/footer.php") ?>


</body>
</html>