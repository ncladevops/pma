<?php
require("../printable/include/mysql.inc.php");
require("../printable/include/optimize.printable.inc.php");
require("globals.php");

$time = microtime();
$time = explode(' ', $time);
$time = $time[1] + $time[0];
$start = $time;

$HIDE_STATUSES = array(30, 20, 37, 35);

$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);

$portal = new OptimizePortal($COMPANY_ID, $db);

$currentUser = $portal->GetUser($_SESSION['currentuserid']);
$isAdmin = $portal->CheckPriv($currentUser->UserID, 'admin');
$isCorpMan = $portal->CheckPriv($currentUser->UserID, 'corporatemanager');
$isRegMan = $portal->CheckPriv($currentUser->UserID, 'regionmanager');
$isDivMan = $portal->CheckPriv($currentUser->UserID, 'divisionmanager');
if ($isAdmin || $isCorpMan || $isRegMan || $isDivMan)
    $isMan = true;
else
    $isMan = false;

$filters = $portal->GetFilters();

$sortCols = null;

if (isset($COMPANY_DEFAULT_VIEW) && $_GET['view'] == '') {
    $_GET['view'] = $COMPANY_DEFAULT_VIEW;
}

if (isset($_GET['view'])) {
    //get table columns for selected view
    if (strpos($_GET['view'], 'filter') === false) {
        $sortCols = $LISTING_VIEWS[$_GET['view']]['columns'];
        $isFilterView = false;
    } else {
        $isFilterView = true;
        //get default Filter Columns from Global
        if ($_GET['view'] == 'filter20') {
            //Greenlink 2nd Voice Scheduled
            $sortCols = $filterCols;
        } else {
            //Default Filter Columns
            $sortCols = $contractCols;
        }
    }
}

//if sortCols not yet set, look for the default
if (!isset($sortCols)) {
//Sort Columns now on Global File
    foreach ($LISTING_VIEWS as $view) {
        //Look for the default
        if ($view['default']) {
            $sortCols = $view['columns'];
            $_GET['view'] = $view['url'];
        }
    }
}


$sortDirs = array('ASC', 'DESC');
$searchFields = array('ID' => 'submissionlisting.SubmissionListingID', 'Company' => 'submissionlisting.Company',
    'First Name' => 'submissionlisting.FirstName', 'Last Name' => 'submissionlisting.LastName',
    'Status' => 'ContactStatus', 'Campaign' => 'submissionlistingtype.ListingType',
    'State' => 'submissionlisting.State', 'Phone' => 'Phone',
    'Email' => 'Email', 'Mailer Code' => 'ExternalID', 'City' => 'submissionlisting.City', 'Zip' => 'Zip');
$dateFilters = array('1' => 'Today', '5' => 'Last 5 days', '10' => 'Last 10 days',
    '30' => 'Last 30 days', '60' => 'Last 60 days', '90' => 'Last 90 days');

if (sizeof($_GET) <= 0) {
    header("Location: " . $_SERVER['PHP_SELF'] . "?saved=1&" . $_SESSION['lastquery']);
    die();
} elseif (sizeof($_GET) == 1 && isset($_GET['message'])) {
    header("Location: " . $_SERVER['PHP_SELF'] . "?message=" . $_GET['message'] . "&" . $_SESSION['lastquery']);
    die();
}

if ($_GET['count'] == '') {
    $leadsPerPage = 25;
} else {
    $leadsPerPage = $_GET['count'];
}

$_GET['offset'] = intval($_GET['offset']) < 0 ? 0 : intval($_GET['offset']);

if (!$currentUser) {
    header("Location: login.php?message=" . urlencode("Not logged in or login error."));
    die();
}

//Change page's timezone for date call functions (for default start and end date range)
$originaltimezone = date('e');
if (!empty($currentUser->UserTimezoneOffset)) {
    date_default_timezone_set($currentUser->UserTimezoneOffset); //update page timezone with the user's preferred timezone
}

if (array_search($_GET['sortby'], $sortCols) === false) {
    $_GET['sortby'] = "DateAdded";
}
if (array_search($_GET['sortdir'], $sortDirs) === false) {
    $_GET['sortdir'] = "DESC";
}

if ($_GET['statusfilter'] != '') {
    $statusString = "(contactstatustype.ContactStatusTypeID LIKE '" . mysql_real_escape_string(stripslashes($_GET['statusfilter'])) . "%')";
} else {
    $statusString = "1";
}

if ($_GET['sltid'] != '') {
    $campString = "(SubmissionListingTypeID = '" . intval($_GET['sltid']) . "')";
} else {
    $campString = "1";
}

if ($_GET['actionfilter'] != '' && $portal->GetContactEventType($_GET['actionfilter'])) {
    $actionString = "(ContactEventTypeID = '" . mysql_real_escape_string(stripslashes($_GET['actionfilter'])) . "')";
} else {
    $actionString = "1";
}

if (!strtotime($_GET['start_range'])) {
    if ($COMPANY_ID == 44) {
        //PMA Site
        $_GET['start_range'] = date("Y-m-d H:i:s", strtotime("01/01/2014"));
    } else {
        //Default of 1 month
        $_GET['start_range'] = date("Y-m-d H:i:s", strtotime("-1 month"));
    }
}

if (!strtotime($_GET['end_range'])) {
    $_GET['end_range'] = date("Y-m-d H:i:s");
}

if (!empty($currentUser->UserTimezoneOffset)) {
    $date = new DateTime($_GET['start_range'], new DateTimeZone($currentUser->UserTimezoneOffset));  //get date / time
    $date->setTimezone(new DateTimeZone($originaltimezone)); //update date / time with the default timezone to check on the one saved on the db
    $startrange = $date->format("Y-m-d");

    $date = new DateTime($_GET['end_range'], new DateTimeZone($currentUser->UserTimezoneOffset));  //get date / time
    $date->setTimezone(new DateTimeZone($originaltimezone)); //update date / time with the default timezone to check on the one saved on the db
    $endrange = $date->format("Y-m-d");
} else {
    $startrange = date("Y-m-d", strtotime($_GET['start_range']));
    $endrange = date("Y-m-d", strtotime($_GET['end_range']));
}
$startWhere = "submissionlisting.DateAdded >= '" . $startrange . " 00:00:00'";

$endWhere = "submissionlisting.DateAdded <= '" . $endrange . " 23:59:59'";

if (!isset($searchFields[$_GET['searchfield']]) || strlen($_GET['search']) <= 0) {
    $whereString = "1";
} else {
    if ($_GET['searchfield'] == 'Phone') { //to include phone2 and workphone on phone queries
        $whereString = "(REPLACE(REPLACE(REPLACE(REPLACE(Phone,' ',''),'(',''),')',''),'-','') LIKE '%" . mysql_real_escape_string(stripslashes(preg_replace("/[^0-9]/", '', $_GET['search']))) . "%'
            OR REPLACE(REPLACE(REPLACE(REPLACE(Phone2,' ',''),'(',''),')',''),'-','') LIKE '%" . mysql_real_escape_string(stripslashes(preg_replace("/[^0-9]/", '', $_GET['search']))) . "%'
            OR REPLACE(REPLACE(REPLACE(REPLACE(WorkPhone,' ',''),'(',''),')',''),'-','') LIKE '%" . mysql_real_escape_string(stripslashes(preg_replace("/[^0-9]/", '', $_GET['search']))) . "%')";
    } else {
        $whereString = $searchFields[$_GET['searchfield']] . " LIKE '%" . mysql_real_escape_string(stripslashes($_GET['search'])) . "%'";
    }
    $startWhere = "1";
    $endWhere = "1";
}

$unassignedString = "((submissionlisting.ContactStatusTypeID = '$NEW_STATUSTYPE_ID' OR submissionlisting.ContactStatusTypeID = '$TRANSFER_STATUSTYPE_ID')
								AND (submissionlistingtype.RegionID = 0 OR submissionlistingtype.RegionID is null OR submissionlistingtype.DivisionID = '$currentUser->DivisionID'
									OR (submissionlistingtype.RegionID = '$currentUser->RegionID' AND submissionlistingtype.DivisionID = 0)))";
if ($isMan) {
    if ($_GET['userfilter'] != '') {
        $userString = "(submissionlisting.UserID = '" . intval($_GET['userfilter']) . "')";
    } else {
        if ($isCorpMan)
            $userString = "1";
        else if ($isRegMan)
            $userString = "(user.RegionID = '" . $currentUser->RegionID . "' OR $unassignedString)";
        else
            $userString = "(user.DivisionID = '" . $currentUser->DivisionID . "' OR $unassignedString)";
    }
    $statusHide = "1";
} else {
    $userString = "(submissionlisting.UserID = '$currentUser->UserID' OR $unassignedString)";
    $statusHide = "submissionlisting.ContactStatusTypeID NOT IN (" . implode(", ", $HIDE_STATUSES) . ") AND contactstatustype.ContactStatus NOT LIKE 'No Prod%' AND contactstatustype.ContactStatus NOT LIKE 'Not Interested%' ";
}

$userGroup = $portal->GetGroup($currentUser->GroupID);
$currentCampaign = $portal->GetSubmissionListingType($userGroup->DefaultSLT);

$hideLeadStore = "submissionlisting.UserID != " . intval($LEADSTORE_USERID) . " AND submissionlisting.ContactStatusTypeID != " . intval($LEADSTORE_STATUSID);

$hideDefCampString = "submissionlisting.ListingType != '" . intval($currentCampaign->SubmissionListingTypeID) . "'";

//Check if the view is filter view
if ($isFilterView) {
    //load filter views
    $leads = $portal->GetFilteredOptimizeContacts($currentUser->UserID, str_replace('filter', '', $_GET['view']), $leadsPerPage, $_GET['offset'], "$hideLeadStore AND $whereString");
    $leadcount = $portal->CountFilteredOptimizeContacts($currentUser->UserID, str_replace('filter', '', $_GET['view']), "$hideLeadStore AND $whereString");
} else {
    //default contact loading
    $leads = $portal->GetOptimizeContacts('all', 'all', " $hideLeadStore AND $userString AND $whereString AND $statusString AND $actionString AND $campString AND $startWhere AND $endWhere AND $hideDefCampString", true, $_GET['sortby'] . " " . $_GET['sortdir'], $leadsPerPage, $_GET['offset']);
    $leadcount = $portal->CountOptimizeContacts('all', 'all', "$hideLeadStore AND $userString AND $whereString AND $statusString AND $actionString AND $campString AND $startWhere AND $endWhere AND $hideDefCampString");
}

$csts = $portal->GetContactStatusTypes('all', 1, true);
$contactStatusesTypes = array();
$shortStatuses = array();
$shortStatusDetails = array();
foreach ($csts as $cst) {
    if (!$cst->Disabled) {
        $shortStatuses[$cst->ShortName] = (strlen($cst->DetailName) > 0 ? 1 : 0);
        $shortStatusDetails[$cst->ShortName][] = $cst->ContactStatus;
        $contactStatusesTypes[$cst->ContactStatus] = $cst;
    }
}
$contactEventTypes = $portal->GetContactEventTypes();
$users = $portal->GetCompanyUsers(1, $orderby = "FirstName");
$camps = $portal->GetSubmissionListingTypes(0, ' ListingType', 'all', "(((submissionlistingtype.UserManageable = 1 AND submissionlistingtype.SubmissionListingSourceID = 9999) || submissionlistingtype.UserManageable = 0) && disabled = 0)");
$cetLookup = array();
foreach ($contactEventTypes as $cet) {
    $cetLookup[$cet->ContactEventTypeID] = $cet->EventType;
}

$tempGet = $_GET;
unset($tempGet['message']);
unset($tempGet['saved']);
$_SESSION['lastquery'] = build_get_query($tempGet);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title>
        <?= $portal->CurrentCompany->CompanyName ?> :: My Leads
    </title>
    <meta http-equiv="content-type" content="text/html;charset=utf-8"/>
    <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico"/>

    <script src="js/func.js"></script>
    <script src="js/contact_status.js"></script>

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>

    <script language="JavaScript" type="text/JavaScript">
        function updateContactEvent(id, actionName) {
            document.getElementById("cetid_" + id).innerHTML = actionName;
        }

        function appointmentPopup(id) {
            wopen('add_appointment.php?id=' + id, 'add_appointment', 350, 550);
            return false;
        }

        function toggleFilter() {
            if (document.getElementById("filter_div").style.display == "none") {
                document.getElementById("filter_div").style.display = "";
                document.getElementById("search_div").style.display = "none";
            }
            else {
                document.getElementById("filter_div").style.display = "none";
                document.getElementById("search_div").style.display = "none";
            }
        }

        function toggleSearch() {
            if (document.getElementById("search_div").style.display == "none") {
                document.getElementById("search_div").style.display = "";
                document.getElementById("filter_div").style.display = "none";
            }
            else {
                document.getElementById("search_div").style.display = "none";
                document.getElementById("filter_div").style.display = "none";
            }
        }

        var statusDetails = new Array();
        <?php
        foreach ($shortStatusDetails as $shortStat => $details) {
            ?>
        statusDetails["<?= $shortStat ?>"] = new Array();
        <?php
        foreach ($details as $det) {
            ?>
        statusDetails["<?= $shortStat ?>"][<?= intval($contactStatusesTypes[$det]->ContactStatusTypeID) ?>] = "<?= $contactStatusesTypes[$det]->DetailName ?>";
        <?php
    }
}
?>
        function showDetailedContactStatus(slid, cstid) {
            document.getElementById("detailContactID").value = slid;
            var statusSelect = document.getElementById("detailStatusID");
            document.getElementById("shortStatus").innerText = cstid;

            var i = 0;
            statusSelect.options.length = i;
            for (var id in statusDetails[cstid]) {
                statusSelect.options.length++;
                statusSelect[i].value = id;
                statusSelect[i].text = statusDetails[cstid][id];
                i++;
            }
            document.getElementById("detailDiv").style.display = "";
        }
        function submitDetailedContactStatus() {
            updateContactStatus(document.getElementById("detailContactID").value, document.getElementById("detailStatusID").value);
            document.getElementById("detailDiv").style.display = "none";
        }

        var rowCount = <?= intval(sizeof($leads)) ?>;

        function CloneSelected() {
            var idArray = new Array();
            for (i = 0; i < rowCount; i++) {
                var checkbox = document.getElementById("checkbox_" + i)
                if (checkbox && checkbox.checked) {
                    idArray.push(checkbox.value);
                }
            }

            if (idArray.length != 1) {
                alert("Please Select one record to clone.");
            }
            else {
                window.open('add_lead.php?clone=' + idArray[0], 'edit_lead', '_blank');
            }
        }

    </script>

    <?php include("components/bootstrap.php") ?>

</head>
<body bgcolor="#FFFFFF" onload="timedRefresh(120);
            checkPendingReminder()">
<?php include("components/header.php") ?>
<div id="body">
    <?php
    $CURRENT_PAGE = "Leads";
    include("components/navbar.php");
    ?>
    <div class="row">
        <div class="col-md-6 pull-left">
            <ul class="nav nav-pills hidden-xs">
                <li><a href="add_lead.php" class="actionButton">
                        <i class="glyphicon glyphicon-plus"></i> Add Lead
                    </a></li>
                <?php if (!$isFilterView): ?>
                    <li><a href="#" class="actionButton" onclick="toggleFilter();
                            return false;">
                            <i class="glyphicon glyphicon-filter"></i> Filter Lead
                        </a>
                    </li>
                <?php endif; ?>
                <li><a href="#" class="actionButton" onclick="toggleSearch();
                            return false;">
                        <i class="glyphicon glyphicon-search"></i> Search Lead
                    </a></li>
                <li><a href="#" class="actionButtonRight" onclick="window.location = window.location;
                            return false;">
                        <i class="glyphicon glyphicon-refresh"></i> Refresh
                    </a></li>
                <?php if (isset($optifinowGlobalSetting['custom']['Clone']) && $optifinowGlobalSetting['custom']['Clone'] == TRUE): ?>
                    <li><a href="#" onclick="CloneSelected();
                            return false;">
                            <i class="glyphicon glyphicon-share-alt"></i> Clone
                        </a></li>
                <?php endif; ?>
                <?php if (isset($optifinowGlobalSetting['feature']['LeadStore']) && $optifinowGlobalSetting['feature']['LeadStore'] == TRUE): ?>
                    <li><a href="lead_store.php">
                            <i class="glyphicon glyphicon-shopping-cart"></i> Lead Store
                        </a></li>
                <?php endif; ?>
            </ul>
        </div>

        <div class="col-md-6 pull-right">
            <form method="get" action="<?= $_SERVER['PHP_SELF'] ?>" class="form-inline">
                <div class="form-group pull-right">
                    <label for="view">View:</label>
                    <select name="view" onchange="this.form.submit();" class="form-control input-sm">
                        <?php foreach ($LISTING_VIEWS as $view): ?>
                            <?php if ($view['enabled']): ?>
                                <?php if (!$view['manager-only'] || ($view['manager-only'] && $isMan)): ?>
                                    <option
                                        value="<?= $view['url'] ?>" <?= $_GET['view'] == $view['url'] ? "SELECTED" : "" ?>><?= $view['label'] ?></option>
                                <?php endif; ?>
                            <?php endif; ?>
                        <?php endforeach; ?>
                        <? foreach ($filters as $f): ?>
                            <option
                                value="filter<?= $f->FilterID ?>" <?= $_GET['view'] == 'filter' . $f->FilterID ? "SELECTED" : "" ?>><?= $f->FilterName ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </form>
        </div>

    </div>

    <?php if (isset($_GET['message'])): ?>
        <div class="container">
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?= $_GET['message']; ?>
            </div>
        </div>
    <?php endif; ?>

    <?php
    $getTemp = $_GET;
    unset($getTemp['search']);
    unset($getTemp['searchfield']);
    unset($getTemp['statusfilter']);
    unset($getTemp['sltid']);
    unset($getTemp['actionfilter']);
    unset($getTemp['start_range']);
    unset($getTemp['end_range']);
    unset($getTemp['userfilter']);
    unset($getTemp['count']);
    $getString = build_get_query($getTemp);
    ?>
    <div class="row">
        <div id="filter_div" style="<?= $_GET['isFilter'] == 'true' ? "" : "display: none;" ?>" class="col-md-12">
            <form method="get" class="form-inline" role="form" action="<?= $_SERVER['PHP_SELF'] . "?" . $getString ?>>">
                <div class="form-group col-md-2">
                    <label for="statusfilter">Status:</label>
                    <select name="statusfilter" onchange="this.form.submit();" class="form-control input-sm">
                        <option value="">- All -</option>
                        <?php
                        foreach ($contactStatusesTypes as $statusName => $status) {
                            ?>
                            <option
                                value="<?= $status->ContactStatusTypeID ?>" <?= $status->ContactStatusTypeID == $_GET['statusfilter'] ? "SELECTED" : "" ?>>
                                <?= $statusName ?>
                            </option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
                <div class="form-group col-md-2">
                    <label for="actionfilter">Contact Event:</label>
                    <select name="actionfilter" onchange="this.form.submit();" class="form-control input-sm">
                        <option value="">- All -</option>
                        <?php
                        foreach ($contactEventTypes as $eType) {
                            ?>
                            <option
                                value="<?= $eType->ContactEventTypeID ?>" <?= $eType->ContactEventTypeID == $_GET['actionfilter'] ? "SELECTED" : "" ?>>
                                <?= $eType->EventType ?>
                            </option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
                <?php
                if ($isMan) {
                    ?>
                    <div class="form-group col-md-2">
                        <label for="userfilter">User:</label>
                        <select name="userfilter" onchange="this.form.submit();" class="form-control input-sm">
                            <option value="">- All -</option>
                            <?php
                            foreach ($users as $u) {
                                if ($isCorpMan || ($isRegMan && $u->RegionID == $currentUser->RegionID) || ($isDivMan && $u->DivisionID == $currentUser->DivisionID)) {
                                    ?>
                                    <option
                                        value="<?= $u->UserID ?>" <?= $u->UserID == $_GET['userfilter'] ? "SELECTED" : "" ?>>
                                        <?= $u->FirstName . " " . $u->LastName ?>
                                    </option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                    <?php
                }
                ?>
                <div class="form-group col-md-2">
                    <label for="sltid">Campaign:</label>
                    <select name="sltid" id="sltid" class="form-control input-sm">
                        <option value="">-- All Lead Sources --</option>
                        <?php
                        foreach ($camps as $c) {
                            ?>
                            <option
                                value="<?= $c->SubmissionListingTypeID ?>" <?= $_GET['sltid'] == $c->SubmissionListingTypeID ? "SELECTED" : "" ?>><?= $c->ListingDescription ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>

                <div class="form-group col-md-1">
                    <label for="start_range">From:</label> <input name="start_range" id="start_range" type="text"
                                                                  class="datepicker form-control input-sm"
                                                                  value="<?= strtotime($_GET['start_range']) ? date("m/d/Y", strtotime($_GET['start_range'])) : "" ?>"
                                                                  onchange="this.form.submit();"/>
                </div>

                <div class="form-group col-md-1">
                    <label for="end_range">To:</label> <input name="end_range" id="end_range" type="text"
                                                              class="datepicker form-control input-sm"
                                                              value="<?= strtotime($_GET['end_range']) ? date("m/d/Y", strtotime($_GET['end_range'])) : "" ?>"
                                                              onchange="this.form.submit();"/>
                </div>

                <div class="form-group col-md-offset-1 col-md-1">
                    <input type="submit" name="Submit" class="form-control btn btn-info btn-sm" value="Go"/>
                    <a href="<?= $_SERVER['PHP_SELF'] ?>?saved=1">clear</a>
                    <input type="hidden" name="isFilter" value="true"/>
                    <input type="hidden" name="view" value="<?= $_GET['view'] ?>"/>
                    <input type="hidden" name="count" value="<?= $_GET['count'] ?>"/>
                </div>
            </form>
        </div>
    </div>

    <div class="row">
        <div id="search_div" style="<?= $_GET['isSearch'] == 'true' ? "" : "display: none;" ?>">
            <form method="get" class="form-inline" role="form" action="<?= $_SERVER['PHP_SELF'] ?>">

                <div class="form-group col-md-2">
                    <label for="search">Search:</label>
                    <input type="search" name="search" class="form-control input-sm" value="<?= $_GET['search'] ?>"
                           placeholder="Search"/>
                </div>
                <div class="form-group col-md-1 chosen-inline">
                    <label>in field: &nbsp;</label>
                    <select name="searchfield" class="form-control input-sm">
                        <?php
                        foreach ($searchFields as $k => $v) {
                            ?>
                            <option value="<?= $k ?>" <?= $k == $_GET['searchfield'] ? "SELECTED" : "" ?>>
                                <?= $k ?>
                            </option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
                <div class="form-group col-md-2 col-md-offset-1">
                    <input type="submit" name="Submit" value="Search" class="form-control btn btn-info btn-sm"/>
                    <input type="hidden" name="isSearch" value="true"/>
                    <input type="hidden" name="view" value="<?= $_GET['view'] ?>"/>
                    <input type="hidden" name="count" value="<?= $_GET['count'] ?>"/>
                    <a href="<?= $_SERVER['PHP_SELF'] ?>?saved=1">clear</a>
                </div>
            </form>
        </div>
    </div>

    <!-- table -->
    <div class="row">
        <div id="LeadDiv" class="table-responsive col-md-12">
            <table class="table table-striped table-hover table-bordered">
                <thead>
                <tr class="lead_table rowheader" id="HeaderRow">
                    <?php
                    foreach ($sortCols as $k => $v) {
                        if ($v != "nosort") {
                            $getTemp = $_GET;
                            $getTemp['sortby'] = $v;
                            $getTemp['sortdir'] = 'DESC';
                            $getDownString = build_get_query($getTemp);
                            $getTemp['sortdir'] = 'ASC';
                            $getUpString = build_get_query($getTemp);
                            ?>
                            <th class="lead_table">
                                <?= $k ?>
                                <?php
                                if ($v != $_GET['sortby'] || $_GET['sortdir'] != 'ASC') {
                                    ?>
                                    <a href="<?= $_SERVER['PHP_SELF'] . "?" . $getUpString ?>"><span
                                            class="small-glyphicon glyphicon glyphicon-chevron-up"></span></a>
                                    <?php
                                }
                                if ($v != $_GET['sortby'] || $_GET['sortdir'] != 'DESC') {
                                    ?>
                                    <a href="<?= $_SERVER['PHP_SELF'] . "?" . $getDownString ?>"><span
                                            class="small-glyphicon glyphicon glyphicon-chevron-down"></span></a>
                                    <?php
                                }
                                ?>
                            </th>
                            <?php
                        } else {
                            ?>
                            <th class="lead_table"><?= $k ?></th>
                            <?php
                        }
                    }
                    ?>
                </tr>
                </thead>
                <tbody>
                <?php
                $lead_step_ids = array();
                if (is_array($leads)) {
                    $i = 0;
                    foreach ($leads as $l) {
                        $i++;
                        array_push($lead_step_ids, $l->SubmissionListingID);
                        $contactHistory = $portal->GetContactHistory($l->SubmissionListingID);
                        $lastCH = $contactHistory[sizeof($contactHistory) - 1];
                        $appt = $portal->GetOpenAppointment($l->SubmissionListingID, 'all');
                        $apptLink = $appt ? "view_appointment.php?contactID={$l->SubmissionListingID}" : "add_appointment.php?id={$l->SubmissionListingID}";
                        ($appt && $appt->PastDue) ? $pastdue = 'color: red;' : $pastdue = '';
                        if ($_GET['view'] == 'bottle') {
                            ?>
                            <tr class="lead_table<?= $i % 2 == 0 ? ' roweven' : ' rowodd' ?>">
                                <td class="lead_table" align="right">
                                    <?php
                                    //if ($isMan) {
                                    ?>
                                    <input type="checkbox" value="<?= $l->SubmissionListingID ?>"
                                           id="checkbox_<?= $i ?>"/>
                                    <?php
                                    //}
                                    ?>
                                </td>
                                <td class="lead_table" align="right">
                                    <?= $l->SubmissionListingID ?>
                                </td>

                                <?php
                                $dadded = new DateTime($l->DateAdded, new DateTimeZone($originaltimezone));  //get date / time base on string saved and server's timezone
                                $dtouched = new DateTime($lastCH->EventDate, new DateTimeZone($originaltimezone));  //get date / time base on string saved and server's timezone
                                if (!empty($currentUser->UserTimezoneOffset)) {
                                    $dadded->setTimezone(new DateTimeZone($currentUser->UserTimezoneOffset)); //update date / time with the user's preferred timezone (for UserTimezoneOffset)
                                    $dtouched->setTimezone(new DateTimeZone($currentUser->UserTimezoneOffset)); //update date / time with the user's preferred timezone (for UserTimezoneOffset)
                                }

                                //Don't convert if it's empty / null
                                if (strtotime($l->DateAssigned)) {
                                    $dassigned = new DateTime($l->DateAssigned, new DateTimeZone($originaltimezone));  //get date / time base on string saved and server's timezone
                                    $dassigned->setTimezone(new DateTimeZone($currentUser->UserTimezoneOffset)); //update date / time with the user's preferred timezone (for UserTimezoneOffset)
                                    $l->DateAssigned = $dassigned->format("m/d/Y g:i:s a");
                                }

                                $l->DateAdded = $dadded->format("m/d/Y g:i:s a");
                                $lastCH->EventDate = $dtouched->format("m/d/Y g:i:s a");
                                ?>

                                <td class="lead_table" align="right">
                                    <?= strtotime($l->DateAdded) ? date("m/d/Y g:i a", strtotime($l->DateAdded)) : "" ?>
                                </td>
                                <?php if ($COMPANY_ID == 46):// Greenlink ?>
                                    <td class="lead_table" align="right">
                                        <?= strtotime($l->DateAssigned) ? date("m/d/Y g:i a", strtotime($l->DateAssigned)) : "" ?>
                                    </td>
                                <?php endif; ?>
                                <td class="lead_table">
                                    <a href="edit_lead.php?id=<?= $l->SubmissionListingID ?>" title="Edit Lead"
                                       data-toggle="tooltip">
                                        <?= $l->FirstName ?>
                                    </a>
                                </td>
                                <td class="lead_table">
                                    <a href="edit_lead.php?id=<?= $l->SubmissionListingID ?>" title="Edit Lead"
                                       data-toggle="tooltip">
                                        <?= $l->LastName ?>
                                    </a>
                                </td>
                                <td class="lead_table">
                                    <?= $l->State ?>
                                </td>
                                <td class="lead_table">
                                    <?= $l->UserFullname ?>
                                </td>
                                <td class="lead_table">

                                    <?php if (isset($optifinowGlobalSetting['custom']['disabledStatus']) && $optifinowGlobalSetting['custom']['disabledStatus'] == TRUE): ?>
                                        <?php foreach ($contactStatusesTypes as $statusName => $status): ?>
                                            <?php if ($status->ContactStatusTypeID == $l->ContactStatusTypeID): ?>
                                                <?= $statusName ?>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    <?php else: ?>
                                        <select name="cstid_<?= $l->SubmissionListingID ?>"
                                                id="cstid_<?= $l->SubmissionListingID ?>"
                                                onchange="updateContactStatus('<?= $l->SubmissionListingID ?>', this.value);"
                                                class="form-submenu input-sm contactStatus">
                                            <?php
                                            foreach ($contactStatusesTypes as $statusName => $status) {
                                                ?>
                                                <option
                                                    value="<?= $status->ContactStatusTypeID ?>" <?= $status->ContactStatusTypeID == $l->ContactStatusTypeID ? "SELECTED" : "" ?>>
                                                    <?= $statusName ?>
                                                </option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    <?php endif; ?>
                                </td>
                                <td class="lead_table" align="right">
                                    <?= strtotime($lastCH->EventDate) ? date("m/d/Y g:i a", strtotime($lastCH->EventDate)) : "" ?>
                                </td>
                                <td class="lead_table bluetext" align="right" style="white-space: nowrap;">
                                    N/A
                                </td>
                                <td class="lead_table bluetext" align="left" style="white-space: nowrap;">
                                    <?= $l->ListingTypeName; ?>
                                </td>
                                <td class="lead_table bluetext" align="right" style="white-space: nowrap;">
                                    <?= $l->Mortgage1Balance; ?>
                                </td>
                                <td class="lead_table" align="center">
                                    <a href="edit_lead.php?id=<?= $l->SubmissionListingID ?>" title="Edit Lead"
                                       data-toggle="tooltip"><i
                                            class="glyphicon glyphicon-edit"></i></a>
                                    <a href="mailto:<?= $l->Email; ?>" data-toggle="tooltip"
                                       title="Send Email"><i class="glyphicon glyphicon-envelope"></i></a>
                                    <?php if (isset($optifinowGlobalSetting['custom']['SMSOnDemand']) && $optifinowGlobalSetting['custom']['SMSOnDemand'] == TRUE): ?>
                                        <a href="#"
                                           onclick="wopen('send_sms.php?id=<?= $l->SubmissionListingID; ?>', 'send_sms', 400, 350);
                                               return false;" data-toggle="tooltip"
                                           title="Send SMS"><i class="glyphicon glyphicon-phone"></i></a>
                                    <?php endif; ?>
                                    <a href="#"
                                       onclick="wopen('<?= $apptLink; ?>', 'add_appointment', 350, 550);
                                           return false;" data-toggle="tooltip"
                                       title="<?= $appt ? "Edit" : "Add" ?> Reminder"><?= $appt ? '<i style="' . $pastdue . '" class="glyphicon glyphicon-calendar"></i>' : '<i class="glyphicon glyphicon-time"></i>' ?></a>
                                    <a href="#"
                                       onclick="wopen('pop_history.php?id=<?= $l->SubmissionListingID; ?>', 'pop_history', 850, 450);
                                           return false;"
                                       title="History" data-toggle="tooltip"><span
                                            class="glyphicon glyphicon-list-alt"></span></a>
                                </td>
                            </tr>
                            <?php
                        } //bottle view

                        else if ($_GET['view'] == 'sales') {
                            ?>
                            <tr class="lead_table<?= $i % 2 == 0 ? ' roweven' : ' rowodd' ?>">
                                <td class="lead_table" align="right">
                                    <?php
                                    //if ($isMan) {
                                    ?>
                                    <input type="checkbox" value="<?= $l->SubmissionListingID ?>"
                                           id="checkbox_<?= $i ?>"/>
                                    <?php
                                    //}
                                    ?>
                                </td>
                                <td class="lead_table" align="right">
                                    <?= $l->SubmissionListingID ?>
                                </td>
                                <?php
                                $dadded = new DateTime($l->DateAdded, new DateTimeZone($originaltimezone));  //get date / time base on string saved and server's timezone
                                $dtouched = new DateTime($lastCH->EventDate, new DateTimeZone($originaltimezone));  //get date / time base on string saved and server's timezone
                                if (!empty($currentUser->UserTimezoneOffset)) {
                                    $dadded->setTimezone(new DateTimeZone($currentUser->UserTimezoneOffset)); //update date / time with the user's preferred timezone (for UserTimezoneOffset)
                                    $dtouched->setTimezone(new DateTimeZone($currentUser->UserTimezoneOffset)); //update date / time with the user's preferred timezone (for UserTimezoneOffset)
                                }

                                //Don't convert if it's empty / null
                                if (strtotime($l->DateAssigned)) {
                                    $dassigned = new DateTime($l->DateAssigned, new DateTimeZone($originaltimezone));  //get date / time base on string saved and server's timezone
                                    $dassigned->setTimezone(new DateTimeZone($currentUser->UserTimezoneOffset)); //update date / time with the user's preferred timezone (for UserTimezoneOffset)
                                    $l->DateAssigned = $dassigned->format("m/d/Y g:i:s a");
                                }

                                $l->DateAdded = $dadded->format("m/d/Y g:i:s a");
                                $lastCH->EventDate = $dtouched->format("m/d/Y g:i:s a");
                                ?>

                                <td class="lead_table" align="right">
                                    <?= strtotime($l->DateAdded) ? date("m/d/Y g:i a", strtotime($l->DateAdded)) : "" ?>
                                </td>
                                <?php if ($COMPANY_ID == 46):// Greenlink ?>
                                    <td class="lead_table" align="right">
                                        <?= strtotime($l->DateAssigned) ? date("m/d/Y g:i a", strtotime($l->DateAssigned)) : "" ?>
                                    </td>
                                <?php endif; ?>
                                <td class="lead_table">
                                    <?= $l->Company ?>
                                </td>
                                <td class="lead_table">
                                    <a href="edit_lead.php?id=<?= $l->SubmissionListingID ?>" title="Edit Lead"
                                       data-toggle="tooltip">
                                        <?= $l->FirstName ?>
                                    </a>
                                </td>
                                <td class="lead_table">
                                    <a href="edit_lead.php?id=<?= $l->SubmissionListingID ?>" title="Edit Lead"
                                       data-toggle="tooltip">
                                        <?= $l->LastName ?>
                                    </a>
                                </td>
                                <td class="lead_table bluetext" align="right" style="white-space: nowrap;">
                                    <a href="#"
                                       onclick="wopen('trigger_call.php?id=<?= $l->SubmissionListingID; ?>&field=WorkPhone', 'add_action', 350, 285);
                                           return false;">
                                        <?= format_phone($l->WorkPhone) ?>
                                    </a>
                                </td>
                                <td class="lead_table bluetext" align="right" style="white-space: nowrap;">
                                    <a href="#"
                                       onclick="wopen('trigger_call.php?id=<?= $l->SubmissionListingID; ?>&field=Phone2', 'add_action', 350, 285);
                                           return false;">
                                        <?= format_phone($l->Phone2) ?>
                                    </a>
                                </td>
                                <td class="lead_table bluetext" align="right" style="white-space: nowrap;">
                                    <?= $l->State ?>
                                </td>
                                <td class="lead_table">
                                    <?php if (isset($optifinowGlobalSetting['custom']['disabledStatus']) && $optifinowGlobalSetting['custom']['disabledStatus'] == TRUE): ?>
                                        <?php foreach ($contactStatusesTypes as $statusName => $status): ?>
                                            <?php if ($status->ContactStatusTypeID == $l->ContactStatusTypeID): ?>
                                                <?= $statusName ?>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    <?php else: ?>
                                        <select name="cstid_<?= $l->SubmissionListingID ?>"
                                                id="cstid_<?= $l->SubmissionListingID ?>"
                                                onchange="updateContactStatus('<?= $l->SubmissionListingID ?>', this.value);"
                                                class="form-submenu input-sm contactStatus">
                                            <?php
                                            foreach ($contactStatusesTypes as $statusName => $status) {
                                                ?>
                                                <option
                                                    value="<?= $status->ContactStatusTypeID ?>" <?= $status->ContactStatusTypeID == $l->ContactStatusTypeID ? "SELECTED" : "" ?>>
                                                    <?= $statusName ?>
                                                </option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    <?php endif; ?>
                                </td>
                                <td class="lead_table" align="left">
                                    <a href="#"
                                       onclick="wopen('add_action.php?id=<?= $l->SubmissionListingID; ?>', 'add_action', 350, 550);
                                           return false;">
                                        <span class="glyphicon glyphicon-star" title="Add New"></span>
                                    </a>
                                    <span
                                        id="cetid_<?= $l->SubmissionListingID ?>"><?= $cetLookup[$l->LastContactEvent->ContactEventTypeID] == "" ? "" : $cetLookup[$l->LastContactEvent->ContactEventTypeID] ?></span>
                                </td>
                                <td class="lead_table">
                                    <?= strtotime($lastCH->EventDate) ? date("m/d/Y g:i a", strtotime($lastCH->EventDate)) : "" ?>
                                </td>
                                <td class="lead_table" align="center">
                                    <a href="edit_lead.php?id=<?= $l->SubmissionListingID ?>" title="Edit Lead"
                                       data-toggle="tooltip"><i
                                            class="glyphicon glyphicon-edit"></i></a>
                                    <a href="mailto:<?= $l->Email; ?>" data-toggle="tooltip"
                                       title="Send Email"><i class="glyphicon glyphicon-envelope"></i></a>
                                    <?php if (isset($optifinowGlobalSetting['custom']['SMSOnDemand']) && $optifinowGlobalSetting['custom']['SMSOnDemand'] == TRUE): ?>
                                        <a href="#"
                                           onclick="wopen('send_sms.php?id=<?= $l->SubmissionListingID; ?>', 'send_sms', 400, 350);
                                               return false;" data-toggle="tooltip"
                                           title="Send SMS"><i class="glyphicon glyphicon-phone"></i></a>
                                    <?php endif; ?>
                                    <a href="#"
                                       onclick="wopen('<?= $apptLink; ?>', 'add_appointment', 350, 550);
                                           return false;" data-toggle="tooltip"
                                       title="<?= $appt ? "Edit" : "Add" ?> Reminder"><?= $appt ? '<i style="' . $pastdue . '" class="glyphicon glyphicon-calendar"></i>' : '<i class="glyphicon glyphicon-time"></i>' ?></a>
                                    <a href="#"
                                       onclick="wopen('pop_history.php?id=<?= $l->SubmissionListingID; ?>', 'pop_history', 850, 450);
                                           return false;"
                                       title="History" data-toggle="tooltip"><span
                                            class="glyphicon glyphicon-list-alt"></span></a>
                                </td>
                            </tr>
                            <?php // sales view
                        } else if ($_GET['view'] == 'filter20') {
                            ?>
                            <tr class="lead_table<?= $i % 2 == 0 ? ' roweven' : ' rowodd' ?>">
                                <td class="lead_table" align="right">
                                    <?php
                                    //if ($isMan) {
                                    ?>
                                    <input type="checkbox" value="<?= $l->SubmissionListingID ?>"
                                           id="checkbox_<?= $i ?>"/>
                                    <?php
                                    //}
                                    ?>
                                </td>
                                <td class="lead_table" align="right">
                                    <?= $l->SubmissionListingID ?>
                                </td>
                                <?php
                                $dadded = new DateTime($l->DateAdded, new DateTimeZone($originaltimezone));  //get date / time base on string saved and server's timezone
                                $dtouched = new DateTime($lastCH->EventDate, new DateTimeZone($originaltimezone));  //get date / time base on string saved and server's timezone
                                if (!empty($currentUser->UserTimezoneOffset)) {
                                    $dadded->setTimezone(new DateTimeZone($currentUser->UserTimezoneOffset)); //update date / time with the user's preferred timezone (for UserTimezoneOffset)
                                    $dtouched->setTimezone(new DateTimeZone($currentUser->UserTimezoneOffset)); //update date / time with the user's preferred timezone (for UserTimezoneOffset)
                                }

                                //Don't convert if it's empty / null
                                if (strtotime($l->DateAssigned)) {
                                    $dassigned = new DateTime($l->DateAssigned, new DateTimeZone($originaltimezone));  //get date / time base on string saved and server's timezone
                                    $dassigned->setTimezone(new DateTimeZone($currentUser->UserTimezoneOffset)); //update date / time with the user's preferred timezone (for UserTimezoneOffset)
                                    $l->DateAssigned = $dassigned->format("m/d/Y g:i:s a");
                                }

                                $l->DateAdded = $dadded->format("m/d/Y g:i:s a");
                                $lastCH->EventDate = $dtouched->format("m/d/Y g:i:s a");
                                ?>

                                <td class="lead_table" align="right">
                                    <?= strtotime($l->DateAdded) ? date("m/d/Y g:i a", strtotime($l->DateAdded)) : "" ?>
                                </td>
                                <?php if ($COMPANY_ID == 46):// Greenlink ?>
                                    <td class="lead_table" align="right">
                                        <?= strtotime($l->DateAssigned) ? date("m/d/Y g:i a", strtotime($l->DateAssigned)) : "" ?>
                                    </td>
                                <?php endif; ?>
                                <td class="lead_table">
                                    <?= $l->ExternalID ?>
                                </td>
                                <td class="lead_table">
                                    <a href="edit_lead.php?id=<?= $l->SubmissionListingID ?>" title="Edit Lead"
                                       data-toggle="tooltip">
                                        <?= $l->FirstName ?>
                                    </a>
                                </td>
                                <td class="lead_table">
                                    <a href="edit_lead.php?id=<?= $l->SubmissionListingID ?>" title="Edit Lead"
                                       data-toggle="tooltip">
                                        <?= $l->LastName ?>
                                    </a>
                                </td>
                                <td class="lead_table bluetext" align="right" style="white-space: nowrap;">
                                    <a href="#"
                                       onclick="wopen('trigger_call.php?id=<?= $l->SubmissionListingID; ?>&field=Phone', 'add_action', 350, 285);
                                           return false;">
                                        <?= format_phone($l->Phone) ?>
                                    </a>
                                </td>
                                <td class="lead_table bluetext" align="right" style="white-space: nowrap;">
                                    <a href="#"
                                       onclick="wopen('trigger_call.php?id=<?= $l->SubmissionListingID; ?>&field=WorkPhone', 'add_action', 350, 285);
                                           return false;">
                                        <?= format_phone($l->WorkPhone) ?>
                                    </a>
                                </td>
                                <td class="lead_table bluetext" align="right" style="white-space: nowrap;">
                                    <a href="#"
                                       onclick="wopen('trigger_call.php?id=<?= $l->SubmissionListingID; ?>&field=Phone2', 'add_action', 350, 285);
                                           return false;">
                                        <?= format_phone($l->Phone2) ?>
                                    </a>
                                </td>
                                <td class="lead_table bluetext" align="right" style="white-space: nowrap;">
                                    <?= $l->State ?>
                                </td>
                                <td class="lead_table" align="left">
                                    <a href="#"
                                       onclick="wopen('add_action.php?id=<?= $l->SubmissionListingID; ?>', 'add_action', 350, 550);
                                           return false;">
                                        <span class="glyphicon glyphicon-star" title="Add New"></span>
                                    </a>
                                    <span
                                        id="cetid_<?= $l->SubmissionListingID ?>"><?= $cetLookup[$l->LastContactEvent->ContactEventTypeID] == "" ? "" : $cetLookup[$l->LastContactEvent->ContactEventTypeID] ?></span>
                                </td>
                                <td class="lead_table">
                                    <?php if (isset($optifinowGlobalSetting['custom']['disabledStatus']) && $optifinowGlobalSetting['custom']['disabledStatus'] == TRUE): ?>
                                        <?php foreach ($contactStatusesTypes as $statusName => $status): ?>
                                            <?php if ($status->ContactStatusTypeID == $l->ContactStatusTypeID): ?>
                                                <?= $statusName ?>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    <?php else: ?>
                                        <select name="cstid_<?= $l->SubmissionListingID ?>"
                                                id="cstid_<?= $l->SubmissionListingID ?>"
                                                onchange="updateContactStatus('<?= $l->SubmissionListingID ?>', this.value);"
                                                class="form-submenu input-sm contactStatus">
                                            <?php
                                            foreach ($contactStatusesTypes as $statusName => $status) {
                                                ?>
                                                <option
                                                    value="<?= $status->ContactStatusTypeID ?>" <?= $status->ContactStatusTypeID == $l->ContactStatusTypeID ? "SELECTED" : "" ?>>
                                                    <?= $statusName ?>
                                                </option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    <?php endif; ?>
                                </td>
                                <td class="lead_table">
                                    <?= $l->SecondVoiceManager ?>
                                </td>
                                <td class="lead_table">
                                    <?= $l->SecondVoiceQueue ?>
                                </td>
                                <td class="lead_table">
                                    <?= strtotime($lastCH->EventDate) ? date("m/d/Y g:i a", strtotime($lastCH->EventDate)) : "" ?>
                                </td>
                                <td class="lead_table" align="center">
                                    <a href="edit_lead.php?id=<?= $l->SubmissionListingID ?>" title="Edit Lead"
                                       data-toggle="tooltip"><i
                                            class="glyphicon glyphicon-edit"></i></a>
                                    <a href="mailto:<?= $l->Email; ?>" data-toggle="tooltip"
                                       title="Send Email"><i class="glyphicon glyphicon-envelope"></i></a>
                                    <?php if (isset($optifinowGlobalSetting['custom']['SMSOnDemand']) && $optifinowGlobalSetting['custom']['SMSOnDemand'] == TRUE): ?>
                                        <a href="#"
                                           onclick="wopen('send_sms.php?id=<?= $l->SubmissionListingID; ?>', 'send_sms', 400, 350);
                                               return false;" data-toggle="tooltip"
                                           title="Send SMS"><i class="glyphicon glyphicon-phone"></i></a>
                                    <?php endif; ?>
                                    <a href="#"
                                       onclick="wopen('<?= $apptLink; ?>', 'add_appointment', 350, 550);
                                           return false;" data-toggle="tooltip"
                                       title="<?= $appt ? "Edit" : "Add" ?> Reminder"><?= $appt ? '<i style="' . $pastdue . '" class="glyphicon glyphicon-calendar"></i>' : '<i class="glyphicon glyphicon-time"></i>' ?></a>
                                    <a href="#"
                                       onclick="wopen('pop_history.php?id=<?= $l->SubmissionListingID; ?>', 'pop_history', 850, 450);
                                           return false;"
                                       title="History" data-toggle="tooltip"><span
                                            class="glyphicon glyphicon-list-alt"></span></a>
                                </td>
                            </tr>
                            <?php
                        } //filter view

                        else {
                            ?>
                            <tr class="lead_table<?= $i % 2 == 0 ? ' roweven' : ' rowodd' ?>">
                                <td class="lead_table" align="right">
                                    <?php
                                    //if ($isMan) {
                                    ?>
                                    <input type="checkbox" value="<?= $l->SubmissionListingID ?>"
                                           id="checkbox_<?= $i ?>"/>
                                    <?php
                                    //}
                                    ?>
                                </td>
                                <td class="lead_table" align="right">
                                    <?= $l->SubmissionListingID ?>
                                </td>
                                <?php
                                $dadded = new DateTime($l->DateAdded, new DateTimeZone($originaltimezone));  //get date / time base on string saved and server's timezone
                                $dtouched = new DateTime($lastCH->EventDate, new DateTimeZone($originaltimezone));  //get date / time base on string saved and server's timezone
                                if (!empty($currentUser->UserTimezoneOffset)) {
                                    $dadded->setTimezone(new DateTimeZone($currentUser->UserTimezoneOffset)); //update date / time with the user's preferred timezone (for UserTimezoneOffset)
                                    $dtouched->setTimezone(new DateTimeZone($currentUser->UserTimezoneOffset)); //update date / time with the user's preferred timezone (for UserTimezoneOffset)
                                }

                                //Don't convert if it's empty / null
                                if (strtotime($l->DateAssigned)) {
                                    $dassigned = new DateTime($l->DateAssigned, new DateTimeZone($originaltimezone));  //get date / time base on string saved and server's timezone
                                    $dassigned->setTimezone(new DateTimeZone($currentUser->UserTimezoneOffset)); //update date / time with the user's preferred timezone (for UserTimezoneOffset)
                                    $l->DateAssigned = $dassigned->format("m/d/Y g:i:s a");
                                }

                                $l->DateAdded = $dadded->format("m/d/Y g:i:s a");
                                $lastCH->EventDate = $dtouched->format("m/d/Y g:i:s a");
                                ?>

                                <td class="lead_table" align="right">
                                    <?= strtotime($l->DateAdded) ? date("m/d/Y g:i a", strtotime($l->DateAdded)) : "" ?>
                                </td>
                                <?php if ($COMPANY_ID == 46):// Greenlink ?>
                                    <td class="lead_table" align="right">
                                        <?= strtotime($l->DateAssigned) ? date("m/d/Y g:i a", strtotime($l->DateAssigned)) : "" ?>
                                    </td>
                                <?php endif; ?>
                                <td class="lead_table">
                                    <?= $l->ExternalID ?>
                                </td>
                                <td class="lead_table">
                                    <a href="edit_lead.php?id=<?= $l->SubmissionListingID ?>" title="Edit Lead"
                                       data-toggle="tooltip">
                                        <?= $l->FirstName ?>
                                    </a>
                                </td>
                                <td class="lead_table">
                                    <a href="edit_lead.php?id=<?= $l->SubmissionListingID ?>" title="Edit Lead"
                                       data-toggle="tooltip">
                                        <?= $l->LastName ?>
                                    </a>
                                </td>
                                <td class="lead_table bluetext" align="right" style="white-space: nowrap;">
                                    <a href="#"
                                       onclick="wopen('trigger_call.php?id=<?= $l->SubmissionListingID; ?>&field=Phone', 'add_action', 350, 285);
                                           return false;">
                                        <?= format_phone($l->Phone) ?>
                                    </a>
                                </td>
                                <td class="lead_table bluetext" align="right" style="white-space: nowrap;">
                                    <a href="#"
                                       onclick="wopen('trigger_call.php?id=<?= $l->SubmissionListingID; ?>&field=WorkPhone', 'add_action', 350, 285);
                                           return false;">
                                        <?= format_phone($l->WorkPhone) ?>
                                    </a>
                                </td>
                                <td class="lead_table bluetext" align="right" style="white-space: nowrap;">
                                    <a href="#"
                                       onclick="wopen('trigger_call.php?id=<?= $l->SubmissionListingID; ?>&field=Phone2', 'add_action', 350, 285);
                                           return false;">
                                        <?= format_phone($l->Phone2) ?>
                                    </a>
                                </td>
                                <td class="lead_table bluetext" align="right" style="white-space: nowrap;">
                                    <?= $l->State ?>
                                </td>
                                <td class="lead_table bluetext" align="right" style="white-space: nowrap;">
                                    <?= $appt ? date("m/d/Y g:i a", strtotime($appt->AppointmentTime)) : "" ?>
                                </td>
                                <td class="lead_table" align="left">
                                    <a href="#"
                                       onclick="wopen('add_action.php?id=<?= $l->SubmissionListingID; ?>', 'add_action', 350, 550);
                                           return false;">
                                        <span class="glyphicon glyphicon-star" title="Add New"></span>
                                    </a>
                                    <span
                                        id="cetid_<?= $l->SubmissionListingID ?>"><?= $cetLookup[$l->LastContactEvent->ContactEventTypeID] == "" ? "" : $cetLookup[$l->LastContactEvent->ContactEventTypeID] ?></span>
                                </td>
                                <td class="lead_table">
                                    <?php if (isset($optifinowGlobalSetting['custom']['disabledStatus']) && $optifinowGlobalSetting['custom']['disabledStatus'] == TRUE): ?>
                                        <?php foreach ($contactStatusesTypes as $statusName => $status): ?>
                                            <?php if ($status->ContactStatusTypeID == $l->ContactStatusTypeID): ?>
                                                <?= $statusName ?>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    <?php else: ?>
                                        <select name="cstid_<?= $l->SubmissionListingID ?>"
                                                id="cstid_<?= $l->SubmissionListingID ?>"
                                                onchange="updateContactStatus('<?= $l->SubmissionListingID ?>', this.value);"
                                                class="form-submenu input-sm contactStatus">
                                            <?php
                                            foreach ($contactStatusesTypes as $statusName => $status) {
                                                ?>
                                                <option
                                                    value="<?= $status->ContactStatusTypeID ?>" <?= $status->ContactStatusTypeID == $l->ContactStatusTypeID ? "SELECTED" : "" ?>>
                                                    <?= $statusName ?>
                                                </option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    <?php endif; ?>
                                </td>
                                <td class="lead_table">
                                    <?= strtotime($lastCH->EventDate) ? date("m/d/Y g:i a", strtotime($lastCH->EventDate)) : "" ?>
                                </td>
                                <td class="lead_table" align="center">
                                    <a href="edit_lead.php?id=<?= $l->SubmissionListingID ?>" title="Edit Lead"
                                       data-toggle="tooltip"><i
                                            class="glyphicon glyphicon-edit"></i></a>
                                    <a href="mailto:<?= $l->Email; ?>" data-toggle="tooltip"
                                       title="Send Email"><i class="glyphicon glyphicon-envelope"></i></a>
                                    <?php if (isset($optifinowGlobalSetting['custom']['SMSOnDemand']) && $optifinowGlobalSetting['custom']['SMSOnDemand'] == TRUE): ?>
                                        <a href="#"
                                           onclick="wopen('send_sms.php?id=<?= $l->SubmissionListingID; ?>', 'send_sms', 400, 350);
                                               return false;" data-toggle="tooltip"
                                           title="Send SMS"><i class="glyphicon glyphicon-phone"></i></a>
                                    <?php endif; ?>
                                    <a href="#"
                                       onclick="wopen('<?= $apptLink; ?>', 'add_appointment', 350, 550);
                                           return false;" data-toggle="tooltip"
                                       title="<?= $appt ? "Edit" : "Add" ?> Reminder"><?= $appt ? '<i style="' . $pastdue . '" class="glyphicon glyphicon-calendar"></i>' : '<i class="glyphicon glyphicon-time"></i>' ?></a>
                                    <a href="#"
                                       onclick="wopen('pop_history.php?id=<?= $l->SubmissionListingID; ?>', 'pop_history', 850, 450);
                                           return false;"
                                       title="History" data-toggle="tooltip"><span
                                            class="glyphicon glyphicon-list-alt"></span></a>
                                </td>
                            </tr>
                            <?php
                        } //default view (contract and filter20)

                    }
                }
                $_SESSION['lead_step_ids'] = implode(";", $lead_step_ids);
                if ($isMan) {
                    ?>

                    <style>
                        .chosen-inline > .chosen-container {
                            position: absolute;
                        }
                    </style>

                    <tr>
                        <td colspan="2" title="Click to Check All"><input type="checkbox" value="1" id="check_all"/>
                            Check All
                        </td>
                        <script type="text/javascript">
                            $(document).ready(function () {
                                $("#check_all").click(function () {
                                    $('input:checkbox').not(this).prop('checked', this.checked);
                                });
                            });
                        </script>
                        <td class="form-inline" colspan="<?= sizeof($sortCols) - 2 ?>">
                            <div class="form-group chosen-inline col-md-2">
                                <label name="newRepID">Reassign Checked Leads to: &nbsp;</label>
                                <select id="newRepID" class="form-control input-sm">
                                    <option value="">-- Select Rep --</option>
                                    <?php
                                    foreach ($users as $u) {
                                        ?>
                                        <option
                                            value="<?= $u->UserID ?>" <?= $u->UserID == $_GET['userfilter'] ? "SELECTED" : "" ?>>
                                            <?= $u->FirstName . " " . $u->LastName ?>
                                        </option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group col-md-1 col-md-offset-2">
                                <input type="button" value="Go" class="btn btn-default btn-sm"
                                       onclick="ReassignLeads()"/>
                            </div>
                            <script>
                                function ReassignLeads() {
                                    var idArray = new Array();
                                    for (var i = 1; i <= <?= $i ?>; i++) {
                                        var checkbox = document.getElementById("checkbox_" + i)
                                        if (checkbox && checkbox.checked) {
                                            idArray.push(checkbox.value);
                                        }
                                    }

                                    if (idArray.length <= 0) {
                                        alert("Please select at least one lead");
                                        return;
                                    }

                                    var repID = document.getElementById("newRepID").value;

                                    if (repID == "") {
                                        alert("Please choose a rep to reassign to.");
                                        return;
                                    }

                                    if (!confirm("Are you sure you want to reassign " + idArray.length + " record(s) this cannot be undone.")) {
                                        return
                                    }

                                    var newURL = "reassign_leads.php?repID=" + repID + "&ids=" + idArray.join(",");

                                    document.location = newURL;

                                }
                            </script>
                        </td>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>
    <!-- pagination -->
    <div class="row">
        <div class="col-md-9">
            <ul class="pagination"><?= build_page_string($_SERVER['PHP_SELF'], $_GET, $leadsPerPage, $_GET['offset'], $leadcount); ?></ul>
            </br> <?= $leadcount ?> total leads
        </div>
        <div class="col-md-3">
            <form method="get" action="<?= $_SERVER['PHP_SELF'] . "?" . $getString ?>" class="form-inline">
                <div class="form-group pull-right">
                    <label for="count">Count:</label>
                    <select name="count" onchange="this.form.submit();" class="form-control input-sm">
                        <option value="25" <?= $_GET['count'] == 25 ? 'selected' : '' ?>>25</option>
                        <option value="50" <?= $_GET['count'] == 50 ? 'selected' : '' ?>>50</option>
                    </select>
                </div>
                <input type="hidden" name="view" value="<?= $_GET['view'] ?>"/>
            </form>
        </div>
    </div>
    <div id="debug_div" style="display: none;"></div>
    <div style="display: none;" class="detail_div" id="detailDiv">
        <div style="text-align: center; background: #CCC; padding: 3px 0px 3px 0px; font-weight: bold;">
            Please Choose a detailed description for<br/>
            <span id="shortStatus"></span>
        </div>
        <div style="width: 394px; text-align: center; padding: 25px 0px 15px 0px;">
            <select name="detailStatusID" id="detailStatusID">
            </select>
            <input type="hidden" name="detailContactID" id="detailContactID"/>
        </div>
        <div style="width: 394px; text-align: center; background: #FFF; padding-bottom: 15px;">
            <a href="#" onclick="submitDetailedContactStatus();">OK</a>
        </div>
    </div>
</div>

<i>
    <?php
    $time = microtime();
    $time = explode(' ', $time);
    $time = $time[1] + $time[0];
    $finish = $time;
    $total_time = round(($finish - $start), 4);
    echo 'page generated in ' . $total_time . ' seconds.';
    ?>
</i>

<?php date_default_timezone_set($originaltimezone); //reset the changed timezone to the original server timezone     ?>
<?php include("components/footer.php") ?>
</body>
</html>