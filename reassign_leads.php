<?php
	require("../printable/include/mysql.inc.php");
	require("../printable/include/optimize.printable.inc.php");
	require("globals.php");
		
	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	
	$portal = new OptimizePortal($COMPANY_ID, $db);
	
	$currentUser = $portal->GetUser($_SESSION['currentuserid']);
	$isAdmin = $portal->CheckPriv($currentUser->UserID, 'admin');
	$isCorpMan = $portal->CheckPriv($currentUser->UserID, 'corporatemanager');
	$isRegMan = $portal->CheckPriv($currentUser->UserID, 'regionmanager');
	$isDivMan = $portal->CheckPriv($currentUser->UserID, 'divisionmanager');
	if($isAdmin || $isCorpMan || $isRegMan || $isDivMan ) 
		$isMan = true;
	else
		$isMan = false;
	
	// Check login
	if( !$isMan ) 
	{
		header( "Location: login.php?message=" . urlencode( "Not logged in or login error.  Please try again." ) );
		die();
  	}
	
	$newRep = $portal->GetUser($_GET['repID']);
	
	if(!$newRep) 
	{
		header( "Location: list_lead.php?message=" . urlencode( "Invalid Rep" ) );
		die();
	}
  	
  	$leads = explode(",", $_GET['ids']);
  	
  	foreach($leads as $lID)
  	{
		$lead = $portal->GetOptimizeContact($lID, $currentUser->UserID);
		if($lead) 
		{
			$lead->UserID = $newRep->UserID;
			$portal->UpdateOptimizeContact($lead);
		}
  	}
  	
  	header( "Location: list_lead.php?message=" . urlencode( "Leads Reassigned." ));