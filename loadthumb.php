<?php
	require("../printable/include/mysql.inc.php");
	require("../printable/include/optimize.printable.inc.php");
	require("globals.php");
	
	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	
	$portal = new OptimizePortal($COMPANY_ID, $db);
	
	$currentUser = $portal->GetUser($_SESSION['currentuserid']);
	
	// Check login
	if( !$currentUser ) 
	{
		header( "Location: login.php?message=" . urlencode( "Not logged in or login error.  Please try again." ) );
		die();
  	}
  	
  	$file = $portal->GetFile($_GET['fileid']);
  		
	if(is_file("$FILES_LOCATION" . $_SESSION['currentcompanyid'] . "/thumbs/$file->ThumbPath"))
	{
				
		header("Content-type: image/jpeg");
		
		$im = imagecreatefromjpeg("$FILES_LOCATION" . $_SESSION['currentcompanyid'] . "/thumbs/$file->ThumbPath");
		
		if($im) {
			if($_GET['thumb'] == 'true' || is_numeric($_GET['fitwidth']))
			{
				// Get Width and hieght of image
				$origWidth = imagesx($im);
				$origHeight = imagesy($im);
				
				if($origWidth > $origHeight)
				{
					$width = (is_numeric($HTTP_GET_VARS['fitwidth']) ? $_GET['fitwidth'] : 58);
					$ratio = $width/$origWidth;
					$height = floor($origHeight * $ratio);
				}
				else 
				{
					$height = (is_numeric($HTTP_GET_VARS['fitwidth']) ? $_GET['fitwidth'] : 58);
					$ratio = $height/$origHeight;
					$width = floor($origWidth * $ratio);
				}
				
				$imOut = imagecreatetruecolor($width, $height);
				
				imagecopyresampled($imOut, $im, 0, 0, 0, 0, $width, $height, $origWidth, $origHeight);		
				
				ImageJpeg($imOut);
				ImageDestroy($imOut);
				ImageDestroy($im);
				
			}
			else 
			{
				ImageJpeg($im);
				ImageDestroy($im);
			}
		}
		else {
			if($_GET['thumb'] == 'true' || is_numeric($_GET['fitwidth']))
			{
				$width = (is_numeric($_GET['fitwidth']) ? $_GET['fitwidth'] : 50);
				$height = 50;
				//Create the image resource 
				$image = ImageCreate($width, $height);  
				
				//Make the background black 
				ImageFill($image, 0, 0, ImageColorAllocate($image, 255, 255, 255)); 
				
				//Add randomly generated string in white to the image
				ImageString($image, 2, 1, 12, "Invalid", ImageColorAllocate($image, 0, 0, 0));
				ImageString($image, 2, 1, 24, "Thumb", ImageColorAllocate($image, 0, 0, 0));
			}
			else
			{
				$width = 200;
				$height = 200;
				//Create the image resource 
				$image = ImageCreate($width, $height);  
				
				//Make the background black 
				ImageFill($image, 0, 0, ImageColorAllocate($image, 255, 255, 255)); 
				
				//Add randomly generated string in white to the image
				ImageString($image, 3, 30, 3, "Invalid Thumb", ImageColorAllocate($image, 0, 0, 0));
			}
			
			//Output the newly created image in jpeg format 
			ImageJpeg($image); 
		    
			//Free up resources
			ImageDestroy($image); 
		}

	}
	else 
	{
		$width = 1;
		$height = 1;
		//Create the image resource 
		$image = ImageCreate($width, $height);  
			
		//Make the background black 
		ImageFill($image, 0, 0, ImageColorAllocate($image, 255, 255, 255)); 
			
	
		//Tell the browser what kind of file is come in 
		header("Content-Type: image/jpeg"); 
	
		//Output the newly created image in jpeg format 
		ImageJpeg($image); 
	    
		//Free up resources
		ImageDestroy($image); 
		
	}
	
 
?>