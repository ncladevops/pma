<?php
require("../printable/include/mysql.inc.php");
require("../printable/include/Optimize.printable.inc.php");
require("globals.php");

$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);

$portal = new OptimizePortal($COMPANY_ID, $db);

$currentUser = $portal->UserAccess($_SESSION['currentuserid']);

// Check login
if (!$currentUser) {
    header("Location: login.php?message=" . urlencode("Not logged in or login error.  Please try again."));
    die();
}

$_GET['section'] = intval($_GET['section']);

// Get Cats
$rootCats = $portal->GetCategories('root', $_GET['section'], true, $currentUser->GroupID);

$cats = array();
$cat = new FileCategory();
foreach ($rootCats as $cat) {
    $cats[] = $portal->GetCategory($cat->CategoryID, true);
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title>
        <?= $portal->CurrentCompany->CompanyName ?> :: Download File
    </title>
    <meta http-equiv="content-type" content="text/html;charset=utf-8"/>
    <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico"/>
    <meta http-equiv="content-type" content="text/html;charset=utf-8"/>
    <script type='text/javascript' src='../printable/include/js/jquery-1.10.2.min.js'></script>
    <script type='text/javascript' src='../printable/include/js/jquery-ui.js'></script>
    <script src="js/func.js"></script>
    <script src="js/email_public.js"></script>
    <script language="JavaScript" type="text/JavaScript">

        $(document).ready(function () {
            // Handler for .ready() called.

            $('#dhtmlgoodies_tree').click(function () {
                $('#filedetails').show();
            });

            $('#search').keyup(function (e) {
                if ($(this).val().length > 2) {
                    $('#search-notice').hide();
                    loadFiles('all');
                    $('#filedetails').show();
                } else {
                    $('#search-notice').show();
                }
            });

            $('input[type=search]').on('search', function () {
                if ($(this).val() == '') {
                    $('#search-notice').hide();
                    loadFiles('all');
                    $('#filedetails').show();
                }
            });

        });

        var transactionURL = "printable_trans.php";
        var filesRequest;
        var fileDetailsRequest;
        var nCategoryID;

        function loadTipImage(fileID) {
            document.getElementById("toolTipBody").innerHTML = "<img src='loadthumb.php?fileid=" + fileID + "&fitwidth=200'>";
        }

        function loadFiles(categoryID) {
            nCategoryID = categoryID;
            document.getElementById("filelist").innerHTML = '<div style="text-align: center; padding-top: 100px;"><img src="images/loading.gif" /><br/>Loading ...</div>';

            filesRequest = GetXmlHttpObject()
            if (filesRequest == null) {
                alert("Your browser does not support AJAX!");
                return;
            }
            var url = transactionURL;
            url = url + "?action=GetFileTable&categoryid=" + categoryID + "&sortBy=" + document.getElementById('sort_by').value + "&search=" + document.getElementById('search').value;
            url = url + "&sid=" + Math.random();
            filesRequest.onreadystatechange = filesRequestStateChanged;
            filesRequest.open("GET", url, true);
            filesRequest.send(null);
        }

        function filesRequestStateChanged() {
            if (filesRequest.readyState == 4) {
                document.getElementById("filelist").innerHTML = filesRequest.responseText;
            }
        }

        function loadFileDetails(id) {

            document.getElementById("filepreview").innerHTML = '&nbsp;<img src="images/loading.gif" />';
            document.getElementById("filenameSpan").innerHTML = 'Loading ...';

            fileDetailsRequest = GetXmlHttpObject()
            if (fileDetailsRequest == null) {
                alert("Your browser does not support AJAX!");
                return;
            }
            var url = transactionURL;
            url = url + "?action=GetFileInfo&id=" + id;
            url = url + "&sid=" + Math.random();
            fileDetailsRequest.onreadystatechange = fileDetailsStateChanged;
            fileDetailsRequest.open("GET", url, true);
            fileDetailsRequest.send(null);
        }

        function fileDetailsStateChanged() {
            if (fileDetailsRequest.readyState == 4) {
                var xmlDoc = fileDetailsRequest.responseXML.documentElement;

                if (xmlDoc.getElementsByTagName("Status")[0].childNodes[0].nodeValue != "Success") {
                    alert("Error Loading file");
                    return;
                }

                if (xmlDoc.getElementsByTagName("FileID")[0].childNodes[0])
                    fileid = xmlDoc.getElementsByTagName("FileID")[0].childNodes[0].nodeValue;
                else
                    fileid = 0;
                if (xmlDoc.getElementsByTagName("FileName")[0].childNodes[0])
                    filename = xmlDoc.getElementsByTagName("FileName")[0].childNodes[0].nodeValue;
                else
                    filename = 0;
                if (xmlDoc.getElementsByTagName("Description")[0].childNodes[0])
                    title = xmlDoc.getElementsByTagName("Description")[0].childNodes[0].nodeValue;
                else
                    title = 0;
                if (xmlDoc.getElementsByTagName("Size")[0].childNodes[0])
                    size = xmlDoc.getElementsByTagName("Size")[0].childNodes[0].nodeValue;
                else
                    size = 0;
                if (xmlDoc.getElementsByTagName("LastEdit")[0].childNodes[0])
                    modified = xmlDoc.getElementsByTagName("LastEdit")[0].childNodes[0].nodeValue;
                else
                    modified = 0;
                if (xmlDoc.getElementsByTagName("Detail")[0].childNodes[0])
                    desc = xmlDoc.getElementsByTagName("Detail")[0].childNodes[0].nodeValue;
                else
                    desc = 0;
                if (xmlDoc.getElementsByTagName("DownloadLink")[0].childNodes[0])
                    downloadlink = xmlDoc.getElementsByTagName("DownloadLink")[0].childNodes[0].nodeValue;
                else
                    downloadlink = "";
                if (xmlDoc.getElementsByTagName("ViewLink")[0].childNodes[0])
                    viewlink = xmlDoc.getElementsByTagName("ViewLink")[0].childNodes[0].nodeValue;
                else
                    viewlink = "";
                if (xmlDoc.getElementsByTagName("CategoryID")[0].childNodes[0])
                    catID = xmlDoc.getElementsByTagName("CategoryID")[0].childNodes[0].nodeValue;
                else
                    catID = 0;
                if (xmlDoc.getElementsByTagName("IsPublic")[0].childNodes[0])
                    isPublic = xmlDoc.getElementsByTagName("IsPublic")[0].childNodes[0].nodeValue;
                else
                    isPublic = 0;

                document.getElementById("filepreview").innerHTML = "&nbsp;<img class='img-thumbnail' src='loadthumb.php?fileid=" + fileid + "&thumb=true&fitwidth=100' />";
                document.getElementById("filenameSpan").innerHTML = filename;
                document.getElementById("titleSpan").innerHTML = title;
                document.getElementById("descSpan").innerHTML = desc;
                document.getElementById("sizeSpan").innerHTML = size;
                document.getElementById("modifiedSpan").innerHTML = modified;

                if (isPublic == 1) {
                    document.getElementById("publicLinkSpan").innerHTML = "<?= $portal->CurrentCompany->Website ?>" + viewlink;
                    document.getElementById("fileemailDiv").style.display = "";
                    document.getElementById("fileemailLink").href = "#";
                    document.getElementById("fileemailLink").onclick = document.getElementById("emailImg" + fileid).onclick;
                }
                else {
                    document.getElementById("publicLinkSpan").innerHTML = "This document is not public";
                    document.getElementById("fileemailDiv").style.display = "none";
                    document.getElementById("fileemailLink").href = "#";
                    document.getElementById("fileemailLink").onclick = "";
                }

                if (catID == 451) {
                    document.getElementById("facebookDiv").style.display = "";
                    document.getElementById("twitterDiv").style.display = "";
                    document.getElementById("filedownloadDiv").style.display = "none";
                    document.getElementById("fileviewDiv").style.display = "none";
                }
                else {
                    document.getElementById("facebookDiv").style.display = "none";
                    document.getElementById("twitterDiv").style.display = "none";

                    if (downloadlink == "") {
                        document.getElementById("filedownloadDiv").style.display = "none";
                        document.getElementById("filedownloadLink").href = "#";
                    }
                    else {
                        document.getElementById("filedownloadDiv").style.display = "";
                        document.getElementById("filedownloadLink").href = downloadlink;
                    }
                    if (viewlink == "") {
                        document.getElementById("fileviewDiv").style.display = "none";
                        document.getElementById("fileviewLink").href = "#";
                    }
                    else {
                        document.getElementById("fileviewDiv").style.display = "";
                        document.getElementById("fileviewLink").href = viewlink;
                    }
                }

            }


        }

        function EmailPublicContent(fileID, fileLink, emailSubject, emailBody) {
            TriggerPublicContentEmail("", 0, fileID, fileLink, emailSubject, emailBody);
        }

    </script>

    <?php include("components/bootstrap.php") ?>

    <script type="text/javascript">
        //Custom selected click add class
        $(document).on('click', '.table > tbody > tr', function(e) {
            $('.table > tbody > tr').removeClass('info');
            $(this).addClass('info');

        });
    </script>

</head>
<body bgcolor="#FFFFFF">
<div id="page">
    <?php include("components/header.php") ?>
    <div id="body">
        <?php
        $CURRENT_PAGE = "Files";
        include("components/navbar.php");
        ?>
        <?php if (isset($_GET['message'])): ?>
            <div class="container">
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?= $_GET['message']; ?>
                </div>
            </div>
        <?php endif; ?>

        <div class="row">
            <div class="form-group col-md-offset-9 col-md-2">
                <span id="search-notice" style="display: none;"><small>search must be at least 3 characters
                    </small></span>
                <input type="search" id="search" name="search" placeholder="Search"
                       class="form-control input-sm"></input>
            </div>

            <?php
            $aSortOptions = array('Description' => 'Description', 'LastEdit' => 'Last Update');
            ?>
            <div class="form-group col-md-1">
                <select name="sort_by" id="sort_by" class="form-control input-sm" onchange="loadFiles(nCategoryID)">
                    <option value="">Sort By</option>
                    <?php
                    foreach ($aSortOptions as $sField => $sTitle) {
                        ?>
                        <option
                            value="<?php echo $sField; ?>" <?php if ($_GET['sort_by'] == $sField) echo "SELECTED"; ?>><?php echo $sTitle; ?></option>
                    <?php
                    }
                    ?>
                </select>
            </div>

        </div>

        <div id="fileContainer" class="foldercontainer row">
            <div id="folderTreeContainer" class="col-md-2 panel panel-default">
                <ul class="nav nav-pills nav-stacked bs-sidebar">
                    <?php
                    if (!empty($cats)) {
                        $fc = new FileCategory();
                        foreach ($cats as $fc) {
                            echo $fc->printNewList();
                        }
                    } else {
                        echo 'No categories';
                    }
                    ?>
                </ul>
            </div>
            <div class="container col-md-10">
                <div id="filelistContainer" class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Files</h3>
                    </div>
                    <div id="filelist" class="panel-body">
                    </div>
                    <div id="filedetails" class="detailinfo panel-footer" style="display: none;">

                        <div class="row">
                            <div class="container col-md-12">
                                <div id="filepreview" class="col-md-2"></div>
                                <div id="filetext" class="col-md-8">
                                    <strong>Filename:</strong> <span id="filenameSpan"></span><br/>
                                    <strong>Title:</strong> <span id="titleSpan"></span><br/>
                                    <strong>Description:</strong> <span id="descSpan"></span><br/>
                                    <strong>Size:</strong> <span id="sizeSpan"></span><br/>
                                    <strong>Modified:</strong> <span id="modifiedSpan"></span><br/>
                                    <strong>Public Link:</strong> <span id="publicLinkSpan"></span>
                                    <input type="text" id="holdtext" style="display:none;"/>
                                </div>
                                <div class="detaillinks col-md-2">
                                    <div id="filedownloadDiv" style="display: none;"><a id="filedownloadLink"
                                                                                        href="#" target="_blank"><i
                                                class="glyphicon glyphicon-save" style="font-size: 20px;"></i> <span
                                                class="label label-default">Download</label></a></div>
                                    <div id="fileviewDiv" style="display: none;"><a id="fileviewLink" href="#"
                                                                                    target="_blank"><i
                                                class="glyphicon glyphicon-zoom-in" style="font-size: 20px;"></i>
                                            <span class="label label-default">View</label></a></div>
                                    <div id="fileemailDiv" style="display: none;"><a id="fileemailLink" href="#"
                                                                                     target="_blank"><i
                                                class="glyphicon glyphicon-envelope" style="font-size: 20px;"></i>
                                            <span class="label label-default">Email</label></a></div>
                                    <div id="facebookDiv" style="display: none;"><a id="facebookLink"
                                                                                    href="http://www.facebook.com"
                                                                                    target="_blank"><img
                                                id="facebookImage" src="images/icos/facebook_large.png" border="0"
                                                class="file_icon" title="Post to Facebook"/></a></div>
                                    <div id="twitterDiv" style="display: none;"><a id="twitterLink"
                                                                                   href="http://www.twitter.com"
                                                                                   target="_blank"><img
                                                id="twitterImage" src="images/icos/twitter_large.png" border="0"
                                                class="file_icon" title="Tweet"/></a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="tipDiv" style="position:absolute; visibility:hidden; z-index:100"></div>
<script language="JavaScript" type="text/JavaScript">
    <?= isset($_GET['category']) ? "loadFiles('" . $_GET['category'] . "');" : "" ?>
</script>

<?php include("components/footer.php") ?>

</body>
</html>