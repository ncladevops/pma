<?php
	require("../printable/include/mysql.inc.php");
	require("../printable/include/optimize.printable.inc.php");
	require("globals.php");
	
	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	
	$portal = new OptimizePortal($COMPANY_ID, $db);
	
	$currentUser = $portal->UserAccess($_SESSION['currentuserid']);
	$isSubAdmin = $portal->CheckPriv($currentUser->UserID, 'subadmin');
		
	// Check login
	if(!$isSubAdmin)
	{
		header( "Location: " . $portal->CurrentCompany->Website . "login.php?message=" . urlencode( "Not logged in or login error.  Please try again." ) );
		die();
	}
  	
  	if($_POST['Submit'] == 'Delete')
	{		
		// Make sure file exists and get old names
		$file = $portal->GetFile($_GET['fileid']);
		
		if(!$file)
		{
			echo "Invalid File";
			die();
		}
		
		
		// delete old file
		if(is_file("{$FILES_LOCATION}" . $portal->CurrentCompany->CompanyID . "/files/$file->FilePath"))
		{
			unlink("{$FILES_LOCATION}" . $portal->CurrentCompany->CompanyID . "/files/$file->FilePath");
		}
		
		// delete old thumb
		if(is_file("{$FILES_LOCATION}" . $portal->CurrentCompany->CompanyID . "/thumbs/$file->ThumbPath"))
		{
			unlink("{$FILES_LOCATION}" . $portal->CurrentCompany->CompanyID . "/thumbs/$file->ThumbPath");
		}
		
		
		// update db
		$portal->DeleteFile($file->FileID);
			
		// direct back to manage files
		header("Location: manage_filedl.php?section=" . $_GET['section']);
	}
	
	// Get list of files
	$file = $portal->GetFile($_GET['fileid']);
		
	if(!$file)
	{
		echo "Invalid File";
		die();
	}
		
	// Lookup File Size
	if($file->FilePath != '')
	{
		$filesize = number_format(filesize("{$FILES_LOCATION}" . $portal->CurrentCompany->CompanyID . "/files/$file->FilePath")/1024, 2);
	}
	else 
	{
		$filesize = '0.00';
	}
		
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
	<title>
		<?= $portal->CurrentCompany->CompanyName ?> :: Delete File
	</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />		
	<link type="text/css" media="all" rel="stylesheet" href="style.css" />
	<link type="text/css" media="all" rel="stylesheet" href="css/edit_file.css" />
	<script  src="js/func.js"></script>	
	<link rel="stylesheet" href="thumbview/thumbnailviewer.css" type="text/css" />
	<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />
</head>
<body bgcolor="#FFFFFF">
<div id="page">
<?php include("components/header.php") ?>
<div id="body">
	<?php 
		$CURRENT_PAGE = "Control Panel";
		include("components/navbar.php"); 
	?>
<form method="post" action="<?= $_SERVER['PHP_SELF']?>?section=<?= $_GET['section'] ?>&fileid=<?= $file->FileID ?>" enctype="multipart/form-data" >
	<div class="error"><?= $_GET['message']; ?></div>
	<div id="AddFileDiv">
		<div class="sectionHeader">
			Delete File
		</div>
		<div class="sectionDiv">
			<strong>Are you sure you want to delete this file? All 
			information will be lost and can not be recovered.</strong><br/>&nbsp;<br/>
			<div class="itemSection">
				<div id="DescriptionDiv">
					<label for="description">Name:</label><br/>
					<b><?= $file->Description ?></b>
				</div>
			</div>
			<div class="itemSectionLong">
				<div id="DetailDiv">
					<label for="detail">Description:</label><br/>
					<b><?= $file->Detail ?></b>
				</div>
			</div>
		</div>
		<center>
		<div class="itemSection">
			<div class="buttonSection">
				<input type="submit" name="Submit" value="Delete"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="button" name="Cancel" value="Cancel" onclick="parent.location='manage_filedl.php?section=<?php echo $_GET['section']; ?>'"/>
			</div>
		</div>
		</center>
	</div>
</form>
</div>
</div>
</body>
</html>