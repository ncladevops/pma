<?php
require("../printable/include/mysql.inc.php");
require("../printable/include/optimize.printable.inc.php");
require("globals.php");

$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);

$portal = new OptimizePortal($COMPANY_ID, $db);

$currentUser = $portal->UserAccess($_SESSION['currentuserid']);
$isSubAdmin = $portal->CheckPriv($currentUser->UserID, 'subadmin');

// Check login
if (!$isSubAdmin) {
    header("Location: " . $portal->CurrentCompany->Website . "login.php?message=" . urlencode("Not logged in or login error.  Please try again."));
    die();
}

$posting = $portal->GetPosting($_GET['postingid']);

if (!$posting) {
    echo "Invalid Posting.";
    die();
}

if ($_POST['Submit'] == 'Save/Update') {
    $newposting = $posting;

    $newposting->SocialCategoryID = $_POST['category'];
    $newposting->PostingName = $_POST['postingname'];
    $newposting->Message = $_POST['message'];
    $newposting->ReferenceURL = $_POST['referenceurl'];
    $newposting->Tweet = $_POST['tweet'];
    if ($portal->CheckPriv($currentUser->UserID, 'admin')) {
        $newposting->GroupID = $_POST['GroupID'];
    }

    if (isset($_POST['facebook'])) {
        $newposting->Facebook = 1;
    } else {
        $newposting->Facebook = 0;
    }
    if (isset($_POST['linkedin'])) {
        $newposting->LinkedIn = 1;
    } else {
        $newposting->LinkedIn = 0;
    }
    if (isset($_POST['twitter'])) {
        $newposting->Twitter = 1;
    } else {
        $newposting->Twitter = 0;
    }

    // if new thumbnail upload
    if ($_FILES['socialimage']['name'] != '') {
        // delete old thumb
        if (is_file("{$FILES_LOCATION}" . $portal->CurrentCompany->CompanyID . "/social/$posting->ImageURL")) {
            unlink("{$FILES_LOCATION}" . $portal->CurrentCompany->CompanyID . "/social/$posting->ImageURL");
        }

        // Copy to output directory
        $tempFileName = $_FILES['socialimage']['tmp_name'];

        // create name for file
        $tName = $_FILES['socialimage']['name'];
        $tPath = $posting->SocialPostingID . "_$tName";

        echo $tName;
        echo '</br>';
        echo $tPath;

        // move to temp	
        move_uploaded_file($tempFileName, "{$FILES_LOCATION}" . $portal->CurrentCompany->CompanyID . "/social/$tPath");

        $newposting->ImageURL = $tPath;
    }

    $portal->UpdatePosting($newposting);

    // direct to manage postings
    header("Location: manage_socialpost.php?section=" . $_GET['section']);
    die();
}


$categories = $portal->GetSocialCategories('all', $_GET['section'], true, 'all');
$gs = $portal->GetCompanyGroups();

$groups = array();

$groups[0] = new Group();
$groups[0]->GroupID = 0;
$groups[0]->GroupName = "All";

foreach ($gs as $g) {
    $groups[$g->GroupID] = $g;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <title>
            <?= $portal->CurrentCompany->CompanyName ?> :: Edit / Update Social Posting
        </title>
        <meta http-equiv="content-type" content="text/html;charset=utf-8" />		
        <script  src="../printable/include/js/jquery-1.10.2.min.js"></script>
        <script  src="js/func.js"></script>
        <script  src="js/socialposting.js"></script>
        <script language="JavaScript" type="text/JavaScript">
            function togglePostingType(postingTypeID)
            {
            for(i = 1; i <= 4; i++)
            {
            document.getElementById("PostingType" + i).checked = false;
            }

            document.getElementById("PostingType" + postingTypeID).checked = true;
            }
        </script>	
        <?php include("components/bootstrap.php") ?>
    </head>
    <body bgcolor="#FFFFFF">
        <div id="page">
            <?php include("components/header.php") ?>
            <div id="body">
                <?php
                $CURRENT_PAGE = "Control Panel";
                include("components/navbar.php");
                ?>
                <form method="post" action="<?= $_SERVER['PHP_SELF'] ?>?postingid=<?= $_GET['postingid'] ?>&section=<?= $_GET['section'] ?>" enctype="multipart/form-data" >
                    <?php if (isset($_GET['message'])): ?>
                        <div class="container">
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <?= $_GET['message']; ?>
                            </div>
                        </div>
                    <?php endif; ?> 
                    <div id="EditPostingDiv" class="well container">
                        <div class="sectionHeader"><h2>Edit/Update Social Posting</h2></div>
                        <div id="PostingDiv" class="longSectionDiv">
                            <div class="itemSection row">
                                <div id="DescriptionDiv" class="form-group col-md-3">
                                    <label for="postingname">Name:</label><br/>
                                    <input type="text" class="form-control input-sm" name="postingname" value="<?= $posting->PostingName ?>" size="45" placeholder="Posting Title" required/>
                                </div>
                            </div>
                            <div class="itemSection row">
                                <div id="postto" class="form-group col-md-3">
                                    <label for="postto">Post To:</label></br>
                                    <input type="checkbox" name="facebook" id="facebookbox" value="1"  <?= $posting->Facebook == 1 ? "CHECKED" : "" ?>><img src="images/facebook.png" height="16" width="16"></img>&nbsp;
                                        <input type="checkbox" name="twitter" id="twitterbox" value="1" <?= $posting->Twitter == 1 ? "CHECKED" : "" ?>><img src="images/twitter.png" height="16" width="16"></img>&nbsp;
                                            <input type="checkbox" name="linkedin" id="linkedinbox" value="1" <?= $posting->LinkedIn == 1 ? "CHECKED" : "" ?>><img src="images/linkedin.png" height="16" width="16"></img>
                                                </div>
                                                </div>
                                                <div class="itemSection row">
                                                    <div id="ReferenceURL" class="form-group col-md-3">
                                                        <label for="referenceurl">Reference URL <span class="info">[<a href="#" title="Reference or source URL, usually a link to a website / page where the information on this post came from. &#10;Will not be included to the post itself.">?</a>]</span> :</label>
                                                        <br/>
                                                        <input id="referenceurl" class="form-control input-sm" type="text" name="referenceurl" value="<?= $posting->ReferenceURL ?>" size="45" placeholder="URL Reference Link"/> 
                                                    </div>
                                                </div>
                                                <div class="itemSection row">
                                                    <div id="Message" class="form-group col-md-6">
                                                        <label for="message">Message:</label>
                                                        <textarea class="form-control input-sm" name="message" id="messagearea" placeholder="Message"><?= $posting->Message ?></textarea>
                                                    </div>
                                                    <div id="Tweet" class="form-group col-md-6">
                                                        <label class="tweetclass" for="tweet">Twitter (character count: <span id="charcount">0</span>/140):</label>
                                                        <textarea class="tweetclass form-control input-sm" id="tweetarea" name="tweet" placeholder="Tweet Message" maxlength="140"><?= $posting->Tweet ?></textarea>
                                                    </div>
                                                </div>

                                                <div class="itemSection row">

                                                    <div id="Image" class="form-group col-md-3">
                                                        <label for="image">Image:</label><br/>
                                                        <?php if (isset($posting->ImageURL)): ?>

                                                            <div id="ThumbnailDiv">
                                                                Thumbnail: 
                                                                <a href="loadsocialthumb.php?postingid=<?= $posting->SocialPostingID ?>&thumb=false" rel="thumbnail"><img src="loadsocialthumb.php?postingid=<?= $posting->SocialPostingID ?>&thumb=true" class="img-thumbnail" border="0"/></a>
                                                            </div>

                                                        <?php endif; ?>




                                                        Upload New: <input type="file" class="form-control input-sm" name="socialimage"/>
                                                    </div>
                                                </div>
                                                <div class="itemSection row">
                                                    <div id="CategoryDiv" class="form-group col-md-3">
                                                        <label for="category">Parent Category:</label><br/>
                                                        <select class="form-control input-sm" name="category" required>
                                                            <option value=""> - Select a category - </option>
                                                            <?php
                                                            $category = new SocialCategory();
                                                            if (sizeof($categories) <= 0) {
                                                                echo '<option value=""> - No Categories - </option>';
                                                            } else {
                                                                foreach ($categories as $category) {
                                                                    if ($portal->CheckPriv($currentUser->UserID, 'admin') || $category->GroupID != 0) {
                                                                        ?>
                                                                        <option value="<?= $category->SocialCategoryID ?>" <?= $category->SocialCategoryID == $posting->SocialCategoryID ? "SELECTED" : "" ?>><?= str_repeat('&nbsp;&nbsp;&nbsp;', $portal->GetCategoryLevel($category->SocialCategoryID)) . $category->CategoryName ?></option>
                                                                        <?php
                                                                    }
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>

                                                <?php
                                                if ($portal->CheckPriv($currentUser->UserID, 'admin')) {
                                                    ?>
                                                    <div class="itemSection row">
                                                        <div id="ParentDiv" class="form-group col-md-3">
                                                            <label for="GroupID">Group:</label><br/>
                                                            <select class="form-control input-sm" name="GroupID" required>
                                                                <?php
                                                                foreach ($groups as $g) {
                                                                    ?>
                                                                    <option value="<?= $g->GroupID ?>" <?= $g->GroupID == $posting->GroupID ? "SELECTED" : "" ?>>
                                                                        <?= $g->GroupName ?>
                                                                    </option>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <?php
                                                }
                                                ?>
                                                <div class="itemSection row">
                                                    <div id="Preview" class="form-group col-md-6">
                                                        <label for="preview">Preview:</label><br/>
                                                        <button id="fbbtn" class="btn btn-default btn-sm"><img src="images/facebook.png" height="16" width="16"/> Preview</button>
                                                        <button id="ttbtn" class="tweetclass btn btn-default btn-sm"><img src="images/twitter.png" height="16" width="16"/> Preview</button>
                                                        <button id="libtn" class="btn btn-default btn-sm"><img src="images/linkedin.png" height="16" width="16"/> Preview</button>
                                                    </div>
                                                </div>
                                                <div class="edit-date pull-right">
                                                    <small>
                                                        <?php if ($posting->DateCreated): ?>
                                                            created: <?= date("m/d/Y, g:iA", strtotime($posting->DateCreated)); ?> </br>
                                                        <?php endif; ?>
                                                        <?php if (strtotime($posting->DateModified) > strtotime($posting->DateCreated)): ?>
                                                            last modified: <?= date("m/d/Y, g:iA", strtotime($posting->DateModified)); ?>
                                                        <?php endif; ?>
                                                    </small>
                                                </div>
                                                <center>
                                                    <div class="itemSection row">
                                                        <div class="buttonSection">
                                                            <input type="submit" name="Submit" class="btn btn-info btn-sm" value="Save/Update"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <input type="button" name="Cancel" value="Cancel" class="btn btn-default btn-sm" onclick="parent.location = 'manage_socialpost.php?section=<?php echo $_GET['section']; ?>'"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <input type="button" name="Delete" value="Delete" class="btn btn-default btn-sm" onclick="parent.location = 'delete_socialposting.php?section=<?php echo $_GET['section']; ?>&postingid=<?php echo $_GET['postingid']; ?>'"/>
                                                        </div>
                                                    </div>
                                                </center>
                                                </div>
                                                </div>
                                                </form> 
                                                </div>
                                                </div>
                                                <?php include("components/footer.php") ?>
                                                </body>
                                                </html>