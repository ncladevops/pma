<?php
	require("../printable/include/mysql.inc.php");
	require("../printable/include/optimize.printable.inc.php");
	require("globals.php");
	require '../printable/include/component/linkedin/linkedin.php';
	
	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	
	$portal = new OptimizePortal($COMPANY_ID, $db);
	
	$currentUser = $portal->UserAccess($_SESSION['currentuserid']);
	$isSuper = $portal->CheckPriv($currentUser->UserID, 'supervisor');
			
	if(!$currentUser)
	{
		die("Not logged in or login error.");
	}
	
	$posting = $portal->GetPosting($_GET['pid']);
	
	if(!$posting || !$posting->LinkedIn) {
	//if(!$portal->CheckPostingAssign($posting->SocialPostingID, $currentUser->UserID)) {
		die("Invalid posting id or you do not have permissions to that posting");
	}
	
	$oauth_callback = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
	$linkedin = new LinkedIn($LINKEDIN_KEY, $LINKEDIN_SECRET, $oauth_callback );
	//$linkedin->debug = true;
		
	if (!isset($_SESSION['linkedin_oauth_token'])) {
		if(strlen($currentUser->LinkedinToken) <= 0) {
			$linkedin->getRequestToken();
			
			// store the token
			$_SESSION['linkedin_oauth_token'] = $linkedin->request_token->key;
			$_SESSION['linkedin_oauth_token_secret'] = $linkedin->request_token->secret;
			$_SESSION['linkedin_oauth_verify'] = true;
			
			// redirect to auth website
			header("Location: " . $linkedin->generateAuthorizeUrl());
			die();
		} else {
			// Authorize User
			list($t,$s) = explode(":",$currentUser->LinkedinToken);
			$linkedin->access_token = new OAuthConsumer($t, $s, 1);
			$response = new SimpleXMLElement($linkedin->getProfile("~:(id,first-name,last-name,headline,picture-url)"));
			
			// if authorized set session
			if($response->status != '401') {
				$_SESSION['linkedin_oauth_token'] = $t;
				$_SESSION['linkedin_oauth_token_secret'] = $s;
			}
			// else clear credentials
			else {
				$currentUser->LinkedinToken = "";
				$portal->UpdateUser($currentUser);
			}
			
			header('Location: ' . basename(__FILE__) . "?pid=" . $posting->SocialPostingID);
			die();
		}

	} elseif (isset($_GET['oauth_verifier']) && isset($_SESSION['linkedin_oauth_verify'])) {
		// verify the token
		$linkedin->request_token = new OAuthConsumer($_SESSION['linkedin_oauth_token'], $_SESSION['linkedin_oauth_token_secret'], 1);
        $linkedin->oauth_verifier = $_GET['oauth_verifier'];
		unset($_SESSION['linkedin_oauth_verify']);

		// get the access token
        $linkedin->getAccessToken($_GET['oauth_verifier']);
		
		// store the token (which is different from the request token!)
		$_SESSION['linkedin_oauth_token'] = $linkedin->access_token->key;
		$_SESSION['linkedin_oauth_token_secret'] = $linkedin->access_token->secret;
		
		// store the token with the user
		$currentUser->LinkedinToken = $linkedin->access_token->key . ":" . $linkedin->access_token->secret;
		$portal->UpdateUser($currentUser);

		// send to same URL, without oauth GET parameters
		header('Location: ' . basename(__FILE__) . "?pid=" . $posting->SocialPostingID);
		die();
	}

	// assign access token on each page load
	$linkedin->access_token = new OAuthConsumer($_SESSION['linkedin_oauth_token'], $_SESSION['linkedin_oauth_token_secret'], 1);
	
	if($_POST['submit'] == 'Post to LinkedIn') {
		if($posting->ImageURL != "") {
			 $posting->Message .= "\n\n" . $SOCIAL_IMG_URL . $posting->SocialPostingID;
		}
		$response = new SimpleXMLElement($linkedin->share($posting->Message));
		$portal->PostSocialTransaction($posting->SocialPostingID, $currentUser->UserID, 'linkedin', $response->{'update-key'});
		echo '<xmp>';
		echo "Share Response\n";
		var_dump($response);
		echo '</xmp>';
		die();
	}
	?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" class="os-win"><!--<![endif]--><head>
	<meta name="lnkd-track-json-lib" content="http://s.c.lnkd.licdn.com/scds/concat/common/js?h=2jds9coeh4w78ed9wblscv68v-eo3jgzogk6v7maxgg86f4u27d">
	<meta name="appName" content="leo">
	<script type="text/javascript" src="http://s.c.lnkd.licdn.com/scds/concat/common/js?h=8swqmpmjehppqzovz8zzfvv9g-aef5jooigi7oiyblwlouo8z90-7tqheyb1qchwa8dejl8nvz7zd-10q339fub5b718xk0pv9lzhpl"></script>
	<title>Share news on LinkedIn | LinkedIn</title>
	<meta http-equiv="content-type" content="text/html;">
	<meta name="pageKey" content="share_offsite">
 <link rel="stylesheet" type="text/css" href="http://s.c.lnkd.licdn.com/scds/concat/common/css?h=3bifs78lai5i0ndyj1ew7316e-74ggnaghidi94zrm60imf01fl-2it1to3q1pt5evainys9ta07p-4uu2pkz5u0jch61r2nhpyyrn8-ee859529mdos6retehcmn9z8a-7poavrvxlvh0irzkbnoyoginp-4om4nn3a2z730xs82d78xj3be-3t9ar1pajet97hzt9uou74qbb-ct4kfyj4tquup0bvqhttvymms-cxff0g818hf3ks7dzixd4lqcq-449dtfu96optpu75y189filyn-265u9sn4x5vwuq2b8bh0p13m0">
 <script type="text/javascript" src="http://s.c.lnkd.licdn.com/scds/concat/common/js?h=9yapbb9c5ce3w0tjhqp8s1ben-be35lq69dqsbgl8h9t4bqpy08-avftajdh5oq2u6k2vaor3czdy-2r5gveucqe4lsolc3n0oljsn1-e9rsfv7b5gx0bk0tln31dx3sq-8v2hz0euzy8m1tk5d6tfrn6j-b88qxy99s08xoes3weacd08uc-6a5und5cdxoyc0xrbi3x7kqqz-5jtrz5noan83x6iogrfoq65dz-5sxnyeselbctwpry658s2lkew-5ee6w9abrtmcjqm2omop15g07-bmajlznvqaout2p7mpludwbus"></script>
                    <link rel="stylesheet" type="text/css" href="http://s.c.lnkd.licdn.com/scds/concat/common/css?h=e9j4flijf1zxb0ceg7pentptv-8k1mwl5acy7aq03x5lv3hny5x-6ramlbadr9lh7v5r7vuc6t4ld">
                    <script type="text/javascript" src="http://s.c.lnkd.licdn.com/scds/concat/common/js?h=62og8s54488owngg0s7escdit-c8ha6zrgpgcni7poa5ctye7il-djim7uyllidc9gta745y2wo5m-51dv6schthjydhvcv6rxvospp-34kh77cjm94ool6ef7iyyl160-25bfyzbe9nkhb54rhzauomdv3-34nej6plgotmo4hbnvjthteuu-eiz4uz8kfbu4iybfohrnt8k53-9gwlc8mz83jdjgs7toofd2cop-c5cupvwtxwkyyvzc08zy7zxoi-4lk8t46gpoexa5i04xwevbe3u-bsnr81j3v60h85h0kpvj4ednu-8v6o0480wy5u6j7f3sh92hzxo-1na957r12xyfe317uma5pn9mc-3h7320kwlnqtbngzm67z2annq"></script>
<script id="yui__dyn_0" type="text/javascript" src="http://edge.quantserve.com/quant.js"></script><script id="yui__dyn_1" type="text/javascript" src="http://b.scorecardresearch.com/beacon.js"></script><script type="text/javascript">
        /* extlib: _reveal */
            LI.Controls.addControl('control-http-12220-13730550-505', 'Reveal', { targetNode: 'nus-share' });
        /* extlib: _ghostlabel */
            LI.Controls.addControl('control-http-12220-13730550-506', 'GhostLabel', {});
        /* extlib: _checktextarea */
LI.i18n.register( 'CheckTextarea-error', 'You have exceeded the maximum length by <strong>{0}<\/strong> character(s).' );
LI.i18n.register( 'CheckTextarea-empty',   'You may add up to <strong>{0}<\/strong> characters.' );
LI.i18n.register( 'CheckTextarea-full', 'You may not add more characters.' );
LI.i18n.register( 'CheckTextarea-countdown',   'You may add up to <strong>{0}<\/strong> more characters.' );
LI.i18n.register( 'CheckTextarea-countdown-file-sharing',   '{1} character(s).' );
LI.i18n.register( 'CheckTextarea-twitter-under', 'Count: <strong>{1}<\/strong>' );
LI.i18n.register( 'CheckTextarea-twitter-over',   'Count: <strong>{1}<\/strong> (Only 140 characters will show on Twitter)' );
LI.i18n.register( 'CheckTextarea-twitter-over-file-sharing',   '{1} character(s): Only the first 140 characters will be shown on Twitter.' );
            LI.Controls.addControl('control-http-12220-13730550-507', 'CheckTextarea', {
              maxLength: 700,
              useTwitterCountdown: 'dialog-cc-twitter'
            });
        /* extlib: _nustweetstatus */
            LI.Controls.addControl('control-http-12220-13730550-508', 'NusTweetStatus', {
                      mode: 'no-account',
                      popupURL: 'https://www.linkedin.com/secure/tether?start=&provider=TWITTER&mini=&twitterSettings=upsell'
                    });
        /* extlib: _styleddropdown */
            LI.Controls.addControl('control-http-12220-13730551-509', 'StyledDropdown', { listClass: 'drop' });
            LI.Controls.addControl('control-http-12220-13730551-510', 'Reveal', { targetNode: 'connection-share' });
        /* extlib: _addresslabels */
LI.i18n.register( 'AddressLabel-limitOver', 'You can send this message to {0} total recipients. You currently have {1};' );
LI.i18n.register( 'AddressLabel-limitAt',   'You have the maximium number of total recipients({0}).' );
LI.i18n.register( 'AddressLabel-limitClose',   'You can send this message to {0} total recipients. You can add {1} more.' );        
    
            LI.Controls.addControl('control-http-12220-13730551-511', 'AddressLabels', {
                    hiddenInput: 'member-ids',
                    freeFormInput: 'guest-emails',
                    freeFormLabel: 'Email address',
                    cache: 'member-cache',
                    dataSource: {
                      url: 'http://www.linkedin.com/wsConnections'
                    },
                    ghostLabelText: 'Start typing a name or email address'
                  });
            LI.Controls.addControl('control-http-12220-13730552-512', 'CheckTextarea', {
                  maxLength: 7000,
                  grow: {
                    onFocus: 80,
                    auto: false
                  }
                });
            
        /* extlib: _sharedialog */
LI.i18n.register( 'ShareDialog-optional', '(optional)' );
LI.i18n.register( 'ShareDialog-required', 'Add a Subject...' );        
    
            LI.Controls.addControl('control-http-12220-13730554-513', 'ShareDialog', {
  mode: 'isOffsite',
  isFileShare : false,
  isUscpShare : false
});
          </script></head>      
<body class="en member v2 sharing-popup chrome-v5 chrome-v5-responsive background-v4 sticky-bg js " id="pagekey-share_offsite">
 <input id="inSlowConfig" type="hidden" value="false">
<script type="text/javascript">document.body.className += " js ";</script>
<div id="header">
 <h1><a href="/home?trk=hb_logo"><img src="http://s.c.lnkd.licdn.com/scds/common/u/img/logos/logo_linkedin_92x22.png" width="92" height="22" alt="LinkedIn" target="_blank"></a></h1>
</div>
<div id="body">
 <div class="wrapper">
<div id="global-error">
</div>
<div id="share-offsite"><form action="<?= $_SERVER['PHP_SELF'] ?>?pid=<?= $posting->SocialPostingID; ?>" method="POST"> <input type="hidden" name="csrfToken" value="ajax:3397641086116794981">
<span style="display:none">
  <input id="group-ids" type="hidden" value="" name="groupIDs">
  <input id="member-ids" type="hidden" value="" name="messageMemberIDs">
  <input id="image-url-id" type="hidden" value="" name="contentImage">
  <input id="ignore-warnings" type="hidden" value="false" name="ignoreWarnings">
  <input id="guest-emails" type="hidden" value="" name="messageGuestEmails">
    <input type="text" name="group-cache" value="" id="group-cache">
    <input type="text" name="send-cache" value="" id="member-cache">
    <input type="text" name="contentImageCount" value="0" id="contentImageCount-shareForm">
    <input type="text" name="contentImageIndex" value="-1" id="contentImageIndex-shareForm">
</span>
<div class="object">
</div>
<div id="sharing-error"></div>
<ul class="global-form">
    <li class="">
           <input type="checkbox" name="isPostingToNetworkUpdates" value="isPostingToNetworkUpdates" checked="" id="isPostingToNetworkUpdates-shareForm">
		   <script id="controlinit-http-12220-13730550-505" type="text/javascript+initialized" class="li-control">
        /* extlib: _reveal */
            LI.Controls.addControl('control-http-12220-13730550-505', 'Reveal', { targetNode: 'nus-share' });
          </script>
          <label for="isPostingToNetworkUpdates-shareForm" class="primary">Post to updates</label>
      <div id="nus-share" class="mini-form" style="display: block;">
          <span class="error" name="postText" id="postText-shareForm-error"></span>
              <label for="postText-shareForm" class="ghost" id="yui-gen1" style="display: none;">Comment optional</label>
          <script id="controlinit-http-12220-13730550-506" type="text/javascript+initialized" class="li-control">
        /* extlib: _ghostlabel */
            LI.Controls.addControl('control-http-12220-13730550-506', 'GhostLabel', {});
          </script>
            <textarea name="postText" id="postText-shareForm" rows="12" cols="40" class="post" placeholder="Comment optional" readonly style="height: 115px;"><?= $posting->Message ?></textarea>
          <script id="controlinit-http-12220-13730550-507" type="text/javascript+initialized" class="li-control">
        /* extlib: _checktextarea */
LI.i18n.register( 'CheckTextarea-error', 'You have exceeded the maximum length by <strong>{0}<\/strong> character(s).' );
LI.i18n.register( 'CheckTextarea-empty',   'You may add up to <strong>{0}<\/strong> characters.' );
LI.i18n.register( 'CheckTextarea-full', 'You may not add more characters.' );
LI.i18n.register( 'CheckTextarea-countdown',   'You may add up to <strong>{0}<\/strong> more characters.' );
LI.i18n.register( 'CheckTextarea-countdown-file-sharing',   '{1} character(s).' );
LI.i18n.register( 'CheckTextarea-twitter-under', 'Count: <strong>{1}<\/strong>' );
LI.i18n.register( 'CheckTextarea-twitter-over',   'Count: <strong>{1}<\/strong> (Only 140 characters will show on Twitter)' );
LI.i18n.register( 'CheckTextarea-twitter-over-file-sharing',   '{1} character(s): Only the first 140 characters will be shown on Twitter.' );
            LI.Controls.addControl('control-http-12220-13730550-507', 'CheckTextarea', {
              maxLength: 700,
              useTwitterCountdown: 'dialog-cc-twitter'
            });
          </script>
      </div>
	  <input type="submit" value="Post to LinkedIn" name="submit" />
    </li>
</ul>
      <input type="hidden" name="submitShare" value="" id="submitShare-shareForm"><input type="hidden" name="sctx" value="O" id="sctx-shareForm"><input type="hidden" name="eid" value="ARTC__31" id="eid-shareForm"><input type="hidden" name="fromShareID" value="" id="fromShareID-shareForm"><input type="hidden" name="trk" value="LI_BADGE_OLD" id="trk-shareForm"><input type="hidden" name="updateID" value="" id="updateID-shareForm"><input type="hidden" name="contentUrl" value="http://{articleUrl}" id="contentUrl-shareForm"><input type="hidden" name="contentResolvedUrl" value="http://{articleUrl}" id="contentResolvedUrl-shareForm"><input type="hidden" name="facebookDescription" value="" id="facebookDescription-shareForm"><input type="hidden" name="facebookCaption" value="" id="facebookCaption-shareForm"><input type="hidden" name="facebookExternalUrl" value="" id="facebookExternalUrl-shareForm"><input type="hidden" name="tetherAccountID" value="" id="tetherAccountID-twitterAccounts-shareForm"><input type="hidden" name="facebookTetherID" value="" id="facebookTetherID-twitterAccounts-shareForm"><input type="hidden" name="groupIDs" value="" id="groupIDs-shareForm"><input type="hidden" name="messageMemberIDs" value="" id="messageMemberIDs-shareForm"><input type="hidden" name="savedState" value="fClQonhjpSVFqSdxsBhBsC5EkOVBsC5EsOVFtmsKrSlIbDhBrCZOs2VJrScLf0E-umdxtCBOk6JLrS9BoS5CbPN5jAZpkAlmhjVVoS5Sqn9gqSZLoClzomoY820afChBt6dBr6ljpn9xq5dHrSZypmdxhDdFbPNBsSNxpzVApnhzpmNBkSlOomxjqSZLoClzokpPqjMw80E-q7hDrClcpmtxsTdBjndKrSBQoSlKrCZzbPMRdzVEt6tKpkNBpS5PsSldsSVLqnhzpmVKrScY820afCxQpSVBj7hzpmFytldPrCZFt6dBrCVLoOYYd3c-q7hDrClct6dBqC9RkTdKrSBQoSlKrCZzf20w2zVQrDlLgTdKrSBQoSlKrCZ3sClyrmlJbPMPdj4-t6VRrQdPrCZFt6dBrCVLgT9BoCRBrjMw80E-t6VRrQdApnhzpmNBkTdKrSBQoSlKrCZzbPMMfDhKtmZ3p6lQoSlIpldPrCZFt6dBrCVLoPMw80E-p6lQoSlIpldBsC5EkSlQonpFsB1PqiYYpndIomo-p6lQoSlIpldBsC5EkSlQonpFsB1PqjMw80E-rDtLq5dBsC5EkSlQonpFsB1PqiYYpnlOt3VKtSZEkSlOomxjpnhxtCBOk7dFf20w2zVEt6tKpkNBpS5PsSlds7lLsCsLf30-q7hDrClcpmtxsTdBjn1RrT9Df20w2zVQrDlLgTdMtmZOhT9BoCRBriYYc3VQrDlLgTdMtmZOhT9BoCRBrjMw80E-t6VRrQdApnhzpmNBkT1RrT9DbPMMfDhKtmZ3p6lQoSlIpldMtmZOpPMw80E-p6lQoSlIpldBsC5EkT1RrT97sSALf6lPr65CfChBt6dBr6ljpn9xq5dMtmZOhTdFf20w2zVKtSZEkSlOomxjs7lLsAtPqiYYpnlOt3VKtSZEkSlOomxjs7lLsAtPqjMw80E-q7hDrClct7dLk6hBpkpJbPMMfCxQpSVBj7hPrR1Apml6rjMw80E-sClQt6BTl4d3sSALf6lPr65CfD9Bt7hFtRh3gTdFf20w2zVVt6BIqm9FsSBmp6lBhCQLf4lejRBihlp5fDBQqmNFoCBPqlpApml6rjMw80E-p6lQoSlIpldBsC5EkShBpkpdsSALf6lRsDg-p6lQoSlIpldBsC5EkShBpkpdsSAY820afCVTrSxjpn9xq5dApml6jndFbPNBtn9QfCVTrSxjpn9xq5dApml6jndFf20w2zVIsBlApnpIrTdBsyYYvmNOlmlIoSBQsC5XbOYWs7hQq3VIsBlApnpIrTdBszMw80E-r79lt6VBt6VLoOYYvmNOlmlIoSBQsC5XbOYWs7hQq3VIsBlQrClQrCZzf20w2zVKrSBQtm9FsDhQgmlAtmNzrABQrClQrCZzbPNBsSNxpzVKrSBQtm9FsDhQgmlAtmNzrABQrClQrCZzf20w2zVUpmhKim5Fp6ldp6lQoSlIpldQrClQrCZzbPMNbjVUpmhKim5Fp6ldp6lQoSlIpldQrClQrCZzf20w2zVQrDlLgS5Fp6ldpmNyonhzpmNBkThKpnhKrScLf30-t6VRrQdxqmhBjmlIoC5QoSlIpldQrClQrCZzf20w2zVEt6tKpkNVsC5Jrnljt6VBt6VLoOYYdz4-q7hDrClcun9xrmRRkThKpnhKrScY820afCxQpSVBj6lIt6Bkt6VBt6VLoOYYd34-q7hDrClcpmNQqlhQrClQrCZzf20w2zVDrCBHoS5Ot2YYh4NfnQl7h452nQBcfCtKqmJzon9Qf20w2zVUomF1sSALf6lRsDg-u65GgndFf20w2zVEt65gsClOsClCpn8Lf4gT9mlzsDlLkSlIoSBQsC52dOkZpmdOtmZPeT1Joip4dOlIsBlBr6dFt79xgzsBfmNOtjJMrm4CpnlOt3RFrCBJeT1Joip4dOlBr7hFl6lIoSBQsC52dOkZpmNQqngXs6Rx9AgT9nBOomRJtldBr6dFt79xgzsBfnBOomRJtnc_pmNzqnhOgmlOomxPbSRLoOVKqmhBqSVFr2VTtTsLbPFMt7hEfCxQol1Opn9OpmpBszMw80E-rCBxrmZ4sClOsClCpn8Lf6RLoOVKqmhBqSVFr3VKqm5JrQhOpn9OpmpBszMw80E-rCBxpQ5BsC5EkTdFbPNBsSNxpzVKqm5DgmlOomxjsSAY820afCtKqnhQpld9lmVLqnhMjSlOomxPbPN5j491gQBck511nRhfjzVDrCBQt6ljillKrSBQs4ZBsC5EsPMw80E-t7dBl491rCZFt71fpn9xq7cLf4h5hkpdfDhPplh2gmVLqnhMjSlOomxPf20w2zVQr7lPpl9BsC5EsOYYhkN2gkd9j51gglZkjQU-t6NRsSlipn9xq7cY820afCVLqnhzgmlOomxPbPNApnhBr71JrQdQpmJzqlhBsC5EkT9LhCNOtjVKrSBQoQ5BsC5EsPMw80E-h4BVt6BQrAlJrT9CbPMNcRZvgRhigjV4inBQqnhKhmRLsCoY820afClMulhQu6lQrCZ3pSVFsC5EsOYYhlh9kQp6jPVBs7Bkt7xBt6VLgStKqn9xq7cY820afClQonhjpSVFqSdxsBhBsC5EkOVBsC5EsOVFtmsKrSlIbDhBrCZOs2VJrScY" id="savedState-shareForm"><input type="hidden" name="ajax" value="true" id="ajax-shareForm"><input type="hidden" name="applicationOrigin" value="" id="applicationOrigin-shareForm"><input type="hidden" name="frontierSubmitShare" value="nhome/submit-post" id="frontierSubmitShare-shareForm"></form>
          <script id="controlinit-http-12220-13730554-513" type="text/javascript+initialized" class="li-control">
        /* extlib: _sharedialog */
LI.i18n.register( 'ShareDialog-optional', '(optional)' );
LI.i18n.register( 'ShareDialog-required', 'Add a Subject...' );        
    
            LI.Controls.addControl('control-http-12220-13730554-513', 'ShareDialog', {
  mode: 'isOffsite',
  isFileShare : false,
  isUscpShare : false
});
          </script>
</div>
 </div>
</div>
<script type="text/javascript">LI.Controls.processQueue();</script>      
<div id="footer">
 <p class="copy">All content Copyright &copy; 2013, LinkedIn Corporation</p>
</div>
          <script id="localChrome"></script>
</body></html>