<?php
	require("../printable/include/mysql.inc.php");
	require("../printable/include/optimize.printable.inc.php");
	require("globals.php");
	
	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	
	$portal = new OptimizePortal($COMPANY_ID, $db);
	
	$currentUser = $portal->GetUser($_SESSION['currentuserid']);
	
	$isSubAdmin = $portal->CheckPriv($currentUser->UserID, 'subadmin');
				
	if(!$isSubAdmin)
	{
		header( "Location: " . $portal->CurrentCompany->Website . "home.php?message=" . urlencode( "Accessed Denied." ) );
		die();
	}
		
	$filter = $portal->GetFilter($_GET['filterid']);
	$filterBlocks = $portal->GetFilterBlocks($filter->FilterID);	
	$groups = $portal->GetCompanyGroups();
			
	if(!$filter)
	{
		header("Location: manage_filters.php?message=" . urlencode("Invalid Filter ID"));
		die();
	}
	
	if($_POST['Submit'] == 'Cancel')
	{
		header('Location: manage_filters.php?message=' . urlencode("Action Canceled. Filter not updated."));
		die();
	}
	elseif($_POST['Submit'] == 'Delete' || $_GET['action'] == 'delete')
	{
		$portal->DeleteFilter($filter->FilterID);
		
		header('Location: manage_filters.php?message=' . urlencode("Action Successful. Filter deleted."));
		die();
	}
	elseif($_POST['FilterName'])
	{
		$filter->FilterName 	= $_POST['FilterName'];
		$filter->GroupID		= $_POST['GroupID'];
		
		$portal->UpdateFilter($filter);
		
		header('Location: manage_filters.php?message=' . urlencode("Action Successful. Filter updated."));
		die();
	}
  	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	<title>
        <?= $portal->CurrentCompany->CompanyName ?> :: Edit Filter
    </title>
    <meta http-equiv="content-type" content="text/html;charset=utf-8"/>

    <?php include("components/bootstrap.php") ?>

    <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico"/>

    <script src="js/func.js"></script>
</head>
<body bgcolor="#FFFFFF">
<?php include("components/header.php") ?>
<div id="body">
	<?php
    $CURRENT_PAGE = "Home";
    include("components/navbar.php");
    ?>
    <?php if (isset($_GET['message'])): ?>
        <div class="container">
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?= $_GET['message']; ?>
            </div>
        </div>
    <?php endif; ?>
	<div id="controlPanelContainer" class="row">
        <div id="folderTreeContainer" class="col-md-offset-2 col-md-2 panel panel-default">
            <?php include("components/controlpanel_tree.php"); ?>
        </div>
        <div class="sectionHeader col-md-7">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Control Panel - Edit Filter</h3>
                </div>
                <div class="panel-body">
                    <div id="detailContainer">
                        <div class="row">
                            <form method="post" action="<?= $_SERVER['PHP_SELF'] ?>?filterid=<?= $filter->FilterID; ?>" id="MainForm">
                            <div style="width: 300px;">
                            <label for="GroupID">Group:</label><br/>
									 <select id="GroupID" name="GroupID">
									  <option value="0">All</option>
									   <?php
										foreach($groups as $g)
										{
										?>
										<option value="<?= $g->GroupID?>"
										<?= $g->GroupID == $filter->GroupID ? "SELECTED" : "" ?>>
										<?= $g->GroupName ?>
										</option>
										<?php } ?>
									  </select>
									  </div><br/>
                             <label for="FilterName">Filter Name:</label><br/>
							<input type="text" id="FilterName" name="FilterName" style="width: 415px" value="<?= $filter->FilterName ?>"/><br/>
                            <table width="100%" class="message_table table table-striped table-hover table-bordered"><br/>
                                <thead>
                                <tr class="filter_table rowheader" id="HeaderRow">
                                    <th class="filter_table">Order</th>
                                    <th class="filter_table">Criteria</th>
                                    <th class="filter_table">Sort By</th>
                                    <th class="filter_table">Limit</th>
                                    <th class="filter_table">&nbsp;</th>
                                    <th class="filter_table">&nbsp;</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php                           
								$i = 0;
								foreach($filterBlocks as $fb) {
								?>
								<tr class="filter_table<?= $i % 2 == 0 ? ' roweven' : ' rowodd' ?>">
									<td>
									<div class="sortArrowContainer" style="float: left;">
										<a href="edit_filterblock.php?action=up&filterblockid=<?= $fb->FilterBlockID ?>"><img src="images/sort_arrow_up.png" border="0" /></a><br/>
										<a href="edit_filterblock.php?action=down&filterblockid=<?= $fb->FilterBlockID ?>"><img src="images/sort_arrow_down.png" border="0" /></a>
									</div>
										<div class="filterOrderNumber">
											<?= $fb->FilterOrder ?>
										</div>
									</td>
									<td>
										<ul>
										<?php
											foreach($fb->FilterFields as $ff) {
											?>
											<li><?= "$ff->LeftValue $ff->CompareValue $ff->RightValue" ?></li>
										<?php } ?>
										</ul>
									</td>
									<td>
									<ul>
									<?php
										foreach($fb->SortFields as $sf) {
									?>
										<li><?= "$sf->Field $sf->Direction" ?></li>
									<?php } ?>
										</ul>
										</td>
										<td><?= $fb->FilterLimit == 0 ? "All" : $fb->FilterLimit ?></td>
										<td><a class="btn btn-default btn-sm actionButtonRight" href="edit_filterblock.php?filterblockid=<?= $fb->FilterBlockID ?>">Edit</a></td>
										<td><a class="btn btn-default btn-sm actionButtonRight" href="edit_filterblock.php?action=delete&filterblockid=<?= $fb->FilterBlockID ?>" onclick="return confirm('Are you sure you want to remove this filter?\nThis action cannot be undone.')">Delete</a></td>
									</tr>
								<?php
								$i++;
								}?>
                                </tbody>
                            </table>
									 <a href="edit_filterblock.php?filterid=<?= $filter->FilterID; ?>">Add new filter criteria</a><br/><br/>
									 <input type="submit" value="Update" name="Submit" id="updateButton"/>&nbsp;&nbsp;
									 <input type="submit" value="Cancel" name="Submit"/>&nbsp;&nbsp;
									 <input type="submit" value="Delete" name="Submit" onclick="return confirm('Are you sure you want to delete this filter?\nThis cannot be undone.')" />
                            </form>
                        </div>
                    </div>
                </div>
           </div>
       </div>
   </div>
</div>	
<?php include("components/footer.php") ?>
</body>
</html>