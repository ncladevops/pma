<?php
	require("../printable/include/mysql.inc.php");
	require("../printable/include/optimize.printable.inc.php");
	require("globals.php");
	
	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	
	$portal = new OptimizePortal($COMPANY_ID, $db);
	
	$currentUser = $portal->GetUser($_SESSION['currentuserid']);
		
	$appt = $portal->GetAppointment($_GET['id'], $currentUser->UserID);

  	if($_POST['Submit'] == 'Delete')
	{
		$portal->DeleteAppointment($_GET['id']);
			
		// direct back to manage files
		header("Location: view_appointments.php");
	}

		
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
	<title>
		<?= $portal->CurrentCompany->CompanyName ?> :: Delete Appointment
	</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />		
	<script  src="js/func.js"></script>	
	<?php include("components/bootstrap.php") ?>
</head>
<body bgcolor="#FFFFFF" onUnload="window.opener.getButtonByValue('Update').click()">
	<form method="post" action="<?= $_SERVER['PHP_SELF']. "?id=". $_GET['id']?>">
		<div id="FileDiv" class="sectionDiv" style="width: 100%;">
			<strong>Are you sure you want to delete this Appointment? All 
			information will be lost and can not be recovered.</strong><br/><br/>
			<div class="itemSection">
				<div id="DescriptionDiv">
					<label for="description">Appointment Time:</label>
					<b><?= $appt->AppointmentTime ?></b>
				</div>
			</div>
		</div>
		<center>
		<div class="itemSection">
			<div class="buttonSection">
				<input type="submit" name="Submit" value="Delete"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="button" name="Cancel" value="Cancel" onclick="parent.location='view_appointments.php'"/>
			</div>
		</div>
		</center>
	</form>
</body>
</html>