<?php
	//die("<h3 style='text-align: center;'>Outbound \"Click To Dial\" calls have been temporarily disabled. You will need to manually dial the number of the lead. This feature will be re-activated at 3:00pm</h3>");
	
	require("../printable/include/mysql.inc.php");
	require("../printable/include/optimize.printable.inc.php");
	require("globals.php");
	include_once("../printable/include/component/asterisktransfer.php");
	
	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	
	$portal = new OptimizePortal($COMPANY_ID, $db);
	
	$currentUser = $portal->GetUser($_SESSION['currentuserid']);
		
	if(!$currentUser)
	{
		die("Session Timedout");
	}
	
	$isSubAdmin = $portal->CheckPriv($currentUser->UserID, 'subadmin');
	
	if($isSubAdmin)
	{
		$userString = "all";
	}
	else 
	{
		$userString = $currentUser->UserID;
	}
	
	$lead = $portal->GetOptimizeContact($_GET['id'], $userString);
	
	if(!$lead)
	{
		die("Cannot Find Lead");
	}
		
	$number = $lead->$_GET['field'];
	
	if(strlen($number) <= 0)
	{
		$message = "Call Failed please check Phone Number ($number)";
	}
	else
	{
		$vcSystem = new VocalocityTransferSystem($currentUser->CustomProfileAttributes["Vocalocity User"], $currentUser->CustomProfileAttributes["Vocalocity Pass"]);
			
		$result = $portal->MakeOutboundVocalocityCall($vcSystem, $number, $lead);
		
		if($result->IsError) {
			$message = "Error initiating call: {$result->Details}";
		}
		else {
			$message = "Call initiated: <xmp>{$result->Details}</xmp>";
		}
	}
		
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
	<title>
		Leads on Demand :: Triggering Call to <?= "$lead->FirstName $lead->LastName" ?>
	</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />		
	<link type="text/css" media="all" rel="stylesheet" href="style.css" />	
	<link type="text/css" media="all" rel="stylesheet" href="css/trigger_call.css" />
	<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />
</head>
<body>
	<div class="sectionHeader">
		Triggering Outbound Call...
	</div>
	<div class="errorHeader">
		<?= $message ?>
	</div>
	<div id="TriggerCallDiv" class="sectionDiv">
		<div class="itemSection">
			<div id="ContactNameDiv">
				<label for="ContactName">Lead Name:</label><br/>
				<?= "$lead->FirstName $lead->LastName" ?>
			</div>
		</div>
		<div class="itemSection">
			<div id="ContactNameDiv">
				<label for="ContactName">Lead Phone Number:</label><br/>
				<?= format_phone($number); ?>
			</div>
		</div>
	</div>
	<div class="buttonDiv">
		<div class="buttonSection">
			<input type="button" value="Close Window" onclick="window.close()" />
		</div>
	</div>
</body>
</html>