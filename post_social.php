<?php
require("../printable/include/mysql.inc.php");
require("../printable/include/Optimize.printable.inc.php");
require("globals.php");

$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);

$portal = new OptimizePortal($COMPANY_ID, $db);

$currentUser = $portal->UserAccess($_SESSION['currentuserid']);

// Check login
if (!$currentUser) {
    header("Location: login.php?message=" . urlencode("Not logged in or login error.  Please try again."));
    die();
}

$_GET['section'] = intval($_GET['section']);

// Get Cats
$rootCats = $portal->GetSocialCategories('root', $_GET['section'], true, $currentUser->GroupID);

$cats = array();
$cat = new SocialCategory();
foreach ($rootCats as $cat) {
    $cats[] = $portal->GetSocialCategory($cat->SocialCategoryID, true);
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <title>
            Optimize on Demand :: Post Social
        </title>
        <meta http-equiv="content-type" content="text/html;charset=utf-8" />		
        <meta http-equiv="content-type" content="text/html;charset=utf-8" />	
        <script type='text/javascript' src='../printable/include/js/jquery-1.10.2.min.js'></script>
        <script type='text/javascript' src='../printable/include/js/jquery-ui.js'></script>
        <script  src="js/func.js"></script>
        <script language="JavaScript" type="text/JavaScript">

            $(document).ready(function() {
            // Handler for .ready() called.

            $('#dhtmlgoodies_tree').click(function() {
            $('#socialdetails').show();
            });


            $('#search').keyup(function(e) {
            if($(this).val().length > 2)
            {
            $('#search-notice').hide();
            loadPostings('all');
            $('#socialdetails').show();
            } else {
            $('#search-notice').show();
            }
            });

            $('input[type=search]').on('search', function () {
            if($(this).val() == '')
            {
            $('#search-notice').hide();
            loadPostings('all');
            $('#socialdetails').show();
            }
            });

            });

            var transactionURL = "printable_trans.php";
            var postingsRequest;
            var postingDetailsRequest;
            var nSocialCategoryID;

            function fulltext(){
            $('.full').show();
            $('.tweet').hide();
            }

            function twittertext(){
            $('.full').hide();
            $('.tweet').show();
            }

            function loadTipImage(postingID)
            {
            document.getElementById("toolTipBody").innerHTML = "<img src='loadsocialthumb.php?postingid=" + postingID + "&fitwidth=200'>";
            }

            function searchPostings(socialCategoryID)
            {
            //search if search string is more than 3 characters
            if(document.getElementById('search').value.length > 3)
            {
            loadPostings(socialCategoryID);
            }

            }

            function emptyPostDetails(){
            document.getElementById("socialpreview").innerHTML = "";
            document.getElementById("titleSpan").innerHTML = "";
            document.getElementById("categorySpan").innerHTML = "";
            document.getElementById("messageSpan").innerHTML = "";
            document.getElementById("tweetSpan").innerHTML = "";
            document.getElementById("createdSpan").innerHTML = "";
            document.getElementById("modifiedSpan").innerHTML = "";

            }

            function loadPostings(socialcategoryID)
            {
            nSocialCategoryID = socialcategoryID;
            document.getElementById("sociallist").innerHTML = '<div style="text-align: center; padding-top: 100px;"><img src="images/loading.gif" /><br/>Loading ...</div>';
            emptyPostDetails();

            postingsRequest=GetXmlHttpObject()
            if (postingsRequest==null)
            {
            alert ("Your browser does not support AJAX!");
            return;
            } 
            var url=transactionURL;
            url=url+"?action=GetPostingTable&socialcategoryid=" + socialcategoryID + "&sortBy=" + document.getElementById('sort_by').value + "&search=" +document.getElementById('search').value;
            url=url+"&sid="+Math.random();
            postingsRequest.onreadystatechange=postingsRequestStateChanged;
            postingsRequest.open("GET",url,true);
            postingsRequest.send(null);	

            }

            function postingsRequestStateChanged()
            {
            if (postingsRequest.readyState==4)
            {
            document.getElementById("sociallist").innerHTML = postingsRequest.responseText;
            }	
            }

            function loadPostDetails(socialpostingid) 
            {
            document.getElementById("socialpreview").innerHTML = '&nbsp;<img src="images/loading.gif" />';
            document.getElementById("titleSpan").innerHTML = 'Loading ...';

            postingDetailsRequest=GetXmlHttpObject()
            if (postingDetailsRequest==null)
            {
            alert ("Your browser does not support AJAX!");
            return;
            } 
            var url=transactionURL;
            url=url+"?action=GetPostInfo&socialpostingid=" + socialpostingid;
            url=url+"&sid="+Math.random();
            postingDetailsRequest.onreadystatechange=postingDetailsStateChanged;
            postingDetailsRequest.open("GET",url,true);
            postingDetailsRequest.send(null);
            }

            function postingDetailsStateChanged() {
            if (postingDetailsRequest.readyState==4)
            {
            var xmlDoc=postingDetailsRequest.responseXML.documentElement;

            if(xmlDoc.getElementsByTagName("Status")[0].childNodes[0].nodeValue != "Success") {
            alert("Error Loading social");
            return;
            }

            if(xmlDoc.getElementsByTagName("SocialPostingID")[0].childNodes[0]) {
            socialpostingid = xmlDoc.getElementsByTagName("SocialPostingID")[0].childNodes[0].nodeValue; }
            else {
            socialpostingid =0; }
            if(xmlDoc.getElementsByTagName("PostingName")[0].childNodes[0]) {
            postingname = xmlDoc.getElementsByTagName("PostingName")[0].childNodes[0].nodeValue; }
            else {
            postingname = "";  }
            if(xmlDoc.getElementsByTagName("Message")[0].childNodes[0]) {
            message = xmlDoc.getElementsByTagName("Message")[0].childNodes[0].nodeValue; }
            else {
            message = ""; }
            if(xmlDoc.getElementsByTagName("Tweet")[0].childNodes[0]) {
            tweet = xmlDoc.getElementsByTagName("Tweet")[0].childNodes[0].nodeValue; }
            else {
            tweet = ""; }
            if(xmlDoc.getElementsByTagName("DateCreated")[0].childNodes[0]) {
            datecreated = xmlDoc.getElementsByTagName("DateCreated")[0].childNodes[0].nodeValue; }
            else {
            datecreated = 0; }
            if(xmlDoc.getElementsByTagName("DateModified")[0].childNodes[0]) {
            datemodified = xmlDoc.getElementsByTagName("DateModified")[0].childNodes[0].nodeValue; }
            else {
            datemodified = 0; }
            if(xmlDoc.getElementsByTagName("SocialCategoryID")[0].childNodes[0]) {
            catID = xmlDoc.getElementsByTagName("SocialCategoryID")[0].childNodes[0].nodeValue; }
            else {
            catID=0; }
            if(xmlDoc.getElementsByTagName("CategoryName")[0].childNodes[0]) {
            categoryname = xmlDoc.getElementsByTagName("CategoryName")[0].childNodes[0].nodeValue; }
            else {
            categoryname = ""; }
            if(xmlDoc.getElementsByTagName("Facebook")[0].childNodes[0].nodeValue == 1 || xmlDoc.getElementsByTagName("LinkedIn")[0].childNodes[0].nodeValue == 1) {
            $('#fullDiv').show();
            full = 1;
            }
            else {
            $('#fullDiv').hide();
            full = 0; }
            if(xmlDoc.getElementsByTagName("Twitter")[0].childNodes[0].nodeValue == 1) {
            $('#tweetDiv').show();
            twitter = 1; }
            else {
            $('#tweetDiv').hide();
            twitter = 0;
            }

            document.getElementById("socialpreview").innerHTML = "&nbsp;<img class='img-thumbnail' src='loadsocialthumb.php?postingid=" + socialpostingid + "&thumb=true&fitwidth=100' />";
            document.getElementById("titleSpan").innerHTML = postingname;
            document.getElementById("categorySpan").innerHTML = categoryname;
            document.getElementById("messageSpan").innerHTML = message;
            document.getElementById("tweetSpan").innerHTML = tweet;
            document.getElementById("createdSpan").innerHTML = datecreated;
            document.getElementById("modifiedSpan").innerHTML = datemodified;

            if(full == 1) {
            fulltext();
            }
            else if(full == 0 && twitter == 1) {
            twittertext();
            }

            }
            }

        </script>	

        <?php include("components/bootstrap.php") ?>

        <script type="text/javascript">
            //Custom selected click add class
            $(document).on('click', '.table > tbody > tr', function(e) {
                $('.table > tbody > tr').removeClass('info');
                $(this).addClass('info');

            });
        </script>

        <script type='text/javascript' src='js/socialposting.js'></script>

    </head>
    <body bgcolor="#FFFFFF">
        <div id="page">
            <?php include("components/header.php") ?>
            <div id="body">
                <?php
                $CURRENT_PAGE = "Social";
                include("components/navbar.php");
                ?>
                <?php if (isset($_GET['message'])): ?>
                    <div class="container">
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <?= $_GET['message']; ?>
                        </div>
                    </div>
                <?php endif; ?> 

                <div class="row">
                    <div class="form-group col-md-offset-9 col-md-2">
                        <span id="search-notice" style="display: none;"><small>search must be at least 3 characters</small></span>
                        <input type="search" id="search" name="search" placeholder="Search" class="form-control input-sm"></input>

                        </div>

                    <div class="form-group col-md-1">
                        <?php
                        $aSortOptions = array('PostingName ASC' => 'Title', 'DateCreated DESC' => 'Recently Added', 'DateModified DESC' => 'Recently Modified');
                        ?>
                        <select name="sort_by" class="form-control input-sm" id="sort_by" onchange="loadPostings(nSocialCategoryID)">
                            <option value="">Sort By</option>
                            <?php
                            foreach ($aSortOptions as $sField => $sTitle) {
                                ?>
                                <option value="<?php echo $sField; ?>" <?php if ($_GET['sort_by'] == $sField) echo "SELECTED"; ?>><?php echo $sTitle; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    </div>

                    <div id="socialContainer" class="row foldercontainer">
                    <div id="folderTreeContainer" class="col-md-2 panel panel-default">
                        <ul class="nav nav-pills nav-stacked bs-sidebar">
                            <?php
                            if (!empty($cats)) {
                                $fc = new SocialCategory();
                                foreach ($cats as $fc) {
                                    echo $fc->printNewList();
                                }
                            } else {
                                echo 'No Categories';
                            }
                            ?>
                        </ul>
                    </div>
                    <div class="container col-md-10">
                        <div id="filelistContainer" class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Social Postings</h3>
                            </div>
                            <div id="sociallist" class="panel-body">
                            </div>
                            <div id="socialdetails" class="detailinfo panel-footer" style="display: none;">
                                <div class="row">
                                    <div class="container col-md-12">
                                        <div id="socialpreview" class="col-md-2"></div>
                                            <div id="socialtext" class="col-md-8">
                                                <strong>Title:</strong> <span id="titleSpan"></span><br/>
                                                <strong>Category:</strong> <span id="categorySpan"></span><br/>
                                                <span class="full"><strong>Message:</strong> <span id="messageSpan"></span><br/></span>
                                                <span class="tweet"><strong>Tweet:</strong> <span id="tweetSpan"></span><br/></span>
                                                <strong>Post Creation Date:</strong> <span id="createdSpan"></span><br/>
                                                <strong>Post Last Modified:</strong> <span id="modifiedSpan"></span>
                                            </div>
                                            <div class="detaillinks col-md-2">
                                                <div id="fullDiv" style="float: right; display: none;"><a id="facebookLink" href=#" title="View Full Text" onclick="fulltext();
                                                        return false;"><span class="label label-default">Full Text</span></a></div>
                                                </br>
                                                <div id="tweetDiv" style="float: right; display: none;"><a id="twitterLink" href="#" "View Tweet Text" onclick="twittertext();return false;"><span class="label label-default">Twitter Text</span></a></div>
                                            </div>
                                    </div>		
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="tipDiv" style="position:absolute; visibility:hidden; z-index:100"></div>
            <script language="JavaScript" type="text/JavaScript">
<?= isset($_GET['category']) ? "loadPostings('" . $_GET['category'] . "');" : "" ?>
            </script>

            <?php include("components/footer.php") ?>

    </body>
</html>