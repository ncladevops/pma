var statusupdate_transactionURL = "printable_trans.php";
var contactStatusRequest;

function updateContactStatus(slid, cstid)
{
	document.getElementById("cstid_" + slid).className = "statusType" + cstid;
	contactStatusRequest=GetXmlHttpObject();
	if (contactStatusRequest==null)
	{
		alert ("Your browser does not support AJAX!");
		return;
	} 
	var url=statusupdate_transactionURL;
	url=url+"?action=UpdateContactStatus&slid=" + slid;
	url=url+"&cstid="+cstid;
	url=url+"&sid="+Math.random();
	contactStatusRequest.onreadystatechange=contactStatusRequestStateChanged;
	contactStatusRequest.open("GET",url,true);
	contactStatusRequest.send(null);
}

function contactStatusRequestStateChanged()
{
	if (contactStatusRequest.readyState==4)
	{
		document.getElementById("debug_div").innerHTML = contactStatusRequest.responseText;
		var xmlDoc=contactStatusRequest.responseXML.documentElement;
		
		// Check if session timedout			
		if(xmlDoc.getElementsByTagName("Status")[0].childNodes[0].nodeValue == 'Timeout')
		{
			parent.location="login.php?logout=true&message='Your+Session+has+timedout.+Please+Login+Again.";
			return;
		}
		if(xmlDoc.getElementsByTagName("Status")[0].childNodes[0].nodeValue == 'Fail')
		{
			alert(xmlDoc.getElementsByTagName("StatusDetail")[0].childNodes[0].nodeValue);
			if(xmlDoc.getElementsByTagName("cstid_old").length > 0)
			{
				document.getElementById("cstid_" + xmlDoc.getElementsByTagName("slid")[0].childNodes[0].nodeValue).value 
					= xmlDoc.getElementsByTagName("cstid_old")[0].childNodes[0].nodeValue;
				document.getElementById("cstid_" + xmlDoc.getElementsByTagName("slid")[0].childNodes[0].nodeValue).className 
					= "statusType" + xmlDoc.getElementsByTagName("cstid_old")[0].childNodes[0].nodeValue;
			}
		}	
		else if(xmlDoc.getElementsByTagName("Status")[0].childNodes[0].nodeValue == 'Success')
		{
			if(document.getElementById("owner_" + xmlDoc.getElementsByTagName("slid")[0].childNodes[0].nodeValue)
				&& xmlDoc.getElementsByTagName("owner").length > 0)
			{
				document.getElementById("owner_" + xmlDoc.getElementsByTagName("slid")[0].childNodes[0].nodeValue).innerHTML 
					= xmlDoc.getElementsByTagName("owner")[0].childNodes[0].nodeValue;
			}
		}
		
		 window.location.href=window.location.href;
	}
	
}