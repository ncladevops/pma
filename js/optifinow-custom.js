$(document).ready(function () {

    $("select").chosen({width: '100%'});
    $('[data-toggle="tooltip"]').tooltip();

//trees
    $('.tree-list').hide(); //hide children nodes on startup

//on node or li click
    $('.foldercontainer li :not(.tree-list)').on('click', function () {

        var treelist = '#' + $(this).closest('li').find('ul.tree-list').attr('id'); //get tree list to show
        var icon = $(this).closest('a').find('i.glyphicon'); //get icon of clicked node or li

        if ($(treelist).is(":visible")) { //toggle feature for tree list
            $(treelist).hide();
        }
        else {
            $(treelist).show();
            $('.detailinfo').show();
        }

        //folder icon animation
        if ($(icon).hasClass('glyphicon-folder-close')) {

            $(".foldercontainer ul.bs-sidebar i.glyphicon-folder-open").each(function () {
                $(this).toggleClass('glyphicon-folder-open glyphicon-folder-close');

            });

            $(icon).toggleClass('glyphicon-folder-close glyphicon-folder-open');
        }

    });

//home page

//carousel
    $('.carousel').carousel();

//datepicker
    $('.datepicker').datepicker({
        showButtonPanel: true,
        showOtherMonths: true,
        selectOtherMonths: true,
        changeMonth: true,
        changeYear: true,
        yearRange: '-100y:c+nn',
        onChangeMonthYear: function (year, month, inst) {
            var curDate = $(this).datepicker("getDate");
            if (curDate == null)
                return;
            if (curDate.getYear() != year || curDate.getMonth() != month - 1) {
                curDate.setYear(year);
                curDate.setMonth(month - 1);
                $(this).datepicker("setDate", curDate);
            }
        }
    });


    //datepicker
    $('.birth-datepicker').datepicker({
        showButtonPanel: true,
        showOtherMonths: true,
        selectOtherMonths: true,
        changeMonth: true,
        changeYear: true,
        yearRange: '-100y:c+nn',
        dateFormat: 'mm/dd/yy',
        onChangeMonthYear: function (year, month, inst) {
            var curDate = $(this).datepicker("getDate");
            if (curDate == null)
                return;
            if (curDate.getYear() != year || curDate.getMonth() != month - 1) {
                curDate.setYear(year);
                curDate.setMonth(month - 1);
                $(this).datepicker("setDate", curDate);
            }
            $(this).change();
        }
    });

    //rich text editor
    $('.cleditor').cleditor();

//edituser
    $('#RegionID').change(function (e) {

        var RegionID = $(this).val(); //get selected RegionID
        var DivisionID = $('#DivisionID').val(); //get division id

        $.post('./json/regionanddivision.php', {'function': 'getdivision'}, function () {
        }).done(function (data) {

            var result = jQuery.parseJSON(data);

            $('#DivisionID option:gt(0)').remove(); //remove select except on top

            $.each(result, function (key, val) {
                if (RegionID == '') {
                    $('#DivisionID').append($("<option></option>").attr("value", val.DivisionID).text(val.DivisionName));
                } else if (val.RegionID == RegionID) {
                    if (val.DivisionID == DivisionID) {
                        $('#DivisionID').append($("<option selected></option>").attr("value", val.DivisionID).text(val.DivisionName));
                    }
                    else {
                        $('#DivisionID').append($("<option></option>").attr("value", val.DivisionID).text(val.DivisionName));
                    }
                }
            }); //append smart dropdowns
        });

    });

    $('#DivisionID').change(function (e) {

        if ($('#RegionID').val() == '') {

            var DivisionID = $(this).val(); //get selected DivisionID

            $.post('./json/regionanddivision.php', {'function': 'getdivision'}, function () {
            }).done(function (data) {

                var result = jQuery.parseJSON(data);

                $.each(result, function (key, val) {
                    if (val.DivisionID == DivisionID) {
                        $('#RegionID').val(val.RegionID).change();

                    }
                }); //select region for selected division
            });
        }

    });

    $('#ChangePassButton').click(function (e) {

        e.preventDefault();

        $(this).hide();

        $('.password').show();

    });


    checkaddresses();


    $('#copyaddress').change(function () {

        if ($('#copyaddress').is(':checked')) {

            copyaddress();

        }
    });

    $('.address').keypress(function () {
        if ($('#copyaddress').is(':checked')) {
            copyaddress();
        }

    });

    $('.address').keyup(function () {
        if ($('#copyaddress').is(':checked')) {
            copyaddress();
        }

    });

    $('input.copyfield').change(function (e) {
        $("input[name=" + $(this).attr('name') + "].copyfield").val($(this).val());
    });

    $('input.copyfield').keyup(function (e) {
        $("input[name=" + $(this).attr('name') + "].copyfield").val($(this).val());
    });

    $('textarea.copyfield').change(function (e) {
        $("textarea[name=" + $(this).attr('name') + "].copyfield").val($(this).val());
    });

    $('textarea.copyfield').keyup(function (e) {
        $("textarea[name=" + $(this).attr('name') + "].copyfield").val($(this).val());
    });

    if ($('#lead-listing').length) {
        $('#lead-listing').DataTable({
            "iDisplayLength": 25,
            "pagingType": "full_numbers",
            "columnDefs": [
                {orderable: false, targets: [0, -1]}
            ],
            "deferRender": true,
            "stateSave": true
        });
        $('#loading-table').hide();
        $('#lead-listing').show();
    }

});

/* Always force submit on Custom Buttons */
$(document).on('click', '.btn-custom', function (e) {
    var form = $('#lead-detail-form');
    var button = $(this);
    var customJS = button.attr('popup');
    var customURL = button.attr('href');

    //add status message
    $('.page-status').text('Updating Lead. Please Wait.');

    //disable the button first
    button.attr('disabled', 'disabled');
    e.preventDefault();
    $.ajax({
        url: form.attr('action'),
        type: 'POST',
        data: form.serialize() + "&Submit=Update",
        success: function (result) {
            if (customJS != '') {
                console.log(customJS);
                $.globalEval(customJS);
                window.location = form.attr('action');
            }
            if (customURL != '#') {
                window.location = customURL;
            }
            //enable the button afterwards
            button.removeAttr('disabled');

            //add status message
            $('.page-status').text('');
        },
    });
});

function copyaddress() {

    $(".returnaddress").each(function () {
        //if($(this).val() == ""){
        var id = $(this).attr('id');
        addressid = id.replace("Other", "");

        $(this).val($('#' + addressid).val());
        //}
    });

}

function checkaddresses() {

    $count = 0;
    $(".returnaddress").each(function () {
        var id = $(this).attr('id');
        addressid = id.replace("Other", "");
        if ($(this).val() == $('#' + addressid).val()) {
            $count++;
        }
    });

    if ($count == 4) {
        $('#copyaddress').attr('checked', 'checked');
    }

    $('#CancelButton').click(function (e) {

        e.preventDefault();

    });

}