
function GetXmlHttpObject()
{
  var xmlHttp=null;
  try
    {
    // Firefox, Opera 8.0+, Safari
    xmlHttp=new XMLHttpRequest();
    }
  catch (e)
    {
    // Internet Explorer
    try
      {
      xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
      }
    catch (e)
      {
      xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    }
  return xmlHttp;
}

function wopen(url, name, popW, popH)
  {
  	if (document.all) {
    	/* the following is only available after onLoad */
    	w = document.body.clientWidth;
    	h = document.body.clientHeight;
    	x = window.screenLeft;
    	y = window.screenTop;
  	}
  	else{
	    w = window.innerWidth;
	    h = window.innerHeight;
	    x = window.screenX;
	    y = window.screenY;
  	}
  	var leftPos = ((w-popW)/2)+x, topPos = ((h-popH)/2)+y;
  	//alert('width='+popW+',height='+popH+',top='+topPos+',left='+leftPos);
  	newwindow = window.open(url,name,'width='+popW+',height='+popH+',top='+topPos+',left='+leftPos);
  	if (window.focus) {newwindow.focus()};
  }
function wopenscroll(url, name, popW, popH)
{
	if (document.all) {
  	/* the following is only available after onLoad */
  	w = document.body.clientWidth;
  	h = document.body.clientHeight;
  	x = window.screenLeft;
  	y = window.screenTop;
	}
	else{
	    w = window.innerWidth;
	    h = window.innerHeight;
	    x = window.screenX;
	    y = window.screenY;
	}
	var leftPos = ((w-popW)/2)+x, topPos = ((h-popH)/2)+y;
	//alert('width='+popW+',height='+popH+',top='+topPos+',left='+leftPos);
	newwindow = window.open(url,name,'scrollbars=1,width='+popW+',height='+popH+',top='+topPos+',left='+leftPos);
	if (window.focus) {newwindow.focus()};
}

function timedRefresh(timeoutPeriod) 
{
	setTimeout("location.reload(true);",timeoutPeriod * 1000);
}
