$(document).ready(function() {

//getting of meta tag - reference url

$('#referenceurl').change(function() {

var url = $(this).val();

if(url != ''){

var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
  '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
  '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
  '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
  '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
  '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
  if(pattern.test(url)) {
    	$.get(url, 
	function(data) {
   	console.log($(data).find('meta[name=adescription]').attr("content"));
	});
  }

}

});
  


/*
$.get(url, 
function(data) {
   $(data).find('meta[name=adescription]').attr("content");
});
*/

//on loads
    if ($('#twitterbox').is(":checked")) {
        $('.tweetclass').show();
    }
    else {
        $('.tweetclass').hide();
    }

        if ($('#facebookbox').is(":checked")) {
            $('#fbbtn').show();
        }
        else {
            $('#fbbtn').hide();
        }

        if ($('#linkedinbox').is(":checked")) {
            $('#libtn').show();
        }
        else {
            $('#libtn').hide();
        }

$('#charcount').html($('#tweetarea').val().length);


    $('#twitterbox').change(function() {

        if ($(this).is(":checked")) {
            $('.tweetclass').show();
        }
        else {
            $('.tweetclass').hide();
        }
    });

    $('#facebookbox').change(function() {

        if ($(this).is(":checked")) {
            $('#fbbtn').show();
        }
        else {
            $('#fbbtn').hide();
        }
    });

    $('#linkedinbox').change(function() {

        if ($(this).is(":checked")) {
            $('#libtn').show();
        }
        else {
            $('#libtn').hide();
        }
    });

    $('#messagearea').bind('input properychange', function() {

            $('#tweetarea').val($(this).val().substring(0,140));
	     $('#charcount').html($('#tweetarea').val().length);
    });

    $('#tweetarea').bind('input properychange', function() {

            $('#charcount').html($(this).val().length);
    });

    $('#fbbtn').click(function(e) {

	e.preventDefault();

	var message = $('#messagearea').val();

       $.post('preview_fb.php', { message : message }, function(data) {
  	  var win=window.open('preview_fb.php','Preview FB','location=no,top=50%,left=50%,width=400,height=300');
    		with(win.document)
    		{
      		open();
      		write(data);
      		close();
    		}
	});

	});

    $('#libtn').click(function(e) {

	e.preventDefault();

	var message = $('#messagearea').val();

       $.post('preview_linkedin.php', { message : message }, function(data) {
  	  var win=window.open('preview_linkedin.php','Preview LinkedIn','location=no,top=50%,left=50%,width=400,height=300');
    		with(win.document)
    		{
      		open();
      		write(data);
      		close();
    		}
	});

	});

    $('#ttbtn').click(function(e) {

	e.preventDefault();

	var tweet = $('#tweetarea').val();
	var charcount = 140 - parseInt($('#charcount').text());

       $.post('preview_tweet.php', { tweet : tweet, charcount : charcount }, function(data) {
  	  var win=window.open('preview_tweet.php','Preview Twitter','location=no,top=50%,left=50%,width=400,height=300');
    		with(win.document)
    		{
      		open();
      		write(data);
      		close();
    		}
	});

	});


});