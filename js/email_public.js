var logpublicemail_transactionURL = "printable_trans.php";
var logPublicEmailRequest;

function TriggerPublicContentEmail(email, slid, fileid, publicLink, emailSubject, emailBody)
{
	if(slid != 0) {
		logPublicEmailRequest=GetXmlHttpObject();
		if (logPublicEmailRequest==null)
		{
			alert ("Your browser does not support AJAX!");
			return;
		} 
		var url=logpublicemail_transactionURL;
		url=url+"?action=LogPublicContentEmail&slid=" + slid;
		url=url+"&fileid="+fileid;
		url=url+"&sid="+Math.random();
		logPublicEmailRequest.onreadystatechange=logPublicEmailRequestStateChanged;
		logPublicEmailRequest.open("GET",url,true);
		logPublicEmailRequest.send(null);
	}
	
	window.open("mailto:" + email + "?subject=" + encodeURIComponent(emailSubject) + "&body=" + encodeURIComponent(emailBody + "\n\n" + publicLink));
}

function logPublicEmailRequestStateChanged()
{
	if (logPublicEmailRequest.readyState==4)
	{
		document.getElementById("debug_div").innerHTML = logPublicEmailRequest.responseText;
		var xmlDoc=logPublicEmailRequest.responseXML.documentElement;
		
		// Check if session timedout			
		if(xmlDoc.getElementsByTagName("Status")[0].childNodes[0].nodeValue == 'Timeout')
		{
			parent.location="login.php?logout=true&message='Your+Session+has+timedout.+Please+Login+Again.";
			return;
		}
		if(xmlDoc.getElementsByTagName("Status")[0].childNodes[0].nodeValue == 'Fail')
		{
			alert(xmlDoc.getElementsByTagName("StatusDetail")[0].childNodes[0].nodeValue);
		}
	}
	
}