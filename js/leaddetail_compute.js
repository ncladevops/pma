/**
 * Created by niknok on 9/9/2015.
 */
/* Field Computations */
function agecompute(dobfield, agefield) {
    var dob = new Date($(dobfield).val());
    var today = new Date();
    var age = Math.floor((today - dob) / (365.25 * 24 * 60 * 60 * 1000));
    if (!isNaN(age)) {
        if (age > 0) {
            $(agefield).val(age);
        } else {
            $(agefield).val(0);
        }
    }
}

function incomecompute(incomefield, otherfield, totalfield) {
    var income = $(incomefield).val() ? $(incomefield).val() : 0;
    var other = $(otherfield).val() ? $(otherfield).val() : 0;
    var total = parseFloat(income) + parseFloat(other);
    if (!isNaN(total) && total != 0) {
        $(totalfield).val(total);
    }
}

function copyvalue(source_field, copy_field) {
    var value = $(source_field).val();
    $(copy_field).val(value);
}

// A $( document ).ready() block.
$(document).ready(function () {
    agecompute('.dobcompute', '.agecompute');
    agecompute('.codobcompute', '.coagecompute');
    incomecompute('.netcompute', '.othercompute', '.totalcompute');
    incomecompute('.conetcompute', '.coothercompute', '.cototalcompute');
});

$(document).on('change', '.dobcompute', function () {
    agecompute('.dobcompute', '.agecompute');
    copyvalue('.dobcompute', '.dobcopy');
});

$(document).on('change', '.codobcompute', function () {
    agecompute('.codobcompute', '.coagecompute');
    copyvalue('.codobcompute', '.codobcopy');
});

$(document).on('change', '.netcompute', function () {
    incomecompute('.netcompute', '.othercompute', '.totalcompute');
});

$(document).on('change', '.othercompute', function () {
    incomecompute('.netcompute', '.othercompute', '.totalcompute');
});

$(document).on('change', '.conetcompute', function () {
    incomecompute('.conetcompute', '.coothercompute', '.cototalcompute');
});

$(document).on('change', '.coothercompute', function () {
    incomecompute('.conetcompute', '.coothercompute', '.cototalcompute');
});