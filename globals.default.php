<?php
//OptifiNow Global Settings array from printable.inc
//features
$optifinowGlobalSetting['feature']['LeadsOnDemand'] = TRUE;
$optifinowGlobalSetting['feature']['SalesOnDemand'] = FALSE;
$optifinowGlobalSetting['feature']['MarketingOnDemand'] = FALSE;
$optifinowGlobalSetting['feature']['ContentOnDemand'] = FALSE;
$optifinowGlobalSetting['feature']['SocialOnDemand'] = FALSE;
$optifinowGlobalSetting['feature']['CollaborateOnDemand'] = FALSE;
$optifinowGlobalSetting['feature']['ReportsOnDemand'] = TRUE;
$optifinowGlobalSetting['feature']['SMSOnDemand'] = FALSE;
$optifinowGlobalSetting['feature']['LeadStore'] = TRUE;
$optifinowGlobalSetting['custom']['Opportunity'] = FALSE;
$optifinowGlobalSetting['custom']['Clone'] = FALSE;
$optifinowGlobalSetting['feature']['MortgageRSS'] = TRUE;
//data
$optifinowGlobalSetting['data']['mortgage'] = TRUE;
$optifinowGlobalSetting['data']['pharmacy'] = FALSE;
//custom
$optifinowGlobalSetting['custom']['disabledStatus'] = TRUE;
$optifinowGlobalSetting['view']['discovery'] = TRUE;

//custom buttons
$CUSTOM_BUTTONS = array(
		'CreditAPI' => array('title' => 'Credit API', 'url' => 'webservice/creditapi_post.php?lid=', 'icon' => 'glyphicon glyphicon-credit-card', 'lead_id' => true, 'popup' => false),
		'CAPIncoming' => array('title' => 'Cap Incoming', 'url' => 'webservice/capapi_post.php?lid=', 'icon' => 'glyphicon glyphicon-arrow-right', 'lead_id' => true, 'popup' => true, 'field' => 'LoanNumber', 'field_label' => 'CAP ID')
);

//View columns
$leadCols = array('<!-- Checkbox -->&nbsp;' => 'nosort', 'ID' => 'submissionlisting.SubmissionListingID', 'Date Added' => 'DateAdded', 'Company' => 'Company', 'First Name' => 'FirstName',
		'Last Name' => 'LastName', 'Work Phone' => 'nosort', 'Cell Phone' => 'nosort',
		'State' => 'State', 'Status' => 'contactstatustype.SortOrder', 'Contact Event' => 'ContactEventTypeID',
		'Last Action Date' => 'submissionlisting.LastTouched', '&nbsp' => 'nosort'
);

$bottleCols = array('<!-- Checkbox -->&nbsp;' => 'nosrt', 'ID' => 'submissionlisting.SubmissionListingID', 'Date Added' => 'DateAdded', 'Date Assigned' => 'DateAssigned', 'First Name' => 'FirstName',
		'Last Name' => 'LastName',
		'State' => 'State', 'Loan Officer' => 'userFullname', 'Status' => 'contactstatustype.SortOrder', 'Last Action Date' => 'submissionlisting.LastTouched',
		'Calls' => 'nosort', 'Campaign' => 'nosort', 'Estimated Credit Debt' => 'Mortgage1Balance', '&nbsp' => 'nosort'
);

$contractCols = array('<!-- Checkbox -->&nbsp;' => 'nosort', 'ID' => 'submissionlisting.SubmissionListingID', 'Date Added' => 'DateAdded', 'Date Assigned' => 'DateAssigned', 'Mailer Code' => 'ExternalID', 'First Name' => 'FirstName',
		'Last Name' => 'LastName', 'Phone' => 'Phone', 'Work Phone' => 'nosort', 'Mobile Phone' => 'nosort',
		'ST' => 'State', 'Next Appt Time' => 'nosort', 'Contact Event' => 'ContactEventTypeID', 'Status' => 'contactstatustype.SortOrder',
		'Last Action Date' => 'submissionlisting.LastTouched', '&nbsp' => 'nosort'
);

$filterCols = array('<!-- Checkbox -->&nbsp;' => 'nosort', 'ID' => 'submissionlisting.SubmissionListingID', 'Date Added' => 'DateAdded', 'Date Assigned' => 'DateAssigned', 'Mailer Code' => 'ExternalID', 'First Name' => 'FirstName',
		'Last Name' => 'LastName', 'Phone' => 'Phone', 'Work Phone' => 'nosort', 'Mobile Phone' => 'nosort',
		'ST' => 'State', 'Contact Event' => 'ContactEventTypeID', 'Status' => 'contactstatustype.SortOrder', '2nd Voice Manager' => 'SecondVoiceManager', '2nd Voice Queued' => 'SecondVoiceQueue',
		'Last Action Date' => 'submissionlisting.LastTouched', '&nbsp' => 'nosort'
);

//views (url and array name should be the same)
$LISTING_VIEWS = array(
		'sales' => array('label' => 'Lead View', 'url' => 'sales', 'enabled' => false, 'default' => false, 'manager-only' => false, 'columns' => $leadCols),
		'contract' => array('label' => 'Contract View', 'url' => 'contract', 'enabled' => true, 'default' => true, 'manager-only' => false, 'columns' => $contractCols),
		'bottle' => array('label' => 'Bottle View', 'url' => 'bottle', 'enabled' => true, 'default' => false, 'manager-only' => true, 'columns' => $bottleCols)
);

$COMPANY_DEFAULT_VIEW = 'filter21';

$DEFAULT_PASSWORD = "greenlink";


$HTTP_GET_VARS = $_GET;
$HTTP_POST_VARS = $_POST;

if($_SERVER['SERVER_NAME'] == 'www.optifinow.com')
{
	$pathFolders = explode("/", $_SERVER['PHP_SELF']);
	session_name($pathFolders[1]);
}
session_start();

$dbhost = "10.60.100.36";
$dbuser = "nclauser";
$dbpass = "nclainc";
$dbdatabase = "printable";

$COMPANY_ID = 46;

$LOGOUT_STRING = "logout.php";
$STORE_PW = "optimize";
$MAIN_STORE_ID = 31;
$CONTACTEVENT_EMAIL = 80;

$LEADSTORE_USERID = 51595;
$LEADSTORE_STATUSID = 660;

$NEW_STATUSTYPE_ID = 625;
$CONTACTED_STATUSTYPE_ID = 663;

$ADDSEARCH_OPTIONS = array( 'Lead Store Transfer' => array(
		'Type' => 'Transfer', 'TransferStatusID' => 624),
		'Lead Store' => array( 'Type' => 'Store', 'StoreStatusID' => 660 ));

$SSO = 1;
$SSO_TOKEN = "DN39LjskX39sxjKA93mA";
$SSO_URL = "https://services.printable.com/TRANS/0.9/SSO.asmx?wsdl";

//	$SSO = 1;
//	$SSO_TOKEN = "17F21FDFE90D75DE4EF525E73FA7980D";
//	$SSO_URL = "https://services.printable.com/TRANS/0.9/SSO.asmx?wsdl";

$SWITCHVOX_TRANSFER_URL = "70.165.48.90";
$SWITCHVOX_TRANSFER_USER = "admin";
$SWITCHVOX_TRANSFER_PASS = "demo";

$FILES_LOCATION = "C:/Inetpub/printablefiles/";

$BB_LINK = "121communication.com/optifinow/forum/";
$LOAN_COMP_URL = "https://sandbox1.gdrhosting.com/optimizeod/loancomp/ovaGetRefiProposal_10.aspx";

$fileSectionNames = array("My Files");

$CONTACTSTATUSID_NEW = 129;

$LEADTRANFERSTATUSID = 141;

$EXACT_TARGET_URL = "https://webservice.s4.exacttarget.com/etframework.wsdl";
$EXACT_TARGET_USER = "exacttargetapi@121communication.com";
$EXACT_TARGET_PASS = "ncla1nc!";

$FACEBOOK_APPID = '196445750511654';
$FACEBOOK_SECRET = 'e66e4f2c1b3df19da31c18b1cfda3254';

$TWITTER_KEY = 'pfbX38YXWR8un06jmUsaQ';
$TWITTER_SECRET = 'KUjPgnxXY44HXdpPDkPnFii4wsEr52nENYtdgGOFrA';
$TWITTER_BEARER = 'AAAAAAAAAAAAAAAAAAAAAMElRwAAAAAAhv8REr5uvh%2F81UeNO8LZyX00sdU%3DLqBlJh6a2XsBDaQWI4eWKaLu1zsVBb4GSNChV6WW0';

$LINKEDIN_KEY = 'i2mlxse5wahp';
$LINKEDIN_SECRET = 'vRzUUKHKcYPBtjV6';

$SOCIAL_IMG_URL = "http://www.optifinow.com/printable/getsocialimage.php?postingid=";

function build_get_query($getArray)
{
	$tempArray = array();
	foreach($getArray as $k=>$v)
	{
		$tempArray[] = "$k=$v";
	}
	return implode("&", $tempArray);
}

function debug_var_dump($var)
{
	echo "<xmp>";
	var_dump($var);
	echo "</xmp>";
}

function format_phone($phone)
{
	$strip_phone = str_replace(array(" ", "-", "(", ")"), "", $phone);

	if(strlen($strip_phone) != 10)
	{
		return $phone;
	}

	return substr($strip_phone, 0, 3) . "-" .
	substr($strip_phone, 3, 3) . "-" .
	substr($strip_phone, 6);
}

function build_page_string($page, $getArray, $count, $offset, $max)
{
	if($count == 0 || $max == 0)
	{
		return "";
	}

	$offset = intval($offset);

	if($offset < 0)
	{
		$offset = 0;
	}

	// Setup navigation links
	if($offset > 0)
	{
		$tempGet = $getArray;
		$tempGet['offset'] = 0;
		$tempGetString = build_get_query($tempGet);
		$prevLink = "<li><a href='$page?$tempGetString'>&lt;&lt; Start</a></li>";

		$prevOffset = $offset - $count;
		$tempGet = $getArray;
		$tempGet['offset'] = $prevOffset;
		$tempGetString = build_get_query($tempGet);
		$prevLink .= "<li><a href='$page?$tempGetString'>&lt; Prev</a></li>";
	}
	else
	{
		$prevLink = "";
	}

	if($offset + $count < $max)
	{
		$tempGet = $getArray;
		$nextOffset = $offset + $count;
		$tempGet['offset'] = $nextOffset;
		$tempGetString = build_get_query($tempGet);
		$nextLink = "&nbsp;&nbsp;<li><a href='$page?$tempGetString'>Next &gt;</a></li>";

		$tempGet = $getArray;
		$lastOffset = $max - $count;
		$tempGet['offset'] = $lastOffset;
		$tempGetString = build_get_query($tempGet);
		$nextLink .= "&nbsp;&nbsp;<li><a href='$page?$tempGetString'>Last &gt;&gt;</a></li>";
	}
	else
	{
		$nextLink = "";
	}

	$currentPage = floor($offset/$count) + 1;
	$lastPage = $max == $count ? 1 : floor($max/$count) + 1;

	//$pageLink = "Page: ";
	$pageLink = "";

	for($i = 1; $i <= $lastPage; $i++)
	{
		if($i == $currentPage)
		{
			//$pageLink .= "$currentPage ";
			$pageLink = "<li class='active'><a href='#'>". $currentPage . "</a><li>";
		}
		else if($i == 1)
		{
			$tempGet = $getArray;
			$tempGet['offset'] = 0;
			$tempGetString = build_get_query($tempGet);
			$pageLink .= "<li><a href='$page?$tempGetString'>1</a></li>";
		}
		else if($i == $lastPage)
		{
			$tempGet = $getArray;
			$tempGet['offset'] = (($lastPage - 1) * $count);
			$tempGetString = build_get_query($tempGet);
			$pageLink .= "<li><a href='$page?$tempGetString'>$lastPage</a></li>";
		}
		else if($currentPage - 2 == $i)
		{
			$tempGet = $getArray;
			$tempGet['offset'] = (($currentPage - 3) * $count);
			$tempGetString = build_get_query($tempGet);
			$pageLink .= "<li><a href='$page?$tempGetString'>" . ($currentPage - 2) . "</a></li>";
		}
		else if($currentPage - 1 == $i)
		{
			$tempGet = $getArray;
			$tempGet['offset'] = (($currentPage - 2) * $count);
			$tempGetString = build_get_query($tempGet);
			$pageLink .= "<li><a href='$page?$tempGetString'>" . ($currentPage - 1) . "</a></li>";
		}
		else if($currentPage + 1 == $i)
		{
			$tempGet = $getArray;
			$tempGet['offset'] = (($currentPage) * $count);
			$tempGetString = build_get_query($tempGet);
			$pageLink .= "<li><a href='$page?$tempGetString'>" . ($currentPage + 1) . "</a></li>";
		}
		else if($currentPage + 2 == $i)
		{
			$tempGet = $getArray;
			$tempGet['offset'] = (($currentPage + 1) * $count);
			$tempGetString = build_get_query($tempGet);
			$pageLink .= "<li><a href='$page?$tempGetString'>" . ($currentPage + 2) . "</a></li>";
		}
		else if ($i == 2 && $currentPage > 4)
		{
			$pageLink .= "<li><a href='#'>...</a></li>";
		}
		else if ($i == 4 && $currentPage <= 1)
		{
			$tempGet = $getArray;
			$tempGet['offset'] = ((3) * $count);
			$tempGetString = build_get_query($tempGet);
			$pageLink .= "<li><a href='$page?$tempGetString'>4</a></li>";
		}
		else if ($i == 5 && $currentPage <= 2)
		{
			$tempGet = $getArray;
			$tempGet['offset'] = ((4) * $count);
			$tempGetString = build_get_query($tempGet);
			$pageLink .= "<li><a href='$page?$tempGetString'>5</a></li>";
		}
		else if ($i == $lastPage - 1  && $currentPage < $lastPage - 3)
		{
			$pageLink .= "<li><a href='#'>...</a></li>";
		}
		else if ($i == $lastPage - 3 && $currentPage >= $lastPage)
		{
			$tempGet = $getArray;
			$tempGet['offset'] = (($lastPage - 4) * $count);
			$tempGetString = build_get_query($tempGet);
			$pageLink .= "<li><a href='$page?$tempGetString'>" . ($lastPage - 3) . "</a></li>";
		}
		else if ($i == $lastPage - 4 && $currentPage >= $lastPage -1)
		{
			$tempGet = $getArray;
			$tempGet['offset'] = (($lastPage - 5) * $count);
			$tempGetString = build_get_query($tempGet);
			$pageLink .= "<li><a href='$page?$tempGetString'>" . ($lastPage - 4) . "</a></li>";
		}
	}
	return $prevLink . " " . $pageLink . " " . $nextLink;
}

function getDateFromWorkingDays($startDate, $days)
{
	$start = strtotime($startDate);

	if(!$start)
	{
		return false;
	}


	if($days >= 0)
	{
		$result = $start + floor(intval($days)/5) * 604800;
		if(date("N", $result) == 6)
		{
			$result = $result + ($days % 5 + ($days % 5 == 0 ? 2 : 1)) * 86400;
		}
		elseif(date("N", $result) == 7)
		{
			$result = $result + ($days % 5 + ($days % 5 == 0 ? 1 : 0)) * 86400;
		}
		elseif( ($days % 5 + date("N", $result)) >= 6)
		{
			$result = $result + ($days % 5 + 2) * 86400;
		}
		else
		{
			$result = $result + ($days % 5) * 86400;
		}
	}
	else
	{
		$days = abs($days);
		$result = $start - floor(intval($days)/5) * 604800;
		if(date("N", $result) == 6)
		{
			$result = $result - ($days % 5 + ($days % 5 == 0 ? 1 : 0)) * 86400;
		}
		elseif(date("N", $result) == 7)
		{
			$result = $result - ($days % 5 + ($days % 5 == 0 ? 2 : 1)) * 86400;
		}
		elseif( (date("N", $result) - $days % 5) < 1)
		{
			$result = $result - ($days % 5 + 2) * 86400;
		}
		else
		{
			$result = $result - ($days % 5) * 86400;
		}
	}

	return date("Y-m-d", $result);
}

function getWorkingDays($startDate,$endDate,$holidays)
{
	//The total number of days between the two dates. We compute the no. of seconds and divide it to 60*60*24
	//We add one to inlude both dates in the interval.
	$days = (strtotime($endDate) - strtotime($startDate)) / 86400 + 1;

	$no_full_weeks = floor($days / 7);
	$no_remaining_days = fmod($days, 7);

	//It will return 1 if it's Monday,.. ,7 for Sunday
	$the_first_day_of_week = date("N",strtotime($startDate));
	$the_last_day_of_week = date("N",strtotime($endDate));

	//---->The two can be equal in leap years when february has 29 days, the equal sign is added here
	//In the first case the whole interval is within a week, in the second case the interval falls in two weeks.
	if ($the_first_day_of_week <= $the_last_day_of_week){
		if ($the_first_day_of_week <= 6 && 6 <= $the_last_day_of_week) $no_remaining_days--;
		if ($the_first_day_of_week <= 7 && 7 <= $the_last_day_of_week) $no_remaining_days--;
	}
	else{
		if ($the_first_day_of_week <= 6) {
			//In the case when the interval falls in two weeks, there will be a weekend for sure
			$no_remaining_days = $no_remaining_days - 2;
		}
	}

	//The no. of business days is: (number of weeks between the two dates) * (5 working days) + the remainder
	//---->february in none leap years gave a remainder of 0 but still calculated weekends between first and last day, this is one way to fix it
	$workingDays = $no_full_weeks * 5;
	if ($no_remaining_days > 0 )
	{
		$workingDays += $no_remaining_days;
	}

	if(is_array($holidays))
	{
		//We subtract the holidays
		foreach($holidays as $holiday){
			$time_stamp=strtotime($holiday);
			//If the holiday doesn't fall in weekend
			if (strtotime($startDate) <= $time_stamp && $time_stamp <= strtotime($endDate) && date("N",$time_stamp) != 6 && date("N",$time_stamp) != 7)
				$workingDays--;
		}
	}

	return $workingDays;
}
?>
GIG