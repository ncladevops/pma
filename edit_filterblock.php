<?php
	require("../printable/include/mysql.inc.php");
	require("../printable/include/leadsondemand.printable.inc.php");
	require("../printable/include/optimize.printable.inc.php");
	require("globals.php");
	
	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	
	$portal = new LeadsOnDemandPortal($COMPANY_ID, $db);
	
	
	$contactFields = new LeadsOnDemandFilterFields();
	$contact = new LeadsOnDemandContact();
	$dateFields = $contact->DATE_FIELDS;
	
	$currentUser = $portal->GetUser($_SESSION['currentuserid']);
	
	$isSubAdmin = $portal->CheckPriv($currentUser->UserID, 'subadmin');
				
	if(!$isSubAdmin)
	{
		header( "Location: " . $portal->CurrentCompany->Website . "home.php?message=" . urlencode( "Accessed Denied." ) );
		die();
	}
	
	if(isset($_GET['filterid'])) {
		$filterBlock = new FilterBlock();
		$filterBlock->FilterBlockID = 0;
		$filterBlock->FilterID = $_GET['filterid'];
	}
	else {
		$filterBlock = $portal->GetFilterBlock($_GET['filterblockid']);
	}
			
	if(!$filterBlock)
	{
		header("Location: manage_filters.php?message=" . urlencode("Invalid Filter ID"));
		die();
	}
	
	if($_POST['Submit'] == 'Cancel')
	{
		header("Location: edit_filter.php?filterid={$filterBlock->FilterID}&message=" . urlencode("Action Canceled. Filter Block not updated."));
		die();
	}
	elseif($_GET['action'] == 'delete') {
		$portal->DeleteFilterBlock($filterBlock->FilterBlockID);
		
		header("Location: edit_filter.php?filterid={$filterBlock->FilterID}&message=" . urlencode("Action Complete. Filter Block has been deleted."));
		die();
	}
	elseif($_GET['action'] == 'up') {
		$portal->MoveFilterBlock($filterBlock->FilterBlockID, 'up');
		
		header("Location: edit_filter.php?filterid={$filterBlock->FilterID}&message=" . urlencode("Action Complete. Filter Block has been moved."));
		die();
	}
	elseif($_GET['action'] == 'down') {
		$portal->MoveFilterBlock($filterBlock->FilterBlockID, 'down');
		
		header("Location: edit_filter.php?filterid={$filterBlock->FilterID}&message=" . urlencode("Action Complete. Filter Block has been moved."));
		die();
	}
	elseif($_POST['Submit'] == 'Update')
	{
		if(isset($_GET['filterid'])) {
			$fbID = $portal->CreateFilterBlock($_GET['filterid']);
			
			$filterBlock = $portal->GetFilterBlock($fbID);
		}
		
		$filterBlock->FilterFields = array();
		foreach($_POST["LeftValue"] as $k=>$v) {
			$ff = new FilterField();
			$ff->LeftValue = $v;
			$ff->CompareValue = $_POST['CompareValue'][$k];
			$ff->RightValue = $_POST['RightValue'][$k];
			
			$filterBlock->FilterFields[] = $ff;
		}
		
		$filterBlock->SortFields = array();
		foreach($_POST["SortField"] as $k=>$v) {
			$sf = new SortField();
			$sf->Field = $k;
			$sf->Direction = $v;
			
			$filterBlock->SortFields[] = $sf;
		}
		
		$filterBlock->FilterLimit = $_POST['FilterLimit'];
		
		$portal->UpdateFilterBlock($filterBlock);
				
		header("Location: edit_filter.php?filterid={$filterBlock->FilterID}&message=" . urlencode("Action Successful. Filter updated."));
		die();
	}
	
  	// Create array of fields that are Lookup driven
	$lfs = $portal->GetLookupFieldNames();
	$lookupFields = array();
	foreach($lfs as $lf) {
		$lookupFields[$lf] = 1;
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	<title>
        <?= $portal->CurrentCompany->CompanyName ?> :: Manage Filters
    </title>
    <meta http-equiv="content-type" content="text/html;charset=utf-8"/>

    <?php include("components/bootstrap.php") ?>

    <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico"/>

	<script  src="js/func.js"></script>
<script>
	var transactionURL = "printable_trans.php";
	var lookupFieldRequest;

	var lookupFields = new Array();
	lookupFields["GroupName"] = 1;
	lookupFields["RegionName"] = 1;
	lookupFields["DivisionName"] = 1;
<?php
	foreach($contactFields->FILTER_FIELDS as $k=>$v)
	{ 
		if($lookupFields[$v] == 1)
		{
?>
	lookupFields["<?= $v ?>"] = 1;
<?php
		}
	 } 
?>

	function AddCriteriaRuleRow()
	{
		var critTable = document.getElementById('CriteriaTable');
		var lastRow = critTable.rows[critTable.rows.length - 1];
		var iteration = (isNaN(parseInt(lastRow.id.substring(7))) ? 0 : parseInt(lastRow.id.substring(7))) + 1;
		var row = critTable.insertRow(critTable.rows.length);

		row.setAttribute("class", (iteration%2 == 0 ? 'rowodd' : 'roweven' ));
		row.id = "CritRow" + iteration;

		// LeftVal cell
		var leftValue = row.insertCell(0);
		var sel = GenerateCriteriaSelect(iteration - 1);
		leftValue.appendChild(sel);
		leftValue.style.textAlign = 'center';
		// Compare cell
		var compareValue = row.insertCell(1);
		var compSel = GenerateCompareSelect(iteration - 1);
		compareValue.appendChild(compSel);
		compareValue.style.textAlign = 'center';
		// RightVal cell
		var rightValue = row.insertCell(2);
		rightValue.id = "RightValueCell" + iteration;
		var el = document.createElement('input');
		el.type = 'text';
		el.name = 'RightValue[' + (iteration - 1) + ']';
		el.id = 'RightValue[' + (iteration - 1) + ']';
		rightValue.appendChild(el);
		// Link cell
		var linkCell = row.insertCell(3);
		linkCell.innerHTML = "<a href='#' class='btn btn-default btn-sm actionButtonRight' onclick='DeleteCriteriaRuleRow(" + iteration + "); return false;'>Delete</a>";
	}

	function GenerateCriteriaSelect(rowNumber)
	{
		var sel = document.createElement('select');
		sel.name = 'LeftValue[' + (rowNumber) + ']';
		sel.id = 'LeftValue[' + (rowNumber) + ']';
		sel.onchange = function(){ UpdateRightValue(rowNumber + 1); }
<?php
	$i = 0;
	foreach($contactFields->FILTER_FIELDS as $k=>$v)
	{
?>
		sel.options[<?= $i ?>] = new Option('<?= $v ?>', '<?= $k ?>');
<?php
		$i++;
	}
?>
		return sel;
	}

	function GenerateCompareSelect(rowNumber)
	{
		var sel = document.createElement('select');
		sel.name = 'CompareValue[' + (rowNumber) + ']';
		sel.id = 'CompareValue[' + (rowNumber) + ']';
<?php
	$i = 0;
	foreach($portal->Compares as $k=>$v)
	{
?>
		sel.options[<?= $i ?>] = new Option('<?= $k ?>', '<?= $k ?>');
<?php
		$i++;
	}
?>
		return sel;
	}

	function DeleteCriteriaRuleRow(rowNumber)
	{
		var row = document.getElementById('CritRow'+rowNumber);
		row.parentNode.removeChild(row);

		// Loop and fix colors
		var critTable = document.getElementById('CriteriaTable');
		for(var i = 1; i < critTable.rows.length; i++)
		{
			critTable.rows[i].setAttribute("class", (i%2 == 0 ? 'rowodd' : 'roweven' ));
		}
	}

	function UpdateRightValue(rowNumber)
	{
		// Remove existing contents from cell
		var rightCell = document.getElementById('RightValueCell'+rowNumber);
		var fieldName = document.getElementById('LeftValue[' + (rowNumber - 1).toString() + ']').value;
		var oldInput = document.getElementById('RightValue[' + (rowNumber - 1).toString() + ']');
		rightCell.removeChild(oldInput);
		
		// Check if Lookup Field
		if(lookupFields[fieldName] == 1) {
			// If Lookup field create select and trigger load
			var el = document.createElement('select');
			el.name = 'RightValue[' + (rowNumber - 1) + ']';
			el.id = 'RightValue[' + (rowNumber - 1) + ']';
			el.options[0] = new Option('Loading...', '');
			LoadLookupOptions(rowNumber, fieldName);
		}
		else {
			// If not Lookup create basic input
			var el = document.createElement('input');
			el.type = 'text';
			el.name = 'RightValue[' + (rowNumber - 1) + ']';
			el.id = 'RightValue[' + (rowNumber - 1) + ']';
		}

		rightCell.appendChild(el);
	}

	function LoadLookupOptions(rowNumber, fieldName)
	{
		lookupFieldRequest = GetXmlHttpObject()
		if (lookupFieldRequest==null)
		{
			alert ("Your browser does not support AJAX!");
			return;
		} 
		var url=transactionURL;
		url=url+"?action=LookupOptions&RowNumber=" + rowNumber;
		url=url+"&FieldName=" + fieldName;
		url=url+"&sid="+Math.random();
		lookupFieldRequest.onreadystatechange=lookupFieldStateChanged;
		lookupFieldRequest.open("GET",url,true);
		lookupFieldRequest.send(null);	
	}

	function lookupFieldStateChanged()
	{
		if (lookupFieldRequest.readyState==4)
		{ 
			var xmlDoc=lookupFieldRequest.responseXML.documentElement;
			
			// Check if session timedout	
			if(xmlDoc.getElementsByTagName("Status").length > 0 && xmlDoc.getElementsByTagName("Status")[0].childNodes[0].nodeValue == 'Timeout')
			{
				parent.location="../login.php?logout=true&message='Your+Session+has+timedout.+Please+Login+Again.";
				return;
			}
			var sel = document.getElementById('RightValue[' 
						+ (xmlDoc.getElementsByTagName("RowNumber")[0].childNodes[0].nodeValue - 1).toString() 
						+ ']');
			sel.length = 0;
			for(var i = 0; i < xmlDoc.getElementsByTagName("Fields")[0].childNodes.length; i++) {
				var field = xmlDoc.getElementsByTagName("Fields")[0].childNodes[i];
				var value;
				var label;
				if(field.getElementsByTagName("Value")[0].childNodes[0])
					value = field.getElementsByTagName("Value")[0].childNodes[0].nodeValue
				else
					value = "";
				if(field.getElementsByTagName("Label")[0].childNodes[0])
					label = field.getElementsByTagName("Label")[0].childNodes[0].nodeValue
				else
					label = "";
					
				sel.options[i] = new Option( value, value );
			}
		}	
	}
	
	function RemoveSort(sortField) {
		var sortList = document.getElementById("SortFieldsList");
		var sortNode = document.getElementById("SortField_" + sortField);
		
		sortList.removeChild(sortNode);
	}
	
	function AddSort() {
		var sortList = document.getElementById("SortFieldsList");
		var sortField = document.getElementById("AddSortField").value;
		var sortDir = document.getElementById("AddSortDirection").value;
		
		var sortNode = document.createElement("li");
		sortNode.setAttribute("id", "SortField_" + sortField);
		sortNode.appendChild(document.createTextNode(sortField + " " + sortDir + " "));
		
		var linkNode = document.createElement("a");
		linkNode.setAttribute("href", "#");
		linkNode.setAttribute("onclick", "RemoveSort('" + sortField + "'); return false;");
		
		linkNode.appendChild(document.createTextNode("delete"));
		
		sortNode.appendChild(linkNode);
		
		var inputNode = document.createElement("input");
		inputNode.setAttribute("type", "hidden");
		inputNode.setAttribute("name", "SortField[" + sortField + "]");
		inputNode.setAttribute("value", sortDir);
		
		sortNode.appendChild(inputNode);
				
		sortList.appendChild(sortNode);		
		
	}
</script>
</head>
<body bgcolor="#FFFFFF">
<?php include("components/header.php") ?>
<div id="body">
	<?php 
		$CURRENT_PAGE = "Home";
		include("components/navbar.php"); 
	?>
	<?php if (isset($_GET['message'])): ?>
        <div class="container">
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?= $_GET['message']; ?>
            </div>
        </div>
    <?php endif; ?>
	<div id="controlPanelContainer" class="row">
        <div id="folderTreeContainer" class="col-md-offset-2 col-md-2 panel panel-default">
            <?php include("components/controlpanel_tree.php"); ?>
        </div>
        <div class="sectionHeader col-md-7">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Control Panel - Manage Filters</h3>
                </div>
                <div class="panel-body">
                    <div id="detailContainer">
                        <form method="post" action="<?= $_SERVER['PHP_SELF'] ?>?<?= $filterBlock->FilterBlockID != 0 ? "filterblockid={$filterBlock->FilterBlockID}" : "filterid={$filterBlock->FilterID}"; ?>" id="MainForm"> 
								<div id="FilterInfoDiv" class="table-responsive row">
                            <table id="CriteriaTable" width="100%" class="message_table table table-striped table-hover table-bordered">
                                <thead>
                                <tr class="filter_table rowheader" id="HeaderRow">
                                    <th class="filter_table">Left Value</th>
                                    <th class="filter_table">&nbsp;</th>
                                    <th class="filter_table">Right Value</th>
                                    <th class="filter_table">&nbsp;</th>
                                </tr>
                                </thead>
                                <tbody>
<?php                    
	$i = 0;
	foreach($filterBlock->FilterFields as $ff) {
?>
				<tr class="filter_table<?= $i % 2 == 0 ? ' roweven' : ' rowodd' ?>" id="CritRow<?= $i+1 ?>">
					<td align="center">
						<select name="LeftValue[<?= $i ?>]" id="LeftValue[<?= $i ?>]" onchange="UpdateRightValue(<?= $i+1 ?>)">
<?php
		foreach($contactFields->FILTER_FIELDS as $k=>$v)
		{
?>
							<option value="<?= $k ?>" <?= $k == $ff->LeftValue ? "SELECTED" : "" ?>><?= $v ?></option>									
<?php } ?>
						</select>
					</td>
					<td align="center">
						<select name="CompareValue[<?= $i ?>]">
<?php
		foreach($portal->Compares as $k=>$v)
		{
?>
							<option value="<?= $v ?>" <?= $k == $ff->CompareValue ? "SELECTED" : "" ?>><?= $v ?></option>									
<?php } ?>
						</select>
					</td>
					<td>
<?php
		if($lookupFields[$contactFields->FILTER_FIELDS[$ff->LeftValue]] == 1) {
?>
						<select name="RightValue[<?= $i ?>]" id="RightValue[<?= $i ?>]">
<?php
			$fields = $portal->GetLookupValues($ff->LeftValue);
			foreach($fields as $f) {
?>
							<option value="<?= $f->LookupValue ?>" <?= $f->LookupValue == $ff->RightValue ? 'SELECTED' : '' ?>><?= $f->LookupLabel ?></option>
<?php } ?>
						</select>
<?php 
		}
		else {
?>
						<input type="text" name="RightValue[<?= $i ?>]" id="RightValue[<?= $i ?>]" value="<?= $ff->RightValue ?>" />
<?php	} ?>
					</td>
					<td><a href="#" class="btn btn-default btn-sm actionButtonRight" onclick="DeleteCriteriaRuleRow(<?= $i+1 ?>); return false;">Delete</a></td>
				</tr>
<?php
		$i++;
	} ?>
                                </tbody>
                            </table>
                            <a href="#" class="btn btn-default btn-sm actionButtonRight" onclick="AddCriteriaRuleRow(); return false;">Add Filter Field</a><br/><br/>
                            <div id="SortFieldsHeader" class="subSectionHeader">Sort Fields</div>
										<div id="SortFieldsDiv">
											<ol id="SortFieldsList">
							<?php
								foreach($filterBlock->SortFields as $sf) {
							?>
										<li id="SortField_<?= $sf->Field ?>">
											<?= "$sf->Field $sf->Direction" ?> 
											<a href="#" onclick="RemoveSort('<?= $sf->Field ?>'); return false;">delete</a>
											<input type="hidden" name="SortField[<?= $sf->Field ?>]" value="<?= $sf->Direction ?>" />
										</li>
								<?php	} ?>
									</ol>
									<div id="AddSortFieldDiv">
										Sort Options:
										<select id="AddSortField">
						<?php
								foreach($contactFields->FILTER_FIELDS as $k=>$v)
								{
								?>
								<option value="<?= $k ?>"><?= $v ?></option>
								<?php		} ?>
							</select>
							<select id="AddSortDirection">
								<option value="ASC">ASC</option>
								<option value="DESC">DESC</option>
							</select><br/><br/>
								<input type="button" value="Add Sort" onclick="AddSort()" />
						</div>
					</div><br/>
					<div id="FilterLimitHeader" class="subSectionHeader">Filter Limit</div>
						<div id="FilterLimitDiv">
						<label>Limit records returned to:</label> <input type="input" name="FilterLimit" value="<?= $filterBlock->FilterLimit; ?>" />
						</div>
						<br/>
                            <input type="submit" value="Update" name="Submit" id="updateButton"/>&nbsp;&nbsp;
                            <input type="submit" value="Cancel" name="Submit"/>
                        </div>
                        </form>
                    </div>
                </div>
           </div>
       </div>
   </div>
</div>	
<?php include("components/footer.php") ?>
</body>
</html>