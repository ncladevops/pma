<?php
	require_once('../soaplib/nusoap.php');

	function SingleSignOn($ssourl, $ssotoken, $loginname, $password, $companyID, $dblink, $tpa = false, $person = null, $productID = 'none')
	{
		$client = new nusoap_client("https://services.printable.com/TRANS/0.9/SSO.asmx?wsdl", true,
						$proxyhost, $proxyport, $proxyusername, $proxypassword);
		$err = $client->getError();
		if ($err) {
			return "Fault";
		}
		
		$proxy = $client->getProxy();
		
		// Setup Partner Credentials
		$PartnerCredentials = array( 'Token' => "$ssotoken");
		
		// Setup SSO Request
		$UserCredentials = array( 'ID' => array( '_' => "{$loginname}_ExtID", 
									'!type' => 'NonPrintable') );
		$Navigation = array();
		$Navigation['ReturnURL'] = 'http://www.optifinow.com/sales/home.php';
		if($productID == 'none')
		{							
			$Navigation['StartPage'] = array( 'PageName' => 'Catalog');
		}
		elseif($productID == 'home')
		{							
			$Navigation['StartPage'] = 	array( 'PageName' => 'Home');
		}
		else 
		{
			$Navigation['StartPage'] = 	array( 'PageName' => 'AddToCart', 'ProductID' => array( '_' => "$productID", 
																											'!type' => 'Printable'));
		}
		
		$SingleSignOnRequest = array( 'UserCredentials' => $UserCredentials,
										'Navigation' => $Navigation );
										
		// Set up Edit User 
		$UserID = array( '_' => "{$loginname}_ExtID", '!type' => 'NonPrintable');
		$Properties = array( 'Login' => $loginname,
								'Password' => 'nclainc');
								
		if($person)
		{
			$Properties['FirstName'] = $person->FirstName;
			$Properties['LastName'] = $person->LastName;
			$Properties['Email'] = $person->ContactEmail;
			
			$UserGroup = array( '_' => $person->Company, 
									'!type' => 'NonPrintable');
		}

		$User = array( 'ID' => $UserID,
						'Properties' => $Properties);
		
								
		if($tpa)	
		{
			// setup Default sets from DB
			$tpaLookupArray = array("First_Name" => "FirstName","Last_Name" => "LastName",
									"Address_1" => "Address","Address_2" => "Address2","City" => "City",
									"State" => "State", "Zip" => "Zipcode","Office" => "ContactPhone",
									"Cell" => "ContactCell","Email" => "Username", "Fax" => "ContactFax",
									"Title" => "Title");
			
			
			foreach($tpaLookupArray as $k=>$v)
			{
				
				$tpaArray[] = array( 'IsHidden' => 'false',
											'IsLocked' => 'false',
											'Name' => "$k",
											'Value' => $person->$v);
			}
			$Defaults = array( array( 'Type' => 'TextProfileAttributes',
										'SyncMode' => 'Auto',
										'Default' => $tpaArray) );
										
			
			//$User['DefaultSets'] = array( 'Defaults' => $Defaults );
			
		}
		
		// Put together SSO
		$param = array(
						'SSOMessage' =>
							array(	'PartnerCredentials' => $PartnerCredentials,
									'SingleSignOnRequest' => $SingleSignOnRequest,
									'EditUserRequest' =>
										array( 'Action' => 'Auto',
												'User' => $User	)
										
							)
					);
		$result = $proxy->Authenticate($param);
//	echo '<h2>Request</h2><pre>' . htmlspecialchars($proxy->request, ENT_QUOTES) . '</pre>';
		// Check for a fault
		if ($proxy->fault) {
			//die("Store is currently Unavailable.");
			print_r($param);
			print_r($result);
			echo '<h2>Request</h2><pre>' . htmlspecialchars($proxy->request, ENT_QUOTES) . '</pre>';
			echo '<h2>Response</h2><pre>' . htmlspecialchars($proxy->response, ENT_QUOTES) . '</pre>';
			return "Fault";
		} else {
			// Check for errors
			$err = $proxy->getError();
			if ($err) {
				// Display the error
				echo '<h2>Error</h2><pre>' . $err . '</pre>';
				return "Fault"; 
			} else {
				return $result['AuthenticateResult'];
			}
		}
	}
	
	function SingleSignOnOLD($ssourl, $ssotoken, $loginname, $password, $companyID, $dblink)
	{
		$client = new soapclient("https://services.printable.com/TRANS/0.9/SSO.asmx?wsdl", true,
						$proxyhost, $proxyport, $proxyusername, $proxypassword);
		$err = $client->getError();
		if ($err) {
			return "Fault";
		}
		
		$proxy = $client->getProxy();
		
		// Setup Partner Credentials
		$PartnerCredentials = array( 'Token' => "$ssotoken");
		
		// Setup SSO Request
		$UserCredentials = array( 'ID' => array( '_' => "{$loginname}_ExtID", 
									'!type' => 'NonPrintable') );
		$Navigation = array( 'ReturnURL' => 'http://121communication.com/timewarner/listing.php',
								'StartPage' => 	array( 'PageName' => 'Catalog'	));
		$SingleSignOnRequest = array( 'UserCredentials' => $UserCredentials,
										'Navigation' => $Navigation );
										
		// Set up Edit User 
		$UserID = array( '_' => "{$loginname}_ExtID", '!type' => 'NonPrintable');
		$Properties = array( 'Login' => $loginname,
								'Password' => $password);
		
		$User = array( 'ID' => $UserID,
						'Properties' => $Properties);
		
		// Put together SSO
		$param = array(
						'SSOMessage' =>
							array(	'PartnerCredentials' => $PartnerCredentials,
									'SingleSignOnRequest' => $SingleSignOnRequest,
									'EditUserRequest' =>
										array( 'Action' => 'Auto',
												'User' => $User	)
										
							)
					);
		$result = $proxy->Authenticate($param);
		//echo '<h2>Request</h2><pre>' . htmlspecialchars($proxy->request, ENT_QUOTES) . '</pre>';
		// Check for a fault
		if ($proxy->fault) {
			print_r($result);
			echo '<h2>Request</h2><pre>' . htmlspecialchars($proxy->request, ENT_QUOTES) . '</pre>';
			echo '<h2>Response</h2><pre>' . htmlspecialchars($proxy->response, ENT_QUOTES) . '</pre>';
			return "Fault";
		} else {
			// Check for errors
			$err = $proxy->getError();
			if ($err) {
				// Display the error
				echo '<h2>Error</h2><pre>' . $err . '</pre>';
				return "Fault"; 
			} else {
				return $result['AuthenticateResult'];
			}
		}
	}
	
	function SingleSignTpaUpdate($ssourl, $ssotoken, $csvArray)
	{
		$client = new soapclient("https://services.printable.com/TRANS/0.9/SSO.asmx?wsdl", true,
						$proxyhost, $proxyport, $proxyusername, $proxypassword);
		$err = $client->getError();
		if ($err) {
			return "Fault";
		}
		
		$proxy = $client->getProxy();
		
		// Setup Partner Credentials
		$PartnerCredentials = array( 'Token' => "$ssotoken");
		
		// Setup SSO Request
		$UserCredentials = array( 'ID' => array( '_' => $csvArray[15] . "_ExtID", 
									'!type' => 'NonPrintable') );
		$Navigation = array( 'ReturnURL' => 'http://121communication.com/downeysavings/listing.php',
								'StartPage' => 	array( 'PageName' => 'Home'	));
		$SingleSignOnRequest = array( 'UserCredentials' => $UserCredentials,
										'Navigation' => $Navigation );
										
		// Set up Edit User 
		$UserID = array( '_' => $csvArray[15] .  "_ExtID", '!type' => 'NonPrintable');
		$Properties = array( 'Login' => $csvArray[15],
								'Password' => 'downey');
		// setup Default sets from DB
		$TpaArray = array();
		
		$TpaArray[0] = array( 'IsHidden' => 'true',
								'IsLocked' => 'true',
								'Name' => "First Name",
								'Value' => $csvArray[0]);
		$TpaArray[1] = array( 'IsHidden' => 'true',
								'IsLocked' => 'true',
								'Name' => "Last Name",
								'Value' => $csvArray[1]);
		$TpaArray[2] = array( 'IsHidden' => 'true',
								'IsLocked' => 'true',
								'Name' => "Title",
								'Value' => $csvArray[2]);
		$TpaArray[3] = array( 'IsHidden' => 'true',
								'IsLocked' => 'true',
								'Name' => "Branch",
								'Value' => $csvArray[3]);
		$TpaArray[4] = array( 'IsHidden' => 'true',
								'IsLocked' => 'true',
								'Name' => "Address 1",
								'Value' => $csvArray[4]);
		$TpaArray[5] = array( 'IsHidden' => 'true',
								'IsLocked' => 'true',
								'Name' => "Address 2",
								'Value' => $csvArray[5]);
		$TpaArray[6] = array( 'IsHidden' => 'true',
								'IsLocked' => 'true',
								'Name' => "City",
								'Value' => $csvArray[8]);
		$TpaArray[7] = array( 'IsHidden' => 'true',
								'IsLocked' => 'true',
								'Name' => "State",
								'Value' => $csvArray[9]);
		$TpaArray[8] = array( 'IsHidden' => 'true',
								'IsLocked' => 'true',
								'Name' => "Zip",
								'Value' => $csvArray[10]);
		$TpaArray[9] = array( 'IsHidden' => 'true',
								'IsLocked' => 'true',
								'Name' => "Phone 1",
								'Value' => $csvArray[11]);
		$TpaArray[10] = array( 'IsHidden' => 'true',
								'IsLocked' => 'true',
								'Name' => "Phone 2",
								'Value' => $csvArray[12]);
		$TpaArray[11] = array( 'IsHidden' => 'true',
								'IsLocked' => 'true',
								'Name' => "Fax",
								'Value' => $csvArray[14]);
		$TpaArray[12] = array( 'IsHidden' => 'true',
								'IsLocked' => 'true',
								'Name' => "Email",
								'Value' => $csvArray[15]);																
		
		$Defaults = array( array( 'Type' => 'TextProfileAttributes',
									'SyncMode' => 'Auto',
									'Default' => $TpaArray) );
		$User = array( 'ID' => $UserID,
						'Properties' => $Properties,
						'DefaultSets' => array( 'Defaults' => $Defaults ) );
		
		// Put together SSO
		$param = array(
						'SSOMessage' =>
							array(	'PartnerCredentials' => $PartnerCredentials,
									'SingleSignOnRequest' => $SingleSignOnRequest,
									'EditUserRequest' =>
										array( 'Action' => 'Auto',
												'User' => $User	)
										
							)
					);
		$result = $proxy->Authenticate($param);
		
		// Check for a fault
		if ($proxy->fault) {
			echo '<h2>Error</h2><pre>' . $err . '</pre>';
			return "Fault";
			
			return "Fault";
		} else {
			// Check for errors
			$err = $proxy->getError();
			if ($err) {
				// Display the error
				echo '<h2>Error</h2><pre>' . $err . '</pre>';
				return "Fault"; 
			} else {
				return $result['AuthenticateResult'];
			}
		}
	}

?>