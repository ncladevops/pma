<?php
require("../printable/include/mysql.inc.php");
require("../printable/include/optimize.printable.inc.php");
require("globals.php");

$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);

$portal = new OptimizePortal($COMPANY_ID, $db);

$currentUser = $portal->UserAccess($_SESSION['currentuserid']);
$isSubAdmin = $portal->CheckPriv($currentUser->UserID, 'subadmin');

// Check login
if (!$isSubAdmin) {
    header("Location: " . $portal->CurrentCompany->Website . "login.php?message=" . urlencode("Not logged in or login error.  Please try again."));
    die();
}

if ($_POST['Submit'] == 'Save/Upload' && $_POST['description'] != '') {
    $desc = addslashes($HTTP_POST_VARS['description']);

    $findme = "FINDME" . rand();

    $fID = $portal->CreateNewFile($findme, $desc, $_GET['section']);

    // direct to edit file
    header("Location: edit_file.php?fileid=$fID&section=" . $_GET['section']);
    die();
} elseif ($_POST['Submit'] == 'Save/Upload' && $_POST['description'] == '') {
    $_GET['message'] = "Invalid File Name";
} elseif ($_POST['Submit'] == 'Cancel') {
    header("Location: manage_filedl.php?section=" . $_GET['section']);
    die();
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <title>
            <?= $portal->CurrentCompany->CompanyName ?> :: Add File
        </title>
        <meta http-equiv="content-type" content="text/html;charset=utf-8" />		
        <script  src="js/func.js"></script>	
        <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />

        <?php include("components/bootstrap.php") ?>

    </head>
    <body bgcolor="#FFFFFF">
        <div id="page">
            <?php include("components/header.php") ?>
            <div id="body">
                <?php
                $CURRENT_PAGE = "Home";
                include("components/navbar.php");
                ?>
                <form method="post" action="<?= $_SERVER['PHP_SELF'] ?>?section=<?= $_GET['section'] ?>" enctype="multipart/form-data" >
                    <?php if (isset($_GET['message'])): ?>
                        <div class="container">
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <?= $_GET['message']; ?>
                            </div>
                        </div>
                    <?php endif; ?> 
                    <div id="AddFileDiv" class="container well">
                        <div class="sectionHeader">
                            <h2>Add New File</h2>
                        </div>
                        <div class="sectionDiv">
                            <div class="itemSection row">
                                <div id="DescriptionDiv" class="form-group col-md-3">
                                    <label for="description">File Name:</label><br/>
                                    <input type="text" class="form-control input-sm" name="description" value="" size="45"/>
                                </div>
                            </div>
                        </div>
                        <center>
                            <div class="itemSection row">
                                <div class="buttonSection">
                                    <input type="submit" name="Submit" class="btn btn-info btn-sm" value="Save/Upload"/>&nbsp;&nbsp;<input type="submit" class="btn btn-default btn-sm" value="Cancel" name="Submit"/>
                                </div>
                            </div>
                        </center>
                    </div>
                </form>	
            </div>
        </div>
        <?php include("components/footer.php") ?>
    </body>
</html>