<?php
require("../printable/include/mysql.inc.php");
require("../printable/include/optimize.printable.inc.php");
require("globals.php");

$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);

$portal = new OptimizePortal($COMPANY_ID, $db);

// Querry to get user info
$currentUser = $portal->GetUser($_SESSION['currentuserid']);

// Check login
if (!$currentUser) {
    die("Sesssion Timed out please login again.");
}


if ($_GET['clone']) {
    $cloneContact = $portal->GetOptimizeContact($_GET['clone'], $currentUser->UserID);

    if (!cloneContact) {
        die("Invalid Contact to Clone.");
    }

    $cloneContact->SubmissionListingID = "";

    $slID = $portal->AddOptimizeContact($cloneContact);

    header("Location: editcontactsmall.php?id=$slID");
    die();
}

$currentCampaign = $portal->GetSubmissionListingType($_GET['listid']);

if (!$currentCampaign) {
    die("Invalid Listing Type.");
}

// if update was pressed
if ($_POST['Submit'] == 'Add Contact') {

    $contact = new OptimizeContact();

    $contact->Company = $_POST['Company'];
    $contact->FirstName = $_POST['FirstName'];
    $contact->LastName = $_POST['LastName'];
    $contact->ListingType = $currentCampaign->SubmissionListingTypeID;
    $contact->UserID = $currentUser->UserID;

    $slID = $portal->AddOptimizeContact($contact);

    if (!$slID) {
        header("Location: contacts.php?message=" . urlencode("Error adding contact."));
        die();
    }

    header("Location: editcontactsmall.php?id=$slID");
    die();
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <title>
            <?= $portal->CurrentCompany->CompanyName ?> :: Add Contact Info
        </title>
        <meta http-equiv="content-type" content="text/html;charset=utf-8" />		
        <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />
        <script  src="include/js/func.js"></script>

        <?php include("components/bootstrap.php") ?>

    </head>
    <body>
        <div id="body">
            <form class="form-horizontal" method="post" action="<?= $_SERVER['PHP_SELF'] ?>?listid=<?= $currentCampaign->SubmissionListingTypeID ?>">
                <div class="container">
                    <ul class="nav nav-tabs" style="margin-bottom: 15px;">
                        <li class="active"><a href="#contactDiv" data-toggle="tab">Contact Info</a></li>
                    </ul>
                    <div id="myTabContent" class="tab-content well">
                        <div class="tab-pane fade active in" id="contactDiv">
                            <div class="row form-group" id="CompanyInfo">
                                <div id="CompanyDiv" class="col-xs-6">
                                    <label for="Company">Company</label>
                                    <input type="text" class="form-control input-sm" name="Company" value="<?= $contact->Company ?>" size="50" />
                                </div>
                            </div>
                            <div class="row form-group" id="NameInfo">
                                <div id="FirstNameDiv" class="col-xs-6">
                                    <label for="FirstName">First Name</label>
                                    <input type="text" class="form-control input-sm" name="FirstName" value="<?= $contact->FirstName ?>" />
                                </div>
                                <div id="LastNameDiv" class="col-xs-6">
                                    <label for="LastName">Last Name</label>
                                    <input type="text" class="form-control input-sm" name="LastName" value="<?= $contact->LastName ?>" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="buttonDiv">
                        <input type="submit" name="Submit" class="btn btn-primary" value="Add Contact" />&nbsp;&nbsp;&nbsp;<input type="button" value="Cancel" class="btn btn-default" onclick="window.close();" />
                    </div>
                </div>
            </form>
        </div>

        <?php include("components/bootstrap-footer.php") ?>

    </body>
</html>