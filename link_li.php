<?php
	require("../printable/include/mysql.inc.php");
	require("../printable/include/optimize.printable.inc.php");
	require("globals.php");
	require '../printable/include/component/linkedin/linkedin.php';
	
	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	
	$portal = new OptimizePortal($COMPANY_ID, $db);
	
	$currentUser = $portal->UserAccess($_SESSION['currentuserid']);
	$isSuper = $portal->CheckPriv($currentUser->UserID, 'supervisor');
			
	if(!$currentUser)
	{
		die("Not logged in or login error.");
	}
	
	if($_GET['remove'] == 'true') {
		$currentUser->LinkedinToken  = "";
		$_SESSION['linkedin_name'] = "";
		unset($_SESSION['linkedin_oauth_token']);
		unset($_SESSION['linkedin_oauth_token_secret']);
		$portal->UpdateUser($currentUser);
?>
<html>
<head><title>Remove LinkedIn Link</title></head>
<body onload="window.opener.updateLinkedInName(''); window.close();">
<a href="#" onclick="window.close()">Click here to close the window</a>
</body>
</html>
<?php
		die();
	}
	
	$oauth_callback = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
	$linkedin = new LinkedIn($LINKEDIN_KEY, $LINKEDIN_SECRET, $oauth_callback );
	
	if (!isset($_SESSION['linkedin_oauth_token'])) {
		if(strlen($currentUser->LinkedinToken) <= 0) {
			$linkedin->getRequestToken();
			
			// store the token
			$_SESSION['linkedin_oauth_token'] = $linkedin->request_token->key;
			$_SESSION['linkedin_oauth_token_secret'] = $linkedin->request_token->secret;
			$_SESSION['linkedin_oauth_verify'] = true;			
			// redirect to auth website
			header("Location: " . $linkedin->generateAuthorizeUrl());
			die();
		} else {
			// Authorize User
			list($t,$s) = explode(":",$currentUser->LinkedinToken);
			$linkedin->access_token = new OAuthConsumer($t, $s, 1);
			$response = new SimpleXMLElement($linkedin->getProfile("~:(id,first-name,last-name,headline,picture-url)"));
			
			// if authorized set session
			if($response->status != '401') {
				$_SESSION['linkedin_oauth_token'] = $t;
				$_SESSION['linkedin_oauth_token_secret'] = $s;
				$_SESSION['linkedin_name'] = $response->{'first-name'} . " " . $response->{'last-name'};
			}
			// else clear credentials
			else {
				unset($_SESSION['linkedin_oauth_token']);
				unset($_SESSION['linkedin_oauth_token_secret']);
				$_SESSION['linkedin_name'] = "";
				$currentUser->LinkedinToken = "";
				$portal->UpdateUser($currentUser);
			}
			
			header('Location: ' . basename(__FILE__));
			die();
		}

	} elseif (isset($_GET['oauth_verifier']) && isset($_SESSION['linkedin_oauth_verify'])) {
		// verify the token
		$linkedin->request_token = new OAuthConsumer($_SESSION['linkedin_oauth_token'], $_SESSION['linkedin_oauth_token_secret'], 1);
        $linkedin->oauth_verifier = $_GET['oauth_verifier'];
		unset($_SESSION['linkedin_oauth_verify']);

		// get the access token
        $linkedin->getAccessToken($_GET['oauth_verifier']);
		
		// store the token (which is different from the request token!)
		$_SESSION['linkedin_oauth_token'] = $linkedin->access_token->key;
		$_SESSION['linkedin_oauth_token_secret'] = $linkedin->access_token->secret;
		
		// store the token with the user
		$currentUser->LinkedinToken = $linkedin->access_token->key . ":" . $linkedin->access_token->secret;
		$portal->UpdateUser($currentUser);
		
		list($t,$s) = explode(":",$currentUser->LinkedinToken);
		$linkedin->access_token = new OAuthConsumer($t, $s, 1);
		$response = new SimpleXMLElement($linkedin->getProfile("~:(id,first-name,last-name,headline,picture-url)"));
		
		// if authorized set session
		if($response->status != '401') {
			$_SESSION['linkedin_name'] = $response->{'first-name'} . " " . $response->{'last-name'};
		}
		// else clear credentials
		else {
			unset($_SESSION['linkedin_oauth_token']);
			unset($_SESSION['linkedin_oauth_token_secret']);
			$_SESSION['linkedin_name'] = "";
			$currentUser->LinkedinToken = "";
			$portal->UpdateUser($currentUser);
		}

		// send to same URL, without oauth GET parameters
		header('Location: ' . basename(__FILE__));
		die();
	} elseif(isset($_SESSION['linkedin_oauth_token']) && $_SESSION['linkedin_name'] == "") {
		unset($_SESSION['linkedin_oauth_token']);
		unset($_SESSION['linkedin_oauth_token_secret']);
	}
?>
<html>
<head><title>Create LinkedIn Link</title></head>
<body onload="window.opener.updateLinkedInName('<?= $_SESSION['linkedin_name'] ?>'); window.close();">
<a href="#" onclick="window.close()">Click here to close the window</a>
</body>
</html>