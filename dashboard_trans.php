<?php
require("../printable/include/mysql.inc.php");
require("../printable/include/optimize.printable.inc.php");
require("globals.php");

$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);

$portal = new OptimizePortal($COMPANY_ID, $db);

$currentUser = $portal->UserAccess($_SESSION['currentuserid']);

// Check login
if (!$currentUser) {
    echo "<Result><Status>Timeout</Status></Result>";
    die();
}


switch ($_GET['dashboard']) {
    case "AppointmentTable":
        $appointments = $portal->GetAppointments($currentUser->UserID);
        ?>
        <table id="ApptTable" class="table table-striped table-hover table-bordered">
            <tr class="AppointmentTable rowheader">
                <td class="AppointmentTable">
                    Name
                </td>
                <td class="AppointmentTable">
                    Appointment Time
                </td>
                <td class="AppointmentTable">
                    Reminder Type
                </td>
                <td class="AppointmentTable">
                    &nbsp;
                </td>
                <td class="AppointmentTable">
                    &nbsp;
                </td>
            </tr>
            <?php
            if (sizeof($appointments) <= 0) {
                ?>
                <tr class="HistoryTable roweven">
                    <td colspan="5" align="center"><b>You have no upcoming appointments</b></td>
                </tr>
            <?php
            } else {
                $i = 0;
                foreach ($appointments as $appt) {
                    $contact = $portal->GetOptimizeContact($appt->ContactID, $currentUser->UserID);
                    if ($contact) {
                        $i++;
                        $slt = $portal->GetSubmissionListingType($contact->ListingType);
                        $userGroup = $portal->GetGroup($currentUser->GroupID);
                        if ($slt->SubmissionListingTypeID != $userGroup->DefaultSLT) {
                            $type = "lead";
                        } else {
                            $type = "contact";
                        }
                        ?>
                        <tr class="HistoryTable <?= $i % 2 == 0 ? 'roweven' : 'rowodd' ?>">
                            <td class="AppointmentTable">
                                <?php
                                if ($type == "lead") {
                                    ?>
                                    <a href="edit_lead.php?id=<?= $contact->SubmissionListingID; ?>">
                                        <?= "$contact->FirstName $contact->LastName" ?>
                                    </a>
                                <?php
                                } else {
                                    ?>
                                    <a href="#"
                                       onclick="wopen('editcontactsmall.php?id=<?= $contact->SubmissionListingID ?>', 'edit_contact', 600, 450);"
                                       title="Edit Contact"><?= "$contact->FirstName $contact->LastName" ?></a>
                                <?php
                                }
                                ?>
                            </td>
                            <td class="AppointmentTable">
                                <?= date("m/d/y g:ia", strtotime($appt->AppointmentTime)); ?>
                            </td>
                            <td class="AppointmentTable">
                                <?= ucfirst($appt->NotifyType) ?>
                            </td>
                            <td class="AppointmentTable">
                                <a href="#"
                                   onclick="wopen('edit_appointment.php?id=<?= $appt->ContactAppointmentID; ?>', 'edit_appointment', 350, 550); return false;"
                                   title="Add/Edit Reminder">View/Edit</a>
                            </td>
                            <td class="AppointmentTable">
                                <a href="#"
                                   onclick="wopen('delete_appointment.php?id=<?= $appt->ContactAppointmentID; ?>', 'edit_appointment', 350, 550); return false;">Delete</a>
                            </td>
                        </tr>
                    <?php
                    }
                }
            }
            ?>
        </table>
        <?php
        break;
    case "AppointmentTableSimp":
        $appointments = $portal->GetAppointments($currentUser->UserID);
        ?>
        <div class="table-responsive">
            <table id="ApptSimp" class="table table-appt table-striped table-hover table-bordered">
                <?php
                if (sizeof($appointments) <= 0) {
                    ?>
                    <tr class="HistoryTable roweven">
                        <td colspan="5" align="center"><b>You have no upcoming appointments</b></td>
                    </tr>
                <?php
                } else {
                $i = 0; ?>

                <thead>
                <tr>
                    <td>
                        Name

                    </td>
                    <td>
                        Appointment Time
                    </td>
                    <td>
                        Notes
                    </td>
                </tr>
                </thead>

                <tbody>

                <?php
                foreach ($appointments as $appt) {
                    ($appt->PastDue) ? $pastdue='color: red;' : $pastdue='';
                    $contact = $portal->GetOptimizeContact($appt->ContactID, $currentUser->UserID);
                    if ($contact) {
                        $i++;
                        $slt = $portal->GetSubmissionListingType($contact->ListingType);
                        $userGroup = $portal->GetGroup($currentUser->GroupID);
                        if ($slt->SubmissionListingTypeID != $userGroup->DefaultSLT) {
                            $type = "lead";
                        } else {
                            $type = "contact";
                        }
                        ?>
                        <tr class="<?= $i % 2 == 0 ? 'roweven' : 'rowodd' ?>">
                            <td>
                                <a href="#"
                                   onclick="wopen('edit_appointment.php?id=<?= $appt->ContactAppointmentID; ?>', 'edit_appointment', 350, 550);
                                       return false;"
                                   title="Add/Edit Reminder" class="btn btn-default btn-sm" data-toggle="tooltip"><span
                                        class="glyphicon glyphicon-edit"></span></a>

                                <?php
                                if ($type == "lead") {
                                    ?>
                                    <a href="edit_lead.php?id=<?= $contact->SubmissionListingID; ?>">
                                        <?= "$contact->FirstName $contact->LastName" ?>
                                    </a>
                                <?php
                                } else {
                                    ?>
                                    <a href="#"
                                       onclick="wopen('editcontactsmall.php?id=<?= $contact->SubmissionListingID ?>', 'edit_contact', 600, 450);"
                                       title="Edit Contact"><?= "$contact->FirstName $contact->LastName" ?></a>
                                <?php
                                }
                                ?>
                            </td>
                            <td>
                                <span style="<?= $pastdue ?>"><?= date("m/d/y g:ia", strtotime($appt->AppointmentTime)); ?></span>
                            </td>
                            <td>
                                <?= $appt->Notes ?>
                            </td>
                        </tr>
                    <?php
                    }
                }

                }
                ?>

                <?php while ($i <= 1) {
                    $i++; ?>
                    <tr class="<?= $i % 2 == 0 ? 'roweven' : 'rowodd' ?>">
                        <td class="AppointmentTableSimp" colspan="3"></td>
                    </tr>
                <?php } ?>

                </tbody>

            </table>
        </div>
        <?php
        break;
    default:
        echo "Dashboard Not Found";

}

?>