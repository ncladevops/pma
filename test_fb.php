<?php
	require("../printable/include/mysql.inc.php");
	require("../printable/include/optimize.printable.inc.php");
	require("globals.php");
	require '../printable/include/component/facebook/facebook.php';
	
	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	
	$portal = new OptimizePortal($COMPANY_ID, $db);
	
	$currentUser = $portal->UserAccess($_SESSION['currentuserid']);
	$isSuper = $portal->CheckPriv($currentUser->UserID, 'supervisor');
			
	if(!$currentUser)
	{
		die("Not logged in or login error.");
	}
	
	$facebook = new Facebook(array(
	  'appId'  => $FACEBOOK_APPID,
	  'secret' => $FACEBOOK_SECRET,
	));	
	
	if($facebook->getUserAccessToken()) {
		$currentUser->FacebookToken = $facebook->getUserAccessToken();
		$portal->UpdateUser($currentUser);
		$facebook->setAccessToken($currentUser->FacebookToken);
	}
	else {
		$facebook->setAccessToken($currentUser->FacebookToken);
	}
		
	$fb_user_id = $facebook->getUserFromAccessToken();
	
	if($_POST['submit'] == 'Post to Facebook') {
		try {
			$ret_obj = $facebook->api('/me/feed', 'POST',
								array(
								  'message' => $_POST['post_text']
							 ));
			echo '<pre>Post ID: ' . $ret_obj['id'] . '</pre>';
			die();
	
	  } catch(FacebookApiException $e) {
		
		$fb_user_id = 0;
	  }

	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
	<title>
		OptifiNow :: Publish onDemand
	</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />		
	<link type="text/css" media="all" rel="stylesheet" href="style.css" />
	<script  src="js/func.js"></script>	

</head>
<body bgcolor="#FFFFFF" onload="">
<?php
	if($fb_user_id) {
?>
<form action="<?= $_SERVER['PHP_SELF'] ?>" method="POST">
	<b>Post:</b><br/>
	<textarea name="post_text"></textarea>
	<input type="submit" value="Post to Facebook" name="submit"/>
</form>
<?php
	}
	else {
		$login_url = $facebook->getLoginUrl( array(
				   'scope' => 'publish_stream'
				   )); 
?>
	Please <a href="<?= $login_url ?>">login.</a>
<?php
	}
?>
</body>
</html>