<?php
require("../printable/include/mysql.inc.php");
require("../printable/include/optimize.printable.inc.php");
require("globals.php");

$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);

$portal = new OptimizePortal($COMPANY_ID, $db);

$currentUser = $portal->GetUser($_SESSION['currentuserid']);

$isSubAdmin = $portal->CheckPriv($currentUser->UserID, 'subadmin');
$isAdmin = $portal->CheckPriv($currentUser->UserID, 'admin');

if (!$isSubAdmin) {
    header("Location: " . $portal->CurrentCompany->Website . "home.php?message=" . urlencode("Accessed Denied."));
    die();
}

$sources = $portal->GetSubmissionListingSources();
$groups = $portal->GetCompanyGroups();

if ($_POST['Submit'] == 'Cancel') {
    header('Location: manage_campaigns.php?message=' . urlencode("Action Canceled. Campaign not updated."));
    die();
} elseif ($_POST['ListingType']) {
    $sltID = $portal->CreateSubmissionListingType($_POST['ListingType'], $_POST['GroupID']);

    $slt = $portal->GetSubmissionListingType($sltID);

    $slt->SubmissionListingSourceID = $_POST['SubmissionListingSourceID'];

    $portal->UpdateSubmissionListingType($slt);

    header("Location: edit_campaign.php?id=$sltID");
    die();
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title>
        <?= $portal->CurrentCompany->CompanyName ?> :: Add New Campaign
    </title>
    <meta http-equiv="content-type" content="text/html;charset=utf-8"/>
    <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico"/>

    <?php include("components/bootstrap.php") ?>

</head>
<body bgcolor="#FFFFFF">
<?php include("components/header.php") ?>
<div id="body">
    <?php
    $CURRENT_PAGE = "Control Panel";
    include("components/navbar.php");
    ?>
    <form method="post" action="<?= $_SERVER['PHP_SELF'] ?>">

        <?php if (isset($_GET['message'])): ?>
            <div class="container">
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?= $_GET['message']; ?>
                </div>
            </div>
        <?php endif; ?>

        <div id="AddMessageDiv" class="well container">
            <div class="sectionHeader">
                <h2>Add New Campaign</h2>
            </div>
            <div class="sectionDiv row">
                <div class="form-group col-md-3">
                    <label for="ListingType">Campaign Name:</label>
                    <input type="text" class="form-control input-sm" id="ListingType" name="ListingType"/>
                </div>
            </div>
            <?php
            if ($isAdmin) {
                ?>
                <div class="sectionDiv row">
                    <div class="form-group col-md-3">
                        <label for="SubmissionListingSourceID">Group:</label>
                        <select class="form-control input-sm" id="GroupID" name="GroupID">
                            <option value="0">All</option>
                            <?php
                            foreach ($groups as $g) {
                                ?>
                                <option value="<?= $g->GroupID ?>"
                                    <?= $g->GroupName == "All" ? "SELECTED" : "" ?>>
                                    <?= $g->GroupName ?>
                                </option>
                            <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
            <?php
            } else {
                echo "<input type='hidden' name='GroupID' value='$currentUser->GroupID'/>";
            }
            ?>
            <div class="sectionDiv row">
                <div class="form-group col-md-3">
                    <label for="SubmissionListingSourceID">Lead Source:</label>
                    <select class="form-control input-sm" id="SubmissionListingSourceID"
                            name="SubmissionListingSourceID">
                        <?php
                        foreach ($sources as $s) {
                            ?>
                            <option value="<?= $s->SubmissionListingSourceID ?>"
                                <?= $s->SubmissionListingSourceID == $campaign->SubmissionListingSourceID ? "SELECTED" : "" ?>>
                                <?= $s->ListingSourceName ?>
                            </option>
                        <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
            <center>
                <div class="itemSection">
                    <div class="buttonSection">
                        <input type="submit" class="btn btn-info btn-sm" value="Add Campaign" name="Submit"/>&nbsp;&nbsp;<input
                            type="submit" class="btn btn-default btn-sm" value="Cancel" name="Submit"/>
                    </div>
                </div>
            </center>
        </div>
    </form>
</div>

<?php include("components/footer.php") ?>

</body>
</html>