<?php
require("../printable/include/mysql.inc.php");
require("../printable/include/optimize.printable.inc.php");
require("globals.php");
include("components/class.myatomparser.php");
require '../printable/include/component/twitter/codebird.php';

$MAX_DASHBOARDS = 5;

$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);

$portal = new OptimizePortal($COMPANY_ID, $db);

$currentUser = $portal->UserAccess($_SESSION['currentuserid']);
$isSuper = $portal->CheckPriv($currentUser->UserID, 'supervisor');

if (!$currentUser) {
    header("Location: login.php?message=" . urlencode("Not logged in or login error."));
    die();
}

$dashboard = $portal->GetDashboard($HOMEPAGE_DBID);
$reports = $portal->GetDashBoardReports($dashboard->DashboardID);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <title>
            <?= $portal->CurrentCompany->CompanyName ?> :: Welcome
        </title>
        <meta http-equiv="content-type" content="text/html;charset=utf-8" />
        <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />		

        <?php include("reports/report_dashboard_includes.php") ?>

        <script type="text/javascript">
            $(document).ready(function() {

                $(document).tooltip();

            });

            var dashboard_transactionURL = "dashboard_trans.php";
            var appointmentTableRequest;
            var appointmentTableSimpleRequest;

            var report_loaderURL = "reports/load_report.php";
            var reportRequest = new Array();

            function LoadDashboards()
            {
                LoadAppointmentTableSimple();
                LoadCalendar();
                LoadDashboardReports();
            }

            function LoadCalendar()
            {
                document.getElementById("CalendarTable_Dashboard").innerHTML = "<iframe src='appt_calendar_sim.php' style='border: 0px; width: 295px; height: 252px;' scrolling='no'></iframe>";
            }

            function LoadAppointmentTableSimple()
            {
                document.getElementById("AppointmentTableSimple_Dashboard").innerHTML = "<img src='images/loading.gif' />"

                appointmentTableSimpleRequest = GetXmlHttpObject();
                if (appointmentTableSimpleRequest == null)
                {
                    alert("Your browser does not support AJAX!");
                    return;
                }
                var url = dashboard_transactionURL;
                url = url + "?dashboard=AppointmentTableSimp";
                url = url + "&sid=" + Math.random();
                appointmentTableSimpleRequest.onreadystatechange = appointmentTableSimpleRequestStateChanged;
                appointmentTableSimpleRequest.open("GET", url, true);
                appointmentTableSimpleRequest.send(null);
            }

            function appointmentTableSimpleRequestStateChanged()
            {
                if (appointmentTableSimpleRequest.readyState == 4)
                {
                    document.getElementById("AppointmentTableSimple_Dashboard").innerHTML = appointmentTableSimpleRequest.responseText;
                }
            }

            function show(id) {
                document.getElementById("spinner" + id).style.display = 'none';
            }

            function LoadDashboardReports() {

                var startDate = document.getElementById("FilterDashboardStart").value;
                var endDate = document.getElementById("FilterDashboardEnd").value;
                var url;
                var u;
<?php
for ($i = 0; $i < $MAX_DASHBOARDS; $i++) {
    if (isset($reports[$i])) {
        if ($reports[$i]->UserBased) {
            $userString = "&sv_RepName=" . urlencode($currentUser->FirstName . " " . $currentUser->LastName);
        } else {
            $userString = "";
        }
        ?>
                        u = encodeURIComponent('<?= $reports[$i]->URL ?>' + "?cmd=search&so_DateAdded=BETWEEN&sv_DateAdded=" + encodeURIComponent(startDate) + "&sv2_DateAdded=" + encodeURIComponent(endDate) + '<?= $userString ?>' + "&export=print");
                        reportRequest[<?= $i ?>] = GetXmlHttpObject();
                        if (reportRequest[<?= $i ?>] == null)
                        {
                            alert("Your browser does not support AJAX!");
                            return;
                        }

                        url = report_loaderURL;
                        url = url + "?url=" + u;
                        url = url + "&sid=" + Math.random();
                        reportRequest[<?= $i ?>].onreadystatechange = reportRequestStateChanged;
                        reportRequest[<?= $i ?>].open("GET", url, true);
                        reportRequest[<?= $i ?>].send(null);

                        document.getElementById("DashboardDiv" + <?= $i + 1 ?>).innerHTML = "<div class='panel panel-default'><div class='panel-heading'><h3 class='panel-title'><?= $reports[$i]->ReportDescription ?></h3></div><div class="panel-body"><img id='spinner<?= $i + 1 ?>' class='spinner' src='images/loading.gif' /> <div id='reportContainer<?= $i + 1 ?>' style='display: none'></div></div></div>";
       <?php
    }
}
?>
            }

            function reportRequestStateChanged()
            {
                for (var i = 0; i < <?= $MAX_DASHBOARDS ?>; i++) {
                    if (reportRequest[i] && reportRequest[i].readyState == 4)
                    {
                        var cont = document.getElementById("reportContainer" + (i + 1));
                        cont.innerHTML = reportRequest[i].responseText;
                        cont.style.display = "";
                        document.getElementById("spinner" + (i + 1)).style.display = "none";

                        var arr = cont.getElementsByTagName('script')
                        for (var n = 0; n < arr.length; n++)
                            eval(arr[n].innerHTML)//run script inside div
                    }
                }
            }

        </script>

        <?php include("components/bootstrap.php") ?>

    </head>
    <body bgcolor="#FFFFFF" onload="LoadDashboards();">
        <?php include("components/header.php") ?>
        <?php
        $CURRENT_PAGE = "Home";
        include("components/navbar.php");
        ?>
        <?php if (isset($_GET['message'])): ?>
            <div class="container">
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?= $_GET['message']; ?>
                </div>
            </div>
        <?php endif; ?>

        <div id="page_content" class="row">
            <div id="main_content" class="col-md-offset-3 col-md-6">
                <div id="dashboard_container">


                    <div class="row">
                        <div id="DashboardDiv1" class="container col-md-6">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Report Panel</h3>
                                </div>
                                <div class="panel-body">
                                    <img class="spinner" src="images/loading.gif" />
                                </div>
                            </div>
                        </div>
                        <div id="DashboardDiv2" class="container col-md-6">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Report Panel</h3>
                                </div>
                                <div class="panel-body">
                                    <img class="spinner" src="images/loading.gif" />
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div id="DashboardDiv3" class="container col-md-6">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Report Panel</h3>
                                </div>
                                <div class="panel-body">
                                    <img class="spinner" src="images/loading.gif" />
                                </div>
                            </div>
                        </div>
                        <div id="DashboardDiv4" class="container col-md-3">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Report Panel</h3>
                                </div>
                                <div class="panel-body">
                                    <img class="spinner" src="images/loading.gif" />
                                </div>
                            </div>
                        </div>
                        <div id="DashboardDiv5" class="container col-md-3">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Report Panel</h3>
                                </div>
                                <div class="panel-body">
                                    <img class="spinner" src="images/loading.gif" />
                                </div>
                            </div>
                        </div>
                    </div>

<div class="row">
  <div class="col-md-2">
    <div class="input-group">
<label class="control-label" for="FilterDashboardStart">From:</label>
      <input type="text" class="form-control col-md-3" name="FilterDashboardStart" class="datepicker" id="FilterDashboardStart" value="<?= date("m/d/Y", strtotime("-1 month")); ?>" /> 
    </div><!-- /input-group -->
  </div><!-- /.col-md-2 -->
  <div class="col-md-2">
    <div class="input-group">
<label class="control-label" for="FilterDashboardEnd">To:</label>
      <input type="text" class="form-control col-md-3" name="FilterDashboardEnd" class="datepicker" id="FilterDashboardEnd" value="<?= date("m/d/Y"); ?>" />
    </div><!-- /input-group -->
  </div><!-- /.col-md-2 -->
  <div class="col-md-1">
    <div class="input-group">
<label class="control-label" for="Button">&nbsp;</label>
      <input type="button" class="btn btn-default" value="Go" onclick="LoadDashboardReports();"/>
    </div><!-- /input-group -->
  </div><!-- /.col-md-1 -->
</div><!-- /.row -->

                </div>
            </div>
            
        </div>
        <?php include("components/footer.php") ?>
    </body>
</html>