<?php
	require("../printable/include/mysql.inc.php");
	require("../printable/include/printable.inc.php");
	require("globals.php");
		


	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	
	$portal = new PrintablePortal($COMPANY_ID, $db);
	$portal->ApprovalEmails = $APPROVAL_EMAIL;
		
	$currentUser = $portal->GetUser($_SESSION['currentuserid']);
		
	// Check login
	if(!$currentUser)
	{
		header( "Location: " . $portal->CurrentCompany->Website . "login.php?message=" . urlencode( "Not logged in or login error.  Please try again." ) );
		die();
	}
	
	$file = $portal->GetFile($HTTP_GET_VARS['fileid']);
		
	if($file)
	{
		$portal->LogFileAccess($file->FileID, $currentUser->UserID);
		
			$ext = substr(strrchr($file->FileName, "."), 1);

		if($file->FileType == 1)
		{
			$filepath = "{$FILES_LOCATION}" . $portal->CurrentCompany->CompanyID . "/files/{$file->FilePath}";
			
			$fd=fopen($filepath,"rb");
			if(!$fd)
			{
				echo "Unable to open file! $filepath";
				die();
			}
			
			if (strcasecmp($ext, 'html') == 0)
			{
			}
			elseif($HTTP_GET_VARS['view'] != 1)
			{
				//do download
				header("Pragma: public");
				header("Expires: 0");
				header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
				header('Content-type: application/force-download');
				header("Content-Transfer-Encoding: binary");
				header("Content-Disposition: attachment; filename=\"{$file->FileName}\";" );
				header("Content-Transfer-Encoding: binary");
				header("Content-Length: ".filesize($filepath));
			}
			else 
			{
				//do download
				header("Pragma: public");
				header("Expires: 0");
				header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
				switch(strtolower($ext))
				{
					case "doc":
						header("Content-Disposition: attachment; filename=\"{$file->FileName}\";" );
						header("Content-type: application/msword");
						break;
					case "pdf":
						header("Content-type: application/pdf");
						break;
					case "mov":
						header("Content-type: video/quicktime");
						break;
					case "ppt":
						header("Content-type: application/mspowerpoint");
						header("Content-Disposition: attachment; filename=\"{$file->FileName}\";" );
						break;
					case "png":
						header("Content-type: image/png");
						break;					
					case "jpg":
					case "jpeg":
						header("Content-type: image/jpeg");
						break;					
					case "gif":
						header("Content-type: image/gif");
						break;					
				}
				header("Content-Transfer-Encoding: binary");
				header("Content-Length: ".filesize($filepath));
			}
			
			
	        while (!feof($fd)) {
	            print(fread($fd,8192));
	        }
	        fclose($fd);
		}
		elseif ($file->FileType == 2)
		{
?>
<html>
	<head>
	</head>
	<body>
		<?= $file->FilePath ?>
	</body>
</html>
<?php			
		}
        
	}
	else 
	{
		echo "Invalid File!";
		die();
	}
	if (strcasecmp($ext, 'html') != 0 && $file->FileType != 2)
	{
?>
<html>
<head>
	<title>Download Attachment</title>
</head>
<body onload=""></body>
</html>
<?php
	}
?>