<?php
	require("../printable/include/mysql.inc.php");
	require("../printable/include/optimize.printable.inc.php");
	require("globals.php");
	require '../printable/include/component/twitter/codebird.php';
	
	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	
	$portal = new OptimizePortal($COMPANY_ID, $db);
	
	$currentUser = $portal->UserAccess($_SESSION['currentuserid']);
	$isSuper = $portal->CheckPriv($currentUser->UserID, 'supervisor');
			
	if(!$currentUser)
	{
		die("Not logged in or login error.");
	}
	
	\Codebird\Codebird::setConsumerKey($TWITTER_KEY, $TWITTER_SECRET);
	
	$cb = \Codebird\Codebird::getInstance();
	
	if (!isset($_SESSION['twitter_oauth_token'])) {
		if(strlen($currentUser->TwitterToken) <= 0) {
			// get the request token
			$reply = $cb->oauth_requestToken(array(
				'oauth_callback' => 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']
			));

			// store the token
			$cb->setToken($reply->oauth_token, $reply->oauth_token_secret);
			$_SESSION['twitter_oauth_token'] = $reply->oauth_token;
			$_SESSION['twitter_oauth_token_secret'] = $reply->oauth_token_secret;
			$_SESSION['twitter_oauth_verify'] = true;

			// redirect to auth website
			$auth_url = $cb->oauth_authorize();
			header('Location: ' . $auth_url);
			die();
		} else {
			// Authorize User
			list($t,$s) = explode(":",$currentUser->TwitterToken);
			$cb->setToken($t, $s);
			$reply = $cb->account_verifyCredentials();
			
			// if authorized set session
			if($reply->httpstatus != '401') {
				$_SESSION['twitter_oauth_token'] = $t;
				$_SESSION['twitter_oauth_token_secret'] = $s;
			}
			// else clear credentials
			else {
				$currentUser->TwitterToken = "";
				$portal->UpdateUser($currentUser);
			}
			
			header('Location: ' . basename(__FILE__));
			die();
		}

	} elseif (isset($_GET['oauth_verifier']) && isset($_SESSION['twitter_oauth_verify'])) {
		// verify the token
		$cb->setToken($_SESSION['twitter_oauth_token'], $_SESSION['twitter_oauth_token_secret']);
		unset($_SESSION['twitter_oauth_verify']);

		// get the access token
		$reply = $cb->oauth_accessToken(array(
			'oauth_verifier' => $_GET['oauth_verifier']
		));

		// store the token (which is different from the request token!)
		$_SESSION['twitter_oauth_token'] = $reply->oauth_token;
		$_SESSION['twitter_oauth_token_secret'] = $reply->oauth_token_secret;
		
		// store the token with the user
		$currentUser->TwitterToken = $reply->oauth_token . ":" . $reply->oauth_token_secret;
		$portal->UpdateUser($currentUser);

		// send to same URL, without oauth GET parameters
		header('Location: ' . basename(__FILE__));
		die();
	}

	// assign access token on each page load
	$cb->setToken($_SESSION['twitter_oauth_token'], $_SESSION['twitter_oauth_token_secret']);
	
	if($_POST['submit'] == 'Tweet') {
		$reply = $cb->statuses_update('status=' . $_POST['post_text']);
		
		var_dump($reply);

	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
	<title>
		OptifiNow :: Publish onDemand
	</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />		
	<link type="text/css" media="all" rel="stylesheet" href="style.css" />
	<script  src="js/func.js"></script>	

</head>
<body bgcolor="#FFFFFF" onload="">
<form action="<?= $_SERVER['PHP_SELF'] ?>" method="POST">
	<b>Post:</b><br/>
	<textarea name="post_text"></textarea>
	<input type="submit" value="Tweet" name="submit"/>
</form>
</body>
</html>