<?php
require("../printable/include/mysql.inc.php");
require("../printable/include/optimize.printable.inc.php");
require("globals.php");

$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);

$portal = new OptimizePortal($COMPANY_ID, $db);

$currentUser = $portal->UserAccess($_SESSION['currentuserid']);
$isSubAdmin = $portal->CheckPriv($currentUser->UserID, 'subadmin');

// Check login
if (!$isSubAdmin) {
    header("Location: " . $portal->CurrentCompany->Website . "login.php?message=" . urlencode("Not logged in or login error.  Please try again."));
    die();
}

if (!isset($_GET['section'])) {
    $_GET['section'] = 0;
}

// Get Current Category
$currentCategory = $portal->GetSocialCategory($_GET['catid']);

if (!$currentCategory) {
    echo "Invalid Category";
    die();
}

if ($_POST['Submit'] == 'Update') {
    $currentCategory->CategoryName = $_POST['CategoryName'];
    $currentCategory->CategoryDesc = $_POST['CategoryDesc'];
    $currentCategory->ParentCategoryID = $_POST['ParentCategoryID'];
    if ($portal->CheckPriv($currentUser->UserID, 'admin'))
        $currentCategory->GroupID = $_POST['GroupID'];

    $portal->UpdateSocialCategory($currentCategory);

    header("Location: manage_socialcats.php?section=" . $_GET['section'] . "&message=" . urlencode("Category Updated."));
    die();
}
elseif ($_POST['Submit'] == 'Cancel') {
    header("Location: manage_socialcats.php?section=" . $_GET['section'] . "&message=" . urlencode("Action Canceled. Folder not updated."));
    die();
} elseif ($_POST['Submit'] == 'Delete' && $currentCategory->CategoryID != 69) {
    $portal->DeleteCategory($currentCategory);

    header("Location: manage_socialcats.php?section=" . $_GET['section'] . "&message=" . urlencode("Category Deleted."));
    die();
}

$categories = $portal->GetSocialCategories('all', $_GET['section'], true, 'all');
$gs = $portal->GetCompanyGroups();

$groups = array();

$groups[0] = new Group();
$groups[0]->GroupID = 0;
$groups[0]->GroupName = "All";

foreach ($gs as $g) {
    $groups[$g->GroupID] = $g;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <title>
            <?= $portal->CurrentCompany->CompanyName ?> :: Edit / Create Social Category
        </title>
        <meta http-equiv="content-type" content="text/html;charset=utf-8" />		
        <script  src="js/func.js"></script>	
        <?php include("components/bootstrap.php") ?>
    </head>
    <body bgcolor="#FFFFFF">
        <div id="page">
            <?php include("components/header.php") ?>
            <div id="body">
                <?php
                $CURRENT_PAGE = "Home";
                include("components/navbar.php");
                ?>
                <form method="post" action="<?= $_SERVER['PHP_SELF'] ?>?catid=<?= $currentCategory->SocialCategoryID ?>&section=<?= $_GET['section'] ?>">
                    <?php if (isset($_GET['message'])): ?>
                        <div class="container">
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <?= $_GET['message']; ?>
                            </div>
                        </div>
                    <?php endif; ?> 
                    <div id="SocialCatDiv" class="well container">
                        <div class="sectionHeader"><h2>Edit/Create Social Category</h2></div>
                        <div class="sectionDiv">
                            <div class="itemSection row">
                                <div id="CategoryNameDiv" class="form-group col-md-3">
                                    <label for="CategoryName">Category Name:</label><br/>
                                    <input type="text" class="form-control input-sm" name="CategoryName" value="<?= $currentCategory->CategoryName ?>" size="45"/>
                                </div>
                            </div>
                            <div class="itemSectionLong row">
                                <div id="CategoryDescDiv" class="form-group col-md-6">
                                    <label for="CategoryDesc">Description:</label><br/>
                                    <textarea class="form-control input-sm" name="CategoryDesc" rows="4" cols="48"><?= $currentCategory->CategoryDesc ?></textarea>
                                </div>
                            </div>
                            <div class="itemSection row">
                                <div id="ParentDiv" class="form-group col-md-3">
                                    <label for="ParentName">Parent Category:</label><br/>
                                    <select name="ParentCategoryID"  class="form-control input-sm">
                                        <option value="0"> - Root - </option>
                                        <?php
                                        $category = new SocialCategory();
                                        foreach ($categories as $category) {
                                            if ($category->SocialCategoryID != $currentCategory->SocialCategoryID && ($portal->CheckPriv($currentUser->UserID, 'admin') || $category->GroupID != 0)) {
                                                ?>
                                                <option value="<?= $category->SocialCategoryID ?>" <?= $category->SocialCategoryID == $currentCategory->ParentCategoryID ? "SELECTED" : "" ?>>
                                                    <?= str_repeat('&nbsp;&nbsp;&nbsp;', $portal->GetSocialCategoryLevel($category->SocialCategoryID)) . $category->CategoryName ?>
                                                </option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <?php
                            if ($portal->CheckPriv($currentUser->UserID, 'admin')) {
                                ?>
                                <div class="itemSection row">
                                    <div id="ParentDiv" class="form-group col-md-3">
                                        <label for="GroupID">Group:</label><br/>
                                        <select class="form-control input-sm" name="GroupID">
                                            <?php
                                            foreach ($groups as $g) {
                                                ?>
                                                <option value="<?= $g->GroupID ?>" <?= $g->GroupID == $currentCategory->GroupID ? "SELECTED" : "" ?>>
                                                    <?= $g->GroupName ?>
                                                </option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>
                            <div class="itemSection row">
                                <div class="buttonSection">
                                    <input type="submit" class="btn btn-info btn-sm" value="Update" name="Submit"/>&nbsp;&nbsp;<input type="submit" value="Cancel" class="btn btn-default btn-sm" name="Submit"/>
                                </div>
                            </div>

                            <div class="edit-date pull-right">
                                <small>
                                    <?php if ($currentCategory->DateCreated): ?>
                                        created: <?= date("m/d/Y, g:iA", strtotime($currentCategory->DateCreated)); ?> </br>
                                    <?php endif; ?>
                                    <?php if (strtotime($currentCategory->DateModified) > strtotime($currentCategory->DateCreated)): ?>
                                        last modified: <?= date("m/d/Y, g:iA", strtotime($currentCategory->DateModified)); ?>
                                    <?php endif; ?>
                                </small>
                            </div>
                        </div>

                    </div>
                </form>

            </div>
        </div>
        <?php include("components/footer.php") ?>
    </body>
</html>