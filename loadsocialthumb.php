<?php

require("../printable/include/mysql.inc.php");
require("../printable/include/optimize.printable.inc.php");
require("globals.php");
require("filefunctions.php");

$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);

$portal = new OptimizePortal($COMPANY_ID, $db);

$currentUser = $portal->GetUser($_SESSION['currentuserid']);

// Check login
if (!$currentUser) {
    header("Location: login.php?message=" . urlencode("Not logged in or login error.  Please try again."));
    die();
}

$posting = $portal->GetPosting($_GET['postingid']);

$fileurl = $FILES_LOCATION . $_SESSION['currentcompanyid'] . "/social/" . $posting->ImageURL;

if (is_file($fileurl)) {

    //$finfo = finfo_open(FILEINFO_MIME_TYPE); PHP 5.3
    //$mime = finfo_file($finfo, $fileurl); PHP 5.3

	//PHP 5.2 fix
	$ext = pathinfo($posting->ImageURL, PATHINFO_EXTENSION);

	switch ($ext) {

	case "png": $mime = "image/png"; break;
	case "gif": $mime = "image/gif"; break;
	case "jpeg":
	case "jpg":
		     $mime = "image/jpeg"; break;
	}
	

    header("Content-type: " . $mime);

    if ($mime == 'image/jpeg' || $mime == 'image/jpg') {
        $im = imagecreatefromjpeg($fileurl);
    } else if ($mime == 'image/png') {
        $im = imagecreatefrompng($fileurl);
    } else if ($mime == 'image/gif') {
        $im = imagecreatefromgif($fileurl);
    }

    if ($im) {
        if ($_GET['thumb'] == 'true' || is_numeric($_GET['fitwidth'])) {
// Get Width and hieght of image
            $origWidth = imagesx($im);
            $origHeight = imagesy($im);

            if ($origWidth > $origHeight) {
                $width = (is_numeric($HTTP_GET_VARS['fitwidth']) ? $_GET['fitwidth'] : 58);
                $ratio = $width / $origWidth;
                $height = floor($origHeight * $ratio);
            } else {
                $height = (is_numeric($HTTP_GET_VARS['fitwidth']) ? $_GET['fitwidth'] : 58);
                $ratio = $height / $origHeight;
                $width = floor($origWidth * $ratio);
            }

            $imOut = imagecreatetruecolor($width, $height);

            imagecopyresampled($imOut, $im, 0, 0, 0, 0, $width, $height, $origWidth, $origHeight);

            if ($mime == 'image/jpeg' || $mime == 'image/jpg') {
                ImageJpeg($imOut);
            } else if ($mime == 'image/png') {
                ImagePng($imOut);
            } else if ($mime == 'image/gif') {
                ImageGif($imOut);
            }
            //ImageJpeg($imOut);
            ImageDestroy($imOut);
            ImageDestroy($im);
        } else {
            if ($mime == 'image/jpeg' || $mime == 'image/jpg') {
                ImageJpeg($im);
            } else if ($mime == 'image/png') {
                ImagePng($im);
            } else if ($mime == 'image/gif') {
                ImageGif($im);
            }
            //ImageJpeg($im);
            ImageDestroy($im);
        }
    } else {
        if ($_GET['thumb'] == 'true' || is_numeric($_GET['fitwidth'])) {
            $width = (is_numeric($_GET['fitwidth']) ? $_GET['fitwidth'] : 50);
            $height = 50;
//Create the image resource 
            $image = ImageCreate($width, $height);

//Make the background black 
            ImageFill($image, 0, 0, ImageColorAllocate($image, 255, 255, 255));

//Add randomly generated string in white to the image
            ImageString($image, 2, 1, 12, "Invalid", ImageColorAllocate($image, 0, 0, 0));
            ImageString($image, 2, 1, 24, "Thumb", ImageColorAllocate($image, 0, 0, 0));
        } else {
            $width = 200;
            $height = 200;
//Create the image resource 
            $image = ImageCreate($width, $height);

//Make the background black 
            ImageFill($image, 0, 0, ImageColorAllocate($image, 255, 255, 255));

//Add randomly generated string in white to the image
            ImageString($image, 3, 30, 3, "Invalid Thumb", ImageColorAllocate($image, 0, 0, 0));
        }

//Output the newly created image in jpeg format 
        //       ImageJpeg($image);
        if ($mime == 'image/jpeg' || $mime == 'image/jpg') {
            ImageJpeg($image);
        } else if ($mime == 'image/png') {
            ImagePng($image);
        } else if ($mime == 'image/gif') {
            ImageGif($image);
        }

//Free up resources
        ImageDestroy($image);
    }
} else {
    $width = 1;
    $height = 1;
//Create the image resource 
    $image = ImageCreate($width, $height);

//Make the background black 
    ImageFill($image, 0, 0, ImageColorAllocate($image, 255, 255, 255));

    header("Content-type: 'image/png'");

//Output the newly created image in jpeg format 
    //ImageJpeg($image);
    ImagePng($image);

//Free up resources
    ImageDestroy($image);
}
?>