<?php

	$demoLookup = 
		array( 'Initial Contact' => 
					array('Direct Mail' => 
								array('Company overview' => 'postcard_mortgage.jpg',
										'OptifiNow features' => 'flyer_glance.jpg'),
							'Email' =>
								array('Thanks for call' => 'postcard_missed.jpg',
										'Sorry I missed you' => 'postcard_message.jpg'),
							'Case Study' =>
								array('Marketing automation' => 'brochure_marketing2.jpg',
										'Big data is here to stay' => 'brochure_content2.jpg'),
							'Webinar' =>
								array('The future of automation' => 'brochure_publish2.jpg'),
							'Infographic' =>
								array('Life of a lead' => 'brochure_leads3.jpg',
										'Life of a prospect' => 'brochure_sales3.jpg'),
							'Presentation' =>
								array('OptifiNow overview' => 'postcard_mortgage.jpg',
										'Leads on Demand overview' => 'postcard_banking.jpg'),
							'Whitepaper' =>
								array(),
							'Newsletter' =>
								array('General News' => 'brochure_sales2.jpg'),
							'Prod. Flyer' =>
								array('OptifiNow at a glance' => 'flyer_glance.jpg',
										'Top advantages of OptifiNow' => 'brochure_publish3.jpg')
					),
				'Solution Proposed' => 
					array('Direct Mail' => 
								array('Product overview' => 'flyer_glance.jpg',
										'User adoption' => 'socialcollab.jpg'),
							'Email' =>
								array('Value proposition' => 'brochure_sales.jpg',
										'OptiFiNow generates greater ROI' => 'brochure_reports.jpg'),
							'Case Study' =>
								array('Customer engagement' => 'brochure_marketing.jpg'),
							'Webinar' =>
								array('Reporting tools' => 'brochure_reports3.jpg',
										'Drive effective marketing' => 'brochure_marketing2.jpg'),
							'Infographic' =>
								array('New marketing life cycle' => 'brochure_marketing3.jpg',
										'Big data tamed' => 'brochure_content3.jpg'),
							'Presentation' =>
								array('How companies drive ROI ' => 'brochure_reports.jpg',
										'Growing customercentricity' => 'brochure_publish3.jpg'),
							'Whitepaper' =>
								array(),
							'Newsletter' =>
								array('General news' => 'flyer_glance.jpg'),
							'Prod. Flyer' =>
								array('Competitive advantage' => 'brochure_reports2.jpg',
										'Social publishing' => 'brochure_collaboration3.jpg')
					),
				'Review Solution' => 
					array('Direct Mail' => 
								array('Company overview' => 'postcard_mortgage.jpg',
										'OptifiNow features' => 'flyer_glance.jpg'),
							'Email' =>
								array('Invite to conference' => 'brochure_collaboration3.jpg',
										'Top reasons companies switch' => 'brochure_marketing.jpg'),
							'Case Study' =>
								array('Marketing automation' => 'brochure_marketing2.jpg',
										'Big data is here to stay' => 'brochure_content2.jpg'),
							'Webinar' =>
								array('The future of automation' => 'brochure_publish2.jpg'),
							'Infographic' =>
								array('Big data big profits' => 'brochure_leads2.jpg'),
							'Presentation' =>
								array('Closing the revenue gap' => 'brochure_sales3.jpg'),
							'Whitepaper' =>
								array('Gartner group' => 'brochure_reports.jpg',
										'Where is the cloud going' => 'brochure_content2.jpg'),
							'Newsletter' =>
								array('General news' => 'flyer_glance.jpg'),
							'Prod. Flyer' =>
								array('New features coming soon' => 'brochure_collaboration.jpg')
					),
				'Deliver Solution' => 
					array('Direct Mail' => 
								array('Future landscape' => 'postcard_mortgage2.jpg'),
							'Email' =>
								array('Value proposition' => 'brochure_sales.jpg',
										'OptiFiNow generates greater ROI' => 'brochure_reports.jpg'),
							'Case Study' =>
								array('Customer engagement' => 'brochure_marketing.jpg'),
							'Webinar' =>
								array('Hear a client speak' => 'brochure_reports2.jpg'),
							'Infographic' =>
								array(),
							'Presentation' =>
								array(),
							'Whitepaper' =>
								array(),
							'Newsletter' =>
								array(),
							'Prod. Flyer' =>
								array()
					),
				'Complete Sale' => 
					array('Direct Mail' => 
								array('Thank you' => 'postcard_mortgage.jpg'),
							'Email' =>
								array('What other clients have said' => 'brochure_reports2.jpg',
										'Hear is your service team' => 'brochure_sales.jpg',
										'Complete this survey' => 'brochure_sales4.jpg'),
							'Case Study' =>
								array('Marketing automation' => 'brochure_marketing2.jpg',
										'Big data is here to stay' => 'brochure_content2.jpg'),
							'Webinar' =>
								array('Reporting tools' => 'brochure_reports3.jpg',
										'Drive effective marketing' => 'brochure_marketing2.jpg'),
							'Infographic' =>
								array(),
							'Presentation' =>
								array(),
							'Whitepaper' =>
								array(),
							'Newsletter' =>
								array(),
							'Prod. Flyer' =>
								array('You are invited to our client counsel dinner' => 'postcard_banking2.jpg')
					),
		);
										