<?php
require("../printable/include/mysql.inc.php");
require("../printable/include/optimize.printable.inc.php");
require("globals.php");

$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);

$portal = new OptimizePortal($COMPANY_ID, $db);

$currentUser = $portal->GetUser($_SESSION['currentuserid']);

$isSubAdmin = $portal->CheckPriv($currentUser->UserID, 'subadmin');

if (!$isSubAdmin) {
    header("Location: " . $portal->CurrentCompany->Website . "home.php?message=" . urlencode("Accessed Denied."));
    die();
}

$campaign = $portal->GetSubmissionListingType($_GET['id']);
$groups = $portal->GetCompanyGroups();
$sources = $portal->GetSubmissionListingSources();
$extensions = $portal->GetExtensions();
$statuses = $portal->GetContactStatusTypes('all', 1, true);

$eLookup = array();
foreach ($extensions as $e) {
    $eLookup[$e->Extension] = $e;
}

if (!$campaign) {
    header("Location: manage_campaigns.php?message=" . urlencode("Invalid Campaign ID"));
    die();
}

if ($_POST['Submit'] == 'Cancel') {
    header('Location: manage_campaigns.php?message=' . urlencode("Action Canceled. Campaign not updated."));
    die();
} elseif ($_GET['disable'] == 'true') {
    $campaign->Disabled = 1;

    $portal->UpdateSubmissionListingType($campaign);

    header('Location: manage_campaigns.php?message=' . urlencode("Action Successful. Campaign disabled."));
    die();
} elseif ($_GET['enable'] == 'true') {
    $campaign->Disabled = 0;

    $portal->UpdateSubmissionListingType($campaign);

    header('Location: manage_campaigns.php?message=' . urlencode("Action Successful. Campaign enabled."));
    die();
} elseif ($_POST['ListingSourceName']) {
    $campaign->ListingType = $_POST['ListingSourceName'];
    $campaign->ListingDescription = $_POST['ListingDescription'];
    $campaign->SubmissionListingSourceID = $_POST['SubmissionListingSourceID'];
    $campaign->OutsideID = $_POST['OutsideID'];
    $campaign->DefaultContactStatusTypeID = $_POST['DefaultContactStatusTypeID'];
    $campaign->LeadCost = $_POST['LeadCost'];
    $campaign->Disabled = $_POST['Disabled'];
    $campaign->ListingClass = $_POST['ListingClass'];
    $campaign->RegionID = $_POST['RegionID'];
    $campaign->DivisionID = $_POST['DivisionID'];
    $campaign->GroupID = $_POST['GroupID'];

    $campaign->Extensions = explode(",", str_replace("ext_", "", $_POST['ExtensionList']), -1);

    $portal->UpdateSubmissionListingType($campaign);

    header('Location: manage_campaigns.php?message=' . urlencode("Action Successful. Campaign updated."));
    die();
}

$regions = $portal->GetCompanyRegions();
$divisions = $portal->GetCompanyDivisions();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title>
        <?= $portal->CurrentCompany->CompanyName ?> :: Edit Campaign
    </title>
    <meta http-equiv="content-type" content="text/html;charset=utf-8"/>
    <link type="text/css" media="all" rel="stylesheet" href="css/edit_campaign.css"/>
    <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico"/>
    <script src="js/func.js"></script>


    <link rel="stylesheet" type="text/css"
          href="https://ajax.googleapis.com/ajax/libs/yui/2.8.1/build/fonts/fonts-min.css"/>
    <script type="text/javascript"
            src="https://ajax.googleapis.com/ajax/libs/yui/2.8.1/build/yahoo-dom-event/yahoo-dom-event.js"></script>
    <script type="text/javascript"
            src="https://ajax.googleapis.com/ajax/libs/yui/2.8.1/build/animation/animation-min.js"></script>
    <script type="text/javascript"
            src="https://ajax.googleapis.com/ajax/libs/yui/2.8.1/build/dragdrop/dragdrop-min.js"></script>

    <?php include("components/bootstrap.php") ?>

</head>
<body bgcolor="#FFFFFF">
<?php include("components/header.php") ?>
<div id="body">
    <?php
    $CURRENT_PAGE = "Home";
    include("components/navbar.php");
    ?>
    <form method="post" action="<?= $_SERVER['PHP_SELF'] ?>?id=<?= $campaign->SubmissionListingTypeID; ?>"
          id="MainForm">

        <?php if (isset($_GET['message'])): ?>
            <div class="container">
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?= $_GET['message']; ?>
                </div>
            </div>
        <?php endif; ?>

        <div id="CampaignInfoDiv" class="container well">
            <div class="sectionHeader"><h2>Edit Campaign</h2></div>
            <div id="CampaignDiv" class="longSectionDiv">
                <div class="itemSection row">
                    <div id="ListingSourceNameDiv" class="form-group col-md-3">
                        <label for="ListingSourceName">Campaign Display Name:</label><br/>
                        <input type="text" class="form-control input-sm" id="ListingSourceName" name="ListingSourceName"
                               value="<?= $campaign->ListingType ?>"/>
                    </div>
                </div>
                <div class="itemSection row">
                    <div id="ListingDescriptionDiv" class="form-group col-md-3">
                        <label for="ListingDescription">Campaign Report Name:</label><br/>
                        <input type="text" class="form-control input-sm" id="ListingDescription"
                               name="ListingDescription" value="<?= $campaign->ListingDescription ?>"/>
                    </div>
                </div>
                <div class="itemSection row">
                    <div id="SubmissionListingSourceIDDiv" class="form-group col-md-3">
                        <label for="SubmissionListingSourceID">Lead Source:</label><br/>
                        <select class="form-control input-sm" id="SubmissionListingSourceID"
                                name="SubmissionListingSourceID">
                            <option value="9999"
                                <?= $campaign->SubmissionListingSourceID == 9999 ? "SELECTED" : "" ?>>
                                Manual Lead Source
                            </option>
                            <?php
                            foreach ($sources as $s) {
                                ?>
                                <option value="<?= $s->SubmissionListingSourceID ?>"
                                    <?= $s->SubmissionListingSourceID == $campaign->SubmissionListingSourceID ? "SELECTED" : "" ?>>
                                    <?= $s->ListingSourceName ?>
                                </option>
                            <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="itemSection row">
                    <div id="OutsideIDDiv" class="form-group col-md-3">
                        <label for="OutsideID">Campaign ID:</label><br/>
                        <input type="text" class="form-control input-sm" id="OutsideID" name="OutsideID"
                               style="width: 180px" value="<?= $campaign->OutsideID ?>"/>
                    </div>
                    <div id="ListingClassDiv" class="form-group col-md-3">
                        <label for="ListingClass">Type:</label><br/>
                        <select id="ListingClass" name="ListingClass" class="form-control input-sm">
                            <option value=""></option>
                            <?php
                            foreach ($leadClasses as $lc) {
                                ?>
                                <option value="<?= $lc ?>"
                                    <?= $lc == $campaign->ListingClass ? "SELECTED" : "" ?>>
                                    <?= $lc ?>
                                </option>
                            <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="itemSection row">
                    <div id="LeadCostDiv" class="form-group col-md-3">
                        <label for="LeadCost">Lead Cost:</label><br/>
                        <input type="text" class="form-control input-sm" id="LeadCost" name="LeadCost"
                               style="width: 40px" value="<?= $campaign->LeadCost ?>"/>
                    </div>
                    <div id="DisabledDiv" class="form-group col-md-3">
                        <label for="LeadCost">Campaign Disabled:</label><br/>
                        <select class="form-control input-sm" name="Disabled" id="Disabled">
                            <option value="0" <?= $campaign->Disabled == 0 ? "SELECTED" : "" ?>>No</option>
                            <option value="1"<?= $campaign->Disabled == 1 ? "SELECTED" : "" ?>>Yes</option>
                        </select>
                    </div>
                </div>
                <div class="itemSection row">
                    <div id="DefaultContactStatusTypeIDDiv" class="form-group col-md-3">
                        <label for="DefaultContactStatusTypeID">Default Status:</label><br/>
                        <select class="form-control input-sm" id="DefaultContactStatusTypeID"
                                name="DefaultContactStatusTypeID">
                            <?php
                            foreach ($statuses as $st) {
                                ?>
                                <option value="<?= $st->ContactStatusTypeID ?>"
                                    <?= $st->ContactStatusTypeID == $campaign->DefaultContactStatusTypeID ? "SELECTED" : "" ?>>
                                    <?= $st->ContactStatus ?>
                                </option>
                            <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="itemSection row">
                    <div id="RegionIDDiv" class="form-group col-md-3">
                        <label for="RegionID">Region:</label><br/>
                        <select class="form-control input-sm" id="RegionID" name="RegionID">
                            <option value="0">All</option>
                            <?php
                            foreach ($regions as $r) {
                                ?>
                                <option value="<?= $r->RegionID ?>"
                                    <?= $r->RegionID == $campaign->RegionID ? "SELECTED" : "" ?>>
                                    <?= $r->RegionName ?>
                                </option>
                            <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div id="DivisionIDDiv" class="form-group col-md-3">
                        <label for="DivisionID">Division:</label><br/>
                        <select class="form-control input-sm" id="DivisionID" name="DivisionID">
                            <option value="0">All</option>
                            <?php
                            foreach ($divisions as $d) {
                                ?>
                                <option value="<?= $d->DivisionID ?>"
                                    <?= $d->DivisionID == $campaign->DivisionID ? "SELECTED" : "" ?>>
                                    <?= $d->DivisionName ?>
                                </option>
                            <?php
                            }
                            ?>
                        </select>
                    </div>

                    <div id="GroupIDDiv" class="form-group col-md-3">
                        <label for="GroupID">Group:</label><br/>
                        <select class="form-control input-sm" id="GroupID" name="GroupID">
                            <option value="0">All</option>
                            <?php
                            foreach ($groups as $g) {
                                ?>
                                <option value="<?= $g->GroupID ?>"
                                    <?= $g->GroupID == $campaign->GroupID ? "SELECTED" : "" ?>>
                                    <?= $g->GroupName ?>
                                </option>
                            <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div id="ExtensionListDiv" class="row">
                    <div class="workarea" class="form-group col-md-6">
                        <div class="categoryHeader">Available Extensions</div>
                        <ul id="avail_exts" class="draglist" align="left">
                            <?php
                            foreach ($extensions as $ext) {
                                if (array_search($ext->Extension, $campaign->Extensions) === false) {
                                    ?>
                                    <li class="list1"
                                        id="ext_<?= $ext->Extension ?>"><?= "$ext->ExtensionName ($ext->Extension)" ?></li>
                                <?php
                                }
                            }
                            ?>
                        </ul>
                    </div>
                    <div class="workarea" class="form-group col-md-6">
                        <div class="categoryHeader">Selected Extensions</div>
                        <ul id="selected_exts" class="draglist" align="left">
                            <?php
                            $selectedList = "";
                            foreach ($campaign->Extensions as $ext) {
                                $selectedList .= "ext_$ext,";
                                ?>
                                <li class="list1"
                                    id="ext_<?= $ext ?>"><?= $eLookup[$ext]->ExtensionName . "($ext)" ?></li>
                            <?php
                            }
                            ?>
                        </ul>
                        <input type="hidden" id="ExtensionList" name="ExtensionList" value="<?= $selectedList ?>"/>
                    </div>
                </div>
            </div>
            <center>
                <div class="itemSection row">
                    <div class="buttonSection">
                        <input type="submit" class="btn btn-info btn-sm" value="Update" name="Submit"
                               id="updateButton"/>&nbsp;&nbsp;<input type="submit" class="btn btn-default btn-sm"
                                                                     value="Cancel" name="Submit"/>
                    </div>
                </div>
            </center>
        </div>
    </form>
</div>
<script type="text/javascript">

    (function () {

        var Dom = YAHOO.util.Dom;
        var Event = YAHOO.util.Event;
        var DDM = YAHOO.util.DragDropMgr;

        //////////////////////////////////////////////////////////////////////////////
        // example app
        //////////////////////////////////////////////////////////////////////////////
        YAHOO.example.DDApp = {
            init: function () {

                var rows = 3, cols = 2, i, j;
                new YAHOO.util.DDTarget("avail_exts");
                new YAHOO.util.DDTarget("selected_exts");

                <?php
                foreach ($campaign->Extensions as $ext) {
                    ?>
                new YAHOO.example.DDList("ext_<?= $ext ?>");
                <?php
            }
            ?>
                <?php
                foreach ($extensions as $ext) {
                    if (array_search($ext->Extension, $campaign->Extensions) === false) {
                        ?>
                new YAHOO.example.DDList("ext_<?= $ext->Extension ?>");
                <?php
            }
        }
        ?>

                Event.on("updateButton", "click", this.showOrder);
            },

            showOrder: function () {
                var selList = Dom.get("selected_exts");
                var selListString = Dom.get("ExtensionList");
                var mainForm = Dom.get("MainForm");
                var items = selList.getElementsByTagName("li");
                var newListString = "";
                for (i = 0; i < items.length; i++) {
                    newListString += items[i].id + ",";
                }
                selListString.value = newListString;

                mainForm.submit();
            }

        };

        //////////////////////////////////////////////////////////////////////////////
        // custom drag and drop implementation
        //////////////////////////////////////////////////////////////////////////////

        YAHOO.example.DDList = function (id, sGroup, config) {

            YAHOO.example.DDList.superclass.constructor.call(this, id, sGroup, config);

            this.logger = this.logger || YAHOO;
            var el = this.getDragEl();
            Dom.setStyle(el, "opacity", 0.67); // The proxy is slightly transparent

            this.goingUp = false;
            this.lastY = 0;
        };

        YAHOO.extend(YAHOO.example.DDList, YAHOO.util.DDProxy, {

            startDrag: function (x, y) {
                this.logger.log(this.id + " startDrag");

                // make the proxy look like the source element
                var dragEl = this.getDragEl();
                var clickEl = this.getEl();
                Dom.setStyle(clickEl, "visibility", "hidden");

                dragEl.innerHTML = clickEl.innerHTML;

                Dom.setStyle(dragEl, "color", Dom.getStyle(clickEl, "color"));
                Dom.setStyle(dragEl, "backgroundColor", Dom.getStyle(clickEl, "backgroundColor"));
                Dom.setStyle(dragEl, "border", "2px solid gray");
            },

            endDrag: function (e) {

                var srcEl = this.getEl();
                var proxy = this.getDragEl();

                // Show the proxy element and animate it to the src element's location
                Dom.setStyle(proxy, "visibility", "");
                var a = new YAHOO.util.Motion(
                    proxy, {
                        points: {
                            to: Dom.getXY(srcEl)
                        }
                    },
                    0.2,
                    YAHOO.util.Easing.easeOut
                )
                var proxyid = proxy.id;
                var thisid = this.id;

                // Hide the proxy and show the source element when finished with the animation
                a.onComplete.subscribe(function () {
                    Dom.setStyle(proxyid, "visibility", "hidden");
                    Dom.setStyle(thisid, "visibility", "");
                });
                a.animate();
            },

            onDragDrop: function (e, id) {

                // If there is one drop interaction, the li was dropped either on the list,
                // or it was dropped on the current location of the source element.
                if (DDM.interactionInfo.drop.length === 1) {

                    // The position of the cursor at the time of the drop (YAHOO.util.Point)
                    var pt = DDM.interactionInfo.point;

                    // The region occupied by the source element at the time of the drop
                    var region = DDM.interactionInfo.sourceRegion;

                    // Check to see if we are over the source element's location.  We will
                    // append to the bottom of the list once we are sure it was a drop in
                    // the negative space (the area of the list without any list items)
                    if (!region.intersect(pt)) {
                        var destEl = Dom.get(id);
                        var destDD = DDM.getDDById(id);
                        destEl.appendChild(this.getEl());
                        destDD.isEmpty = false;
                        DDM.refreshCache();
                    }

                }
            },

            onDrag: function (e) {

                // Keep track of the direction of the drag for use during onDragOver
                var y = Event.getPageY(e);

                if (y < this.lastY) {
                    this.goingUp = true;
                } else if (y > this.lastY) {
                    this.goingUp = false;
                }

                this.lastY = y;
            },

            onDragOver: function (e, id) {

                var srcEl = this.getEl();
                var destEl = Dom.get(id);

                // We are only concerned with list items, we ignore the dragover
                // notifications for the list.
                if (destEl.nodeName.toLowerCase() == "li") {
                    var orig_p = srcEl.parentNode;
                    var p = destEl.parentNode;

                    if (this.goingUp) {
                        p.insertBefore(srcEl, destEl); // insert above
                    } else {
                        p.insertBefore(srcEl, destEl.nextSibling); // insert below
                    }

                    DDM.refreshCache();
                }
            }
        });

        Event.onDOMReady(YAHOO.example.DDApp.init, YAHOO.example.DDApp, true);

    })();


</script>

<?php include("components/footer.php") ?>

</body>
</html>