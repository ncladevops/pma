<?php
require("../printable/include/mysql.inc.php");
require("../printable/include/optimize.printable.inc.php");
require("globals.php");

$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);

$portal = new OptimizePortal($COMPANY_ID, $db);

$prefixes = array("", "Mr.", "Mrs.", "Ms.", "Miss");

// Querry to get user info
$currentUser = $portal->GetUser($_SESSION['currentuserid']);
// Check login
if (!$currentUser) {
    header("Location: login.php?message=" . urlencode("Not logged in or login error.  Please try again."));
    die();
}

//Change page's timezone for date call functions (for default start and end date range)
$originaltimezone = date('e');
if (!empty($currentUser->UserTimezoneOffset)) {
    date_default_timezone_set($currentUser->UserTimezoneOffset); //update page timezone with the user's preferred timezone
}

$isAdmin = $portal->CheckPriv($currentUser->UserID, 'admin');

$contact = $portal->GetOptimizeContact($_GET['id'], $currentUser->UserID);

if (!$contact) {
    echo "Invalid Contact.";
    die();
}

$closeWindow = false;
$newClose = false;

// if update was pressed
if ($_POST['Submit'] == 'Submit') {
    $contact->Company = addslashes($_POST['Company']);
    $contact->Title = addslashes($_POST['Title']);
    $contact->Prefix = addslashes($_POST['Prefix']);
    $contact->FirstName = addslashes($_POST['FirstName']);
    $contact->LastName = addslashes($_POST['LastName']);
    $contact->Address1 = addslashes($_POST['Address1']);
    $contact->Address2 = addslashes($_POST['Address2']);
    $contact->Address3 = addslashes($_POST['Address3']);
    $contact->City = addslashes($_POST['City']);
    $contact->State = addslashes($_POST['State']);
    $contact->Zip = addslashes($_POST['Zip']);
    $contact->WorkPhone = addslashes($_POST['WorkPhone']);
    $contact->CellPhone = addslashes($_POST['CellPhone']);
    $contact->CompanyWebsite = addslashes($_POST['CompanyWebsite']);
    $contact->Email = addslashes($_POST['Email']);

    $contact->HomeAddress1 = addslashes($_POST['HomeAddress1']);
    $contact->HomeAddress2 = addslashes($_POST['HomeAddress2']);
    $contact->HomeAddress3 = addslashes($_POST['HomeAddress3']);
    $contact->HomeCity = addslashes($_POST['HomeCity']);
    $contact->HomeState = addslashes($_POST['HomeState']);
    $contact->HomeZip = addslashes($_POST['HomeZip']);
    if ($_POST['Phone'] == '') {
        $contact->Phone = addslashes($_POST['WorkPhone']);
    } else {
        $contact->Phone = addslashes($_POST['Phone']);
    }
    $contact->Fax = addslashes($_POST['Fax']);
    $contact->PublicContact = intval($_POST['PublicContact']);
    $contact->Category = $_POST['Category'];
    $contact->Segment = $_POST['Segment'];
    $contact->Birthdate = $_POST['Birthdate'];
    $contact->Anniversary = $_POST['Anniversary'];
    $contact->Campaign = $_POST['Campaign'];

    $contact->Comments = addslashes($_POST['Comments']);

    $portal->UpdateOptimizeContact($contact);

    $closeWindow = true;
}

$allEvents = $portal->GetContactHistory($contact->SubmissionListingID);

$categoryValues = $portal->GetLookupValues('Category', $currentUser->GroupID);
$segmentValues = $portal->GetLookupValues('Segment', $currentUser->GroupID);
$campaignValues = $portal->GetLookupValues('Campaign', $currentUser->GroupID);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <title>
            <?= $portal->CurrentCompany->CompanyName ?> :: Edit Contact Info
        </title>
        <meta http-equiv="content-type" content="text/html;charset=utf-8" />		
        <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />
        <script  src="include/js/func.js"></script>
        <script type="text/javascript">//<![CDATA[

            var cal = Calendar.setup({
            onSelect: function(cal) { cal.hide() },
            bottomBar: true,
            dateFormat : "%m/%d/%Y"
            });

            //]]></script>

        <?php include("components/bootstrap.php") ?>

    </head>
    <body onload="<?= $closeWindow ? "window.opener.location.href = window.opener.location.href;window.close()" : "" ?>">
        <form class="form-horizontal" method="post" action="<?= $_SERVER['PHP_SELF'] ?>?id=<?= $contact->SubmissionListingID ?>">
            <div class="container">
                    <ul class="nav nav-tabs" style="margin-bottom: 15px;">
                        <li class="active"><a href="#contactDiv" data-toggle="tab">Contact Info</a></li>
                        <li class=""><a href="#additionalDiv" data-toggle="tab">Additional Info</a></li>
                        <li class=""><a href="#statusDiv" data-toggle="tab">Status</a></li>
                        <li class=""><a href="#notesDiv" data-toggle="tab">Notes</a></li>
                    </ul>
                    <div id="myTabContent" class="tab-content well">
                        <div class="tab-pane fade active in" id="contactDiv">
                            <fieldset>
                                <div id="CompanyInfo" class="row form-group">
                                    <div id="CompanyDiv" class="col-xs-6">
                                        <label for="Company">Company</label>
                                        <input type="text" class="form-control input-sm" name="Company" value="<?= $contact->Company ?>" size="50" />
                                    </div>
                                    <div id="TitleDiv" class="col-xs-6">
                                        <label for="Title">Title</label>
                                        <input type="text" class="form-control input-sm" name="Title" value="<?= $contact->Title ?>" />
                                    </div>
                                </div>
                                <div class="row form-group" id="NameInfo">
                                    <div id="PrefixDiv" class="col-xs-2">
                                        <label for="Prefix">Prefix</label>
                                        <select class="form-control input-sm" name="Prefix">
                                            <?php
                                            foreach ($prefixes as $p) {
                                                ?>
                                                <option value="<?= $p ?>" <?= strcasecmp(str_replace(".", "", $p), str_replace(".", "", $contact->Prefix)) == 0 ? 'SELECTED' : "" ?>>
                                                    <?= $p ?>
                                                </option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div id="FirstNameDiv" class="col-xs-5">
                                        <label for="FirstName">First Name</label>
                                        <input type="text" class="form-control input-sm" name="FirstName" value="<?= $contact->FirstName ?>" />
                                    </div>
                                    <div id="LastNameDiv" class="col-xs-5">
                                        <label for="LastName">Last Name</label>
                                        <input type="text" class="form-control input-sm" name="LastName" value="<?= $contact->LastName ?>" />
                                    </div>
                                </div>
                                <div id="AddressInfo" class="row form-group">
                                    <div id="Address1Div" class="col-xs-12">
                                        <label for="Address1">Address</label>
                                        <input type="text" class="form-control input-sm" name="Address1" value="<?= $contact->Address1 ?>" size="50" />
                                    </div>
                                    <div id="Address2Div" class="col-xs-12">
                                        <label for="Address2">Address 2</label>
                                        <input type="text" class="form-control input-sm" name="Address2" value="<?= $contact->Address2 ?>" size="50" />
                                    </div>
                                    <div id="Address3Div" class="col-xs-12">
                                        <label for="Address3">Address 3</label>
                                        <input type="text" class="form-control input-sm" name="Address3" value="<?= $contact->Address3 ?>" size="50" />
                                    </div>
                                </div>
                                <div class="row form-group" id="AddressInfo2">
                                    <div id="CityDiv" class="col-xs-6">
                                        <label for="City">City</label>
                                        <input type="text" class="form-control input-sm col-xs-5" name="City" value="<?= $contact->City ?>" size="25" />
                                    </div>
                                    <div id="StateDiv" class="col-xs-2">
                                        <label for="City">State</label>
                                        <input type="text" class="form-control input-sm col-xs-3" name="State" value="<?= $contact->State ?>" size="2" />
                                    </div>
                                    <div id="ZipDiv" class="col-xs-4">
                                        <label for="City">Zip</label>
                                        <input type="text" class="form-control input-sm col-xs-4" name="Zip" value="<?= $contact->Zip ?>" />
                                    </div>
                                </div> 
                                <div class="row form-group" id="ContactInfo">
                                    <div id="WorkPhoneDiv" class="col-xs-4">
                                        <label for="FirstName">Work Phone</label>
                                        <input type="text" class="form-control input-sm" name="WorkPhone" value="<?= $contact->WorkPhone ?>" />
                                    </div>
                                    <div id="CellPhoneDiv" class="col-xs-4">
                                        <label for="FirstName">Mobile Phone</label>
                                        <input type="text" class="form-control input-sm" name="CellPhone" value="<?= $contact->CellPhone ?>" />
                                    </div>
                                    <div id="FaxDiv" class="col-xs-4">
                                        <label for="Fax">Fax</label>
                                        <input type="text" class="form-control input-sm" name="Fax" value="<?= $contact->Fax ?>" />
                                    </div>
                                </div>
                                <div class="row form-group" id="ContactInfo2">
                                    <div id="WebsiteDiv" class="col-xs-6">
                                        <label for="CompanyWebsite">Website</label>
                                        <input type="text" class="form-control input-sm" name="CompanyWebsite" value="<?= $contact->CompanyWebsite ?>"  size="40" />
                                    </div>
                                    <div id="EmailDiv" class="col-xs-6">
                                        <label for="Email">Email</label>
                                        <input type="text" class="form-control input-sm" name="Email" value="<?= $contact->Email ?>"  size="40" />
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                        <div class="tab-pane fade" id="additionalDiv">

                            <div class="row form-group" id="HomeAddressInfo">
                                <div id="HomeAddress1Div" class="col-xs-12">
                                    <label for="HomeAddress">Home Address</label>
                                    <input type="text" class="form-control input-sm" name="HomeAddress1" value="<?= $contact->HomeAddress1 ?>" size="50" />
                                </div>
                                <div id="HomeAddress2Div" class="col-xs-12">
                                    <label for="HomeAddress2">Address 2</label>
                                    <input type="text" class="form-control input-sm" name="HomeAddress2" value="<?= $contact->HomeAddress2 ?>" size="50" />
                                </div>
                                <div id="HomeAddress3Div" class="col-xs-12">
                                    <label for="HomeAddress3">Address 3</label>
                                    <input type="text" class="form-control input-sm" name="HomeAddress3" value="<?= $contact->HomeAddress3 ?>" size="50" />
                                </div>
                            </div>
                            <div class="row form-group" id="HomeAddressInfo2">
                                <div id="HomeCityDiv" class="col-xs-6">
                                    <label for="City">City</label>
                                    <input type="text" class="form-control input-sm" name="HomeCity" value="<?= $contact->HomeCity ?>" size="25" />
                                </div>
                                <div id="HomeStateDiv" class="col-xs-2">
                                    <label for="City">State</label>
                                    <input type="text" class="form-control input-sm" name="HomeState" value="<?= $contact->HomeState ?>" size="2" />
                                </div>
                                <div id="HomeZipDiv" class="col-xs-4">
                                    <label for="City">Zip</label>
                                    <input type="text" class="form-control input-sm" name="HomeZip" value="<?= $contact->HomeZip ?>" />
                                </div>
                            </div>
                            <div class="row form-group" id="OtherInfo">
                                <div id="PhoneDiv" class="col-xs-4">
                                    <label for="FirstName">Home Phone</label>
                                    <input type="text" class="form-control input-sm" name="Phone" value="<?= $contact->Phone ?>" />
                                </div>
                                <div id="BirthdateDiv" class="col-xs-4">
                                    <label for="Birthdate">Birthday</label>
                                    <input type="text" class="form-control input-sm datepicker" id="Birthdate" name="Birthdate" value="<?= (strtotime($contact->Birthdate) ? date("m/d/Y", strtotime($contact->Birthdate)) : '') ?>" />
                                </div>
                                <div id="AnniversaryDiv" class="col-xs-4">
                                    <label for="Anniversary">Anniversary</label>
                                    <input type="text" class="form-control input-sm datepicker" id="Anniversary" name="Anniversary" value="<?= (strtotime($contact->Anniversary) ? date("m/d/Y", strtotime($contact->Anniversary)) : '') ?>" />
                                </div>
                            </div>
                            <div id="PublicContactDiv">
                                <label for="PublicContact">Is Public?</label>
                                <input type="checkbox" name="PublicContact" value="1" <?= $contact->PublicContact == 1 ? 'CHECKED' : '' ?>/>
                            </div>
                            <div id="CategoryDiv">
                                <label for="Category">Category</label>
                                <select class="form-control input-sm" name="Category">
                                    <option value=""></option>
                                    <?php
                                    foreach ($categoryValues as $val) {
                                        ?>
                                        <option value="<?= $val->LookupValue ?>" <?= $val->LookupValue == $contact->Category ? 'SELECTED' : '' ?>><?= $val->LookupLabel ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <div id="SegmentDiv">
                                <label for="Segment">Segment</label>
                                <select class="form-control input-sm" name="Segment">
                                    <option value=""></option>
                                    <?php
                                    foreach ($segmentValues as $val) {
                                        ?>
                                        <option value="<?= $val->LookupValue ?>" <?= $val->LookupValue == $contact->Segment ? 'SELECTED' : '' ?>><?= $val->LookupLabel ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>	
                            <div id="CampaignDiv">
                                <label for="Campaign">Campaign</label>
                                <select class="form-control input-sm" name="Campaign">
                                    <option value=""></option>
                                    <?php
                                    foreach ($campaignValues as $val) {
                                        ?>
                                        <option value="<?= $val->LookupValue ?>" <?= $val->LookupValue == $contact->Campaign ? 'SELECTED' : '' ?>><?= $val->LookupLabel ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="statusDiv">

                            <table class="table table-striped table-bordered table-hover table-responsive">
                                <thead>
                                    <tr class="eventTable rowheader">
                                        <th>
                                            Event Type
                                        </th>
                                        <th>
                                            Time
                                        </th>
                                        <th>
                                            Notes
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 0;
                                    foreach ($allEvents as $event) {
                                        $i++;
                                        ?>

                                        <?php
                                        $date = new DateTime($event->EventDate, new DateTimeZone($originaltimezone));  //get date / time base on string saved and server's timezone
                                        if (!empty($currentUser->UserTimezoneOffset)) {
                                            $date->setTimezone(new DateTimeZone($currentUser->UserTimezoneOffset)); //update date / time with the user's preferred timezone (for UserTimezoneOffset)
                                        }
                                        $event->EventDate = $date->format("m/d/Y g:i:s a");
                                        ?>

                                        <tr class="eventTable<?= $i % 2 == 0 ? ' roweven' : ' rowodd' ?>">
                                            <td class="eventTable">
                                                <?= $event->EventDescription ?>
                                            </td>
                                            <td class="eventTable">
                                                <?= strtotime($event->EventDate) ? date("m/d/Y g:i:s a", strtotime($event->EventDate)) : ""; ?>
                                            </td>
                                            <td class="eventTable">
                                                <?= $event->EventNotes ?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>  	
                                </tbody>
                            </table>
                        </div>
                        <div class="tab-pane fade" id="notesDiv">
                            <div id="CommentsDiv">
                                <label for="Comments">Notes</label>
                                <textarea name="Comments" class="form-control" cols="55" rows="14"><?= $contact->Comments ?></textarea>
                            </div>
                        </div>
                    </div>
                    <div id="buttonDiv" class="center">
                        <input type="submit" name="Submit" value="Submit" class="btn btn-primary" />&nbsp;&nbsp;&nbsp;<input type="button" value="Cancel" class="btn btn-default" onclick="window.close();" />
                    </div>

            </div>

        </form>
        <?php date_default_timezone_set($originaltimezone); //reset the changed timezone to the original server timezone  ?>
        <?php include("components/bootstrap-footer.php") ?>
    </body>
</html>