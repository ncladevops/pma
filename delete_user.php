<?php
	require("../printable/include/mysql.inc.php");
	require("../printable/include/optimize.printable.inc.php");
	require("globals.php");
	
	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	
	$portal = new OptimizePortal($COMPANY_ID, $db);
	
	$currentUser = $portal->UserAccess($_SESSION['currentuserid']);
	
	$isSubAdmin = $portal->CheckPriv($currentUser->UserID, 'subadmin');
		
	if(!$isSubAdmin)
	{
		die( "Accessed Denied." );
	}
	
	$user = $portal->GetUser($_GET['id']);
		
	if(!$user)
	{
		die("Invalid User ID");
	}
	
	// Check if user has children
	$children = $portal->GetChildren($user->UserID);
	
	if(sizeof($children) <= 0) {
	
		$portal->EmailUserDeleted($user->UserID, $currentUser->ContactEmail);
		
		$user->DisabledBy = $currentUser->UserID;
		$portal->UpdateUser($user);
		
		$portal->DisableUser($user->UserID);
		
		header("Location: manage_users.php?message=" . urlencode("User de-activated successfully"));
		die();
	}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
	<head>
	<title>
		<?= $portal->CurrentCompany->CompanyName ?> :: Add User
	</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />		
	<link type="text/css" media="all" rel="stylesheet" href="style.css" />
	<link type="text/css" media="all" rel="stylesheet" href="css/edit_user.css" />
<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />
	<script  src="js/func.js"></script>
</head>
<body bgcolor="#FFFFFF" onload="initPage()">
<div id="page">
<?php include("components/header.php") ?>
<div id="body">
	<?php 
		$CURRENT_PAGE = "Home";
		include("components/navbar.php"); 
	?>
<form method="post" action="<?= $_SERVER['PHP_SELF'] ?>?id=<?= $user->UserID; ?>">
	<div class="error"><?= $_GET['message']; ?></div>
	<div id="UserInfoDiv">
		<div class="sectionHeader">
			De-activate User
		</div>
		<div class="sectionDiv">
			
			<table cellpadding="0" align="center" width="700">
				<tr>
	  				<td>
						The supervisor <?= $user->FirstName . " " . $user->LastName ?> has <?= sizeof($children) ?> users assigned to them. You can not de-activate this user untill they have been re-assigned.<br/>
						&nbsp;<br/>
						<a href="manage_users.php">Click here to return to the user listing</a>
						
	  				</td>
         		</tr>
         	</table>
		</div>
	</div>
</form>
</div>
</div>
<?php include("components/footer.php") ?>
</body>
</html>