<?php
require("../printable/include/mysql.inc.php");
require("../printable/include/optimize.printable.inc.php");
require("globals.php");

$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);

$portal = new OptimizePortal($COMPANY_ID, $db);

$currentUser = $portal->GetUser($_SESSION['currentuserid']);

//Change page's timezone for date call functions (for default start and end date range)
$originaltimezone = date('e');

if (!$currentUser) {
    header("Location: login.php?message=" . urlencode("Not logged in or login error."));
    die();
}

// See if appointment is open for contact
$appt = $portal->GetAppointment($_GET['id'], $currentUser->UserID);

if (!$appt) {
    die("Invalid ReminderID");
}

$lead = $portal->GetOptimizeContact($appt->ContactID, $currentUser->UserID);

if (!$lead) {
    die("Invalid Appointment");
}

// Set last Notifty
$appt->LastNotifyTime = date("Y-m-d H:i:s");
$portal->UpdateAppointment($appt);

if ($_POST['Submit'] == 'Postpone') {
    $appt->NotifyTime = date("Y-m-d H:i:s", strtotime("+" . intval($_POST['ReminderWarning']) . " minutes"));
    $portal->UpdateAppointment($appt);
    $close = true;
} elseif ($_POST['Submit'] == 'Dismiss') {
    $appt->NotifyTime = "0000-00-00";
    $portal->UpdateAppointment($appt);
    $close = true;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <title>
            Marketing on Demand :: Reminder for <?= "$lead->FirstName $lead->LastName" ?>
        </title>
        <meta http-equiv="content-type" content="text/html;charset=utf-8" />
        <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />

        <?php include("components/bootstrap.php") ?>

    </head>
    <body onload="<?= $close == true ? "window.close();" : ($error == 1 ? "alert('$errorText')" : "" ) ?>">
        <form method="post" action="<?= $_SERVER['PHP_SELF'] ?>?id=<?= $appt->ContactAppointmentID; ?>">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="glyphicon glyphicon-time"></i> Reminder</h3>
                </div>
                <div class="panel-body">

                    <div class="itemSection">
                        <div id="ContactNameDiv">
                            <label for="ContactName">Contact Name:</label>
                            <?= "$lead->FirstName $lead->LastName" ?> (<a href='#' onclick="window.opener.location = 'edit_lead.php?id=<?= $lead->SubmissionListingID ?>'">view lead</a>)
                        </div>
                    </div>
                    <div class="itemSection">
                        <div id="PhoneDiv">
                            <label for="Phone">Work Phone:</label>
                            <?= format_phone($lead->WorkPhone) ?>
                        </div>
                        <div id="Phone2Div">
                            <label for="Phone2">Cell Phone:</label>
                            <?= format_phone($lead->CellPhone) ?>
                        </div>
                    </div>

                    <?php
                    $date = new DateTime($appt->AppointmentTime, new DateTimeZone($originaltimezone));  //get date / time base on string saved and server's timezone
                    if (!empty($currentUser->UserTimezoneOffset)) {
                        $date->setTimezone(new DateTimeZone($currentUser->UserTimezoneOffset)); //update date / time with the user's preferred timezone (for UserTimezoneOffset)
                    }
                    $printdate = $date->format("m/d/Y g:i a");
                    ?>

                    <div class="itemSection">
                        <div id="AppointmentDateDiv">
                            <label for="NotifyTime">Appointment Date:</label>
                            <?= $printdate ?>
                        </div>
                    </div>
                    <div class="longItemSection">
                        <div id="EventNotesDiv">
                            <label for="Notes">Notes:</label><br/>
                            <textarea name="Notes" rows="8" class="form-control" style="width: 95%;" readonly><?= $appt->Notes ?></textarea>
                        </div>
                    </div>
                    <div class="itemSection">
                        <div id="ReminderWarningDiv">
                            <label for="ReminderWarning">Remind Again:</label><br/>
                            <select name="ReminderWarning" class="input-sm form-control">
                                <option value="5" <?= $dateDiff == 5 ? "SELECTED" : "" ?>>5 Minutes</option>
                                <option value="10" <?= $dateDiff == 10 ? "SELECTED" : "" ?>>10 Minutes</option>
                                <option value="15" <?= $dateDiff == 15 ? "SELECTED" : "" ?>>15 Minutes</option>
                                <option value="20" <?= $dateDiff == 20 ? "SELECTED" : "" ?>>20 Minutes</option>
                                <option value="30" <?= $dateDiff == 30 ? "SELECTED" : "" ?>>30 Minutes</option>
                                <option value="60" <?= $dateDiff == 60 ? "SELECTED" : "" ?>>1 hour</option>
                                <option value="120" <?= $dateDiff == 120 ? "SELECTED" : "" ?>>2 hours</option>
                                <option value="300" <?= $dateDiff == 300 ? "SELECTED" : "" ?>>5 hours</option>
                                <option value="720" <?= $dateDiff == 720 ? "SELECTED" : "" ?>>12 hours</option>
                                <option value="1440" <?= $dateDiff == 1440 ? "SELECTED" : "" ?>>1 day</option>
                                <option value="2880" <?= $dateDiff == 2880 ? "SELECTED" : "" ?>>2 days</option>
                                <option value="7200" <?= $dateDiff == 7200 ? "SELECTED" : "" ?>>5 days</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <div class="center">
                        <input type="submit" value="Postpone" name="Submit" class="btn btn-default" id="Submit"/>&nbsp;&nbsp;<input type="submit" value="Dismiss" class="btn btn-default" name="Submit" />
                    </div>
                </div>
            </div>
        </form>

        <?php include("components/bootstrap-footer.php") ?>

    </body>
</html>