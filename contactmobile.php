<?php
require("../printable/include/mysql.inc.php");
require("../printable/include/optimize.printable.inc.php");
require("globals.php");

$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);

$portal = new OptimizePortal($COMPANY_ID, $db);

// Querry to get user info
$currentUser = $portal->GetUser($_SESSION['currentuserid']);
// Check login
if (!$currentUser) {
    header("Location: login.php?message=" . urlencode("Not logged in or login error.  Please try again."));
    die();
}

//Change page's timezone for date call functions (for default start and end date range)
$originaltimezone = date('e');
if (!empty($currentUser->UserTimezoneOffset)) {
    date_default_timezone_set($currentUser->UserTimezoneOffset); //update page timezone with the user's preferred timezone
}

$isAdmin = $portal->CheckPriv($currentUser->UserID, 'admin');

$contact = $portal->GetOptimizeContact($_GET['id'], $currentUser->UserID);

if (!$contact) {
    echo "Invalid Contact.";
    die();
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <title>
            <?= $portal->CurrentCompany->CompanyName ?> :: View Contact Info
        </title>
        <meta http-equiv="content-type" content="text/html;charset=utf-8" />		
        <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />

        <script type="text/javascript">
            function logContactEvent(contactID, eventTypeID)
            {

<?php
foreach ($cets as $cet) {
    ?>
                    if (document.getElementById("event_" + contactID + "_<?= $cet->ContactEventTypeID ?>"))
                        document.getElementById("event_" + contactID + "_<?= $cet->ContactEventTypeID ?>").checked = false;
    <?php
}
?>
                if (document.getElementById("event_" + contactID + "_" + eventTypeID))
                    document.getElementById("event_" + contactID + "_" + eventTypeID).checked = true;


                contactEventRequest = GetXmlHttpObject()
                if (contactEventRequest == null)
                {
                    alert("Your browser does not support AJAX!");
                    return;
                }
                var url = transactionURL;
                url = url + "?action=LogContactEvent&slid=" + contactID;
                url = url + "&etid=" + eventTypeID;
                url = url + "&sid=" + Math.random();
                contactEventRequest.onreadystatechange = contactEventStateChanged;
                contactEventRequest.open("GET", url, true);
                contactEventRequest.send(null);
            }

            function contactEventStateChanged()
            {
                if (contactEventRequest.readyState == 4)
                {
                    var xmlDoc = contactEventRequest.responseXML.documentElement;

                    // Check if session timedout			
                    if (xmlDoc.getElementsByTagName("Status")[0].childNodes[0].nodeValue == 'Timeout')
                    {
                        parent.location = "login.php?logout=true&message='Your+Session+has+timedout.+Please+Login+Again.";
                        return;
                    }
                    if (xmlDoc.getElementsByTagName("Status")[0].childNodes[0].nodeValue == 'Fail')
                    {
                        alert("Unable to set the Contact Event for that Contact");
                    }

<?php
// Build string of Notes Prompts
$notesArray = array();
$notesString = "";
foreach ($cets as $cet) {
    if ($cet->Notes == 1) {
        $notesArray[] = 'xmlDoc.getElementsByTagName("EventTypeID")[0].childNodes[0].nodeValue == "' . $cet->ContactEventTypeID . '"';
    }
}
$notesString = implode(" || ", $notesArray);
if ($notesString != '') {
    ?>
                        if (<?= $notesString ?>)
                        {
                            wopen('setnotes.php?ceid=' + xmlDoc.getElementsByTagName("ContactEventID")[0].childNodes[0].nodeValue, 'Set_Notes', 400, 275);
                        }
    <?php
}
?>
                }

            }
        </script>

        <?php include("components/bootstrap.php") ?>

    </head>
    <body>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?= $contact->FirstName . ' ' . $contact->LastName ?></h3>               
            </div>
            <div class="panel-body">
                <div class="info-group row">
                    <p class="info-label"><i class="glyphicon glyphicon-phone-alt"></i> Work Phone</p>
                    <h5><?= $contact->WorkPhone ?></h5>
                </div>
                <div class="info-group row">
                    <p class="info-label"><i class="glyphicon glyphicon-phone"></i> Cell Phone</p>
                    <h5><?= $contact->CellPhone ?></h5>
                </div>
                <div class="info-group row">
                    <p class="info-label"><i class="glyphicon glyphicon-envelope"></i> Email</p>
                    <a href="mailto:<?= $contact->Email ?>"
                       onclick="logContactEvent('<?= $contact->SubmissionListingID ?>', '<?= $CONTACTEVENT_EMAIL ?>');" title="Send Email">
                        <h5><?= $contact->Email ?></h5>
                    </a>
                </div>
                <div class="info-group row">
                    <p class="info-label"><i class="glyphicon glyphicon-home"></i> Address</p>
                    <address>
                        <?= $contact->Address1 ?></br>
                        <?= $contact->Address2 == "" ? "" : $contact->Address2 . "</br>" ?>
                        <?= $contact->Address3 == "" ? "" : $contact->Address3 . "</br>" ?>
                        <?= $contact->City . ', ' . $contact->State . ' ' . $contact->Zip ?>
                    </address>
                </div>

            </div>
            <div class="panel-footer">
                <div class="btn-group">
                    <button type="button" class="btn btn-success">Set <i class="glyphicon glyphicon-time"></i></button>
                </div>  
                <div class="btn-group">
                    <button type="button" class="btn btn-info"><i class="glyphicon glyphicon-edit"></i> Edit</button>
                </div>  
            </div>
        </div>
        <?php date_default_timezone_set($originaltimezone); //reset the changed timezone to the original server timezone    ?>
        <?php include("components/bootstrap-footer.php") ?>
    </body>
</html>