<html lang="en" dir="ltr" class="" style="display: block;"><head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
    <title>Post a Tweet on Twitter</title>

    <link href="https://abs.twimg.com/a/1374692310/tfw/css/tfw.bundle.css" media="screen" rel="stylesheet" type="text/css">
    <!--[if (IEMobile) & (lt IE 9)]>
    <link href="https://abs.twimg.com/a/1374692310/tfw/css/tfw_iemobile.bundle.css" media="screen" rel="stylesheet" type="text/css" />
    <![endif]-->

    
<meta name="native-url" content="twitter://post?text=%20%23TwitterStories">


    <style type="text/css">
      html { display:none; }
    </style>
    <noscript>
      &lt;style type="text/css"&gt;
        html { display: block; }
      &lt;/style&gt;
    </noscript>
    <!--[if IEMobile & lt IE 9]>
    <style type="text/css">
      html { display: block; }
    </style>
    <![endif]-->
  </head>
  <body class="
 
 tfw
 en
 logged-in
 
 nofooter
 noloki
 js">

    <div id="header" role="banner">
      <div class="bar">
        <h1 class="logo"><a href="https://twitter.com/home" class="alternate-context" target="_blank">Twitter</a></h1>

        
        <div id="session" role="navigation">
          
          <div class="user-menu">
            <h3 class="session">User Management</h3>
            <ul class="session">
              <li class="new"><a href="/intent/tweet">New Tweet</a></li>
              <li class="settings"><a href="/settings" target="_blank">Settings</a></li>
              <li class="help"><a href="/help" target="_blank">Help</a></li>
              
                <li class="signout"><form action="/intent/session" method="post"><div style="margin:0;padding:0"><input name="_method" type="hidden" value="delete"><input name="authenticity_token" type="hidden" value="6e364544471cdcfffdf6bed12526bdec7b3d335e"></div>
                    <input id="referer" name="referer" type="hidden" value="/intent/tweet?hashtags=TwitterStories%2C&amp;original_referer=https%3A%2F%2Fdev.twitter.com%2Fdocs%2Ftweet-button&amp;tw_p=tweetbutton">
                    <input type="submit" class="textual link" value="Sign out">
                </form></li>
              
            </ul>
          </div>
        </div>
        
      </div>
    </div>

    <div id="bd" role="main">
      

  <h2 class="action-information">What's happening?</h2>

  <form action="https://twitter.com/intent/tweet/update" id="update-form" method="post"><div style="margin:0;padding:0"><input name="authenticity_token" type="hidden" value="6e364544471cdcfffdf6bed12526bdec7b3d335e"></div>
  <div class="hd">
    <input name="hashtags" type="hidden" value="TwitterStories,">
<input name="original_referer" type="hidden" value="https://dev.twitter.com/docs/tweet-button">



      <label for="status" id="status-label">Tweet Text</label>
  </div>

  

  <div class="bd">
    <span class="field">
      <textarea id="status" name="status" required="" autofocus="" aria-required="true" aria-describedby="post-error char-count"><?= $_POST['tweet'] ?></textarea>
    </span>
  </div>

  <div class="ft">
    
    <fieldset class="submit">
      <input type="text" id="char-count" aria-live="polite" disabled="" value="<?= $_POST['charcount'] ?>"> remaining
    </fieldset>
    
  </div>
  </form>



    </div>

    

    

    <script id="LR3" type="text/javascript" async="" src="https://abs.twimg.com/a/1374692310/javascripts/modules/imports/jquery.js"></script><script id="LR2" type="text/javascript" async="" src="https://abs.twimg.com/a/1374692310/javascripts/modules/tfw/intents/tweetbox.js"></script><script id="LR1" type="text/javascript" async="" src="https://abs.twimg.com/a/1374692310/javascripts/modules/tfw/intents/main.js"></script><script type="text/javascript" charset="utf-8">
      var twttr = twttr || {};
      twttr.form_authenticity_token = '6e364544471cdcfffdf6bed12526bdec7b3d335e';
      if (self == top) {
        document.documentElement.style.display = 'block';
      }
    </script>

    <script src="https://abs.twimg.com/a/1374692310/javascripts/loadrunner.js" data-main="tfw/intents/main" data-path="https://abs.twimg.com/a/1374692310/javascripts/modules" type="text/javascript"></script>

    

    
    <script type="text/javascript" charset="utf-8">
      
  var twttr = twttr || {};
  twttr.tco = {
    length: 22
  };

  using('tfw/intents/tweetbox', 'imports/jquery', function(Tweetbox, $) {
    $('#status').each(function() {
      new Tweetbox(this, 0, 0);
    });
  });

      
    </script>



  

</body></html>