<?php
	require("../printable/include/mysql.inc.php");
	require("../printable/include/optimize.printable.inc.php");
	require("globals.php");
	
	$HIDE_STATUSES = array(30, 20, 37, 35);
	
	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	
	$portal = new OptimizePortal($COMPANY_ID, $db);
	
	$currentUser = $portal->GetUser($_SESSION['currentuserid']);
	$isSubAdmin = $portal->CheckPriv($currentUser->UserID, 'subadmin');
		
	// Create sort Array
  	$sortCols = array( 'ID' => 'submissionlisting.SubmissionListingID',  'Date Added' => 'DateAdded', 'Company'=>'Company', 'First Name' => 'FirstName', 
  					'Last Name' => 'LastName', 'Home Phone' => 'nosort', 'Cell Phone' => 'nosort',
  					'State'=>'State', 'Status' => 'contactstatustype.SortOrder', 'Contact Event' => 'ContactEventTypeID', 
  					'Last Action Date'=>'nosort', '&nbsp' => 'nosort'
  					);
  	$sortDirs = array( 'ASC', 'DESC');
  	$searchFields = array( 'ID' => 'submissionlisting.SubmissionListingID', 'LastName' => 'submissionlisting.LastName',
  							'Status' => 'ContactStatus', 'Campaign' => 'submissionlistingtype.ListingType',
  							'State' => 'submissionlisting.State', 'Phone' => 'Phone',
  							'Email' => 'Email' );
  	$dateFilters = array('1'=>'Today', '5'=>'Last 5 days', '10'=>'Last 10 days',
  						'30'=>'Last 30 days', '60'=>'Last 60 days', '90'=>'Last 90 days');

  	if(sizeof($_GET) <= 0)
  	{
  		header("Location: " . $_SERVER['PHP_SELF'] . "?saved=1&" . $_SESSION['lastquery']);
  		die();
  	}
  	elseif (sizeof($_GET) == 1 && isset($_GET['message']) )
  	{
  		header("Location: " . $_SERVER['PHP_SELF'] . "?message=" . $_GET['message']. "&" . $_SESSION['lastquery']);
  		die();
  	}
  	
  	$leadsPerPage = 25;
  	$_GET['offset'] = intval($_GET['offset']) < 0 ? 0 : intval($_GET['offset']);
  	
	if(!$currentUser)
	{
		header("Location: login.php?message=" .urlencode("Not logged in or login error."));
		die();
	}
	
	if(array_search($_GET['sortby'], $sortCols) === false)
	{
		$_GET['sortby'] = "DateAdded";
	}
	if(array_search($_GET['sortdir'], $sortDirs) === false)
	{
		$_GET['sortdir'] = "DESC";
	}
		
	if($_GET['statusfilter'] != '')
	{
		$statusString = "(contactstatustype.ContactStatus LIKE '" . mysql_real_escape_string(stripslashes($_GET['statusfilter'])) . "%')";
	}
	else
	{
		$statusString = "1";
	}
	
	if($_GET['actionfilter'] != '' && $portal->GetContactEventType($_GET['actionfilter']))
	{
		$actionString = "(ContactEventTypeID = '" . mysql_real_escape_string(stripslashes($_GET['actionfilter'])) . "')";
	}
	else
	{
		$actionString = "1";
	}
	
	if(!strtotime($_GET['start_range']))
	{
		$_GET['start_range'] = date("Y-m-1");
		
	}
	$startWhere = "submissionlisting.DateAdded >= '" . date("Y-m-d", strtotime($_GET['start_range'])) ." 00:00:00'";
	
	if(!strtotime($_GET['end_range']))
	{
		$_GET['end_range'] = date("Y-m-d");
	}
	$endWhere = "submissionlisting.DateAdded <= '" . date("Y-m-d", strtotime($_GET['end_range'])) ." 23:59:59'";
	
	if(!isset($searchFields[$_GET['searchfield']]) || strlen($_GET['search']) <= 0)
	{
		$whereString = "1";
	}
	else
	{
		$whereString = $searchFields[$_GET['searchfield']] . " LIKE '%" . mysql_real_escape_string(stripslashes($_GET['search'])) . "%'";
		$startWhere = "1";
		$endWhere = "1";
	}
	
	if($isSubAdmin) {
		$userString = 1;
	}
	else {
		$userString = "(submissionlisting.UserID = '$currentUser->UserID' || submissionlisting.ContactStatusTypeID = '$CONTACTSTATUSID_NEW')";
	}
		
	$userGroup = $portal->GetGroup($currentUser->GroupID);
  	$currentCampaign = $portal->GetSubmissionListingType($userGroup->DefaultSLT);
  	$hideDefCampString = "submissionlisting.ListingType != '" . intval($currentCampaign->SubmissionListingTypeID) . "'"; 
	$leads = $portal->GetOptimizeContacts('all', 'all', 
		"$userString AND $whereString AND $statusString AND $actionString AND $startWhere AND $endWhere AND $hideDefCampString", 
		false, $_GET['sortby'] . " " . $_GET['sortdir'],
		$leadsPerPage, $_GET['offset']);
	$leadcount = $portal->CountOptimizeContacts('all', 'all', 
		"$userString AND $whereString AND $statusString AND $actionString AND $startWhere AND $endWhere AND $hideDefCampString");
	
	$csts = $portal->GetContactStatusTypes('all', 1, true);
	$contactStatuseTypes = array();
	$shortStatuses = array();
	$shortStatusDetails = array();
	foreach($csts as $cst) {
		if(!$cst->Disabled) {
			$shortStatuses[$cst->ShortName] = (strlen($cst->DetailName) > 0 ? 1 : 0);
			$shortStatusDetails[$cst->ShortName][] = $cst->ContactStatus;
			$contactStatuseTypes[$cst->ContactStatus] = $cst;
		}
	}
	$contactEventTypes = $portal->GetContactEventTypes();
	$users = $portal->GetCompanyUsers();
	$cetLookup = array();
	foreach($contactEventTypes as $cet)
	{
		$cetLookup[$cet->ContactEventTypeID] = $cet->EventType;
	}
	
	$tempGet = $_GET;
	unset($tempGet['message']);
	unset($tempGet['saved']);
	$_SESSION['lastquery'] = build_get_query($tempGet);
	echo "<!-- $userString AND $whereString AND $statusString AND $actionString AND $startWhere AND $endWhere AND $hideDefCampString -->";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
	<title>
		Optimize on Demand :: My Leads
	</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />	
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="author" content="OptifiNow">

	<!-- Bootstrap core CSS -->
    <link href="dist/css/bootstrap.css" rel="stylesheet">

    <!-- Bootstrap theme -->
    <link href="dist/css/bootstrap-theme.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/optifinow-custom.css" rel="stylesheet">

<script type="text/javascript">//<![CDATA[

      var cal = Calendar.setup({
          onSelect: function(cal) { cal.hide() },
          bottomBar: true,
          dateFormat : "%m/%d/%Y"
      });

//]]></script>	
	<script  src="js/func.js"></script>	
	<script  src="js/contact_status.js"></script>
<script language="JavaScript" type="text/JavaScript">
	function updateContactEvent(id, actionName)
	{
		document.getElementById("cetid_" + id).innerHTML = actionName;
	}

	function toggleFilter() {
		if(document.getElementById("filter_div").style.display == "none")
		{
			document.getElementById("filter_div").style.display = "";
			document.getElementById("search_div").style.display = "none";
		}
		else
		{
			document.getElementById("filter_div").style.display = "none";
			document.getElementById("search_div").style.display = "none";
		}
	}

	function toggleSearch() {
		if(document.getElementById("search_div").style.display == "none")
		{
			document.getElementById("search_div").style.display = "";
			document.getElementById("filter_div").style.display = "none";
		}
		else
		{
			document.getElementById("search_div").style.display = "none";
			document.getElementById("filter_div").style.display = "none";
		}
	}

	var statusDetails = new Array();
<?php
	foreach ($shortStatusDetails as $shortStat=>$details) {
?>
	statusDetails["<?= $shortStat ?>"] = new Array();
<?php 
		foreach($details as $det) {
?>
	statusDetails["<?= $shortStat ?>"][<?= intval($contactStatuseTypes[$det]->ContactStatusTypeID) ?>] = "<?= $contactStatuseTypes[$det]->DetailName ?>";
<?php 
			
		}
	} 
?>
	function showDetailedContactStatus(slid, cstid) {
		document.getElementById("detailContactID").value = slid;
		var statusSelect = document.getElementById("detailStatusID");
		document.getElementById("shortStatus").innerText = cstid;

		var i = 0;
		statusSelect.options.length = i;
		for(var id in statusDetails[cstid])
		{
			statusSelect.options.length++;
			statusSelect[i].value = id;
			statusSelect[i].text = statusDetails[cstid][id];
			i++;
		}
		document.getElementById("detailDiv").style.display = "";
	}
	function submitDetailedContactStatus() {
		updateContactStatus(document.getElementById("detailContactID").value, document.getElementById("detailStatusID").value);
		document.getElementById("detailDiv").style.display = "none";
	}
</script>
<?php include("components/reminder.php") ?>
</head>
<body bgcolor="#FFFFFF" onload="timedRefresh(120);checkPendingReminder()">
<?php include("components/header2.php") ?>
<div id="body" class="container">
	<?php 
		$CURRENT_PAGE = "Leads";
		include("components/navbar2.php"); 
	?>
	<div id="actionBar">

		<ul class="nav nav-pills">
                <li><a href="add_lead.php"><i class="glyphicon glyphicon-plus"></i> Add Lead</a></li>
		  <li><a href="#" onclick="toggleFilter(); return false;"><i class="glyphicon glyphicon-filter"></i> Filter Lead</a></li>
		  <li><a href="#" onclick="toggleSearch(); return false;"><i class="glyphicon glyphicon-search"></i> Search Lead</a></li>
		  <li><a href="#" onclick="window.location=window.location; return false;"><i class="glyphicon glyphicon-refresh"></i> Refresh</a></li>
              </ul>
	</div>
	<div style="width: 100%; height: 20px; clear: both; text-align: center; <?= strlen($_GET['message']) > 0 ? "" : "display: none" ?>">
		<div class="error">
<?php
	echo $_GET['message'];
	unset($_GET['message']);
?>
		</div>
	</div>
<?php
	$getTemp = $_GET;
	unset($getTemp['search']);
	unset($getTemp['searchfield']);
	unset($getTemp['statusfilter']);
	unset($getTemp['actionfilter']);
	unset($getTemp['start_range']);
	unset($getTemp['end_range']);
	unset($getTemp['userfilter']);
	$getString = build_get_query($getTemp);
?>	
	<div id="filter_div" style="<?= $_GET['isFilter'] == 'true' ? "" : "display: none;" ?>">
	<form method="get" action="<?= $_SERVER['PHP_SELF'] . "?" . $getString ?>>">
		<div class="backNav" style="width: 100%;">
			<label for="statusfilter">Status:</label> 
			<select name="statusfilter" onchange="this.form.submit();">
				<option value="">- All -</option>
<?php
	foreach($shortStatuses as $stat=>$isShort)
	{
		if((int)$sType->Disabled == 1) continue;
?>
				<option value="<?= $stat ?>" <?= $stat == $_GET['statusfilter'] ? "SELECTED" : "" ?>>
					<?= $stat ?>
				</option>				
<?php
	}
?>
			</select>
			<label for="actionfilter">Action:</label> 
			<select name="actionfilter" onchange="this.form.submit();">
				<option value="">- All -</option>
<?php
	foreach($contactEventTypes as $eType)
	{
?>
				<option value="<?= $eType->ContactEventTypeID ?>" <?= $eType->ContactEventTypeID == $_GET['actionfilter'] ? "SELECTED" : "" ?>>
					<?= $eType->EventType ?>
				</option>				
<?php
	}
?>
			</select>
<?php
	if($isSubAdmin)
	{
?>
			<label for="userfilter">User:</label> 
			<select name="userfilter" onchange="this.form.submit();">
				<option value="">- All -</option>
<?php
		foreach($users as $u)
		{
?>
				<option value="<?= $u->UserID ?>" <?= $u->UserID == $_GET['userfilter'] ? "SELECTED" : "" ?>>
					<?= $u->FirstName . " " . $u->LastName ?>
				</option>				
<?php
		}
?>
			</select>
<?php
	}
?>
			<label>From:</label> <input name="start_range" id="start_range" type="text" class="date_input" value="<?= strtotime($_GET['start_range']) ? date("m/d/Y", strtotime($_GET['start_range'])) : "" ?>" onchange="this.form.submit();"/>
			<script type="text/javascript">
				cal.manageFields("start_range", "start_range", "%m/%d/%Y");
			</script>
					<label>To:</label> <input name="end_range" id="end_range" type="text" class="date_input" value="<?= strtotime($_GET['end_range']) ? date("m/d/Y", strtotime($_GET['end_range'])) : "" ?>" onchange="this.form.submit();"/>
			<script type="text/javascript">
				cal.manageFields("end_range", "end_range", "%m/%d/%Y");
			</script>
			<input type="submit" name="Submit" class="btn btn-default" value="Go" />
			<a href="<?= $_SERVER['PHP_SELF'] ?>?saved=1">clear all</a>
			<input type="hidden" name="isFilter" value="true" />
		</div>
	</form>
	</div>
	<div id="search_div" style="<?= $_GET['isSearch'] == 'true' ? "" : "display: none;" ?>">
	<form method="get" action="<?= $_SERVER['PHP_SELF'] ?>">
		<div class="backNav" style="height: 20px; width: 100%; min-width: 600px">
			
			<div style="float: right; text-align: right;">	
				<label for="search">Search:</label> 
				<input type="search" name="search"  value="<?= $_GET['search'] ?>" />
				in field 
				<select name="searchfield">
<?php
	foreach($searchFields as $k=>$v)
	{
?>
					<option value="<?= $k ?>" <?= $k == $_GET['searchfield'] ? "SELECTED" : "" ?>>
						<?= $k ?>
					</option>				
<?php
	}
?>
				</select>
				<input type="submit" name="Submit" class="btn btn-default" value="Search" />
			<input type="hidden" name="isSearch" value="true" />
			</div>
		</div>
	</form>
	</div>
	<div id="LeadDiv" class="table-responsive">
	<table class="table table-striped table-bordered table-hover" >
		<tr class="lead_table rowheader" id="HeaderRow">
<?php
	foreach($sortCols as $k=>$v)
	{
		if($v != "nosort")
		{
			$getTemp = $_GET;
			$getTemp['sortby'] = $v;
			$getTemp['sortdir'] = 'DESC';
			$getDownString = build_get_query($getTemp);
			$getTemp['sortdir'] = 'ASC';
			$getUpString = build_get_query($getTemp);
?>
			<th class="lead_table">
				<?= $k ?>
<?php
			if($v != $_GET['sortby'] || $_GET['sortdir'] != 'ASC')
			{ 
?>
				<a href="<?= $_SERVER['PHP_SELF'] . "?" . $getUpString ?>"><img src="images/sort_up.png" border="0"/></a>
<?php
			}
			if($v != $_GET['sortby'] || $_GET['sortdir'] != 'DESC')
			{ 
?>		
				<a href="<?= $_SERVER['PHP_SELF'] . "?" . $getDownString ?>"><img src="images/sort_down.png" border="0"/></a>
<?php
			}
?>		
			</th>
<?php		
		}
		else
		{
?>
			<th class="lead_table"><?= $k ?></th>
<?php
		}
	}
?>
		</tr>
<?php
	$lead_step_ids = array();
	if(is_array($leads))
	{
		$i = 0;
		foreach($leads as $l)
		{
			$i++;
			array_push($lead_step_ids, $l->SubmissionListingID);
			$contactHistory = $portal->GetContactHistory($l->SubmissionListingID);
			$lastCH = $contactHistory[sizeof($contactHistory) - 1];
			$appt = $portal->GetOpenAppointment($l->SubmissionListingID, $currentUser->UserID);
?>
		<tr class="lead_table<?= $i%2==0 ? ' roweven' : ' rowodd' ?>">
			<td class="lead_table" align="right">
				<?= $l->SubmissionListingID ?>
			</td>
			<td class="lead_table" align="right">
				<?= strtotime($l->DateAdded) ? date("m/d/Y g:i a", strtotime($l->DateAdded)) : "" ?>
			</td>
			<td class="lead_table">
				<?= $l->Company ?>
			</td>
			<td class="lead_table">
				<?= $l->FirstName ?>
			</td>
			<td class="lead_table">
				<?= $l->LastName ?>
			</td>
			<td class="lead_table bluetext" align="right" style="white-space: nowrap;">
				<a href="#" onclick="wopen('trigger_call.php?id=<?= $l->SubmissionListingID; ?>&field=Phone', 'add_action', 350, 285); return false;">
					<?= format_phone($l->Phone) ?>
				</a>
			</td>
			<td class="lead_table bluetext" align="right" style="white-space: nowrap;">
				<a href="#" onclick="wopen('trigger_call.php?id=<?= $l->SubmissionListingID; ?>&field=Phone2', 'add_action', 350, 285); return false;">
					<?= format_phone($l->Phone2) ?>
				</a>
			</td>
			<td class="lead_table bluetext" align="right" style="white-space: nowrap;">
				<?= $l->State ?>
			</td>
			<td class="lead_table" align="center">
				<select	style="color: <?= isset($statColors[$contactStatuseTypes[$l->ContactStatus]->ShortName]) ? $statColors[$contactStatuseTypes[$l->ContactStatus]->ShortName] : "black" ?>" 
						name="cstid_<?= $l->SubmissionListingID ?>"	
						id="cstid_<?= $l->SubmissionListingID ?>"
						onchange="updateContactStatus('<?= $l->SubmissionListingID ?>', this.value);">
<?php
			$shown = false;
			foreach($shortStatuses as $stat=>$isShort)
			{
				$sColor = isset($statColors[$stat]) ? $statColors[$stat] : "black";
				if($isShort) {
					$shown = (array_search($l->ContactStatus, $shortStatusDetails[$stat]) === false) ? $shown : true; 
?>
					<option style="color: <?= $sColor ?>"
							value="<?= $stat ?>"
							<?= array_search($l->ContactStatus, $shortStatusDetails[$stat]) === false ? '' : 'SELECTED'?>>
						<?= $stat ?>
					</option>			
<?php
					
				}
				else {
					$shown = ($contactStatuseTypes[$stat]->ContactStatusTypeID == $l->ContactStatusTypeID) ? true : $shown; 
?>
					<option style="color: <?= $sColor ?>"
							value="<?= $contactStatuseTypes[$stat]->ContactStatusTypeID ?>"
							<?= $contactStatuseTypes[$stat]->ContactStatusTypeID == $l->ContactStatusTypeID ? 'SELECTED' : ''?>>
						<?= $stat ?>
					</option>			
<?php
				}
			}
			if(!$shown) {
?>
					<option value="<?= $l->ContactStatusTypeID ?>" selected="selected">
						<?= $l->ContactStatus ?>
					</option>			
<?php				
			}
?>	
				</select>
			</td>
			<td class="lead_table" align="left">
				<a href="#" onclick="wopen('add_action.php?id=<?= $l->SubmissionListingID; ?>', 'add_action', 350, 350); return false;">
					<i class="glyphicon glyphicon-star"> </i>				</a>
				<span id="cetid_<?= $l->SubmissionListingID ?>"><?= $cetLookup[$l->LastContactEvent->ContactEventTypeID] == "" ? "" : $cetLookup[$l->LastContactEvent->ContactEventTypeID] ?></span>
			</td>
			<td class="lead_table">
				<?= strtotime($lastCH->EventDate) ? date("m/d/Y g:i a", strtotime($lastCH->EventDate)) : "" ?>
			</td>
			<td class="lead_table" align="center" width="70px">
				<a href="edit_lead.php?id=<?= $l->SubmissionListingID ?>" title="Edit Lead"> <p class="glyphicon glyphicon-edit"></p> </a>
				<a href="mailto:<?= $l->Email; ?>" 
					title="Send Email"> <p class="glyphicon glyphicon-envelope"> </p> </a>
				<a href="#" 
					onclick="wopen('add_appointment.php?id=<?= $l->SubmissionListingID; ?>', 'add_appointment', 350, 435); return false;"
					title="<?= $appt ? "Edit" : "Add" ?> Reminder"> <p class="glyphicon glyphicon-time"> </p> </a>
			</td>
		</tr>
<?php
		}		
	}
	$_SESSION['lead_step_ids'] = implode(";", $lead_step_ids);
?>
	</table>
	</div>
	<div class="paginationNav"><center><?= build_page_string($_SERVER['PHP_SELF'], $_GET, $leadsPerPage, $_GET['offset'], $leadcount); ?></center></div>
	<div id="debug_div" style="display: none;"></div>
	<div style="display: none;" class="detail_div" id="detailDiv">
		<div style="text-align: center; background: #CCC; padding: 3px 0px 3px 0px; font-weight: bold;">
			Please Choose a detailed description for<br/>
			<span id="shortStatus"></span>
		</div>
		<div style="width: 394px; text-align: center; padding: 25px 0px 15px 0px;">
			<select name="detailStatusID" id="detailStatusID">
			</select>
			<input type="hidden" name="detailContactID" id="detailContactID" />
		</div>
		<div style="width: 394px; text-align: center; background: #FFF; padding-bottom: 15px;">
			<a href="#" onclick="submitDetailedContactStatus();">OK</a>
		</div>
	</div>
</div>

<?php include("components/footer.php") ?>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="assets/js/jquery.js"></script>
    <script src="dist/js/bootstrap.min.js"></script>

</body>
</html>