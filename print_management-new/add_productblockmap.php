<?php
	require("../../printable/include/mysql.inc.php");
	require("../../printable/include/printable.inc.php");
	require("../globals.php");
	
	$lvs = array("UserID", "GroupID", "CampaignID", "Vertical");
	
	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	
	$portal = new PrintablePortal($COMPANY_ID, $db);
		
	$currentUser = $portal->GetUser($_SESSION['currentuserid']);
		
	// Check login
	if(!$portal->CheckPriv($currentUser->UserID, 'subadmin'))
	{
		header( "Location: " . $portal->CurrentCompany->Website . "login.php?message=" . urlencode( "Not logged in or login error.  Please try again." ) );
		die();
	}
	
	$product = $portal->GetProduct($_GET['productid']);
	
	if(!$product)
	{
		header("Location: index.php?message=" . urlencode("Invalid Product ID"));
		die();
	}
	
	$pbmid = $portal->AddProductBlockMap($_GET['productid'], $_GET['variable']);
	
	if($pbmid)
	{
		header("Location: edit_productblockmap.php?pbmid=$pbmid");
		die();
	}
	else
	{
		header("Location: edit_productblock.php?productid=" . $_GET['productid'] . "&variable=" . $_GET['variable'] . "&message=". urlencode("Action Canceled. Product Block Map not updated."));
		die();
	}
	
?>