<?php
	require("../../printable/include/mysql.inc.php");
	require("../../printable/include/printable.inc.php");
	require("../../printable/include/contactfactory.inc.php");
	require("../globals.php");
	
	$variableRowsMax = 15;
	
	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	$factory = new ContactFactory($db);
	
	$portal = new PrintablePortal($COMPANY_ID, $db);
		
	$currentUser = $portal->GetUser($_SESSION['currentuserid']);
	$isAdmin = $portal->CheckPriv($currentUser->UserID, 'admin');	
	
	$contactFields = $factory->GetContactFields($portal->CurrentCompany->CompanyID);
	$dateFields = $factory->GetDateFields($portal->CurrentCompany->CompanyID);
		
	// Check login
	if(!$portal->CheckPriv($currentUser->UserID, 'subadmin'))
	{
		header( "Location: " . $portal->CurrentCompany->Website . "login.php?message=" . urlencode( "Not logged in or login error.  Please try again." ) );
		die();
	}
	
	$trigger = $portal->GetTrigger($_GET['id']);
	$triggerProducts = $portal->GetProductTriggers($trigger->TriggerID);
	$products = $portal->GetProducts();
	
	if(!$trigger)
	{
		header("Location: index.php?message=" . urlencode("Invalid Trigger ID"));
		die();
	}
	
	if($_GET['action'] == 'disable' || $_GET['action'] == 'enable')
	{
		$trigger->Disabled = ($_GET['action'] == 'disable' ? 1 : 0);
		
		$portal->UpdateTrigger($trigger);
		
		header("Location: manage_trigger.php?message=". urlencode("Action Completed. Trigger " . $_GET['action'] . "d."));
		die();
	}
	elseif($_GET['action'] == 'delete')
	{		
		$portal->DeleteTrigger($trigger->TriggerID);
		
		header("Location: manage_trigger.php?message=". urlencode("Action Completed. Trigger deleted."));
		die();
	}
	elseif($_POST['Submit'] == 'Cancel')
	{
		header("Location: manage_trigger.php?message=". urlencode("Action Canceled. Trigger not updated."));
		die();
	}
	elseif($_POST['Submit'] == 'Update')
	{		
  		$trigger->TriggerName	= $_POST['TriggerName'];
  		$trigger->TriggerType 	= $_POST['TriggerType'];
  		switch($trigger->TriggerType)
  		{
  			case "Date":
  				$trigger->TriggerObject = $_POST['TriggerObjectDate'];
  				$trigger->TriggerValue = intval($_POST['TriggerValueDate']);
  				break;
  			case "Field":
  				$trigger->TriggerObject = $_POST['TriggerObjectField'];
  				$trigger->TriggerValue = $_POST['TriggerValueField'];
  				break;
  			default:
  				$trigger->TriggerObject = "";
  				$trigger->TriggerValue = "";
  				break;
  		}
  		$trigger->FillCriteria($_POST['LeftValue'], $_POST['CompareValue'],  $_POST['RightValue']);
  		
  		$portal->UpdateTrigger($trigger);
  		
  		if(!isset($_POST['ProductID']))
  		{
  			$productTriggerArray = array();
  		}
  		else
  		{
  			$productTriggerArray = array_combine($_POST['ProductID'], $_POST['TimeOffset']);
  		}
  		
  		$portal->AddProductTriggers($trigger->TriggerID, $productTriggerArray);
		
		header("Location: manage_trigger.php?message=". urlencode("Action Completed. Trigger updated."));
		die();
	}
	
	// Create array of fields that are Lookup driven
	$lfs = $portal->GetLookupFieldNames();
	$lookupFields = array();
	foreach($lfs as $lf) {
		$lookupFields[$lf] = 1;
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
	<title>
	<?= $portal->CurrentCompany->CompanyName ?> :: Edit Trigger
	</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />		
	<link type="text/css" media="all" rel="stylesheet" href="../style.css" />		
	<link type="text/css" media="all" rel="stylesheet" href="style.css" />	
<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />
	<script  src="../include/js/func.js"></script>
<script>
	var transactionURL = "../printable_trans.php";
	var lookupFieldRequest;

	var lookupFields = new Array();
	lookupFields["GroupName"] = 1;
	lookupFields["RegionName"] = 1;
	lookupFields["DivisionName"] = 1;
<?php
	foreach($contactFields as $k)
	{ 
		if($lookupFields[$k] == 1)
		{
?>
	lookupFields["<?= $k ?>"] = 1;
<?php
		}
	 } 
?>

	function AddCriteriaRuleRow()
	{
		var critTable = document.getElementById('CriteriaTable');
		var lastRow = critTable.rows[critTable.rows.length - 1];
		var iteration = (isNaN(parseInt(lastRow.id.substring(7))) ? 0 : parseInt(lastRow.id.substring(7))) + 1;
		var row = critTable.insertRow(critTable.rows.length);

		row.setAttribute("class", (iteration%2 == 0 ? 'tableDetail' : 'tableDetailLight' ));
		row.id = "CritRow" + iteration;

		// LeftVal cell
		var leftValue = row.insertCell(0);
		var sel = GenerateCriteriaSelect(iteration - 1);
		leftValue.appendChild(sel);
		leftValue.style.textAlign = 'center';
		// Compare cell
		var compareValue = row.insertCell(1);
		var compSel = GenerateCompareSelect(iteration - 1);
		compareValue.appendChild(compSel);
		compareValue.style.textAlign = 'center';
		// RightVal cell
		var rightValue = row.insertCell(2);
		rightValue.id = "RightValueCell" + iteration;
		var el = document.createElement('input');
		el.type = 'text';
		el.name = 'RightValue[' + (iteration - 1) + ']';
		el.id = 'RightValue[' + (iteration - 1) + ']';
		rightValue.appendChild(el);
		// Link cell
		var linkCell = row.insertCell(3);
		linkCell.innerHTML = "<a href='#' onclick='DeleteCriteriaRuleRow(" + iteration + "); return false;'>delete</a>";
	}

	function GenerateCriteriaSelect(rowNumber)
	{
		var sel = document.createElement('select');
		sel.name = 'LeftValue[' + (rowNumber) + ']';
		sel.id = 'LeftValue[' + (rowNumber) + ']';
		sel.onchange = function(){ UpdateRightValue(rowNumber + 1); }
<?php
	$i = 0;
	foreach($contactFields as $k)
	{
?>
		sel.options[<?= $i ?>] = new Option('<?= $k ?>', '<?= $k ?>');
<?php
		$i++;
	}
?>
		return sel;
	}

	function GenerateCompareSelect(rowNumber)
	{
		var sel = document.createElement('select');
		sel.name = 'CompareValue[' + (rowNumber) + ']';
		sel.id = 'CompareValue[' + (rowNumber) + ']';
<?php
	$i = 0;
	foreach($portal->Compares as $k=>$v)
	{
?>
		sel.options[<?= $i ?>] = new Option('<?= $k ?>', '<?= $k ?>');
<?php
		$i++;
	}
?>
		return sel;
	}

	function DeleteCriteriaRuleRow(rowNumber)
	{
		var row = document.getElementById('CritRow'+rowNumber);
		row.parentNode.removeChild(row);

		// Loop and fix colors
		var critTable = document.getElementById('CriteriaTable');
		for(var i = 1; i < critTable.rows.length; i++)
		{
			critTable.rows[i].setAttribute("class", (i%2 == 0 ? 'tableDetail' : 'tableDetailLight' ));
		}
	}

	function ShowTriggerType(type)
	{
		dateDiv = document.getElementById('DateTriggerType');
		fieldDiv = document.getElementById('FieldTriggerType');

		dateDiv.style.display = "none";
		fieldDiv.style.display = "none";
		
		if(type == 'Field')
		{
			fieldDiv.style.display = "";
		}
		else if(type == "Date")
		{
			dateDiv.style.display = "";
		}
	}

	function AddActionRow()
	{
		var actTable = document.getElementById('ActionTable');
		var lastRow = actTable.rows[actTable.rows.length - 1];
		var iteration = parseInt(lastRow.id.substring(9)) + 1;
		var row = actTable.insertRow(actTable.rows.length);

		row.setAttribute("class", (iteration%2 == 0 ? 'tableDetail' : 'tableDetailLight' ));
		row.id = "ActionRow" + iteration;

		// ProductID cell
		var productCell = row.insertCell(0);
		var sel = GenerateProductSelect(iteration - 1);
		productCell.appendChild(sel);
		productCell.style.textAlign = 'center';
		// TimeOffset cell
		var offsetCell = row.insertCell(1);
		var el = document.createElement('input');
		el.type = 'text';
		el.name = 'TimeOffset[' + (iteration - 1) + ']';
		el.id = 'TimeOffset[' + (iteration - 1) + ']';
		el.value = 0;
		el.style.width = "25px";
		offsetCell.appendChild(el);
		offsetCell.appendChild(document.createTextNode(" days"));
		// Link cell
		var linkCell = row.insertCell(2);
		linkCell.innerHTML = "<a href='#' onclick='DeleteActionRow(" + iteration + "); return false;'>delete</a>";
	}

	function GenerateProductSelect(rowNumber)
	{
		var sel = document.createElement('select');
		sel.name = 'ProductID[' + (rowNumber) + ']';
		sel.id = 'ProductID[' + (rowNumber) + ']';
<?php
	$i = 0;
	foreach($products as $prod)
	{
		if($prod->GroupID == 0)
		{
?>
		sel.options[<?= $i ?>] = new Option('<?= $prod->ProductName ?>', '<?= $prod->ProductID ?>');
<?php
			$i++;
		}
	}
?>
		return sel;
	}

	function DeleteActionRow(rowNumber)
	{
		var row = document.getElementById('ActionRow'+rowNumber);
		row.parentNode.removeChild(row);

		// Loop and fix colors
		var actTable = document.getElementById('ActionTable');
		for(var i = 1; i < actTable.rows.length; i++)
		{
			actTable.rows[i].setAttribute("class", (i%2 == 0 ? 'tableDetail' : 'tableDetailLight' ));
		}
	}

	function UpdateRightValue(rowNumber)
	{
		// Remove existing contents from cell
		var rightCell = document.getElementById('RightValueCell'+rowNumber);
		var fieldName = document.getElementById('LeftValue[' + (rowNumber - 1).toString() + ']').value;
		var oldInput = document.getElementById('RightValue[' + (rowNumber - 1).toString() + ']');
		rightCell.removeChild(oldInput);
		
		// Check if Lookup Field
		if(lookupFields[fieldName] == 1) {
			// If Lookup field create select and trigger load
			var el = document.createElement('select');
			el.name = 'RightValue[' + (rowNumber - 1) + ']';
			el.id = 'RightValue[' + (rowNumber - 1) + ']';
			el.options[0] = new Option('Loading...', '');
			LoadLookupOptions(rowNumber, fieldName);
		}
		else {
			// If not Lookup create basic input
			var el = document.createElement('input');
			el.type = 'text';
			el.name = 'RightValue[' + (rowNumber - 1) + ']';
			el.id = 'RightValue[' + (rowNumber - 1) + ']';
		}

		rightCell.appendChild(el);
	}

	function LoadLookupOptions(rowNumber, fieldName)
	{
		lookupFieldRequest = GetXmlHttpObject()
		if (lookupFieldRequest==null)
		{
			alert ("Your browser does not support AJAX!");
			return;
		} 
		var url=transactionURL;
		url=url+"?action=LookupOptions&RowNumber=" + rowNumber;
		url=url+"&FieldName=" + fieldName;
		url=url+"&sid="+Math.random();
		lookupFieldRequest.onreadystatechange=lookupFieldStateChanged;
		lookupFieldRequest.open("GET",url,true);
		lookupFieldRequest.send(null);	
	}

	function lookupFieldStateChanged()
	{
		if (lookupFieldRequest.readyState==4)
		{ 
			var xmlDoc=lookupFieldRequest.responseXML.documentElement;
			
			// Check if session timedout	
			if(xmlDoc.getElementsByTagName("Status").length > 0 && xmlDoc.getElementsByTagName("Status")[0].childNodes[0].nodeValue == 'Timeout')
			{
				parent.location="../login.php?logout=true&message='Your+Session+has+timedout.+Please+Login+Again.";
				return;
			}
			var sel = document.getElementById('RightValue[' 
						+ (xmlDoc.getElementsByTagName("RowNumber")[0].childNodes[0].nodeValue - 1).toString() 
						+ ']');
			sel.length = 0;
			for(var i = 0; i < xmlDoc.getElementsByTagName("Fields")[0].childNodes.length; i++) {
				var field = xmlDoc.getElementsByTagName("Fields")[0].childNodes[i];
				var value;
				var label;
				if(field.getElementsByTagName("Value")[0].childNodes[0])
					value = field.getElementsByTagName("Value")[0].childNodes[0].nodeValue
				else
					value = "";
				if(field.getElementsByTagName("Label")[0].childNodes[0])
					label = field.getElementsByTagName("Label")[0].childNodes[0].nodeValue
				else
					label = "";
					
				sel.options[i] = new Option( value, value );
			}
		}	
	}
	
	function initPage()
	{
		<?= $isAdmin ? "" : "DisableInput();" ?>
	}

	function DisableInput()
	{
		var inputs=document.getElementsByTagName('input');
	
		for(i=0; i < inputs.length; i++)
		{
			inputs[i].disabled = true;
		}

		var selects=document.getElementsByTagName('select');
		
		for(i=0; i < selects.length; i++)
		{
			selects[i].disabled = true;
		}
		
		var areas=document.getElementsByTagName('textarea');
		
		for(i=0; i < areas.length; i++)
		{
			areas[i].disabled = true;
		}
	}
</script>
	
</head>
<body bgcolor="#FFFFFF" onload='initPage();'>
<?php
	include("header.php");
	include("navbar.php");
?>
<table cellspacing="0" cellpadding="0" width="770" bgcolor="#FFFFFF">
	<tr>
  		<td>
			<table border="0" cellpadding="0" cellspacing="0" align="center" width="100%">
				<tr>
					<td>
					<?php
						$CURRENTPAGE = 'triggermanage';
					?>
					</td>
				</tr>
			</table> 
			<table cellpadding="0" width="700">
				<tr>
	  				<td>
	  					<form method="post" action="<?= $_SERVER['PHP_SELF'] ?>?id=<?= $trigger->TriggerID ?>" enctype="multipart/form-data">
	  					<div align="center" class="headerTextLarge">Manage Print Trigger</div>
	  					<div align="center" class="headerTextLarge"><?= $_GET['message'] ?></div>
			  			<div class="fieldSection">
			  				<div class="itemSection">
			  					<label>Trigger Name:</label><br/>
			  					<input type="text" name="TriggerName" value="<?= $trigger->TriggerName ?>"/>
			  				</div>
			  				<div class="itemSection">
			  					<label>Trigger Type:</label><br/>
			  					<select name="TriggerType" onchange="ShowTriggerType(this.value);">
			  						<option value="Manual" <?= $trigger->TriggerType == "Manual" ? "SELECTED" : "" ?>>Manual</option>
			  						<option value="Date" <?= $trigger->TriggerType == "Date" ? "SELECTED" : "" ?>>Date</option>
			  						<option value="Field" <?= $trigger->TriggerType == "Field" ? "SELECTED" : "" ?>>Field Change</option>
			  					</select>
			  				</div>	
			  				<div class="itemSection" id="DateTriggerType" <?= $trigger->TriggerType == "Date" ? "" : "style='display: none;'" ?>>
			  					<label>Trigger On:</label><br/>
			  					<select name="TriggerObjectDate">
			  						<option value="">Please Choose...</option>
<?php
		foreach($dateFields as $v)
		{
?>
									<option value="<?= $v?>" <?= $v == $trigger->TriggerObject ? "SELECTED" : "" ?>><?= $v ?></option>									
<?php
		}
?>
			  					</select>
			  					is past
			  					<input name="TriggerValueDate" value="<?= $trigger->TriggerValue ?>" size="4" /> days
			  				</div>		
			  				<div class="itemSection" id="FieldTriggerType" <?= $trigger->TriggerType == "Field" ? "" : "style='display: none;'" ?>>
			  					<label>Trigger On:</label><br/>
			  					<select name="TriggerObjectField">
			  						<option value="">Please Choose...</option>
<?php
		foreach($contactFields as $k)
		{
?>
									<option value="<?= $k?>" <?= $k == $trigger->TriggerObject ? "SELECTED" : "" ?>><?= $k ?></option>									
<?php
		}
?>
			  					</select>
			  					Changed To
			  					<input name="TriggerValueField" value="<?= $trigger->TriggerValue ?>" />
			  				</div>	  				
			  				<div class="itemSection">
			  					<label>Criteria:</label><br/>
			  					<table width="100%" id="CriteriaTable">
			  						<tr class="tableHeader">
			  							<td>Left Value</td>
			  							<td align="center">&nbsp;</td>
			  							<td>Right Value</td>
										<?php
										if ($isAdmin){
										?>
			  							<td>&nbsp;</td>
										<?php
										}
										?>
			  						</tr>
<?php
	$i = 0;
	foreach($trigger->Criteria as $crit)
	{
?>
									<tr class="<?= $i % 2 == 0 ? "tableDetailLight" : "tableDetail"?>" id="CritRow<?= $i+1 ?>">
										<td align="center">
											<select name="LeftValue[<?= $i ?>]" id="LeftValue[<?= $i ?>]" onchange="UpdateRightValue(<?= $i+1 ?>)">
<?php
		foreach($contactFields as $k)
		{
?>
												<option value="<?= $k?>" <?= $k == $crit['LeftValue'] ? "SELECTED" : "" ?>><?= $k ?></option>									
<?php
		}
?>
											</select>
										</td>
										<td align="center">
											<select name="CompareValue[<?= $i ?>]">
<?php
		foreach($portal->Compares as $k=>$v)
		{
?>
												<option value="<?= $v ?>" <?= $k == $crit['CompareValue'] ? "SELECTED" : "" ?>><?= $v ?></option>									
<?php
		}
?>
											</select>
										</td>
										<td id="RightValueCell<?= $i+1 ?>">
<?php
		if($lookupFields[$crit['LeftValue']] == 1) {
?>
											<select name="RightValue[<?= $i ?>]" id="RightValue[<?= $i ?>]">
<?php
			$fields = $portal->GetLookupValues($crit['LeftValue']);
			foreach($fields as $f) {
?>
												<option value="<?= $f->LookupValue ?>" <?= $f->LookupValue == $crit['RightValue'] ? 'SELECTED' : '' ?>><?= $f->LookupLabel ?></option>
<?php 
			} 
?>
											</select>
<?php 
		}
		else {
?>
											<input type="text" name="RightValue[<?= $i ?>]" id="RightValue[<?= $i ?>]" value="<?= $crit['RightValue'] ?>" />
<?php
		} 
?>
<?php
										if ($isAdmin){
										?>
										<td><a href="#" onclick="DeleteCriteriaRuleRow(<?= $i+1 ?>); return false;">delete</a></td>
										<?php
										}
										?>
									</tr>
<?php
		$i++;
	}
?>
			  					</table>
								<?php
										if ($isAdmin){
										?>
			  					<div>
			  						<a href="#" onclick="AddCriteriaRuleRow(); return false;">Add Criteria</a>
			  					</div>
								<?php
								}
								?>
			  				</div>
			  				<div class="itemSection">
			  					<label>Actions:</label><br/>
			  					<table width="100%" id="ActionTable">
			  						<tr class="tableHeader" id="ActionRow0">
			  							<td>Product</td>
			  							<td>Wait Time</td>
										<?php
										if ($isAdmin){
										?>
			  							<td>&nbsp;</td>
										<?php
										}
										?>
			  						</tr>
<?php
	$i = 0;
	foreach($triggerProducts as $trigProd)
	{
?>
									<tr class="<?= $i % 2 == 0 ? "tableDetailLight" : "tableDetail"?>" id="ActionRow<?= $i+1 ?>">
										<td align="center">
											<select name="ProductID[<?= $i ?>]">
<?php
		foreach($products as $prod)
		{
			if($prod->GroupID == 0)
			{
?>
												<option value="<?= $prod->ProductID ?>" <?= $prod->ProductID == $trigProd->ProductID ? "SELECTED" : "" ?>><?= $prod->ProductName ?></option>									
<?php
			}
		}
?>
											</select>
										</td>
										<td align="left">
											<input name="TimeOffset[<?= $i ?>]" value="<?= $trigProd->TimeOffset ?>" style="width: 25px;"/> days
										</td>
										<?php
										if ($isAdmin){
										?>
										<td><a href="#" onclick="DeleteActionRow(<?= $i+1 ?>); return false;">delete</a></td>
										<?php
										}
										?>
									</tr>
<?php
		$i++;
	}
?>
			  					</table>
								<?php
										if ($isAdmin){
										?>
			  					<div>
			  						<a href="#" onclick="AddActionRow(); return false;">Add Action</a>
			  					</div>
								<?php
								}
								?>
			  				</div>
							<?php
										if ($isAdmin){
										?>
			  				<div class="itemSection">
			  					<center>
			  						<input type="submit" value="Update" name="Submit"/><input type="submit" value="Cancel" name="Submit"/>
			  					</center>
			  				</div>
							<?php
							}
							?>
			  			</div>
			  			</form>
	  				</td>
         		</tr>
         	</table>
		</td>
	</tr>
</table>
</body>
</html>