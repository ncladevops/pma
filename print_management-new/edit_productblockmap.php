<?php
	require("../../printable/include/mysql.inc.php");
	require("../../printable/include/printable.inc.php");
	require("../../printable/include/contactfactory.inc.php");
	require("../globals.php");
		
	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	
	$portal = new PrintablePortal($COMPANY_ID, $db);
	$factory = new ContactFactory($db);
	
	$contactFields = $factory->GetContactFields($portal->CurrentCompany->CompanyID);
	$dateFields = $factory->GetDateFields($portal->CurrentCompany->CompanyID);
	
	$currentUser = $portal->GetUser($_SESSION['currentuserid']);
	$isAdmin = $portal->CheckPriv($currentUser->UserID, 'admin');
		
	// Check login
	if(!$portal->CheckPriv($currentUser->UserID, 'subadmin'))
	{
		header( "Location: " . $portal->CurrentCompany->Website . "login.php?message=" . urlencode( "Not logged in or login error.  Please try again." ) );
		die();
	}
	
	$blockMap = $portal->GetProductBlockMap($_GET['pbmid']);
	
	$product = $portal->GetProduct($blockMap->ProductID);
	
	$blocks = $portal->GetProductBlocks();
	
	if(!$blockMap || !$product)
	{
		header("Location: index.php?message=" . urlencode("Invalid Product Block Rule ID"));
		die();
	}
	
	if($_GET['action'] == 'delete')
	{
		$portal->DeleteProductBlockMap($blockMap->ProductBlockMapID);
		
		header("Location: edit_productblock.php?productid={$blockMap->ProductID}&variable={$blockMap->Variable}&message=". urlencode("Action Completed. Product Block Map deleted."));
		die();
	}
	elseif($_GET['action'] == 'moveup')
	{
		$portal->MoveProductBlockMap($blockMap->ProductBlockMapID, 'up');
		
		header("Location: edit_productblock.php?productid={$blockMap->ProductID}&variable={$blockMap->Variable}&message=". urlencode("Action Completed. Product Block Map deleted."));
		die();
	}
	elseif($_GET['action'] == 'movedown')
	{
		$portal->MoveProductBlockMap($blockMap->ProductBlockMapID, 'down');
		
		header("Location: edit_productblock.php?productid={$blockMap->ProductID}&variable={$blockMap->Variable}&message=". urlencode("Action Completed. Product Block Map deleted."));
		die();
	}
	elseif($_POST['Submit'] == 'Cancel')
	{
		header("Location: edit_productblock.php?productid={$blockMap->ProductID}&variable={$blockMap->Variable}&message=". urlencode("Action Canceled. Product Block Map not updated."));
		die();
	}
	elseif($_POST['Submit'] == 'Update')
	{
		$blockMap->ProductBlockID = $_POST['ProductBlockID'];
		$blockMap->FillCriteria($_POST['LeftValue'], $_POST['CompareValue'], $_POST['RightValue']);
		
		$portal->UpdateProductBlockMap($blockMap);
		
		header("Location: edit_productblock.php?productid={$blockMap->ProductID}&variable={$blockMap->Variable}&message=". urlencode("Action Completed. Product Block Map Updated."));
		die();
	}
	
	// Create array of fields that are Lookup driven
	$lfs = $portal->GetLookupFieldNames();
	$lookupFields = array();
	foreach($lfs as $lf) {
		$lookupFields[$lf] = 1;
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
	<title>
		<?= $portal->CurrentCompany->CompanyName ?> :: Edit Product Block Map
	</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />		
	<link type="text/css" media="all" rel="stylesheet" href="../style.css" />		
	<link type="text/css" media="all" rel="stylesheet" href="style.css" />	
<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />
	<script  src="../include/js/func.js"></script>
<script>
	var transactionURL = "../printable_trans.php";
	var lookupFieldRequest;

	var lookupFields = new Array();
	lookupFields["GroupName"] = 1;
	lookupFields["RegionName"] = 1;
	lookupFields["DivisionName"] = 1;
<?php
	foreach($contactFields as $k)
	{ 
		if($lookupFields[$k] == 1)
		{
?>
	lookupFields["<?= $k ?>"] = 1;
<?php
		}
	 } 
?>

	function AddCriteriaRuleRow()
	{
		var critTable = document.getElementById('CriteriaTable');
		var lastRow = critTable.rows[critTable.rows.length - 1];
		var iteration = (isNaN(parseInt(lastRow.id.substring(7))) ? 0 : parseInt(lastRow.id.substring(7))) + 1;
		var row = critTable.insertRow(critTable.rows.length);

		row.setAttribute("class", (iteration%2 == 0 ? 'tableDetail' : 'tableDetailLight' ));
		row.id = "CritRow" + iteration;

		// LeftVal cell
		var leftValue = row.insertCell(0);
		var sel = GenerateCriteriaSelect(iteration - 1);
		leftValue.appendChild(sel);
		leftValue.style.textAlign = 'center';
		// Compare cell
		var compareValue = row.insertCell(1);
		var compSel = GenerateCompareSelect(iteration - 1);
		compareValue.appendChild(compSel);
		compareValue.style.textAlign = 'center';
		// RightVal cell
		var rightValue = row.insertCell(2);
		rightValue.id = "RightValueCell" + iteration;
		var el = document.createElement('input');
		el.type = 'text';
		el.name = 'RightValue[' + (iteration - 1) + ']';
		el.id = 'RightValue[' + (iteration - 1) + ']';
		rightValue.appendChild(el);
		// Link cell
		var linkCell = row.insertCell(3);
		linkCell.innerHTML = "<a href='#' onclick='DeleteCriteriaRuleRow(" + iteration + "); return false;'>delete</a>";
	}

	function GenerateCriteriaSelect(rowNumber)
	{
		var sel = document.createElement('select');
		sel.name = 'LeftValue[' + (rowNumber) + ']';
		sel.id = 'LeftValue[' + (rowNumber) + ']';
		sel.onchange = function(){ UpdateRightValue(rowNumber + 1); }
<?php
	$i = 0;
	foreach($contactFields as $k)
	{
?>
		sel.options[<?= $i ?>] = new Option('<?= $k ?>', '<?= $k ?>');
<?php
		$i++;
	}
?>
		return sel;
	}

	function GenerateCompareSelect(rowNumber)
	{
		var sel = document.createElement('select');
		sel.name = 'CompareValue[' + (rowNumber) + ']';
		sel.id = 'CompareValue[' + (rowNumber) + ']';
<?php
	$i = 0;
	foreach($portal->Compares as $k=>$v)
	{
?>
		sel.options[<?= $i ?>] = new Option('<?= $k ?>', '<?= $k ?>');
<?php
		$i++;
	}
?>
		return sel;
	}

	function DeleteCriteriaRuleRow(rowNumber)
	{
		var row = document.getElementById('CritRow'+rowNumber);
		row.parentNode.removeChild(row);

		// Loop and fix colors
		var critTable = document.getElementById('CriteriaTable');
		for(var i = 1; i < critTable.rows.length; i++)
		{
			critTable.rows[i].setAttribute("class", (i%2 == 0 ? 'tableDetail' : 'tableDetailLight' ));
		}
	}

	function ChooseBlock(id, name)
	{
		var blockName = document.getElementById('blockNameSpan');
		var blockImage = document.getElementById('blockImageLink');
		var blockInput = document.getElementById('ProductBlockID');
		blockName.innerHTML = name;
		blockImage.innerHTML = '<img src="/printable/getblockimage.php?id=' + id + '&thumb=true"  style="vertical-align: middle;" />';
		blockImage.href = '/printable/getblockimage.php?id=' + id;
		blockInput.value = id;
		
	}

	function UpdateRightValue(rowNumber)
	{
		// Remove existing contents from cell
		var rightCell = document.getElementById('RightValueCell'+rowNumber);
		var fieldName = document.getElementById('LeftValue[' + (rowNumber - 1).toString() + ']').value;
		var oldInput = document.getElementById('RightValue[' + (rowNumber - 1).toString() + ']');
		rightCell.removeChild(oldInput);
		
		// Check if Lookup Field
		if(lookupFields[fieldName] == 1) {
			// If Lookup field create select and trigger load
			var el = document.createElement('select');
			el.name = 'RightValue[' + (rowNumber - 1) + ']';
			el.id = 'RightValue[' + (rowNumber - 1) + ']';
			el.options[0] = new Option('Loading...', '');
			LoadLookupOptions(rowNumber, fieldName);
		}
		else {
			// If not Lookup create basic input
			var el = document.createElement('input');
			el.type = 'text';
			el.name = 'RightValue[' + (rowNumber - 1) + ']';
			el.id = 'RightValue[' + (rowNumber - 1) + ']';
		}

		rightCell.appendChild(el);
	}

	function LoadLookupOptions(rowNumber, fieldName)
	{
		lookupFieldRequest = GetXmlHttpObject()
		if (lookupFieldRequest==null)
		{
			alert ("Your browser does not support AJAX!");
			return;
		} 
		var url=transactionURL;
		url=url+"?action=LookupOptions&RowNumber=" + rowNumber;
		url=url+"&FieldName=" + fieldName;
		url=url+"&sid="+Math.random();
		lookupFieldRequest.onreadystatechange=lookupFieldStateChanged;
		lookupFieldRequest.open("GET",url,true);
		lookupFieldRequest.send(null);	
	}

	function lookupFieldStateChanged()
	{
		if (lookupFieldRequest.readyState==4)
		{ 
			var xmlDoc=lookupFieldRequest.responseXML.documentElement;
			
			// Check if session timedout	
			if(xmlDoc.getElementsByTagName("Status").length > 0 && xmlDoc.getElementsByTagName("Status")[0].childNodes[0].nodeValue == 'Timeout')
			{
				parent.location="../login.php?logout=true&message='Your+Session+has+timedout.+Please+Login+Again.";
				return;
			}
			var sel = document.getElementById('RightValue[' 
						+ (xmlDoc.getElementsByTagName("RowNumber")[0].childNodes[0].nodeValue - 1).toString() 
						+ ']');
			sel.length = 0;
			for(var i = 0; i < xmlDoc.getElementsByTagName("Fields")[0].childNodes.length; i++) {
				var field = xmlDoc.getElementsByTagName("Fields")[0].childNodes[i];
				var value;
				var label;
				if(field.getElementsByTagName("Value")[0].childNodes[0])
					value = field.getElementsByTagName("Value")[0].childNodes[0].nodeValue
				else
					value = "";
				if(field.getElementsByTagName("Label")[0].childNodes[0])
					label = field.getElementsByTagName("Label")[0].childNodes[0].nodeValue
				else
					label = "";
					
				sel.options[i] = new Option( value, value );
			}
		}	
	}
</script>	
</head>
<body bgcolor="#FFFFFF">
<?php
		include("header.php");
		include("navbar.php");
?>
<table cellspacing="0" cellpadding="0" width="770" align="center" bgcolor="#FFFFFF">
	<tr>
  		<td>
			<table border="0" cellpadding="0" cellspacing="0" align="center" width="100%">
				<tr>
					<td>
					<?php
						$CURRENTPAGE = 'productmanage';
						 
					?>
					</td>
				</tr>
			</table>
			<table cellpadding="0" width="700">
				<tr>
	  				<td>
	  					<form method="post" action="<?= $_SERVER['PHP_SELF'] ?>?pbmid=<?= $blockMap->ProductBlockMapID ?>">
	  					<div align="center" class="headerTextLarge">Manage Print Product Variable Block Mapping Rule</div>
	  					<div align="center" class="headerTextLarge"><?= $HTTP_GET_VARS['message'] ?></div>
			  			<div class="fieldSection">
			  				<div class="itemSection">
			  					<label>Product Name:</label><br/>
			  					<?= $product->ProductName; ?>
			  				</div>
			  				<div class="itemSection">
			  					<label>Variable Section:</label><br/>
			  					<?= $blockMap->Variable + 1 ?>
			  				</div>
			  				<div class="itemSection">
			  					<label>Rule #:</label><br/>
			  					<?= $blockMap->SortOrder + 1 ?>
			  				</div>
			  				<div class="itemSection">
			  					<label>Product Block:</label><br/>
			  					<input type="hidden" name="ProductBlockID" value="<?= $blockMap->ProductBlockID ?>" id="ProductBlockID" />
								<span id="blockNameSpan"><?= $blockMap->BlockName != "" ? $blockMap->BlockName : "None Selected" ?></span><br/> 
								<a href="<?= $blockMap->BlockThumbnailPath; ?>" target="_blank" id="blockImageLink">
									<img src="<?= $blockMap->BlockThumbnailPath; ?>&thumb=true"  style="vertical-align: middle;" />
								</a><br/>
												<?php
	if($isAdmin) {
?>
								<a href="list_productblocks.php" onclick="window.open('list_productblocks.php', 'blocklist', 'width=800,height=600,scrollbars=yes;resizable=yes'); return false;">Change</a>
								<?php
								}
								?>
			  				</div>
			  				
			  				<div class="itemSection">
			  					<label>Criteria:</label><br/>
			  					<table width="100%" id="CriteriaTable">
			  						<tr class="tableHeader">
			  							<td>Left Value</td>
			  							<td align="center">&nbsp;</td>
			  							<td>Right Value</td>
										<?php
	if($isAdmin) {
?>
			  							<td>&nbsp;</td>
										<?php
										}
										?>
			  						</tr>
<?php
	$i = 0;
	foreach($blockMap->Criteria as $crit)
	{
?>
									<tr class="<?= $i % 2 == 0 ? "tableDetailLight" : "tableDetail"?>" id="CritRow<?= $i+1 ?>">
										<td align="center">
											<select name="LeftValue[<?= $i ?>]" id="LeftValue[<?= $i ?>]" onchange="UpdateRightValue(<?= $i+1 ?>)">
<?php
		foreach($contactFields as $k)
		{
?>
												<option value="<?= $k?>" <?= $k == $crit['LeftValue'] ? "SELECTED" : "" ?>><?= $k ?></option>									
<?php
		}
?>
											</select>
										</td>
										<td align="center">
											<select name="CompareValue[<?= $i ?>]">
<?php
		foreach($portal->Compares as $k=>$v)
		{
?>
												<option value="<?= $v ?>" <?= $k == $crit['CompareValue'] ? "SELECTED" : "" ?>><?= $v ?></option>									
<?php
		}
?>
											</select>
										</td>
										<td id="RightValueCell<?= $i+1 ?>">
<?php
		if($lookupFields[$crit['LeftValue']] == 1) {
?>
											<select name="RightValue[<?= $i ?>]" id="RightValue[<?= $i ?>]">
<?php
			$fields = $portal->GetLookupValues($crit['LeftValue']);
			foreach($fields as $f) {
?>
												<option value="<?= $f->LookupValue ?>" <?= $f->LookupValue == $crit['RightValue'] ? 'SELECTED' : '' ?>><?= $f->LookupLabel ?></option>
<?php 
			} 
?>
											</select>
<?php 
		}
		else {
?>
											<input type="text" name="RightValue[<?= $i ?>]" id="RightValue[<?= $i ?>]" value="<?= $crit['RightValue'] ?>" />
<?php
		} 
?>
										</td>
										<?php
	if($isAdmin) {
?>
										<td><a href="#" onclick="DeleteCriteriaRuleRow(<?= $i+1 ?>); return false;">delete</a></td>
										<?php
										}
										?>
									</tr>
<?php
		$i++;
	}
?>
			  					</table>
								<?php
	if($isAdmin) {
?>
			  					<div>
			  						<a href="#" onclick="AddCriteriaRuleRow(); return false;">Add Criteria</a>
			  					</div>
								<?php
								}
								?>
			  				</div>
							<?php
	if($isAdmin) {
?>
			  				<div class="itemSection">
			  					<center>
			  						<input type="submit" value="Update" name="Submit"/><input type="submit" value="Cancel" name="Submit"/>
			  					</center>
			  				</div>
							<?php
							}
							?>
			  			</div>
	  				</td>
         		</tr>
         	</table>
		</td>
	</tr>
</table>
</body>
</html>