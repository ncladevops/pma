<?php
/*<table width="770">
	<tr bgcolor="185f8d" valign="middle" align="center">
		<td class="navbar_item"><a href="../controlpanel.php">&lt;- Return to<br/>Control Panel</a></td>
		<td class="<?= ( $CURRENTPAGE == 'printmanage' ? 'navbar_item_selected' : 'navbar_item' ) ?>"><a href="index.php">Print<br/>Management</a></td>
		<td class="<?= ( $CURRENTPAGE == 'productmanage' ? 'navbar_item_selected' : 'navbar_item' ) ?>"><a href="manage_product.php">Product<br/>Management</a></td>
		<td class="<?= ( $CURRENTPAGE == 'triggermanage' ? 'navbar_item_selected' : 'navbar_item' ) ?>"><a href="manage_trigger.php">Trigger<br/>Management</a></td>
	</tr>
</table>
*/
?>
<div id="navbar_container">
	<div id="navbar_fullsize">
		<div class="navbar_item" 
			onclick="window.location = '../controlpanel.php';">
			<a href="../controlpanel.php">
				Control Panel
			</a>
		</div>
		<div class="navbar_item<?= $CURRENTPAGE == 'printmanage' ? '_selected' : '' ?>" 
			onclick="window.location = 'index.php';">
			<a href="index.php">
				Print Management
			</a>
		</div>
		<div class="navbar_item<?= $CURRENTPAGE == 'productmanage' ? '_selected' : '' ?>" 
			onclick="window.location = 'manage_product.php';">
			<a href="manage_product.php">
				Product Management
			</a>
		</div>
		<div class="navbar_item<?= $CURRENTPAGE == 'triggermanage' ? '_selected' : '' ?>" 
			onclick="window.location = 'manage_trigger.php';">
			<a href="manage_trigger.php">
				Trigger Management
			</a>
		</div>
		<div class="navbar_item" 
			onclick="window.location = '../admin';">
			<a href="../admin">
				Admin Panel
			</a>
		</div>
		<div class="navbar_item" 
			onclick="window.location = '../login.php?logout=true';">
			<a href="../login.php?logout=true">
				Log Out
			</a>
		</div>
		<div class="navbar_item navbar_rightspacer" >
		</div>
	</div>
</div>