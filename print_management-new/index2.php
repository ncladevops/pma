<?php
	require("../../printable/include/mysql.inc.php");
	require("../../printable/include/printable.inc.php");
	require("../globals.php");
	
	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	
	$portal = new PrintablePortal($COMPANY_ID, $db);
		
	$currentUser = $portal->GetUser($_SESSION['currentuserid']);
		
	// Check login
	if(!$portal->CheckPriv($currentUser->UserID, 'subadmin'))
	{
		header( "Location: " . $portal->CurrentCompany->Website . "login.php?message=" . urlencode( "Not logged in or login error.  Please try again." ) );
		die();
	}

			
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
	<title>
		<?= $portal->CurrentCompany->CompanyName ?> :: Control Panel
	</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />		
	<link type="text/css" media="all" rel="stylesheet" href="../style.css" />
<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />	
	<script  src="../include/js/func.js"></script>	
</head>
<body bgcolor="#FFFFFF">
<table cellspacing="0" cellpadding="0" width="770" align="center" bgcolor="#FFFFFF">
	<tr>
  		<td>
			<table border="0" cellPadding="0" cellSpacing="0" align="center" width="100%">
				<tr>
			
					<td class="MastHead"><img src="../images/main_banner.jpg" alt="" border="0" title="" /></td>
				</tr>
				<tr>
					<td>
					<?php
						$CURRENTPAGE = 'printmanage';
						include("navbar.php") 
					?>
					</td>
				</tr>
			</table>
			<table width="730" align="center">
				<tr align="right">
					<td>
						<a href="../login.php?logout=true" class="blueLink">Log Out</a> - <a href="../changepass.php" class="blueLink">Change Password</a>
					</td>
				</tr>
			</table> 
			<table cellpadding="0" align="center" width="700">
				<tr>
	  				<td>
	  					<div align="center" class="headerTextLarge">Campaign Management Control Panel</div>
	  					<div align="center" class="headerTextLarge"><?= $HTTP_GET_VARS['message'] ?></div>
			  			<table width="415" border="0" cellpadding="0" cellspacing="0">
		                  <tr>
		                    <td>
		                    	<table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
			                        <tr>
			                          <td width="17" height="17"><img src="../images/box_ab_r1_c1.jpg" width="17" height="17" border="0"/></td>
			                          <td background="../images/box_ab_r1_c3.jpg" style="font-size:6px;">&nbsp;</td>
			                          <td width="17" height="17"><img src="../images/box_ab_r1_c4.jpg" width="17" height="17" border="0" /></td>
			                        </tr>
		                    	</table>
		                    </td>
		                  </tr>
		                  <tr>
		                    <td><table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
		                        <tr>
		                          <td width="10" background="../images/box_ab_r2_c1.jpg">&nbsp;</td>
		                          <td background="../images/box_ab_r2_c2.jpg">
		                          <ul>
		                            <li><a href="manage_product.php" class="blueLink"><img src="../images/add1.gif" width="48" height="56" align="absmiddle" border="0"/>Manage Products</a></li>
		                            <li><a href="manage_trigger.php" class="blueLink"><img src="../images/add1.gif" width="48" height="56" align="absmiddle" border="0"/>Manage Triggers</a></li>
		                          	<li><a href="proof.php" class="blueLink" onclick="wopen('proof.php', 'upload_proof', 600, 450); return false;"><img src="../images/upload.gif" width="48" height="56" align="absmiddle" border="0"/>Upload Proof File</a></li>
		                          </ul>
		                          </td>
		                          <td width="9" background="../images/box_ab_r2_c5.jpg">&nbsp;</td>
		                        </tr>
		                    </table></td>
		                  </tr>
		                  <tr>
		                    <td><table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
		                        <tr>
		                          <td width="16" height="19"><img name="box_ab_r3_c1" src="../images/box_ab_r3_c1.jpg" width="16" height="19" border="0" id="box_ab_r3_c1" alt="" /></td>
		                          <td background="../images/box_ab_r3_c3.jpg">&nbsp;</td>
		                          <td width="17" height="19"><img name="box_ab_r3_c4" src="../images/box_ab_r3_c4.jpg" width="17" height="19" border="0" id="box_ab_r3_c4" alt="" /></td>
		                        </tr>
		                    </table></td>
		                  </tr>
		                </table>
	  				</td>
         		</tr>
         	</table>
    		<?php include "../footer.php" ?>
		</td>
	</tr>
	<tr>
  		<td>
  			<table width="100%"  border="0" cellspacing="0" cellpadding="0">
    			<tr>
      				<td class="darkBlueOutline">&nbsp;<br>&nbsp;</td>
    			</tr>
  			</table>
  		</td>
	</tr>
</table>