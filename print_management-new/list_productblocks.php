<?php
	require("../../printable/include/mysql.inc.php");
	require("../../printable/include/printable.inc.php");
	require("../globals.php");
	
	$sortCols = array("<!-- Image -->&nbsp;" => "nosort", "Block Name" => "BlockName", "Print File Updated" => "FileNameUpdated DESC", 
							"Email File Updated" => "EmailFileNameUpdated DESC", "<!-- Edit -->&nbsp;" => "nosort");
	
	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	
	$portal = new PrintablePortal($COMPANY_ID, $db);
		
	$currentUser = $portal->GetUser($_SESSION['currentuserid']);
	
	if(array_search($_GET['sortby'], $sortCols) === false)
	{
		$_GET['sortby'] = "BlockName DESC";
	}
		
	if(strlen($_GET['search']) <= 0)
	{
		$whereString = "1";
	}
	else
	{
		$whereString = "BlockName LIKE '%" . mysql_real_escape_string(stripslashes($_GET['search'])) . "%' OR FileName LIKE '%" . mysql_real_escape_string(stripslashes($_GET['search'])) . "%'";
	}
	
	// Check login
	if(!$portal->CheckPriv($currentUser->UserID, 'subadmin'))
	{
		header( "Location: " . $portal->CurrentCompany->Website . "login.php?message=" . urlencode( "Not logged in or login error.  Please try again." ) );
		die();
	}
	
	$blocks = $portal->GetProductBlocks(false, $whereString, $_GET['sortby']);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
	<title>
		<?= $portal->CurrentCompany->CompanyName ?> :: Available Product Blocks
	</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />		
	<link type="text/css" media="all" rel="stylesheet" href="../style.css" />		
	<link type="text/css" media="all" rel="stylesheet" href="style.css" />	
	<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />
<script>
	function LoadChoice(id, name)
	{
		window.opener.ChooseBlock(id, name);
	}
</script>	
</head>
<body bgcolor="#FFFFFF">
<form method="get" action="<?= $_SERVER['PHP_SELF'] ?>">
<div style="text-align: right;">
<?php
	$getTemp = $_GET;
	unset($getTemp['search']);
	foreach($getTemp as $k=>$v)
	{
?>
	<input type='hidden' name='<?= $k ?>' value='<?= $v ?>' />
<?php 
	}
?>
	Search: <input type="text" name="search" value="<?= $_GET['search'] ?>" />
	<input type="submit" name="Submit" value="Go" /> 
</div>
</form>
<table width="100%">
	<tr class="tableHeader">
<?php
	foreach($sortCols as $k=>$v)
	{
		if($v != "nosort" && $_GET['sortby'] != $v)
		{
			$getTemp = $_GET;
			$getTemp['sortby'] = $v;
			$getString = build_get_query($getTemp);
?>
			<th>
				<a href="<?= $_SERVER['PHP_SELF'] . "?" . $getString ?>" class="whiteLink">
					<span class="whitetext"><?= $k ?></span>
				</a>
			</th>
<?php		
		}
		else
		{
?>
			<th><?= $k ?></th>
<?php
		}
	}
?>
	</tr>
<?php
	foreach($blocks as $b)
	{
?>
	<tr style="height: 50px;" class="tableDetail">
		<td align="center">
			<img src="<?= $b->ThumbnailPath; ?>&thumb=true" />
		</td>
		<td align="left">
			<?= $b->BlockName; ?>
		</td>
		<td align="left">
			<?= strtotime($b->FileNameUpdated) ? date("m/d/Y<b\\r/>g:i:s a", strtotime($b->FileNameUpdated)) : "" ?>
		</td>
		<td align="left">
			<?= strtotime($b->EmailFileNameUpdated) ? date("m/d/Y<b\\r/>g:i:s a", strtotime($b->EmailFileNameUpdated)) : "" ?>
		</td>
		<td>
			<a href="#" onclick="LoadChoice(<?= $b->ProductBlockID ?>, '<?= $b->BlockName ?>'); window.close();">choose</a>
		</td>
	</tr>
<?php
	}
?>
</table>
</body>
</html>