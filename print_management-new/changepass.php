<?php
	require("../../printable/include/mysql.inc.php");
	require("../../printable/include/printable.inc.php");
	require("../globals.php");
	
	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	
	$portal = new PrintablePortal($COMPANY_ID, $db);
	$portal->ApprovalEmails = $APPROVAL_EMAIL;
		
	$currentUser = $portal->GetUser($_SESSION['currentuserid']);
		
	// Check login
	if(!$currentUser->UserID)
	{
		header( "Location: " . $portal->CurrentCompany->Website . "login.php?message=" . urlencode( "Not logged in or login error.  Please try again." ) );
		die();
	}
	
	if( isset($HTTP_POST_VARS['Submit']))
	{
			
		// Check if new password matches confirm
		if( $HTTP_POST_VARS['newpass'] == $HTTP_POST_VARS['confirmpass'] )
		{
			// check to see if old pass is correct				
			if( $portal->CheckPass($currentUser->Username, addslashes($HTTP_POST_VARS['oldpass'])) ) 
			{
				// set new password
				$portal->ChangePass($currentUser->UserID, $HTTP_POST_VARS['newpass']);	
				
				// redirect user to listing
				$message = "Password has been changed.";
				header("Location: listing.php?message=" . urlencode($message));
			}
			else
			{
				$HTTP_GET_VARS['message'] = "Old password is incorrect.";
			}
			
			
		}
		else
		{
			$HTTP_GET_VARS['message'] = "New password does not match.";
		}
		
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
	<title>
		<?= $portal->CurrentCompany->CompanyName ?> :: Change Password
	</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />		
	<link type="text/css" media="all" rel="stylesheet" href="style.css" />
	<link type="text/css" media="all" rel="stylesheet" href="../style.css" />
	<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />
</head>
<body bgcolor="#FFFFFF">
<?php
	include("header.php");
?>
<table cellspacing="0" cellpadding="0" width="770" align="center" bgcolor="#FFFFFF">
	<tr>
  		<td>
			<table border="0" cellPadding="0" cellSpacing="0" align="center" width="100%">
				<tr>
					<td>
					<?php
						$CURRENTPAGE = 'listing';
						include("navbar.php") 
					?>
					</td>
				</tr>
			</table>
			<table width="730">
				<tr align="right">
					<td>
						<a href="login.php?logout=true" class="blueLink">Log Out</a> - <a href="changepass.php" class="blueLink">Change Password</a>
					</td>
				</tr>
			</table> 
    		&nbsp;
			<table cellpadding="5" width="600">
				<tr>
	  				<td align="center">
<?php
	if( isset( $HTTP_GET_VARS['message'] ) )
	{
?>
						<div class="warningText"><?= $HTTP_GET_VARS['message'] ?><br>&nbsp;<br></div>
<?php
	}
	if( isset( $HTTP_GET_VARS['firstlogin'] ) )
	{
?>
						<div class="headerTextLarge">
							Welcome <?= $currentUser->FirstName ?> Since this is your first visit please<br>
							Change Your Password
						</div>
<?php
	}
	else 
	{
?>
            			<div class="headerTextLarge">Set new password: </div>
            			<div class="headerText">Please enter your current and new password below.</div>
<?php
	}
?>
            			<form name="form1" method="post" action="changepass.php?<?= $_SERVER['QUERY_STRING'] ?>">
              			<table width="400" cellpadding="4" cellspacing="3" border="1">
<?php
	if($HTTP_GET_VARS['firstlogin'] == 'true')
	{
?>
							<input type="hidden" value="<?= $STORE_PW ?>" name="oldpass">
<?php
	}
	else 
	{
?>
			                <tr>
			                  <td class="tableHeader"><p><strong>Old Password:</strong></p>
			                  </td>
			                  <td class="tableDetail"><input name="oldpass" type="password" id="oldpass" size="40"></td>
			                </tr>
<?php
	}
?>
			                <tr>
			                  <td class="tableHeader"><p><strong>New Password:</strong></p>
			                  </td>
			                  <td class="tableDetail"><input name="newpass" type="password" id="newpass" size="40"></td>
			                </tr>
			                <tr>
			                  <td class="tableHeader"><p><strong>Confirm Password:</strong></p>
			                  </td>
			                  <td class="tableDetail"><input name="confirmpass" type="password" id="confirmpass" size="40"></td>
			                </tr>
			                <tr align="center">
			                  <td colspan="2" ><input type="submit" name="Submit" value="Change"></td>
			                </tr>
              			</table>
            			</form>
	  				</td>
         		</tr>
         	</table>
		</td>
	</tr>
</table>
</body>
</html>