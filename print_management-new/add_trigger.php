<?php
	require("../../printable/include/mysql.inc.php");
	require("../../printable/include/printable.inc.php");
	require("../globals.php");
	
	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	
	$portal = new PrintablePortal($COMPANY_ID, $db);
		
	$currentUser = $portal->GetUser($_SESSION['currentuserid']);
		
	// Check login
	if(!$portal->CheckPriv($currentUser->UserID, 'subadmin'))
	{
		header( "Location: " . $portal->CurrentCompany->Website . "login.php?message=" . urlencode( "Not logged in or login error.  Please try again." ) );
		die();
	}
	
	if($_POST['Submit'] == 'Cancel')
	{
		header("Location: manage_trigger.php?message=". urlencode("Action Canceled. Trigger not Added."));
		die();
	}
	elseif($_POST['Submit'] == 'Add')
	{
		$trigger = $portal->GetTrigger($portal->AddTrigger($_POST['TriggerName']));
		
  		$trigger->TriggerType = $_POST['TriggerType'];
  		
  		$portal->UpdateTrigger($trigger);
		
		header("Location: edit_trigger.php?id={$trigger->TriggerID}&message=". urlencode("Action Completed. Trigger Added."));
		die();
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
	<title>
		<?= $portal->CurrentCompany->CompanyName ?> :: Add Trigger
	</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />		
	<link type="text/css" media="all" rel="stylesheet" href="../style.css" />		
	<link type="text/css" media="all" rel="stylesheet" href="style.css" />	
<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />
</head>
<body bgcolor="#FFFFFF">
<?php
	include("header.php");
	include("navbar.php");
?>
<table cellspacing="0" cellpadding="0" width="770" align="center" bgcolor="#FFFFFF">
	<tr>
  		<td>
			<table border="0" cellpadding="0" cellspacing="0" align="center" width="100%">
				<tr>
					<td>
					<?php
						$CURRENTPAGE = 'triggermanage';
						
					?>
					</td>
				</tr>
			</table>
			<table cellpadding="0" width="700">
				<tr>
	  				<td>
	  					<form method="post" action="<?= $_SERVER['PHP_SELF'] ?>" enctype="multipart/form-data">
	  					<div align="center" class="headerTextLarge">Add Print Trigger</div>
	  					<div align="center" class="headerTextLarge"><?= $_GET['message'] ?></div>
			  			<div class="fieldSection">
			  				<div class="itemSection">
			  					<label>Trigger Name:</label><br/>
			  					<input type="text" name="TriggerName" value=""/>
			  				</div>
			  				<div class="itemSection">
			  					<label>Trigger Type:</label><br/>
			  					<select name="TriggerType">
			  						<option value="Manual">Manual</option>
			  						<option value="Date">Date</option>
			  						<option value="Field">Field Change</option>
			  					</select>
			  				</div>
			  				<div class="itemSection">
			  					<center>
			  						<input type="submit" value="Add" name="Submit"/><input type="submit" value="Cancel" name="Submit"/>
			  					</center>
			  				</div>
			  			</div>
			  			</form>
	  				</td>
         		</tr>
         	</table>
		</td>
	</tr>
</table>
</body>
</html>