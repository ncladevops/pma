<?php
	require("../../printable/include/mysql.inc.php");
	require("../../printable/include/printable.inc.php");
	require("../globals.php");
	
	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	
	$portal = new PrintablePortal($COMPANY_ID, $db);
		
	$currentUser = $portal->GetUser($_SESSION['currentuserid']);
	$isAdmin = $portal->CheckPriv($currentUser->UserID, 'admin');
	// Check login
	if(!$portal->CheckPriv($currentUser->UserID, 'subadmin'))
	{
		header( "Location: " . $portal->CurrentCompany->Website . "login.php?message=" . urlencode( "Not logged in or login error.  Please try again." ) );
		die();
	}
	
	$products = $portal->GetProducts();
	$ts = $portal->GetProductTemplates();
	
	$templates = array();
	foreach($ts as $t)
	{
		$templates[$t->ProductTemplateID] = $t;
	}		
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
	<title>
		<?= $portal->CurrentCompany->CompanyName ?> :: Manange Product
	</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />		
	<link type="text/css" media="all" rel="stylesheet" href="../style.css" />		
	<link type="text/css" media="all" rel="stylesheet" href="style.css" />		
	<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />
</head>
<body bgcolor="#FFFFFF">
<?php 
	$CURRENTPAGE = 'productmanage';
	include("header.php");
	include("navbar.php");
		?>
<table cellspacing="0" cellpadding="0" width="100%">
	<tr>
  		<td>
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td>
					<?php
						
					?>
					</td>
				</tr>
			</table>
			<div width="100%">
			<br/>
			<table cellpadding="0" border="0" align="center">
				<tr>
	  				<td>
	  					<div align="center" class="headerTextLarge">Manage Print Products</div>
	  					<div align="center" class="headerTextLarge"><?= $HTTP_GET_VARS['message'] ?></div>
			  			<table width="100%">
			  				<tr class="tableHeader">
			  					<td>
			  						Product Name
			  					</td>
			  					<td>
			  						Template Name
			  					</td>
			  					<td>
			  						Sections
			  					</td>
			  					<td>
			  						Preview
			  					</td>
			  					<td>
			  						&nbsp;
			  					</td>
								<?php
								if ($isAdmin)
								{ ?>
			  					<td>
			  						&nbsp;
			  					</td>
			  					<td>
			  						&nbsp;
			  					</td>
								<?php
								}
								?>
			  				</tr>
<?php
	$i = 0;
	foreach($products as $product)
	{
		if($product->GroupID == 0)
		{
			$sections = $templates[$product->PrintableProductID]->VariableFieldCount;
			$filledSections = $portal->GetFilledProductBlockCount($product->ProductID);
			$sectionsNeeded = $sections - $filledSections;
			$disableLabel = $product->Disabled ? "enable" : "disable";
?>
							<tr class="<?= $i % 2 == 0 ? "tableDetailLight" : "tableDetail"?>" style="height: 60px;">
								<td><?= $product->ProductName ?></td>
								<td><?= $templates[$product->PrintableProductID]->TemplateName . ($product->PrintableProductID == 0 ? " (ID: {$product->FrontFileName})" : "")?></td>
								<td><?= $sections . ($sectionsNeeded > 0 ? " ($sectionsNeeded are blank)" : "")?></td>
								<td align="center">
									<a href="<?= $product->ThumbnailPath; ?>" target="_blank">
										<img src="<?= $product->ThumbnailPath; ?>&thumb=true" />
									</a>
								</td>
								<td><a href="edit_product.php?id=<?= $product->ProductID ?>"><?= $isAdmin ? "edit" : "view" ?></a></td>
								<?php
								if ($isAdmin)
								{ ?>
								<td>
									<a href="edit_product.php?id=<?= $product->ProductID ?>&action=<?= $disableLabel ?>" 
										onclick="return confirm('Are you sure you want to <?= $disableLabel ?> this product')">
										<?= $disableLabel ?>
									</a>
								</td>
								<td>
									<a href="edit_product.php?id=<?= $product->ProductID ?>&action=delete" 
										onclick="return confirm('Are you sure you want to delete this product')">
										delete
									</a>
								</td>
								<?php
								}
								?>
							</tr>
<?php
			$i++;
		}
	}
?>
			  			</table>
						<?php
								if ($isAdmin)
								{ ?>
			  			<div style="padding-top: 5px;">
			  				<a href="add_product.php">Add Product</a> | <a href="clone_product.php">Clone Product</a>
			  			</div>
						<?php
								}
								?>
	  				</td>
         		</tr>
         	</table>
			</div>
		</td>
	</tr>
</table>
</body>
</html>