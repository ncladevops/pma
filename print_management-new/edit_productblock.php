<?php
	require("../../printable/include/mysql.inc.php");
	require("../../printable/include/printable.inc.php");
	require("../../printable/include/contactfactory.inc.php");
	require("../globals.php");
	
	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	
	$portal = new PrintablePortal($COMPANY_ID, $db);
	$factory = new ContactFactory($db);
		
	$currentUser = $portal->GetUser($_SESSION['currentuserid']);
	$isAdmin = $portal->CheckPriv($currentUser->UserID, 'admin');
		
	// Check login
	if(!$portal->CheckPriv($currentUser->UserID, 'subadmin'))
	{
		header( "Location: " . $portal->CurrentCompany->Website . "login.php?message=" . urlencode( "Not logged in or login error.  Please try again." ) );
		die();
	}
	
	$blockMaps = $portal->GetProductBlockMaps($_GET['productid'], $_GET['variable']);
	
	$product = $portal->GetProduct($_GET['productid']);
	$bmInfo = $portal->GetProductBlockMappingInfo($product->ProductID, $_GET['variable']);
	
	if(!$product)
	{
		header("Location: index.php?message=" . urlencode("Invalid Product ID"));
		die();
	}
	
	if($_POST['Submit'] == 'Cancel')
	{
		header("Location: edit_product.php?id={$product->ProductID}&message=". urlencode("Action Canceled. Product not updated."));
		die();
	}
	elseif($_POST['Submit'] == 'Update')
	{
		$bmInfo->MappingType = $_POST['MappingType'];
		$bmInfo->MaxLength = $_POST['MaxLength'];
		$bmInfo->ContactField = $_POST['ContactField'];
		$bmInfo->DefaultValue = $_POST['DefaultValue'];
		$bmInfo->Required = ($_POST['Required'] == 1 ? true : false);
		
		if($bmInfo->MappingType == 'dropdown') {
			$bmInfo->Options = explode("\n", $_POST['DropdownOptions']);
		}
		else {
			$bmInfo->Options = array();
		}
				
		$portal->UpdateProductBlockMappingInfo($bmInfo);
		
		header("Location: edit_product.php?id={$product->ProductID}&message=". urlencode("Action Completed. Product updated."));
		die();
	}
	$contactFields = $factory->GetContactFields($portal->CurrentCompany->CompanyID);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
	<title>
		<?= $portal->CurrentCompany->CompanyName ?> :: Edit Product Block
	</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />		
	<link type="text/css" media="all" rel="stylesheet" href="../style.css" />		
	<link type="text/css" media="all" rel="stylesheet" href="style.css" />	
<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />
<script>
	function toggleOptions(val) {
		if(val == 'string' || val == 'date' || val == 'time' || val == 'logo' || val == 'dropdown' || val == 'daterange') {
			document.getElementById('optionTypeSection').style.display = "none";
			document.getElementById('nonoptionTypeSection').style.display = "";
		}
		else {
			document.getElementById('optionTypeSection').style.display = "";
			document.getElementById('nonoptionTypeSection').style.display = "none";
		}

		if(val == 'dropdown') {
			document.getElementById('dropdownoptionsdiv').style.display = "";	
		}
		else {
			document.getElementById('dropdownoptionsdiv').style.display = "none";
		}
		
	}
	function initPage()
	{
		<?= $isAdmin ? "" : "DisableInput();" ?>
	}

	function DisableInput()
	{
		var inputs=document.getElementsByTagName('input');
	
		for(i=0; i < inputs.length; i++)
		{
			inputs[i].disabled = true;
		}

		var selects=document.getElementsByTagName('select');
		
		for(i=0; i < selects.length; i++)
		{
			selects[i].disabled = true;
		}
		
		var areas=document.getElementsByTagName('textarea');
		
		for(i=0; i < areas.length; i++)
		{
			areas[i].disabled = true;
		}
	}
</script>	
</head>
<body bgcolor="#FFFFFF" onload='initPage();'>
<?php
		include("header.php");
		include("navbar.php");
?>
<table cellspacing="0" cellpadding="0" width="770" align="center" bgcolor="#FFFFFF">
	<tr>
  		<td>
			<table border="0" cellpadding="0" cellspacing="0" align="center" width="100%">
				<tr>
					<td>
					<?php
						$CURRENTPAGE = 'productmanage';
					?>
					</td>
				</tr>
			</table> 
			<table cellpadding="0" width="700">
				<tr>
	  				<td>
	  					<form method="post" action="<?= $_SERVER['PHP_SELF'] ?>?productid=<?= $product->ProductID ?>&variable=<?= $_GET['variable'] ?>" enctype="multipart/form-data">
	  					<div align="center" class="headerTextLarge">Manage Print Product Variable Block Mapping</div>
	  					<div align="center" class="headerTextLarge"><?= $HTTP_GET_VARS['message'] ?></div>
			  			<div class="fieldSection">
			  				<div class="itemSection">
			  					<label>Product Name:</label><br/>
			  					<?= $product->ProductName; ?>
			  				</div>
			  				<div class="itemSection">
			  					<label>Variable Section:</label><br/>
			  					<?= intval($_GET['variable']) + 1 ?>
			  				</div>
			  				<div class="itemSection">
			  					<label>Mapping Type:</label><br/>
			  					<select name="MappingType" onchange="toggleOptions(this.value)">
			  						<option value='string' <?= $bmInfo->MappingType == 'string' ? 'SELECTED' : '' ?>>String</option>
			  						<option value='option' <?= $bmInfo->MappingType == 'option' || $bmInfo->MappingType == 'default' ? 'SELECTED' : '' ?>>Option</option>
			  					</select>
			  				</div>
			  				<div class="itemSection" id="nonoptionTypeSection" style="<?= $bmInfo->MappingType == 'option' || $bmInfo->MappingType == 'default' ? 'display: none;' : ''?>">
			  					<label>Max Length</label><br/>
			  					<input type="text" name="MaxLength" value="<?= $bmInfo->MaxLength; ?>" style="width: 85px;" /><br/>
			  					<label>Mapped Field</label><br/>
			  					<select name="ContactField">
			  						<option value="">- Please Choose -</option>
<?php
	foreach($contactFields as $k)
	{
?>
									<option value="<?= $k?>" <?= $k == $bmInfo->ContactField ? "SELECTED" : "" ?>><?= $k ?></option>									
<?php
	}
?>
			  					</select><br/>
			  					<label>Default Value</label><br/>
			  					<input type="text" name="DefaultValue" value="<?= $bmInfo->DefaultValue; ?>" style="width: 185px;" /><br/>
			  					<label>Field Required?</label><br/>
			  					<input type="checkbox" name="Required" value="1" <?= $bmInfo->Required ? "CHECKED" : "" ?> /><br/>
			  					<div id="dropdownoptionsdiv" style="<?= $bmInfo->MappingType == 'dropdown' ? '' : 'display: none;'?>">
				  					<label>Options</label><br/>
				  					<textarea name="DropdownOptions" style="width: 325px; height: 125px;"><?= implode("\n", $bmInfo->Options) ?></textarea><br/>
				  				</div>
								<?php
							if ($isAdmin){
							?>
			  					<center>
			  						<input type="submit" value="Update" name="Submit"/><input type="submit" value="Cancel" name="Submit"/>
			  					</center>
									<?php
							}
							?>
			  				</div>
			  				<div class="itemSection" id="optionTypeSection" style="<?= $bmInfo->MappingType == 'option' || $bmInfo->MappingType == 'default' ? '' : 'display: none;'?>">
			  					<label>Block Mapping Rules:</label><br/>
			  					<table width="100%">
			  						<tr class="tableHeader">
			  							<td>&nbsp;</td>
			  							<td>Rule #</td>
			  							<td>Criteria</td>
			  							<td>Block</td>
			  							<td>&nbsp;</td>
			  							<td>&nbsp;</td>
			  						</tr>
<?php
	$i = 0;
	foreach($blockMaps as $bm)
	{
?>
									<tr class="<?= $i % 2 == 0 ? "tableDetailLight" : "tableDetail"?>" style="height: 60px;">
										<td align="center">
											<a href="edit_productblockmap.php?pbmid=<?= $bm->ProductBlockMapID?>&action=moveup"><img src="../images/sort_up_<?= $i % 2 == 0 ? "light" : "dark"?>.jpg" border=0 /></a> 
											<a href="edit_productblockmap.php?pbmid=<?= $bm->ProductBlockMapID?>&action=movedown"><img src="../images/sort_down_<?= $i % 2 == 0 ? "light" : "dark"?>.jpg" border=0 /></a>
										</td>
										<td>
											<?= $bm->SortOrder + 1 ?>
										</td>
										<td>
<?php
		if(sizeof($bm->Criteria) == 0)
			echo "DEFAULT";
		foreach($bm->Criteria as $crit)
		{
			
?>
											<?= $crit['LeftValue'] . " " . $crit['CompareValue'] . " " . $crit['RightValue']?><br/>
<?php
		}
?>
										</td>
										<td>
											<?= $bm->BlockName ?> 
											<a href="<?= $bm->BlockThumbnailPath; ?>" target="_blank">
												<img src="<?= $bm->BlockThumbnailPath; ?>&thumb=true"  style="vertical-align: middle;" />
											</a>
										</td>
										<td><a href="edit_productblockmap.php?pbmid=<?= $bm->ProductBlockMapID?>">edit</a></td>
										<td>
											<a href="edit_productblockmap.php?pbmid=<?= $bm->ProductBlockMapID?>&action=delete"
												onclick="return confirm('Are you sure you want to delete this rule?')">
												delete
											</a>
										</td>
									</tr>
<?php
		$i++;
	}
?>
			  					</table>
			  					<div style="padding-top: 10px;">
			  						<a href="edit_product.php?id=<?= $product->ProductID ?>">Back to Manage Product</a>
<?php
if ($isAdmin) {
?>									| <a href="add_productblockmap.php?productid=<?= $product->ProductID ?>&variable=<?= intval($_GET['variable']) ?>">Add New Rule</a> 
<?php } ?>
			  					</div>
			  				</div>
			  			</div>
						</form>
	  				</td>
         		</tr>
         	</table>
		</td>
	</tr>
</table>
</body>
</html>