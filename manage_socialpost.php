<?php
require("../printable/include/mysql.inc.php");
require("../printable/include/optimize.printable.inc.php");
require("globals.php");

$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);

$portal = new OptimizePortal($COMPANY_ID, $db);

$currentUser = $portal->UserAccess($_SESSION['currentuserid']);
$isSubAdmin = $portal->CheckPriv($currentUser->UserID, 'subadmin');

// Check login
if (!$isSubAdmin) {
    header("Location: " . $portal->CurrentCompany->Website . "login.php?message=" . urlencode("Not logged in or login error.  Please try again."));
    die();
}

if (!isset($_GET['section'])) {
    header("Location: controlpanel.php");
}

// Get list of postings
$company_postings = $portal->GetPostings($portal->CurrentCompany->CompanyID, $_GET['section']);

$gs = $portal->GetCompanyGroups();

$groups = array();

$groups[0] = new Group();
$groups[0]->GroupID = 0;
$groups[0]->GroupName = "All";

foreach ($gs as $g) {
    $groups[$g->GroupID] = $g;
}

// Get Cats
$rootCats = $portal->GetSocialCategories('root', $_GET['section'], true, $currentUser->GroupID);

$cats = array();
$cat = new SocialCategory();
foreach ($rootCats as $cat) {
    $cats[] = $portal->GetSocialCategory($cat->SocialCategoryID, true);
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <title>
            Optimize on Demand :: Coming Soon!
        </title>
        <meta http-equiv="content-type" content="text/html;charset=utf-8" />
        <script type='text/javascript' src='../printable/include/js/jquery-1.10.2.min.js'></script>		
        <script language="JavaScript" type="text/JavaScript">
            $(document).ready(function() {

            $('#tableloading').html('<td colspan="5" align="center"><div style="text-align: center;"><img src="images/loading.gif" /><br/>Loading ...</div></td>');

            // Handler for .ready() called.
            var search = $('#search').val();
            var socialcategoryid = $('#socialcategoryid').val();

            $.ajax({
            url: 'printable_trans.php',
            data: { 'section': <?= $_GET['section'] ?>  , 'action' : 'GetEditPostingTable' , 'socialcategoryid' : socialcategoryid , 'search' : search },
            dataType: 'html',
            success: function(data) {
            $('#tablelist').html(data);
            $('#tableloading').html('');
            }
            });


            $('#search').keyup(function(e) {

            $('#tableloading').html('<td colspan="5" align="center"><div style="text-align: center;"><img src="images/loading.gif" /><br/>Loading ...</div></td>');

            search = $('#search').val();
            socialcategoryid = $('#socialcategoryid').val();

            $.ajax({
            url: 'printable_trans.php',
            data: { 'section': <?= $_GET['section'] ?> , 'action' : 'GetEditPostingTable' , 'socialcategoryid' : socialcategoryid , 'search' : search },
            dataType: 'html',
            success: function(data) {
            $('#tablelist').html(data);
            $('#tableloading').html('');
            }
            });
            });

            $('#socialcategoryid').change(function(){

            $('#tableloading').html('<td colspan="5" align="center"><div style="text-align: center;"><img src="images/loading.gif" /><br/>Loading ...</div></td>');				

            search = $('#search').val();
            socialcategoryid = $('#socialcategoryid').val();			

            $.ajax({
            url: 'printable_trans.php',
            data: { 'section': <?= $_GET['section'] ?> , 'action' : 'GetEditPostingTable' , 'socialcategoryid' : socialcategoryid , 'search' : search },
            dataType: 'html',
            success: function(data) {
            $('#tablelist').html(data);
            $('#tableloading').html('');
            }
            });

            });

            });

        </script>

        <?php include("components/bootstrap.php") ?>

    </head>
    <body bgcolor="#FFFFFF">
        <div id="page">
            <?php include("components/header.php") ?>
            <div id="body">
                <?php
                $CURRENT_PAGE = "Home";
                include("components/navbar.php");
                ?>
                <?php if (isset($_GET['message'])): ?>
                    <div class="container">
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <?= $_GET['message']; ?>
                        </div>
                    </div>
                <?php endif; ?> 

                <div id="controlPanelContainer" class="row">
                    <div id="folderTreeContainer" class="col-md-offset-2 col-md-2 panel panel-default">
                        <?php include("components/controlpanel_tree.php"); ?>
                    </div>

                    <div class="sectionHeader col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Control Panel - Edit Files</h3>
                            </div>
                            <div class="panel-body">
                                <div id="detailContainer">
                                    <div id="actionBar" class="row">
                                        <input type="search" id="search" class="actionButton input-sm form-submenu" name="search" placeholder="Search"></input>

                                        <select id="socialcategoryid" name="socialcategoryid" class="input-sm form-submenu">
                                            <option value="all">Select Category</option>
                                            <option value="all">-- All --</option>
                                            <option value="">-- Uncategorized --</option>
                                            <?php
                                            $fc = new SocialCategory();
                                            foreach ($cats as $fc) {
                                                echo $fc->printSelect();
                                            }
                                            ?>
                                        </select>
                                        <a class="actionButton btn btn-default btn-sm" href="add_socialposting.php?section=<?= $_GET['section'] ?>">Add Posting</a>
                                    </div>
                                    <div class="table-responsive row">
                                        <table width="100%" class="file_table table table-striped table-hover table-bordered">
                                            <thead>
                                                <tr class=" file_table rowheader">
                                                    <td>Name</td>
                                                    <td>Category</td>
                                                    <td>Group</td>
                                                    <td>Date Created</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                            </thead>
                                            <tbody id="tablelist">
                                            </tbody>
                                            <tfoot id="tableloading">
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div>&nbsp;</div>
                            <strong>Click on <span class="Font14">Create New</span> to add a Posting to the System</strong></em></span>&nbsp;&nbsp;&nbsp;&nbsp; <input type="button" name="Upload" value="Create New" class="btn btn-default btn-sm" onclick="parent.location = 'add_socialposting.php?section=<?= $_GET['section'] ?>'" />
                        </div>
                    </div>
                </div>
            </div>
            <?php include("components/footer.php") ?>
    </body>
</html>