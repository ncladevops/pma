<?php
	require("../printable/include/mysql.inc.php");
	require("../printable/include/optimize.printable.inc.php");
	require("globals.php");
	require '../printable/include/component/facebook/facebook.php';
	require '../printable/include/component/twitter/codebird.php';
	require '../printable/include/component/linkedin/linkedin.php';
	
	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	
	$portal = new OptimizePortal($COMPANY_ID, $db);
	
	$currentUser = $portal->UserAccess($_SESSION['currentuserid']);
	
	$isSubAdmin = $portal->CheckPriv($currentUser->UserID, 'subadmin');
		
	if(!$isSubAdmin || !isset($_GET['id']))
	{
		$_GET['id'] = $currentUser->UserID;
	}
	
	$user = $portal->GetUser($_GET['id']);
		
	if(!$user)
	{
		header("Location: manage_users.php?message=" . urlencode("Invalid User ID"));
		die();
	}
	
	if($_POST['Submit'] == 'Update')
	{
		if($_POST['NewPassword1'] == $_POST['NewPassword2'] 
			&& strlen($_POST['NewPassword1']) > 0)
		{
			$user->Password = $_POST['NewPassword1'];
		}
		
		if($_POST['NewPassword1'] != $_POST['NewPassword2'] 
			&& strlen($_POST['NewPassword1']) > 0)
		{
			$_GET['message'] = "Passwords do not match";
		}
		else
		{
			foreach($_POST as $k=>$v)
			{
				$user->$k = $v;
			}
			
			$portal->UpdateUser($user);
			
			if($isSubAdmin)
			{
				header('Location: manage_users.php?message=' . urlencode("User updated successfully."));
				die();
			}
			else
			{
				header('Location: home.php?message=' . urlencode("Your profile has been updated successfully."));
				die();				
			}
		}
		
	}
	elseif($_POST['Submit'] == 'Cancel')
	{
		if($isSubAdmin)
		{
			header('Location: manage_users.php?message=' . urlencode("Action Canceled. User not updated."));
			die();
		}
		else
		{
			header('Location: home.php?message=' . urlencode("Action Canceled. Profile not updated."));
			die();				
		}
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
	<title>
		<?= $portal->CurrentCompany->CompanyName ?> :: User Profile Information
	</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />		
	<link type="text/css" media="all" rel="stylesheet" href="style.css" />
	<link type="text/css" media="all" rel="stylesheet" href="css/edit_user.css" />
	<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />
	<script  src="js/func.js"></script>
<script language="JavaScript" type="text/JavaScript">
	function initPage()
	{
		<?= $isSubAdmin || true ? "" : "DisableInput();" ?>
	}

	function DisableInput()
	{
		var inputs=document.getElementsByTagName('input');
	
		for(i=0; i < inputs.length; i++)
		{
			inputs[i].disabled = true;
		}

		var selects=document.getElementsByTagName('select');
		
		for(i=0; i < selects.length; i++)
		{
			selects[i].disabled = true;
		}
	}

</script>
</head>
<body bgcolor="#FFFFFF" onload="initPage()">
<div id="page">
<?php include("components/header.php") ?>
<div id="body">
	<?php 
		$CURRENT_PAGE = "Home";
		include("components/navbar.php"); 
	?>
<form method="post" action="<?= $_SERVER['PHP_SELF'] ?>?id=<?= $user->UserID; ?>">
	<div class="error"><?= $_GET['message']; ?></div>
	<div id="UserInfoDiv">
		<div class="sectionHeader">
			User Info
		</div>
		<div class="sectionDiv">
			<div class="itemSection">
				<div id="UsernameDiv">
					<label for="Username">Username:</label><br/>
					<?= $user->Username ?>
				</div>
			</div>
			<div class="itemSection">
				<div id="FirstNameDiv">
					<label for="FirstName">First Name:</label><br/>
					<input type="text" id="FirstName" name="FirstName" style="width: 130px" value="<?= $user->FirstName; ?>"/>
				</div>
				<div id="LastNameDiv">
					<label for="LastName">Last Name:</label><br/>
					<input type="text" id="LastName" name="LastName" style="width: 130px" value="<?= $user->LastName; ?>"/>
				</div>
				<div id="TitleDiv">
					<label for="Title">Title:</label>
					<input type="text" id="Title" name="Title" style="width: 145px" value="<?= $user->Title ?>" />
				</div>
			</div>
			<div class="itemSection">
				<div id="AddressDiv">
					<label for="Address">Street Address:</label><br/>
					<input type="text" id="Address" name="Address" style="width: 415px" value="<?= $user->Address; ?>"/>
				</div>
			</div>
			<div class="itemSection">
				<div id="CityDiv">
					<label for="City">City:</label><br/>
					<input type="text" id="City" name="City" style="width: 250px" value="<?= $user->City; ?>"/>
				</div>
				<div id="StateDiv">
					<label for="State">ST:</label><br/>
					<input type="text" id="State" name="State" style="width: 35px" value="<?= $user->State; ?>"/>
				</div>
				<div id="ZipcodeDiv">
					<label for="Zipcode">Zip:</label><br/>
					<input type="text" id="Zipcode" name="ZiZipcodep" style="width: 100px" value="<?= $user->Zipcode; ?>"/>
				</div>
			</div>
			<div class="itemSection">
				<div id="ContactPhoneDiv">
					<label for="ContactPhone">Phone:</label><br/>
					<input type="text" id="ContactPhone" name="ContactPhone" style="width: 120px" value="<?= $user->ContactPhone; ?>"/>
				</div>
				<div id="ContactCellDiv">
					<label for="ContactCell">Cell Phone:</label><br/>
					<input type="text" id="ContactCell" name="ContactCell" style="width: 120px" value="<?= $user->ContactCell; ?>"/>
				</div>
				<div id="ContactFaxDiv">
					<label for="ContactFax">Fax:</label><br/>
					<input type="text" id="ContactFax" name="ContactFax" style="width: 120px" value="<?= $user->ContactFax; ?>"/>
				</div>
			</div>
<?php
	//if($isSubAdmin)
	if(true)
	{
?>
			<div class="itemSection">
				<div id="NewPassword1Div">
					<label for="NewPassword1">New Password:</label><br/>
					<input type="password" id="NewPassword1" name="NewPassword1" style="width: 120px" value=""/>
				</div>
			</div>
			<div class="itemSection">
				<div id="NewPassword2Div">
					<label for="NewPassword2">Confirm Password:</label><br/>
					<input type="password" id="NewPassword2" name="NewPassword2" style="width: 120px" value=""/>
				</div>
			</div>
<?php
	}
?>
			<div class="itemSection">
<?php
	$facebook = new Facebook(array(
	  'appId'  => $FACEBOOK_APPID,
	  'secret' => $FACEBOOK_SECRET,
	));	
	
	$facebook->setAccessToken($currentUser->FacebookToken);
	try{
		$fb_user_profile = $facebook->api('/me', 'GET');
	}
	catch(Exception $e) {
		$fb_user_profile = false;
	}
	
	$loggedin_fb = false;
	if($fb_user_profile) {
		$loggedin_fb = true;
		$fb_string = $fb_user_profile['name'] . " (<a href='#'>remove</a>)";
	}
?>
				<div id="FacebookDiv">
<script>
	function updateFacebookName(name) {
		if(name == "") {
			document.getElementById('FacebookLoggedInDiv').style.display = "none";
			document.getElementById('FacebookLoggedOutDiv').style.display = "";
			document.getElementById('FacebookNameSpan').innerHTML = "";
		}
		else {
			document.getElementById('FacebookLoggedInDiv').style.display = "";
			document.getElementById('FacebookLoggedOutDiv').style.display = "none";
			document.getElementById('FacebookNameSpan').innerHTML = name;
		}
	}
</script>
					<div id="FacebookLoggedInDiv" style="<?= !$loggedin_fb ? "display: none" : "" ?>">
						<label for="Facebook">Facebook Account:</label> <span id="FacebookNameSpan"><?= $fb_user_profile['name'] ?></span> (<a href='#' onclick="wopen('link_fb.php?remove=true','Link_Social',400,300); return false;">remove</a>)
					</div>
					<div id="FacebookLoggedOutDiv" style="<?= $loggedin_fb ? "display: none" : "" ?>">
						<label for="Facebook">Facebook Account:</label> (<a href='#' onclick="wopen('link_fb.php','Link_Social',400,300); return false;">Setup Now</a>)
					</div>
				</div>
<?php
	Codebird::setConsumerKey($TWITTER_KEY, $TWITTER_SECRET);
	
	$cb = Codebird::getInstance();
	
	if($_SESSION['twitter_name'] == "" && $currentUser->TwitterToken != ""){
		list($t,$s) = explode(":",$currentUser->TwitterToken);
		$cb->setToken($t, $s);
		$reply = $cb->account_verifyCredentials();
		
		// if authorized set session
		if($reply->httpstatus != '401') {
			$_SESSION['twitter_name'] = $reply->name;
		}
		// else clear credentials
		else {
			$currentUser->TwitterToken = "";
			$_SESSION['twitter_name'] = "";
			$portal->UpdateUser($currentUser);
		}
	}
	
	$loggedin_tw = false;
	if($_SESSION['twitter_name'] != "") {
		$loggedin_tw = true;
	}
?>
				<div id="TwitterDiv">
<script>
	function updateTwitterName(name) {
		if(name == "") {
			document.getElementById('TwitterLoggedInDiv').style.display = "none";
			document.getElementById('TwitterLoggedOutDiv').style.display = "";
			document.getElementById('TwitterNameSpan').innerHTML = "";
		}
		else {
			document.getElementById('TwitterLoggedInDiv').style.display = "";
			document.getElementById('TwitterLoggedOutDiv').style.display = "none";
			document.getElementById('TwitterNameSpan').innerHTML = name;
		}
	}
</script>
					<div id="TwitterLoggedInDiv" style="<?= !$loggedin_tw ? "display: none" : "" ?>">
						<label for="Twitter">Twitter Account:</label> <span id="TwitterNameSpan"><?= $_SESSION['twitter_name'] ?></span> (<a href='#' onclick="wopen('link_tw.php?remove=true','Link_Social',400,300); return false;">remove</a>)
					</div>
					<div id="TwitterLoggedOutDiv" style="<?= $loggedin_tw ? "display: none" : "" ?>">
						<label for="Twitter">Twitter Account:</label> (<a href='#' onclick="wopen('link_tw.php','Link_Social',400,300); return false;">Setup Now</a>)
					</div>
				</div>
<?php
	$oauth_callback = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
	$linkedin = new LinkedIn($LINKEDIN_KEY, $LINKEDIN_SECRET, $oauth_callback );
	
	if($_SESSION['linkedin_name'] == "" && $currentUser->LinkedinToken != ""){
		list($t,$s) = explode(":",$currentUser->LinkedinToken);
		$linkedin->access_token = new OAuthConsumer($t, $s, 1);
		$response = new SimpleXMLElement($linkedin->getProfile("~:(id,first-name,last-name,headline,picture-url)"));
		
		// if authorized set session
		if($response->status != '401') {
			$_SESSION['linkedin_name'] = $response->{'first-name'} . " " . $response->{'last-name'};
		}
		// else clear credentials
		else {
			$currentUser->LinkedinToken = "";
			$_SESSION['linkedin_name'] = "";
			$portal->UpdateUser($currentUser);
		}
	}
	
	$loggedin_li = false;
	if($_SESSION['linkedin_name'] != "") {
		$loggedin_li = true;
	}
?>
				<div id="LinkedInDiv">
<script>
	function updateLinkedInName(name) {
		if(name == "") {
			document.getElementById('LinkedInLoggedInDiv').style.display = "none";
			document.getElementById('LinkedInLoggedOutDiv').style.display = "";
			document.getElementById('LinkedInNameSpan').innerHTML = "";
		}
		else {
			document.getElementById('LinkedInLoggedInDiv').style.display = "";
			document.getElementById('LinkedInLoggedOutDiv').style.display = "none";
			document.getElementById('LinkedInNameSpan').innerHTML = name;
		}
	}
</script>
					<div id="LinkedInLoggedInDiv" style="<?= !$loggedin_li ? "display: none" : "" ?>">
						<label for="LinkedIn">LinkedIn Account:</label> <span id="LinkedInNameSpan"><?= $_SESSION['linkedin_name'] ?></span> (<a href='#' onclick="wopen('link_li.php?remove=true','Link_Social',400,300); return false;">remove</a>)
					</div>
					<div id="LinkedInLoggedOutDiv" style="<?= $loggedin_li ? "display: none" : "" ?>">
						<label for="LinkedIn">LinkedIn Account:</label> (<a href='#' onclick="wopen('link_li.php','Link_Social',400,300); return false;">Setup Now</a>)
					</div>
				</div>
			</div>
		</div>
<?php
	//if($isSubAdmin)
	if(true)
	{
?>
	<center>
		<div class="itemSection">
			<div class="buttonSection">
				<input type="submit" value="Update" name="Submit"/>&nbsp;&nbsp;<input type="submit" value="Cancel" name="Submit"/>
			</div>
		</div>
	</center>
<?php
	}
?>
	</div>
</form>
</div>
</div>
</body>
</html>