<?php
	require("../printable/include/mysql.inc.php");
	require("../printable/include/optimize.printable.inc.php");
	require("globals.php");
		
	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	
	$portal = new OptimizePortal($COMPANY_ID, $db);
	
	$currentUser = $portal->GetUser($_SESSION['currentuserid']);
	
	// Check login
	if( !$currentUser ) 
	{
		header( "Location: login.php?message=" . urlencode( "Not logged in or login error.  Please try again." ) );
		die();
  	}
  	
  	$contacts = explode(",", $_GET['delete']);
  	  	
  	foreach($contacts as $cID)
  	{
  		$portal->DeleteContact($cID, $currentUser->UserID);
  	}
  	
  	header( "Location: contacts.php?message=" . urlencode( "Contacts deleted." ));