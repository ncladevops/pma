<?php
require("../printable/include/mysql.inc.php");
require("../printable/include/optimize.printable.inc.php");
require("globals.php");

$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);

$portal = new OptimizePortal($COMPANY_ID, $db);

$currentUser = $portal->GetUser($_SESSION['currentuserid']);

if (!$currentUser) {
    header("Location: login.php?message=" . urlencode("Not logged in or login error."));
    die();
}

if ($_GET['id'] == 0) {
    $oppt = new Opportunity;

    $lead = $portal->GetOptimizeContact($_GET['contactid'], $currentUser->UserID);
} else {
    $oppt = $portal->GetOpportunity($_GET['id']);

    $lead = $portal->GetOptimizeContact($oppt->ContactID, $currentUser->UserID);
}

if (!$lead) {
    die("Invalid ID");
}

if ($_POST['Submit'] == 'Update') {
    if (!$oppt->OpportunityID) {
        $opptID = $portal->AddOpportunity($lead->SubmissionListingID);
        $oppt = $portal->GetOpportunity($opptID);
    }

    $oppt->OpportunityStageID = $_POST['OpportunityStageID'];
    $oppt->OpportunityTypeID = $_POST['OpportunityTypeID'];
    $oppt->EstimatedCloseDate = $_POST['EstimatedCloseDate'];
    $oppt->Value = str_replace(",", "", $_POST['Value']);
    $oppt->Notes = $_POST['Notes'];
    $oppt->Probability = $_POST['Probability'];
    $oppt->QuoteNumber = $_POST['QuoteNumber'];

    $portal->UpdateOpportunity($oppt);

    echo "<html><body onload='window.close()'></body></html>";
    die();
}

$stages = $portal->GetOpportunityStages();
$types = $portal->GetOpportunityTypes();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title>
        <?= $portal->CurrentCompany->CompanyName ?> :: Modify Opportunity for <?= "$lead->FirstName $lead->LastName" ?>
    </title>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />
    <?php include("components/bootstrap.php") ?>

</head>
<body onload="<?= $close == true ? "parent.location='download_vcs.php?id=$AppointmentID'" : ($error == 1 ? "alert('$errorText')" : "" ) ?>">
<form method="post" action="<?= $_SERVER['PHP_SELF'] ?>?<?= $oppt->OpportunityID ? "id=$oppt->OpportunityID" : "id=0&contactid=$lead->SubmissionListingID"; ?>">
    <div class="sectionHeader row">
        <h2>Modify Opportunity</h2>
    </div>

    <div id="EditOpportunityDiv" class="sectionDiv row">
        <div class="itemSection row">
            <div id="ContactNameDiv" class="form-group col-xs-6">
                <label for="ContactName">Contact Name:</label></br>
                <span><?= "$lead->FirstName $lead->LastName" ?></span>
            </div>

            <div id="CreateDateDiv" class="col-xs-6">
                <label for="CreateDate">Create Date:</label></br>
                <span><?= strtotime($oppt->CreateDate) ? date("m/d/Y", strtotime($oppt->CreateDate)) : date("m/d/Y"); ?></span>
            </div>

        </div>
        <div class="itemSection row">
            <div id="QuoteNumberDiv" class="form-group col-xs-12">
                <label for="Value">Quote Number:</label><br/>
                <input class="form-control input-sm" type="text" id="QuoteNumber" name="QuoteNumber" value="<?= $oppt->QuoteNumber; ?>"/>
            </div>
        </div>
        <div class="itemSection row">
            <div id="OpportunityStageIDDiv" class="form-group col-xs-12">
                <label for="OpportunityStageID">Stage:</label><br/>
                <select class="form-control input-sm" name="OpportunityStageID" id="OpportunityStageID">
                    <option value="">- Please Choose -</option>
                    <?php
                    foreach ($stages as $stage) {
                        ?>
                        <option value="<?= $stage->OpportunityStageID ?>" <?= $stage->OpportunityStageID == $oppt->OpportunityStageID ? "SELECTED" : "" ?>><?= $stage->OpportunityStageName ?></option>
                        <?php
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="itemSection row">
            <div id="OpportunityTypeIDDiv" class="form-group col-xs-12">
                <label for="OpportunityTypeID">Type:</label><br/>
                <select class="form-control input-sm" name="OpportunityTypeID" id="OpportunityTypeID">
                    <option value="">- Please Choose -</option>
                    <?php
                    foreach ($types as $type) {
                        ?>
                        <option value="<?= $type->OpportunityTypeID ?>" <?= $type->OpportunityTypeID == $oppt->OpportunityTypeID ? "SELECTED" : "" ?>><?= $type->OpportunityTypeName ?></option>
                        <?php
                    }
                    ?>
                </select>
            </div>
            <div id="EstimatedCloseDateDiv" class="form-group col-xs-12">
                <label for="EstimatedCloseDate">Est. Close Date:</label><br/>
                <input class="form-control input-sm datepicker" type="text" id="EstimatedCloseDate" name="EstimatedCloseDate" value="<?= strtotime($oppt->EstimatedCloseDate) ? date("m/d/Y", strtotime($oppt->EstimatedCloseDate)) : ""; ?>"/>
            </div>
        </div>
        <div class="itemSection row">
            <div id="ValueDiv" class="form-group col-xs-7">
                <label for="Value">Value:</label><br/>
                <input class="form-control input-sm" type="text" id="Value" name="Value" value="<?= $oppt->Value; ?>"/>
            </div>
            <div id="ProbabilityDiv" class="form-group col-xs-5">
                <label for="Probability">Probability (%):</label><br/>
                <input class="form-control input-sm" type="text" id="Probability" name="Probability" value="<?= $oppt->Probability; ?>"/>
            </div>
        </div>
        <div class="longItemSection row">
            <div id="NotesDiv" class="form-group col-xs-12">
                <label for="Notes">Notes:</label><br/>
                <textarea class="form-control input-sm" name="Notes"><?= $oppt->Notes ?></textarea>
            </div>
        </div>
    </div>
    <div class="buttonDiv row">
        <center>
            <div class="buttonSection">
                <input type="submit" value="Update" name="Submit" id="Submit" class="btn btn-info btn-sm" />&nbsp;&nbsp;<input type="button" value="Cancel" onclick="window.close()" class="btn btn-default btn-sm" />
            </div>
        </center>
    </div>
</form>
<?php include("components/bootstrap-footer.php") ?>
</body>
</html>