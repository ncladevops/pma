<?php
require("../printable/include/mysql.inc.php");
require("../printable/include/optimize.printable.inc.php");
require("globals.php");

$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);

$portal = new OptimizePortal($COMPANY_ID, $db);

$currentUser = $portal->UserAccess($_SESSION['currentuserid']);
$isSubAdmin = $portal->CheckPriv($currentUser->UserID, 'subadmin');

// Check login
if (!$isSubAdmin) {
    header("Location: " . $portal->CurrentCompany->Website . "login.php?message=" . urlencode("Not logged in or login error.  Please try again."));
    die();
}

$file = $portal->GetFile($_GET['fileid']);

if (!$file) {
    echo "Invalid File.";
    die();
}

if ($_POST['Submit'] == 'Save/Upload') {
    $desc = $_POST['description'];
    $categoryID = $_POST['category'];
    $docName = '';
    $docPath = '';
    $tPath = '';

    // if new thumbnail upload
    if ($_FILES['thumbnail']['name'] != '') {
        // delete old thumb
        if (is_file("{$FILES_LOCATION}" . $portal->CurrentCompany->CompanyID . "/thumbs/$file->ThumbPath")) {
            unlink("{$FILES_LOCATION}" . $portal->CurrentCompany->CompanyID . "/thumbs/$file->ThumbPath");
        }

        // Copy to output directory
        $tempFileName = $_FILES['thumbnail']['tmp_name'];

        // create name for file
        $tName = $_FILES['thumbnail']['name'];
        $tPath = $file->FileID . "_$tName";

        // move to temp	
        move_uploaded_file($tempFileName, "{$FILES_LOCATION}" . $portal->CurrentCompany->CompanyID . "/thumbs/$tPath");
    }

    if ($_POST['FileType'] == 2) {
        // delete old file
        if (is_file("{$FILES_LOCATION}" . $portal->CurrentCompany->CompanyID . "/files/$file->FilePath")) {
            unlink("{$FILES_LOCATION}" . $portal->CurrentCompany->CompanyID . "/files/$file->FilePath");
        }

        $docName = "Embeded Code";
        $docPath = $_POST['embed'];
    } elseif ($_POST['FileType'] == 3) {
        // delete old file
        if (is_file("{$FILES_LOCATION}" . $portal->CurrentCompany->CompanyID . "/files/$file->FilePath")) {
            unlink("{$FILES_LOCATION}" . $portal->CurrentCompany->CompanyID . "/files/$file->FilePath");
        }

        $docName = "Printable SKU";
        $docPath = $_POST['sku'];
    } elseif ($_POST['FileType'] == 4) {
        // delete old file
        if (is_file("{$FILES_LOCATION}" . $portal->CurrentCompany->CompanyID . "/files/$file->FilePath")) {
            unlink("{$FILES_LOCATION}" . $portal->CurrentCompany->CompanyID . "/files/$file->FilePath");
        }

        $docName = "URL";
        $docPath = $_POST['url'];
    } else {
        // if new document upload
        if ($_FILES['document']['name'] != '') {
            // delete old file
            if (is_file("{$FILES_LOCATION}" . $portal->CurrentCompany->CompanyID . "/files/$file->FilePath")) {
                unlink("{$FILES_LOCATION}" . $portal->CurrentCompany->CompanyID . "/files/$file->FilePath");
            }

            // Copy to output directory
            $tempFileName = $_FILES['document']['tmp_name'];

            // create name for file
            $docName = $_FILES['document']['name'];
            $docPath = $file->FileID . "_$docName";

            // move to temp	
            move_uploaded_file($tempFileName, "{$FILES_LOCATION}" . $portal->CurrentCompany->CompanyID . "/files/$docPath");
        }
    }

    // update db
    $file->Description = $_POST['description'];
    $file->Detail = $_POST['detail'];
    $file->CategoryID = $_POST['category'];
    $file->FileName = ($docName != '' ? $docName : $file->FileName );
    $file->FilePath = ($docPath != '' ? $docPath : $file->FilePath );
    $file->ThumbPath = ($tPath != '' ? $tPath : $file->ThumbPath);
    $file->LastEdit = date("Y-m-d H:i:s");
    $file->FileType = $_POST['FileType'];
    $file->IsPublic = $_POST['IsPublic'];
    $file->PublicSubject = $_POST['PublicSubject'];
    $file->PublicBody = $_POST['PublicBody'];
    $file->DownloadType = $_POST['DownloadType'];

    $portal->UpdateFile($file);

    // direct to manage files
    header("Location: manage_filedl.php?section=" . $_GET['section']);
    die();
}

// Lookup File Size
if (is_file("{$FILES_LOCATION}" . $portal->CurrentCompany->CompanyID . "/files/$file->FilePath")) {
    $filesize = number_format(filesize("{$FILES_LOCATION}" . $portal->CurrentCompany->CompanyID . "/files/$file->FilePath") / 1024, 2);
} else {
    $filesize = '0.00';
}

$categories = $portal->GetCategories('all', $_GET['section'], true, 'all');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <title>
            <?= $portal->CurrentCompany->CompanyName ?> :: Control Panel
        </title>
        <meta http-equiv="content-type" content="text/html;charset=utf-8" />		
        <script  src="js/func.js"></script>	
        <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />
        <script language="JavaScript" type="text/JavaScript">
            function toggleFileType(fileTypeID)
            {
            for(i = 1; i <= 4; i++)
            {
            document.getElementById("FileType" + i).checked = false;
            }

            document.getElementById("FileType" + fileTypeID).checked = true;
            }

            function TogglePublicSection(isPublic) {
            if(isPublic == 1) {
            document.getElementById("publicParams").style.display = "";
            document.getElementById("FileDiv").style.height = "900px";
            }
            else {
            document.getElementById("publicParams").style.display = "none";
            document.getElementById("FileDiv").style.height = "700px";
            }
            }
        </script>	

        <?php include("components/bootstrap.php") ?>

    </head>
    <body bgcolor="#FFFFFF">
        <div id="page">
            <?php include("components/header.php") ?>
            <div id="body">
                <?php
                $CURRENT_PAGE = "Control Panel";
                include("components/navbar.php");
                ?>
                <form method="post" action="<?= $_SERVER['PHP_SELF'] ?>?fileid=<?= $_GET['fileid'] ?>&section=<?= $_GET['section'] ?>" enctype="multipart/form-data" >

                    <?php if (isset($_GET['message'])): ?>
                        <div class="container">
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <?= $_GET['message']; ?>
                            </div>
                        </div>
                    <?php endif; ?> 

                    <div id="EditFileDiv" class="well container">
                        <div class="sectionHeader"><h2>Edit/Upload File</h2></div>
                        <div id="FileDiv" class="longSectionDiv" style="<?= $file->IsPublic == 1 ? "height: 900px;" : ""; ?>">
                            <div class="itemSection row">
                                <div id="DescriptionDiv" class="form-group col-md-3">
                                    <label for="description">Name:</label><br/>
                                    <input class="form-control input-sm" type="text" name="description" value="<?= $file->Description ?>" size="45"/>
                                </div>
                            </div>
                            <div class="itemSectionLong row">
                                <div id="DetailDiv" class="form-group col-md-6">
                                    <label for="detail">Description:</label><br/>
                                    <textarea  class="form-control input-sm" name="detail" rows="7" cols="30"><?= $file->Detail ?></textarea>
                                </div>
                            </div>
                            <div class="itemSection row">
                                <div id="CategoryDiv" class="form-group col-md-3">
                                    <label for="category">Parent Folder:</label><br/>
                                    <select  class="form-control input-sm" name="category">
                                        <option value="0"> - Select a category - </option>
                                        <?php
                                        $category = new FileCategory();
                                        if (sizeof($categories) <= 0) {
                                            echo '<option value="0"> - No Categories - </option>';
                                        } else {
                                            foreach ($categories as $category) {
                                                if ($portal->CheckPriv($currentUser->UserID, 'admin') || $category->GroupID != 0) {
                                                    ?>
                                                    <option value="<?= $category->CategoryID ?>" <?= $category->CategoryID == $file->CategoryID ? "SELECTED" : "" ?>><?= str_repeat('&nbsp;&nbsp;&nbsp;', $portal->GetCategoryLevel($category->CategoryID)) . $category->CategoryName ?></option>
                                                    <?php
                                                }
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="itemSection row">
                                <div id="DownloadTypeDiv" class="form-group col-md-6">
                                    <label for="DownloadType">Download Type:</label><br/>
                                    <input type="radio" name="DownloadType" value="1" <?= $file->DownloadType == 1 ? "CHECKED" : "" ?>/> <label>View Only</label>
                                    <input type="radio" name="DownloadType" value="2" <?= $file->DownloadType == 2 ? "CHECKED" : "" ?>/> <label>Download Only</label>
                                    <input type="radio" name="DownloadType" value="3" <?= $file->DownloadType == 3 ? "CHECKED" : "" ?>/> <label>View &amp; Download</label>

                                </div>
                            </div>
                            <div class="itemSection row">
                                <div id="IsPublicDiv" class="form-group col-md-3">
                                    <label for="IsPublic">Is Public:</label><br/>
                                    <input type="radio" name="IsPublic" value="1" <?= $file->IsPublic == 1 ? "CHECKED" : "" ?> onchange="TogglePublicSection(this.value);"/> <label>Yes</label>
                                    <input type="radio" name="IsPublic" value="0" <?= $file->IsPublic == 0 ? "CHECKED" : "" ?> onchange="TogglePublicSection(this.value);"/> <label>No</label>												
                                </div>
                            </div>
                            <div id="publicParams" style="<?= $file->IsPublic == 1 ? "" : "display: none;"; ?>">
                                <div class="itemSection row">
                                    <div id="PublicSubjectDiv" class="form-group col-md-3">
                                        <label for="PublicSubject">Public Subject:</label><br/>
                                        <input class="form-control input-sm" type="text" name="PublicSubject" value="<?= $file->PublicSubject ?>" size="95"/>							
                                    </div>
                                </div>
                                <div class="itemSectionLong row" class="form-group col-md-3">
                                    <div id="PublicBody">
                                        <label for="PublicBody">Public Body:</label><br/>
                                        <textarea name="PublicBody" rows="7" cols="30"><?= $file->PublicBody ?></textarea>
                                    </div>
                                </div>				
                            </div>
                            <div class="itemSectionMedium row">
                                <div id="ThumbnailDiv" class="form-group col-md-3">
                                    <label for="thumbnail">Thumbnail:</label><br/>
                                    <a href="loadthumb.php?fileid=<?= $file->FileID ?>&thumb=false" rel="thumbnail"><img class="img-thumbnail" src="loadthumb.php?fileid=<?= $file->FileID ?>&thumb=true" border="0"/></a>
                                    Upload New:<input class="form-control input-sm" type="file" name="thumbnail"/>
                                </div>
                            </div>
                            <div class="subSectionHeader">File Type Setup</div>
                            <div class="itemSectionMedium row">
                                <div id="FileUploadDiv"  class="form-group col-md-3">
                                    <input type="radio" name="FileType" id="FileType1" value="1" <?= $file->FileType == 1 ? "CHECKED" : "" ?>/><strong>Upload File:</strong>
                                    <div class="radioDetail">
                                        Upload New: <input class="form-control input-sm" type="file" name="document" onclick="toggleFileType(1);" /><br/>
                                        <?php
                                        if ($file->FileType == 1) {
                                            ?>
                                            Current: <?= "$file->FileName ({$filesize}KB)" ?> 
                                            <a href="getfile.php?fileid=<?= $file->FileID ?>" onClick="wopen('getfile.php?fileid=<?= $file->FileID ?>', 'getfile', 580, 350);
                                                    return false">
                                                download
                                            </a>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="itemSectionLong row">
                                <div id="EmbedDiv" class="form-group col-md-3">
                                    <input type="radio" name="FileType" id="FileType2" value="2" <?= $file->FileType == 2 ? "CHECKED" : "" ?>/><strong>Embed:</strong>
                                    <div class="radioDetail">
                                        <textarea cols="30" rows="7" name="embed" onclick="toggleFileType(2);"><?= $file->FileType == 2 ? $file->FilePath : "" ?></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="itemSectionMedium row">
                                <div id="SKUDiv"  class="form-group col-md-3">
                                    <input type="radio" name="FileType" id="FileType3" value="3" <?= $file->FileType == 3 ? "CHECKED" : "" ?>/><strong>SKU:</strong>
                                    <div class="radioDetail">
                                        <input class="form-control input-sm" type="text" name="sku" onclick="toggleFileType(3);" size="10" value="<?= $file->FileType == 3 ? $file->FilePath : "" ?>"/>
                                    </div>
                                </div>
                            </div>
                            <div class="itemSectionMedium row">
                                <div id="URLDiv" class="form-group col-md-3">
                                    <input type="radio" name="FileType" id="FileType4" value="4" <?= $file->FileType == 4 ? "CHECKED" : "" ?>/><strong>URL:</strong>
                                    <div class="radioDetail">
                                        <input class="form-control input-sm" type="text" name="url" onclick="toggleFileType(4);" size="55" value="<?= $file->FileType == 4 ? $file->FilePath : "" ?>"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <center>
                            <div class="itemSection row">
                                <div class="buttonSection">
                                    <input class="btn btn-info btn-sm" type="submit" name="Submit" value="Save/Upload"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <input class="btn btn-default btn-sm" type="button" name="Cancel" value="Cancel" onclick="parent.location = 'manage_filedl.php?section=<?php echo $_GET['section']; ?>'"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <input class="btn btn-default btn-sm" type="button" name="Delete" value="Delete" onclick="parent.location = 'delete_file.php?section=<?php echo $_GET['section']; ?>&fileid=<?php echo $_GET['fileid']; ?>'"/>
                                </div>
                            </div>
                        </center>
                    </div>
                </form>
            </div>
        </div>
        <?php include("components/footer.php") ?>
    </body>
</html>