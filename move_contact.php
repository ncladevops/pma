<?php
require("../printable/include/mysql.inc.php");
require("../printable/include/optimize.printable.inc.php");
require("globals.php");

$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
$portal = new OptimizePortal($COMPANY_ID, $db);

// Query to get user info
$currentUser = $portal->GetUser($_SESSION['currentuserid']);
// Check login
if (!$currentUser) {
    die("Sesssion Timed out please login again.");
}

// get current lead
$leadID = '';
if ($_GET['id'] != '') {
    $leadID = $_GET['id'];
}
$lead = $portal->GetOptimizeContact(mysql_real_escape_string($leadID), $currentUser->UserID);
if (!$lead) {
    die("Existing lead not found.");
}

if (!$lead) {
    header("Location: list_lead.php?&message=" . urlencode("Lead not found."));
    die();
}

// transfer fields from lead to contact
$userGroup = $portal->GetGroup($currentUser->GroupID);
$currentCampaign = $portal->GetSubmissionListingType($userGroup->DefaultSLT);

if ($currentCampaign->SubmissionListingTypeID != "") {
    $lead->ListingType = $currentCampaign->SubmissionListingTypeID;
    $portal->UpdateOptimizeContact($lead);

// add an entry that the contact was moved
    $query = "INSERT INTO `contactevent` (`ContactID`, `ContactEventTypeID`, `EventTime`, `EventCreatedTime`, `EventNotes`, `UserID`)
		VALUES (" . $lead->SubmissionListingID . ", 0, date(\"Y-m-d H:i:s\"), date(\"Y-m-d H:i:s\"), 'Contact transferred from Leads onDemand.', " . $currentUser->UserID . ")";
    $db->Query($query);
} else {
    die("Default Submission Listing for Sales onDemand not set for this User.");
}

?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html>
    <head><title>Transferring...</title>
        <script>
            function moveParent() {
                window.opener.location.href = 'editcontact.php?id=<?= $lead->SubmissionListingID ?>';
                window.close();
            }
        </script>
    </head>
    <body onLoad="moveParent();"></body>
    </html>
<?php
die();
?>