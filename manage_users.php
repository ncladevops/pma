<?php
require("../printable/include/mysql.inc.php");
require("../printable/include/leadsondemand.printable.inc.php");
require("globals.php");

$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);

$portal = new LeadsOnDemandPortal($COMPANY_ID, $db);

$currentUser = $portal->UserAccess($_SESSION['currentuserid']);

if (!$portal->CheckPriv($currentUser->UserID, 'subadmin')) {
    header("Location: " . $portal->CurrentCompany->Website . "home.php?message=" . urlencode("Accessed Denied."));
    die();
}

$sortCols = array('ID' => 'u.UserID', 'First Name' => 'u.FirstName, u.LastName', 'Last Name' => 'u.LastName, u.FirstName',
    'Username/Email' => 'u.Username', 'Supervisor' => 'su.Username', 'Leads' => 'nosort',
    'Edit/View' => 'nosort', 'De-Activate' => 'nosort'
);
$searchFields = array('ID' => 'u.UserID', 'First Name' => 'u.FirstName', 'Last Name' => 'u.LastName',
    'Username' => 'u.Username', 'Supervisor' => 'su.Username');

$filterOptions = array(0 => 'Enabled', 2 => 'Pending', 3 => 'Submitted', 1 => "Deactivated");

if (!isset($filterOptions[$_GET['filter']])) {
    $_GET['filter'] = 0;
}

if (array_search($_GET['sortby'], $sortCols) === false) {
    $_GET['sortby'] = "u.LastName, u.FirstName";
}

if (!isset($searchFields[$_GET['searchfield']])) {
    $whereString = "1";
} else {
    $whereString = $searchFields[$_GET['searchfield']] . " LIKE '%" . mysql_real_escape_string(stripslashes($_GET['search'])) . "%'";
}

$users = $portal->GetCompanyUsers("$whereString", $_GET['sortby'], intval($_GET['filter']));
$newLeadCounts = $portal->GetLeadCounts("all", "Today", $NEW_STATUSTYPE_ID);
$transLeadCounts = $portal->GetLeadCounts("all", "Today", $TRANSFER_STATUSTYPE_ID);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <title>
            <?= $portal->CurrentCompany->CompanyName ?> :: Manage Users
        </title>
        <meta http-equiv="content-type" content="text/html;charset=utf-8" />
        <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />
        <script  src="js/func.js"></script>	

        <?php include("components/bootstrap.php") ?>

    </head>
    <body bgcolor="#FFFFFF">
        <div id="page">
            <?php include("components/header.php") ?>
            <div id="body">
                <?php
                $CURRENT_PAGE = "Home";
                include("components/navbar.php");
                ?>

                <?php if (isset($_GET['message'])): ?>
                    <div class="container">
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <?= $_GET['message']; ?>
                        </div>
                    </div>
                <?php endif; ?> 

                <div id="controlPanelContainer" class="row">
                    <div id="folderTreeContainer" class="col-md-offset-2 col-md-2 panel panel-default">
                        <?php include("components/controlpanel_tree.php"); ?>
                    </div>
                    
                    <div class="sectionHeader col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Control Panel - Edit Users</h3>
                            </div>
                            <div class="panel-body">
                                <div id="detailContainer">
                                    <div class="row">
                                        <?php
                                        $getTemp = $_GET;
                                        unset($getTemp['search']);
                                        unset($getTemp['searchfield']);
                                        $getString = build_get_query($getTemp);
                                        ?>	
                                        <form method="get" action="<?= $_SERVER['PHP_SELF'] . "?" . $getString ?>>">
                                            <div class="backNav" style="height: 30px;">
                                                <div style="float: left; text-align: left;padding-top: 3px;padding-left: 5px;">
                                                    <label for="filter">Filter:</label> 
                                                    <select name="filter" class="input-sm form-submenu" onchange="this.form.submit();">
                                                        <?php
                                                        foreach ($filterOptions as $k => $v) {
                                                            ?>
                                                            <option value="<?= $k ?>" <?= $k == $_GET['filter'] ? "SELECTED" : "" ?>>
                                                                <?= $v ?>
                                                            </option>				
                                                            <?php
                                                        }
                                                        ?>
                                                    </select> <a href="add_user.php" class="btn btn-sm btn-primary" role="button">Add User</a>
                                                </div>
                                                <div style="float: right; text-align: right;">	
                                                    <label for="search">Search:</label> 
                                                    <input type="text" name="search" class="input-sm form-submenu" style="width: 120px;" value="<?= $_GET['search'] ?>" />
                                                    in field 
                                                    <select name="searchfield" class="input-sm form-submenu">
                                                        <?php
                                                        foreach ($searchFields as $k => $v) {
                                                            ?>
                                                            <option value="<?= $k ?>" <?= $k == $_GET['searchfield'] ? "SELECTED" : "" ?>>
                                                                <?= $k ?>
                                                            </option>				
                                                            <?php
                                                        }
                                                        ?>
                                                    </select>
                                                    <input type="submit" class="btn btn-sm btn-default" role="button" name="Submit" value="Search" />
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div style="overflow: auto;" id="UserDiv" class="row table-responsive">
                                        <table class="table table-striped table-hover table-bordered" >
                                            <thead>
                                                <tr class="user_table rowheader" id="HeaderRow">
                                                    <?php
                                                    foreach ($sortCols as $k => $v) {
                                                        if ($v != "nosort" && $_GET['sortby'] != $v) {
                                                            $getTemp = $_GET;
                                                            $getTemp['sortby'] = $v;
                                                            $getString = build_get_query($getTemp);
                                                            ?>
                                                            <th class="user_table">
                                                                <a href="<?= $_SERVER['PHP_SELF'] . "?" . $getString ?>">
                                                                    <?= $k ?>
                                                                </a>
                                                            </th>
                                                            <?php
                                                        } else {
                                                            ?>
                                                            <th class="user_table"><?= $k ?></th>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                if (is_array($users)) {
                                                    $i = 0;
                                                    foreach ($users as $u) {
                                                        $i++;
                                                        ?>
                                                        <tr class="user_table<?= $i % 2 == 0 ? ' roweven' : ' rowodd' ?>">
                                                            <td class="user_table" align="right">
                                                                <?= $u->UserID ?>
                                                            </td>
                                                            <td class="user_table">
                                                                <?= $u->FirstName ?>
                                                            </td>
                                                            <td class="user_table">
                                                                <?= $u->LastName ?>
                                                            </td>
                                                            <td class="user_table">
                                                                <?= $u->Username ?>
                                                            </td>
                                                            <td class="user_table">
                                                                <?= $u->SupervisorEmail ?>
                                                            </td>
                                                            <td class="user_table">
                                                                <?= ($newLeadCounts[$u->UserID] + $transLeadCounts[$u->UserID]) . "/" . $u->ListingBudget ?>
                                                            </td>
                                                            <td class="user_table" align="right">
                                                                <a href="edit_user.php?id=<?= $u->UserID ?>">
                                                                    <i class="glyphicon glyphicon-edit"></i> edit
                                                                </a>
                                                            </td>
                                                            <td class="user_table" align="right">
                                                                <a href="delete_user.php?id=<?= $u->UserID ?>" onclick="return confirm('Are you sure you want to de-activate this user?')">
                                                                   <i class="glyphicon glyphicon-ban-circle"></i>  de-activate
                                                                </a>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php include("components/footer.php") ?>
    </body>
</html>