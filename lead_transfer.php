<?php
	require("../printable/include/mysql.inc.php");
	require("../printable/include/optimize.printable.inc.php");
	require("globals.php");
	
	$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);
	
	$portal = new OptimizePortal($COMPANY_ID, $db);
	
	$currentUser = $portal->GetUser($_SESSION['currentuserid']);
		
	if(!$currentUser)
	{
		header("Location: login.php?message=" .urlencode("Not logged in or login error."));
		die();
	}
	
	if($portal->CheckPriv($currentUser->UserID, 'subadmin'))
	{
		$userString = "all";
	}
	else 
	{
		$userString = $currentUser->UserID;
	}
	
	$lead = $portal->GetOptimizeContact($_GET['id'], $userString);
		
	if(!$lead)
	{
		die("Invalid Lead ID");
	}
	
	if($_POST['Submit'] == 'Convert') {
		$user = $portal->GetUser($_POST['TransferTo']);
		
		if(!$user) {
			die("Invalid Transfer Target");
		}
		
		$uGroup = $portal->GetGroup($user->GroupID);
		
		$lead->UserID = $user->UserID;
		$lead->ListingType = $uGroup->DefaultSLT;
		$lead->ContactStatusTypeID = $LEADTRANFERSTATUSID;
		
		$portal->UpdateOptimizeContact($lead);
		
		header('Location: list_lead.php?message=' . urlencode("Lead transfered."));
		die();		
	}
	elseif($_POST['Submit'] == 'Cancel') {
		header('Location: edit_lead.php?id=' . $lead->SubmissionListingID . '&message=' . urlencode("Action Canceled. Lead has not been transfered."));
		die();		
	}
	
	$users = $portal->GetCompanyUsers();
		
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
	<title>
		<?= $portal->CurrentCompany->CompanyName ?> :: Convert <?= "$lead->FirstName $lead->LastName" ?> to Prospect
	</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />		
	<link type="text/css" media="all" rel="stylesheet" href="style.css" />
	<link type="text/css" media="all" rel="stylesheet" href="css/lead_transfer.css" />
	<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />
	<script  src="js/func.js"></script>

</head>
<body bgcolor="#FFFFFF">
<?php 
	include("components/header.php");
?>
<div id="body">
<?php 
	$CURRENT_PAGE = "Leads";
	include("components/navbar.php");
?>
<form method="post" action="<?= $_SERVER['PHP_SELF'] ?>?id=<?= $lead->SubmissionListingID ?>">
	<div class="error">
		<?= $_GET['message']; ?>
	</div>
	<div id="LeadTransferDiv">
		<div class="sectionHeader">
			Convert to Prospect
		</div>
		<div id="TransferDiv" class="sectionDiv">
			<div class="itemSection">
				<div id="ContactNameDiv">
					<label for="ContactName">Lead Name:</label><br/>
					<?= "$lead->FirstName $lead->LastName" ?>
				</div>
			</div>
			<div class="itemSection">
				<div id="TransferToDiv">
					<label for="TransferTo">Tranfser To:</label><br/>
					<select id="TransferTo" name="TransferTo">
						<option value="">- Please Choose -</option>
<?php
	foreach($users as $u) {
?>
						<option value="<?= $u->UserID ?>"><?= "$u->FirstName $u->LastName" ?></option>
<?php
	}
?>
					</select>
				</div>
			</div>
		</div>	
	</div>
	<center>
		<div class="itemSection">
			<div class="buttonSection">
				<input type="submit" value="Convert" name="Submit"/>&nbsp;&nbsp;
				<input type="submit" value="Cancel" name="Submit"/>
			</div>
		</div>
	</center>
	</div>
</form>
</div>	
</body>
</html>