<?php
require("../printable/include/mysql.inc.php");
require("../printable/include/optimize.printable.inc.php");
require("globals.php");

$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);

$portal = new OptimizePortal($COMPANY_ID, $db);

$currentUser = $portal->UserAccess($_SESSION['currentuserid']);

if ($_GET['logout'] == 'true') {
    $portal->EndUserSession($currentUser->UserID);
    session_destroy();
    if ($_GET['message'] == '') {
        $_GET['message'] = "You are now logged out.";
    }
    header("Location: login.php?message=" . urlencode($_GET['message']));
    die();
}

if ($currentUser) {
    header("Location: home.php?message=" . urlencode("You are already logged in"));
    die();
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title>
        <?= $portal->CurrentCompany->CompanyName ?> :: Login
    </title>
    <meta http-equiv="content-type" content="text/html;charset=utf-8"/>
    <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico"/>

    <?php include("components/bootstrap.php") ?>

</head>

<body onload="document.forms.login_form.username.focus();">

<div id="login_container" class="well col-md-offset-2 col-md-8">

    <div class="row">

        <div class="col-md-4">

            <form method="post" action="auth.php" name="login_form" id="login_form" class="form-horizontal"
                  role="form">

                <div class="row">

                    <h2 class="form-signin-heading"><img style="max-width: 250px;"
                                                         src="images/OptifiNowLogo.png"/>
                    </h2>

                    <?php if (isset($_GET['message'])): ?>
                        <div class="container col-md-12">
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert"
                                        aria-hidden="true">&times;</button>
                                <?= $_GET['message']; ?>
                            </div>
                        </div>
                    <?php endif; ?>

                </div>

                <div class="row">
                    <div class="form-group">
                        <label for="username" class="col-sm-3 control-label">Username</label>

                        <div class="col-xs-12 col-sm-8 col-md-12 col-lg-12">
                            <input type="text" class="form-control input-sm" id="username" name="username"
                                   placeholder="Username"/>
                        </div>
                    </div>


                    <div class="form-group">
                        <label for="password" class="col-sm-3 control-label">Password</label>

                        <div class="col-xs-12 col-sm-8 col-md-12 col-lg-12">
                            <input type="password" class="form-control input-sm" id="password" name="password"
                                   placeholder="Password"/>
                        </div>
                    </div>
                    <div id="button_div">
                        <div class="form-group">
                            <div class="col-lg-12 col-md-12 col-sm-11">
                                <input type="submit" name="Submit" class="btn btn-primary btn-sm btn-block" value="Log-In"/>
                            </div>
                        </div>

                    </div>
                    <div class="form-group">
                        <div class="col-sm-8">
                            <div class="checkbox">
                                <label>
                                    <input name="" type="checkbox" value=""/> Yes, keep me logged in.
                                </label>
                                <a class="help-block" href="forgot_pass.php">Forgot your password?</a>
                            </div>
                        </div>
                    </div>

                </div>

            </form>
        </div>

        <div class="col-md-8 hidden-xs hidden-sm">
            <div id="optifinow-carousel" class="carousel slide" data-ride="carousel" data-interval="6000">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#optifinow-carousel" data-slide-to="0" class="active"></li>
                    <li data-target="#optifinow-carousel" data-slide-to="1"></li>
                    <li data-target="#optifinow-carousel" data-slide-to="2"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="images/banner/leads.png" class="img-responsive">

                        <div class="carousel-caption">
                            OptifiNow: Leads onDemand
                        </div>
                    </div>
                    <div class="item">
                        <img src="images/banner/sales.png" class="img-responsive">

                        <div class="carousel-caption">
                            OptifiNow: Sales onDemand
                        </div>
                    </div>
                    <div class="item">
                        <img src="images/banner/reports.png" class="img-responsive">

                        <div class="carousel-caption">
                            OptifiNow: Reports onDemand
                        </div>
                    </div>
                </div>

                <!-- Controls -->
                <a class="left carousel-control" href="#optifinow-carousel" role="button"
                   data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#optifinow-carousel" role="button"
                   data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>

    </div>

</div>

<?php include("components/bootstrap-footer.php") ?>


</body>
</html>