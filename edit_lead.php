<?php
require("../printable/include/mysql.inc.php");
require("../printable/include/optimize.printable.inc.php");
require("../printable/include/component/fanniemae.php");
require("globals.php");

if ($_POST['popup_window'] == 'true') {
    $_GET['popup'] = true;
}

$db = new MySqlDatabase($dbhost, $dbuser, $dbpass, $dbdatabase);

$portal = new OptimizePortal($COMPANY_ID, $db);

$currentUser = $portal->GetUser($_SESSION['currentuserid']);

if (!$currentUser) {
    header("Location: login.php?message=" . urlencode("Not logged in or login error."));
    die();
}

$isAdmin = $portal->CheckPriv($currentUser->UserID, 'admin');
$isCorpMan = $portal->CheckPriv($currentUser->UserID, 'corporatemanager');
$isRegMan = $portal->CheckPriv($currentUser->UserID, 'regionmanager');
$isDivMan = $portal->CheckPriv($currentUser->UserID, 'divisionmanager');
if ($isAdmin || $isCorpMan || $isRegMan || $isDivMan)
    $isMan = true;
else
    $isMan = false;

$lead = $portal->GetOptimizeContact($_GET['id'], $currentUser->UserID);

$supervisors = $portal->GetSupervisors();

$leadRep = $portal->GetUser($lead->UserID);

$lead_step_ids = explode(";", $_SESSION['lead_step_ids']);

if (!$lead) {
    header("Location: home.php?message=" . urlencode("Lead not Available") . '&' . $_SESSION['lastquery']);
    die();
}

if ($_POST['Submit'] == 'Update' || $_POST['Submit'] == 'Update & Close') {
    //format DOB from m/d/Y to MySQL (Y-m-d) Date Format
    $_POST['DOB'] = $_POST['DOB'] ? date("Y-m-d", strtotime($_POST['DOB'])) : "";
    $_POST['CoDOB'] = $_POST['CoDOB'] ? date("Y-m-d", strtotime($_POST['CoDOB'])) : "";


    if ($_POST['ContactStatusTypeID'] == 1 && !$isMan) {
        $_POST['ContactStatusTypeID'] = $lead->ContactStatusTypeID;
    }

    $oldStatus = $lead->ContactStatusTypeID;
    foreach ($_POST as $k => $v) {
        $lead->$k = $v;
    }

    if ($isMan && $_POST['NewUserID'] != $lead->UserID) {
        $lead->UserID = $_POST['NewUserID'];
    } else {
        if (!$isMan) {
            //only update lead assignment if updater is not manager
            $lead->UserID = $currentUser->UserID;
        }
    }
    if ($_POST['DoNotCall'] == '1')
        $lead->DoNotCall = 1;
    else
        $lead->DoNotCall = 0;

    if ($_POST['DoNotEmail'] == '1')
        $lead->DoNotEmail = 1;
    else
        $lead->DoNotEmail = 0;

    /* When status update needs some reassignment or email sending */
    $cst = $portal->GetContactStatusType($_POST['ContactStatusTypeID']);

    if ($cst->ReassignUserID != 0) {
        $lead->UserID = $cst->ReassignUserID;
    }

    if ($cst->EmailUserID != 0) {
        $portal->EmailContactStatusChange($lead->SubmissionListingID, $cst->EmailUserID, $lead->ContactStatusTypeID);
    }


    $portal->UpdateOptimizeContact($lead);

    if ($_POST['Submit'] == 'Update & Close') {
        header('Location: list_lead.php?message=' . urlencode("Lead updated successfully.") . '&' . $_SESSION['lastquery']);
        die();
    }
} elseif ($_POST['Submit'] == 'Cancel') {
    header('Location: list_lead.php?message=' . urlencode("Action Canceled. Lead not updated.") . '&' . $_SESSION['lastquery']);
    die();
} elseif ($_POST['Submit'] == 'Delete' && $isMan) {
    $portal->DeleteContact($lead->SubmissionListingID, ($isMan ? $lead->UserID : $currentUser->UserID));

    header('Location: list_lead.php?message=' . urlencode("Lead deleted successfully.") . '&' . $_SESSION['lastquery']);
    die();
}
if ($_POST['Submit'] == 'Export to FannieMae File') {
    $fm = new FannieMae($lead);
    $outString = $fm->generateFNM();

    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header('Content-type: application/force-download');
    header("Content-Transfer-Encoding: binary");
    header("Content-Disposition: attachment; filename=\"$lead->LastName.fnm\";");
    header("Content-Transfer-Encoding: binary");

    echo $outString;
    die;
}

$csts = $portal->GetContactStatusTypes('all', 1, true);
$contactStatusesTypes = array();
$shortStatuses = array();
$shortStatusDetails = array();
foreach ($csts as $cst) {
    if (!$cst->readonly) {
        $shortStatuses[$cst->ShortName] = (strlen($cst->DetailName) > 0 ? 1 : 0);
        $shortStatusDetails[$cst->ShortName][] = $cst->ContactStatus;
        $contactStatusesTypes[$cst->ContactStatus] = $cst;
    }
}
$contactEventTypes = $portal->GetContactEventTypes();
$cetLookup = array();
foreach ($contactEventTypes as $cet) {
    $cetLookup[$cet->ContactEventTypeID] = $cet->EventType;
}

$allEvents = $portal->GetContactHistory($lead->SubmissionListingID, "DESC");

$messages = $portal->GetContactSms($lead->SubmissionListingID);

$users = $portal->GetCompanyUsers();
$slt = $portal->GetSubmissionListingType($lead->ListingType);
$appt = $portal->GetOpenAppointment($lead->SubmissionListingID, 'all');

$categoryValues = $portal->GetLookupValues('Category', $currentUser->GroupID);
$segmentValues = $portal->GetLookupValues('Segment', $currentUser->GroupID);
$campaignValues = $portal->GetLookupValues('Campaign', $currentUser->GroupID);

$language_lookups = $portal->GetLookupValues("LanguagePreference");
$loanType_lookups = $portal->GetLookupValues("LoanType");
$AmortizationType_lookups = $portal->GetLookupValues("AmortizationType");
$propertyType_lookups = $portal->GetLookupValues("PropertyWillBe");
$PurposeOfLoan_lookups = $portal->GetLookupValues("PurposeOfLoan");
$maritalStatusLookUp = $portal->GetLookupValues("MaritalStatus");
$PurposeOfRefinance_lookups = $portal->GetLookupValues("PurposeOfRefinance");
$AreYouStillCharging_lookups = $portal->GetLookupValues("AreYouStillCharging");
$Employment_lookups = $portal->GetLookupValues("Employment");
$CoEmployment_lookups = $portal->GetLookupValues("CoEmployment");
$SecondVoiceQueue_lookups = $portal->GetLookupValues("SecondVoiceQueue");
$OnTheHomeNumber_lookups = $portal->GetLookupValues("OnTheHomeNumber");
$Timezone_lookups = $portal->GetLookupValues("Timezone");


$oppts = $portal->GetOpportunities($lead->SubmissionListingID);

$contactHistory = $portal->GetContactHistory($lead->SubmissionListingID);
$lastCH = $contactHistory[sizeof($contactHistory) - 1];

//Timezone Fixes
$originaltimezone = date('e');

$dadded = new DateTime($lead->DateAdded, new DateTimeZone($originaltimezone));  //get date / time base on string saved and server's timezone
$dtouched = new DateTime($lastCH->EventDate, new DateTimeZone($originaltimezone));  //get date / time base on string saved and server's timezone
if (!empty($currentUser->UserTimezoneOffset)) {
    $dadded->setTimezone(new DateTimeZone($currentUser->UserTimezoneOffset)); //update date / time with the user's preferred timezone (for UserTimezoneOffset)
    $dtouched->setTimezone(new DateTimeZone($currentUser->UserTimezoneOffset)); //update date / time with the user's preferred timezone (for UserTimezoneOffset)
}

//Don't convert if it's empty / null
if (strtotime($l->DateAssigned)) {
    $dassigned = new DateTime($l->DateAssigned, new DateTimeZone($originaltimezone));  //get date / time base on string saved and server's timezone
    $dassigned->setTimezone(new DateTimeZone($currentUser->UserTimezoneOffset)); //update date / time with the user's preferred timezone (for UserTimezoneOffset)
    $l->DateAssigned = $dassigned->format("m/d/Y g:i:s a");
}

$lead->DateAdded = $dadded->format("m/d/Y g:i:s a");
$lastCH->EventDate = $dtouched->format("m/d/Y g:i:s a");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title>
        <?= $portal->CurrentCompany->CompanyName ?> Leads on Demand :: Edit Lead
    </title>
    <meta http-equiv="content-type" content="text/html;charset=utf-8"/>
    <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico"/>
    <script src="js/func.js"></script>
    <script src="js/contact_status.js"></script>

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>

    <script language="JavaScript" type="text/JavaScript">
        function updateContactEvent(id, actionName) {
            document.getElementById("LastActionSpan").innerHTML = actionName;
        }

        function appointmentPopup(id) {
            wopen('add_appointment.php?id=' + id, 'add_appointment', 350, 550);
            return false;
        }

        var statusDetails = new Array();
        <?php
        foreach ($shortStatusDetails as $shortStat => $details) {
            ?>
        statusDetails["<?= $shortStat ?>"] = new Array();
        <?php
        foreach ($details as $det) {
            ?>
        statusDetails["<?= $shortStat ?>"][<?= intval($contactStatusesTypes[$det]->ContactStatusTypeID) ?>] = "<?= $contactStatusesTypes[$det]->DetailName ?>";
        <?php
    }
}
?>
        function updateContactStatus(cstid) {
            if (isNaN(cstid)) {
                showDetailedContactStatus(cstid);
                return;
            }
            document.getElementById("ContactStatusTypeID").value = cstid;
        }
        function showDetailedContactStatus(cstid) {
            var statusSelect = document.getElementById("detailStatusID");
            document.getElementById("shortStatus").innerText = cstid;
            var i = 0;
            statusSelect.options.length = i;
            for (var id in statusDetails[cstid]) {
                statusSelect.options.length++;
                statusSelect[i].value = id;
                statusSelect[i].text = statusDetails[cstid][id];
                i++;
            }
            document.getElementById("detailDiv").style.display = "";
        }
        function submitDetailedContactStatus() {
            updateContactStatus(document.getElementById("detailStatusID").value);
            document.getElementById("detailDiv").style.display = "none";
        }
    </script>
    <?php include("components/bootstrap.php") ?>


    <style>
        /*Edit Lead - Discovery tab*/

        .Applicant {
            padding: 0px;
        }

        .agediv {
            width: 40%;
        }

        .dobfield {
            padding-right: 30px;
            padding-left: 0px;
        }

        /*.discoveryfield
        {
            padding-left:0px;
            padding-right:0px;
        }
        */

    </style>

</head>
<body bgcolor="#FFFFFF">
<?php
if ($_GET['popup'] == 'true') {

} else {
    include("components/header.php");
}
?>
<div id="body">
    <?php
    if ($_GET['popup'] == 'true') {
        include("components/popupbar.php");
    } else {
        $CURRENT_PAGE = "Leads";
        include("components/navbar.php");
    }
    ?>
    <form id="lead-detail-form" method="post"
          action="<?= $_SERVER['PHP_SELF'] ?>?id=<?= $lead->SubmissionListingID . (isset($_GET['index']) ? "&index=" . $_GET['index'] : ""); ?>">
        <?php if (isset($_GET['message'])): ?>
            <div class="container">
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?= $_GET['message']; ?>
                </div>
            </div>
        <?php endif; ?>
        <div class="row">
            <div id="EditLeadDiv" class="col-md-9">

                <div class="row">

                    <div class="panel panel-default panel-lead">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                Lead ID: <?= $lead->SubmissionListingID; ?>
                                <small>
                                        <span class="pull-right">
                                    <strong>Creation
                                        Date:</strong> <?= strtotime($lead->DateAdded) ? date("m/d/Y g:i a", strtotime($lead->DateAdded)) : "" ?>

                                            <?php if (strtotime($lead->DateAssigned)): ?>
                                                &nbsp; <strong>Date
                                                    Assigned:</strong> <?= date("m/d/Y g:i a", strtotime($lead->DateAssigned)) ?>
                                            <?php endif; ?>
                                        </span>
                                </small>
                            </h3>
                        </div>

                        <div class="panel-body">

                            <div class="itemSection row">
                                <div id="FirstNameDiv" class="form-group col-md-6">
                                    <label for="FirstName">Lead First Name</label><br/>
                                    <input type="text" class="form-control input-sm copyfield" id="FirstName"
                                           name="FirstName"
                                           value="<?= $lead->FirstName; ?>"/>
                                    <span class="visible-print"><?= $lead->FirstName ?></span>
                                </div>
                                <div id="LastNameDiv" class="form-group col-md-6">
                                    <label for="LastName">Last Name</label><br/>
                                    <input type="text" class="form-control input-sm copyfield" id="LastName"
                                           name="LastName"
                                           value="<?= $lead->LastName; ?>"/>
                                    <span class="visible-print"><?= $lead->LastName ?></span>
                                </div>
                            </div>
                            <div class="itemSection row">
                                <div id="CompanyDiv" class="form-group col-md-6">
                                    <label for="Company">Company</label><br/>
                                    <input type="text" class="form-control input-sm" id="Company" name="Company"
                                           value="<?= $lead->Company ?>" placeholder="Employer / Company"/>
                                    <span class="visible-print"><?= $lead->Company ?></span>
                                </div>
                                <div id="PosDiv" class="form-group col-md-6">
                                    <label for="Title">Position/Title</label>
                                    <input type="text" class="form-control input-sm" id="Title" name="Title"
                                           value="<?= $lead->Title ?>"/>
                                    <span class="visible-print"><?= $lead->Title ?></span>
                                </div>
                            </div>

                            <div class="itemSection row">
                                <div id="ListingTypeDiv" class="form-group col-md-4">
                                    <label for="ListingType">Lead Source</label><br/>
                                    <?php
                                    if ($slt->UserManageable == 1):
                                        $slts = $portal->GetSubmissionListingTypes(0, 'SortOrder, SubmissionListingTypeID', 'all', 'UserManageable = 1 && submissionlistingtype.SubmissionListingSourceID = 9999');
                                        ?>
                                        <select class="form-control input-sm" name="ListingType" id="ListingType">
                                            <?php
                                            foreach ($slts as $slt) {
                                                ?>
                                                <option value="<?= $slt->SubmissionListingTypeID ?>"
                                                    <?= $slt->SubmissionListingTypeID == $lead->ListingType ? "SELECTED" : "" ?>>
                                                    <?= $slt->ListingType ?>
                                                </option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                        <span class="visible-print"><?= $lead->ListingTypeName ?></span>
                                    <?php else: ?>
                                        <?php $campaign = $portal->GetSubmissionListingType($lead->ListingType); ?>
                                        <?= $campaign->ListingSourceName ?>
                                    <?php endif; ?>
                                </div>
                                <div id="OtherListingTypeDiv" class="form-group col-md-4">
                                    <label for="OtherListingType">Campaign Code</label><br/>
                                    <?php if ($slt->UserManageable == 1): ?>
                                        <input type="text" class="form-control input-sm" id="OtherListingType"
                                               name="OtherListingType" value="<?= $lead->OtherListingType; ?>"/>
                                        <span class="visible-print"><?= $lead->OtherListingType ?></span>
                                    <?php else: ?>
                                        <?= $lead->ListingTypeName ?>
                                    <?php endif; ?>
                                </div>

                                <div id="NewUserIDDiv" class="form-group col-md-4">
                                    <label for="NewUserID">Assigned To</label><br/>
                                    <?php
                                    if ($isMan) {
                                        ?>
                                        <select class="form-control input-sm" name="NewUserID"
                                                id="NewUserID">
                                            <?php
                                            foreach ($users as $u) {
                                                if ($isCorpMan || ($isRegMan && $u->RegionID == $currentUser->RegionID) || ($isDivMan && $u->DivisionID == $currentUser->DivisionID)) {
                                                    ?>
                                                    <option
                                                        value="<?= $u->UserID ?>" <?= $u->UserID == $lead->UserID ? "SELECTED" : "" ?>>
                                                        <?= $u->FirstName . " " . $u->LastName ?>
                                                    </option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                        <span class="visible-print"><?= $lead->UserFullname ?></span>
                                        <?php
                                    } elseif ($lead->ContactStatusTypeID != 1) {
                                        echo $lead->UserFullname;
                                    }
                                    ?>
                                </div>

                            </div>

                        </div>

                    </div>


                </div>

                <div class="row">

                    <div class="tab-well well" role="tabpanel">

                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation"><a href="#info" aria-controls="info"
                                                       role="tab" <?= $optifinowGlobalSetting['view']['discovery'] ? "" : "class='active'" ?>
                                                       data-toggle="tab"><h5>Info</h5></a></li>


                            <?php if ($optifinowGlobalSetting['data']['mortgage'] == TRUE): ?>

                                <?php if ($optifinowGlobalSetting['view']['discovery'] == TRUE): ?>

                                    <li role="presentation" class="active"><a href="#discovery"
                                                                              aria-controls="discovery" role="tab"
                                                                              data-toggle="tab"><h5>Discovery</h5></a>
                                    </li>

                                <?php else: ?>

                                    <li role="presentation"><a href="#mortgage" aria-controls="mortgage" role="tab"
                                                               data-toggle="tab"><h5>Contract</h5></a></li>


                                <?php endif; ?>

                            <?php else: ?>

                                <li role="presentation"><a href="#details" aria-controls="details" role="tab"
                                                           data-toggle="tab"><h5>Details</h5></a></li>


                            <?php endif; ?>
                            <li role="presentation"><a href="#notes" aria-controls="notes" role="tab"
                                                       data-toggle="tab">
                                    <h5>
                                        Notes</h5></a></li>
                            <?php if (isset($optifinowGlobalSetting['custom']['SMSOnDemand']) && $optifinowGlobalSetting['custom']['SMSOnDemand'] == TRUE): ?>
                                <li role="presentation"><a href="#messages" aria-controls="messages" role="tab"
                                                           data-toggle="tab"><h5>
                                            SMS</h5></a></li>
                            <?php endif; ?>
                            <?php if (isset($optifinowGlobalSetting['custom']['Opportunity']) && $optifinowGlobalSetting['custom']['Opportunity'] == TRUE): ?>
                                <li role="presentation"><a href="#opportunities" aria-controls="opportunities"
                                                           role="tab"
                                                           data-toggle="tab"><h5>
                                            Opportunities</h5></a></li>
                            <?php endif; ?>
                            <li role="presentation"><a href="#history" aria-controls="history" role="tab"
                                                       data-toggle="tab">
                                    <h5>History</h5></a></li>

                        </ul>


                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel"
                                 class="tab-pane <?= $optifinowGlobalSetting['view']['discovery'] ? "" : "active" ?>"
                                 id="info">

                                <div class="subSectionHeader">
                                    <h4>Lead Info</h4>
                                </div>

                                <div id="CustomerInfoDiv" class="row">
                                    <div id="MainSection" class="col-md-12">
                                        <div class="itemSectionDouble row">
                                            <div id="Address1Div" class="form-group col-md-6">
                                                <label for="Address1">Street Address</label>
                                                <input type="text" class="form-control input-sm copyfield" id="Address1"
                                                       name="Address1"
                                                       value="<?= $lead->Address1; ?>"/>
                                            </div>
                                            <div id="CityDiv" class="form-group col-md-6">
                                                <label for="City">City</label><br/>
                                                <input type="text" class="form-control input-sm copyfield" id="City"
                                                       name="City"
                                                       value="<?= $lead->City; ?>"/>
                                                <span class="visible-print"><?= $lead->City ?></span>
                                            </div>
                                        </div>
                                        <div class="itemSection row">
                                            <div id="Address2Div" class="form-group col-md-6">
                                                <label for="Address2">Address 2</label>
                                                <input type="text" class="form-control input-sm copyfield" id="Address2"
                                                       name="Address2"
                                                       value="<?= $lead->Address2; ?>"/>
                                                <span class="visible-print"><?= $lead->Address2 ?></span>
                                            </div>

                                            <div id="StateDiv" class="form-group col-md-3">
                                                <label for="State">ST</label><br/>
                                                <input type="text" class="form-control input-sm copyfield" id="State"
                                                       name="State"
                                                       value="<?= $lead->State; ?>"/>
                                                <span class="visible-print"><?= $lead->State ?></span>
                                            </div>
                                            <div id="ZipDiv" class="form-group col-md-3">
                                                <label for="Zip">Zip</label><br/>
                                                <input type="text" class="form-control input-sm copyfield" id="Zip"
                                                       name="Zip"
                                                       value="<?= $lead->Zip; ?>"/>
                                                <span class="visible-print"><?= $lead->Zip ?></span>
                                            </div>
                                        </div>
                                        <div class="itemSection row">
                                            <div id="PhoneDiv" class="form-group col-md-3">
                                                <label for="Phone">
                                                    Home Phone:
                                                    (<a href="#"
                                                        onclick="wopen('trigger_call.php?id=<?= $lead->SubmissionListingID; ?>&field=Phone', 'add_action', 350, 285);
                                                            return false;">call</a>)
                                                </label><br/>
                                                <input type="text" class="form-control input-sm copyfield" id="Phone"
                                                       name="Phone"
                                                       value="<?= $lead->Phone; ?>"/>
                                                <span class="visible-print"><?= $lead->Phone ?></span>
                                            </div>
                                            <div id="WorkPhoneDiv" class="form-group col-md-3">
                                                <label for="WorkPhone">
                                                    Work Phone:
                                                    (<a href="#"
                                                        onclick="wopen('trigger_call.php?id=<?= $lead->SubmissionListingID; ?>&field=WorkPhone', 'add_action', 350, 285);
                                                            return false;">call</a>)
                                                </label><br/>
                                                <input type="text" class="form-control input-sm copyfield"
                                                       id="WorkPhone"
                                                       name="WorkPhone"
                                                       value="<?= $lead->WorkPhone; ?>"/>
                                                <span class="visible-print"><?= $lead->WorkPhone ?></span>
                                            </div>
                                            <div id="Phone2Div" class="form-group col-md-2">
                                                <label for="Phone2">
                                                    Mobile Phone:
                                                    <?php if (isset($optifinowGlobalSetting['custom']['SMSOnDemand']) && $optifinowGlobalSetting['custom']['SMSOnDemand'] == TRUE): ?>
                                                        (<a href="#"
                                                            onclick="wopen('trigger_call.php?id=<?= $lead->SubmissionListingID; ?>&field=Phone2', 'add_action', 350, 285);
                                                                return false;">call</a> / <a href="#"
                                                                                             onclick="wopen('send_sms.php?id=<?= $lead->SubmissionListingID; ?>', 'send_sms', 400, 350);
                                                                                                 return false;"
                                                                                             title="Send SMS">sms <i
                                                                class="glyphicon glyphicon-phone"></i></a> )
                                                        <?php if ($portal->Unsubscribe_Check($lead->SubmissionListingID)): ?>
                                                            <span class="glyphicon glyphicon glyphicon-ban-circle"
                                                                  data-toggle="tooltip"
                                                                  title="Unsubscribed" style="color: red;"></span>
                                                        <?php else: ?>
                                                            <span class="glyphicon glyphicon glyphicon-ok-circle"
                                                                  data-toggle="tooltip"
                                                                  title="Subscribed" style="color: green;"></span>
                                                        <?php endif; ?>
                                                    <?php else: ?>
                                                        (<a href="#"
                                                            onclick="wopen('trigger_call.php?id=<?= $lead->SubmissionListingID; ?>&field=Phone2', 'add_action', 350, 285);
                                                                return false;">call</a>)
                                                    <?php endif; ?>
                                                </label><br/>
                                                <input type="text" class="form-control input-sm copyfield" id="Phone2"
                                                       name="Phone2"
                                                       value="<?= $lead->Phone2; ?>"/>
                                                <span class="visible-print"><?= $lead->Phone2 ?></span>
                                            </div>
                                            <div id="FaxDiv" class="form-group col-md-2">
                                                <label for="Fax">Fax</label><br/>
                                                <input type="text" class="form-control input-sm copyfield" id="Fax"
                                                       name="Fax"
                                                       value="<?= $lead->Fax; ?>"/>
                                                <span class="visible-print"><?= $lead->Fax ?></span>
                                            </div>
                                            <div id="DoNotCallDiv" class="form-group col-md-2">
                                                <label for="DoNotCall">Do Not Call</label><br/>
                                                <input type="checkbox" id="DoNotCall" name="DoNotCall" value="1"
                                                    <?= $lead->DoNotCall ? "checked" : ""; ?> />
                                                <span class="visible-print"><?= $lead->DoNotCall ?></span>
                                            </div>
                                        </div>
                                        <div class="itemSection row">
                                            <div id="EmailDiv" class="form-group col-md-5">
                                                <label for="Email">Email Address: (<a
                                                        href="mailto:<?= $lead->Email; ?>">email</a>)</label><br/>
                                                <input type="text" class="form-control input-sm copyfield" id="Email"
                                                       name="Email"
                                                       value="<?= $lead->Email; ?>"/>
                                                <span class="visible-print"><?= $lead->Email ?></span>
                                            </div>
                                            <div id="WebsiteDiv" class="form-group col-md-5">
                                                <label for="Website">Website</label><br/>
                                                <input type="text" class="form-control input-sm" id="Website"
                                                       name="CompanyWebsite"
                                                       value="<?= $lead->CompanyWebsite; ?>"/>
                                                <span class="visible-print"><?= $lead->CompanyWebsite ?></span>
                                            </div>
                                            <div id="DoNotEmailDiv" class="form-group col-md-2">
                                                <label for="DoNotEmail">Do Not Email</label><br/>
                                                <input type="checkbox" id="DoNotEmail" name="DoNotEmail" value="1"
                                                    <?= $lead->DoNotEmail ? "checked" : ""; ?> />
                                                <span class="visible-print"><?= $lead->DoNotEmail ?></span>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                            <!-- End Info -->


                            <div role="tabpanel" class="tab-pane" id="details">

                                <div class="sectionHeader">
                                    <h4>Lead Details</h4>
                                </div>


                                <div id="DetailDiv" class="sectionDiv">

                                    <div class="itemSection row">
                                        <div id="CategoryDiv" class="form-group col-md-4">
                                            <label for="Category">Category</label><br/>
                                            <select class="form-control input-sm" name="Category">
                                                <option value=""></option>
                                                <?php
                                                foreach ($categoryValues as $val) {
                                                    ?>
                                                    <option
                                                        value="<?= $val->LookupValue ?>" <?= $val->LookupValue == $lead->Category ? 'SELECTED' : '' ?>><?= $val->LookupLabel ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                            <span class="visible-print"><?= $lead->Category ?></span>
                                        </div>
                                        <div id="SegmentDiv" class="form-group col-md-4">
                                            <label for="Segment">Segment</label><br/>
                                            <select class="form-control input-sm" name="Segment">
                                                <option value=""></option>
                                                <?php
                                                foreach ($segmentValues as $val) {
                                                    ?>
                                                    <option
                                                        value="<?= $val->LookupValue ?>" <?= $val->LookupValue == $lead->Segment ? 'SELECTED' : '' ?>><?= $val->LookupLabel ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                            <span class="visible-print"><?= $lead->Segment ?></span>
                                        </div>
                                        <div id="CampaignDiv" class="form-group col-md-4">
                                            <label for="Campaign">Outbound Campaign</label><br/>
                                            <select class="form-control input-sm" name="Campaign">
                                                <option value=""></option>
                                                <?php
                                                foreach ($campaignValues as $val) {
                                                    ?>
                                                    <option
                                                        value="<?= $val->LookupValue ?>" <?= $val->LookupValue == $lead->Campaign ? 'SELECTED' : '' ?>><?= $val->LookupLabel ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                            <span class="visible-print"><?= $lead->Campaign ?></span>
                                        </div>
                                    </div>

                                    <div id="CustomerInfoDiv" class="row">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div id="HomeAddress1Div" class="form-group col-md-6">
                                                    <label class="infolabel" for="HomeAddress">Home Address</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="HomeAddress1"
                                                           value="<?= $lead->HomeAddress1 ?>" size="50"/>
                                                </div>
                                                <div id="HomeCityDiv" class="form-group col-md-6">
                                                    <label class="infolabel" for="HomeCity">City</label>
                                                    <input type="text" class="form-control input-sm" name="HomeCity"
                                                           value="<?= $lead->HomeCity ?>"/>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div id="HomeAddress2Div" class="form-group col-md-6">
                                                    <label class="infolabel" for="HomeAddress2">Address 2</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="HomeAddress2"
                                                           value="<?= $lead->HomeAddress2 ?>" size="50"/>
                                                </div>
                                                <div id="HomeStateDiv" class="form-group col-md-3">
                                                    <label class="infolabel" for="HomeState">State</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="HomeState"
                                                           value="<?= $lead->HomeState ?>" size="2"/>
                                                </div>
                                                <div id="HomeZipDiv" class="form-group col-md-3">
                                                    <label class="infolabel" for="HomeZip">Zip</label>
                                                    <input type="text" class="form-control input-sm" name="HomeZip"
                                                           value="<?= $lead->HomeZip ?>" size="10"/>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div id="PhoneDiv" class="form-group col-md-6">
                                                    <label class="infolabel" for="Phone">Home Phone</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="BorrowerPhone"
                                                           value="<?= $lead->Phone ?>"/>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div id="AnniversaryDiv" class="form-group col-md-6">
                                                    <label class="infolabel" for="Anniversary">Anniversary</label>
                                                    <input type="text" class="datepicker form-control input-sm"
                                                           id="Anniversary" name="Anniversary"
                                                           value="<?= (strtotime($lead->Anniversary) ? date("m/d/Y", strtotime($lead->Anniversary)) : '') ?>"/>
                                                </div>
                                                <div id="BirthdateDiv" class="form-group col-md-6">
                                                    <label for="Birthdate">Birthday</label>
                                                    <input type="text" class="datepicker form-control input-sm"
                                                           id="Birthdate" name="Birthdate"
                                                           value="<?= (strtotime($lead->Birthdate) ? date("Y", strtotime($lead->Birthdate)) : ""); ?>"/>
                                                <span
                                                    class="visible-print"><?= (strtotime($lead->Birthdate) ? date("Y", strtotime($lead->Birthdate)) : ""); ?></span>
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!-- End Details -->


                            <?php if ($optifinowGlobalSetting['data']['mortgage'] == TRUE): ?>

                                <!--Discovery-->
                                <?php if ($optifinowGlobalSetting['view']['discovery'] == TRUE): ?>
                                    <div role="tabpanel" class="tab-pane active" id="discovery">

                                        <div class="row General">
                                            <h4>General</h4>

                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <label class="text-right text-right col-lg-4" for="Supervisor">2nd
                                                        Voice Manager</label>

                                                    <div class="discoveryfield col-lg-8">
                                                        <input type="text"
                                                               class=" form-control input-sm"
                                                               name="SecondVoiceManager"
                                                               value="<?= $lead->SecondVoiceManager ?>" readonly>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--sectionHeader-->

                                            <div class="col-lg-4">
                                                <label class="infolabel text-right col-lg-4" for="ExternalID">Mailer
                                                    Code</label>

                                                <div class="col-lg-8" style="margin-bottom: 12px;">
                                                    <input type="text"
                                                           class=" form-control input-sm"
                                                           name="ExternalID"
                                                           value="<?= $lead->ExternalID ?>">
                                                </div>
                                            </div>

                                            <div class="col-lg-4">
                                                <label class="infolabel text-right col-lg-4" for="SecondVoiceQueue">2nd
                                                    Voice Queue</label>

                                                <div class="col-lg-8 discoveryfield" style="margin-bottom: 12px;">
                                                    <select id="SecondVoiceQueue" name="SecondVoiceQueue"/>
                                                    <option value=""></option>
                                                    <?php
                                                    foreach ($SecondVoiceQueue_lookups as $lt) {
                                                        ?>
                                                        <option
                                                            value="<?= $lt->LookupValue ?>" <?= $lt->LookupValue == $lead->SecondVoiceQueue ? "SELECTED" : "" ?>><?= $lt->LookupLabel ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-lg-8" style="margin-bottom: 30px;">
                                                <label class="infolabel col-lg-2">CAP URL</label>

                                                <div class="col-lg-10">
                                                    <?php if ($lead->LoanURL): ?>
                                                        <span class="loan-url"><a href="<?= $lead->LoanURL; ?>"
                                                                                  target="_blank"><?= $lead->LoanURL; ?></a></span>
                                                    <?php else: ?>
                                                        <span class="loan-url">N/A</span>
                                                    <?php endif; ?>
                                                </div>
                                            </div>


                                        </div>
                                        <!--General-->


                                        <div class="row sectionHeader RefIdInfo">
                                            <h4>Ref ID Info</h4>

                                            <div class="col-lg-4">

                                                <div class="form-group row">
                                                    <label class="text-right col-lg-4" for="">Applicant</label>

                                                    <div class="discoveryfield col-lg-8">
                                                        <div class="Applicant col-lg-6">
                                                            <input type="text"
                                                                   class=" form-control input-sm copyfield"
                                                                   name="FirstName"
                                                                   value="<?= $lead->FirstName ?>">
                                                        </div>
                                                        <div class="Applicant col-lg-6">
                                                            <input type="text"
                                                                   class=" form-control input-sm copyfield"
                                                                   name="LastName"
                                                                   value="<?= $lead->LastName ?>">
                                                        </div>

                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="text-right col-lg-4" for="">Co-Applicant</label>

                                                    <div class="discoveryfield col-lg-8">
                                                        <div class="Applicant col-lg-6">
                                                            <input type="text"
                                                                   class=" form-control input-sm copyfield"
                                                                   name="FirstName2"
                                                                   value="<?= $lead->FirstName2 ?>"
                                                                   placeholder="First Name">
                                                        </div>
                                                        <div class="Applicant col-lg-6">
                                                            <input type="text"
                                                                   class=" form-control input-sm copyfield"
                                                                   name="LastName2"
                                                                   value="<?= $lead->LastName2 ?>"
                                                                   placeholder="Last Name">
                                                        </div>

                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="text-right col-lg-4" for="">Address</label>

                                                    <div class="col-lg-8">
                                                        <input type="text"
                                                               class=" form-control input-sm copyfield"
                                                               name="Address1"
                                                               value="<?= $lead->Address1 ?>"></div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="text-right col-lg-4" for="">State</label>

                                                    <div class="col-lg-8">
                                                        <input type="text"
                                                               class=" form-control input-sm copyfield"
                                                               name="State"
                                                               value="<?= $lead->State ?>"></div>
                                                </div>
                                            </div>

                                            <div class="col-lg-4">
                                                <div class="form-group row">
                                                    <label class="text-right col-lg-4" for="DOB">DOB</label>

                                                    <div class="col-lg-8">
                                                        <input type="text"
                                                               class=" form-control input-sm dobcopy"
                                                               name=""
                                                               value="<?= strtotime($lead->DOB) ? date('m/d/Y', strtotime($lead->DOB)) : "" ?>"
                                                               readonly
                                                        ></div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="text-right col-lg-4" for="DOB">DOB</label>

                                                    <div class="col-lg-8">
                                                        <input type="text"
                                                               class=" form-control input-sm codobcopy"
                                                               name=""
                                                               value="<?= strtotime($lead->CoDOB) ? date('m/d/Y', strtotime($lead->CoDOB)) : "" ?>"
                                                               readonly
                                                        ></div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="text-right col-lg-4" for="">City</label>

                                                    <div class="col-lg-8">
                                                        <input type="text"
                                                               class=" form-control input-sm copyfield"
                                                               name="City"
                                                               value="<?= $lead->City ?>"></div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="text-right col-lg-4" for="">Zip</label>

                                                    <div class="col-lg-8">
                                                        <input type="text"
                                                               class=" form-control input-sm copyfield"
                                                               name="Zip"
                                                               value="<?= $lead->Zip ?>"></div>
                                                </div>
                                            </div>


                                            <div class="col-lg-4">

                                                <div class="form-group row">
                                                    <label class="infolabel col-lg-4 text-right"
                                                           for="Age">Age</label>

                                                    <div class="col-lg-3">
                                                        <input type="text"
                                                               class=" form-control input-sm agecompute"
                                                               placeholder="Age"
                                                               value=""
                                                               size="2" readonly></div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="infolabel col-lg-4 text-right"
                                                           for="Age">Age</label>

                                                    <div class="col-lg-3">
                                                        <input type="text"
                                                               class=" form-control input-sm coagecompute"
                                                               placeholder="Age"
                                                               value=""
                                                               size="2" readonly></div>
                                                </div>


                                                <div id="MaritalStatusDiv" class="form-group col-lg-12">
                                                    <label class="infolabel col-lg-4 text-right" for="MaritalStatus">Marital
                                                        Status</label>

                                                    <div class="col-lg-8">
                                                        <select id="MaritalStatus" name="MaritalStatus">
                                                            <option value=""></option>
                                                            <?php foreach ($maritalStatusLookUp as $ms) {
                                                                ?>
                                                                <option
                                                                    value="<?= $ms->LookupValue ?>" <?= $ms->LookupValue == $lead->MaritalStatus ? "SELECTED" : "" ?>><?= $ms->LookupLabel ?></option>
                                                                <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                        <!--RefIdInfo-->


                                        <div class="ContactInfo row">
                                            <h4>Contact Info</h4>

                                            <h5 class="col-lg-4"><label for="Phone">Borrower</label></h5>
                                            <h5 class="col-lg-8"><label for="Phone">Co-Borrower</label></h5>

                                            <div class="col-lg-4">

                                                <div class="form-group row">
                                                    <label class="text-right col-lg-4" for="Phone">Home Number</label>

                                                    <div class="col-lg-8">
                                                        <input type="text"
                                                               class=" form-control input-sm copyfield"
                                                               name="Phone"
                                                               value="<?= $lead->Phone ?>">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="text-right col-lg-4" for="Phone2">Cell Number</label>

                                                    <div class="col-lg-8">
                                                        <input type="text"
                                                               class=" form-control input-sm copyfield"
                                                               name="Phone2"
                                                               value="<?= $lead->Phone2 ?>"></div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="text-right col-lg-4" for="WorkPhone">Work
                                                        Number</label>

                                                    <div class="col-lg-8">
                                                        <input type="text"
                                                               class=" form-control input-sm copyfield"
                                                               name="WorkPhone"
                                                               value="<?= $lead->WorkPhone ?>"></div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="text-right col-lg-4" for="Email">Email Address</label>

                                                    <div class="col-lg-8">
                                                        <input type="text"
                                                               class=" form-control input-sm copyfield"
                                                               name="Email"
                                                               value="<?= $lead->Email ?>"></div>
                                                </div>
                                            </div>


                                            <div class="col-lg-4">


                                                <div class="form-group row">
                                                    <label class="text-right col-lg-4" for="Phone">Home Number</label>

                                                    <div class="col-lg-8">
                                                        <input type="text"
                                                               class=" form-control input-sm"
                                                               name="CoPhone"
                                                               value="<?= $lead->CoPhone ?>">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="text-right col-lg-4" for="Phone2">Cell Number</label>

                                                    <div class="col-lg-8">
                                                        <input type="text"
                                                               class=" form-control input-sm"
                                                               name="CoPhone2"
                                                               value="<?= $lead->CoPhone2 ?>"></div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="text-right col-lg-4" for="WorkPhone">Work
                                                        Number</label>

                                                    <div class="col-lg-8">
                                                        <input type="text"
                                                               class=" form-control input-sm"
                                                               name="CoWorkPhone"
                                                               value="<?= $lead->CoWorkPhone ?>"></div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="text-right col-lg-4" for="Email">Email Address</label>

                                                    <div class="col-lg-8">
                                                        <input type="text"
                                                               class=" form-control input-sm"
                                                               name="CoEmail"
                                                               value="<?= $lead->CoEmail ?>"></div>
                                                </div>
                                            </div>

                                            <div class="col-lg-4">

                                                <div class="form-group row">
                                                    <label class="BestTimeToCalldiv text-right col-lg-6"
                                                           for="ContactTime">Best time to call</label>

                                                    <div class="col-lg-6">
                                                        <input type="text"
                                                               class=" form-control input-sm"
                                                               name="ContactTime"
                                                               value="<?= $lead->ContactTime; ?>"></div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="OnTheHomeNumberDiv infolabel text-right col-lg-6"
                                                           for="ContactOn">On</label>

                                                    <div id="ContactOnDiv" class="col-lg-6">
                                                        <select name="ContactOn" id="ContactOn">
                                                            <option value=""></option>
                                                            <?php
                                                            foreach ($OnTheHomeNumber_lookups as $at) {
                                                                ?>
                                                                <option
                                                                    value="<?= $at->LookupValue ?>" <?= $at->LookupValue == $lead->ContactOn ? "SELECTED" : "" ?>><?= $at->LookupLabel ?></option>
                                                                <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="TimezoneDiv infolabel text-right col-lg-6"
                                                           for="Timezone">Timezone</label>

                                                    <div id="TimezoneDiv" class="row col-lg-6">
                                                        <select name="ContactTimezone" id="ContactTimezone">
                                                            <option value=""></option>
                                                            <?php
                                                            foreach ($Timezone_lookups as $at) {
                                                                ?>
                                                                <option
                                                                    value="<?= $at->LookupValue ?>" <?= $at->LookupValue == $lead->ContactTimezone ? "SELECTED" : "" ?>><?= $at->LookupLabel ?></option>
                                                                <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>

                                        <div class="row sectionHeader LoanPurpose">

                                            <h4 class="col-lg-12">Loan Purpose</h4>


                                            <div class="row">

                                                <div class="col-lg-4">

                                                    <label class="infolabel col-lg-6 text-right" for="LoanPurpose">Reason
                                                        for Loan</label>

                                                    <div id="LoanPurposeDiv" class="form-group row col-lg-6">
                                                        <select name="LoanPurpose" id="LoanPurpose">
                                                            <option value=""></option>
                                                            <?php
                                                            foreach ($PurposeOfLoan_lookups as $at) {
                                                                ?>
                                                                <option
                                                                    value="<?= $at->LookupValue ?>" <?= $at->LookupValue == $lead->LoanPurpose ? "SELECTED" : "" ?>><?= $at->LookupLabel ?></option>
                                                                <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="text-right col-lg-6" for="">Estimated Credit
                                                            Debt</label>

                                                        <div class="col-lg-6">
                                                            <input type="text"
                                                                   class=" form-control input-sm"
                                                                   name="Mortgage1Balance"
                                                                   value="<?= $lead->Mortgage1Balance ? $lead->Mortgage1Balance : "" ?>">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="text-right col-lg-6" for="">Monthly
                                                            Payment</label>

                                                        <div class="col-lg-6">
                                                            <input type="text"
                                                                   class=" form-control input-sm"
                                                                   name="OtherDebt1Payment"
                                                                   value="<?= $lead->OtherDebt1Payment ? $lead->OtherDebt1Payment : "" ?>">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="text-right col-lg-6" for="PaymentYears">Years
                                                            Making
                                                            Payments</label>

                                                        <div class="col-lg-6">
                                                            <input type="text"
                                                                   class=" form-control input-sm"
                                                                   name="PaymentYears"
                                                                   value="<?= $lead->PaymentYears; ?>"></div>
                                                    </div>


                                                </div>


                                                <div class="col-lg-4">

                                                    <div class="form-group row">
                                                        <label for=""><em>CREDIT CARD PAYMENTS</em></label>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="infolabel col-lg-6 text-right"
                                                               for="StillCharging">Are You Still Charging?</label>

                                                        <div id="AreYouStillChargingDiv" class="form-group col-lg-6">
                                                            <select name="StillCharging" id="StillCharging">
                                                                <option value=""></option>
                                                                <?php
                                                                foreach ($AreYouStillCharging_lookups as $at) {
                                                                    ?>
                                                                    <option
                                                                        value="<?= $at->LookupValue ?>" <?= $at->LookupValue == $lead->StillCharging ? "SELECTED" : "" ?>><?= $at->LookupLabel ?></option>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="text-right col-lg-6" for="">Monthly
                                                            Charges</label>

                                                        <div class="col-lg-6">
                                                            <input type="text"
                                                                   class=" form-control input-sm"
                                                                   name="MonthlyCharges"
                                                                   value="<?= $lead->MonthlyCharges ? $lead->MonthlyCharges : ""; ?>">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="text-right col-lg-6" for="">Any Balance
                                                            Transfers? </label>

                                                        <div class="col-lg-6">
                                                            <label
                                                                class="radio_label <?= $lead->BalanceTransfer == '1' ? '' : 'no_print'; ?>">Yes</label><input
                                                                type="radio" name="BalanceTransfer"
                                                                id="BalanceTransfer_Yes"
                                                                value="1" <?= $lead->BalanceTransfer == '1' ? 'CHECKED' : ''; ?> />
                                                            <label
                                                                class="radio_label <?= $lead->BalanceTransfer != '1' ? '' : 'no_print'; ?>">No</label><input
                                                                type="radio" name="BalanceTransfer"
                                                                id="BalanceTransfer_No"
                                                                value="0" <?= $lead->BalanceTransfer != '1' ? 'CHECKED' : ''; ?> />
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="row col-lg-12">
                                                    <em>We offer different loan terms, so in order to ensure we are
                                                        putting you into a loan thats affordable, please help me better
                                                        understand your:
                                                    </em>
                                                </div>

                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <label class="text-right col-lg-6" for="PayoffGoal">Payoff
                                                            Goals</label>

                                                        <div class="col-lg-6">
                                                            <input type="text"
                                                                   class=" form-control input-sm"
                                                                   name="PayoffGoal"
                                                                   value="<?= $lead->PayoffGoal ? $lead->PayoffGoal : ""; ?>">
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-4">
                                                        <div class="form-group row">
                                                            <label class="text-right col-lg-6" for="PaymentGoal">Payment
                                                                Goal</label>

                                                            <div class="col-lg-6">
                                                                <input type="text"
                                                                       class=" form-control input-sm"
                                                                       name="PaymentGoal"
                                                                       value="<?= $lead->PaymentGoal ? $lead->PaymentGoal : ""; ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            <!--LoanPurpose-->

                                            <div class="row sectionHeader"><!--Employment-->
                                                <h4>Employment</h4>


                                                <div class="col-lg-4">

                                                    <div class="form-group row">
                                                        <label class="infolabel col-lg-5 text-right" for="Employment">Employment
                                                            Status</label>

                                                        <div id="EmploymentDiv" class="form-group row col-lg-7">
                                                            <select name="Employment" id="Employment">
                                                                <option value=""></option>
                                                                <?php
                                                                foreach ($Employment_lookups as $at) {
                                                                    ?>
                                                                    <option
                                                                        value="<?= $at->LookupValue ?>" <?= $at->LookupValue == $lead->Employment ? "SELECTED" : "" ?>><?= $at->LookupLabel ?></option>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="text-right col-lg-4"
                                                               for="EmployerName">Employer</label>

                                                        <div class="col-lg-8">
                                                            <input type="text" name="EmployerName"
                                                                   class=" form-control input-sm"
                                                                   value="<?= $lead->EmployerName; ?>"></div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="text-right col-lg-4" for="Title">Position</label>

                                                        <div class="col-lg-8">
                                                            <input type="text" name="Occupation"
                                                                   class=" form-control input-sm"
                                                                   value="<?= $lead->Occupation; ?>"></div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="text-right col-lg-4" for="">Net Income</label>

                                                        <div class="col-lg-8">
                                                            <input type="text"
                                                                   class=" form-control input-sm netcompute"
                                                                   name="GMI"
                                                                   value="<?= $lead->GMI ? $lead->GMI : "" ?>"></div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="text-right col-lg-4" for="OtherIncome">Other
                                                            Income</label>

                                                        <div class="col-lg-8">
                                                            <input type="text"
                                                                   class=" form-control input-sm othercompute"
                                                                   name="OtherIncome"
                                                                   value="<?= $lead->OtherIncome ? $lead->OtherIncome : ""; ?>">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="text-right col-lg-4" for="">Source</label>

                                                        <div class="col-lg-8">
                                                            <input type="text"
                                                                   class=" form-control input-sm"
                                                                   name="IncomeSource"
                                                                   value="<?= $lead->IncomeSource; ?>"></div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="text-right col-lg-4" for="">Total Net
                                                            Income</label>

                                                        <div class="col-lg-8">
                                                            <input type="text"
                                                                   class=" form-control input-sm totalcompute"
                                                                   name=""
                                                                   value=""" readonly>
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="col-lg-4">

                                                    <div class="form-group row">
                                                        <label class="infolabel col-lg-5 text-right" for="CoEmployment">Employment
                                                            Status</label>

                                                        <div id="CoEmploymentDiv" class="form-group row col-lg-7">
                                                            <select name="CoEmployment" id="CoEmployment">
                                                                <option value=""></option>
                                                                <?php
                                                                foreach ($CoEmployment_lookups as $at) {
                                                                    ?>
                                                                    <option
                                                                        value="<?= $at->LookupValue ?>" <?= $at->LookupValue == $lead->CoEmployment ? "SELECTED" : "" ?>><?= $at->LookupLabel ?></option>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="text-right col-lg-4"
                                                               for="CoEmployerName">Employer</label>

                                                        <div class="col-lg-8">
                                                            <input type="text"
                                                                   class=" form-control input-sm"
                                                                   name="CoEmployerName"
                                                                   value="<?= $lead->CoEmployerName; ?>">
                                                        </div>
                                                    </div>


                                                    <div class="form-group row">
                                                        <label class="text-right col-lg-4"
                                                               for="CoOccupation">Position</label>

                                                        <div class="col-lg-8">
                                                            <input type="text"
                                                                   class=" form-control input-sm"
                                                                   name="CoOccupation"
                                                                   value="<?= $lead->CoOccupation; ?>"></div>
                                                    </div>


                                                    <div class="form-group row">
                                                        <label class="text-right col-lg-4" for="">Net Income</label>

                                                        <div class="col-lg-8">
                                                            <input type="text"
                                                                   class=" form-control input-sm conetcompute"
                                                                   name="CoGMI"
                                                                   value="<?= $lead->CoGMI ? $lead->CoGMI : "" ?>">
                                                        </div>
                                                    </div>


                                                    <div class="form-group row">
                                                        <label class="text-right col-lg-4" for="OtherIncome2">Other
                                                            Income</label>

                                                        <div class="col-lg-8">
                                                            <input type="text"
                                                                   class=" form-control input-sm coothercompute"
                                                                   name="CoOtherIncome"
                                                                   value="<?= $lead->CoOtherIncome ? $lead->CoOtherIncome : ""; ?>">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="text-right col-lg-4" for="">Source</label>

                                                        <div class="col-lg-8">
                                                            <input type="text"
                                                                   class=" form-control input-sm"
                                                                   name="CoIncomeSource"
                                                                   value="<?= $lead->CoIncomeSource; ?>">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="text-right col-lg-4" for="">Total Net
                                                            Income</label>

                                                        <div class="col-lg-8">
                                                            <input type="text"
                                                                   class=" form-control input-sm cototalcompute"
                                                                   name=""
                                                                   value="" readonly></div>
                                                    </div>
                                                </div>

                                            </div>
                                            <!--Employment-->

                                            <div class="row sectionHeader"><!--Residency-->
                                                <h4>Residency</h4>

                                                <div class="col-lg-4">
                                                    <div class="form-group row">
                                                        <label class="infolabel col-lg-4 text-right"
                                                               for="Occupancy"><em>Occupancy</em></label>

                                                        <div id="OccupancyDiv" class="form-group col-lg-8">
                                                            <select id="Occupancy" name="Occupancy"
                                                                    title="Occupancy">
                                                                <option value=""></option>
                                                                <option
                                                                    value="Own" <?= "Own" == $lead->Occupancy ? "SELECTED" : "" ?>>
                                                                    Own
                                                                </option>
                                                                <option
                                                                    value="Rent" <?= "Rent" == $lead->Occupancy ? "SELECTED" : "" ?>>
                                                                    Rent
                                                                </option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="text-right col-lg-4" for="MarketValue">Property
                                                            Value</label>

                                                        <div class="col-lg-8">
                                                            <input type="text"
                                                                   class=" form-control input-sm"
                                                                   name="MarketValue"
                                                                   value="<?= $lead->MarketValue ? $lead->MarketValue : ""; ?>">
                                                        </div>
                                                    </div>


                                                    <div class="form-group row">
                                                        <label class="text-right col-lg-4"
                                                               for="Mortgage1Balance">Balance</label>

                                                        <div class="col-lg-8">
                                                            <input type="text" name="Balance"
                                                                   class=" form-control input-sm"
                                                                   value="<?= $lead->Balance ? $lead->Balance : ""; ?>">
                                                        </div>
                                                    </div>

                                                </div>


                                                <div class="col-lg-4">
                                                    <div class="form-group row">
                                                        <label class="text-right col-lg-4" for="Mortgage1Payment">Housing
                                                            Payment</label>

                                                        <div class="col-lg-8">
                                                            <input type="text"
                                                                   class=" form-control input-sm"
                                                                   name="Mortgage1Payment"
                                                                   value="<?= $lead->Mortgage1Payment ? $lead->Mortgage1Payment : ""; ?>">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="infolabel col-lg-4 text-right" for="LoanType">Loan
                                                            Type</label>

                                                        <div id="LoanTypeDiv" class="form-group col-lg-8">
                                                            <select id="LoanType" name="LoanType"/>
                                                            <?php
                                                            foreach ($loanType_lookups as $lt) {
                                                                ?>
                                                                <option
                                                                    value="<?= $lt->LookupValue ?>" <?= $lt->LookupValue == $lead->LoanType ? "SELECTED" : "" ?>><?= $lt->LookupLabel ?></option>
                                                                <?php
                                                            }
                                                            ?>
                                                            </select>
                                                        </div>
                                                    </div>

                                                </div>

                                            </div>
                                            <!--Residency-->

                                            <div class="row sectionHeader"><!--Credit-->
                                                <h4>Credit</h4>

                                                <div class="col-lg-4">
                                                    <div class="form-group row">
                                                        <label class="text-right col-lg-4" for="">Applicant</label>

                                                        <div class="discoveryfield col-lg-8">
                                                            <div class="Applicant col-lg-6">
                                                                <input type="text"
                                                                       class=" form-control input-sm copyfield"
                                                                       name="FirstName"
                                                                       value="<?= $lead->FirstName ?>">
                                                            </div>
                                                            <div class="Applicant col-lg-6">
                                                                <input type="text"
                                                                       class=" form-control input-sm copyfield"
                                                                       name="LastName"
                                                                       value="<?= $lead->LastName ?>">
                                                            </div>

                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="text-right col-lg-4" for="">Co-Applicant</label>

                                                        <div class="discoveryfield col-lg-8">
                                                            <div class="Applicant col-lg-6">
                                                                <input type="text"
                                                                       class=" form-control input-sm copyfield"
                                                                       name="FirstName2"
                                                                       value="<?= $lead->FirstName2 ?>"
                                                                       placeholder="First Name">
                                                            </div>
                                                            <div class="Applicant col-lg-6">
                                                                <input type="text"
                                                                       class=" form-control input-sm copyfield"
                                                                       name="LastName2"
                                                                       value="<?= $lead->LastName2 ?>"
                                                                       placeholder="Last Name">
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-lg-4">
                                                    <div class="form-group row">
                                                        <label class="infolabel col-lg-4 text-right"
                                                               for="DOB">DOB</label>

                                                        <div class="col-lg-8">
                                                            <input type="text"
                                                                   class=" form-control input-sm birth-datepicker dobcompute"
                                                                   name="DOB"
                                                                   value="<?= strtotime($lead->DOB) ? date('m/d/Y', strtotime($lead->DOB)) : "" ?>">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="infolabel col-lg-4 text-right"
                                                               for="CoDOB">DOB</label>

                                                        <div class="col-lg-8">
                                                            <input type="text"
                                                                   class=" form-control input-sm birth-datepicker codobcompute"
                                                                   name="CoDOB"
                                                                   value="<?= strtotime($lead->CoDOB) ? date('m/d/Y', strtotime($lead->CoDOB)) : "" ?>">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-lg-4">

                                                    <div class="form-group row">
                                                        <label class="text-right col-lg-6" for="Info">Social
                                                            Security</label>

                                                        <div class="col-lg-6">
                                                            <input type="text"
                                                                   class=" form-control input-sm"
                                                                   name="Info"
                                                                   value="<?= $lead->Info ?>" size="12"></div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="text-right col-lg-6" for="CoInfo">Social
                                                            Security</label>

                                                        <div class="col-lg-6">
                                                            <input type="text"
                                                                   class=" form-control input-sm"
                                                                   name="CoInfo"
                                                                   value="<?= $lead->CoInfo; ?>" size="12"></div>
                                                    </div>

                                                </div>

                                            </div>
                                            <!--Credit-->


                                        </div>
                                    </div><!-- End Discovery -->


                                <?php else: ?>

                                    <!--Mortgage -->
                                    <div role="tabpanel" class="tab-pane" id="mortgage">

                                        <div class="sectionHeader">
                                            <h4>I. Borrower Information</h4>
                                        </div>
                                        <div id="Mortgage-I" class="sectionDiv">
                                            <div class="row">
                                                <h5>Borrower</h5>
                                            </div>
                                            <div class="row">
                                                <div id="BorrowerFirstNameDiv" class="form-group col-md-4">
                                                    <label class="infolabel" for="BorrowerFirstName">First Name</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="BorrowerFirstName"
                                                           value="<?= $lead->FirstName ?>" readonly/>
                                                </div>
                                                <div id="BorrowerLastNameDiv" class="form-group col-md-4">
                                                    <label class="infolabel" for="BorrowerLastName">Last Name</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="BorrowerLastName"
                                                           value="<?= $lead->LastName ?>" readonly/>
                                                </div>
                                                <div id="DOBDiv" class="form-group col-md-2">
                                                    <label class="infolabel" for="DOB">Birthdate</label>
                                                    <input type="text" class="form-control input-sm birth-datepicker"
                                                           name="DOB"
                                                           value="<?= $lead->DOB ?>" size="4"/>
                                                </div>
                                                <div id="InfoDiv" class="form-group col-md-2">
                                                    <label class="infolabel" for="Info">SSN</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="Info"
                                                           value="<?= $lead->Info ?>" size="12"/>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div id="BorrowerAddress1Div" class="form-group col-md-4">
                                                    <label class="infolabel" for="BorrowerAddress1">Street
                                                        Address</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="BorrowerAddress1"
                                                           value="<?= $lead->Address1 ?>" readonly/>
                                                </div>
                                                <div id="BorrowerCityDiv" class="form-group col-md-3">
                                                    <label class="infolabel" for="BorrowerCity">City</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="BorrowerCity"
                                                           value="<?= $lead->City ?>" readonly/>
                                                </div>
                                                <div id="BorrowerStateDiv" class="form-group col-md-2">
                                                    <label class="infolabel" for="BorrowerState">State</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="BorrowerState"
                                                           value="<?= $lead->State ?>" size="15" readonly/>
                                                </div>
                                                <div id="BorrowerZipDiv" class="form-group col-md-3">
                                                    <label class="infolabel" for="BorrowerZip">Zip</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="BorrowerZip"
                                                           value="<?= $lead->Zip ?>" size="10" readonly/>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div id="BorrowerPhoneDiv" class="form-group col-md-3">
                                                    <label class="infolabel" for="BorrowerPhone">Home Phone</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="BorrowerPhone"
                                                           value="<?= $lead->Phone ?>" size="15" readonly/>
                                                </div>
                                                <div id="BorrowerPhone2Div" class="form-group col-md-3">
                                                    <label class="infolabel" for="BorrowerPhone2">Cell Phone</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="BorrowerPhone2"
                                                           value="<?= $lead->Phone2 ?>" size="15" readonly/>
                                                </div>
                                                <div id="ContactTimeDiv" class="form-group col-md-2">
                                                    <label class="infolabel" for="ContactTime">Contact Time</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="ContactTime"
                                                           value="<?= $lead->ContactTime ?>" size="15"/>
                                                </div>
                                                <div id="BorrowerEmailDiv" class="form-group col-md-4">
                                                    <label class="infolabel" for="BorrowerEmail">Email Address</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="BorrowerEmail"
                                                           value="<?= $lead->Email ?>" readonly/>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div id="MaritalStatusDiv" class="form-group">
                                                    <label class="infolabel" for="MaritalStatus">Marital Status</label>
                                                    <select id="MaritalStatus" name="MaritalStatus">
                                                        <option value=""></option>
                                                        <?php foreach ($maritalStatusLookUp as $ms) {
                                                            ?>
                                                            <option
                                                                value="<?= $ms->LookupValue ?>" <?= $ms->LookupValue == $lead->MaritalStatus ? "SELECTED" : "" ?>><?= $ms->LookupLabel ?></option>
                                                            <?php
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div id="SpouseFirstNameDiv" class="form-group col-md-3">
                                                    <label class="infolabel" for="SpouseFirstName">Spouse First
                                                        Name</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="SpouseFirstName"
                                                           value="<?= $lead->SpouseFirstName ?>"/>
                                                </div>
                                                <div id="SpouseLastNameDiv" class="form-group col-md-3">
                                                    <label class="infolabel" for="SpouseLastName">Spouse Last
                                                        Name</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="SpouseLastName"
                                                           value="<?= $lead->SpouseLastName ?>"/>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <h5>Co-Borrower</h5>
                                            </div>
                                            <div class="row">
                                                <div id="FirstName2Div" class="form-group col-md-4">
                                                    <label class="infolabel" for="FirstName2">First Name</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="FirstName2"
                                                           value="<?= $lead->FirstName2 ?>"/>
                                                </div>
                                                <div id="LastName2Div" class="form-group col-md-4">
                                                    <label class="infolabel" for="LastName2">Last Name</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="LastName2"
                                                           value="<?= $lead->LastName2 ?>"/>
                                                </div>
                                                <div id="CoDOBDiv" class="form-group col-md-2">
                                                    <label class="infolabel" for="CoDOB">Birthdate</label>
                                                    <input type="text" class="form-control input-sm birth-datepicker"
                                                           name="CoDOB"
                                                           value="<?= $lead->CoDOB ?>" size="4"/>
                                                </div>
                                                <div id="CoInfoDiv" class="form-group col-md-2">
                                                    <label class="infolabel" for="CoInfo">SSN</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="CoInfo"
                                                           value="<?= $lead->CoInfo ?>" size="12"/>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div id="CoAddressDiv" class="form-group col-md-4">
                                                    <label class="infolabel" for="CoAddress">Street Address</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="CoAddress"
                                                           value="<?= $lead->CoAddress ?>"/>
                                                </div>
                                                <div id="CoCityDiv" class="form-group col-md-3">
                                                    <label class="infolabel" for="CoCity">City</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="CoCity"
                                                           value="<?= $lead->CoCity ?>"/>
                                                </div>
                                                <div id="CoStateDiv" class="form-group col-md-2">
                                                    <label class="infolabel" for="CoState">State</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="CoState"
                                                           value="<?= $lead->CoState ?>" size="15"/>
                                                </div>
                                                <div id="CoZipDiv" class="form-group col-md-3">
                                                    <label class="infolabel" for="CoZip">Zip</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="CoZip"
                                                           value="<?= $lead->CoZip ?>" size="10"/>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="sectionHeader">
                                            <h4>II. Employment Information</h4>
                                        </div>
                                        <div id="Mortgage-II" class="sectionDiv">
                                            <div class="row">
                                                <h5>Borrower</h5>
                                            </div>
                                            <div class="row">
                                                <div id="EmployerNameDiv" class="form-group col-md-5">
                                                    <label class="infolabel" for="EmployerName">Employer's Name</label>
                                                    <input type="text" class="form-control input-sm"
                                                           value="<?= $lead->Company ?>" size="50" placeholder="Company"
                                                           readonly/>
                                                </div>
                                                <div id="OccupationDiv" class="form-group col-md-4">
                                                    <label class="infolabel" for="Occupation">Position/Title</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="Occupation"
                                                           value="<?= $lead->Occupation ?>"/>
                                                </div>
                                                <div id="TimeOnJobDiv" class="form-group col-md-3">
                                                    <label class="infolabel" for="TimeOnJob">Years at Job</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="TimeOnJob"
                                                           value="<?= $lead->TimeOnJob ?>" size="4"/>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div id="EmployerAddressDiv" class="form-group col-md-4">
                                                    <label class="infolabel" for="EmployerAddress">Employer
                                                        Address</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="EmployerAddress"
                                                           value="<?= $lead->EmployerAddress ?>" size="40"/>
                                                </div>
                                                <div id="EmployerCityDiv" class="form-group col-md-3">
                                                    <label class="infolabel" for="EmployerCity">City</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="EmployerCity"
                                                           value="<?= $lead->EmployerCity ?>" size="40"/>
                                                </div>
                                                <div id="EmployerStateDiv" class="form-group col-md-2">
                                                    <label class="infolabel" for="EmployerState">State</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="EmployerState"
                                                           value="<?= $lead->EmployerState ?>" size="15"/>
                                                </div>
                                                <div id="EmployerZipDiv" class="form-group col-md-3">
                                                    <label class="infolabel" for="EmployerZip">Zip</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="EmployerZip"
                                                           value="<?= $lead->EmployerZip ?>" size="10"/>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div id="EmployerWorkPhoneDiv" class="form-group col-md-3">
                                                    <label class="infolabel" for="EmployerWorkPhone">Work Phone</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="EmployerWorkPhone"
                                                           value="<?= $lead->WorkPhone ?>" size="15" readonly/>
                                                </div>
                                                <div id="BFSDiv" class="form-group col-md-3">
                                                    <label class="infolabel" for="BFS">Self Employed</label><br>
                                                    <label
                                                        class="radio_label <?= $lead->BFS == '1' ? '' : 'no_print'; ?>">Yes</label><input
                                                        type="radio" name="BFS" id="BFS_Yes"
                                                        value="1" <?= $lead->BFS == '1' ? 'CHECKED' : ''; ?> />
                                                    <label
                                                        class="radio_label <?= $lead->BFS != '1' ? '' : 'no_print'; ?>">No</label><input
                                                        type="radio" name="BFS" id="BFS_No"
                                                        value="0" <?= $lead->BFS != '1' ? 'CHECKED' : ''; ?> />
                                                </div>
                                            </div>
                                            <div class="row">
                                                <h5>Co-Borrower</h5>
                                            </div>
                                            <div class="row">
                                                <div id="CoEmployerNameDiv" class="form-group col-md-5">
                                                    <label class="infolabel" for="CoEmployerName">Co-Borrower Employer's
                                                        Name</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="CoEmployerName"
                                                           value="<?= $lead->CoEmployerName ?>" size="50"/>
                                                </div>
                                                <div id="CoOccupationDiv" class="form-group col-md-4">
                                                    <label class="infolabel" for="CoOccupation">Position/Title</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="CoOccupation"
                                                           value="<?= $lead->CoOccupation ?>"/>
                                                </div>
                                                <div id="CoTimeOnJobDiv" class="form-group col-md-3">
                                                    <label class="infolabel" for="CoTimeOnJob">Years at Job</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="CoTimeOnJob"
                                                           value="<?= $lead->CoTimeOnJob ?>" size="4"/>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div id="CoEmployerAddressDiv" class="form-group col-md-4">
                                                    <label class="infolabel" for="CoEmployerAddress">Employer
                                                        Address</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="CoEmployerAddress"
                                                           value="<?= $lead->CoEmployerAddress ?>" size="40"/>
                                                </div>
                                                <div id="CoEmployerCityDiv" class="form-group col-md-3">
                                                    <label class="infolabel" for="CoEmployerCity">City</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="CoEmployerCity"
                                                           value="<?= $lead->CoEmployerCity ?>" size="40"/>
                                                </div>
                                                <div id="CoEmployerStateDiv" class="form-group col-md-2">
                                                    <label class="infolabel" for="CoEmployerState">State</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="CoEmployerState"
                                                           value="<?= $lead->CoEmployerState ?>" size="15"/>
                                                </div>
                                                <div id="CoEmployerZipDiv" class="form-group col-md-3">
                                                    <label class="infolabel" for="CoEmployerZip">Zip</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="CoEmployerZip"
                                                           value="<?= $lead->CoEmployerZip ?>" size="10"/>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div id="CoWorkPhoneDiv" class="form-group col-md-3">
                                                    <label class="infolabel" for="CoWorkPhone">Work Phone</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="CoWorkPhone"
                                                           value="<?= $lead->CoWorkPhone ?>" size="15"/>
                                                </div>
                                                <div id="CoBFSDiv" class="form-group col-md-3">
                                                    <label class="infolabel" for="CoBFS">Self Employed</label><br>
                                                    <label
                                                        class="radio_label <?= $lead->CoBFS == '1' ? '' : 'no_print'; ?>">Yes</label><input
                                                        type="radio" name="CoBFS" id="CoBFS_Yes"
                                                        value="1" <?= $lead->CoBFS == '1' ? 'CHECKED' : ''; ?> />
                                                    <label
                                                        class="radio_label <?= $lead->CoBFS != '1' ? '' : 'no_print'; ?>">No</label><input
                                                        type="radio" name="CoBFS" id="CoBFS_No"
                                                        value="0" <?= $lead->CoBFS != '1' ? 'CHECKED' : ''; ?> />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="sectionHeader">
                                            <h4>III. Monthly Income &amp; Combined Housing Expenses</h4>
                                        </div>
                                        <div id="Mortgage-III" class="sectionDiv">
                                            <div class="row">
                                                <h5>Income</h5>
                                            </div>
                                            <div class="row">
                                                <div id="GMIDiv" class="form-group col-md-3">
                                                    <label class="infolabel" for="GMI">Borrower GMI</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="GMI"
                                                           value="<?= $lead->GMI ?>" size="10"/>
                                                </div>
                                                <div id="CoGMIDiv" class="form-group col-md-3">
                                                    <label class="infolabel" for="GMI">Borrower GMI</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="GMI"
                                                           value="<?= $lead->GMI ?>" size="10"/>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <h5>Expenses</h5>
                                            </div>
                                            <div class="row">
                                                <div id="Mortgage1PaymentDiv" class="form-group col-md-3">
                                                    <label class="infolabel" for="Mortgage1Payment">First
                                                        Mortgage</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="Mortgage1Payment"
                                                           value="<?= $lead->Mortgage1Payment ?>" size="15"/>
                                                </div>
                                                <div id="Mortgage2PaymentDiv" class="form-group col-md-3">
                                                    <label class="infolabel" for="Mortgage2Payment">Second
                                                        Mortgage</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="Mortgage2Payment"
                                                           value="<?= $lead->Mortgage2Payment ?>" size="15"/>
                                                </div>
                                                <div id="PropertyTaxesDiv" class="form-group col-md-3">
                                                    <label class="infolabel" for="PropertyTaxes">Property Taxes</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="PropertyTaxes"
                                                           value="<?= $lead->PropertyTaxes ?>" size="15"/>
                                                </div>
                                                <div id="AnnualInsuranceDiv" class="form-group col-md-3">
                                                    <label class="infolabel" for="AnnualInsurance">Hazard
                                                        Insurance</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="AnnualInsurance"
                                                           value="<?= $lead->AnnualInsurance ?>" size="15"/>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div id="HOADuesDiv" class="form-group col-md-3">
                                                    <label class="infolabel" for="HOADues">HOA Dues</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="HOADues"
                                                           value="<?= $lead->HOADues ?>" size="15"/>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="sectionHeader">
                                            <h4>IV. Assets &amp; Liabilities</h4>
                                        </div>
                                        <div id="Mortgage-IV" class="sectionDiv">
                                            <div class="row">
                                                <h5>First Mortgage</h5>
                                            </div>
                                            <div class="row">
                                                <div id="Mortgage1LenderDiv" class="form-group col-md-3">
                                                    <label class="infolabel" for="Mortgage1Lender">Lender</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="Mortgage1Lender"
                                                           value="<?= $lead->Mortgage1Lender ?>" size="15"/>
                                                </div>
                                                <div id="ALMortgage1BalanceDiv" class="form-group col-md-3">
                                                    <label class="infolabel" for="ALMortgage1Balance">Balance</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="ALMortgage1Balance"
                                                           value="<?= $lead->Mortgage1Balance ?>" size="15" readonly/>
                                                </div>
                                                <div id="ALMortgage1RateDiv" class="form-group col-md-3">
                                                    <label class="infolabel" for="ALMortgage1Rate">Rate</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="ALMortgage1Rate"
                                                           value="<?= $lead->Mortgage1Rate ?>" size="15" readonly/>
                                                </div>
                                                <div id="Mortgage1TermDiv" class="form-group col-md-3">
                                                    <label class="infolabel" for="Mortgage1Term">Term</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="Mortgage1Term"
                                                           value="<?= $lead->Mortgage1Term ?>" size="15"/>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <h5>Second Mortgage</h5>
                                            </div>
                                            <div class="row">
                                                <div id="Mortgage2LenderDiv" class="form-group col-md-3">
                                                    <label class="infolabel" for="Mortgage2Lender">Lender</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="Mortgage2Lender"
                                                           value="<?= $lead->Mortgage2Lender ?>" size="15"/>
                                                </div>
                                                <div id="Mortgage2BalanceDiv" class="form-group col-md-3">
                                                    <label class="infolabel" for="Mortgage2Balance">Balance</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="Mortgage2Balance"
                                                           value="<?= $lead->Mortgage2Balance ?>" size="15"/>
                                                </div>
                                                <div id="Mortgage2RateDiv" class="form-group col-md-3">
                                                    <label class="infolabel" for="Mortgage2Rate">Rate</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="Mortgage2Rate"
                                                           value="<?= $lead->Mortgage2Rate ?>" size="15"/>
                                                </div>
                                                <div id="Mortgage2TermDiv" class="form-group col-md-3">
                                                    <label class="infolabel" for="Mortgage2Term">Term</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="Mortgage2Term"
                                                           value="<?= $lead->Mortgage2Term ?>" size="15"/>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <h5>Other Current Debt</h5>
                                            </div>
                                            <div class="row">
                                                <div id="OtherDebt1PaymentDiv" class="form-group col-md-3">
                                                    <label class="infolabel" for="OtherDebt1Payment">Payment</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="OtherDebt1Payment"
                                                           value="<?= $lead->OtherDebt1Payment ?>" size="15"/>
                                                </div>
                                                <div id="OtherDebt2PaymentDiv" class="form-group col-md-3">
                                                    <label class="infolabel" for="OtherDebt2Payment">Payment</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="OtherDebt2Payment"
                                                           value="<?= $lead->OtherDebt2Payment ?>" size="15"/>
                                                </div>
                                                <div id="OtherDebt3PaymentDiv" class="form-group col-md-3">
                                                    <label class="infolabel" for="OtherDebt3Payment">Payment</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="OtherDebt3Payment"
                                                           value="<?= $lead->OtherDebt3Payment ?>" size="15"/>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div id="OtherDebt1BalanceDiv" class="form-group col-md-3">
                                                    <label class="infolabel" for="OtherDebt1Balance">Balance</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="OtherDebt1Balance"
                                                           value="<?= $lead->OtherDebt1Balance ?>" size="15"/>
                                                </div>
                                                <div id="OtherDebt2BalanceDiv" class="form-group col-md-3">
                                                    <label class="infolabel" for="OtherDebt2Balance">Balance</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="OtherDebt2Balance"
                                                           value="<?= $lead->OtherDebt2Balance ?>" size="15"/>
                                                </div>
                                                <div id="OtherDebt3BalanceDiv" class="form-group col-md-3">
                                                    <label class="infolabel" for="OtherDebt3Balance">Balance</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="OtherDebt3Balance"
                                                           value="<?= $lead->OtherDebt3Balance ?>" size="15"/>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <h5>Additional Information</h5>
                                            </div>
                                            <div class="row">
                                                <div id="FormerBankruptcyDiv" class="form-group col-md-3">
                                                    <label class="infolabel" for="FormerBankruptcy">Former
                                                        Bankruptcy</label><br>
                                                    <label
                                                        class="radio_label <?= $lead->FormerBankruptcy == '1' ? '' : 'no_print'; ?>">Yes</label><input
                                                        type="radio" name="FormerBankruptcy" id="FormerBankruptcy_Yes"
                                                        value="1" <?= $lead->FormerBankruptcy == '1' ? 'CHECKED' : ''; ?> />
                                                    <label
                                                        class="radio_label <?= $lead->FormerBankruptcy != '1' ? '' : 'no_print'; ?>">No</label><input
                                                        type="radio" name="FormerBankruptcy" id="FormerBankruptcy_No"
                                                        value="0" <?= $lead->FormerBankruptcy != '1' ? 'CHECKED' : ''; ?> />
                                                </div>
                                                <div id="JudgementsDiv" class="form-group col-md-3">
                                                    <label class="infolabel"
                                                           for="Judgements">Judgements/Liens</label><br>
                                                    <label
                                                        class="radio_label <?= $lead->Judgements == '1' ? '' : 'no_print'; ?>">Yes</label><input
                                                        type="radio" name="Judgements" id="Judgements_Yes"
                                                        value="1" <?= $lead->Judgements == '1' ? 'CHECKED' : ''; ?> />
                                                    <label
                                                        class="radio_label <?= $lead->Judgements != '1' ? '' : 'no_print'; ?>">No</label><input
                                                        type="radio" name="Judgements" id="Judgements_No"
                                                        value="0" <?= $lead->Judgements != '1' ? 'CHECKED' : ''; ?> />
                                                </div>
                                                <div id="BankruptcyTypeDiv" class="form-group col-md-3">
                                                    <label class="infolabel" for="BankruptcyType">Chapter</label>
                                                    <select name="BankruptcyType" id="BankruptcyType">
                                                        <option value=""></option>
                                                        <option
                                                            value="11" <?= $lead->BankruptcyType == '11' ? 'SELECTED' : ''; ?>>
                                                            11
                                                        </option>
                                                        <option
                                                            value="7" <?= $lead->BankruptcyType == '7' ? 'SELECTED' : ''; ?>>
                                                            7
                                                        </option>
                                                        <option
                                                            value="Foreclosure" <?= $lead->BankruptcyType == 'Foreclosure' ? 'SELECTED' : ''; ?>>
                                                            Foreclosure
                                                        </option>
                                                    </select>
                                                </div>
                                                <div id="BankruptcyYearDiv" class="form-group col-md-3">
                                                    <label class="infolabel" for="BankruptcyYear">Year Filed</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="BankruptcyYear"
                                                           value="<?= $lead->BankruptcyYear ?>" size="4"/>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div id="DownPaymentDiv" class="form-group col-md-3">
                                                    <label class="infolabel" for="DownPayment">Down Payment</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="DownPayment"
                                                           value="<?= $lead->DownPayment ?>" size="4"/>
                                                </div>
                                                <div id="AssetsAmountDiv" class="form-group col-md-3">
                                                    <label class="infolabel" for="AssetsAmount">Assets Amount</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="AssetsAmount"
                                                           value="<?= $lead->AssetsAmount ?>" size="4"/>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="sectionHeader">
                                            <h4>V. Property Information &amp; Purpose of Loan</h4>
                                        </div>
                                        <div id="Mortgage-V" class="sectionDiv">
                                            <div class="row">
                                                <div id="PropertyAddressDiv" class="form-group col-md-7">
                                                    <label class="infolabel" for="PropertyAddress">Street
                                                        Address</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="PropertyAddress"
                                                           value="<?= $lead->PropertyAddress ?>" size="50"/>
                                                </div>
                                                <div id="PropertyAddresDiv" class="form-group col-md-3">
                                                    <label class="infolabel" for="PropertyFOund">Property
                                                        Found</label><br>
                                                    <label
                                                        class="radio_label <?= $lead->PropertyFound == '1' ? '' : 'no_print'; ?>">Yes</label><input
                                                        type="radio" name="PropertyFound" id="PropertyFound_Yes"
                                                        value="1" <?= $lead->PropertyFound == '1' ? 'CHECKED' : ''; ?> />
                                                    <label
                                                        class="radio_label <?= $lead->PropertyFound != '1' ? '' : 'no_print'; ?>">No</label><input
                                                        type="radio" name="PropertyFound" id="PropertyFound_No"
                                                        value="0" <?= $lead->PropertyFound != '1' ? 'CHECKED' : ''; ?> />
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div id="PropertyCityDiv" class="form-group col-md-4">
                                                    <label class="infolabel" for="PropertyCity">City</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="PropertyCity"
                                                           value="<?= $lead->PropertyCity ?>" size="10"/>
                                                </div>
                                                <div id="PropertyStateDiv" class="form-group col-md-2">
                                                    <label class="infolabel" for="PropertyState">State</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="PropertyState"
                                                           value="<?= $lead->PropertyState ?>" size="2"/>
                                                </div>
                                                <div id="PropertyZipDiv" class="form-group col-md-2">
                                                    <label class="infolabel" for="PropertyZip">Zip</label>
                                                    <input type="text" class="form-control input-sm" name="PropertyZip"
                                                           value="<?= $lead->PropertyZip ?>" size="10"/>
                                                </div>
                                                <div id="PropertyCountyDiv" class="form-group col-md-4">
                                                    <label class="infolabel" for="PropertyCounty">County</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="PropertyCounty"
                                                           value="<?= $lead->PropertyCounty ?>" size="40"/>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div id="LoanPurposeDiv" class="form-group col-md-5">
                                                    <label class="infolabel" for="LoanPurpose">Purpose of Loan</label>
                                                    <select name="LoanPurpose" id="LoanPurpose">
                                                        <option value=""></option>
                                                        <?php
                                                        foreach ($PurposeOfLoan_lookups as $at) {
                                                            ?>
                                                            <option
                                                                value="<?= $at->LookupValue ?>" <?= $at->LookupValue == $lead->LoanPurpose ? "SELECTED" : "" ?>><?= $at->LookupLabel ?></option>
                                                            <?php
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div id="PurposeOfRefinanceDiv" class="form-group col-md-5">
                                                    <label class="infolabel" for="PurposeOfRefinance">Purpose of
                                                        Refinance</label>
                                                    <select name="PurposeOfRefinance" id="PurposeOfRefinance">
                                                        <option value=""></option>
                                                        <?php
                                                        foreach ($PurposeOfRefinance_lookups as $at) {
                                                            ?>
                                                            <option
                                                                value="<?= $at->LookupValue ?>" <?= $at->LookupValue == $lead->PurposeOfRefinance ? "SELECTED" : "" ?>><?= $at->LookupLabel ?></option>
                                                            <?php
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div id="PurchaseYearDiv" class="form-group col-md-3">
                                                    <label class="infolabel" for="PurchaseYear">Year Acquired</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="PurchaseYear"
                                                           value="<?= $lead->PurchaseYear ?>" size="4"/>
                                                </div>
                                                <div id="PurchasePriceDiv" class="form-group col-md-3">
                                                    <label class="infolabel" for="PurchasePrice">Original Cost</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="PurchasePrice"
                                                           value="<?= $lead->PurchasePrice ?>" size="15"/>
                                                </div>
                                                <div id="MarketValueDiv" class="form-group col-md-3">
                                                    <label class="infolabel" for="MarketValue">Present Value</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="MarketValue"
                                                           value="<?= $lead->MarketValue ?>" size="15"/>
                                                </div>
                                                <div id="NewHomeValueDiv" class="form-group col-md-3">
                                                    <label class="infolabel" for="NewHomeValue">New Home Value</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="NewHomeValue"
                                                           value="<?= $lead->NewHomeValue ?>" size="15"/>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div id="BedroomCountDiv" class="form-group col-md-3">
                                                    <label class="infolabel" for="BedroomCount">Bedroom Count</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="BedroomCount"
                                                           value="<?= $lead->BedroomCount ?>" size="2"/>
                                                </div>
                                                <div id="BathroomCountDiv" class="form-group col-md-3">
                                                    <label class="infolabel" for="BathroomCount">Bathroom Count</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="BathroomCount"
                                                           value="<?= $lead->BathroomCount ?>" size="2"/>
                                                </div>
                                                <div id="SquareFootageDiv" class="form-group col-md-3">
                                                    <label class="infolabel" for="SquareFootage">Sq. Footage</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="SquareFootage"
                                                           value="<?= $lead->SquareFootage ?>" size="10"/>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="sectionHeader">
                                            <h4>VI. Type of Mortgage &amp; Terms of Loan</h4>
                                        </div>
                                        <div id="Mortgage-VI" class="sectionDiv">
                                            <div class="row">
                                                <div id="LoanTypeDiv" class="form-group col-md-4">
                                                    <label class="infolabel" for="LoanType">Mortgage Applied For</label>
                                                    <select id="LoanType" name="LoanType"/>
                                                    <option value=""></option>
                                                    <?php
                                                    foreach ($loanType_lookups as $lt) {
                                                        ?>
                                                        <option
                                                            value="<?= $lt->LookupValue ?>" <?= $lt->LookupValue == $lead->LoanType ? "SELECTED" : "" ?>><?= $lt->LookupLabel ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                    </select>
                                                </div>

                                                <div id="PropertyTypeDiv" class="form-group col-md-4">
                                                    <label class="infolabel" for="PropertyType">Property will be</label>
                                                    <select id="PropertyType" name="PropertyType">
                                                        <option value=""></option>
                                                        <?php
                                                        foreach ($propertyType_lookups as $pt) {
                                                            ?>
                                                            <option
                                                                value="<?= $pt->LookupValue ?>" <?= $pt->LookupValue == $lead->PropertyType ? "SELECTED" : "" ?>><?= $pt->LookupLabel ?>
                                                            </option>
                                                            <?php
                                                        }
                                                        ?>
                                                    </select>
                                                </div>

                                                <div id="OwnerOccupiedDiv" class="form-group col-md-2">
                                                    <label class="infolabel" for="OwnerOccupied">Occupied</label>
                                                    <select id="OwnerOccupied" name="OwnerOccupied"
                                                            title="Do you intend to occupy the property?">
                                                        <option value=""></option>
                                                        <option
                                                            value="Yes" <?= "Yes" == $lead->OwnerOccupied ? "SELECTED" : "" ?>>
                                                            Yes
                                                        </option>
                                                        <option
                                                            value="No" <?= "No" == $lead->OwnerOccupied ? "SELECTED" : "" ?>>
                                                            No
                                                        </option>
                                                    </select>
                                                </div>
                                                <div id="LanguagePreferenceDiv" class="form-group col-md-2">
                                                    <label class="infolabel" for="LanguagePreference">Language</label>
                                                    <select name="LanguagePreference" id="LanguagePreference">
                                                        <?php
                                                        foreach ($language_lookups as $lu) {
                                                            ?>
                                                            <option
                                                                value="<?= $lu->LookupValue ?>" <?= $lu->LookupValue == $lead->LanguagePreference ? "SELECTED" : "" ?>><?= $lu->LookupLabel ?></option>
                                                            <?php
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div id="LoanNumberDiv" class="form-group col-md-3">
                                                    <label class="infolabel" for="LoanNumber">Loan Number</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="LoanNumber"
                                                           value="<?= $lead->LoanNumber ?>" size="50"/>
                                                </div>
                                                <div id="Mortgage1BalanceDiv" class="form-group col-md-3">
                                                    <label class="infolabel" for="Mortgage1Balance">Exist Loan
                                                        Amt.</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="Mortgage1Balance"
                                                           value="<?= $lead->Mortgage1Balance ?>"/>
                                                </div>
                                                <div id="CreditProfileDiv" class="form-group col-md-3">
                                                    <label class="infolabel" for="CreditProfile">Est. Credit
                                                        Score</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="CreditProfile"
                                                           value="<?= $lead->CreditProfile ?>"/>
                                                </div>
                                                <div id="Mortgage1RateDiv" class="form-group col-md-3">
                                                    <label class="infolabel" for="Mortgage1Rate">Interest Rate</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="Mortgage1Rate"
                                                           value="<?= $lead->Mortgage1Rate ?>" size="10"/>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div id="LoanAmountDiv" class="form-group col-md-3">
                                                    <label class="infolabel" for="LoanAmount">Loan Amount</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="LoanAmount"
                                                           value="<?= $lead->LoanAmount ?>"/>
                                                </div>
                                                <div id="CashOutDiv" class="form-group col-md-3">
                                                    <label class="infolabel" for="CashOut">Cash Out</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="CashOut"
                                                           value="<?= $lead->CashOut ?>"/>
                                                </div>
                                                <div id="LTVDiv" class="form-group col-md-3">
                                                    <label class="infolabel" for="LTV">LTV</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="LTV"
                                                           value="<?= $lead->LTV ?>" size="10"/>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div id="RateTypeDiv" class="form-group col-md-3">
                                                    <label class="infolabel" for="RateType">Amortization</label>
                                                    <select id="RateType" name="RateType">
                                                        <option value=""></option>
                                                        <?php
                                                        foreach ($AmortizationType_lookups as $at) {
                                                            ?>
                                                            <option
                                                                value="<?= $at->LookupValue ?>" <?= $at->LookupValue == $lead->RateType ? "SELECTED" : "" ?>><?= $at->LookupLabel ?></option>
                                                            <?php
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div id="LoanTimeframeDiv" class="form-group col-md-3">
                                                    <label class="infolabel" for="LoanTimeframe">Number of
                                                        Months</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="LoanTimeframe"
                                                           value="<?= $lead->LoanTimeframe ?>"/>
                                                </div>
                                                <div id="FHALoanDiv" class="form-group col-md-3">
                                                    <label class="infolabel" for="FHALoan">Current FHA Loan</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="FHALoan"
                                                           value="<?= $lead->FHALoan ?>"/>
                                                </div>
                                                <div id="SignedContractDiv" class="form-group col-md-3">
                                                    <label class="infolabel" for="SignedContract">Signed
                                                        Contract</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="SignedContract"
                                                           value="<?= $lead->SignedContract ?>"/>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="sectionHeader">
                                            <h4>VII. Misc Info</h4>
                                        </div>
                                        <div id="Mortgage-VII" class="sectionDiv">
                                            <div class="row">
                                                <div id="NeedPurchaseRealtorDiv" class="form-group col-md-3">
                                                    <label class="infolabel" for="NeedPurchaseRealtor">Need Purchase
                                                        Realtor</label><br/>
                                                    <label
                                                        class="radio_label <?= $lead->NeedPurchaseRealtor == '1' ? '' : 'no_print'; ?>">Yes</label>
                                                    <input type="radio" name="NeedPurchaseRealtor"
                                                           id="NeedPurchaseRealtor_Yes"
                                                           value="1" <?= $lead->NeedPurchaseRealtor == '1' ? 'CHECKED' : ''; ?> />
                                                    <label
                                                        class="radio_label <?= $lead->NeedPurchaseRealtor != '1' ? '' : 'no_print'; ?>">No</label><input
                                                        type="radio" name="NeedPurchaseRealtor"
                                                        id="NeedPurchaseRealtor_No"
                                                        value="0" <?= $lead->NeedPurchaseRealtor != '1' ? 'CHECKED' : ''; ?> />
                                                </div>
                                                <div id="RealtorNameDiv" class="form-group col-md-3">
                                                    <label class="infolabel" for="RealtorName">Realtor Name</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="RealtorName"
                                                           value="<?= $lead->RealtorName ?>"/>
                                                </div>
                                                <div id="RealtorPhoneDiv" class="form-group col-md-3">
                                                    <label class="infolabel" for="DOB">Realtor Phone</label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="RealtorPhone"
                                                           value="<?= $lead->RealtorPhone ?>"/>
                                                </div>
                                                <div id="NeedPurchaseRealtorDiv" class="form-group col-md-3">
                                                    <label class="infolabel" for="NeedSellRealtor">Need Sell
                                                        Realtor</label><br/>
                                                    <label
                                                        class="radio_label <?= $lead->NeedSellRealtor == '1' ? '' : 'no_print'; ?>">Yes</label>
                                                    <input type="radio" name="NeedSellRealtor" id="NeedSellRealtor_Yes"
                                                           value="1" <?= $lead->NeedSellRealtor == '1' ? 'CHECKED' : ''; ?> />
                                                    <label
                                                        class="radio_label <?= $lead->NeedSellRealtor != '1' ? '' : 'no_print'; ?>">No</label>
                                                    <input type="radio" name="NeedSellRealtor" id="NeedSellRealtor_No"
                                                           value="0" <?= $lead->NeedSellRealtor != '1' ? 'CHECKED' : ''; ?> />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <!-- End Mortgage -->

                            <?php endif; ?>


                            <div role="tabpanel" class="tab-pane" id="notes">

                                <h4>Notes</h4>

                                <textarea id="Comments" name="Comments" rows="5"
                                          class="hidden-print form-control input-sm copyfield"><?= $lead->Comments ?></textarea>
                                <span class="visible-print"><?= $lead->Comments; ?></span>


                            </div>
                            <!-- End Notes -->

                            <?php if (isset($optifinowGlobalSetting['custom']['SMSOnDemand']) && $optifinowGlobalSetting['custom']['SMSOnDemand'] == TRUE): ?>
                                <div role="tabpanel" class="tab-pane" id="messages">

                                    <h4>Messages
                                        <small>
                                            <a href="#"
                                               onclick="wopen('send_sms.php?id=<?= $lead->SubmissionListingID; ?>', 'send_sms', 400, 350);
                                                   return false;" title="Send SMS">sms <i
                                                    class="glyphicon glyphicon-phone"></i></a>
                                        </small>
                                    </h4>

                                    <div class="table-responsive">
                                        <table class="event_table table table-striped table-hover table-bordered">
                                            <thead style="font-weight: bold;">
                                            <tr>
                                                <td>Sender</td>
                                                <td>Text</td>
                                                <td>Time</td>
                                            </tr>

                                            </thead>
                                            <tbody>

                                            <?php foreach ($messages as $m): ?>
                                                <tr>
                                                    <?php if ($m->Type != ""): ?>
                                                        <?php
                                                        if ($m->Type == "Response") {
                                                            $label = 'warning';
                                                        } else if ($m->Type == "Manual Send") {
                                                            $label = 'info';
                                                        } else {
                                                            $label = 'default';
                                                        }
                                                        ?>
                                                        <td align="center"><span
                                                                class="label label-<?= $label ?>"><?= ($m->Type == "Response") ? $m->LeadName : $m->UserName ?></span>
                                                        </td>
                                                    <?php else: ?>
                                                        <td align="center"><span
                                                                class="label label-default"><?= (str_replace('+1', '', str_replace('-', '', $m->From)) == str_replace('+1', '', str_replace('-', '', $lead->Phone2))) ? $lead->FirstName . ' ' . $lead->LastName : $currentUser->FirstName . ' ' . $currentUser->LastName ?></span>
                                                        </td>
                                                    <?php endif; ?>
                                                    <td align="left"
                                                        style="min-width: 200px;"><?php echo $m->Body; ?></td>
                                                    <td align="center">
                                                        <i><?php echo date('Y-m-d g:ia', strtotime($m->Time)); ?></i>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- End Messages -->
                            <?php endif; ?>



                            <?php if (isset($optifinowGlobalSetting['custom']['Opportunity']) && $optifinowGlobalSetting['custom']['Opportunity'] == TRUE): ?>
                                <div role="tabpanel" class="tab-pane" id="opportunities">

                                    <div class="subSectionHeader">
                                        <h4>Opportunities
                                            <small> (<a href="#"
                                                        onclick="wopen('edit_opportunity.php?id=0&contactid=<?= $lead->SubmissionListingID; ?>', 'edit_opportunity', 350, 600);
                                                            return false;">create</a>)
                                            </small>
                                        </h4>
                                    </div>
                                    <div id="OpportunityDiv" class="sectionDiv table-responsive row">
                                        <table class="event_table table table-striped table-hover table-bordered">
                                            <tr class="event_table rowheader">
                                                <th class="event_table">
                                                    Stage
                                                </th>
                                                <th class="event_table">
                                                    Type
                                                </th>
                                                <th class="event_table">
                                                    Value
                                                </th>
                                                <th class="event_table">
                                                    Create Date
                                                </th>
                                                <th class="event_table">
                                                    Est. Close Date
                                                </th>
                                                <th class="event_table">
                                                    &nbsp;
                                                </th>
                                            </tr>
                                            <?php
                                            if (sizeof($oppts) > 0) {
                                                $i = 0;
                                                foreach ($oppts as $oppt) {
                                                    $i++;
                                                    ?>

                                                    <?php
                                                    $date = new DateTime($oppt->CreateDate, new DateTimeZone($originaltimezone));  //get date / time base on string saved and server's timezone
                                                    if (!empty($currentUser->UserTimezoneOffset)) {
                                                        $date->setTimezone(new DateTimeZone($currentUser->UserTimezoneOffset)); //update date / time with the user's preferred timezone (for UserTimezoneOffset)
                                                    }
                                                    $oppt->CreateDate = $date->format("m/d/Y g:i:s a");

                                                    $date = new DateTime($oppt->EstimatedCloseDate, new DateTimeZone($originaltimezone));  //get date / time base on string saved and server's timezone
                                                    if (!empty($currentUser->UserTimezoneOffset)) {
                                                        $date->setTimezone(new DateTimeZone($currentUser->UserTimezoneOffset)); //update date / time with the user's preferred timezone (for UserTimezoneOffset)
                                                    }
                                                    $oppt->EstimatedCloseDate = $date->format("m/d/Y g:i:s a");
                                                    ?>


                                                    <tr class="event_table<?= $i % 2 == 0 ? ' roweven' : ' rowodd' ?>">
                                                        <td class="event_table">
                                                            <?= $oppt->OpportunityStageName ?>
                                                        </td>
                                                        <td class="event_table">
                                                            <?= $oppt->OpportunityTypeName ?>
                                                        </td>
                                                        <td class="event_table">
                                                            $ <?= number_format($oppt->Value); ?>
                                                        </td>
                                                        <td class="event_table">
                                                            <?= strtotime($oppt->CreateDate) ? date("m/d/Y", strtotime($oppt->CreateDate)) : "" ?>
                                                        </td>
                                                        <td class="event_table">
                                                            <?= strtotime($oppt->EstimatedCloseDate) ? date("m/d/Y", strtotime($oppt->EstimatedCloseDate)) : "" ?>
                                                        </td>
                                                        <td class="event_table">
                                                            <a href="#"
                                                               onclick="wopen('edit_opportunity.php?id=<?= $oppt->OpportunityID; ?>', 'edit_opportunity', 350, 600);
                                                                   return false;">edit</a>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                }
                                            } else {
                                                ?>
                                                <tr class="event_table rowodd">
                                                    <td colspan="6" style="text-align: center;">
                                                        No Opportunities have been defined for this lead
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                        </table>
                                    </div>


                                </div>
                                <!-- End Opportunity -->
                            <?php endif; ?>

                            <div role="tabpanel" class="tab-pane" id="history">

                                <div id="EventTypeDiv" class="table-responsive">
                                    <table class="event_table table table-striped table-hover table-bordered">
                                        <tr class="event_table rowheader">
                                            <th class="event_table">
                                                Event Type
                                            </th>
                                            <th class="event_table">
                                                Event Time
                                            </th>
                                            <th class="event_table">
                                                Notes
                                            </th>
                                            <th class="event_table">
                                                User
                                            </th>
                                        </tr>

                                        <?php
                                        $i = 0;
                                        foreach ($allEvents as $event) {
                                            $i++;
                                            ?>

                                            <?php
                                            $date = new DateTime($event->EventDate, new DateTimeZone($originaltimezone));  //get date / time base on string saved and server's timezone
                                            if (!empty($currentUser->UserTimezoneOffset)) {
                                                $date->setTimezone(new DateTimeZone($currentUser->UserTimezoneOffset)); //update date / time with the user's preferred timezone (for UserTimezoneOffset)
                                            }
                                            $event->EventDate = $date->format("m/d/Y g:i:s a");
                                            ?>

                                            <tr class="event_table<?= $i % 2 == 0 ? ' roweven' : ' rowodd' ?>">
                                                <td class="event_table">
                                                    <?= $event->EventDescription ?>
                                                </td>
                                                <td class="event_table">
                                                    <?= strtotime($event->EventDate) ? date("m/d/Y g:i:s a", strtotime($event->EventDate)) : ""; ?>
                                                </td>
                                                <td class="event_table">
                                                    <?= $event->EventNotes ?>
                                                </td>
                                                <td class="event_table">
                                                    <?= $event->UserFullname ?>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </table>
                                </div>

                            </div>
                        </div>

                    </div>


                </div>

                </br>

                <div class="row center">
                    <span class="label label-default page-status"></span>
                </div>

                </br>

                <div class="row">

                    <center>
                        <div class="itemSection row">
                            <div class="buttonSection">
                                <input type="submit" class="btn btn-info" value="Update" name="Submit"/>&nbsp;&nbsp;
                                <input type="submit" class="btn btn-info" value="Update & Close" name="Submit"/>&nbsp;&nbsp;
                                <?php
                                if ($_GET['popup'] == 'true') {

                                } else { ?>
                                    <input type="submit" class="btn btn-default" value="Cancel"
                                           name="Submit"/>&nbsp;&nbsp;

                                    <?php if ($isMan) {
                                        ?>
                                        <input type="submit" class="btn btn-danger" value="Delete" name="Submit"
                                               onclick="return confirm('Are you sure you want to delete this lead?')"/>&nbsp;&nbsp;
                                        <?php
                                    }
                                    ?>

                                    <?php
                                }
                                ?>

                                <?php if (isset($CUSTOM_BUTTONS)): ?>

                                    <?php foreach ($CUSTOM_BUTTONS as $button): ?>

                                        <?php $buttonTitle = isset($button['field']) && isset($button['field_label']) && $lead->$button['field'] != '' ? $button['field_label'] . ": " . $lead->$button['field'] : $button['title']; ?>
                                        <?php $disabledButton = isset($button['field']) && $lead->$button['field'] != '' ? 'disabled' : ''; ?>

                                        <?php if ($button['popup']): ?>
                                            <a href="#"
                                               popup="wopen('<?= $button['lead_id'] ? $button['url'] . $lead->SubmissionListingID : $button['url'] ?>', '<?= $button['title'] ?>', 250, 250);"
                                               class="btn btn-primary btn-custom"
                                               data-toggle="tooltip" data-placement="top"
                                               title="<?= $buttonTitle ?>" <?= $disabledButton ?>><?= $buttonTitle ?></a>
                                        <?php else: ?>
                                            <a href="<?= $button['lead_id'] ? $button['url'] . $lead->SubmissionListingID : $button['url'] ?>"
                                               class="btn btn-primary btn-custom"
                                               data-toggle="tooltip" data-placement="top"
                                               title="<?= $buttonTitle ?>" <?= $disabledButton ?>><?= $buttonTitle ?></a>
                                        <?php endif; ?>

                                    <?php endforeach; ?>

                                <?php endif; ?>

                            </div>
                        </div>
                    </center>

                </div>

            </div>
            <!-- tabs div -->

            <div class="col-md-3">
                <div id="actions">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Actions</h3>
                        </div>
                        <div class="panel-body">

                            <div class="row">
                                <div id="ContactStatusTypeIDDiv" class="form-group col-md-12">
                                    <label for="ContactStatusTypeID">Status</label><br/>
                                <span style="text-align: left;">

                                    <?php if (isset($optifinowGlobalSetting['custom']['disabledStatus']) && $optifinowGlobalSetting['custom']['disabledStatus'] == TRUE): ?>
                                        <?php foreach ($contactStatusesTypes as $statusName => $status): ?>
                                            <?php if ($status->ContactStatusTypeID == $lead->ContactStatusTypeID): ?>
                                                <?= $statusName ?>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    <?php else: ?>

                                        <select name="ContactStatusTypeID"
                                                id="ContactStatusTypeID"
                                                class="form-submenu input-sm contactStatus">
                                            <?php
                                            foreach ($contactStatusesTypes as $statusName => $status) {
                                                ?>
                                                <option
                                                    value="<?= $status->ContactStatusTypeID ?>" <?= $status->ContactStatusTypeID == $lead->ContactStatusTypeID ? "SELECTED" : "" ?>>
                                                    <?= $statusName ?>
                                                </option>
                                                <?php
                                            }
                                            ?>
                                        </select>

                                    <?php endif; ?>

                                </span>
                                    <span class="visible-print"><?= $lead->ContactStatus ?></span>
                                </div>
                            </div>

                            <div class="row">
                                <div id="LastContactEventTypeIDDiv" class="form-group col-md-12">
                                    <label for="LastContactEventTypeID">Last Action</label>

                                    <div class="input-group" id="LastActionSpan">
                                        <span class="form-control">
                                            <?= $cetLookup[$lead->LastContactEvent->ContactEventTypeID] ? $cetLookup[$lead->LastContactEvent->ContactEventTypeID] : "No action taken." ?>
                                            </span>
                                        <span class="input-group-btn">
                                        <a href="#" class="btn btn-primary btn-custom"
                                           onclick="wopen('add_action.php?id=<?= $lead->SubmissionListingID; ?>', 'add_action', 350, 550);
                                               return false;" class="hidden-print">New Action</a>
                                            </span>
                                    </div>
                                </div>

                                <div class="row">
                                    <div id="LastActionDiv" class="form-group col-md-12">
                                        <label for="LastActionDiv">Last Action Date</label>

                                        <div class="input-group">
                                            <?= strtotime($lastCH->EventDate) ? date("m/d/Y g:i a", strtotime($lastCH->EventDate)) : "" ?>
                                        </div>
                                    </div>


                                </div>

                                <div class="row">
                                    <div id="NextAppointmentDiv" class="form-group col-md-12">
                                        <label for="NextAppointment">Next Appointment Date</label>

                                        <div class="input-group">
                                            <?php if ($appt): ?>
                                                <a href="#"
                                                   onclick="wopen('edit_appointment.php?id=<?= $appt->ContactAppointmentID ?>', 'edit', 350, 450);"><?= date("m/d/Y g:i a", strtotime($appt->AppointmentTime)) ?></a>
                                            <?php else: ?>
                                                -
                                            <?php endif; ?>
                                        </div>
                                    </div>

                                    <br/>

                                    <div class="itemSection row">
                                        <div class="col-md-2">
                                            <?php if ($appt): ?>
                                                <a href="#" class="btn btn-primary"
                                                   onclick="wopen('view_appointment.php?contactID=<?= $lead->SubmissionListingID ?>', 'edit', 450, 450);"
                                                   data-toggle="tooltip" data-placement="top"
                                                   title="View Appointments"><span
                                                        class="glyphicon glyphicon-calendar" aria-hidden="true"
                                                        <?php if ($appt->PastDue) { ?> style="color: red;" <?php } ?>></span></a>
                                            <?php else: ?>
                                                <a href="#" class="btn btn-primary"
                                                   onclick="appointmentPopup(<?= $lead->SubmissionListingID; ?>);"
                                                   data-toggle="tooltip" data-placement="top"
                                                   title="Add Appointment"><span
                                                        class="glyphicon glyphicon-time" aria-hidden="true"></span></a>
                                            <?php endif; ?>
                                        </div>
                                        <?php if (isset($optifinowGlobalSetting['custom']['Opportunity']) && $optifinowGlobalSetting['custom']['Opportunity'] == TRUE): ?>
                                            <div class="col-md-2">
                                                <a href="#" class="btn btn-primary"
                                                   onclick="wopen('edit_opportunity.php?id=0&contactid=<?= $lead->SubmissionListingID; ?>', 'edit_opportunity', 350, 600);
                                                       return false;" data-toggle="tooltip" data-placement="top"
                                                   title="Create Opportunity"><span class="glyphicon glyphicon-usd"
                                                                                    aria-hidden="true"></span></a>
                                            </div>
                                        <?php endif; ?>
                                        <?php if (isset($optifinowGlobalSetting['custom']['ContentOnDemand']) && $optifinowGlobalSetting['custom']['ContentOnDemand'] == TRUE): ?>
                                            <div class="col-md-2">
                                                <a href="#" class="btn btn-primary"
                                                   onclick="wopen('email_file.php?slid=<?= $lead->SubmissionListingID; ?>', 'choose_content', 900, 600);
                                                       return false;" data-toggle="tooltip" data-placement="top"
                                                   title="Email ContentOnDemand"><span
                                                        class="glyphicon glyphicon-paperclip"
                                                        aria-hidden="true"></span></a>
                                            </div>
                                        <?php endif; ?>
                                        <div class="col-md-2">
                                            <a href="mailto:<?= $lead->Email; ?>" class="btn btn-primary"
                                               data-toggle="tooltip" data-placement="top"
                                               title="Compose New Email"><span
                                                    class="glyphicon glyphicon-envelope"
                                                    aria-hidden="true"></span></a>
                                        </div>

                                        <?php if (isset($CUSTOM_BUTTONS)): ?>

                                            <?php foreach ($CUSTOM_BUTTONS as $button): ?>

                                                <?php $buttonTitle = isset($button['field']) && isset($button['field_label']) && $lead->$button['field'] != '' ? $button['field_label'] . ": " . $lead->$button['field'] : $button['title']; ?>
                                                <?php $disabledButton = isset($button['field']) && $lead->$button['field'] != '' ? 'disabled' : ''; ?>

                                                <div class="col-md-2">
                                                    <?php if ($button['popup']): ?>
                                                        <a href="#"
                                                           popup="wopen('<?= $button['lead_id'] ? $button['url'] . $lead->SubmissionListingID : $button['url'] ?>', '<?= $button['title'] ?>', 250, 250);"
                                                           class="btn btn-primary btn-custom"
                                                           data-toggle="tooltip" data-placement="top"
                                                           title="<?= $buttonTitle ?>" <?= $disabledButton ?>><span
                                                                class="<?= $button['icon'] ?>"
                                                                aria-hidden="true"></span></a>
                                                    <?php else: ?>
                                                        <a href="<?= $button['lead_id'] ? $button['url'] . $lead->SubmissionListingID : $button['url'] ?>"
                                                           class="btn btn-primary btn-custom"
                                                           data-toggle="tooltip" data-placement="top"
                                                           title="<?= $buttonTitle ?>" <?= $disabledButton ?>><span
                                                                class="<?= $button['icon'] ?>"
                                                                aria-hidden="true"></span></a>
                                                    <?php endif; ?>
                                                </div>

                                            <?php endforeach; ?>

                                        <?php endif; ?>


                                    </div>

                                    <br/>

                                    <div class="row center">
                                        <span class="label label-default page-status"></span>
                                    </div>

                                    <br/>

                                    <div class="buttonSection row">
                                        <?php
                                        if (isset($_GET['index']) && isset($lead_step_ids[intval($_GET['index']) - 1])) {
                                            ?>
                                            <a href="edit_lead.php?id=<?= $lead_step_ids[intval($_GET['index']) - 1] ?>&index=<?= intval($_GET['index']) - 1 ?>">
                                                &lt;- Prev</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <?php
                                        }
                                        ?>
                                        <div class="form-group col-md-6">
                                            <input type="submit" class="btn btn-info btn-block btn-submit"
                                                   value="Update"
                                                   name="Submit"/>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <input type="submit" class="btn btn-info btn-block" value="Update & Close"
                                                   name="Submit"/>
                                        </div>
                                        <?php
                                        if ($_GET['popup'] == 'true') {

                                        } else { ?>
                                            <div class="form-group col-md-6">
                                                <input type="submit" class="btn btn-default btn-block" value="Cancel"
                                                       name="Submit"/>
                                            </div>

                                            <?php if ($isMan) {
                                                ?>
                                                <div class="form-group col-md-6">
                                                    <input type="submit" class="btn btn-danger btn-block" value="Delete"
                                                           name="Submit"
                                                           onclick="return confirm('Are you sure you want to delete this lead?')"/>
                                                </div>
                                                <?php
                                            }
                                            ?>

                                            <?php
                                        }
                                        if (isset($_GET['index']) && isset($lead_step_ids[intval($_GET['index']) + 1])) {
                                            ?>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a
                                                href="edit_lead.php?id=<?= $lead_step_ids[intval($_GET['index']) + 1] ?>&index=<?= intval($_GET['index']) + 1 ?>">Next
                                                -&gt;</a>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end buttons -->

                    </div>
                    <!-- actions div -->

                </div>
                <!-- row -->

                <div id="sidebar-note">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Notes</h3>
                        </div>
                        <div class="panel-body">
                                                            <textarea name="Comments" rows="8"
                                                                      class="hidden-print form-control input-sm copyfield"><?= $lead->Comments ?></textarea>
                            <span class="visible-print"><?= $lead->Comments; ?></span>
                        </div>
                    </div>
                </div>


    </form>
    <form method="post" action="<?= $REAL_TAG_URL ?>" target="_blank" id="RealTagForm">
        <input type="hidden" name="name" value="<?= $lead->FirstName . " " . $lead->LastName ?>"/>
        <input type="hidden" name="address" value="<?= $lead->Address1 ?>"/>
        <input type="hidden" name="zip" value="<?= substr($lead->Zip, 0, 5) ?>"/>
    </form>
    <form method="post" action="<?= $LOAN_COMP_URL ?>" target="_blank" id="LoanCompForm">
        <input type="hidden" name="custName" value="<?= $lead->FirstName . " " . $lead->LastName ?>"/>
        <input type="hidden" name="custAddr" value="<?= $lead->Address1 ?>"/>
        <input type="hidden" name="custCSZ" value="<?= "{$lead->City}, {$lead->State} {$lead->Zip}" ?>"/>
        <input type="hidden" name="custEmail" value="<?= $lead->Email ?>"/>


        <input type="hidden" name="loName" value="<?= $currentUser->FirstName . " " . $currentUser->LastName ?>"/>
        <input type="hidden" name="loTollFreePhone" value="<?= $currentUser->ContactPhone ?>"/>
        <input type="hidden" name="loDirectPhone" value="<?= $currentUser->ContactCell ?>"/>
        <input type="hidden" name="loFax" value="<?= $currentUser->ContactFax ?>"/>
        <input type="hidden" name="loEmail" value="<?= $currentUser->ContactEmail ?>"/>

        <input type="hidden" name="fMortBal" value="<?= $lead->Mortgage1Balance ?>"/>
        <input type="hidden" name="fPmt" value="<?= $lead->Mortgage1Payment ?>"/>
        <input type="hidden" name="sMortBal" value="<?= $lead->Mortgage2Balance ?>"/>
        <input type="hidden" name="sPmt" value="<?= $lead->Mortgage2Payment ?>"/>
        <input type="hidden" name="pTaxBal" value="<?= $lead->PropertyTaxes ?>"/>
        <input type="hidden" name="pTaxPmt" value="<?= number_format($lead->PropertyTaxes / 12, 2); ?>"/>
        <input type="hidden" name="insBal" value="<?= $lead->AnnualInsurance; ?>"/>
        <input type="hidden" name="insPmt" value="<?= number_format($lead->AnnualInsurance / 12, 2); ?>"/>
        <input type="hidden" name="oLiabBal"
               value="<?= $lead->OtherDebt1Balance + $lead->OtherDebt2Balance + $lead->OtherDebt3Balance; ?>"/>
        <input type="hidden" name="oLiabPmt"
               value="<?= $lead->OtherDebt1Payment + $lead->OtherDebt2Payment + $lead->OtherDebt3Payment; ?>"/>

        <input type="hidden" name="cHomeVal" value="<?= $lead->MarketValue; ?>"/>

        <input type="hidden" name="transferIn" value="1"/>
    </form>
    <form method="post" action="<?= $PURCH_PROP_URL ?>" target="_blank" id="PurchPropForm">
        <input type="hidden" name="custName" value="<?= $lead->FirstName . " " . $lead->LastName ?>"/>
        <input type="hidden" name="custAddr" value="<?= $lead->Address1 ?>"/>
        <input type="hidden" name="custCSZ" value="<?= "{$lead->City}, {$lead->State} {$lead->Zip}" ?>"/>
        <input type="hidden" name="custEmail" value="<?= $lead->Email ?>"/>


        <input type="hidden" name="loName" value="<?= $currentUser->FirstName . " " . $currentUser->LastName ?>"/>
        <input type="hidden" name="loTollFreePhone" value="<?= $currentUser->ContactPhone ?>"/>
        <input type="hidden" name="loDirectPhone" value="<?= $currentUser->ContactCell ?>"/>
        <input type="hidden" name="loFax" value="<?= $currentUser->ContactFax ?>"/>
        <input type="hidden" name="loEmail" value="<?= $currentUser->ContactEmail ?>"/>

        <input type="hidden" name="fMortBal" value="<?= $lead->Mortgage1Balance ?>"/>
        <input type="hidden" name="fPmt" value="<?= $lead->Mortgage1Payment ?>"/>
        <input type="hidden" name="sMortBal" value="<?= $lead->Mortgage2Balance ?>"/>
        <input type="hidden" name="sPmt" value="<?= $lead->Mortgage2Payment ?>"/>
        <input type="hidden" name="pTaxBal" value="<?= $lead->PropertyTaxes ?>"/>
        <input type="hidden" name="pTaxPmt" value="<?= number_format($lead->PropertyTaxes / 12, 2); ?>"/>
        <input type="hidden" name="insBal" value="<?= $lead->AnnualInsurance; ?>"/>
        <input type="hidden" name="insPmt" value="<?= number_format($lead->AnnualInsurance / 12, 2); ?>"/>
        <input type="hidden" name="oLiabBal"
               value="<?= $lead->OtherDebt1Balance + $lead->OtherDebt2Balance + $lead->OtherDebt3Balance; ?>"/>
        <input type="hidden" name="oLiabPmt"
               value="<?= $lead->OtherDebt1Payment + $lead->OtherDebt2Payment + $lead->OtherDebt3Payment; ?>"/>

        <input type="hidden" name="cHomeVal" value="<?= $lead->MarketValue; ?>"/>

        <input type="hidden" name="transferIn" value="1"/>
    </form>
    <div style="display: none;" class="detail_div" id="detailDiv">
        <div style="text-align: center; background: #CCC; padding: 3px 0px 3px 0px; font-weight: bold;">
            Please Choose a detailed description for<br/>
            <span id="shortStatus"></span>
        </div>
        <div style="width: 394px; text-align: center; padding: 25px 0px 15px 0px;">
            <select class="form-control input-sm" name="detailStatusID" id="detailStatusID">
            </select>
        </div>
        <div style="width: 394px; text-align: center; background: #FFF; padding-bottom: 15px;">
            <a href="#" onclick="submitDetailedContactStatus();">OK</a>
        </div>
    </div>

    <?php include("components/footer.php") ?>

    <script type="text/javascript" src="js/leaddetail_compute.js"></script>

</body>
</html>